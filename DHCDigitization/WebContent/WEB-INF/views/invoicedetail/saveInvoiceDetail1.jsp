<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="author" content="Rajkumar Giri, Rupnarayan Shahu">
<title>Fresh case</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
.centered-modal.in {
    display: flex !important;
}
.centered-modal .modal-dialog {
    margin: auto;
}
.blue-input:focus {
  background: green;
  color: white;
}
</style>
<script type="text/javascript">

// by bilal


function submitform()
{
	
  document.myform.submit();
}


$(function() {
	 $('#no-of-page').change(function(){
		 $.ajax({
		    	type :'post',
				url : '${pageContext.request.contextPath}/calculateAmount',
				 data : {
					noOfPage : $('#no-of-page').val()
				}, 
				dataType:'json', 
				success : function(result) {
					$('#amount').val(result);
				}
			});
	}); 
	$('.report').datepicker({ dateFormat: 'dd-mm-yy' });
});

var form = document.getElementById("print");
form.reset(); 

 function validateInvoiceFrm() {
	 
	var casetype=document.getElementById("case-type").value;	
	var clientname=document.getElementById("client-name").value;	
	var caseno=document.getElementById("caseno").value;	
	var noofpage=document.getElementById("no-of-page").value;	
	var amount=document.getElementById("amount").value;	
	if(casetype==""){
		casetype=document.getElementById("case-type-msg").innerHtml="";
		return false;
	}
	if(clientname==""){
		casetype=document.getElementById("case-type-msg").innerHtml="";
		return false;
	}
	if(caseno==""){
		casetype=document.getElementById("case-type-msg").value="error";
		return false;
	}
	if(noofpage==""){
		casetype=document.getElementById("case-type-msg").innerHtml="";
		return false;
	}
	if(amount==""){
		casetype=document.getElementById("case-type-msg").innerHtml="";
		return false;
	}
	return 	false;
} 
 
 disableEnterKey =function ()
 {
	 console.log("innnnnnnnnnnnnnnnnnnnnnnnnnnn");
      var key;      
      if(window.event)
           key = window.event.keyCode; //IE
      else
           key = e.which; //firefox      

      return (key != 13);
 }
 
 input.addEventListener('keypress',disableEnterKey,false);
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">FRESH CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning clearfix">
		<form:form action="${pageContext.request.contextPath}/saveInvoiceDetail" name ="myForm"  method="post" modelAttribute="invoiceDetail" autocomplete="off" target="_blank" onsubmit="return validateInvoiceFrm()">
		<form:hidden path="autoNo"/>
		<table class="table table-condensed borderless clearfix">
		<tbody>
			<tr>
				<td align="right" class="label-text">
					<label for="freshCaseDate">DATE</label>
				</td>
				<td colspan="4">
					<input type="text" id="freshcase-date" class="form-control input-sm input-width" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${freshDate}"/>" disabled="disabled">
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="caseType">CASE TYPE</form:label>
				</td>
				<td colspan="4">
					<form:select path="caseType" id="case-type" class="form-control input-sm input-width">
					<form:option value="">-- SELECT --</form:option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'FRESH_CASE_TYPE'}">
						<form:option value="${lookup.lookupValue}">${lookup.lookupValue}</form:option>
						</c:if>
					</c:forEach>
					</form:select>
					<form:errors path="caseType" id="case-type-msg" class="text-danger"></form:errors>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="clientName">CLIENT NAME</form:label>
				</td>
				<td colspan="4">
					<form:input path="clientName" id="client-name" onkeypress="disableEnterKey(event)" class="form-control input-sm input-width"/>
					<form:errors path="clientName" class="text-danger"></form:errors>
					<span id="client-name-msg" class="text-danger"></span>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="caseNo">CASE NO</form:label>
				</td>
				<td colspan="4">
					<form:input path="caseNo" value="NA" id="caseno" class="form-control input-sm input-width"/>
					<form:errors path="caseNo" id="caseno-msg" class="text-danger"></form:errors>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="noOfPage">NO OF PAGE</form:label>
				</td>
				<td colspan="4">
				    <form:input path="noOfPage" id="no-of-page" onchange="checkAllNumeric(this);" class="form-control input-sm input-width"/>
					<form:errors path="noOfPage" class="text-danger"></form:errors>
					<span id="no-of-page-msg" class="text-danger"></span>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="amount">AMOUNT</form:label>
				</td>
				<td colspan="4">
				    <form:input path="amount" id="amount" class="form-control input-sm" readonly="true"/>
				    <form:errors path="amount" id="amount-msg" class="text-danger"></form:errors>
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td><input class="btn btn-sm btn-primary btn-block form-btn input-width blue-input" type="submit" value="Print" onclick="submitform()"></td>
				<td><a class="btn btn-sm btn-warning btn-block form-btn input-width" role="button" href="${pageContext.request.contextPath}/newInvoice">RESET</a>	</td>
				<td></td>
			</tr>
			<tr>
			 	<td></td>
			 	<td><button class="btn btn-sm btn-info btn-block form-btn input-width" type="button" data-toggle="modal" data-target="#myModal">PRINT PREVIOUS BILL</button></td>
				<td><button class="btn btn-sm btn-success btn-block form-btn input-width" type="button" data-toggle="modal" data-target="#reportModal">REPORT</button></td>
				<td><button class="btn btn-sm btn-danger btn-block form-btn input-width" type="button" data-toggle="modal" data-target="#billCancelModal">BILL CANCEL</button></td>
				<td><button class="btn btn-sm btn-warning btn-block form-btn input-width" type="button" data-toggle="modal" data-target="#cancelreportModal">BILL CANCEL REPORT</button></td>
			</tr>
		</tbody>
	</table>
	</form:form>
	</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
 </div>
</div>
</div>
<div class="modal fade centered-modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times text-danger"></i></button>
          <h6 class="modal-title">PRINT PREVIOUS BILL</h6>
        </div>
        <div class="modal-body">
        <form:form autocomplete="off" action="${pageContext.request.contextPath}/previousReciept" method="post" target="_blank">
         <table class="table table-condensed borderless clearfix">
		 <tbody>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="billNo">BILL NUMBER</label>
				</td>
				<td>
					<input type="text" name="billNo" id="billNo" class="form-control input-sm input-width" required="required"/>
				</td>
				<td>
					<button class="btn btn-sm btn-success btn-block form-btn input-width" type="submit">PRINT</button>
				</td>
			</tr>
			</tbody>
			</table>
		</form:form>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade centered-modal" id="billCancelModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times text-danger"></i></button>
          <h6 class="modal-title">BILL CANCEL</h6>
        </div>
        <div class="modal-body">
        <form:form autocomplete="off" action="${pageContext.request.contextPath}/cancelInvoiceDetail" method="post">
        <table class="table table-condensed borderless clearfix">
		<tbody>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="billNo">BILL NUMBER</label>
				</td>
				<td colspan="2">
					<input type="text" name="billNo" id="billNo" class="form-control input-sm input-width" required="required"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="canceledremark">REMARK</label>
				</td>
				<td colspan="2">
					<input type="text" name="canceledRemark" id="canceledremark" class="form-control input-sm input-width" required="required"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><button class="btn btn-sm btn-success btn-block form-btn input-width" type="submit">OK</button></td>
				<td><button class="btn btn-sm btn-success btn-block form-btn input-width" type="reset">CLEAR</button></td>
			</tr>
			</tbody>
			</table>
			</form:form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade centered-modal" id="reportModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times text-danger"></i></button>
          <h6 class="modal-title">PRINT BILL REPORT</h6>
        </div>
        <div class="modal-body">
        <form:form id="report-form" autocomplete="off" action="${pageContext.request.contextPath}/getAllInvoiceDetailListByDate" method="post">
         <table class="table table-condensed borderless clearfix">
		 <tbody>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="billNo">FROM DATE</label>
				</td>
				<td>
					<input type="text" name="fromDate" id="from-date" class="form-control input-sm input-width report" required="required"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="billNo">TO DATE</label>
				</td>
				<td>
					<input type="text" name="toDate" id="to-date" class="form-control input-sm input-width report" required="required"/>
				</td>
			</tr>
			<tr>
				<td>
					<button class="btn btn-sm btn-success btn-block form-btn input-width" type="submit" onsubmit="reportformReset();">DOWNLOAD</button>
				</td>
			</tr>
			</tbody>
			</table>
		</form:form>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade centered-modal" id="cancelreportModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times text-danger"></i></button>
          <h6 class="modal-title">PRINT CANCELED BILL</h6>
        </div>
        <div class="modal-body">
        <form:form autocomplete="off" action="${pageContext.request.contextPath}/getAllCancelInvoiceDetailListByDate" method="post">
         <table class="table table-condensed borderless clearfix">
		 <tbody>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="billNo">FROM DATE</label>
				</td>
				<td>
					<input type="text" name="fromDate" class="form-control input-sm input-width report" required="required"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="billNo">TO DATE</label>
				</td>
				<td>
					<input type="text" name="toDate" class="form-control input-sm input-width report" required="required"/>
				</td>
			</tr>
			<tr>
				<td>
					<button class="btn btn-sm btn-success btn-block form-btn input-width" type="submit" onsubmit="reportformReset();">DOWNLOAD</button>
				</td>
			</tr>
			</tbody>
			</table>
		</form:form>
        </div>
      </div>
    </div>
  </div>
  <c:if test="${not empty reportmessage}">
    <script type="text/javascript">
    	$(function() {
		 $('#remportsuccessmodal').modal('show');
		});
    </script>
  </c:if>
  <div class="modal fade centered-modal" id="remportsuccessmodal" role="dialog">
    <div class="modal-dialog modal-xs">
      <div class="modal-content text-center">
        <div class="modal-body">
         <p class="text-success">BILL NOT FOUND</p>
         <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>