<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="author" content="Rajkumar Giri, Rupnarayan Shahu">
<title>Fresh case</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
.centered-modal.in {
    display: flex !important;
}
.centered-modal .modal-dialog {
    margin: auto;
}
</style>
<style type="text/css">
hr.style1{
	border-top: 1px solid #8c8b8b;
	width: 200px;
	margin-top: 0px;
	margin-bottom: 0px;
}
hr.style2{
	border-top: 1px solid #8c8b8b;
	width: 100%;
	margin-top: 0px;
	margin-bottom: 0px;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">FRESH CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
		<div class="panel panel-body panel-warning clearfix">
		<form:form autocomplete="off" name="contact-form" action="${pageContext.request.contextPath}/saveInvoiceDetail" method="post">
		<table class="table table-condensed borderless clearfix">
			<tbody>
				<tr><td colspan="2" class="text-center">* To be filed with the Pet./Appl./Doc *</td></tr>
				<tr><td colspan="2" class="text-center"><strong>Scanning Fee Charges</strong></td></tr>
				<tr><td colspan="2"><hr class="style1"></td></tr>
				<tr><td colspan="2" class="text-center">HIGH COURT OF DELHI</td></tr>
				<tr><td colspan="2" class="text-center">NEW DELHI-110003</td></tr>
				<tr><td colspan="2" class="text-center">CASE MEMO</td></tr>
				<tr><td colspan="2"><hr class="style2"></td></tr>
				<tr>
					<td>Bill No.</td>
					<td>${invoiceDetail.billNo}</td>
				</tr>
				<tr>
					<td>Date.</td>
					<td><fmt:formatDate type = "date" value = "${invoiceDetail.createdOn}" pattern="dd-MM-yyyy"/> Time <fmt:formatDate type = "time" value = "${invoiceDetail.createdOn}"/></td>
				</tr>
				<tr>
					<td>Case Type.</td>
					<td>${invoiceDetail.caseType}</td>
				</tr>
				<tr>
					<td>Case No.</td>
					<td>${invoiceDetail.caseNo}</td>
				</tr>
				<tr>
					<td>Pages</td>
					<td>${invoiceDetail.noOfPage}</td>
				</tr>
				<tr>
					<td>Total Amount</td>
					<td>${invoiceDetail.amount}</td>
				</tr>
				<tr>
					<td>Auto</td>
					<td>${invoiceDetail.invoiceId}</td>
				</tr>
				<tr><td colspan="2"><hr class="style2"></td></tr>
				<tr><td colspan="2">"--THANK YOU--"</td></tr>
				<tr>
					<td align="center"><button class="btn btn-sm btn-primary " type="submit">PRINT</button></td>
				</tr>
		</tbody>
	</table>
	</form:form>
	</div>
	</div>
	<div class="col-sm-4"></div>
	</div>
 </div>
</div>
</div>
</section>
</body>
</html>