<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">USER ROLE LIST</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
		<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-6">
		<form:form autocomplete="off" action="${pageContext.request.contextPath}/saveUserRole" method="post" modelAttribute="userRole">
		<form:hidden path="roleId"/>
		<table class="table table-condensed borderless">
		<tbody>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="roleName">ROLE NAME:</form:label>
				</td>
				<td>
					<form:input path="roleName" class="form-control input-sm" placeholder="ENTER USER ROLE"/>
					<form:errors path="roleName" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="center">
				 <button class="btn btn-sm btn-primary btn-block form-btn" type="submit">SUBMIT</button>
				</td>
			</tr>
		</tbody>
	</table>
	</form:form> 
	<div  class="text-success small" align="center">
	<c:if test="${not empty successMessage}">
	<script type="text/javascript">
		alert("User Role saved successfully");
	</script>
	</c:if>
	</div>
	<div  class="text-danger" align="center">${errorMessage}</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
	</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<table class="table table-condensed table-bordered">
		<thead>
			<tr class="bg-primary">
				<th>ROLE NAME</th>
				<th>CREATED BY</th>
				<th class="text-center">CREATED ON</th>
				<th class="text-center">STATUS</th>
				<th class="text-center" colspan="3">ACTION</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="userRole" items="${userRoleList}">
				<tr>
				 <td>${userRole.roleName}</td>
				 <td>${userRole.createdBy}</td>
				 <td class="text-center"><fmt:formatDate pattern="dd-MM-yyyy" value="${userRole.createdOn}" /></td>
				 <td class="text-center ${userRole.status}"><i class="fa fa-check fa-lg " aria-hidden="true"></i></td>
				 <td align="center">
					 <a href="${pageContext.request.contextPath}/editUserRole?roleId=${userRole.roleId}" class="btn btn-xs btn-warning" role="button"><i class="fa fa-pencil"></i> EDIT</a>
				 </td>
				</tr>
			</c:forEach>
			<c:if test="${empty userRoleList}">
				<tr><td colspan="7" class="text-warning">RECORD NOT FOUND</td></tr>
			</c:if>
		</tbody>
		<c:if test="${not empty userRoleList}">
		<tfoot>
			<tr class="bg-primary">
				<td>Total: ${userRoleList.size()}</td>
				<td></td>
				<td colspan="5"></td>
	       </tr>
		</tfoot>
		</c:if>
		</table>
		</div>
		<div class="col-sm-2"></div>
	</div>
	</div>
	</div>
</div>
</section>
</body>
</html>