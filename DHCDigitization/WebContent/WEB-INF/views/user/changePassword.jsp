<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
</style>
<script> 
// Function to check Whether both passwords 
// is same or not. 
function checkPassword(form) { 
    var password=document.getElementById("");
    var repassword=document.getElementById("");

    // If password not entered 
    if (password1 == '') 
        alert ("Please enter Password"); 
          
    // If confirm password not entered 
    else if (password2 == '') 
        alert ("Please enter confirm password"); 
          
    // If Not same return False.     
    else if (password1 != password2) { 
        alert ("\nPassword did not match: Please try again...") 
        return false; 
    } 

    // If same return True. 
    else{ 
        alert("Password Match: Welcome to GeeksforGeeks!") 
        return true; 
    } 
} 
</script> 
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">CHANGE PASSWORD</li>
   	</ol>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
		<form:form autocomplete="off" action="${pageContext.request.contextPath}/saveChangePassword" method="post" modelAttribute="users">
		<table class="table table-condensed borderless">
		<tbody>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="password">PASSWORD</form:label>
				</td>
				<td>
					<form:password path="password" class="form-control input-sm" placeholder="ENTER PASSWORD"/>
					<form:errors path="password" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="reconfirmPassword">REPASSWORD</form:label>
				</td>
				<td>
					<form:password path="reconfirmPassword" class="form-control input-sm" placeholder="ENTER PASSWORD"/>
					<form:errors path="reconfirmPassword" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="center">
				 <button class="btn btn-sm btn-primary btn-block form-btn" type="submit">SUBMIT</button>
				</td>
			</tr>
		</tbody>
	</table>
	<div  class="text-success" align="center">
	<c:if test="${not empty passwordmsg}">
	<p class="text-center">${passwordmsg}</p>
	</c:if>
	</div>
	</form:form> 
	</div>
	</div>
	<div class="col-sm-4"></div>
	</div>
	</div>
	</div>
</div>
</section>
</body>
</html>