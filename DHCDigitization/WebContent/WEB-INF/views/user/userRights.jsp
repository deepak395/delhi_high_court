<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
</style>
<script type="text/javascript">
function saveOrUpdateUserRights(menuId, submenuId) {
    $.ajax({
    	type :'POST',
		url : '${pageContext.request.contextPath}/updateUserRights',
		data : {
			username :$("#username").val(),
			menuId :menuId,
			submenuId :submenuId
		},
		success : function(result) {
			$("#msg").html(result);
		}
	});
}
</script>
</head>
<body>

<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">USER ROLE LIST</li>
   	</ol>
	<div class="row">
  	    <div class="col-sm-3"></div>
		<div class="col-sm-6">
		<form action="${pageContext.request.contextPath}/getUserRightsByUsername" method="post">
		<table class="table table-condensed table-bordered">
		<thead>
		<tr>
			<td>
			  <input type="hidden" value="${username}" id="username">
			   <select  class="form-control input-sm" name="username">
				   <option value="">SELECT USER</option>
				   <c:forEach var="user" items="${userList}">
				   <option value="${user.username}">${user.username}</option>
				   </c:forEach>
			 </select>
			</td>
			<td>
				<button class="btn btn-sm btn-primary form-btn" type="submit">SEARCH</button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span id="msg" class="text-success text-center"></span>
			</td>
			<td></td>
		</tr>
		<tr class="bg-primary">
			<th width="200">MENU</th>
			<th width="400">SUBMENU</th>
			<th width="20" class="text-center">PERMISSION</th>
		</tr>
	</thead>
	<tbody data-ng-repeat="menuForm in menuFormList">
		<c:forEach var="menuForm" items="${menuFormLists}">
		<tr class="bg-danger">
			<td colspan="4">
				${menuForm.menu.menuName}
			</td>
		</tr> 
		<c:forEach var="submenu" items="${menuForm.submenuList}">
		<tr>
		    <td></td>
			<td>${submenu.submenuName}</td>
			<td class="text-center">
			<c:choose>
				<c:when test="${submenu.status eq true}">
					<input type="checkbox" id="submenuId" onclick="saveOrUpdateUserRights('${menuForm.menu.menuId}','${submenu.submenuId}')" checked="checked"> 
				</c:when>
				<c:otherwise>
				<input type="checkbox" id="submenuId" onclick="saveOrUpdateUserRights('${menuForm.menu.menuId}','${submenu.submenuId}')"> 
				</c:otherwise>
			</c:choose>
			</td>
		</tr>
		</c:forEach>
		</c:forEach>
	</tbody>
	</table> 
	</form> 
	</div>
	</div>
	<div class="col-sm-3"></div>
	</div>
	</div>
	</div>
</section>
</body>
</html>