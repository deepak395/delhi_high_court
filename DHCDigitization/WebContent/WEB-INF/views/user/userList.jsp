<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	  $("#localFileLocation").change(function(){
	        var str = $("#localFileLocation").val();
	          var regex = /^[a-z]:((\/|(\\?))[\w .]+)+/i;
	          var patt = new RegExp(regex);
	          var res = patt.test(str);
	          var n =  str.match(/\\/);
	           
	          if (n !== null){
	                    var str = str.replace("\\", "\\\\");
	                    str = str.concat("\\\\");1
	             }
	             var n =  str.match(/\//);
	           if (n !== null){
	                    var str = str.replace("\/", "\\\\");
	                    str = str.concat("\\\\");
	             }
	           $("#localFileLocation").val(str);
	    });
});
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">USER ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-4">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
		<form:form autocomplete="off" action="${pageContext.request.contextPath}/saveUser" method="post" modelAttribute="userDetail">
		<form:hidden path="id"/>
		<table class="table table-condensed borderless">
		<tbody>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="roleName">ROLE NAME</form:label>
				</td>
			     <td>
					<form:select path="roleName" class="form-control input-sm">
						<form:option value="">SELECT ROLE</form:option>
						<c:forEach var="userRole" items="${userRoleList}">
						<form:option value="${userRole.roleName}">${userRole.roleName}</form:option>
						</c:forEach>
					</form:select>			   
					<form:errors path="roleName" class="text-danger"/>
				</td> 
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="userFullName">FULL NAME</form:label>
				</td>
				<td>
					<form:input path="userFullName" class="form-control input-sm"  placeholder="ENTER FULLNAME"/>
					<form:errors path="userFullName" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="username">USERNAME</form:label>
				</td>
				<td>
					<form:input path="username" class="form-control input-sm"  placeholder="ENTER USERNAME"/>
					<form:errors path="username" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="password">PASSWORD</form:label>
				</td>
				<td>
					<form:password path="password" class="form-control input-sm" placeholder="ENTER PASSWORD"/>
					<form:errors path="password" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="reconfirmPassword">REPASSWORD</form:label>
				</td>
				<td>
					<form:password path="reconfirmPassword" class="form-control input-sm" placeholder="ENTER PASSWORD"/>
					<form:errors path="reconfirmPassword" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="localFileLocation">SYSTEM PATH</form:label>
				</td>
				<td>
					<form:input path="localFileLocation" class="form-control input-sm" id='localFileLocation' value="D:\DHC_DOWNLOAD" placeholder="ENTER SOURCE PATH"/>
					<form:errors path="localFileLocation" class="text-danger"/>
				</td>
			</tr>
			<tr>
				<td align="center"></td>
				<td align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn" id='saveUser' type="submit">SUBMIT</button>
				</td>
			</tr>
		</tbody>
	</table>
	<div  class="text-success" align="center">${successMessage}</div>
	<div  class="text-danger" align="center">${errorMessage}</div>
	</form:form> 
	</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
			<form class="form-horizontal form-margin" action="${pageContext.request.contextPath}/uploadUser" method="post" enctype="multipart/form-data">
				<table class="table table-condensed borderless">
				<tr>
					<td colspan="2" align="center"></td>
				</tr>
				<tr>
					<td align="right" class="label-text required">
					   <label>SELECT FILE</label>
					</td>
					<td>
						<input type="file" name="docFile" accept=".xls,.xlsx"  placeholder="select file" required="required"/>
					</td>
			  </tr>
	          <tr>
				<td align="center">
					<a href="${pageContext.request.contextPath}/downloadUserTemplate" class="btn btn-sm btn-warning" role="button">
					<i class="fa fa-download"></i> DOWNLOAD TEMPLATE</a>
				</td>
				<td align="center">
					<button class="btn btn-sm btn-primary form-btn" type="submit">UPLOAD</button>
				</td>
			 </tr>
			</table>
			</form>
		</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<table id="example" class="table table-condensed table-bordered display">
				<thead>
					<tr class="bg-primary">
						<th>USER NAME</th>
						<th>FULL NAME</th>
						<th>ROLE NAME</th>
						<th>SOURCE FILE LOCATION</th>
						<th class="text-center">CREATED ON</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.username}</td>
						<td>${user.userFullName}</td>
						<td>${user.roleName}</td>
						<td>${user.localFileLocation}</td>
						<td class="text-center"><fmt:formatDate pattern="dd-MM-yyyy" value="${user.createdOn}" /></td>
						<td align="center">
							<a href="${pageContext.request.contextPath}/editUser?id=${user.id}" class="btn btn-xs btn-warning" role="button"><i class="fa fa-pencil"></i> EDIT</a>
				 		</td>
					</tr>
					</c:forEach>
					<c:if test="${empty userList}">
				      <tr><td colspan="6" class="text-warning">RECORD NOT FOUND</td></tr>
		     	   </c:if>
					</tbody>
					<c:if test="${not empty userList}">
			         <tfoot>
			        	<tr class="bg-primary">
					    <td>Total: ${userList.size()}</td>
					    <td></td>
						<td colspan="5"></td>
				        </tr>
					</tfoot>
				</c:if>
				</table>
		</div>
		<div class="col-sm-2"></div>
	</div>	
	</div>
	</div>
</div>
</section>
</body>
</html>