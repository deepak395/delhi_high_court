<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<script type="text/javascript">
function checkqcdone(e) {
    $.ajax({
    	type :'POST',
		url : '${pageContext.request.contextPath}/checkPendingCaseDocument',
		data : {
			documentId : e,
		},
		success : function(result) {
		 if(result=='true'){
			 alert("Already file downloded");
		 }else{
			 window.location.href = "${pageContext.request.contextPath}/downloadPendingCaseDocument?documentId="+e;
		 	}
		}
	});
}

</script>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item active"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">PENDING CASE QC</li>
	   	</ol>
		<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
		<div class="panel panel-warning panel-body">
		<form autocomplete="off" class="form-inline" action="${pageContext.request.contextPath}/getAllPendingCaseListForQc" method="post">
		<table class="table table-condensed borderless">
		<tbody> 
			<%-- <tr>
				<td>
				<form:checkbox path="caseType" id="casetype" value="DAK"/> If DAK</td>
				<td colspan="6"></td>
			</tr> --%>
			<tr>
				<td align="right" class="label-text required">
					<label for="caseNo">CASE NO</label>
				</td>
				<td>
					<input type="text" name="caseNo" class="form-control input-sm in-width" required="required"/>
				</td>
				<td align="right" class="label-text required">
					<label for="caseYear">CASE YEAR</label>
				</td>
				<td>
					<input type="text" name="caseYear" list="yearList" class="form-control input-sm input-width" maxlength="4" required="required"/>
					<datalist id="yearList">
						<c:forEach var="year" begin="1966" end="2019">
						<option value="${year}"></option>
						</c:forEach>
					</datalist>
				</td>
				<td align="right" class="label-text">
				<label for="caseCategory">CASE CATEGORY</label>
				</td>
				<td>
					<select name="caseCategory" class="form-control input-sm in-width">
					<option value="">--SELECT--</option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'CASE_CATEGORY'}">
						<option value="${lookup.lookupValue}">${lookup.lookupValue}</option>
						</c:if>
					</c:forEach>
					</select>
				</td>
				<td><button type="submit" class="form-control btn-sm btn-block btn-success in-width">SEARCH</button></td>
			</tr>
			</tbody>
			</table>
			</form>
			</div>
			<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
						<th>CASE NO</th>
						<th>CASE YEAR</th>
						<th>CASE CATEGORY</th>
						<th>CASE TYPE</th>
						<th>HEARING DATE</th>
						<th>TOTAL FILE</th>
						<th>REMARKS</th>
						<th>DEALING</th>
						<th>ENTRY DATE</th>
						<th>SCANNED ON</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty pendingCaseDetail}">
				<c:forEach var="pendingCase" items="${pendingCaseDetail}">
					<tr>
						<td>${pendingCase.caseNo}</td>
						<td>${pendingCase.caseYear}</td>
						<td>${pendingCase.caseCategory}</td>
						<td>${pendingCase.caseType}</td>
						<td>${pendingCase.hearingDate}</td>
						<td>${pendingCase.totalFile}</td>
						<td>${pendingCase.remarks}</td>
						<td>${pendingCase.dealing}</td>
						<td>${pendingCase.createdOn}</td>
						<td>${pendingCase.scannedOn}</td>
						 <form:form action="${pageContext.request.contextPath}/pendingCaseQcDone" modelAttribute="pendingCaseDocument" method="post" >
						<td width="70">
						<form:hidden path="documentId" value="${pendingCase.pendingCaseId}"/>
							<form:hidden path="caseNo" value="${pendingCase.caseNo}"/>
							<form:hidden path="caseYear" value="${pendingCase.caseYear}"/>
							<form:hidden path="caseCategory" value="${pendingCase.caseCategory}"/>
							<button type="submit" class="btn btn-xs btn-success">DONE</button>
							
						</td>
						<!-- <td width="70">
							<button type="submit" class="btn btn-xs btn-success">DONE</button>
						</td>  -->
						</form:form>
						<%-- <td>
							<form action="${pageContext.request.contextPath}/getAllPendingCaseDocumentListForQc" method="get">
								<input type="hidden" name="pendingCaseId" value="${pendingCaseDetail.pendingCaseId}">
								<button type="submit" class="btn btn-xs btn-success">FETCH</button>
							</form>
						</td> --%>
					</tr>
					</c:forEach>
					</c:if>
					<%-- <c:if test="${empty pendingCaseDetail}">
					<tr>
						<td colspan="11" class="text-danger">RECORD NOT FOUND</td>
					</tr>
					</c:if> --%>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="11"></td>
					</tr>
				</tfoot>
			</table>
			<c:if test="${not empty pendingCaseDocumentList}">
			<div class="row">
				<div class="col-sm-6">
					<table class="table table-condensed table-bordered">
					<thead>
					<tr class="bg-primary">
						<th>DOCUMENT NAME</th>
						<th class="text-center">ACTION</th>
					</tr>
					</thead>
					<tbody>
						<c:forEach var="pendingCaseDocument" items="${pendingCaseDocumentList}">
						<tr>
							<td>${pendingCaseDocument.documentName}</td>
							<td class="text-center">
								<button type="button" class="btn btn-xs btn-warning" onclick="checkqcdone(${pendingCaseDocument.documentId})">QC</button>
							</td>
						</tr>
						</c:forEach>
					</tbody>
					<tfoot>
					<tr class="bg-primary">
					<td colspan="8" class="text-danger"></td>
					</tr>
					</tfoot>
					</table>
				</div>
				<div class="col-sm-6">
				<table class="table table-condensed table-bordered">
					<tbody>
						<%-- <form:form action="${pageContext.request.contextPath}/pendingCaseQcDone" modelAttribute="pendingCaseDocument" method="post" enctype="multipart/form-data">
						<tr>
						    <td class="label-text text-right">SELECT FILE</td>
							<td>
								<form:hidden path="caseNo" value="${pendingCaseDetail.caseNo}"/>
								<form:hidden path="caseYear" value="${pendingCaseDetail.caseYear}"/>
								<form:hidden path="caseCategory" value="${pendingCaseDetail.caseCategory}"/>
								<form:hidden path="caseType" value="${pendingCaseDetail.caseType}"/>
							<!-- 	<form:input type="file" path="docFile" onchange="validateFileExtension(this);" multiple="multiple" required="required"/>  -->
							</td>
							</tr>
							<tr>
							<td align="center" colspan="2">
								<button type="submit" class="btn btn-sm btn-success">DONE</button>
							</td>
						</tr>
						</form:form> --%>
					</tbody>
				</table>
				</div>
			</div>
			</c:if>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>	
	</div>
	</div>
</section>
<c:if test="${not empty qcpendingCaseDocumentList or not empty invalidpendingCaseDocumentList}">
<script type="text/javascript">
   	$(function() {
	 	$('#qcdocmodal').modal('show');
	});
   </script>
	<div class="modal fade centered-modal" id="qcdocmodal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-body">
         <table class="table table-condensed table-bordered">
			<thead>
				<tr class="bg-primary">
					<th>DOCUMENT NAME</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${not empty qcpendingCaseDocumentList}">
				<tr class="bg-success"><td>QC DONE SUCCESSFULLY FILE</td></tr>
				 <c:forEach var="pendingCaseDocument" items="${qcpendingCaseDocumentList}">
				<tr>
					<td>${pendingCaseDocument.documentName}</td>
				</tr>
				</c:forEach> 
				</c:if>
				<c:if test="${not empty invalidpendingCaseDocumentList}">
				<tr class="bg-danger"><td>INVALID FILE</td></tr>
				 <c:forEach var="invalidpendingCaseDocument" items="${invalidpendingCaseDocumentList}">
				<tr>
					<td>${invalidpendingCaseDocument.documentName}</td>
				</tr>
				</c:forEach> 
				</c:if>
			</tbody>
			<tfoot>
				<tr>
				<td class="text-center"> <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">OK</button></td>
				</tr>
			</tfoot>
	    </table>
        </div>
      </div>
    </div>
  </div>
</c:if>
<c:if test="${not empty scanningmsg}">
				<script type="text/javascript">
			    	$(function() {
					 	$('#scanningdocmodal').modal('show');
					});
			    </script>
				<div class="modal fade centered-modal" id="scanningdocmodal" role="dialog">
				    <div class="modal-dialog modal-md">
				      <div class="modal-content">
				        <div class="modal-body">
				        <p>${scanningmsg}</p>
				         <table class="table table-condensed table-bordered">
						<thead>
							<!-- <tr class="bg-primary">
								<th>DOCUMENT NAME</th>
							</tr> -->
						</thead>
						<%-- <tbody>
							 <c:forEach var="pendingCaseDocument" items="${pendingCaseDocumentList}">
							<tr>
								<td>${pendingCaseDocument.documentName}</td>
							</tr>
							</c:forEach> 
						</tbody> --%>
						<tfoot>
							<tr>
							<td class="text-center"> <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">OK</button></td>
							</tr>
						</tfoot>
					    </table>
				        
				        </div>
				      </div>
				    </div>
				  </div>
					
				</c:if>
<footer>
</footer>
</body>
</html>