<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
 <%-- <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/footer.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/> --%>
 <script src="${pageContext.request.contextPath}/resources/js/angularJs/angular.min.js"></script>
 <%-- <script src="${pageContext.request.contextPath}/resources/js/angularJs/angular.min.js"></script> --%>
 <script src="${pageContext.request.contextPath}/resources/js/angularJs/angular-messages.js"></script> 
 <%-- <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>  --%>
 <script src="${pageContext.request.contextPath}/resources/js/controller/savePendingCaseController.js"></script>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
.blue-input:focus {
  background: green;
  color: white;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>


<section class="cantent"  ng-app="dhcApp">
<div class="container-fluid" ng-controller="savePendingCaseCtrl">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">PENDING CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
		<form autocomplete="off" name="pendingForm" >
		
		<table class="table table-condensed borderless">
		<tbody>
			<tr>
				<td align="right" class="label-text required">
					<%-- <form:label path="caseNo">CASE NO</form:label> --%>
					<label for="caseno">CASE NO</label>
				</td>
				<td>
					<input type="text" name="caseno" ng-model ="pendingCase.caseNo" id="caseno"  class="form-control input-sm input-width" tabindex="1" autofocus required>
					<div ng-messages="pendingForm.caseno.$error">
  <div ng-message="required">This field is required</div>
</div>
					
					<span id="caseno-msg" class="text-danger"></span>
				</td>
				<td></td>
				<td align="right" class="label-text">
					
						<label for="courtno">COURT NO</label>
				</td>
				<td>
				    <input type="text" name="courtno" id="courtno" ng-model ="pendingCase.courtNo" class="form-control input-sm input-width" tabindex="6">
				    
					
					<span id="caseno-msg" class="text-danger"></span>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				
					<label for="caseyear">CASE YEAR</label>
				</td>
				<td>
				 <input type="text" name="caseYear" id="caseYear"  ng-blur ="yearValFocusOut(pendingCase.caseYear)"  ng-focus="yearVal(pendingCase.caseYear)"  ng-model="pendingCase.caseYear" class="form-control input-sm input-width" tabindex="2" maxlength="4" required>
				<!--  <select required style="width: 60%;" 
													class="form-control" ng-model="pendingCase.caseYear"
													name="caseYear" id="caseYear"
													ng-options="year as year for year in years">
												</select> 
												<span style="color: red" ng-show="fd_case_year.$dirty && fd_case_year.$invalid && fd_case_year.$error "></span>	 -->
				 <div ng-messages="pendingForm.caseYear.$error">
 			 <div ng-message="required">This field is required</div>
				</div>
				<span id="yearvalmsg" class="text-danger"></span>
					
				</td>
				<td></td>
				<td align="right" class="label-text">
					<label for="totalFile">TOTAL FILE</label>
				</td>
				<td>
				    
					 <input type="text" name="totalfile" id="totalfile" ng-model ="pendingCase.totalFile" class="form-control input-sm input-width" tabindex="7" >
					<span id="totalfile-msg" class="text-danger"></span>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<label for="caseCategory">CASE CATEGORY</label>
				</td>
				<td>
					<select class="form-control input-sm input-width"  name ="caseCat" data-ng-options="o for o in caseCategoryOptions" data-ng-model="pendingCase.caseCategory" id ="caseCategory" tabindex="3" required>
					<option value="" >Select Case Category</option></select>
					 <div ng-messages="pendingForm.caseCat.$error">
 				 <div ng-message="required">This field is required</div>
					</div>
					
				</td>
				<td></td>
				<td align="right" class="label-text required">
						<label for="caseType">CASE TYPE</label>
				</td>
				<td>
					<select class="form-control input-sm input-width" name ="caseType" data-ng-options="o for o in caseTypeoptions" data-ng-model="pendingCase.caseType" id ="caseType" tabindex="8" required>
					<option value="" >Select Case Type</option></select>
						 <div ng-messages="pendingForm.caseType.$error">
					  <div ng-message="required">This field is required</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					
					<label for="hearing-date">HEARING DATE</label>
				</td>
				<td> 
				    <input  type="text" id="hearing-date" maxlength="10" ng-blur="validatedate(pendingCase.hearingDate)" ng-focus="clearvalidatedate(pendingCase.hearingDate)" ng-model ="pendingCase.hearingDate" ng-keypress="addHyphen(this)" class="form-control input-sm input-width" tabindex="4"/>
				    
					
					<span id="hearing-date-msg" class="text-danger"></span>
				</td>
				<td class="label-text">
				<input type ="checkbox"  value="REGULAR" id="regular" name ="regular" ng-click="inputDisable(this);" /> IF REGULAR</td>
				<td align="right" class="label-text">
					<label for="remarks">REMARKS</label>
				</td>
				<td>
				  
				    <input type="text" name="remarks" ng-model ="pendingCase.remarks" id="remarks"  class="form-control input-sm input-width" tabindex="10">
				  
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text">
					
					<label for="dealing">DEALING</label>
				</td>
				<td>
				    
				     <input type="text" name="dealing" ng-model ="pendingCase.dealing" id="dealing"  class="form-control input-sm input-width" tabindex="5">
					
				</td>
				<td></td>
				<td align="right" class="label-text">
					
					<label for="createdBy">ENTERED BY</label>
				</td>
				<td>
				  <%--   <form:input path="createdBy" class="form-control input-sm input-width" value="${user.userFullName}" readonly="true"/> --%>
				       <input type="text" name="createdBy"  id="createdBy" value="${user.userFullName}" class="form-control input-sm input-width" readonly="true" >
				   
				</td>
			</tr>
			<tr>
				<td colspan="1"></td>
				<td>
<!-- 				<button class="btn btn-sm btn-primary btn-block form-btn input-width blue-input" type="submit" tabindex="11" ng-change="validatedate(this);">SUBMIT</button>
 -->								<button class="btn btn-sm btn-primary btn-block form-btn input-width blue-input" id ="subButton" ng-blur ="returnToTab1()" type="submit" tabindex="11" ng-click="savePendingCase(pendingCase)">SUBMIT</button>
				
								
								<!-- <button type="button" class="btn btn-info" ng-click="savePendingCase(pendingCase)">Button</button> -->
				
				</td>
				<td><a class="btn btn-sm btn-danger btn-block form-btn input-width" role="button" href="${pageContext.request.contextPath}/pendingCase">RESET</a></td>
				<td><a class="btn btn-sm btn-info btn-block form-btn input-width" role="button" href="${pageContext.request.contextPath}/getAllPendingCaseList"><i class="fa fa-hand-o-right" aria-hidden="true"></i>PENDING LIST</a></td>	
			</tr>
		</tbody>
	</table>
	
	</form>
	</div>
	
	</div>
	<div class="col-sm-2"></div>
	</div>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<table  class="table table-condensed table-striped table-bordered" style="width:100%;">
			<thead>
				<tr class="bg-primary">
					<th>CASE NO</th>
					<th>CASE YEAR</th>
					<th>CASE CATEGORY</th>
					<th>CASE TYPE</th>
					<th>HEARING DATE</th>
					<th>TOTAL FILE</th>
					<th>DATA ENTRY DATE</th>
					<th>DATA ENTRY BY</th>
					<th>COURT NO</th>
					<th>DEALING</th>
					<th>REMARKS</th>
				</tr>
			</thead>
			<tbody>
				
				<tr ng-repeat="data in pendingCaseList " class="odd gradeX"> 
					<td>{{data.caseNo}}</td>
					<td>{{data.caseYear}}</td>
					<td>{{data.caseCategory}}</td>
					<td>{{data.caseType}}</td>
					<td>{{data.hearingDate}}</td>
					<td>{{data.totalFile}}</td>
					<td>{{data.createdOn}}</td>
					<td>{{data.createdBy}}</td>
					<td>{{data.courtNo}}</td>
					<td>{{data.dealing}}</td>
					<td>{{data.remarks}}</td>
				</tr>
				
			</tbody>
			<tfoot>
			
	      
			</tfoot>
		</table>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
</div>
</div>
</section>

</body>
</html>