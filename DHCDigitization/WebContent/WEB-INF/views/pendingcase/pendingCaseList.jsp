<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">PENDING CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="panel panel-warning panel-body">
		
		<form autocomplete="off" action="${pageContext.request.contextPath}/searchPendingCase" method="post">
		<table class="table table-condensed borderless">
		<tbody> 
			<tr>
				<td align="right" class="label-text">
					<label for="caseNo">CASE NO</label>
				</td>
				<td>
					<input type="text" name="caseNo" class="form-control input-sm in-width" required="required"/>
				</td>
				<td align="right" class="label-text">
					<label for="caseYear">CASE YEAR</label>
				</td>
				<td>
					<input type="text" name="caseYear" list="yearList" class="form-control input-sm input-width" maxlength="4" required="required"/>
					<datalist id="yearList">
						<c:forEach var="year" begin="1966" end="2022">
						<option value="${year}"></option>
						</c:forEach>
					</datalist>
				</td>
				<td align="right" class="label-text">
				<label for="caseCategory">CASE CATEGORY</label>
				</td>
				<td>
					<select name="caseCategory" class="form-control input-sm in-width">
					<option value="">--SELECT--</option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'CASE_CATEGORY'}">
						<option value="${lookup.lookupValue}">${lookup.lookupValue}</option>
						</c:if>
					</c:forEach>
					</select>
				</td>
				<td><button type="submit" class="form-control btn-sm btn-block btn-success in-width">SEARCH</button></td>
				<tr>
			<td></td>
			<td><a class="btn btn-sm btn-danger btn-block form-btn input-width" role="button" href="${pageContext.request.contextPath}/pendingCase"><i class="fa fa-hand-o-right fa-lg" aria-hidden="true">BACK TO DATA ENTRY</a></td>
			</tr>
			</tbody>
			</table>
			</form>
			</div>
			<table class="table table-condensed table-bordered table-striped">
				<thead>
					<tr class="bg-primary">
						<th>CASE NO</th>
						<th>CASE YEAR</th>
						<th>CASE CATEGORY</th>
						<th>CASE TYPE</th>
						<th>HEARING DATE</th>
						<th>TOTAL FILE</th>
						<th>DATA ENTRY DATE</th>
						<th>DATA ENTRY BY</th>
						<th>COURT NO</th>
						<th>DEALING</th>
						<th>REMARKS</th>
						<th class="text-center">EDIT</th>
						<th class="text-center">DELETE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="pendingCase" items="${pendingCaseList}">
					<tr> 
						<td>${pendingCase.caseNo}</td>
						<td>${pendingCase.caseYear}</td>
						<td>${pendingCase.caseCategory}</td>
						<td>${pendingCase.caseType}</td>
						<td>${pendingCase.hearingDate}</td>
						<td>${pendingCase.totalFile}</td>
						<td>${pendingCase.createdOn}</td>
						<td>${pendingCase.createdBy}</td>
						<td>${pendingCase.courtNo}</td>
						<td>${pendingCase.dealing}</td>
						<td>${pendingCase.remarks}</td>
						<td align="center">
							<a href="${pageContext.request.contextPath}/editPendingCase?pendingCaseId=${pendingCase.pendingCaseId}" class="btn btn-xs btn-warning" role="button"><i class="fa fa-pencil"></i> EDIT</a>
				 		</td>
				 		<td align="center">
							<a href="${pageContext.request.contextPath}/deletePendingCase?pendingCaseId=${pendingCase.pendingCaseId}" class="btn btn-xs btn-danger" role="button"><i class="fa fa-pencil"></i>DELETE</a>
							
				 		</td>
					</tr>
					</c:forEach>
				</tbody>
				<tfoot>
				<tr class="bg-primary">
					<td>Total: ${pendingCaseList.size()}</td>
					<td></td>
					<td colspan="10"></td>
		       	</tr>
				</tfoot>
			</table>
			<c:if test="${not empty pendingCaseList && list.size()>10}">
			<c:set var="pageNum"  value="${page}"></c:set>
			<ul class="pager">
			    <li class="previous"><a href="${pageContext.request.contextPath}/getAllPendingCaseList?pageNo=${pageNum-1}">Previous</a></li>
			    <li class="next"><a href="${pageContext.request.contextPath}/getAllPendingCaseList?pageNo=${pageNum+1}">Next</a></li>
		    </ul>
		    </c:if>
		</div>
		<div class="col-sm-1"></div>
	</div>	
	</div>
	</div>
</div>

</section>
</body>
</html>