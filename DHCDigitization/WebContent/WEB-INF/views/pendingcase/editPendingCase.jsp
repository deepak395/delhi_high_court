<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
.blue-input:focus {
  background: green;
  color: white;
}
</style>
<script type="text/javascript">
function addHyphen(element) {
	let ele = document.getElementById(element.id);
    ele = ele.value.split('-').join('');   
    if(ele.length<=5){
    	let finalVal = ele.match(/.{1,2}/g).join('-');
    	document.getElementById(element.id).value = finalVal;
    }
}
function inputDisable(e) {
	 if(document.getElementById(e.id).checked){
	  	document.getElementById("hearing-date").disabled = true;
	 }else{
		 document.getElementById("hearing-date").disabled = false;
	 } 
}
function validatedate(inputText){
	var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	if(inputText.value.match(dateformat)){
	document.form1.text1.focus();
	var opera2 = inputText.value.split('-');
	lopera1 = opera1.length;
	lopera2 = opera2.length;
	if (lopera1>1){
		var pdate = inputText.value.split('-');
	}
	var dd = parseInt(pdate[0]);
	var mm  = parseInt(pdate[1]);
	var yy = parseInt(pdate[2]);
	if (mm==1 || mm>2)
	if (mm==2){
		var lyear = false;
		if ( (!(yy % 4) && yy % 100) || !(yy % 400)) {
		lyear = true;
		}
		if ((lyear==false) && (dd>=29)){
			document.getElementById("hearing-date-msg").innerHTML = "Invalid date format!";
			return false;
		}
		if ((lyear==true) && (dd>29)){
			document.getElementById("hearing-date-msg").innerHTML = "Invalid date format!";
			return false;
		}
	} 
	}else{
		document.getElementById("hearing-date-msg").innerHTML = "Invalid date format!";
		document.form1.text1.focus();
		return false;
	}
}
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">PENDING CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
		<form:form action="${pageContext.request.contextPath}/updatePendingCase" method="post" modelAttribute="pendingCase" autocomplete="off">
		<form:hidden path="pendingCaseId"/>
		<table class="table table-condensed borderless">
		<tbody>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="caseNo">CASE NO</form:label>
				</td>
				<td>
					<%-- <form:input path="caseNo" id="caseno" onchange="checkAllNumeric(this)" class="form-control input-sm input-width" tabindex="1"/> --%>
					<form:input path="caseNo" id="caseno"  class="form-control input-sm input-width" tabindex="1"/>
					<form:errors path="caseNo" class="text-danger"></form:errors>
					<span id="caseno-msg" class="text-danger"></span>
				</td>
				<td></td>
				<td align="right" class="label-text">
					<form:label path="courtNo">COURT NO</form:label>
				</td>
				<td>
				    <form:input path="courtNo" class="form-control input-sm input-width" tabindex="6"/>
					<form:errors path="courtNo" class="text-danger"></form:errors>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="caseYear">CASE YEAR</form:label>
				</td>
				<td>
					<form:input path="caseYear" id="caseyear" list="yearList" onchange="checkAllNumeric(this)" class="form-control input-sm input-width" maxlength="4" tabindex="2"/>
					<datalist id="yearList">
						<c:forEach var="year" begin="1966" end="2022">
						<option value="${year}"></option>
						</c:forEach>
					</datalist>
					<form:errors path="caseYear" class="text-danger"></form:errors>
					<span id="caseyear-msg" class="text-danger"></span>
				</td>
				<td></td>
				<td align="right" class="label-text">
					<form:label path="totalFile">TOTAL FILE</form:label>
				</td>
				<td>
				    <form:input path="totalFile" id="totalfile" class="form-control input-sm input-width" tabindex="7"/>
					<form:errors path="totalFile" class="text-danger"></form:errors>
					<span id="totalfile-msg" class="text-danger"></span>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
				<form:label path="caseType">CASE CATEGORY</form:label>
				</td>
				<td>
					<form:select path="caseCategory" class="form-control input-sm input-width" tabindex="3" >
					<form:option value="">--SELECT--</form:option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'CASE_CATEGORY'}">
						<form:option value="${lookup.lookupValue}">${lookup.lookupValue}</form:option>
						</c:if>
					</c:forEach>
					</form:select>
					<form:errors path="caseCategory" class="text-danger"></form:errors>
				</td>
				<td></td>
				<td align="right" class="label-text required">
					<form:label path="caseType">CASE TYPE</form:label>
				</td>
				<td>
					<form:select path="caseType" class="form-control input-sm input-width" tabindex="8" >
					<form:option value="">--SELECT--</form:option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'PENDING_CASE_TYPE'}">
						<form:option value="${lookup.lookupValue}">${lookup.lookupValue}</form:option>
						</c:if>
					</c:forEach>
					</form:select>
					<form:errors path="caseType" class="text-danger"></form:errors>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text required">
					<form:label path="hearingDate">HEARING DATE</form:label>
				</td>
				<td> 
				    <form:input path="hearingDate" type="text" id="hearing-date" maxlength="10" onchange="validatedate(this);" onkeypress="addHyphen(this)" class="form-control input-sm input-width" tabindex="4"/>
					<form:errors path="hearingDate" class="text-danger"></form:errors>
					<span id="hearing-date-msg" class="text-danger"></span>
				</td>
				<td class="label-text"><form:checkbox path="ifRegulor" value="REGULAR" id="regulor" onclick="inputDisable(this);"/> IF REGULAR</td>
				<td align="right" class="label-text">
					<form:label path="remarks">REMARKS</form:label>
				</td>
				<td>
				    <form:input path="remarks" class="form-control input-sm input-width" tabindex="9"/>
				    <form:errors path="remarks" class="text-danger"></form:errors>
				</td>
			</tr>
			<tr>
				<td align="right" class="label-text">
					<form:label path="dealing">DEALING</form:label>
				</td>
				<td>
				    <form:input path="dealing" class="form-control input-sm input-width" tabindex="5"/>
					<form:errors path="dealing" class="text-danger"></form:errors>
				</td>
				<td></td>
				<td align="right" class="label-text">
					<form:label path="createdBy">ENTERED BY</form:label>
				</td>
				<td>
				    <form:input path="updatedBy" class="form-control input-sm input-width" value="${user.username}" readonly="true"/>
				    <form:errors path="createdBy" class="text-danger"></form:errors>
				</td>
			</tr>
			<tr>
				<td colspan="1"></td>
				<td align="center">
				<button class="btn btn-sm btn-primary btn-block form-btn blue-input" type="submit" tabindex="10">UPDATE</button>
				</td>
			     <td><a class="btn btn-sm btn-info btn-block form-btn input-width" role="button" href="${pageContext.request.contextPath}/getAllPendingCaseList"><i class="fa fa-hand-o-right" aria-hidden="true"></i>PENDING LIST</a></td>
			</tr>
		</tbody>
	</table>
	</form:form>
	</div>
	<h3 class="text-center text-success">${pendingcasemsg}</h3>
	</div>
	<div class="col-sm-2"></div>
	</div>
</div>
</div>
</div>
</section>
</body>
</html>