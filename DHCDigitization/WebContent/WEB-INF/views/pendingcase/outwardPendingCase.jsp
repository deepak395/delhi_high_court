<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
 <%-- <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/footer.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/> --%>
 <script src="${pageContext.request.contextPath}/resources/js/angularJs/angular.min.js"></script>
 <%-- <script src="${pageContext.request.contextPath}/resources/js/angularJs/angular.min.js"></script> --%>
 <script src="${pageContext.request.contextPath}/resources/js/angularJs/angular-messages.js"></script> 
 <%-- <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>  --%>
 <script src="${pageContext.request.contextPath}/resources/js/controller/outwardPendingCaseController.js"></script>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
.blue-input:focus {
  background: green;
  color: white;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>


<section class="cantent"  ng-app="dhcApp">
<div class="container-fluid" ng-controller="outwardPendingCaseCtrl">

<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">OUTWARD CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning" style="padding-bottom: 0px;">
		<form autocomplete="off" name="pendingForm" >
		
		<table class="table table-condensed borderless">
		<tbody>
			<tr>
				<td align="right" class="label-text required">
					<%-- <form:label path="caseNo">CASE NO</form:label> --%>
					<label for="caseno">CASE NO</label>
				</td>
				<td>
					<input type="text" name="caseno" ng-model ="outwardCase.caseNo" id="caseno"  class="form-control input-sm input-width" tabindex="1" autofocus required>
					<div>
                  </div>
					
					<span id="caseno-msg" class="text-danger"></span>
				</td>
				<td></td>
				<td align="right" class="label-text required">
				
					<label for="caseyear">CASE YEAR</label>
				</td>
				<td>
				 <input type="text" name="caseYear" id="caseYear"  ng-blur ="yearValFocusOut(outwardCase.caseYear)"  ng-focus="yearVal(outwardCase.caseYear)"  ng-model="outwardCase.caseYear" class="form-control input-sm input-width" tabindex="2" maxlength="4" required>
				<!--  <select required style="width: 60%;" 
													class="form-control" ng-model="pendingCase.caseYear"
													name="caseYear" id="caseYear"
													ng-options="year as year for year in years">
												</select> 
												<span style="color: red" ng-show="fd_case_year.$dirty && fd_case_year.$invalid && fd_case_year.$error "></span>	 -->
				 <div>
				</div>
				<span id="yearvalmsg" class="text-danger"></span>
					
				</td>
				<td align="right" class="label-text required">
				<label for="caseCategory">CASE CATEGORY</label>
				</td>
				<td>
					<select class="form-control input-sm input-width"  name ="caseCat" data-ng-options="o for o in caseCategoryOptions" data-ng-model="outwardCase.caseCategory" id ="caseCategory" tabindex="3" required>
					<option value="" >Select Case Category</option></select>
					 <div>
					</div>
					
				</td>
				<td></td>
				<td align="right" class="label-text required">
					
					<label for="outward-date">OUTWARD DATE</label>
				</td>
				<td> 
				    <input  type="text" id="outward-date" maxlength="10" ng-blur="validatedate(outwardCase.outward_date)" ng-model ="outwardCase.outward_date" ng-keypress="addHyphen(this)" class="form-control input-sm input-width" tabindex="4"/>
					<span id="outward-date-msg" class="text-danger"></span>
				</td>
			<tr>
				<td colspan="1"></td>
				<td>
			 <button class="btn btn-sm btn-primary btn-block form-btn input-width blue-input" id ="subButton" ng-blur ="returnToTab1()" type="submit" tabindex="11" ng-click="saveOutwardCase(outwardCase)">SUBMIT</button>
				</td>
				
			</tr>
		</tbody>
	</table>
	
	</form>
	</div>
	
	</div>
	</div>
</div>
</div>
</div>
</section>

</body>
</html>