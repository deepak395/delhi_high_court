<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">PENDING CASE SCANNING</li>
   		</ol>
		<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
		<div class="panel panel-warning panel-body">
		<label for="myCheck">DAK</label>
			<input type="checkbox" id="myCheck" onclick="dakCheckBox()">
		<form autocomplete="off" class="form-inline" action="${pageContext.request.contextPath}/getAllPendingCaseListForScanning" method="post">
		<table class="table table-condensed borderless">
		<tbody> 
			<%-- <tr>
				<td>
				<form:checkbox path="caseType" id="casetype" value="DAK"/> If DAK</td>
				<td colspan="6"></td>
			</tr> --%>
			<tr>
				<td align="right" class="label-text required">
					<label for="caseNo">CASE NO</label>
				</td>
				<td>
					<input type="text" name="caseNo" id ="caseNo" class="form-control input-sm in-width" required="required"/>
				</td>
				<td align="right" class="label-text required">
					<label for="caseYear">CASE YEAR</label>
				</td>
				<td>
					<input type="text" name="caseYear" list="yearList" id = "caseYear" class="form-control input-sm input-width" maxlength="4" required="required"/>
					<datalist id="yearList">
						<c:forEach var="year" begin="1966" end="2019">
						<option value="${year}"></option>
						</c:forEach>
					</datalist>
				</td>
				<td align="right" class="label-text">
				<label for="caseCategory">CASE CATEGORY</label>
				</td>
				<td>
					<select name="caseCategory" id ="caseCat" class="form-control input-sm in-width">
					<option value="">--SELECT--</option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'CASE_CATEGORY'}">
						<option value="${lookup.lookupValue}">${lookup.lookupValue}</option>
						</c:if>
					</c:forEach>
					</select>
				</td>
				<td><button type="submit" id ="searchCase" class="form-control btn-sm btn-block btn-success in-width">SEARCH</button>
				<button type="button" id ="saveDak" class="form-control btn-sm btn-block btn-success in-width" style="display: none" onclick="persistDak()">OK</button></td>
			</tr>
			</tbody>
			</table>
			</form>
			</div>
			<div class="row">
				<div class="col-sm-12">
				<div class="panel panel-warning panel-body">
				<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
						<th>CASE NO</th>
						<th>CASE YEAR</th>
						<th>CASE CATEGORY</th>
						<th>CASE TYPE</th>
						<th>HEARING DATE</th>
						<th>REMARKS</th>
						<th>TOTAL FILE</th>
						<th>ENTRY DATE</th>
						<th>DEALING</th>
						<!-- <th>SELECT FILE</th> -->
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty pendingCaseDetail}">
					<c:forEach var="pendingCase" items="${pendingCaseDetail}">
					<tr>
						<td>${pendingCase.caseNo}</td>
						<td>${pendingCase.caseYear}</td>
						<td>${pendingCase.caseCategory}</td>
						<td>${pendingCase.caseType}</td>
						<td>${pendingCase.hearingDate}</td>
						<td>${pendingCase.remarks}</td>
						<td>${pendingCase.totalFile}</td>
						<td>${pendingCase.createdOn}</td>
						<td>${pendingCase.dealing}</td>	

						<td>
						<c:choose>
						<c:when test="${pendingCase.acceptedByScanner}">
						
						 <form:form action="${pageContext.request.contextPath}/pendingCaseScanningDoneNew" modelAttribute="pendingCaseDocument" method="post" enctype="multipart/form-data">
					
							<form:hidden path="pendingCaseId" value="${pendingCase.pendingCaseId}"/>
							<form:hidden path="caseNo" value="${pendingCase.caseNo}"/>
							<form:hidden path="caseYear" value="${pendingCase.caseYear}"/>
							<form:hidden path="caseCategory" value="${pendingCase.caseCategory}"/>
							<form:hidden path="caseStage" value="${pendingCase.caseStage}"/>
							<form:hidden path="caseType" value="${pendingCase.caseType}"/>
							<!-- <form:input type="file" path="docFile" onchange="validateFileExtension(this);" multiple="multiple" required="required"/> -->
						
						
						<div width="70">
						
							<button type="submit" class="btn btn-xs btn-success">DONE</button>
						</div>
							</form:form>
						 </c:when>  
						  <c:otherwise>
						  <form:form action="${pageContext.request.contextPath}/pendingCaseCreateFolder" modelAttribute="pendingCaseDocument" method="post" enctype="multipart/form-data">
						
						<form:hidden path="pendingCaseId" value="${pendingCase.pendingCaseId}"/>
							<form:hidden path="caseNo" value="${pendingCase.caseNo}"/>
							<form:hidden path="caseYear" value="${pendingCase.caseYear}"/>
							<form:hidden path="caseCategory" value="${pendingCase.caseCategory}"/>
							<form:hidden path="caseStage" value="${pendingCase.caseStage}"/>
							<form:hidden path="caseType" value="${pendingCase.caseType}"/>
							
							<!-- <form:input type="file" path="docFile" onchange="validateFileExtension(this);" multiple="multiple" required="required"/> -->
						
						<%-- <c:if test="${pendingCase.acceptedByScanner}"> --%>
						<div width="70">
							<button type="submit" class="btn btn-xs btn-success">ACCEPT CASE</button>
						</div>
						
						<%-- </c:if> --%>
						  </form:form>
						   </c:otherwise>
						   </c:choose>
						</td>
						
						
						
						
						
						
						 
					
						<%-- <form:form action="${pageContext.request.contextPath}/pendingCaseCreateFolder" modelAttribute="pendingCaseDocument" method="post" enctype="multipart/form-data">
						<td>
						<form:hidden path="pendingCaseId" value="${pendingCase.pendingCaseId}"/>
							<form:hidden path="caseNo" value="${pendingCase.caseNo}"/>
							<form:hidden path="caseYear" value="${pendingCase.caseYear}"/>
							<form:hidden path="caseCategory" value="${pendingCase.caseCategory}"/>
							<form:hidden path="caseStage" value="${pendingCase.caseStage}"/>
							<form:hidden path="caseType" value="${pendingCase.caseType}"/>
							
							<!-- <form:input type="file" path="docFile" onchange="validateFileExtension(this);" multiple="multiple" required="required"/> -->
						</td>
						<c:if test="${pendingCase.acceptedByScanner}">
						<td width="70">
							<button type="submit" class="btn btn-xs btn-success">ACCEPT CASE</button>
						</td>
						
						</c:if>
						 
						</form:form> --%>
						<!-- <td width="70"><button type="submit" class="btn btn-xs btn-success">DONE</button></td> -->
					</tr>
					</c:forEach>
					</c:if>
					<%-- <c:if test="${empty pendingCaseDetail}">
					<tr>
						<td colspan="10" class="text-danger">RECORD NOT FOUND</td>
					</tr>
					</c:if> --%>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="11"></td>
					</tr>
				</tfoot>
				</table>
				</div>
				<c:if test="${not empty scanningmsg}">
				<script type="text/javascript">
			    	$(function() {
					 	$('#scanningdocmodal').modal('show');
					});
			    </script>
				<div class="modal fade centered-modal" id="scanningdocmodal" role="dialog">
				    <div class="modal-dialog modal-md">
				      <div class="modal-content">
				        <div class="modal-body">
				        <p>${scanningmsg}</p>
				         <table class="table table-condensed table-bordered">
						<thead>
							<!-- <tr class="bg-primary">
								<th>DOCUMENT NAME</th>
							</tr> -->
						</thead>
						<%-- <tbody>
							 <c:forEach var="pendingCaseDocument" items="${pendingCaseDocumentList}">
							<tr>
								<td>${pendingCaseDocument.documentName}</td>
							</tr>
							</c:forEach> 
						</tbody> --%>
						<tfoot>
							<tr>
							<td class="text-center"> <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">OK</button></td>
							</tr>
						</tfoot>
					    </table>
				        
				        </div>
				      </div>
				    </div>
				  </div>
					
				</c:if>
				</div>
			</div>
		</div>
		
		<div class="modal fade centered-modal" id="dakmodal" role="dialog">
				    <div class="modal-dialog modal-md">
				      <div class="modal-content">
				        <div class="modal-body">
				        <p>PLEASE FILL ALL THE FIELDS</p>
				         <table class="table table-condensed table-bordered">
						<thead>
							<!-- <tr class="bg-primary">
								<th>DOCUMENT NAME</th>
							</tr> -->
						</thead>
						<%-- <tbody>
							 <c:forEach var="pendingCaseDocument" items="${pendingCaseDocumentList}">
							<tr>
								<td>${pendingCaseDocument.documentName}</td>
							</tr>
							</c:forEach> 
						</tbody> --%>
						<tfoot>
							<tr>
							<td class="text-center"> <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">OK</button></td>
							</tr>
						</tfoot>
					    </table>
				        
				        </div>
				      </div>
				    </div>
				  </div>
		
		
		<div class="col-sm-1"></div>
	</div>	
	</div>
	</div>
</div> 
<script type="text/javascript">
function dakCheckBox() {
	console.log("invokeddddddddddddddddddddddddddddd");
	
	/* document.getElementById('searchCase').style.display = 'none';
	document.getElementById('saveDak').style.display = 'block'; */
	
   // document.getElementById('ifNo').style.display = 'none';
    
    
	   // Get the checkbox
	   var checkBox = document.getElementById("myCheck");
	  // Get the output text
	  var text = document.getElementById("text");

	  // If the checkbox is checked, display the output text
	  if (checkBox.checked == true){
		  document.getElementById('searchCase').style.display = 'none';
			document.getElementById('saveDak').style.display = 'block';
	  } else {
		  document.getElementById('searchCase').style.display = 'block';
			document.getElementById('saveDak').style.display = 'none';
	  } 
	}
	
function persistDak()	{
	
	console.log("function calledddddddddddddd");
	  var caseCategory =document.getElementById('caseCat');
	  var caseNo =document.getElementById('caseNo').value;
	  var caseYear =document.getElementById('caseYear').value;
	  console.log("function calledddddddddddddd",caseYear);
	  var b =caseCategory.options[caseCategory.selectedIndex].value;
	  console.log("function calledddddddddddddd",b);
	  var pendingCase1 ={};
	  pendingCase1.caseYear =caseYear;
	  pendingCase1.caseCategory =b;
	  pendingCase1.caseNo =caseNo;
	  console.log("function calledddddddddddddd",pendingCase1);
	  
if(true){
$.ajax({
	type :'post',
	url : '${pageContext.request.contextPath}/saveDak',
	 data : {caseYear:caseYear,caseNo:caseNo,caseCategory:b },
	success : function(result) {
		console.log("dataaaaaaaaaaaaaaaa",result);
		if(result =="FILL ALL FIELDS"){
			$('#dakmodal').modal('show');
			//window.location.href = "${pageContext.request.contextPath}/getPendingCase?pendingCaseId="+result;
		}
		else {
			window.location.href = "${pageContext.request.contextPath}/getPendingCase?pendingCaseId="+result;
		}
	}
});
}
}

function yearValidation(year,ev) {

	  var text = /^[0-9]+$/;
	  if(ev.type=="blur" || year.length==4 && ev.keyCode!=8 && ev.keyCode!=46) {
	    if (year != 0) {
	        if ((year != "") && (!text.test(year))) {

	            alert("Please Enter Numeric Values Only");
	            return false;
	        }

	        if (year.length != 4) {
	            alert("Year is not proper. Please check");
	            return false;
	        }
	        var current_year=new Date().getFullYear();
	        if((year < 1920) || (year > current_year))
	            {
	            alert("Year should be in range 1920 to current year");
	            return false;
	            }
	        return true;
	    } }
	}
	
	

			    </script>
</section>
<footer>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>