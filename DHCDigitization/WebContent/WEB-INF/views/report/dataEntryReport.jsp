<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.col-md-3 .card-counter a{
	display: block;
	text-decoration: none;
}

</style>
<script type="text/javascript">
$(function() {
	$('.report').datepicker({dateFormat: 'dd-mm-yy', constrainInput: false});
	//$( ".report" ).datepicker({ constrainInput: false });
});

function inputDisable(e) {
	 if(document.getElementById(e.id).checked){
	  	document.getElementById("scanning").disabled = true;
		document.getElementById("qc").disabled = true;
		document.getElementById("bookmarking").disabled = true;
	 }else{
		document.getElementById("scanning").disabled = false;
		document.getElementById("qc").disabled = false;
		document.getElementById("bookmarking").disabled = false;
	 } 
	}
function removeDisable(e) {
	document.getElementById("scanning").disabled = false;
	document.getElementById("qc").disabled = false;
	document.getElementById("bookmarking").disabled = false;
}

/* function compare(){
    var startDt = document.getElementById("from-date").value;
    var endDt = document.getElementById("to-date").value;
    if((new Date(startDt) <= new Date(endDt))){
    	
    }else{
    	 var endDt = document.getElementById().value=""
    	alert("not valid date");
    }
} */
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">DATA ENTRY REPORT</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning clearfix">
		<h5><b>REPORT</b></h5>
		<form autocomplete="off" action="${pageContext.request.contextPath}/searchDataEntryReport" method="get">
			<table class="table table-condensed borderless clearfix">
			<!-- <tr>
				<td>CASE</td>
				<td><input type="radio" name="case" value="BILL" id="bill" onclick="inputDisable(this);" required="required"> BILL</td>
				<td><input type="radio" name="case" value="FRESH" id="fresh" onclick="removeDisable(this)" required="required"> FRESH CASE</td>
				<td><input type="radio" name="case" value="PENDING" id="pending" onclick="removeDisable(this)" required="required"> PENDING CASE</td>
				<td><input type="radio" name="case" value="PENDING DATA ENTRY" id="pending case" onclick="inputDisable(this);" required="required"> PENDING DATA ENTRY</td>
				<td></td>
			</tr> -->
			<!-- <tr>
				<td>PROCESS</td>
				<td><input type="checkbox" name="caseStage" value="PENDING FOR SCAN" id="scanning"> DATA ENTRY</td>
				<td><input type="checkbox" name="caseStage" value="SCANNING DONE" id="scanning"> SCANNED</td>
				<td><input type="checkbox" name="caseStage" value="QC DONE" id="qc"> QC</td>
				<td><input type="checkbox" name="caseStage" value="BOOKMARKING DONE" id="bookmarking"> BOOKMARKED</td>
			</tr> -->
			 <tr>
			 	<td>DATE</td>
			 	<td><input type="text" name="fromDate" placeholder="FROM DATE" id="from-date" value="${fn:escapeXml(param.fromDate)}" class="form-control input-sm input-width report" required="required"/></td>
			 	<td><input type="text" name="toDate" value="${fn:escapeXml(param.toDate)}" placeholder="TO DATE" id="to-date" class="form-control input-sm input-width report" required="required"/></td>
			    
			 	<td>
			 	</td>
			 </tr>
			  <tr>
			 	<td>USER</td>
			 	<td>
			 	 <select name="username" class="form-control input-sm input-width">
			 	    <option value="">select</option>
                     <c:forEach var="user" items="${userList}">
                     <option value="${user.userFullName}">${user.userFullName}</option>
                  </c:forEach>
                </select>
			 	</td>
			 </tr>
			 
			 
			<%--  <tr>
			 	<td>USER</td>
			 	<td>
			 	 <select name="username" class="form-control input-sm input-width">
			 	    <option value="ALL">ALL</option>
                     <c:forEach var="user" items="${userList}">
                     <option value="${user.username}">${user.username}</option>
                  </c:forEach>
                </select>
			 	</td>
			 </tr> --%>
			 <tr>
				<td colspan="4" align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">DISPLAY</button>
				</td>
			</tr>
	  </table>
	</form>
	<div>
	</div>
	</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning clearfix">
		<form autocomplete="off" action="${pageContext.request.contextPath}/exportHearingReport" method="post">
		<input type="hidden" name="casename" value="${casename}">
		<input type="hidden" name="caseStage" value="${caseStage}">
		<input type="hidden" name="from" value="${fromDate}">
		<input type="hidden" name="to" value="${toDate}">
					<input type="hidden" name="username" value="${username}">
		<c:if test="${not empty freshCaseReportList}">
		<table class="table table-condensed borderless clearfix">
		  <thead>
			    <tr class="bg-primary">
					<th>CASE TYPE</th>
					<th>AUTO NO</th>
					<th>DIARY NO</th>
					<th>CASE YAER</th>
					<th>DOCUMENT NAME</th>
				</tr>
			</thead>
				<tbody>
					<c:forEach var="freshCase" items="${freshCaseReportList}">
					<tr>
							<td>${freshCase.caseType}</td>
							<td>${freshCase.autoNo}</td>
							<td>${freshCase.diaryNo}</td>
							<td>${freshCase.year}</td>
							<td>${freshCase.documentName}</td>
					</tr>
					</c:forEach>
				</tbody>
		  </table>
		  </c:if>
		  <c:if test="${not empty pendingCaseList}">
		  <tr class="bg-primary">
					<td>Total: ${pendingCaseList.size()}</td>
					<td> Scan: ${scanList.size()}</td>
					<td> Qc: ${qcList.size()}</td>
					<td colspan="10"></td>
		    </tr>
		  <table class="table table-condensed borderless clearfix">
		  
		  <thead>
			    <tr class="bg-primary">
			        <th>CASE NO</th>
			        <th>CASE YEAR</th>
					<th>CASE CATEGORY</th>
					<th>CASE TYPE</th>
					<th>COURT NO </th>
					<th>TOTAL FILE </th>
					<th>HEARING DATE</th>
					<th>REMARKS</th>
					 <th>DATA ENTRY DATE</th>
					<th>SCANNED DATE</th>
					<th>SCANNNED BY</th>
					<th>QC DATE</th>
					<th>QC BY</th>
				</tr>
			</thead>
				<tbody>
					<c:forEach var="pendingCase" items="${pendingCaseList}">
						<tr>
							<td>${pendingCase.caseNo}</td>
							<td>${pendingCase.caseYear}</td>
							<td>${pendingCase.caseCategory}</td>
							<td>${pendingCase.caseType}</td>
							<td>${pendingCase.courtNo}</td>
							 <td>${pendingCase.totalFile}</td>
							<td>${pendingCase.hearingDate}</td>
							<td>${pendingCase.remarks}</td>
							 <td>${pendingCase.createdOn}</td>
							<td>${pendingCase.scannedOn}</td>
							<td>${pendingCase.scannerFullname}</td>
							<td>${pendingCase.qcOn}</td>
						    <td>${pendingCase.qcFullname}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		 </c:if>
		 <c:if test="${not empty pendingCaseDataEntryReportList}">
		  <table class="table table-condensed borderless clearfix">
		  <thead>
			    <tr class="bg-primary">
			        <th>PENDING CASE ID</th>
			        <th>CASE NO</th>
			        <th>COURT NO</th>
			        <th>TOTAL FILE</th>
			        <th>CASE YEAR</th>
					<th>CASE CATEGORY</th>
					<th>CASE TYPE</th>
					<th>HEARING DATE</th>
					<th>USER NAME</th>
				     <th>CASE STAGE</th>
					
				</tr>
			</thead>
				<tbody>
					<c:forEach var="pendingCaseDatEnty" items="${pendingCaseDataEntryReportList}">
					<tr>
							<td>${pendingCaseDatEnty.pendingCaseId}</td>
							<td>${pendingCaseDatEnty.caseNo}</td>
							<td>${pendingCaseDatEnty.courtNo}</td>
							<td>${pendingCaseDatEnty.totalFile}</td>
							<td>${pendingCaseDatEnty.caseYear}</td>
							<td>${pendingCaseDatEnty.caseCategory}</td>
							<td>${pendingCaseDatEnty.caseType}</td>
							<td>${pendingCaseDatEnty.hearingDate}</td>
							<td>${pendingCaseDatEnty.createdBy}</td>
							<td>${pendingCaseDatEnty.caseStage}</td>
							
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		<c:if test="${not empty invoiceDetailList}">
		  <table class="table table-condensed borderless clearfix">
		  	<thead>
			    <tr class="bg-primary">
			        <th>AUTO NO</th>
			        <th>BILL NO</th>
			        <th>CLIENT NAME</th>
			        <th>CASE NO</th>
			        <th>CASE TYPE</th>
			        <th>BILL BY</th>
					<th>NO OF PAGE</th>
					<th>AMOUNT</th>
					
					
				</tr>
			</thead>
			<tbody>
				<c:forEach var="invoice" items="${invoiceDetailList}">
					<tr>
						<td>${invoice.autoNo}</td>
						<td>${invoice.billNo}</td>
						<td>${invoice.clientName}</td>
						<td>${invoice.caseNo}</td>
						<td>${invoice.caseType}</td>
						<td>${invoice.createdBy}</td>
						<td>${invoice.noOfPage}</td>
						<td>${invoice.amount}</td>
						
					</tr>
				</c:forEach>
			</tbody>
		  </table>
		</c:if>
		<%-- <c:if test="${(not empty freshCaseReportList) or (not empty pendingCaseFormList) or (not empty pendingCaseList) or (not empty invoiceDetailList)}">
				<button class="btn btn-sm btn-info btn-block form-btn input-width" type="submit"> <i class="fa fa-file-excel-o"></i>  DOWNLOAD REPORT</button>
		</c:if> --%>
		</form>
		</div>
		<div>
	  </div>
	<div class="col-sm-2"></div>
	</div>
	</div>
	</div>
	</div>
</div> 
</section>
<footer>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>