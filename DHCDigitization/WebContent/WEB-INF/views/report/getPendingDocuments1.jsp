<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<%-- <jsp:include page="../common/resourcesLib.jsp"></jsp:include> --%>
</head>
<body>

        <table class="table table-condensed table-bordered table-striped">
				<thead>
					<tr class="bg-primary">
						<th>CASE NO</th>
						<th>CASE YEAR</th>
						<th>CASE CATEGORY</th>
						<th>DOCUMENT NAME</th>
						<th>SCANNED PAGES</th>
						<th>SCANNING DATE</th>
						<th>QC PAGES</th>
						<th>QC DATE</th>
						<th>ACTION</th>
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach var="pendingCase" items="${pendingCaseList}">
					<tr> 
						<td>${pendingCase.caseNo}</td>
						<td>${pendingCase.caseYear}</td>
						<td>${pendingCase.caseCategory}</td>
						<td>${pendingCase.documentName}</td>
						<td>${pendingCase.scanningPage}</td>
						<td>${pendingCase.scanningOn}</td>
						<td>${pendingCase.qcPage}</td>
						<td>${pendingCase.qcOn}</td>
						<td><a type="button" href="${pageContext.request.contextPath}/getDocumentsById?documentId=${pendingCase.documentId}" target="_blank" class="btn btn-primary btn-sm">View</a></td>
					</tr>
					</c:forEach>
				</tbody>
				<tfoot>
				<tr class="bg-primary">
					<td>Total: ${pendingCaseList.size()}</td>
					<td></td>
					<td colspan="10"></td>
		       	</tr>
				</tfoot>
			</table>
			
</body>
</html>