<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
<script type="text/javascript">
function freshSearchValidate() {
	var autono=document.getElementById("autono").value;
	var diaryno=document.getElementById("diaryno").value;
	var year=document.getElementById("year").value;
	if(autono=="" && diaryno=="" && year==""){
		alert("Please enter atleast one");
		return false;
	}
	
}

</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">REPORT</li>
   		</ol>
		<div class="row">
		<div class="col-sm-12">
		<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-4">
		<div class="panel panel-body panel-warning clearfix">
		<form autocomplete="off" action="${pageContext.request.contextPath}/getAllFreshCaseDocument" method="post" onsubmit="return freshSearchValidate()" >
			<table class="table table-condensed borderless clearfix">
			<tr class="bg-primary">
				<td colspan="2">FRESH CASE</td>
			</tr>
			 <tr>
			 	<td>AUTO NO</td>
			 	<td><input type="text" name="autoNo" id="autono" placeholder="AUTO NO" class="form-control input-sm input-width"/></td>
			 </tr>
			 <tr>
			 	<td>DIARY NO</td>
			 	<td><input type="text" name="diaryNo" id="diaryno" placeholder="DIARY NO" class="form-control input-sm input-width"/></td>
			 </tr>
			 <tr>
			 	<td>YEAR</td>
			 	<td><input type="text" name="year" id="year" placeholder="YEAR" class="form-control input-sm input-width"/></td>
			 </tr>
			 <tr>
				<td colspan="2" align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">SEARCH</button>
				</td>
			</tr>
	  </table>
	</form>
	<div>
	</div>
	</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-body panel-warning clearfix">
		<form autocomplete="off" action="${pageContext.request.contextPath}/getAllPendingCaseDocument" method="post">
			<table class="table table-condensed borderless clearfix">
			<tr class="bg-primary">
				<td colspan="2">PENDING CASE</td>
			</tr>
			 <tr>
			 	<td>CASE NO</td>
			 	<td><input type="text" name="caseNo" placeholder="CASE NO" value="${fn:escapeXml(param.caseNo)}" class="form-control input-sm input-width" required="required"/></td>
			 </tr>
			 <tr>
			 	<td>CASE YEAR</td>
			 	<td>
			 		<input name="caseYear" list="yearList" placeholder="CASE YEAR"  value="${fn:escapeXml(param.caseYear)}" class="form-control input-sm input-width" maxlength="4" required="required"/>
					<datalist id="yearList">
						<c:forEach var="year" begin="1960" end="2025">
						<option value="${year}"></option>
						</c:forEach>
					</datalist>
			 	</td>
			 </tr>
			 <tr>
			 	<td>CASE CATEGORY</td>
			 	<td>
			 		<select name="caseCategory" class="form-control input-sm in-width">
					<option value="">--SELECT--</option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'CASE_CATEGORY'}">
						<option value="${lookup.lookupValue}" ${lookup.lookupValue== selectedModule ? 'selected':'' }>${lookup.lookupValue}</option>
						</c:if>
					</c:forEach>
					</select>
			 	</td>
			 </tr>
			 <tr>
				<td colspan="2" align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">SEARCH</button>
				</td>
			</tr>
	  </table>
	</form>
	<div>
	</div>
	</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
		<div class="row">
		<div class="col-sm-12">
		<div class="panel panel-warning panel-body">
			<c:if test="${not empty pendingcaseList}">
			<table class="table table-condensed table-bordered">
			<thead>
				<tr class="bg-primary">
				
					<th>CASE NO</th>
					<th>CASE YEAR</th>
					<th>CASE CATEGORY</th>
					<th>CASE TYPE</th>
					<th>HEARING DATE</th>
					<th>REMARKS</th>
					<th>COURT NO</th>
					<th>DEALING</th>
					<th>TOTAL FILE</th>
					<th>DATA ENTRY DATE</th>
					<th>DATA ENTRY BY</th>
					<th>SCANNNED DATE</th>
					<th>SCANNED BY</th>
					<th>QC DATE</th>
					<th>QC BY</th>
					<th>DOCUMENT VIEW</th>
					
				</tr>
			</thead>
			<tbody>
				<c:forEach var="pendingCase" items="${pendingcaseList}">
				<tr>
					<td>${pendingCase.caseNo}</td>
					<td>${pendingCase.caseYear}</td>
					<td>${pendingCase.caseCategory}</td>
					<td>${pendingCase.caseType}</td>
					<td>${pendingCase.hearingDate}</td>
					<td>${pendingCase.remarks}</td>
					<td>${pendingCase.courtNo}</td>
					<td>${pendingCase.dealing}</td>
					
					<td>${pendingCase.totalFile}</td>
					<td>${pendingCase.createdOn}</td>
					<td>${pendingCase.createdBy}</td>
					<td>${pendingCase.scannedOn}</td>
					<td>${pendingCase.scannerFullname}</td>
					<td>${pendingCase.qcOn}</td>
					<td>${pendingCase.qcFullname}</td>
				  	<%-- <td align="center"><a href="searchAllPendingCaseExcelExport?pendingCaseId=${pendingCaseDocumentForm.pendingCase.pendingCaseId}" class="btn btn-xs btn-info" role="button"><i class="fa fa-file-excel-o"></i>DOWNLOAD REPORT</a></td> --%>
				  	<%--  <td><a href="${pageContext.request.contextPath}/getPendingDocuments?pendingCaseId=${pendingCase.pendingCaseId}" class="btn btn-xs btn-primary" role="button" >Documents</a></td>  --%>
				  	<td><button type="button" href="${pageContext.request.contextPath}/getPendingDocuments?pendingCaseId=${pendingCase.pendingCaseId}" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#myModal">View Document</button></td>
				  	
				  
				</tr>
				</c:forEach>
				<c:if test="${empty pendingcaseList}">
				<tr>
					<td colspan="10" class="text-danger">RECORD NOT FOUND</td>
				</tr>
				</c:if>
			</tbody>
			<tfoot>
				<tr class="bg-primary">
				<td colspan="16"></td>
				</tr>
			</tfoot>
			</table>
			</c:if>
		    <c:if test="${not empty freshCaseList}">
		    <div style="overflow:scroll;height:80px;width:100%;overflow:auto">
			<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
					    <th style="display: none;">ID</th>
						<th>DIARY NO</th>
						<th>YEAR</th>
						<th>DOCUMENT TYPE</th>
						<th>CASE STAGE</th>
						<th>CASE TYPE</th>
						<th>SACANNING DATE</th>
						<th>SACAN BY</th>
						<th>QC DATE</th>
						<th>QC BY</th>
						<th>BOOKMARK DATE</th>
						<th>BOOKMARK BY</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="freshCase" items="${freshCaseList}">
					<tr>
						<td style="display: none;">${freshCase.id}</td>
						<td>${freshCase.diaryNo}</td>
						<td>${freshCase.year}</td>
						<td>${freshCase.documentType}</td>
						<td>${freshCase.caseStage}</td>
						<td>${freshCase.caseType}</td>
						<td><fmt:formatDate pattern="dd-MM-yyyy" value="${freshCase.scanningOn}" /></td>
						<td>${freshCase.scanningBy}</td>
						<td><fmt:formatDate pattern="dd-MM-yyyy" value="${freshCase.qcOn}" /></td>
						<td>${freshCase.qcBy}</td>
						<td><fmt:formatDate pattern="dd-MM-yyyy" value="${freshCase.bookmarkingOn}" /></td>
						<td>${freshCase.bookmarkingBy}</td>
						<td><a href="searchAllFreshCaseExcelExport?id=${freshCase.id}"><i class="fa fa-file-excel-o"></i>DOWNLOAD REPORT</a></td>
					</tr>
					</c:forEach>
					<c:if test="${empty freshCaseList}">
					<tr>
						<td colspan="10" class="text-danger">RECORD NOT FOUND</td>
					</tr>
					</c:if>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="12"></td>
					</tr>
				</tfoot>
				</table>
         </div>
				
				</c:if>
				
				</div>
				</div>
			</div>
		</div>
		
		<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
      <div class="col-sm-12">
      <jsp:include page="../report/getPendingDocuments1.jsp"></jsp:include> 
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
		<!--------------------------------------------------->
	</div>	
	</div>
  </div>
</div> 
</section>
<footer>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>