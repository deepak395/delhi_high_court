<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.col-md-3 .card-counter a{
	display: block;
	text-decoration: none;
}
</style>
<script type="text/javascript">
$(function() {
	$('.report').datepicker({dateFormat: 'dd-mm-yy', constrainInput: false, maxDate: '0'});
	//$( ".report" ).datepicker({ constrainInput: false });
});

function inputDisable(e) {
	 if(document.getElementById(e.id).checked){
	  	document.getElementById("scanning").disabled = true;
		document.getElementById("qc").disabled = true;
		document.getElementById("bookmarking").disabled = true;
	 }else{
		document.getElementById("scanning").disabled = false;
		document.getElementById("qc").disabled = false;
		document.getElementById("bookmarking").disabled = false;
	 } 
	}
function removeDisable(e) {
	document.getElementById("scanning").disabled = false;
	document.getElementById("qc").disabled = false;
	document.getElementById("bookmarking").disabled = false;
}

/* function compare(){
    var startDt = document.getElementById("from-date").value;
    var endDt = document.getElementById("to-date").value;
    if((new Date(startDt) <= new Date(endDt))){
    	
    }else{
    	 var endDt = document.getElementById().value=""
    	alert("not valid date");
    }
} */
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">ALL REPORT</li>
   	</ol>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-body panel-warning clearfix">
		<h5><b>REPORT</b></h5>
		<form autocomplete="off" action="${pageContext.request.contextPath}/viewPdfFormat" method="get">
			<table class="table table-condensed borderless clearfix">
			 <tr>
			 	<td>DATE</td>
			 	<td><input type="text" name="fromDate" placeholder="FROM DATE" id="from-date" class="form-control input-sm input-width report" required="required"/></td>
			 	<td><input type="text" name="toDate" placeholder="TO DATE" id="to-date" class="form-control input-sm input-width report" required="required"/></td>
			 	<td>
			 	</td>
			 </tr>
			 <tr>
				<td colspan="4" align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">DISPLAY</button>
				</td>
			</tr>
	  </table>
	</form>
	<div>
	</div>
	</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
	</div>
	</div>
</div> 
</section>
<footer>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>