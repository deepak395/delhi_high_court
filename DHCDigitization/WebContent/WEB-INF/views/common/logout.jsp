<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Logout</title>
<jsp:include page="resourcesLib.jsp"></jsp:include>
</head>
<body>
<header>
<jsp:include page="header.jsp"></jsp:include>
</header>
<section>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-10 text-danger">
		<p><a href="${pageContext.request.contextPath}/">Login again</a></p>
		</div>
		<div class="col-sm-2"></div>
	</div>
</div>	
</section>
<footer>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>