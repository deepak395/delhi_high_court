<!-- css file -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/footer.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.css" rel="stylesheet"/>
 <link href="${pageContext.request.contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
  
 <!--  js file -->
 <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
 <script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
 <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
 <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
 <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
 