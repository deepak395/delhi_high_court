<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#" style="background-color: #fff">
     <img alt="" src="${pageContext.request.contextPath}/resources/img/stockholding-dms.jpg" style="width:70px;padding:5px;margin-top: -10px;">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
	  <ul class="nav navbar-nav">
	      <li class="logo-font"><a href="#">DELHI HIGH COURT</a></li>
	  </ul>
      <ul class="nav navbar-nav navbar-right">
      <c:forEach var="menuForm" items="${menuFormList}">
      <c:choose>
      	<c:when test="${user.roleName eq 'SUPER ADMIN'}">
      		<li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown" href="#">${menuForm.menu.menuName}
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
			        <c:forEach var="submenu" items="${menuForm.submenuList}">
			         <li><a href="${pageContext.request.contextPath}/${submenu.submenuUrl}">${submenu.submenuName}</a></li>
			        </c:forEach>
		        </ul>
	      	</li>
      	</c:when>
      	<c:otherwise>
      		<c:forEach var="submenu" items="${menuForm.submenuList}">
	         <li><a href="${pageContext.request.contextPath}/${submenu.submenuUrl}">${submenu.submenuName}</a></li>
	        </c:forEach>
      	</c:otherwise>
      	</c:choose>
      	</c:forEach>
        <c:if test="${not empty user}">
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">  ${user.userFullName} <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="${pageContext.request.contextPath}/changePassword">CHANGE PASSWORD</a></li>
          <li><a href="${pageContext.request.contextPath}/logout">LOGOUT</a></li>
        </ul>
      </li>
      </c:if>
      <c:if test="${empty user}">
       <li><a href="${pageContext.request.contextPath}/">LOGIN</a></li>
       </c:if>
      </ul>
    </div>
  </div>
</nav>