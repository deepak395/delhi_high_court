<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="sidebar nav-side-menu">
 <div class="brand">${client.clientName}</div>
 	<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
     <div class="menu-list">
         <ul id="menu-content" class="menu-content collapse out">
             <li>
               <a href="${pageContext.request.contextPath}/dashboard">
              	 <i class="fa fa-dashboard fa-lg"></i> Dashboard
               </a>
             </li>
     		<%int i=1;%>
     		<c:forEach var="menuForm" items="${menuFormList}">
     		<c:if test="${not empty menuForm.submenu}">
     		<li data-toggle="collapse" data-target="#products<%=i%>" class="collapsed active">
               <a href="#"><i class="fa ${menuForm.menu.menuName} fa-lg"></i> ${menuForm.menu.menuName} <span class="arrow"></span></a>
                <ul class="sub-menu collapse" id="products<%=i%>">
                <c:forEach var="submenu" items="${menuForm.submenu}">
                 <li><a href="${submenu.submenuUrl}"><i class="fa fa-chevron-right"></i> ${submenu.submenuName}</a></li>
                 </c:forEach>
             	</ul>
             </li>
             </c:if>
             <%i++; %>
     		</c:forEach>	
         </ul>
    </div>
</div>