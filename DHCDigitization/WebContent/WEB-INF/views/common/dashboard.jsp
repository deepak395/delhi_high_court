<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
<script type="text/javascript">
function freshSearchValidate() {
	var autono=document.getElementById("autono").value;
	var diaryno=document.getElementById("diaryno").value;
	var year=document.getElementById("year").value;
	if(autono=="" && diaryno=="" && year==""){
		alert("Please enter atleast one");
		return false;
	}
	
}
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        </ol>
	       <div class="row">
	       <div class="col-sm-10 col-sm-offset-2">
	       
	            <img alt="" src="${pageContext.request.contextPath}/resources/img/delhi-high-court.jpg" style="width:80%;">
	       
	       
	       
	       </div>
	       
		
	        
	        
	        </div>
	        
	        
	        
	        
	        
	        
	        </div>
	        
	       <!--  <li class="breadcrumb-item active">SEARCH</li> -->
		<%-- <div class="row">
		<div class="col-sm-12">
		<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-4">
		<div class="panel panel-body panel-warning clearfix">
		<form autocomplete="off" action="${pageContext.request.contextPath}/searchAllFreshCase" method="post" onsubmit="return freshSearchValidate()" >
			<table class="table table-condensed borderless clearfix">
			<tr class="bg-primary">
				<td colspan="2">FRESH CASE</td>
			</tr>
			 <tr>
			 	<td>AUTO NO</td>
			 	<td><input type="text" name="autoNo" id="autono" placeholder="AUTO NO" class="form-control input-sm input-width"/></td>
			 </tr>
			 <tr>
			 	<td>DIARY NO</td>
			 	<td><input type="text" name="diaryNo" id="diaryno" placeholder="DIARY NO" class="form-control input-sm input-width"/></td>
			 </tr>
			 <tr>
			 	<td>YEAR</td>
			 	<td><input type="text" name="year" id="year" placeholder="YEAR" class="form-control input-sm input-width"/></td>
			 </tr>
			 <tr>
				<td colspan="2" align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">SEARCH</button>
				</td>
			</tr>
	  </table>
	</form>
	</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-body panel-warning clearfix">
		<form autocomplete="off" action="${pageContext.request.contextPath}/searchAllPendingCase" method="post">
			<table class="table table-condensed borderless clearfix">
			<tr class="bg-primary">
				<td colspan="2">PENDING CASE</td>
			</tr>
			 <tr>
			 	<td>CASE NO</td>
			 	<td><input type="text" name="caseNo" placeholder="CASE NO" class="form-control input-sm input-width" required="required"/></td>
			 </tr>
			 <tr>
			 	<td>CASE YEAR</td>
			 	<td>
			 		<input name="caseYear" list="yearList" placeholder="CASE YEAR" class="form-control input-sm input-width" maxlength="4" required="required"/>
					<datalist id="yearList">
						<c:forEach var="year" begin="1966" end="2019">
						<option value="${year}"></option>
						</c:forEach>
					</datalist>
			 	</td>
			 </tr>
			 <tr>
			 	<td>CASE CATEGORY</td>
			 	<td>
			 		<select name="caseCategory" class="form-control input-sm in-width">
					<option value="">--SELECT--</option>
					<c:forEach var="lookup" items="${lookupList}">
						<c:if test="${lookup.lookupName eq 'CASE_CATEGORY'}">
						<option value="${lookup.lookupValue}">${lookup.lookupValue}</option>
						</c:if>
					</c:forEach>
					</select>
			 	</td>
			 </tr>
			 <tr>
				<td colspan="2" align="center">
					<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">SEARCH</button>
				</td>
			</tr>
	  </table>
	</form>
	<div>
	</div>
	</div>
	</div>
	<div class="col-sm-2"></div>
	</div>
		<div class="row">
				<div class="col-sm-12">
				<div class="panel panel-warning panel-body">
					<c:if test="${not empty pendingcasedetail}">
					<table class="table table-condensed table-bordered">
					<thead>
						<tr class="bg-primary">
							<th>CASE NO</th>
							<th>CASE YEAR</th>
							<th>CASE CATEGORY</th>
							<th>CASE TYPE</th>
							<th>HEARING DATE</th>
							<th>REMARKS</th>
							<th>TOTAL FILE</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${pendingcasedetail.caseNo}</td>
							<td>${pendingcasedetail.caseYear}</td>
							<td>${pendingcasedetail.caseCategory}</td>
							<td>${pendingcasedetail.caseType}</td>
							<td>${pendingcasedetail.hearingDate}</td>
							<td>${pendingcasedetail.remarks}</td>
							<td>${pendingcasedetail.totalFile}</td>
						</tr>
						<c:if test="${empty pendingCaseList}">
						<tr>
							<td colspan="10" class="text-danger">RECORD NOT FOUND</td>
						</tr>
						</c:if>
					</tbody>
					<tfoot>
						<tr class="bg-primary">
						<td colspan="10"></td>
						</tr>
					</tfoot>
					</table>
					</c:if>
					
				<c:if test="${not empty freshCaseList}">
				<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
						<th>DIARY NO</th>
						<th>YEAR</th>
						<th>DOCUMENT TYPE</th>
						<th>CASE STAGE</th>
						<th>DOCUMENT NAME</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="freshCase" items="${freshCaseList}">
					<tr>
						<td>${freshCase.diaryNo}</td>
						<td>${freshCase.year}</td>
						<td>${freshCase.documentType}</td>
						<td>${freshCase.caseStage}</td>
						<td>${freshCase.documentName}</td>
					</tr>
					</c:forEach>
					<c:if test="${empty freshCaseList}">
					<tr>
						<td colspan="10" class="text-danger">RECORD NOT FOUND</td>
					</tr>
					</c:if>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="10"></td>
					</tr>
				</tfoot>
				</table>
				</c:if>
				</div>
				</div>
			</div>
		</div>
	</div>	
 --%>	
  </div>
</div> 
</section>
<footer>
<jsp:include page="footer.jsp"></jsp:include>
</footer>
</body>
</html>