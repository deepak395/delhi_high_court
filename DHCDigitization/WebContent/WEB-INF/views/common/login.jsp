<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Login</title>
<jsp:include page="resourcesLib.jsp"></jsp:include>
<link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet"/>
</head>
<body>
<div class="container layer">
<div class="row">
	<div class="col-sm-4 text-center">
	<div class="text-right clogo-div">
	<img alt="" src="${pageContext.request.contextPath}/resources/img/logo.gif" id="clogo">
	</div>
	</div>
	<div class="col-sm-4">
		<div class="login-form" style="margin-top: 20px;">
		    <form action="${pageContext.request.contextPath}/login" method="post">
				<div class="avatar">
					<i class="fa fa-user" style="font-size: 60px;margin: auto;"></i>
				</div> 
				<h3 style="background-color: #fff;" class="text-center">
				<img alt="" src="${pageContext.request.contextPath}/resources/img/stockholding-dms.jpg" style="width:70px;padding:2px;">
				&nbsp;DIGITIZATION
				</h3>
		       
		       <div style="margin-bottom: 25px" class="input-group">
		           <span class="input-group-addon"><i class="fa fa-user"></i></span>
		           <input id="login-username" type="text" class="form-control login-input" name="username" placeholder="USERNAME">                                        
		       </div>
				<div style="margin-bottom: 25px" class="input-group">
		             <span class="input-group-addon"><i class="fa fa-lock"></i></span>
		             <input id="login-password" type="password" class="form-control login-input" name="password" placeholder="PASSWORD">
		         </div>  
		        <div class="form-group">
		            <button type="submit"  class="btn btn-lg btn-block login-btn">SIGN IN</button>
		        </div>
		        <div class="clearfix">
		            <p class="text-center" style="color: #ff8533"><c:if test="${not empty errormsg}">${errormsg}</c:if></p>
		        </div>
		    </form> 
	    <p class="text-center small">
	    
	    </p>
	   </div>
	</div>
	<div class="col-sm-4"></div>
</div>
</div>
</body>
</html>