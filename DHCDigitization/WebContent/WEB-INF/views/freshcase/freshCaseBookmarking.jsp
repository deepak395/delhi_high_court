<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
<script type="text/javascript">


</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">FRESH CASE BOOKMARK</li>
   		</ol>
   		
			
			<div class="panel panel-warning panel-body">
		<form autocomplete="off" class="form-inline" action="${pageContext.request.contextPath}/getFreshCaseBookMarkByDiaryNo" method="post">
		<table class="table table-condensed borderless">
		<tbody> 
			
			<tr>
				<td align="right" class="label-text required">
					<label for="caseNo">DIARY NUMBER</label>
				</td>
				<td>
					<input type="text" name="diaryNo" id ="diaryNo" class="form-control input-sm in-width" required="required"/>
				</td>
				
				<td align="right" class="label-text required">
					<label for="year">YEAR</label>
				</td>
				<td>
					<input type="text" name="year" id ="year" class="form-control input-sm in-width" required="required"/>
				</td>
				
				
				
				<td><button type="submit" id ="searchCase" class="form-control btn-sm btn-block btn-success in-width">SEARCH</button>
			
			</tr>
			</tbody>
			</table>
			</form>
			</div>
				<c:if test="${not empty freshCaseDetail}">
				<div class="panel panel-warning panel-body">
				<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
						<th>CASE TYPE</th>
						<th>AUTO NO</th>
						<th>DIARY NO</th>
						<th>CASE YAER</th>
						<th>DOCUMENT TYPE</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${freshCaseDetail.caseType}</td>
						<td>${freshCaseDetail.autoNo}</td>
						<td>${freshCaseDetail.diaryNo}</td>
						<td>${freshCaseDetail.year}</td>
						<td>${freshCaseDetail.documentType}</td>
					<%-- 	<td>			<a href="<c:url var="${pageContext.request.contextPath}/action" >
            <c:param name="command" value="load"/>
            <c:param name="id" value="${pendingCase.id}"/>
        </c:url>">TEST</a></td> --%>
        <%-- <td>			<a href="<c:url var="${pageContext.request.contextPath}/action" >
            <c:param name="command" value="load"/>
            <c:param name="id" value="${freshCaseDetail.id}"/>
        </c:url>">TEST</a></td> --%>
       <td> <a class=" form-control  btn-sm btn-primary btn-block " style=" text-align:center;" role="button" href="${pageContext.request.contextPath}/bookmarkingDoneByBookmarkUser?id=<c:out value="${freshCaseDetail.id}"/>">Done</a> </td>
        
					</tr>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="8" class="text-danger"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</c:if>
			</div>
			<div class="col-sm-2"></div>
		</div>
		
		<%-- </form:form> --%>
	</div>	
	</div>
</div>
</section>
<c:if test="${not empty successMessage}">
	<script type="text/javascript">
		$(function() {
			 $('#scanningsuccessmodal').modal('show');
		});
	</script>
</c:if>
<div class="modal fade centered-modal" id="scanningsuccessmodal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-body">
         <p class="text-success">${successMessage}</p>
         <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
</div>
<footer>
<%-- <jsp:include page="../common/footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>




<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<script type="text/javascript">
$(function() {
	 $('.case-type').click(function(){
		  $("#fresh-list").empty(); 
		 if($(this).prop("checked")==true){
			 $("#case-type").val($(this).val()); 
		    $.ajax({
		    	type :'POST',
				url : '${pageContext.request.contextPath}/getAllFreshcaseListByForBookmarking',
				data : {
					caseType : $(this).val()
				},
				dataType:'json',
				success : function(result) {
					$.each(result, function(index, value) {
					$("#fresh-list").append("<tr>"
						+"<td>"+value.caseType+"</td>"
						+"<td>"+value.autoNo+"</td>"
						+"<td>"+value.diaryNo+"</td>"
						+"<td>"+value.year+"</td>"
						+"<td>"+value.documentName+"</td>"
						+"<td class='text-center'>"
						+"<form action='${pageContext.request.contextPath}/downloadFreshCaseDocument' method='post'>"
						+"<input type='hidden' name='id' value='"+value.id+"'>"
						+"<button type='button' class='btn btn-xs btn-warning' value='"+value.id+"' onclick='checkBookmarkingdone(this);'>BOOKMARK</button>&nbsp;&nbsp;"
						+"<button type='button' class='btn btn-xs btn-success' value='"+value.id+"' onclick='bmdone(this);'>DONE</button>"
						+"</form>"
						+"</td>"
						+"</tr>");  
					});
				}
			});
		 }
	  });
	 
	  $("#btnSubmit").click(function (event) { 

	        event.preventDefault();
	        var form = $('#fileUploadForm')[0];
	        var data = new FormData(form);
	        $("#btnSubmit").prop("disabled", true);
	        $.ajax({
	            type: "POST",
	            enctype: 'multipart/form-data',
	            url: "${pageContext.request.contextPath}/freshcaseBookmarkingDone",
	            data: data,
	            processData: false,
	            contentType: false,
	            cache: false,
	            timeout: 600000,
	            success: function (data) {
		             $('#bmModal').modal('hide');
	            	 $('#bookmarkingsuccessmodal').modal('show');
		             $("#btnSubmit").prop("disabled", false);
		             $("#fresh-list").empty(); 
		    		 if($("#case-type").val()!=''){
		    		    $.ajax({
		    		    	type :'POST',
		    				url : '${pageContext.request.contextPath}/getAllFreshcaseListByForBookmarking',
		    				data : {
		    					caseType : $("#case-type").val()
		    				},
		    				dataType:'json',
		    				success : function(result) {
		    					$.each(result, function(index, value) {
	    						$("#fresh-list").append("<tr>"
	   								+"<td>"+value.caseType+"</td>"
	   								+"<td>"+value.autoNo+"</td>"
	   								+"<td>"+value.diaryNo+"</td>"
	   								+"<td>"+value.year+"</td>"
	   								+"<td>"+value.documentName+"</td>"
	   								+"<td class='text-center'>"
	   								+"<form action='${pageContext.request.contextPath}/downloadFreshCaseDocument' method='post'>"
	   								+"<input type='hidden' name='id' value='"+value.id+"'>"
	   								+"<button type='button' class='btn btn-xs btn-warning' value='"+value.id+"' onclick='checkBookmarkingdone(this);'>BOOKMARK</button>&nbsp;&nbsp;"
	   								+"<button type='button' class='btn btn-xs btn-success' value='"+value.id+"' onclick='bmdone(this);'>DONE</button>"
	   								+"</form>"
	   								+"</td>"
	   								+"</tr>");
		    					});
		    				}
		    			});
		    		 }
	            }
	        });

	    }); 

});
function checkBookmarkingdone(e) {
    $.ajax({
    	type :'POST',
		url : '${pageContext.request.contextPath}/checkQcDone',
		data : {
			id : e.value,
			action:'BOOKMARKING'
		},
		success : function(result) {
		 if(result=='true'){
			 alert("Already file downloded");
		 }else{
			 window.location.href = "${pageContext.request.contextPath}/downloadFreshCaseDocument?id="+e.value+"&action=BOOKMARKING";
		 	}
		}
	});
}
function bmdone(e) {
	$("#case-id").val(e.value)	
    $('#bmModal').modal('show');
}
$(document).ready(function(){
    $("#diary-no").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#fresh-list tr").filter(function() {
      	$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item active"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">FRESH CASE BOOKMARK</li>
	   	</ol>
		<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="panel panel-warning panel-body">
			<table class="table table-condensed borderless">
				<tbody> 
					<tr>
						<td>
							<div class="checkbox">
								<c:forEach var="lookup" items="${lookupList}">
									<c:if test="${lookup.lookupName eq 'FRESH_CASE_TYPE'}">
									<label><input name="caseType" id="case-type" class="case-type" type="radio" value="${lookup.lookupValue}"> ${lookup.lookupValue}</label>
									</c:if>
								</c:forEach>
							</div>
							<input type="hidden" id="case-type">
						</td>
						<td align="right" class="label-text required">
							<label for="diaryNo">DIARY NO</label>
						</td>
						<td>
							<input type="text" name="diaryNo" id="diary-no" class="form-control input-sm">
						</td>
					</tr>
				</tbody>
			</table>
			</div>
			<div class="panel panel-warning panel-body">
				<table class="table table-condensed table-bordered">
					<thead>
						<tr class="bg-primary">
							<th>CASE TYPE</th>
							<th>AUTO NO</th>
							<th>DIARY NO</th>
							<th>CASE YEAR</th>
							<th>DOCUMENT NAME</th>
							<th class="text-center">ACTION</th>
						</tr>
					</thead>
					<tbody id="fresh-list">
						
					</tbody>
					<tfoot>
						<tr class="bg-primary">
						<td colspan="6" class="text-danger"></td>
						</tr>
					</tfoot>
				</table>
				</div>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>	
	</div>
</div>
</section>
<div class="modal fade centered-modal" id="bmModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times text-danger"></i></button>
          <h6 class="modal-title">BOOKMARK FRESH CASE</h6>
        </div>
        <div class="modal-body">
        <form:form id="fileUploadForm" autocomplete="off" action="${pageContext.request.contextPath}/freshcaseBookmarkingDone" method="post" enctype="multipart/form-data">
         <table class="table table-condensed borderless clearfix">
		 <tbody>
			<tr>
				<td align="right" class="label-text required" width="100px">
					<label for="doc-file">SELECT FILE:</label>
				</td>
				<td>
					<input type="hidden" name="id" id="case-id">
					<input type="file" name="docFile" id="doc-file"/>
				</td>
				<td>
					<button class="btn btn-sm btn-success btn-block form-btn input-width" id="btnSubmit" type="submit">UPLOAD</button>
				</td>
			</tr>
			</tbody>
			</table>
		</form:form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade centered-modal" id="bookmarkingsuccessmodal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body">
         <p class="text-success">BOOKMARKONG DONE SUCCESSFULLY</p>
         <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
<footer>
<jsp:include page="footer.jsp"></jsp:include>
</footer>
</body>
</html> --%>