<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<%-- <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
<script type="text/javascript">
$(function() {
	 $('.case-type').click(function(){
		 $("#auto-no").empty();
		 if($(this).prop("checked")==true){
		    $.ajax({
		    	type :'POST',
				url : '${pageContext.request.contextPath}/getAllInvoiceDetailListByCaseType',
				data : {
					caseType : $(this).val()
				},
				dataType:'json',
				success : function(result) {
					$.each(result, function(index, value) {
						console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaa",value);
						$("#auto-no").append("<option value='"+value.autoNo+"'>"+value.autoNo+"</option>"); 
						console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaa",value);
						
					});
				}
			});
		 }
	  });
	 
	 $('#invoiceId').change(function(){
		 var invoiceId=$('#invoiceId').val();
		 if(invoiceId!=""){
			    $.ajax({
			    	type :'POST',
					url : '${pageContext.request.contextPath}/validateAutoNo',
					data : {
						invoiceId : invoiceId
					},
					success : function(result) {
						if(result=="SUCCESS"){
							$('#autono-msg').html("");
						}else{
							$('#autono-msg').html("Not valid auto no");
							$('#invoiceId').val("");
							
						}
					}
				});
			}
	  });
});

function getFileData(myFile){
   var file = myFile.files[0];  
   var filename = file.name.split(".")[0];
   var res = filename.split("-");
   $("#dairy-no").val(res[0]);
   $("#year").val(res[1]);
}
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">FRESH CASE SCANNING</li>
   		</ol>
   		<form:form autocomplete="off" action="${pageContext.request.contextPath}/saveFreshCaseDocument" modelAttribute="freshCase" method="post" enctype="multipart/form-data">
		<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-warning panel-body">
		<div class="row">
		<div class="col-sm-6">
		<div class="panel panel-warning panel-body">
			<table class="table table-condensed borderless">
				<tbody> 
				    <tr><td class="required">CASE TYPE<td></tr>
					<tr>
						<td>
							<div class="checkbox">
								<c:forEach var="lookup" items="${lookupList}">
									<c:if test="${lookup.lookupName eq 'FRESH_CASE_TYPE'}">
									<label><form:radiobutton path="caseType" class="case-type" value="${lookup.lookupValue}" onclick="getFreshcaseList(this);"/> ${lookup.lookupValue}</label>
									</c:if>
								</c:forEach>
							</div>
							<form:errors path="caseType" class="text-danger"></form:errors>
						</td>
						</tr>
				</tbody>
			</table>
			</div>
			<div class="panel panel-warning panel-body">
			<table class="table table-condensed borderless">
				<tbody> 
				    <tr><td>DOCUMENT TYPE<td></tr>
					<tr>
						<td>
							<div class="checkbox">
								<c:forEach var="lookup" items="${lookupList}">
									<c:if test="${lookup.lookupName eq 'FRESH_DOCUMENT_TYPE'}">
									<label><form:radiobutton path="documentType" class="case-type" value="${lookup.lookupValue}" onclick="getFreshcaseList(this);"/> ${lookup.lookupValue}</label>
									</c:if>
								</c:forEach>
							</div>
							<form:errors path="caseType" class="text-danger"></form:errors>
						</td>
						</tr>
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-sm-6">
		<table class="table table-condensed borderless">
			<tbody> 
				<tr><td colspan="2"></td></tr>
				<tr>
				<td class="label-text required">AUTO NO</td>
				<td>
				    <form:input path="autoNo" id="invoiceId" list="auto-no" class="form-control input-sm input-width"/>
				    <datalist id="auto-no"></datalist>
				    <form:errors path="autoNo" class="text-danger"></form:errors>
				    <span id="autono-msg" class="text-danger"></span>
				</td>
				</tr>
				<tr>
					<td align="right" class="label-text required">
						<form:label path="docFile">SELECT DOCUMENT</form:label>
					</td>
					<td>
						<form:input type="file" id="cfile" path="docFile" onchange="getFileData(this);validateFileExtension(this);"/> 
						<form:errors path="docFile" class="text-danger"></form:errors>
					</td>
				</tr>
				<tr>
					<td class="label-text required">DIARY NO</td>
					<td>
					    <form:input path="diaryNo" id="dairy-no" class="form-control input-sm input-width" readonly="true"/>
					    <form:errors path="diaryNo" class="text-danger"></form:errors>
					</td>
				</tr>
				<tr>
					<td class="label-text required">YEAR</td>
					<td>
						<form:input path="year" id="year" class="form-control input-sm input-width" readonly="true"/>
						 <form:errors path="year" class="text-danger"></form:errors>
					</td>
				</tr>
				<tr><td colspan="2"></td></tr>
				<tr><td colspan="2"></td></tr>
				<tr> 
					<td></td>
				 	<td align="center">
				 	<button type="submit" class="form-control btn-sm btn-primary btn-block">UPLOAD</button>
				 	</td>
				 </tr>
			</tbody>
		</table>
		</div>
		</div>
			</div>
				<c:if test="${not empty freshCaseDetail}">
				<div class="panel panel-warning panel-body">
				<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
						<th>CASE TYPE</th>
						<th>AUTO NO</th>
						<th>DIARY NO</th>
						<th>CASE YAER</th>
						<th>DOCUMENT NAME</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${freshCaseDetail.caseType}</td>
						<td>${freshCaseDetail.autoNo}</td>
						<td>${freshCaseDetail.diaryNo}</td>
						<td>${freshCaseDetail.year}</td>
						<td>${freshCaseDetail.documentName}</td>
					</tr>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="8" class="text-danger"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</c:if>
			</div>
			<div class="col-sm-2"></div>
		</div>
		</form:form>
	</div>	
	</div>
</div>
</section>
<c:if test="${not empty successMessage}">
	<script type="text/javascript">
		$(function() {
			 $('#scanningsuccessmodal').modal('show');
		});
	</script>
</c:if>
<div class="modal fade centered-modal" id="scanningsuccessmodal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-body">
         <p class="text-success">SCANNING DONE SUCCESSFULLY</p>
         <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
</div>
<footer>
<jsp:include page="../common/footer.jsp"></jsp:include>
</footer>
</body>
</html>
 --%>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.in-width{
	width: 100%!important;
}
</style>
<script type="text/javascript">



function clearDocType(){
	console.log("method calleddddddddddddddd");
//	var ele =document.getElementsByName("docType");
	/* for(i =0 ; i < ele.length ; i++){
		console.log("method calleddddddddddddddd",ele[i].checked);
		ele[i].checked =false;
	} */
	
	 document.getElementById("documentType1").checked = false;
	 document.getElementById("documentType2").checked = false;
	 document.getElementById("documentType3").checked = false;
	
}
$(function() {
	 $('#invoiceId').focusout(function(){
		 var autoNo =document.getElementById("invoiceId").value;
		 console.log(autoNo);
		if(autoNo && !isNaN(autoNo)) {
		// $("#auto-no").empty();
		// if($(this).prop("checked")==true){
		    $.ajax({
		    	type :'POST',
				url : '${pageContext.request.contextPath}/getFreshCaseTypeByAutoNo',
				data : {
					autoNo : autoNo
				},
				dataType:'json',
				success : function(result) {
					
					console.log("sssssssssssss",result);
					if(result != "not found" ){
						var type1 =  document.getElementById("caseType1").value;
						var type2 =  document.getElementById("caseType2").value;

						var type3 =  document.getElementById("caseType3").value;

						var type4 =  document.getElementById("caseType4").value;
						var type5 =  document.getElementById("caseType5").value; 

						console.log("sssssssssssss",type1);
						console.log("sssssssssssss",type2);

						console.log("sssssssssssss",type3);

						console.log("sssssssssssss",type4);
						 console.log("sssssssssssss",type5); 


						var freshCaseType =result.caseType;
						
						if(freshCaseType === type1 )
						  document.getElementById("caseType1").checked = true;
						else if(freshCaseType === type2)
							  document.getElementById("caseType2").checked = true;
						else if(freshCaseType === type3)
							  document.getElementById("caseType3").checked = true;
						 else if(freshCaseType === type4)
							  document.getElementById("caseType4").checked = true;
				 		  else if(freshCaseType === type5)
							  document.getElementById("caseType5").checked = true; 
						/* else 
							  document.getElementById("caseType4").checked = true; */ 
						}else
							{
							$('#autono-msg1').html("Auto No Not Found..Please check");
							}

						// var checkBox = document.getElementById("myCheck");
						  // Get the output text
						  /* var text = document.getElementById("text");

						  // If the checkbox is checked, display the output text
						  if (checkBox.checked == true){
							  document.getElementById('searchCase').style.display = 'none';
								document.getElementById('saveDak').style.display = 'block';
						  } else {
							  document.getElementById('searchCase').style.display = 'block';
								document.getElementById('saveDak').style.display = 'none';
						  }  */	
						
					
					/* alert(result);
					$.each(result, function(index, value) {
						console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaa",value);
						$("#auto-no").append("<option value='"+value.autoNo+"'>"+value.autoNo+"</option>"); 
						console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaa",value);
						
					}); */
				}
			});
		// }
	 }
		else{
			$('#autono-msg').html("Not valid auto no");
			
		}
	  });
	 
	 $('#invoiceId').change(function(){
		 var invoiceId=$('#invoiceId').val();
		 if(invoiceId!=""){
			    $.ajax({
			    	type :'POST',
					url : '${pageContext.request.contextPath}/validateAutoNo',
					data : {
						invoiceId : invoiceId
					},
					success : function(result) {
						if(result=="SUCCESS"){
							/* $('#autono-msg').html("");
							}else{
							$('#autono-msg').html("Not valid auto no");
							$('#invoiceId').val(""); */
							
						}
					}
				});
			}
	  });
});


$(function() {
	 $('#invoiceId').focus(function(){
		 var autoNo =document.getElementById("invoiceId").value;
		 console.log(autoNo);
		
			$('#autono-msg').html("");
			$('#autono-msg1').html("");
			
			
		
	  });
	 
	 $('#invoiceId').change(function(){
		 var invoiceId=$('#invoiceId').val();
		 if(invoiceId!=""){
			    $.ajax({
			    	type :'POST',
					url : '${pageContext.request.contextPath}/validateAutoNo',
					data : {
						invoiceId : invoiceId
					},
					success : function(result) {
						if(result=="SUCCESS"){
							/* $('#autono-msg').html("");
							}else{
							$('#autono-msg').html("Not valid auto no");
							$('#invoiceId').val(""); */
							
						}
					}
				});
			}
	  });
});






function getFileData(myFile){
   var file = myFile.files[0];  
   var filename = file.name.split(".")[0];
   var res = filename.split("-");
   $("#dairy-no").val(res[0]);
   $("#year").val(res[1]);
}
</script>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
	        <li class="breadcrumb-item active">FRESH CASE SCANNING</li>
   		</ol>
   		<form:form autocomplete="off" action="${pageContext.request.contextPath}/saveFreshCaseDocument" modelAttribute="freshCase" method="post" enctype="multipart/form-data">
		<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
		<div class="panel panel-warning panel-body">
		<div class="row">
		<div class="col-sm-6">
		<div class="panel panel-warning panel-body">
			<table class="table table-condensed borderless">
				<tbody> 
				    <tr><td class="required">CASE TYPE<td></tr>
					<tr>
						<td>
							<div class="checkbox">
								<c:forEach var="lookup" items="${lookupList}">
									<c:if test="${lookup.lookupName eq 'FRESH_CASE_TYPE'}">
									<label><form:radiobutton path="caseType" class="case-type" value="${lookup.lookupValue}"/> ${lookup.lookupValue}</label>
									</c:if>
								</c:forEach>
							</div>
							<form:errors path="caseType" class="text-danger"></form:errors>
						</td>
						</tr>
				</tbody>
			</table>
			</div>
			<div class="panel panel-warning panel-body">
			<table class="table table-condensed borderless">
				<tbody> 
				    <tr><td>DOCUMENT TYPE<td></tr>
					<tr>
						<td>
							<div class="checkbox">
								<c:forEach var="lookup" items="${lookupList}">
									<c:if test="${lookup.lookupName eq 'FRESH_DOCUMENT_TYPE'}">
									<label><form:radiobutton path="documentType"  name ="docType" class="case-type" value="${lookup.lookupValue}" /> ${lookup.lookupValue}</label>
									</c:if>
								</c:forEach>
								&nbsp
								&nbsp
								<input type ="button" name="clear" id ="clearType" value ="clear" onclick="clearDocType();">
							</div>
							<form:errors path="caseType" class="text-danger"></form:errors>
						</td>
						</tr>
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-sm-6">
		<table class="table table-condensed borderless">
			<tbody> 
				<tr><td colspan="2"></td></tr>
				<tr>
				<td class="label-text required">AUTO NO</td>
				<td>
				    <form:input path="autoNo" id="invoiceId" list="auto-no"  class="form-control input-sm input-width"/>
				   <!--  <datalist id="auto-no"></datalist> -->
				    <form:errors path="autoNo" class="text-danger"></form:errors>
				    <span id="autono-msg" class="text-danger"></span>
				    <span id="autono-msg1" class="text-danger"></span>
				    
				</td>
				</tr>
				<%-- <tr>
					<td align="right" class="label-text required">
						<form:label path="docFile">SELECT DOCUMENT</form:label>
					</td>
					<td>
						<form:input type="file" id="cfile" path="docFile" onchange="getFileData(this);validateFileExtension(this);"/> 
						<form:errors path="docFile" class="text-danger"></form:errors>
					</td>
				</tr> --%>
				<tr>
					<td class="label-text required">DIARY NO</td>
					<td>
					    <form:input path="diaryNo" id="dairy-no" class="form-control input-sm input-width"/>
					    <form:errors path="diaryNo" class="text-danger"></form:errors>
					</td>
				</tr>
				<tr>
					<td class="label-text required">YEAR</td>
					<td>
						<form:input path="year" id="year" readonly ="false" class="form-control input-sm input-width"/>
						 <form:errors path="year" class="text-danger"></form:errors>
					</td>
				</tr>
				<tr><td colspan="2"></td></tr>
				<tr><td colspan="2"></td></tr>
				<tr> 
					<td></td>
				 	<td colspan="2" >
				 	<button type="submit" class="form-control btn-sm btn-primary btn-block">ACCEPT CASE</button>
				 	</td>
				 	 </tr>
				 	 <tr><td colspan="2"></td></tr>
			
				 	 <tr>
				 	 <td></td>
				 	<td colspan="2">
				 	<a class=" form-control  btn-sm btn-warning btn-block " style=" text-align:center;" role="button" href="${pageContext.request.contextPath}/freshCaseScanning">RESET</a>
				 	</td>
				</tr>
			</tbody>
		</table>
		</div>
		</div>
			</div>
			</form:form>
			
			<div class="panel panel-warning panel-body">
		<form autocomplete="off" class="form-inline" action="${pageContext.request.contextPath}/getFreshCaseScanByDiaryNo" method="post">
		<table class="table table-condensed borderless">
		<tbody> 
			
			<tr>
				<td align="right" class="label-text required">
					<label for="diaryNo">DIARY NUMBER</label>
				</td>
				<td>
					<input type="text" name="diaryNo" id ="diaryNo" class="form-control input-sm in-width" required="required"/>
				</td>
				<td align="right" class="label-text required">
					<label for="year">YEAR</label>
				</td>
				<td>
					<input type="text" name="year"  id ="year" class="form-control input-sm in-width"  required="required"/>
				</td>
				
				
				
				
				<td><button type="submit" id ="searchCase" class="form-control btn-sm btn-block btn-success in-width">SEARCH</button>
			
			</tr>
			</tbody>
			</table>
			</form>
			</div>
				<c:if test="${not empty freshCaseDetail}">
				<div class="panel panel-warning panel-body">
				<table class="table table-condensed table-bordered">
				<thead>
					<tr class="bg-primary">
						<th>CASE TYPE</th>
						<th>AUTO NO</th>
						<th>DIARY NO</th>
						<th>CASE YAER</th>
						<th>DOCUMENT TYPE</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${freshCaseDetail.caseType}</td>
						<td>${freshCaseDetail.autoNo}</td>
						<td>${freshCaseDetail.diaryNo}</td>
						<td>${freshCaseDetail.year}</td>
						<td>${freshCaseDetail.documentType}</td>
					<%-- 	<td>			<a href="<c:url var="${pageContext.request.contextPath}/action" >
            <c:param name="command" value="load"/>
            <c:param name="id" value="${pendingCase.id}"/>
        </c:url>">TEST</a></td> --%>
        <%-- <td>			<a href="<c:url var="${pageContext.request.contextPath}/action" >
            <c:param name="command" value="load"/>
            <c:param name="id" value="${freshCaseDetail.id}"/>
        </c:url>">TEST</a></td> --%>
       <td> <a class=" form-control  btn-sm btn-primary btn-block " style=" text-align:center;" role="button" href="${pageContext.request.contextPath}/acceptFreshCase?id=<c:out value="${freshCaseDetail.id}"/>">Done</a> </td>
        
					</tr>
				</tbody>
				<tfoot>
					<tr class="bg-primary">
					<td colspan="8" class="text-danger"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</c:if>
			</div>
			<div class="col-sm-2"></div>
		</div>
		
		<%-- </form:form> --%>
	</div>	
	</div>
</div>
</section>
<c:if test="${not empty successMessage}">
	<script type="text/javascript">
		$(function() {
			 $('#scanningsuccessmodal').modal('show');
		});
	</script>
</c:if>
<div class="modal fade centered-modal" id="scanningsuccessmodal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-body">
         <p class="text-success">${successMessage}</p>
         <button type="button" class="btn btn-default btn-sm"  data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
</div>
<footer>
<%-- <jsp:include page="../common/footer.jsp"></jsp:include> --%>
</footer>
</body>
</html>