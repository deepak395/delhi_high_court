<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="author" content="Rajkumar Giri">
<title>Fresh case</title>
<jsp:include page="../common/resourcesLib.jsp"></jsp:include>
<style type="text/css">
.input-width{
	width:100%!important;
}
</style>
</head>
<body>
<header>
<jsp:include page="../common/header.jsp"></jsp:include>
</header>
<section class="cantent">
<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
	<ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">HOME</a></li>
        <li class="breadcrumb-item active">FRESH CASE ENTRY</li>
   	</ol>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
		 <div class="panel panel-body panel-warning clearfix">
			<form autocomplete="off" action="${pageContext.request.contextPath}/searchFreshCase" method="post" onsubmit="return freshSearchValidate()" >
				<table class="table table-condensed borderless clearfix">
					 <tr>
					 	<td>AUTO NO</td>
					 	<td><input type="text" name="autoNo" id="autono" placeholder="AUTO NO" class="form-control input-sm input-width"/></td>
					 	<td>DIARY NO</td>
					 	<td><input type="text" name="diaryNo" id="diaryno" placeholder="DIARY NO" class="form-control input-sm input-width"/></td>
					 	<td>YEAR</td>
					 	<td><input type="text" name="year" id="year" placeholder="YEAR" class="form-control input-sm input-width"/></td>
						<td colspan="2" align="center">
							<button class="btn btn-sm btn-primary btn-block form-btn input-width" type="submit">SEARCH</button>
						</td>
					</tr>
			  </table>
			</form>
			</div>
			<table class="table table-condensed table-bordered table-striped">
				<thead>
					<tr class="bg-primary">
						<th>AUTO NO</th>
						<th>CLIENT NAME</th>
						<th>CASE NO</th>
						<th>CASE TYPE</th>
						<th>NO OF PAGE</th>
						<th>AMOUNT</th>
						<th>DATE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="invoiceDetail" items="${invoiceDetailList}">
					<tr> 
						<td>${invoiceDetail.autoNo}</td>
						<td>${invoiceDetail.clientName}</td>
						<td>${invoiceDetail.caseNo}</td>
						<td>${invoiceDetail.caseType}</td>
						<td>${invoiceDetail.noOfPage}</td>
						<td>${invoiceDetail.amount}</td>
						<td><fmt:formatDate pattern="dd-MM-yyyy" value="${invoiceDetail.createdOn}" /></td>
					</tr>
					</c:forEach>
				</tbody>
				<tfoot>
				<tr class="bg-primary">
					<td>Total: ${invoiceDetailList.size()}</td>
					<td></td>
					<td colspan="5"></td>
		       	</tr>
				</tfoot>
			</table>
			<c:if test="${not empty invoiceDetailList}">
			<c:set var="pageNum"  value="${page}"></c:set>
			<ul class="pager">
			    <li class="previous"><a href="${pageContext.request.contextPath}/getAllPendingCaseList?pageNo=${pageNum-1}">Previous</a></li>
			    <li class="next"><a href="${pageContext.request.contextPath}/getAllPendingCaseList?pageNo=${pageNum+1}">Next</a></li>
		    </ul>
		    </c:if>
		</div>
		<div class="col-sm-1"></div>
	</div>	
 </div>
</div>
</div>
</section>
</body>
</html>