 $(document).ready(function() {
         function disablePrev() { window.history.forward() }
         window.onload = disablePrev();
         window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
      });

window.onload = function () {  
    document.onkeydown = function (e) {  
        return (e.which || e.keyCode) != 116;  
    };  
}  

$(function() {   
	$('input').keyup(function(){
	       $(this).val($(this).val().toUpperCase());
	});
});

function checkAllNumeric(inputtxt){
	   var numbers = /^[0-9]+$/;
	   if(!inputtxt.value.match(numbers)){
		   document.getElementById(inputtxt.id+"-msg").innerHTML = "Only numeric allowed";
		   document.getElementById(inputtxt.id).value = "";
	   }else{
		   document.getElementById(inputtxt.id+"-msg").innerHTML = "";
	   }
} 

function checkAllCharacter(inputtxt){
	var letters = /^[a-zA-Z ]*$/;
   if(!inputtxt.value.match(letters)){
	   document.getElementById(inputtxt.id+"-msg").innerHTML = "Only character allowed";
	   document.getElementById(inputtxt.id).value = "";
   }else{
	   document.getElementById(inputtxt.id+"-msg").innerHTML = "";
   }
} 

function validateFileExtension(e) {
	var fup = document.getElementById(e.id);
	var fileName = fup.value;
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
	if(ext == "pdf"){
		return true;
	}else{
		alert("Upload pdf file only");
		fup.focus();
		fup.value="";
		return false;
	}
    return true;
}

/*$(document).ready(function() {
    $('#example').DataTable({
         scrollCollapse: true,
         paging:         false,
    	}
    );
} );*/
$(document).ready(function() {
    $('#example').DataTable();
} );

$(document).ready( function () {
    $('#myTable').DataTable();
} );