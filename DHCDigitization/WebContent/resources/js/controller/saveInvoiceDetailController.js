var DhcApp = angular.module("dhcApp", ['ngMessages']);

DhcApp
		.controller(
				"saveInvoiceDetailCtrl",
				function($scope, $http) {
					$scope.pendingCase = {};
					$scope.years=[];
					$scope.pendingCaseList =[];
					$scope.yearVerify =false;
					var currentYear=new Date().getFullYear();
					
					$scope.caseType ='';
					$scope.caseCategory ='';
					$scope.invoiceDetail ={};
					$scope.isNumber =false;

					var baseUrl = "/DHCDigitization/";

					$scope.caseCategoryOptions = [];
					$scope.caseTypeoptions = [];
					$scope.paymentModeoptions = [];

					//	$scope.selectedOption = $scope.options[1];			
					/*getYears();*/
				//	getCaseCategory();
					getCaseType();
					getPaymentModeList();
				/*	getPendingCaseObject();*/
				//	getLastPendingCases()
					
					
				
					
				$scope.returnToTab1=function(){
						document.getElementById("caseType").focus();
						
					}
				
				$scope.checkCase =function(caseNo){
					if(caseNo){
					}else{
						document.getElementById("case-no-msg").innerHTML = "PLEASE ENTER CASE NO";
					}
				}
				$scope.txNoDisabled=true;
				$scope.checkPaymentMode =function(paymentMode){
					if(paymentMode){
						if(paymentMode!='CASH'){
							$scope.txNoDisabled=false;
							$scope.invoiceDetail.transactionNo=''
						}else{
							$scope.txNoDisabled=true;
							$scope.invoiceDetail.transactionNo='NA';
						}
					}else{
						document.getElementById("payment-mode-msg").innerHTML = "PLEASE ENTER PAYMENT MODE";
					}
				}
				
				$scope.clearPaymentMode =function(paymentMode){
					document.getElementById("payment-mode-msg").innerHTML = "";
				}
				
				$scope.clearCase =function (caseNo){
					document.getElementById("case-no-msg").innerHTML = "";
				}
				
				$scope.checkClientName =function(clientName){
					if(clientName){
						
					}else{
						document.getElementById("client-name-msg").innerHTML = "PLEASE ENTER CLIENT NAME";
					}
				}
				
				$scope.clearClientName =function (clientName){
					document.getElementById("client-name-msg").innerHTML = "";
				}
				
				$scope.checkNoOfPage =function(noOfPage){
					if(noOfPage){
						if(!isNaN(noOfPage)){
						$scope.isNumber =true;
						}else{
							document.getElementById("no-of-page-msg").innerHTML = "PLEASE ENTER NUMERIC VALUES";
						}
						
					}else{
						document.getElementById("no-of-page-msg").innerHTML = "PLEASE ENTER NO OF PAGES";
					}
				}
				
				$scope.clearNoOfPage =function (noOfPage){
					document.getElementById("no-of-page-msg").innerHTML = "";
				}
				
				$scope.checkCaseType =function(caseType){
					if(caseType){
						
					}else{
						document.getElementById("case-type-msg").innerHTML = "PLEASE SELECT CASE TYPE";
					}
				}
				
				$scope.clearCaseType =function (caseType){
					document.getElementById("case-type-msg").innerHTML = "";
				}
				
				/*$scope.clearCaseType =function (caseType){
					document.getElementById("case-type-msg").innerHTML = "";
				}*/
				
					
					function getYears()
					{
						var currentYear=new Date().getFullYear();
						for(var year=1960;year<=currentYear;year++)
						{
							$scope.years.push(year);
						}
					} 
					
		
					
					function getCaseType() {
						var response = $http.get(baseUrl + 'getFreshCaseType');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										
										for(var i =0; i<data.length ;i++){
										$scope.caseTypeoptions.push(data[i].lookupValue);
										
										
									}
										

									}
								});

					}
					
					
					
					function getPaymentModeList() {
						var response = $http.get(baseUrl + 'getPaymentModeList');
						response.success(function(data, status, headers, config) {
							if (data) {
								for(var i =0; i<data.length ;i++){
								$scope.paymentModeoptions.push(data[i].lookupValue);
							}
						}
						});
						$scope.invoiceDetail.paymentMode="CASH";
					}
					
					function getPendingCaseObject() {

						var response = $http.get(baseUrl + 'getPendingCaseObject');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										$scope.caseTypeoptions = data;
										

									}
								});

					}
					

					// function to do add one more field

					// function to save Meta Data
					
					$scope.saveInvoiceDetail =function(){
						
						console.log("function calleddddddddddddddddd",$scope.invoiceDetail);
						
						
						
						$scope.invoiceDetail.amount =document.getElementById("amount").value ;
						
						if($scope.invoiceDetail.amount && $scope.invoiceDetail.caseNo 
								&& $scope.invoiceDetail.noOfPage && $scope.invoiceDetail.clientName 
								&& $scope.invoiceDetail.caseType && $scope.invoiceDetail.paymentMode && $scope.invoiceDetail.transactionNo){
							if($scope.isNumber){
						console.log("function calleddddddddddddddddd",$scope.invoiceDetail);
						var response = $http.post(baseUrl
								+ 'saveInvoiceDetails', angular.toJson($scope.invoiceDetail));
						response
								.success(function(data, status, headers, config) {
									
									if (data) {
										console.log("dataaaaaaaaaaaa",data);
										$scope.invoiceDetail ={};
										$scope.invoiceDetail.caseNo ="NA";
										document.getElementById("caseType").focus();
										//window.location.href=baseUrl+"newInvoiceReceipt/"+data.autoNo;
										window.open(baseUrl+"newInvoiceReceipt/"+data.autoNo,'_blank');
										/*$scope.yearVerify =false;
										document.getElementById("regular").checked=false;
										document.getElementById("caseno").focus();
										document.getElementById("hearing-date").disabled = false;
										document.getElementById("subButton").disabled = false;


										$scope.pendingCase ={};
										$scope.pendingCaseList =data;*/
										
										
						/*				alert("Successfully Saved");*/
									

									}
								});
						}
						else {
							alert("PLEASE ENTER NUMERIC VALUES IN NO OF PAGE FIELD");
						}
					}
						else{
							alert("PLEASE FILL ALL THE FIELDS");
						}
						
					}

					$scope.savePendingCase = function(pendingCase) {
                              
                                 $scope.pendingCase=pendingCase;
						
						if($scope.pendingCase.caseType && $scope.pendingCase.caseCategory && $scope.pendingCase.caseYear &&  $scope.pendingCase.caseNo && ($scope.pendingCase.hearingDate || $scope.pendingCase.ifRegulor =='REGULAR') ){
							if($scope.yearVerify){
								document.getElementById("subButton").disabled = true;
						var response = $http.post(baseUrl
								+ 'savePendingDataEntry', angular.toJson($scope.pendingCase));
						response
								.success(function(data, status, headers, config) {
									
									if (data) {
										$scope.yearVerify =false;
										document.getElementById("regular").checked=false;
										document.getElementById("caseno").focus();
										document.getElementById("hearing-date").disabled = false;
										document.getElementById("subButton").disabled = false;


										$scope.pendingCase ={};
										$scope.pendingCaseList =data;
										
										
						/*				alert("Successfully Saved");*/
									

									}
								});
						}
						else{
							document.getElementById("yearvalmsg").innerHTML = "PLEASE CHECK YOUR YEAR";
						}
						}
						else{
							
							alert("please fill all the fields");
						}
					}
					
					
					
					function getLastPendingCases(){
						
						var response = $http.get(baseUrl + 'getLastPendingCases');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										$scope.pendingCaseList = data;
										console.log("casese Typeeeeeeeeee",
												$scope.caseTypeoptions)
										// window.close();

									}
								});
					}

					$scope.addHyphen = function(element) {

						let ele = document.getElementById("hearing-date");
						ele = ele.value.split('-').join('');
						if (ele.length <= 5 && ele) {
							let finalVal = ele.match(/.{1,2}/g).join('-');
							document.getElementById("hearing-date").value = finalVal;
						}
					}
					$scope.inputDisable = function(e) {
						var a = document.getElementById("regular").checked;

						if (a) {
						
							$scope.pendingCase.ifRegulor ='REGULAR';
							
							$scope.pendingCase.hearingDate = null;
							document.getElementById("hearing-date").value = "";
							document.getElementById("hearing-date").disabled = true;
						} else {
						
							$scope.pendingCase.ifRegulor =null;
							$scope.pendingCase.hearingDate = null;
							document.getElementById("hearing-date").value = "";
							document.getElementById("hearing-date").disabled = false;
						}
					}
					$scope.validatedate = function(inputText) {
						var date = $scope.pendingCase.hearingDate
						var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
						if (date.match(dateformat)) {
							/*document.form1.text1.focus();*/
							var opera2 = date.split('-');
							/*lopera1 = opera1.length;*/
							lopera2 = opera2.length;
							if (lopera2 > 1) {
								var pdate = date.split('-');
							}
							var dd = parseInt(pdate[0]);
							var mm = parseInt(pdate[1]);
							var yy = parseInt(pdate[2]);
							if (mm == 1 || mm > 2)
								if (mm == 2) {
									var lyear = false;
									if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
										lyear = true;
									}
									if ((lyear == false) && (dd >= 29)) {
										/*document
												.getElementById("hearing-date-msg").innerHTML = "Invalid date format !";*/
										return false;
									}
									if ((lyear == true) && (dd > 29)) {
		/*								document
												.getElementById("hearing-date-msg").innerHTML = "Invalid date format!";*/
										return false;
									}
								}
						} else {
/*							document.getElementById("hearing-date-msg").innerHTML = "Invalid date format!";
*/
							return false;
						}
					}
					
					
				$scope.yearVal =	function(caseYear){
					document.getElementById("yearvalmsg").innerHTML = "";
					console.log("focus in",caseYear);
					
				}
				
				$scope.yearValFocusOut =	function(caseYear){
					var a ='bilal';
					console.log("condtionnnn of nan",isNaN(a));
					if(a.isNaN){
						console.log("condtionnnn is trueee",a);
					}
					else{
						console.log("condtionnnn is false");
					}
					if($scope.pendingCase.caseYear){
						
						if(!isNaN($scope.pendingCase.caseYear)){
							
						
					console.log("focus out",$scope.pendingCase.caseYear.length);
					if($scope.pendingCase.caseYear.length < 4){
						document.getElementById("yearvalmsg").innerHTML = "PLEASE ENTER FOUR DIGIT IN YEAR";
					}else if($scope.pendingCase.caseYear.length == 4){
						if($scope.pendingCase.caseYear >= 1700 && $scope.pendingCase.caseYear <= currentYear){
							$scope.yearVerify =true;
						}
						else {
							document.getElementById("yearvalmsg").innerHTML = "PLEASE ENTER YEAR IN RANGE";
						}
						
					}
						}
						else {
							
							document.getElementById("yearvalmsg").innerHTML = "PLEASE ENTER NUMERIC VALUES";
							console.log("numberrrr");
						}
					}
					else{
						
					}
					
				}

					function yearValidation(year, ev) {

						var text = /^[0-9]+$/;
						if (ev.type == "blur" || year.length == 4
								&& ev.keyCode != 8 && ev.keyCode != 46) {
							if (year != 0) {
								if ((year != "") && (!text.test(year))) {

									alert("Please Enter Numeric Values Only");
									return false;
								}

								if (year.length != 4) {
									alert("Year is not proper. Please check");
									return false;
								}
								var current_year = new Date().getFullYear();
								if ((year < 1920) || (year > current_year)) {
									alert("Year should be in range 1920 to current year");
									return false;
								}
								return true;
							}
						}
					}
					// function to do validation.

				});
