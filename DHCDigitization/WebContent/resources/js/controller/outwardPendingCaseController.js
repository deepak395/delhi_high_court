var DhcApp = angular.module("dhcApp", ['ngMessages']);

DhcApp
		.controller(
				"outwardPendingCaseCtrl",
				function($scope, $http) {
										
					$scope.outwardCase = {};
					$scope.years=[];
					$scope.pendingCaseList =[];
					$scope.yearVerify =false;
					var currentYear=new Date().getFullYear();
					
					$scope.caseType ='';
					$scope.caseCategory ='';

					var baseUrl = "/DHCDigitization/";

					$scope.caseCategoryOptions = [];
					$scope.caseTypeoptions = [];
					$scope.dateValidate=false;

					//	$scope.selectedOption = $scope.options[1];			
					/*getYears();*/
					getCaseCategory();
					getCaseType();
				/*	getPendingCaseObject();*/
					getLastPendingCases()
					
					
					
				$scope.returnToTab1=function(){
						document.getElementById("caseno").focus();
						   
					}
					
					function getYears()
					{
						var currentYear=new Date().getFullYear();
						for(var year=1960;year<=currentYear;year++)
						{
							$scope.years.push(year);
						}
					} 
					
					function getCaseCategory() {

						var response = $http.get(baseUrl + 'getCaseCategory');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										
												
												
												for(var i =0; i<data.length ;i++){
												$scope.caseCategoryOptions.push(data[i].lookupValue);
												
												
											}
												
	

									}
								});

					}

					function getCaseType() {

						var response = $http.get(baseUrl + 'getCaseType');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										
										for(var i =0; i<data.length ;i++){
										$scope.caseTypeoptions.push(data[i].lookupValue);
										
										
									}
										

									}
								});

					}
					
					function getPendingCaseObject() {

						var response = $http.get(baseUrl + 'getPendingCaseObject');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										$scope.caseTypeoptions = data;
										

									}
								});

					}
					

					// function to do add one more field

					// function to save Meta Data

					$scope.saveOutwardCase = function(outwardCase) {
                              
                                 $scope.outwardCase=outwardCase;
                                 debugger
                              /*   console.log("regular"+$scope.pendingCase.ifRegulor);
                                 console.log("date validation",$scope.dateValidate);*/
								document.getElementById("subButton").disabled = true;
						var response = $http.post(baseUrl
								+ 'saveOutwardDataEntry', angular.toJson($scope.outwardCase));
						response.success(function(data, status, headers, config) {
									if (data) {
										/*$scope.yearVerify =false;
										document.getElementById("regular").checked=false;
										document.getElementById("caseno").focus();
										$scope.dateValidate =false;
										document.getElementById("hearing-date").disabled = false;
										document.getElementById("subButton").disabled = false;
										//$scope.pendingCase ={};
										$scope.pendingCaseList =data;*/
						/*				alert("Successfully Saved");*/
										document.getElementById("subButton").disabled = false;
										alert("Successfully Saved")
									

									}
								});
						}
					
					
					
					function getLastPendingCases(){
						
						var response = $http.get(baseUrl + 'getLastPendingCases');
						response
								.success(function(data, status, headers, config) {
									if (data) {
										$scope.pendingCaseList = data;
										console.log("casese Typeeeeeeeeee",
												$scope.caseTypeoptions)
										// window.close();

									}
								});
					}

					$scope.addHyphen = function(element) {

						let ele = document.getElementById("outward-date");
						ele = ele.value.split('-').join('');
						if (ele.length <= 5 && ele) {
							let finalVal = ele.match(/.{1,2}/g).join('-');
							document.getElementById("outward-date").value = finalVal;
						}
					}
					/*$scope.inputDisable = function(e) {
						var a = document.getElementById("regular").checked;

						if (a) {
						
							$scope.pendingCase.ifRegulor ='REGULAR';
							
							$scope.pendingCase.hearingDate = null;
							document.getElementById("hearing-date").value = "";
							document.getElementById("hearing-date").disabled = true;
						} else {
						
							$scope.pendingCase.ifRegulor =null;
							$scope.pendingCase.hearingDate = null;
							document.getElementById("hearing-date").value = "";
							document.getElementById("hearing-date").disabled = false;
						}
					}*/
					$scope.validatedate = function(inputText) {
						var date = $scope.outwardCase.outward_date
						console.log("dateee",date);
						var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
						if (date.match(dateformat)) {
							
							console.log("dateee",date);
							
							
							
							
							/*document.form1.text1.focus();*/
							var opera2 = date.split('-');
							/*lopera1 = opera1.length;*/
							
							lopera2 = opera2.length;
							if (lopera2 > 1) {
								var pdate = date.split('-');
							}
							var dd = parseInt(pdate[0]);
							var mm = parseInt(pdate[1]);
							var yy = parseInt(pdate[2]);
							
							var d = new Date(yy, mm-1, dd,23,59,58);
							var currentDate= new Date();
							

							
							if (d.getTime() >= currentDate.getTime()) {
								console.log("when date is greater",date);
								$scope.dateValidate =true;
								
								
							}
						    /*else{
						    	document.getElementById("outward-date-msg").innerHTML = "DATE IS LESS THAN THE CURRENT DATE";
						    	  console.log("date is lesser "); 
						    }*/
						    javascript: ; 
							if (mm == 1 || mm > 2)
								if (mm === 2) {
									var lyear = false;
									if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
										lyear = true;
									}
									if ((lyear == false) && (dd >= 29)) {
										document
												.getElementById("outward-date-msg").innerHTML = "Invalid date format !";
										
									}
									if ((lyear == true) && (dd > 29)) {
										document
												.getElementById("outward-date-msg").innerHTML = "Invalid date format!";
									
									}
									
									
								}
						} else {
							document.getElementById("outward-date-msg").innerHTML = "Invalid date format!";

							
						}
					}
					
					$scope.clearvalidatedate =function(outwardDate){
						document.getElementById("outward-date-msg").innerHTML = "";
					}
					
				$scope.yearVal =	function(caseYear){
					document.getElementById("yearvalmsg").innerHTML = "";
					console.log("focus in",caseYear);
					
				}
				
				$scope.yearValFocusOut =	function(caseYear){
					var a ='bilal';
					console.log("condtionnnn of nan",isNaN(a));
					if(a.isNaN){
						console.log("condtionnnn is trueee",a);
					}
					else{
						console.log("condtionnnn is false");
					}
					if($scope.outwardCase.caseYear){
						
						if(!isNaN($scope.outwardCase.caseYear)){
							
						
					console.log("focus out",$scope.outwardCase.caseYear.length);
					if($scope.outwardCase.caseYear.length < 4){
						document.getElementById("yearvalmsg").innerHTML = "PLEASE ENTER FOUR DIGIT IN YEAR";
					}else if($scope.outwardCase.caseYear.length == 4){
						if($scope.outwardCase.caseYear >= 1700 && $scope.outwardCase.caseYear <= currentYear){
							$scope.yearVerify =true;
						}
						else {
							document.getElementById("yearvalmsg").innerHTML = "PLEASE ENTER YEAR IN RANGE";
						}
						
					}
						}
						else {
							
							document.getElementById("yearvalmsg").innerHTML = "PLEASE ENTER NUMERIC VALUES";
							console.log("numberrrr");
						}
					}
					else{
						
					}
					
				}

					function yearValidation(year, ev) {

						var text = /^[0-9]+$/;
						if (ev.type == "blur" || year.length == 4
								&& ev.keyCode != 8 && ev.keyCode != 46) {
							if (year != 0) {
								if ((year != "") && (!text.test(year))) {

									alert("Please Enter Numeric Values Only");
									return false;
								}

								if (year.length != 4) {
									alert("Year is not proper. Please check");
									return false;
								}
								var current_year = new Date().getFullYear();
								if ((year < 1920) || (year > current_year)) {
									alert("Year should be in range 1920 to current year");
									return false;
								}
								return true;
							}
						}
					}
					// function to do validation.

				});
