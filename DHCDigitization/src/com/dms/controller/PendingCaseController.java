package com.dms.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.enumm.CaseStage;
import com.dms.model.Folder;
import com.dms.model.Lookup;
import com.dms.model.OutwardCase;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;
import com.dms.model.User;
import com.dms.service.FolderService;
import com.dms.service.LookupService;
import com.dms.service.PendingCaseDocumentService;
import com.dms.service.PendingCaseService;
import com.dms.utility.FileUtility;
import com.dms.utility.FolderPathUtility;
import com.dms.utility.JsonUtility;
import com.dms.utility.PdfDocUtility;
import com.dms.validator.PendingCaseValidator;
import com.google.gson.Gson;


@Controller
public class PendingCaseController {
	
	private static Logger logger=Logger.getLogger(PendingCaseController.class);
	
	@Autowired
	PendingCaseService pendingCaseService;
	
	@Autowired
	PendingCaseValidator pendingCaseValidator;
	
	@Autowired
	PendingCaseDocumentService pendingCaseDocumentService;
	
	@Autowired
	LookupService lookupService;
	
	@Autowired
	FolderService folderService;
	
	@Autowired
	FolderPathUtility folderPathUtility;
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/pendingCase", method=RequestMethod.GET)
	public ModelAndView pendingCase(PendingCase pendingCase, HttpSession session) {
		

		logger.info("");
		activityLog.info("pendingCase");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		List<PendingCase> pendingCaseList=pendingCaseService.getAllPendingCaseListByPagination(0, 10);
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCase", pendingCase);
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.setViewName("pendingcase/savePendingCase");
		return mav;
	}
	
	@RequestMapping(value="/getAllPendingCaseListForOutward", method= {RequestMethod.POST})
	public @ResponseBody String getAllPendingCaseListForOutward(@RequestBody String pendindCase,HttpSession session) {
		Gson g = new Gson();
PendingCase pendingCase = g.fromJson(pendindCase, PendingCase.class);
		
		System.out.println("---------------------------------"+pendingCase);
		activityLog.info("Post savePendingCase");
		String response =null;
		activityLog.info("Get getAllPendingCaseListForScanning");
		ModelAndView mav=new ModelAndView();// for multiple records changed by deepak
		/*List<PendingCase> pendingCaseDetail=pendingCaseService.getAllPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());*/
		List<PendingCase> pendingCaseDetail=pendingCaseService.getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit1(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
		if(pendingCaseDetail != null){
		
			
			response =JsonUtility.getJosn(pendingCaseDetail);
		}
		
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value="/getCaseCategory", method=RequestMethod.GET)
	public String getCaseCategory( HttpSession session) {
		
		//List<Lookup> caseCategory =lookupService.getAllLookupListByLookupName("CASE_CATEGORY");
		List<Lookup> caseCategory =lookupService.getAllCaseCategory("CASE_CATEGORY");
		
		System.out.println("dataaaaaaaaaaaa"+caseCategory);


		String response =JsonUtility.getJosn(caseCategory);
		
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value="/getCaseType", method=RequestMethod.GET)
	public String getCaseType( HttpSession session) {
		
		List<Lookup> caseType =lookupService.getAllLookupListByLookupName("PENDING_CASE_TYPE");

		String response =JsonUtility.getJosn(caseType);
		
		return response;
	}
	

	
	/*@RequestMapping(value="/getPendingCaseObject", method=RequestMethod.GET)
	public @ResponseBody PendingCase getPendingCaseObject( HttpSession session) {
		
	   PendingCase pc= new PendingCase();
	   pc.setStatus(true);
		String response =JsonUtility.getJosn(pc);
		
		return new PendingCase();
	}*/
	
	/*@RequestMapping(value="/savePendingCase", method=RequestMethod.GET)
	public ModelAndView getsavePendingCase(HttpSession session) {
		
		activityLog.info("Get savePendingCase");
		ModelAndView mav=new ModelAndView();
		User user = (User) session.getAttribute("user");
		logger.info("Save Pending Case: "+user.getUserFullName());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		List<PendingCase> pendingCaseList=pendingCaseService.getAllPendingCaseListByPagination(0, 10);
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCase", new PendingCase());
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.setViewName("pendingcase/savePendingCase");
		return mav;
	}*/
	
	
	  
	@RequestMapping(value="/savePendingDataEntry", method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	public 	@ResponseBody String getsavePendingCase(@RequestBody String pendindCase,HttpSession session) {
		
		Gson g = new Gson();
		PendingCase pendingCase = g.fromJson(pendindCase, PendingCase.class);
		Date date=null;
		System.out.println("---------------------------------"+pendingCase);
		activityLog.info("Post savePendingCase");
		String response =null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");  
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		User user = (User)session.getAttribute("user");
		logger.info("Save Pending Case:"+user.getUserFullName());
		List<Lookup> lookupList=null;
		List<PendingCase> pendingCaseList=null;
		Date hearing_report=null;
		if(pendingCase.getHearingDate() ==null) {
			pendingCase.setHearingDate(pendingCase.getIfRegulor());
		}
		else {
			 System.out.println(pendingCase.getHearingDate()+"Hearing_Report");
			try {
			 date= formatter.parse(pendingCase.getHearingDate());
			 hearing_report=formatter1.parse(formatter1.format(date));
			}catch(Exception e) {
				e.printStackTrace();
			}
			pendingCase.setHearing_report(hearing_report);
		}
		
		
			pendingCase.setCreatedOn(new Date());
			pendingCase.setCaseStage(CaseStage.PENDINGSCAN.getStage());
			pendingCase.setScanFile(0);
			pendingCase.setQcFile(0);
			pendingCase.setStatus(true);
			pendingCase.setCreatedBy(user.getUserFullName());
			pendingCase.setAcceptedByScanner(false);
			Long id=pendingCaseService.savePendingCase(pendingCase);
			if(id!=null) {
				pendingCase.setPendingCaseId(id);
				
				List<PendingCase> pclist =	pendingCaseService.getAllPendingCaseListByPagination(0,15);
				response =JsonUtility.getJosn(pclist);
			}
		
		
	
		
		return response;
	}
	
	@RequestMapping(value="/getLastPendingCases", method=RequestMethod.GET)
	public 	@ResponseBody String getLastPendingCases(HttpSession session) {
		
		String response =null;
			
				List<PendingCase> pclist =	pendingCaseService.getAllPendingCaseListByPagination(0,15);
				response =JsonUtility.getJosn(pclist);
			
		
		
	
		
		return response;
	}
	
	//by bilal
	@ResponseBody
	@RequestMapping(value="/saveDak", method={RequestMethod.POST,RequestMethod.GET})
	public String saveDak(@RequestParam("caseYear") String caseYear,@RequestParam("caseNo") String caseNo,@RequestParam("caseCategory") String caseCategory, HttpSession session) {
	/*	public ModelAndView saveDak(@RequestParam("pendingcase") PendingCase pendingCase, HttpSession session) {*/
			
		activityLog.info("Post savePendingCase");
		
		
		
	
		if(caseCategory.isEmpty()){
			caseCategory =null;

		}
		if(caseNo.isEmpty()){		
			caseNo =null;
		}
		if(caseYear.isEmpty()){
			caseYear =null;
			}
		
		ModelAndView mav=new ModelAndView();
		User user = (User)session.getAttribute("user");
		logger.info("Save Dak By:"+user.getUserFullName());
		 PendingCase pendingCase =new PendingCase();
	//	List<Lookup> lookupList=null;
		List<PendingCase> pendingCaseList=null;
		
	
		if(caseNo != null  && caseCategory != null  && caseYear != null ){
			
		
			pendingCase.setCreatedOn(new Date());
			pendingCase.setCaseStage(CaseStage.PENDINGSCAN.getStage());
			pendingCase.setScanFile(0);
			pendingCase.setQcFile(0);
			pendingCase.setStatus(true);
			pendingCase.setCreatedBy(user.getUserFullName());
			pendingCase.setHearingDate("REGULAR");
			pendingCase.setCaseType("DAK");
			pendingCase.setAcceptedByScanner(false);
			pendingCase.setCaseCategory(caseCategory);
			pendingCase.setCaseNo(caseNo);
			pendingCase.setCaseYear(caseYear);
			
			Long id=pendingCaseService.savePendingCase(pendingCase);
			if(id!=null) {
				pendingCase.setPendingCaseId(id);
				List<PendingCase> pendingCase2 =new ArrayList<PendingCase>();
				pendingCase2.add(pendingCase);
								List<Lookup> lookupList=lookupService.getAllLookupList();
								mav.addObject("pendingCaseDetail", pendingCase2);
								mav.addObject("pendingCase", new PendingCase());
								mav.addObject("pendingCaseDocument", new PendingCaseDocument());
								mav.addObject("lookupList", lookupList);
								mav.addObject("scanningmsg", "DAK SAVED SUCCESFULLY");
								
							mav.setViewName("pendingcase/pendingCaseScanning");
							return pendingCase.getPendingCaseId().toString();
				
			}
		}
		else {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("pendingCaseDetail", null);
			mav.addObject("pendingCase", new PendingCase());
			mav.addObject("pendingCaseDocument", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("pendingcasemsg", "PLEASE FILL ALL THE THREE FIELDS");
			
		  return "FILL ALL FIELDS";
			
		}
	
		
					return pendingCase.getPendingCaseId().toString();
	}
	
	
	@RequestMapping(value="/getPendingCase", method= {RequestMethod.GET})
	public ModelAndView getPendingCase(@RequestParam("pendingCaseId") Long pendingCaseId, HttpSession session) throws Exception {
		
		System.out.println("pending caseeeeeeeeeeeeeeeeeee"+pendingCaseId);
		
		Long id =pendingCaseId;
		activityLog.info("Get pendingCaseScanningDone");
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		//Long id=null;
		
		User user=(User)session.getAttribute("user");
		logger.info("Get Pending Case: "+user.getUserFullName());
		
		
		
		//List<Lookup> l =lookupService.getAllLookupListByLookupName("SCAN");
		//String destPath =l.get(0).getLookupValue()+File.separator+user.getUsername();
		
		
		
	

		// if the directory does not exist, create it
		
		
		
		
		if(user!=null) {
			PendingCase pendingCase =pendingCaseService.getPendingCaseByPendingCaseId(id);
			//PendingCase pendingCase=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase1.getCaseNo(), pendingCase1.getCaseYear(), pendingCase1.getCaseCategory());
			
			
			    

			       
			    
			
			
			List<PendingCase> pendingCase2 =new ArrayList<PendingCase>();
			pendingCase2.add(pendingCase);
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", pendingCase2);
							mav.addObject("pendingCase", new PendingCase());
							mav.addObject("pendingCaseDocument", new PendingCaseDocument());
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg","DAK SAVED SUCCESSFULLY");
							
						mav.setViewName("pendingcase/pendingCaseScanning");
					
				
	}
		
		return mav;
	
	}
	

	@RequestMapping(value="/savePendingCase", method=RequestMethod.POST)
	public ModelAndView savePendingCase(@ModelAttribute("pendingCase") PendingCase pendingCase, Errors errors, HttpSession session) {
			
		activityLog.info("Post savePendingCase");
		ModelAndView mav=new ModelAndView();
		User user = (User)session.getAttribute("user");
		logger.info("Save Pending Case:"+user.getUserFullName());
		List<Lookup> lookupList=null;
		List<PendingCase> pendingCaseList=null;
		if(pendingCase.getIfRegulor()!=null) {
			pendingCase.setHearingDate(pendingCase.getIfRegulor());
		}
		pendingCaseValidator.validate(pendingCase, errors);
		if(errors.hasErrors()) {
			mav.setViewName("pendingcase/savePendingCase");
		}else {
			pendingCase.setCreatedOn(new Date());
			pendingCase.setCaseStage(CaseStage.PENDINGSCAN.getStage());
			pendingCase.setScanFile(0);
			pendingCase.setQcFile(0);
			pendingCase.setStatus(true);
			pendingCase.setCreatedBy(user.getUserFullName());
			Long id=pendingCaseService.savePendingCase(pendingCase);
			if(id!=null) {
				mav.addObject("pendingCase", new PendingCase());
				mav.addObject("pendingcasemsg", "SAVED SUCCESSFULLY");
			}
		}
		lookupList=lookupService.getAllLookupList();
		pendingCaseList=pendingCaseService.getAllPendingCaseList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.setViewName("pendingcase/savePendingCase");
		return mav;
	}
	
	@RequestMapping(value="/getAllPendingCaseList", method=RequestMethod.GET)
	public ModelAndView getAllPendingCaseList(HttpServletRequest request) {
		
		activityLog.info("Get getAllPendingCaseList");
		ModelAndView mav=new ModelAndView();
		List<PendingCase> pendingCaseList=null;
		int pageNumber=1;
		int maxRecord=10;
		String pageNo=request.getParameter("pageNo");
		if(pageNo!=null) {
			pageNumber=Integer.parseInt(pageNo);
			pendingCaseList=pendingCaseService.getAllPendingCaseListByPagination(pageNumber, maxRecord);
		}else {
			pendingCaseList=pendingCaseService.getAllPendingCaseListByPagination(pageNumber, maxRecord);
		}
		List<PendingCase> list=pendingCaseService.getAllPendingCaseList();
		if (pendingCaseList!=null && list!=null) {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("lookupList", lookupList);
			mav.addObject("pendingCaseList", pendingCaseList);
			mav.addObject("page", pageNumber);
			mav.addObject("list", list);
		}
		mav.setViewName("pendingcase/pendingCaseList");
		return mav;
	}
	
	@RequestMapping(value="/searchPendingCase", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView searchPendingCase(PendingCase pendingCase) {
		
		activityLog.info("Post-Get searchPendingCase");
		ModelAndView mav=new ModelAndView();
		List<PendingCase> pendingCaseList=pendingCaseService.getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		//mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.setViewName("pendingcase/pendingCaseList");
		return mav;
	}
	
	@RequestMapping(value="/editPendingCase", method=RequestMethod.GET)
	public ModelAndView editPendingCase(@RequestParam("pendingCaseId") Long pendingCaseId) {
		
		activityLog.info("Get searchPendingCase");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		List<PendingCase> pendingCaseList=pendingCaseService.getAllPendingCaseList();
		PendingCase pendingCase=pendingCaseService.getPendingCaseByPendingCaseId(pendingCaseId);
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCase", pendingCase);
		mav.setViewName("pendingcase/editPendingCase");
		return mav;
	}
	
	
	@RequestMapping(value="/deletePendingCase", method=RequestMethod.GET)
	public ModelAndView deletePendingCase(@RequestParam("pendingCaseId") Long pendingCaseId) {
		
		activityLog.info("Get deletePendingCase");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
	 List<PendingCase> pendingCaseList=pendingCaseService.getAllPendingCaseList();
		PendingCase pendingCase=pendingCaseService.getPendingCaseByPendingCaseId(pendingCaseId);
		pendingCase.setRemarks("WRONG ENTRY");
	    pendingCase.setStatus(false);	
		pendingCaseService.updatePendingCase(pendingCase);
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCase", pendingCase);
		mav.setViewName("pendingcase/editPendingCase");
		return mav;
	}
	
	
	
	@RequestMapping(value="/updatePendingCase", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView updatePendingCase(@ModelAttribute("pendingCase") PendingCase pendingCase, Errors errors) {
		
		activityLog.info("Get updatePendingCase");
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCases=null;
		List<Lookup> lookupList=null;
		if(pendingCase.getIfRegulor()!=null) {
			pendingCase.setHearingDate(pendingCase.getIfRegulor());
		}
		pendingCaseValidator.validate(pendingCase, errors);
		if(errors.hasErrors()) {
			mav.setViewName("pendingcase/editPendingCase");
		}else {
			pendingCases=pendingCaseService.getPendingCaseByPendingCaseId(pendingCase.getPendingCaseId());
			if(pendingCases!=null) {
				pendingCases.setCaseNo(pendingCase.getCaseNo());
				pendingCases.setCourtNo(pendingCase.getCourtNo());
				pendingCases.setCaseYear(pendingCase.getCaseYear());
				pendingCases.setTotalFile(pendingCase.getTotalFile());
				pendingCases.setCaseCategory(pendingCase.getCaseCategory());
				pendingCases.setCaseType(pendingCase.getCaseType());
				pendingCases.setHearingDate(pendingCase.getHearingDate());
				pendingCases.setRemarks(pendingCase.getRemarks());
				pendingCases.setDealing(pendingCase.getDealing());
				pendingCases.setUpdatedBy(pendingCase.getUpdatedBy());
				pendingCases.setUpdatedOn(new Date());
				boolean flag=pendingCaseService.updatePendingCase(pendingCases);
				if(flag) {
					mav.addObject("pendingCase", new PendingCase());
					mav.addObject("pendingcasemsg", "UPDATED SUCCESSFULLY");
				}
			}
		}
		lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.setViewName("pendingcase/editPendingCase");
		return mav;
	}
	
	@RequestMapping(value="/pendingCaseScanning", method=RequestMethod.GET)
	public ModelAndView pendingCaseScanning() {
		
		activityLog.info("Get pendingCaseScanning");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCaseDocument", new PendingCaseDocument());
		mav.setViewName("pendingcase/pendingCaseScanning");
		return mav;
	}
	
	@RequestMapping(value="/getAllPendingCaseListForScanning", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getAllPendingCaseListForScanning(@ModelAttribute("pendingCase") PendingCase pendingCase) {
		
		activityLog.info("Get getAllPendingCaseListForScanning");
		ModelAndView mav=new ModelAndView();// for multiple records changed by deepak
		List<PendingCase> pendingCaseDetail=pendingCaseService.getAllPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
		if(pendingCaseDetail != null){
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCaseDetail", pendingCaseDetail);
		mav.addObject("pendingCaseDocument", new PendingCaseDocument());
		mav.setViewName("pendingcase/pendingCaseScanning");
		return mav;
		}
		else if(pendingCaseDetail == null){
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("lookupList", lookupList);
			mav.addObject("scanningmsg","CASE NOT FOUND AT PENDING FOR SCAN");
			mav.addObject("pendingCaseDetail", new ArrayList<PendingCase>());
			mav.addObject("pendingCaseDocument", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.setViewName("pendingcase/pendingCaseScanning");
		}
		mav.setViewName("pendingcase/pendingCaseScanning");
		return mav;
	}
	
	
	// BY BILAL
	
	@RequestMapping(value="/pendingCaseScanningDoneNew", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView pendingCaseScanningDoneNew(@ModelAttribute("pendingCaseDocument") PendingCase pendingCaseDocument, HttpSession session) throws Exception {
		
		activityLog.info("Get pendingCaseScanningDone");
		
		System.out.println("dataaaaaaaaaaaaaaaaa"+pendingCaseDocument);
		
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		Long id=null;
		List<PendingCaseDocument> pendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		User user=(User)session.getAttribute("user");
		logger.info("Pending Case Scanning Done New:"+user.getUserFullName());
		if(user!=null) {
			//PendingCase pendingCase1=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategoryAndcaseStageAndcaseType(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory(),pendingCaseDocument.getCaseStage(),pendingCaseDocument.getCaseType());
			PendingCase pendingCase1=pendingCaseService.getPendingCaseByPendingCaseId(pendingCaseDocument.getPendingCaseId());


			
			if(pendingCase1!=null) {
				//Folder folder=folderService.getFolderByFolderName(pendingCase.getCaseType(), "SCAN");
				//if(folder!=null) {
				
					
					pendingCase1.setCaseStage(CaseStage.READYTOMOVESCANNEDFILES.getStage());
					pendingCase1.setScannedBy(user.getUsername());
					pendingCase1.setScannerFullname(user.getUserFullName());
					pendingCase1.setScannedOn(new Date());
					
					
					boolean updated  =pendingCaseService.updatePendingCase(pendingCase1);
					//String path=folderPathUtility.getPendingCaseFolderPath(folder.getFolderId(), caseNo, caseYear, caseCategory);
					
						
					
					if(updated){
							
						
							
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", null);
							//mav.addObject("pendingCaseDocument", pendingCaseDocument);
							mav.addObject("pendingCase", new PendingCase());
						//	mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg", "SCANNING DONE SUCCESSFULLY");
					}
							
							else {
								List<Lookup> lookupList=lookupService.getAllLookupList();
								mav.addObject("pendingCaseDetail", null);
								mav.addObject("scanningmsg","SOME PROBLEM WHILE UPDATING");
								mav.addObject("pendingCase", new PendingCase());
								mav.addObject("pendingCaseDocument", new PendingCaseDocument());
								mav.addObject("lookupList", lookupList);
								
							}
							}
						mav.setViewName("pendingcase/pendingCaseScanning");
					}
				
			
		
		return mav;
	}
	
	
	
	/*@RequestMapping(value="/pendingCaseScanningDoneNew", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView pendingCaseScanningDoneNew(@ModelAttribute("pendingCaseDocument") PendingCaseDocument pendingCaseDocument, HttpSession session) throws Exception {
		
		activityLog.info("Get pendingCaseScanningDone");
		
		System.out.println("dataaaaaaaaaaaaaaaaa"+pendingCaseDocument);
		
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		Long id=null;
		List<PendingCaseDocument> pendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			//PendingCase pendingCase1=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategoryAndcaseStageAndcaseType(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory(),pendingCaseDocument.getCaseStage(),pendingCaseDocument.getCaseType());
			PendingCase pendingCase1=pendingCaseService.getPendingCaseByPendingCaseId(pendingCaseDocument.getDocumentId());


			
			if(pendingCase1!=null) {
				//Folder folder=folderService.getFolderByFolderName(pendingCase.getCaseType(), "SCAN");
				//if(folder!=null) {
				
					
					pendingCase1.setCaseStage(CaseStage.READYTOMOVESCANNEDFILES.getStage());
					pendingCase1.setScannedBy(user.getUsername());
					pendingCase1.setScannerFullname(user.getUserFullName());
					pendingCase1.setScannedOn(new Date());
					
					
					boolean updated  =pendingCaseService.updatePendingCase(pendingCase1);
					//String path=folderPathUtility.getPendingCaseFolderPath(folder.getFolderId(), caseNo, caseYear, caseCategory);
					
						
					
					
							
							
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", pendingCaseDetail);
							//mav.addObject("pendingCaseDocument", pendingCaseDocument);
							mav.addObject("pendingCase", new PendingCase());
						//	mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg", "hello");
							}
						mav.setViewName("pendingcase/pendingCaseScanning");
					}
				
			
		
		return mav;
	}*/
	
	@RequestMapping(value="/pendingCaseScanningDone", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView pendingCaseScanningDone(@ModelAttribute("pendingCaseDocument") PendingCaseDocument pendingCaseDocument, HttpSession session) throws Exception {
		
		activityLog.info("Get pendingCaseScanningDone");
		
		
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		Long id=null;
		List<PendingCaseDocument> pendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		User user=(User)session.getAttribute("user");
		logger.info("Pendig=ng Case Scanning Done :"+user.getUserFullName());
		if(user!=null) {
			PendingCase pendingCase=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory());
			
			if(pendingCase!=null) {
				//Folder folder=folderService.getFolderByFolderName(pendingCase.getCaseType(), "SCAN");
				//if(folder!=null) {
					String caseNo=pendingCase.getCaseNo();
					String caseYear=pendingCase.getCaseYear();
					String caseCategory=pendingCase.getCaseCategory();
					
					pendingCase.setCaseStage(CaseStage.READYTOMOVESCANNEDFILES.getStage());
					pendingCase.setScannedBy(user.getUsername());
					pendingCase.setScannerFullname(user.getUserFullName());
					pendingCase.setScannedOn(new Date());
					
					
					boolean updated  =pendingCaseService.updatePendingCase(pendingCase);
					//String path=folderPathUtility.getPendingCaseFolderPath(folder.getFolderId(), caseNo, caseYear, caseCategory);
					
						
					
					
							
							
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", pendingCaseDetail);
							//mav.addObject("pendingCaseDocument", pendingCaseDocument);
							mav.addObject("pendingCase", new PendingCase());
						//	mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg", "hello");
							}
						mav.setViewName("pendingcase/pendingCaseScanning");
					}
				
			
		
		return mav;
	}
	
	
	// bY BILAL
	@RequestMapping(value="/pendingCaseCreateFolder", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView pendingCaseAcceptScanning(@ModelAttribute("pendingCaseDocument") PendingCase pendingCase1, HttpSession session) throws Exception {
		
		System.out.println("pending caseeeeeeeeeeeeeeeeeee"+pendingCase1);
		activityLog.info("Get pendingCaseScanningDone");
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		Long id=null;
		
		User user=(User)session.getAttribute("user");
		logger.info("Pending Case Create Folder: "+user.getUserFullName());
		
		
		
		List<Lookup> l =lookupService.getAllLookupListByLookupName("SCAN");
		String destPath =l.get(0).getLookupValue()+File.separator+user.getUsername();
		
		
		
		File theDir = new File(destPath);

		// if the directory does not exist, create it
		
		
		
		
		if(user!=null) {
			PendingCase pendingCase =pendingCaseService.getPendingCaseByPendingCaseId(pendingCase1.getPendingCaseId());
			//PendingCase pendingCase=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase1.getCaseNo(), pendingCase1.getCaseYear(), pendingCase1.getCaseCategory());
			
			if(pendingCase != null && pendingCase.getCaseStage().equals("PENDING FOR SCAN")){
				
				try{
					pendingCase.setAcceptedByScanner(true);
					pendingCaseService.updatePendingCase(pendingCase);
					
				}
				catch(Exception e){
					
					
				}
			
			if (theDir.exists()) {
				
				
			    System.out.println("creating directory: " + theDir.getName());
			    
			    String casefolder =theDir+File.separator+pendingCase.getCaseCategory()+"_"+pendingCase.getCaseNo()+"_"+pendingCase.getCaseYear();
			  
			    File caseDir = new File(casefolder);
			    
			    if(!caseDir.exists()){
			    	try{
				    	caseDir.mkdir();
				    	//Long id
				      
				    } 
				    catch(Exception se){
				        logger.info(se);
				    }   
			    	
			    	
			    }
			    

			       
			    
			}
			}
			List<PendingCase> pendingCase2 =new ArrayList<PendingCase>();
			pendingCase2.add(pendingCase);
			if(!pendingCase2.isEmpty()){
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", pendingCase2);
							mav.addObject("pendingCase", new PendingCase());
							mav.addObject("pendingCaseDocument", new PendingCaseDocument());
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg", "PENDING CASE ACCEPTED");
			}
			else {
				List<Lookup> lookupList=lookupService.getAllLookupList();
				mav.addObject("pendingCaseDetail", new ArrayList<PendingCase>());
				mav.addObject("scanningmsg","NOT FOUND");
				mav.addObject("pendingCase", new PendingCase());
				mav.addObject("pendingCaseDocument", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				
			}
							}
						mav.setViewName("pendingcase/pendingCaseScanning");
					
				
			
		
		return mav;
	}
	
	
	/*@RequestMapping(value="/pendingCaseScanningDone", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView pendingCaseScanningDone(@ModelAttribute("pendingCaseDocument") PendingCaseDocument pendingCaseDocument, HttpSession session) throws Exception {
		
		activityLog.info("Get pendingCaseScanningDone");
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		Long id=null;
		List<PendingCaseDocument> pendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			PendingCase pendingCase=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory());
			if(pendingCase!=null) {
				Folder folder=folderService.getFolderByFolderName(pendingCase.getCaseType(), "SCAN");
				if(folder!=null) {
					String caseNo=pendingCase.getCaseNo();
					String caseYear=pendingCase.getCaseYear();
					String caseCategory=pendingCase.getCaseCategory();
					String path=folderPathUtility.getPendingCaseFolderPath(folder.getFolderId(), caseNo, caseYear, caseCategory);
					if(pendingCaseDocument!=null) {
						List<MultipartFile> documentFileList=pendingCaseDocument.getDocFile();
						if (documentFileList!=null) {
							
							
							for (MultipartFile multipartFile : documentFileList) {
								boolean uploadflag=FileUtility.uploadFileToServer(multipartFile, multipartFile.getOriginalFilename(), path);
								if (uploadflag) {
									pendingCaseDocument.setDocumentName(multipartFile.getOriginalFilename());
									pendingCaseDocument.setScanningPage(PdfDocUtility.getPdfPageCount(path, multipartFile.getOriginalFilename()));
									pendingCaseDocument.setScanningFolderId(folder.getFolderId());
									pendingCaseDocument.setCaseNo(pendingCase.getCaseNo());
									pendingCaseDocument.setCaseYear(pendingCase.getCaseYear());
									pendingCaseDocument.setCaseCategory(pendingCase.getCaseCategory());
									pendingCaseDocument.setCaseType(pendingCase.getCaseType());
									pendingCaseDocument.setPendingCaseId(pendingCase.getPendingCaseId());
									pendingCaseDocument.setCaseStage(CaseStage.SCANNED.getStage());
									pendingCaseDocument.setScanningOn(new Date());
									pendingCaseDocument.setScanningBy(user.getUserFullName());
									pendingCaseDocument.setQcDownloaded(false);
									pendingCaseDocument.setStatus(true);
									id=pendingCaseDocumentService.savePendingCaseDocument(pendingCaseDocument);
									if(id!=null) {
										pendingCase.setCaseStage(pendingCaseDocumentService.getPendingCaseDocumentByDocumentId(id).getCaseStage());
										pendingCaseService.updatePendingCase(pendingCase);
										pendingCaseDocumentList.add(pendingCaseDocumentService.getPendingCaseDocumentByDocumentId(id));
										
									}
								}
							}
							
							if(id!=null) {
								pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
							}
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", pendingCaseDetail);
							mav.addObject("pendingCaseDocument", pendingCaseDocument);
							mav.addObject("pendingCase", new PendingCase());
							mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg", "hello");
							}
						mav.setViewName("pendingcase/pendingCaseScanning");
					}
				}
			}
		}
		return mav;
	}*/
	
	@RequestMapping(value="/pendingCaseQc", method=RequestMethod.GET)
	public ModelAndView pendingCaseQc() {
		
		activityLog.info("Get pendingCaseQc");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("documentDetail", new PendingCaseDocument());
		mav.setViewName("pendingcase/pendingCaseQc");
		return mav;
	}
	
	@RequestMapping(value="/getAllPendingCaseListForQc", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getAllPendingCaseListForQc(@ModelAttribute("pendingCase") PendingCase pendingCase) {
		
		activityLog.info("Get getAllPendingCaseListForQc");
		ModelAndView mav=new ModelAndView();
		List<PendingCaseDocument> pendingCaseDocumentList=null;
		List<PendingCase> pendingCaseDetail=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategoryForQc(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
		if(pendingCaseDetail!=null) {
			
		//	pendingCaseDocumentList=pendingCaseDocumentService.getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(caseNo, caseYear, caseCategory, caseStage);
		
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCase", pendingCase);
		mav.addObject("pendingCaseDetail", pendingCaseDetail);
		mav.addObject("pendingCaseDocument", new PendingCaseDocument());
		mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
		mav.setViewName("pendingcase/pendingCaseQc");
		return mav;
		}
		else {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("lookupList", lookupList);
			mav.addObject("pendingCase", pendingCase);
			mav.addObject("scanningmsg","CASE NOT FOUND AT QC");
			mav.addObject("pendingCaseDetail", pendingCaseDetail);
			mav.addObject("pendingCaseDocument", new PendingCaseDocument());
			mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
			mav.setViewName("pendingcase/pendingCaseQc");
			return mav;
		}
	}
	
	/*@RequestMapping(value="/getAllPendingCaseDocumentListForQc", method= RequestMethod.GET)
	public ModelAndView getAllPendingCaseDocumentListForQc(PendingCase pendingCases) {
		
		activityLog.info("Get getAllPendingCaseDocumentListForQc");
		ModelAndView mav=new ModelAndView();
		List<PendingCaseDocument> pendingCaseDocumentList=null;
		if(pendingCases!=null) {
			PendingCase pendingCase=pendingCaseService.getPendingCaseByPendingCaseId(pendingCases.getPendingCaseId());
			if(pendingCase!=null) {
				PendingCase pendingCaseDetail=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
				if(pendingCaseDetail!=null) {
					String caseNo=pendingCaseDetail.getCaseNo();
					String caseYear=pendingCaseDetail.getCaseYear();
					String caseCategory=pendingCaseDetail.getCaseCategory();
					String caseStage=CaseStage.SCANNED.getStage();
					pendingCaseDocumentList=pendingCaseDocumentService.getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(caseNo, caseYear, caseCategory, caseStage);
				}
				List<Lookup> lookupList=lookupService.getAllLookupList();
				mav.addObject("lookupList", lookupList);
				mav.addObject("pendingCase", pendingCase);
				mav.addObject("pendingCaseDetail", pendingCaseDetail);
				mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
				mav.setViewName("pendingcase/pendingCaseQc");
			}
		}
		return mav;
	}*/
	
	@RequestMapping(value="/pendingCaseQcDone", method=RequestMethod.POST)
	public ModelAndView pendingCaseQcDone(@ModelAttribute("pendingCaseDocument") PendingCaseDocument pendingCaseDocument, HttpSession session) throws Exception {
		
		activityLog.info("Pending Case QC Done");
		ModelAndView mav=new ModelAndView();
		PendingCase pendingCaseDetail=null;
		Long id=null;
		List<PendingCaseDocument> pendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		User user=(User)session.getAttribute("user");
		logger.info("Pending Case QC Done: "+user.getUserFullName());
		if(user!=null) {
		
			PendingCase pendingCase=pendingCaseService.getPendingCaseByPendingCaseId(pendingCaseDocument.getDocumentId());
		//	List<PendingCase> pendingCase=pendingCaseService.getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory());
			if(pendingCase!=null) {
				//Folder folder=folderService.getFolderByFolderName(pendingCase.getCaseType(), "SCAN");
				//if(folder!=null) {
					String caseNo=pendingCase.getCaseNo();
					String caseYear=pendingCase.getCaseYear();
					String caseCategory=pendingCase.getCaseCategory();
					
					pendingCase.setCaseStage(CaseStage.READYTOMOVEQCFILES.getStage());
					pendingCase.setQcBy(user.getUsername());
					pendingCase.setQcFullname(user.getUserFullName());
					pendingCase.setQcOn(new Date());
					
					
					boolean updated  =pendingCaseService.updatePendingCase(pendingCase);
											
							List<Lookup> lookupList=lookupService.getAllLookupList();
							mav.addObject("pendingCaseDetail", pendingCaseDetail);
							mav.addObject("pendingCase", new PendingCase());
							mav.addObject("lookupList", lookupList);
							mav.addObject("scanningmsg", "Qc Done Successfully");
							}
			           mav.setViewName("pendingcase/pendingCaseQc");
		}
					
				
			
		
		return mav;
	}
	/*@RequestMapping(value="/pendingCaseQcDone", method=RequestMethod.POST)
	public ModelAndView pendingCaseQcDone(@ModelAttribute("pendingCaseDocument") PendingCaseDocument pendingCaseDocument, HttpSession session) throws Exception {
		
		activityLog.info("Post pendingCaseQcDone");
		ModelAndView mav=new ModelAndView();
		List<PendingCaseDocument> qcpendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		List<PendingCaseDocument> invalidpendingCaseDocumentList=new ArrayList<PendingCaseDocument>();
		String path="";
		boolean flag=false;
		List<PendingCaseDocument> pendingCaseDocumentList=null;
		PendingCaseDocument pendingCaseDocuments=null;
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			String caseNo=pendingCaseDocument.getCaseNo();
			String caseYear=pendingCaseDocument.getCaseYear();
			String caseCategory=pendingCaseDocument.getCaseCategory();
			Folder folder=folderService.getFolderByFolderName(pendingCaseDocument.getCaseType(), "QC");
			if(folder!=null) {
				path=folderPathUtility.getPendingCaseFolderPath(folder.getFolderId(), caseNo, caseYear, caseCategory);
				List<MultipartFile> multipartFileList=pendingCaseDocument.getDocFile();
				if(multipartFileList!=null) {
					for (MultipartFile multipartFile : multipartFileList) {
						pendingCaseDocuments=pendingCaseDocumentService.getPendingCaseDocumentByDocumentName(multipartFile.getOriginalFilename(), caseNo, caseYear, caseCategory);
						if(pendingCaseDocuments!=null) {
							boolean uploadflag=FileUtility.uploadFileToServer(multipartFile, multipartFile.getOriginalFilename(), path);
							if(uploadflag) {
								pendingCaseDocuments.setQcPage(PdfDocUtility.getPdfPageCount(path, multipartFile.getOriginalFilename()));
								pendingCaseDocuments.setQcFolderId(folder.getFolderId());
								pendingCaseDocuments.setCaseStage(CaseStage.QC.getStage());
								pendingCaseDocuments.setQcOn(new Date());
								pendingCaseDocuments.setQcBy(user.getUserFullName());
								flag=pendingCaseDocumentService.updatePendingCaseDocument(pendingCaseDocuments);
								if(flag) {
									qcpendingCaseDocumentList.add(pendingCaseDocuments);
								}
							}
						}else{
							PendingCaseDocument pendingCaseDocument2=new PendingCaseDocument();
							pendingCaseDocument2.setDocumentName(multipartFile.getOriginalFilename());
							invalidpendingCaseDocumentList.add(pendingCaseDocument2);
						}
					}
					PendingCase pendingCaseDetail=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(caseNo, caseYear, caseCategory);
					if(pendingCaseDetail!=null) {
						String caseStage=CaseStage.SCANNED.getStage();
						pendingCaseDocumentList=pendingCaseDocumentService.getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(caseNo, caseYear, caseCategory, caseStage);
						mav.addObject("pendingCaseDocuments", pendingCaseDocuments);
						mav.addObject("pendingCaseDocumentList", pendingCaseDocumentList);
						mav.addObject("qcpendingCaseDocumentList", qcpendingCaseDocumentList);
						mav.addObject("invalidpendingCaseDocumentList", invalidpendingCaseDocumentList);
						mav.addObject("pendingCaseDetail", pendingCaseDetail);
						mav.addObject("pendingCase", new PendingCase());
						mav.setViewName("pendingcase/pendingCaseQc");
					}
				}
			}
		}
		return mav;
	}*/
	/*
	@RequestMapping(value="/downloadPendingCaseDocument", method=RequestMethod.GET)
	public void downloadPendingCaseDocument(@RequestParam("documentId") Long documentId, HttpServletResponse response) {
									
		String destinationPath="";
		activityLog.info("Get downloadPendingCaseDocument");
		PendingCaseDocument pendingCaseDocument=pendingCaseDocumentService.getPendingCaseDocumentByDocumentId(documentId);
		if(pendingCaseDocument!=null) {
			String caseNo=pendingCaseDocument.getCaseNo();
			String caseYear=pendingCaseDocument.getCaseYear();
			String caseCategory=pendingCaseDocument.getCaseCategory();
			destinationPath=folderPathUtility.getPendingCaseFolderPath(pendingCaseDocument.getScanningFolderId(), caseNo, caseYear, caseCategory);
			response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename="+pendingCaseDocument.getDocumentName());
			Path file = Paths.get(destinationPath, pendingCaseDocument.getDocumentName());
	        if (Files.exists(file)){
	             try {
	                 Files.copy(file, response.getOutputStream());
	                 response.getOutputStream().flush();
	                 pendingCaseDocument.setQcDownloaded(true);
		    		 pendingCaseDocumentService.updatePendingCaseDocument(pendingCaseDocument);
	             }
	             catch (Exception ex) {
	                 ex.printStackTrace();
	             }
	        }
		}
	}
	*/
	
	@RequestMapping(value="/downloadPendingCaseDocument", method=RequestMethod.GET)
	public void downloadPendingCaseDocument(@RequestParam("documentId") Long documentId, HttpServletResponse response) {
								
		activityLog.info("downloadPendingCaseDocument");
		String destinationPath="";
		PendingCaseDocument pendingCaseDocument=pendingCaseDocumentService.getPendingCaseDocumentByDocumentId(documentId);
		if(pendingCaseDocument!=null) {
			String caseNo=pendingCaseDocument.getCaseNo();
			String caseYear=pendingCaseDocument.getCaseYear();
			String caseCategory=pendingCaseDocument.getCaseCategory();
			destinationPath=folderPathUtility.getPendingCaseDownloadFolderPath(pendingCaseDocument.getScanningOn(), pendingCaseDocument.getScanningFolderId(), caseNo, caseYear, caseCategory);
			response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename="+pendingCaseDocument.getDocumentName());
			Path file = Paths.get(destinationPath, pendingCaseDocument.getDocumentName());
	        if (Files.exists(file)){
	             try {
	                 Files.copy(file, response.getOutputStream());
	                 response.getOutputStream().flush();
	                 pendingCaseDocument.setQcDownloaded(true);
		    		 pendingCaseDocumentService.updatePendingCaseDocument(pendingCaseDocument);
	             }
	             catch (Exception ex) {
	                 ex.printStackTrace();
	             }
	        }
		}
	}
	@ResponseBody
	@RequestMapping(value="/checkPendingCaseDocument", method=RequestMethod.POST)
	public String checkqcPendingCaseDocument(@RequestParam("documentId") Long documentId) {
		
		String str="";
		activityLog.info("Post checkPendingCaseDocument");
		PendingCaseDocument pendingCaseDocument=pendingCaseDocumentService.getPendingCaseDocumentByDocumentId(documentId);
		if(pendingCaseDocument!=null) {
			if(pendingCaseDocument.getQcDownloaded()) {
				str="true";
			}else {
				str="false";
			}
		}
		return str;
	}
	



@RequestMapping(value="/pendingCaseOutward", method=RequestMethod.GET)
public ModelAndView pendingCaseOutward() {
	
	activityLog.info("Get pendingCaseQc");
	ModelAndView mav=new ModelAndView();
	List<Lookup> lookupList=lookupService.getAllLookupList();
	mav.addObject("lookupList", lookupList);
	mav.addObject("documentDetail", new PendingCaseDocument());
	mav.setViewName("pendingcase/outwardPendingCase");
	return mav;
}


@RequestMapping(value="/saveOutwardDataEntry", method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
public 	@ResponseBody String saveOutwardDataEntry(@RequestBody String outwardCase,HttpSession session) {
	
	Gson g = new Gson();
	OutwardCase outwardCs = g.fromJson(outwardCase, OutwardCase.class);
	
	System.out.println("---------------------------------"+outwardCs);
	activityLog.info("Post savePendingCase");
	String response =null;
	
	User user = (User)session.getAttribute("user");
	logger.info("Save Pending Case:"+user.getUserFullName());
	List<Lookup> lookupList=null;
	List<PendingCase> pendingCaseList=null;
	
	     outwardCs.setCreatedOn(new Date());
	     outwardCs.setCreatedBy(user.getUserFullName());
		Long id=pendingCaseService.saveOutwardCase(outwardCs);
		if(id!=null) {
			response =JsonUtility.getJosn("TRUE");
		}
	
	return response;
}





}
