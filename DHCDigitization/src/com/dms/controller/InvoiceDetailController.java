package com.dms.controller;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.model.DeleteFilesLocal;
import com.dms.model.FreshCase;
import com.dms.model.InvoiceDetail;
import com.dms.model.Lookup;
import com.dms.model.User;
import com.dms.service.DeleteFilesLocalService;
import com.dms.service.FreshCaseService;
import com.dms.service.InvoiceDetailService;
import com.dms.service.LookupService;
import com.dms.service.PendingCaseDocumentService;
import com.dms.utility.JsonUtility;
import com.dms.validator.InvoiceDetailValidator;
import com.google.gson.Gson;
/**
 * 
 * @author Rajkumar Giri
 * @date 03/10/2019
 *
 */
@Controller
public class InvoiceDetailController {
	
	private static Logger logger=Logger.getLogger(InvoiceDetailController.class);
	
	@Autowired
	InvoiceDetailService invoiceDetailService;
	
	@Autowired
	InvoiceDetailValidator invoiceDetailValidator;
	
	@Autowired
	PendingCaseDocumentService documentDetailService;
	
	@Autowired
	LookupService lookupService;
	
	@Autowired
	DeleteFilesLocalService deleteFilesLocalService;
	
	@Autowired
	FreshCaseService freshCaseService;
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/newInvoice", method=RequestMethod.GET)
	public ModelAndView newInvoice(HttpSession session) throws ParseException {
		
		logger.info("View Billing page");
		activityLog.info("newInvoice");
		ModelAndView mav=new ModelAndView();
		User user = (User) session.getAttribute("user");
	    logger.info("New Inovice: "+user.getUserFullName());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("invoiceDetail", new InvoiceDetail());
		mav.addObject("freshDate", 	new Date());
		mav.setViewName("invoicedetail/saveInvoiceDetail");
		return mav;
	}
	
	@RequestMapping(value="/saveInvoiceDetail", method=RequestMethod.GET)
	public ModelAndView getsaveInvoiceDetail(HttpSession session) throws ParseException {
		
		activityLog.info(" Get saveInvoiceDetail");
		ModelAndView mav=new ModelAndView();
		User user = (User) session.getAttribute("user");
	    logger.info("Save Inovice Detail: "+user.getUserFullName());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		
		mav.addObject("lookupList", lookupList);
		mav.addObject("invoiceDetail", new InvoiceDetail());
		mav.addObject("freshDate", 	new Date());
		mav.setViewName("invoicedetail/saveInvoiceDetail");
		return mav;
	}
	
	@RequestMapping(value="/newInvoiceReceipt/{id}", method=RequestMethod.GET)
	public ModelAndView newInvoiceReceipt(@PathVariable("id") Long id, HttpSession session) throws ParseException {
		InvoiceDetail invoiceDetails =null;
		activityLog.info(" Get saveInvoiceDetail");
		ModelAndView mav=new ModelAndView();
		User user = (User) session.getAttribute("user");
	    logger.info("Save Inovice Detail: "+user.getUserFullName());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		invoiceDetails=invoiceDetailService.getInvoiceDetailByAutoNo(id);
		
		mav.addObject("invoiceDetail", invoiceDetails);
		mav.setViewName("freshcaseRecieptView");
	
		return mav;
	}
	
	@ResponseBody
	@RequestMapping(value="/getFreshCaseType", method=RequestMethod.GET)
	public String getFreshCaseType( HttpSession session) {
		
		List<Lookup> caseType =lookupService.getAllLookupListByLookupName("FRESH_CASE_TYPE");
		String response =JsonUtility.getJosn(caseType);
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value="/getPaymentModeList", method=RequestMethod.GET)
	public String getPaymentModeList(HttpSession session) {
		
		List<Lookup> paymentMode=lookupService.getAllLookupListByLookupName("PAYMENT_MODE");
		String response=JsonUtility.getJosn(paymentMode);
		return response;
	}
	
	@RequestMapping(value="/saveInvoiceDetails", method=RequestMethod.POST)
	public @ResponseBody String saveInvoiceDetail(@RequestBody String invoiceDetailstring, HttpSession session) {
		String response = null;
		activityLog.info("Post saveInvoiceDetail");
		InvoiceDetail invoiceDetails =null;
		Gson g = new Gson();
		InvoiceDetail invoiceDetail = g.fromJson(invoiceDetailstring, InvoiceDetail.class);
		Date date=new Date();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		String currentDate=df.format(date);
		User user=(User)session.getAttribute("user");
		logger.info("Save Inovice Detail: "+user.getUserFullName());
		if(user!=null) {
			invoiceDetail.setCreatedOn(date);
			
			//generate unique invoice number
			invoiceDetail.setInvoiceNo(invoiceDetailService.getInvoiceNo());
			invoiceDetail.setCreatedBy(user.getUserFullName());
			invoiceDetail.setStatus(true);
			invoiceDetail.setIsCanceled(false);
			Long id=invoiceDetailService.saveInvoiceDetail(invoiceDetail);
			if(id!=null) {
				invoiceDetails=invoiceDetailService.getInvoiceDetailByAutoNo(id);
				if(invoiceDetails!=null) {
					invoiceDetails.setBillNo(currentDate+"/"+invoiceDetails.getTempBillNo());
					if(invoiceDetailService.updateInvoiceDetail(invoiceDetails)) {
						DeleteFilesLocal deleteFilesLocal=new DeleteFilesLocal();
						deleteFilesLocal.setFileName("invoice.pdf");
						deleteFilesLocal.setIsDeleted(false);
						deleteFilesLocal.setCreatedOn(new Date());
						deleteFilesLocal.setUserId(user.getId());
						deleteFilesLocalService.saveDeleteFileFromScanFolder(deleteFilesLocal);
					}
				}
			
			
		}
		}
		response =JsonUtility.getJosn(invoiceDetails);
		return response;
	}

	/*@RequestMapping(value="/saveInvoiceDetail", method=RequestMethod.POST)
	public ModelAndView saveInvoiceDetail(@ModelAttribute("invoiceDetail") InvoiceDetail invoiceDetail, Errors errors, HttpSession session) {
		
		activityLog.info("Post saveInvoiceDetail");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		Date date=new Date();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		String currentDate=df.format(date);
		User user=(User)session.getAttribute("user");
		logger.info("Save Inovice Detail: "+user.getUserFullName());
		if(user!=null) {
			invoiceDetailValidator.validate(invoiceDetail, errors);
			if(errors.hasErrors()) {
				mav.addObject("lookupList", lookupList);
				mav.addObject("freshDate", 	date);
				mav.setViewName("invoicedetail/saveInvoiceDetail");
			}else {
				invoiceDetail.setCreatedOn(date);
				invoiceDetail.setCreatedBy(user.getUserFullName());
				invoiceDetail.setStatus(true);
				invoiceDetail.setIsCanceled(false);
				Long id=invoiceDetailService.saveInvoiceDetail(invoiceDetail);
				if(id!=null) {
					InvoiceDetail invoiceDetails=invoiceDetailService.getInvoiceDetailByAutoNo(id);
					if(invoiceDetails!=null) {
						invoiceDetails.setBillNo(currentDate+"/"+invoiceDetails.getTempBillNo());
						if(invoiceDetailService.updateInvoiceDetail(invoiceDetails)) {
							DeleteFilesLocal deleteFilesLocal=new DeleteFilesLocal();
							deleteFilesLocal.setFileName("invoice.pdf");
							deleteFilesLocal.setIsDeleted(false);
							deleteFilesLocal.setCreatedOn(new Date());
							deleteFilesLocal.setUserId(user.getId());
							deleteFilesLocalService.saveDeleteFileFromScanFolder(deleteFilesLocal);
							mav.addObject("invoiceDetail", invoiceDetails);
							mav.setViewName("freshcaseRecieptView");
						}
					}
				}
			}
		}
		return mav;
	}*/
	
	@RequestMapping(value="/getAllInvoiceList", method=RequestMethod.GET)
	public ModelAndView getAllInvoiceList(HttpSession session) throws ParseException {
		
		activityLog.info("Get getAllInvoiceList");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Get All Inovice Detail: "+user.getUserFullName());
		if(user!=null) {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			List<InvoiceDetail> invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByUsername(user.getUsername());
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetailList", invoiceDetailList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.setViewName("invoicedetail/invoiceDetailList");
		}
		return mav;
	}
	
	@RequestMapping(value="/searchInvoice", method=RequestMethod.POST)
	public ModelAndView searchInvoice(InvoiceDetail invoiceDetail, HttpSession session) throws ParseException {
		
		activityLog.info("Post searchInvoice");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Search Invoice Detail: "+user.getUserFullName());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		List<InvoiceDetail> invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByAutoNoAndCaseType(invoiceDetail.getAutoNo(), invoiceDetail.getCaseType(), invoiceDetail.getCaseNo());
		mav.addObject("lookupList", lookupList);
		mav.addObject("invoiceDetailList", invoiceDetailList);
		mav.setViewName("invoicedetail/invoiceDetailList");
		return mav;
	}
	
	@RequestMapping(value="/editInvoiceDetail", method= RequestMethod.GET)
	public ModelAndView editInvoiceDetail(@RequestParam("autoNo") Long autoNo, HttpSession session) {
		
		activityLog.info("Get editInvoiceDetail");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Edit Invoice Detail: "+user.getUserFullName());
		if(user!=null) {
			InvoiceDetail invoiceDetail=invoiceDetailService.getInvoiceDetailByAutoNo(autoNo);
			List<Lookup> lookupList=lookupService.getAllLookupList();
			List<InvoiceDetail> invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByUsername(user.getUsername());
			mav.addObject("invoiceDetail", invoiceDetail);
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetailList", invoiceDetailList);
			mav.setViewName("invoicedetail/editInvoiceDetail");
		}
		return mav;
	}
	
	@RequestMapping(value="/updateInvoiceDetail", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView updateInvoiceDetail(@ModelAttribute("invoiceDetail") InvoiceDetail invoiceDetail, Errors errors, HttpSession session) {
		
		activityLog.info(" Get Post updateInvoiceDetail");
		ModelAndView mav=new ModelAndView();
		InvoiceDetail invoiceDetails=null;
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		logger.info("Update Invoice Detail: "+user.getUserFullName());
		if(user!=null) {
			invoiceDetailValidator.validate(invoiceDetail, errors);
			if(errors.hasErrors()) {
				mav.addObject("lookupList", lookupList);
				mav.setViewName("invoicedetail/editInvoiceDetail");
			}else {
				invoiceDetails=invoiceDetailService.getInvoiceDetailByAutoNo(invoiceDetail.getAutoNo());
				if(invoiceDetails!=null) {
					invoiceDetails.setCaseType(invoiceDetail.getCaseType());
					invoiceDetails.setClientName(invoiceDetail.getClientName());
					invoiceDetails.setCaseNo(invoiceDetail.getCaseNo());
					invoiceDetails.setNoOfPage(invoiceDetail.getNoOfPage());
					invoiceDetails.setAmount(invoiceDetail.getAmount());
					invoiceDetails.setUpdatedOn(new Date());
					invoiceDetails.setUpdatedBy(user.getUsername());
					boolean flag=invoiceDetailService.updateInvoiceDetail(invoiceDetails);
					if(flag) {
						mav.addObject("invoiceDetail", invoiceDetails);
						mav.setViewName("freshcaseRecieptView");
					}
				}
			}
		}
		return mav;
	}
	
	@RequestMapping(value="/cancelInvoiceDetail", method=RequestMethod.POST)
	public ModelAndView cancelInvoiceDetail(@RequestParam("billNo") String billNo, 
			@RequestParam("canceledRemark") String canceledRemark, HttpSession session) {
		
		activityLog.info("  Post cancelInvoiceDetail");
		ModelAndView mav=new ModelAndView();
		boolean flag=false;
		InvoiceDetail invoiceDetail=null;
		User user=(User) session.getAttribute("user");
		logger.info("Cancel Invoice Detail: "+user.getUserFullName());
		if(user!=null) {
			invoiceDetail=invoiceDetailService.getInvoiceDetailByBillNo(billNo);
			invoiceDetail.setIsCanceled(true);
			invoiceDetail.setCanceledRemark(canceledRemark);
			invoiceDetail.setCanceledBy(user.getUsername());
			invoiceDetail.setCanceledOn(new Date());
			if(invoiceDetail!=null) {
				flag=invoiceDetailService.updateInvoiceDetail(invoiceDetail);
				if (flag) {
					mav.setViewName("redirect:/newInvoice");
				}
			}
		}
		return mav;
	}
	
	@RequestMapping(value="/previousReciept", method=RequestMethod.POST)
	public ModelAndView previousReciept(@RequestParam("billNo") String billNo) {
		
		activityLog.info("  Post previousReciept");
		ModelAndView mav=new ModelAndView();
		InvoiceDetail invoiceDetail=invoiceDetailService.getInvoiceDetailByBillNo(billNo);
		if(invoiceDetail!=null) {
			mav.addObject("invoiceDetail", invoiceDetail);
			mav.setViewName("freshcaseRecieptView");
		}else {
			mav.addObject("reportmessage", "RECORD NOT FOUND");
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.addObject("freshDate", 	new Date());
			mav.setViewName("invoicedetail/saveInvoiceDetail");
		}
		return mav;
	}
	
	@ResponseBody
	@RequestMapping(value="/calculateAmount", method=RequestMethod.POST)
	public String calculateAmount(@RequestParam("noOfPage") Integer noOfPage) {
		
		activityLog.info("  Post calculateAmount");
		DecimalFormat decimalFormat = new DecimalFormat("0.#####");
		String amont="";
		Integer count=null;
		Double perpageAmt=null;
		Double minpageRate=null;
		Double gstAmt=null;
		if(noOfPage!=null) {
			List<Lookup> rateperpageList=lookupService.getAllLookupListByLookupName("RATE_PER_PAGE");
			List<Lookup> pagecountList=lookupService.getAllLookupListByLookupName("MIN_PAGE_COUNT");
			List<Lookup> minpageRateList=lookupService.getAllLookupListByLookupName("MIN_PAGE_RATE");
			List<Lookup> gstRateList=lookupService.getAllLookupListByLookupName("GST_RATE");
			if(rateperpageList!=null && pagecountList!=null && minpageRateList!=null && gstRateList!=null) {
				Lookup rateperpage=rateperpageList.get(0);
				Lookup pagecount=pagecountList.get(0);
				Lookup minpage=minpageRateList.get(0);
				Lookup gst=gstRateList.get(0);
				if(rateperpage!=null && pagecount!=null &&  minpage!=null) {
					perpageAmt=Double.parseDouble(rateperpage.getLookupValue());
					count=Integer.parseInt(pagecount.getLookupValue());
					minpageRate=Double.parseDouble(minpage.getLookupValue());
					gstAmt=Double.parseDouble(gst.getLookupValue());
					if(noOfPage<=count) {
						amont=decimalFormat.format(Math.ceil(minpageRate));
					}else {
						Double damt=perpageAmt*noOfPage;
						amont = decimalFormat.format(Math.ceil(damt+(damt*gstAmt/100)));
					}
				}
			}
		}
		String totalAmt=JsonUtility.getJosn(amont);
		return totalAmt;
	}
	
	@ResponseBody
	@RequestMapping(value="/getAllInvoiceDetailListByCaseType", method=RequestMethod.POST)
	public String getAllInvoiceDetailListByCaseType(@RequestParam("caseType") String caseType) {
		
		FreshCase freshCase=null;
		
		activityLog.info("  Post getAllInvoiceDetailListByCaseType");
		List<InvoiceDetail> freshCaseList=new ArrayList<InvoiceDetail>();
		List<InvoiceDetail> freshCaseList2=invoiceDetailService.getAllInvoiceDetailListByCaseType(caseType);
		if(freshCaseList2!=null) {
			for (InvoiceDetail invoiceDetail : freshCaseList2) {
				freshCase=freshCaseService.getFreshCaseByAutoNo(invoiceDetail.getAutoNo());
				if(freshCase==null) {
					freshCaseList.add(invoiceDetail);
				}
			}
		}
		String josnList=JsonUtility.getJosn(freshCaseList);
		return josnList;
	}
	
	@ResponseBody
	@RequestMapping(value="/validateAutoNo", method=RequestMethod.POST)
	public String getAllInvoiceDetailListByCaseType(@RequestParam("invoiceId") Long invoiceId) {
		
		String str="";
		activityLog.info("  Post validateAutoNo");
		InvoiceDetail invoiceDetail=invoiceDetailService.getInvoiceDetailByAutoNo(invoiceId);
		if(invoiceDetail!=null) {
			str="SUCCESS";
		}else {
			str="FAIL";
		}
		return str;
	}
	
	
	/*@RequestMapping(value="/getAllInvoiceDetailListByDate", method=RequestMethod.POST)
	public ModelAndView getAllInvoiceDetailListByDate(@RequestParam("fromDate") String formDate, @RequestParam("toDate") String toDate) throws Exception {
		
		activityLog.info("  Post getAllInvoiceDetailListByDate");
		ModelAndView mav=new ModelAndView();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(formDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		List<InvoiceDetail> invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDate(startDate, endDate);
		if(invoiceDetailList!=null) {
			mav.addObject("invoiceDetailList", invoiceDetailList);
			mav.setViewName("invoiceDetailReportView");
		}else {
			mav.addObject("reportmessage", "RECORD NOT FOUND");
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.addObject("freshDate", 	new Date());
			mav.setViewName("invoicedetail/saveInvoiceDetail");
		}
		return mav;
	}*/
	
	
// New Method -- to extract bill report in pdf form	
	@RequestMapping(value="/getAllInvoiceDetailListByDate", method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getAllInvoiceDetailListByDate(@RequestParam("fromDate") String formDate, @RequestParam("toDate") String toDate, HttpSession session) throws Exception {
								
		activityLog.info("getAllInvoiceDetailListByDate");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Get All Invoice Detail List By Date: "+user.getUserFullName());
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(formDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		List<InvoiceDetail> invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDateAndUsername(startDate, endDate,user.getUserFullName());
		if(invoiceDetailList!=null) {
			mav.addObject("invoiceDetailList", invoiceDetailList);
			mav.setViewName("invoiceDetailReportPdfView");
		}else {
			mav.addObject("reportmessage", "RECORD NOT FOUND");
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.addObject("freshDate", 	new Date());
			mav.setViewName("invoicedetail/saveInvoiceDetail");
		}
		return mav;
	}
	
// Commented to extract report in pdf form	
/*	@RequestMapping(value="/getAllCancelInvoiceDetailListByDate", method=RequestMethod.POST)
	public ModelAndView getAllCancelInvoiceDetailListByDate(@RequestParam("fromDate") String formDate, @RequestParam("toDate") String toDate) throws Exception {
		
		activityLog.info("  Post getAllCancelInvoiceDetailListByDate");
		ModelAndView mav=new ModelAndView();
		List<InvoiceDetail> invoiceDetailList=new ArrayList<InvoiceDetail>();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(formDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		List<InvoiceDetail> invoiceDetailList2=invoiceDetailService.getAllCancelledInvoiceDetailListByDate(startDate, endDate);
		if(invoiceDetailList2!=null) {
			for (InvoiceDetail invoiceDetail : invoiceDetailList2) {
				if(invoiceDetail.getIsCanceled()!=null) {
					if(invoiceDetail.getIsCanceled()) {
						invoiceDetailList.add(invoiceDetail);
					}
				}
			}
			if(invoiceDetailList!=null) {
				mav.addObject("invoiceDetailList", invoiceDetailList);
				mav.setViewName("invoiceCancelDetailReportView");
			}
		}else {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("reportmessage", "RECORD NOT FOUND");
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.addObject("freshDate", 	new Date());
			mav.setViewName("invoicedetail/saveInvoiceDetail");
		}
		return mav;
	}

*/

	// New Method -- to extract bill report in pdf form	-- By Sameer on 10-11-2019 
	/*@RequestMapping(value="/getAllCancelInvoiceDetailListByDate", method=RequestMethod.POST)
	public ModelAndView getAllCancelInvoiceDetailListByDate(@RequestParam("fromDate") String formDate, @RequestParam("toDate") String toDate, HttpSession session) throws Exception {
		
		activityLog.info("getAllCancelInvoiceDetailListByDate");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		List<InvoiceDetail> invoiceDetailList=new ArrayList<InvoiceDetail>();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(formDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		List<InvoiceDetail> invoiceDetailList2=invoiceDetailService.getAllInvoiceDetailListByDateAndUsername(startDate, endDate,user.getUsername());
		if(invoiceDetailList2!=null) {
			for (InvoiceDetail invoiceDetail : invoiceDetailList2) {
				if(invoiceDetail.getIsCanceled()!=null) {
					if(invoiceDetail.getIsCanceled()) {
						invoiceDetailList.add(invoiceDetail);
					}
				}
			}
			if(invoiceDetailList!=null) {
				mav.addObject("invoiceDetailList", invoiceDetailList);
				mav.setViewName("invoiceCancelDetailReportPdfView");
			}
		}else {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("reportmessage", "RECORD NOT FOUND");
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.addObject("freshDate", 	new Date());
			mav.setViewName("invoicedetail/saveInvoiceDetail");
		}
		return mav;
	}	*/

	@RequestMapping(value="/getAllCancelInvoiceDetailListByDate", method=RequestMethod.POST)
	public ModelAndView getAllCancelInvoiceDetailListByDate(@RequestParam("fromDate") String formDate, @RequestParam("toDate") String toDate, HttpSession session) throws Exception {
		
		activityLog.info("getAllCancelInvoiceDetailListByDate");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Get All Cancel Invoice Detail List By Date: "+user.getUserFullName());
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(formDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		List<InvoiceDetail> invoiceDetailList2=invoiceDetailService.getAllCancelledInvoiceDetailListByDateAndUsername(startDate, endDate,user.getUserFullName());
		System.out.println("invoiceDetailList2"+invoiceDetailList2);
		if(invoiceDetailList2!=null) {
			
			mav.addObject("invoiceDetailList", invoiceDetailList2);
			mav.setViewName("invoiceCancelDetailReportPdfView");
		}else {
			List<Lookup> lookupList=lookupService.getAllLookupList();
			mav.addObject("reportmessage", "RECORD NOT FOUND");
			mav.addObject("lookupList", lookupList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.addObject("freshDate", 	new Date());
			mav.setViewName("invoicedetail/saveInvoiceDetail");
		}
		return mav;
	}
	
}
