package com.dms.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.model.User;
import com.dms.model.UserRole;
import com.dms.service.UserRoleService;
import com.dms.validator.UserRoleValidator;

@Controller
public class UserRoleController {
	
	private static Logger logger=Logger.getLogger(UserRoleController.class);
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserRoleValidator userRoleValidator;
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/getAllRoleList", method=RequestMethod.GET)
	public ModelAndView getAllRoleList() {
		
		logger.info("");
		activityLog.info("getAllRoleList");
		ModelAndView mav=new ModelAndView();
		List<UserRole> userRoleList=userRoleService.getAllUserRoleList();
		mav.addObject("userRoleList", userRoleList);
		mav.addObject("userRole", new UserRole());
		mav.setViewName("user/userRoleList");
		return mav;
	}

	/*	save user role */
	@RequestMapping(value="/saveUserRole", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView saveClient(@ModelAttribute("userRole") UserRole userRole, Errors errors, HttpSession session) {
		
		activityLog.info("saveUserRole");
		ModelAndView mav=new ModelAndView();
		List<UserRole> userRoleList=null;
		User userDetail=(User)session.getAttribute("user");
		if(userDetail!=null) {
			userRoleValidator.validate(userRole, errors);
			if (errors.hasErrors()) {
				mav.setViewName("user/userRoleList");
			}else {
				userRole.setCreatedOn(new Date());
				userRole.setCreatedBy(userDetail.getUsername());
				userRole.setStatus(true);
				boolean flag=userRoleService.saveOrUpdateUserRole(userRole);
				if (flag) {
					mav.addObject("userRole", new UserRole());
					mav.addObject("successMessage", "UserRole saved");
					mav.setViewName("redirect:/getAllRoleList");
				}
			}
			userRoleList=userRoleService.getAllUserRoleList();
			mav.addObject("userRoleList", userRoleList);
		}
		return mav;
	}
	
	@RequestMapping(value="/editUserRole", method=RequestMethod.GET)
	public ModelAndView viewOrEditUserRole(@RequestParam("roleId") long roleId) {
		
		activityLog.info("editUserRole");
		ModelAndView mav=new ModelAndView();
		UserRole userRole=userRoleService.getUserRoleByRoleId(roleId);
		if(userRole!=null) {
			mav.addObject("userRole", userRole);
		}
		List<UserRole> userRoleList=userRoleService.getAllUserRoleList();
		mav.addObject("userRoleList", userRoleList);
		mav.setViewName("user/userRoleList");
		return mav;
	}


}
