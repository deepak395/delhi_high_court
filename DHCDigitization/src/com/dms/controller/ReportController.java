package com.dms.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.form.PendingCaseDocumentForm;
import com.dms.form.PendingCaseForm;
import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.InvoiceDetail;
import com.dms.model.Lookup;
import com.dms.model.OutwardCase;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;
import com.dms.model.User;
import com.dms.service.AllReportService;
import com.dms.service.FreshCaseService;
import com.dms.service.InvoiceDetailService;
import com.dms.service.LookupService;
import com.dms.service.PendingCaseDocumentService;
import com.dms.service.PendingCaseService;
import com.dms.service.UserService;
import com.dms.utility.FileUtility;

/**
 * 
 * @author Rupnarayan Shahu
 *
 */
@Controller
public class ReportController {
	
	private static Logger logger=Logger.getLogger(ReportController.class);
	
	@Autowired
	private AllReportService allReportService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	InvoiceDetailService invoiceDetailService;
	
	@Autowired
	FreshCaseService freshCaseService;
	
	@Autowired
	LookupService lookupService;
	
	@Autowired
	PendingCaseService pendingCaseService;
	
	@Autowired
	PendingCaseDocumentService pendingCaseDocumentService;
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/getAllReport", method=RequestMethod.GET)
	public ModelAndView getAllReport(HttpSession session) {
		
		logger.info("");
		activityLog.info("getAllReport");
		ModelAndView mav=new ModelAndView();
		List<User> userList=userService.getAllUserList();
		mav.addObject("userList", userList);
		mav.setViewName("report/allReport");
		return mav;
	}
	
	@RequestMapping(value="/searchAllReport", method= {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView searchAllReport(@RequestParam("case") String casename,
			 					   @RequestParam("fromDate") String fromDate, 
			 					   @RequestParam("toDate") String toDate,
			 					   @RequestParam(required=false,defaultValue="") String caseStage,
			 					   @RequestParam("username") String username, HttpServletRequest request) throws Exception {
		activityLog.info("searchAllReport");
		ModelAndView mav=new ModelAndView();
		List<PendingCaseForm> pendingCaseFormList=new ArrayList<PendingCaseForm>();
		PendingCaseForm pendingCaseForm=null;
		List<FreshCaseDocument> freshCaseReportList=null;
		List<PendingCaseDocument> pendingCaseReportList=null;
		List<PendingCase> pendingCaseDataEntryReportList=null;
		List<InvoiceDetail> invoiceDetailList=null;
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(fromDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		if(casename.equals("FRESH")) {
			if(username.equals("ALL")) {
				freshCaseReportList=allReportService.getAllFreshReportListByDateAndStage(startDate, endDate, caseStage);
			}else {
				freshCaseReportList=allReportService.getAllFreshReportListByDateAndStageAndUsername(startDate, endDate, caseStage, username);
			
			}
		}else if(casename.equals("PENDING")){
			if(username.equals("ALL")) {
				pendingCaseReportList=allReportService.getAllPendingCaseDocumentListByDateAndStage(startDate, endDate, caseStage);
			}else {
				pendingCaseReportList=allReportService.getAllPendingCaseDocumentListByDateAndStageAndUsername(startDate, endDate, caseStage, username);
			}
			if(pendingCaseReportList!=null) {
				for (PendingCaseDocument pendingCaseDocument : pendingCaseReportList) {
					pendingCaseForm=new PendingCaseForm();
					pendingCaseForm.setPendingCaseDocument(pendingCaseDocument);
					pendingCaseForm.setPendingCase(pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory()));
					pendingCaseFormList.add(pendingCaseForm);
				}
			}
		}else if(casename.equals("BILL")){
			if(username.equals("ALL")){
				invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDate(startDate, endDate);
			}else{
				invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDateAndUsername(startDate, endDate, username);
			}
		}else if(casename.equals("PENDING DATA ENTRY")){
			if(username.equals("ALL")) {
				pendingCaseDataEntryReportList=allReportService.getAllPendingCaseDataEntryDetailListByDate(startDate, endDate);
			}else {
				pendingCaseDataEntryReportList=allReportService.getAllPendingCaseDataEntryDetailListByDateAndUsername(startDate, endDate,  username);
			}
		}
		List<User> userList=userService.getAllUserList();
		
		mav.addObject("freshCaseReportList", freshCaseReportList);
		mav.addObject("pendingCaseFormList", pendingCaseFormList);
		mav.addObject("invoiceDetailList", invoiceDetailList);
		mav.addObject("pendingCaseDataEntryReportList", pendingCaseDataEntryReportList);
		mav.addObject("userList", userList);
		mav.addObject("casename", casename);
		mav.addObject("caseStage", caseStage);
		mav.addObject("username", username);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.setViewName("report/allReport");
		return mav;
	}
	
	@RequestMapping(value="/exportAllReport", method=RequestMethod.POST)
	public ModelAndView exportAllReport(@RequestParam("casename") String casename, @RequestParam("caseStage") String caseStage,
			 					   @RequestParam("from") String fromDate, @RequestParam("to") String toDate,
			 					  @RequestParam("username") String username,
			 					@RequestParam("action") String action) throws Exception {
		
		System.out.println("action"+action);
		activityLog.info("exportAllReport");
		ModelAndView mav=new ModelAndView();
		List<FreshCaseDocument> freshCaseReportList=null;
		List<PendingCaseForm> pendingCaseFormList=new ArrayList<PendingCaseForm>();
		PendingCaseForm pendingCaseForm=null;
		List<PendingCaseDocument> pendingCaseReportList=null;
		List<PendingCase> pendingCaseDataEntryReportList=null;
		List<InvoiceDetail> invoiceDetailList=null;
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(fromDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		
		if(casename.equals("FRESH")) {
			if(username.equals("ALL")) {
				freshCaseReportList=allReportService.getAllFreshReportListByDateAndStage(startDate, endDate, caseStage);
			}else {
				freshCaseReportList=allReportService.getAllFreshReportListByDateAndStageAndUsername(startDate, endDate, caseStage, username);
			}
			
			FreshCaseDocument freshCase = freshCaseReportList.get(0);
			freshCase.setCaseStage(caseStage);
			freshCaseReportList.set(0, freshCase);
			
			mav.addObject("freshCaseReportList", freshCaseReportList);
			//mav.setViewName("freshCaseDocumentExcelView");
			mav.setViewName("freshcaseReportExcelView");
			//mav.setViewName("freshcaseReportPdfView");
		}else if(casename.equals("PENDING")){
			if(username.equals("ALL")) {
				pendingCaseReportList=allReportService.getAllPendingCaseDocumentListByDateAndStage(startDate, endDate, caseStage);
			}else {
				pendingCaseReportList=allReportService.getAllPendingCaseDocumentListByDateAndStageAndUsername(startDate, endDate, caseStage, username);
			}
			
			
			if(pendingCaseReportList!=null) {
				for (PendingCaseDocument pendingCaseDocument : pendingCaseReportList) {
					pendingCaseForm=new PendingCaseForm();
					pendingCaseForm.setPendingCaseDocument(pendingCaseDocument);
					pendingCaseForm.setPendingCase(pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCaseDocument.getCaseNo(), pendingCaseDocument.getCaseYear(), pendingCaseDocument.getCaseCategory()));
					pendingCaseFormList.add(pendingCaseForm);
				}
			}
			
			PendingCaseDocument pendingCaseDocument = pendingCaseReportList.get(0);
			pendingCaseDocument.setCaseStageReport(caseStage);
			pendingCaseReportList.set(0, pendingCaseDocument);
			
			mav.addObject("pendingCaseFormList", pendingCaseFormList);
			mav.setViewName("pendingcaseReportExcelView");
			
		}else if(casename.equals("BILL")){
			List<Lookup> gstRateList=lookupService.getAllLookupListByLookupName("GST_RATE");
			Lookup lookupGST=gstRateList.get(0);
			if(lookupGST!=null){
				if(action.equals("excel")){
					if(username.equals("ALL")) {
						invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDate(startDate, endDate);
					}else {
						invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDateAndUsername(startDate, endDate, username);
					}
					mav.addObject("invoiceDetailList", invoiceDetailList);
					mav.addObject("lookupGST", lookupGST.getLookupValue());
					mav.setViewName("billReportExcelView");
				}else{
					
					if(username.equals("ALL")) {
						invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDate(startDate, endDate);
					}else {
						invoiceDetailList=invoiceDetailService.getAllInvoiceDetailListByDateAndUsername(startDate, endDate, username);
					}
					mav.addObject("invoiceDetailList", invoiceDetailList);
					mav.addObject("dateRange", fromDate+" TO "+toDate);
					mav.addObject("lookupGST", lookupGST.getLookupValue());
					mav.setViewName("billReportPdfView");
				}
			}
		}else if(casename.equals("PENDING DATA ENTRY")){
			if(username.equals("ALL")) {
				pendingCaseDataEntryReportList=allReportService.getAllPendingCaseDataEntryDetailListByDate(startDate, endDate);
			}else {
				pendingCaseDataEntryReportList=allReportService.getAllPendingCaseDataEntryDetailListByDateAndUsername(startDate, endDate,  username);
			}
			mav.addObject("pendingCaseDataEntryReportList", pendingCaseDataEntryReportList);
			mav.setViewName("pendingDataEntryReportExcelView");
		}
		
		
		return mav;
	}
	
	@RequestMapping(value="/getAllDocumentReport", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getAllDocumentReport(PendingCase pendingCase) {
		
		activityLog.info("getAllDocumentReport");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.setViewName("report/allDocumentReport");
		return mav;
	}
	
	@RequestMapping(value="/getAllFreshCaseDocument", method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getAllFreshCaseDocument(FreshCase freshCase) {
		
		activityLog.info("getAllFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		List<FreshCase> freshCaseList=freshCaseService.getAllFreshCaseListByDiaryNoAndAutoNo(freshCase.getAutoNo(), freshCase.getDiaryNo(), freshCase.getYear());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("freshCaseList", freshCaseList);
		mav.setViewName("report/allDocumentReport");
		return mav;
	}
	
	@RequestMapping(value="/searchAllFreshCaseExcelExport", method=RequestMethod.GET)
	public ModelAndView searchAllFreshCaseExcelExport(@RequestParam Long id) {
		
		activityLog.info("searchAllFreshCaseExcelExport");
		ModelAndView mav=new ModelAndView();
		FreshCase freshCaseList=freshCaseService.getFreshCaseById(id);
		mav.addObject("freshCaseList", Arrays.asList(freshCaseList));
		mav.setViewName("freshCaseDocumentExcelView");
		return mav;
	}
	
	@RequestMapping(value="/getAllPendingCaseDocument", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getAllPendingCaseDocumentReport(PendingCase pendingCase,HttpServletRequest request) {
		activityLog.info("getAllPendingCaseDocument");
		ModelAndView mav=new ModelAndView();
	/*	List<PendingCaseDocumentForm> pendingCaseDocumentFormList=new ArrayList<PendingCaseDocumentForm>();
		PendingCaseDocumentForm pendingCaseDocumentForm=null;*/
		List<PendingCase> pendingcaseList=pendingCaseService.getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
		/*if(pendingcaseDetail!=null){
			String caseNo=pendingcaseDetail.getCaseNo();
			String caseYear=pendingcaseDetail.getCaseYear();
			String caseCategory=pendingcaseDetail.getCaseCategory();
			pendingCaseDocumentForm=new PendingCaseDocumentForm();
			pendingCaseDocumentForm.setPendingCase(pendingcaseDetail);
			pendingCaseDocumentForm.setPendingCaseDocumentList(pendingCaseDocumentService.getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(caseNo, caseYear, caseCategory));
			pendingCaseDocumentFormList.add(pendingCaseDocumentForm);
		}*/
		List<Lookup> lookupList=lookupService.getAllLookupList();
		request.setAttribute("selectedModule", request.getParameter("caseCategory"));
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingcaseList", pendingcaseList);
		mav.setViewName("report/allDocumentReport");
		return mav;
	
	}
	
	@RequestMapping(value="/searchAllPendingCaseExcelExport", method=RequestMethod.GET)
	public ModelAndView searchAllPendingCaseExcelExport(@RequestParam("pendingCaseId") Long pendingCaseId) {
		
		activityLog.info("searchAllPendingCaseExcelExport");
		ModelAndView mav=new ModelAndView();
		List<PendingCaseDocumentForm> pendingCaseDocumentFormList=new ArrayList<PendingCaseDocumentForm>();
		PendingCaseDocumentForm pendingCaseDocumentForm=null;
		PendingCase pendingCase=pendingCaseService.getPendingCaseByPendingCaseId(pendingCaseId);
		if(pendingCase!=null) {
			String caseNo=pendingCase.getCaseNo();
			String caseYear=pendingCase.getCaseYear();
			String caseCategory=pendingCase.getCaseCategory();
			pendingCaseDocumentForm=new PendingCaseDocumentForm();
			pendingCaseDocumentForm.setPendingCase(pendingCase);
			pendingCaseDocumentForm.setPendingCaseDocumentList(pendingCaseDocumentService.getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(caseNo, caseYear, caseCategory));
			pendingCaseDocumentFormList.add(pendingCaseDocumentForm);
		}
		mav.addObject("pendingCaseList", pendingCaseDocumentFormList);
		mav.setViewName("pendingCaseDocumentExcelView");
		return mav;
	}
	
	@RequestMapping(value="/getHearingReport", method=RequestMethod.GET)
	public ModelAndView getHearingReport(HttpSession session) {
		
		activityLog.info("getHearingReport");
		ModelAndView mav=new ModelAndView();
		List<User> userList= userService.getUserListByRole("PENDING DATA ENTRY");
		mav.addObject("userList", userList);
		mav.setViewName("report/hearingReport");
		return mav;
	}
	
	@RequestMapping(value="/getDataEntryReport", method=RequestMethod.GET)
	public ModelAndView getDataEntryReport(HttpSession session) {
		
		activityLog.info("getDataEntryReport");
		ModelAndView mav=new ModelAndView();
		List<User> userList= userService.getUserListByRole("PENDING DATA ENTRY");
		mav.addObject("userList", userList);
		mav.setViewName("report/dataEntryReport");
		return mav;
	}
	
	@RequestMapping(value="/searchDataEntryReport", method= {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView searchDataEntryReport( @RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			 					   @RequestParam(required=false,defaultValue="") String caseStage, @RequestParam("username") String username,
			 					    HttpServletRequest request) throws Exception {
		
		activityLog.info("searchDataEntryReport");
		ModelAndView mav=new ModelAndView();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");  
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		List<PendingCase> scanList=new ArrayList<PendingCase>();
		List<PendingCase> qcList=new ArrayList<PendingCase>();
		List<PendingCase> pendingCaseList=null;
		Date ft_date=formatter.parse(fromDate);
		
		fromDate=formatter1.format(ft_date);
		
           Date dt_date=formatter.parse(toDate);
		   toDate=formatter1.format(dt_date);
           if(caseStage!=null) 
	       pendingCaseList=allReportService.getDataEntryReportByDate(fromDate,toDate,username);
           if(pendingCaseList!=null) {
             for(PendingCase pc: pendingCaseList) {
            	 
            	 if(pc.getScannedOn()!=null) {
            	   scanList.add(pc);
             }
            	 if(pc.getQcOn()!=null) {
              	   qcList.add(pc);
            	 }
             }
           }
         List<User> userList= userService.getUserListByRole("PENDING DATA ENTRY");
   		mav.addObject("userList", userList);
        mav.addObject("scanList", scanList);   
        mav.addObject("qcList", qcList);  
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.addObject("caseStage", caseStage);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.setViewName("report/dataEntryReport");
		return mav;
	}
	
	
	
	
	
	
	
	@RequestMapping(value="/searchHearingReport", method= {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView getBacklogList( @RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,@RequestParam("courtNo") String courtNo,
			 					   @RequestParam(required=false,defaultValue="") String caseStage, @RequestParam("username") String username,
			 					    HttpServletRequest request) throws Exception {
		
		activityLog.info("searchHearingReport");
		ModelAndView mav=new ModelAndView();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");  
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		List<PendingCase> scanList=new ArrayList<PendingCase>();
		List<PendingCase> qcList=new ArrayList<PendingCase>();
		List<PendingCase> pendingCaseList=null;
		Date ft_date=formatter.parse(fromDate);
		
		fromDate=formatter1.format(ft_date);
		
           Date dt_date=formatter.parse(toDate);
		   toDate=formatter1.format(dt_date);
           if(caseStage!=null) 
	       pendingCaseList=allReportService.getAllPendingHearingReportByDate(fromDate,toDate,courtNo,username);
           if(pendingCaseList!=null) {
             for(PendingCase pc: pendingCaseList) {
            	 
            	 if(pc.getScannedOn()!=null) {
            	   scanList.add(pc);
             }
            	 if(pc.getQcOn()!=null) {
              	   qcList.add(pc);
            	 }
             }
           }
         List<User> userList= userService.getUserListByRole("PENDING DATA ENTRY");
   		mav.addObject("userList", userList);
        mav.addObject("scanList", scanList);   
        mav.addObject("qcList", qcList);  
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.addObject("caseStage", caseStage);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.setViewName("report/hearingReport");
		return mav;
	}
	
	
	@RequestMapping(value="/exportHearingReport", method=RequestMethod.POST)
	public ModelAndView exportHearingReport(@RequestParam("caseStage") String caseStage,
			 					   @RequestParam("from") String fromDate, @RequestParam("to") String toDate,@RequestParam("courtNo") String courtNo, @RequestParam("username") String username)
			 					   throws Exception {
		
		activityLog.info("exportHearingReport");
		ModelAndView mav=new ModelAndView();
		List<PendingCase> pendingCaseList=null;
           if(caseStage!=null) 
        	   pendingCaseList=allReportService.getAllPendingHearingReportByDate(fromDate,toDate,courtNo,username);
		mav.addObject("pendingCaseList", pendingCaseList);
			mav.setViewName("HearingDateExcelView");
		
		return mav;
	}
	
	@RequestMapping(value="/getBacklogList", method=RequestMethod.GET)
	public ModelAndView getBacklogList(HttpSession session) {
		
		activityLog.info("getBacklogList");
		ModelAndView mav=new ModelAndView();
		List<User> userList=userService.getAllUserList();
		mav.addObject("userList", userList);
		mav.setViewName("report/backlogReport");
		return mav;
	}
	
	
	
	@RequestMapping(value="/getAllBacklogList", method= {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView getAllBacklogList( @RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,@RequestParam("username") String courtNo,
			 					   @RequestParam(required=false,defaultValue="") String caseStage,
			 					    HttpServletRequest request) throws Exception {
		
		activityLog.info("getAllBacklogList");
		ModelAndView mav=new ModelAndView();
		List<PendingCase> pendingCaseList=null;
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(fromDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		List<User> userList=userService.getAllUserList();
	       pendingCaseList=allReportService.getUserWiseBacklog(startDate,endDate,courtNo);
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.addObject("caseStage", caseStage);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.addObject("userList", userList);
		mav.setViewName("report/backlogReport");
		return mav;
	}
	
	
	@RequestMapping(value="/getOutwardCases", method=RequestMethod.GET)
	public ModelAndView getOutwardCases(HttpSession session) {
		activityLog.info("getOutwardCases");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.setViewName("report/outwardReport");
		return mav;
	}
	
	
	@RequestMapping(value="/searchOutwardCases", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getOutwardCases(OutwardCase outwardCase) {
		activityLog.info("searchOutwardCases");
		ModelAndView mav=new ModelAndView();
		List<OutwardCase> outwardcaseList=pendingCaseService.getAllOutwardCaseByCaseNoAndCaseYearAndCaseCategoryEdit(outwardCase.getCaseNo(), outwardCase.getCaseYear(), outwardCase.getCaseCategory());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("outwardcaseList", outwardcaseList);
		mav.setViewName("report/outwardReport");
		return mav;
	
	}
/*	@RequestMapping(value="/getPendingDocuments", method=RequestMethod.GET)
	public ModelAndView getPendingDocuments(HttpSession session) {
		activityLog.info("getOutwardCases");
		ModelAndView mav=new ModelAndView();
		mav.setViewName("report/getPendingDocuments");
		return mav;
	}*/
	
	
	@RequestMapping(value="/getPendingDocuments", method=RequestMethod.GET)
	public ModelAndView getPendingDocuments(@RequestParam("pendingCaseId") Long pendingCaseId, HttpSession session) {
		
		activityLog.info("getPendingDocuments");
		ModelAndView mav=new ModelAndView();
         List<PendingCaseDocument> docList= new ArrayList<PendingCaseDocument>();
		 docList=pendingCaseDocumentService.getAllPendingCaseDocumentListByPendingCaseId(pendingCaseId);
			mav.addObject("pendingCaseList", docList);
			mav.setViewName("report/getPendingDocuments1");
		
		return mav;
	}
	
	@RequestMapping(value = "/getDocumentsById", method = RequestMethod.GET)
	public ModelAndView getDocumentsById(@RequestParam("documentId") Long id, HttpSession session){

		ModelAndView mav=new ModelAndView();
		String destinationPath=session.getServletContext().getRealPath("/resources/upload/");
		PendingCaseDocument document=pendingCaseDocumentService.getPendingCaseDocumentByDocumentId(id);
		logger.info("document"+ document.toString());
		String doc_path="";
		List<Lookup> lookup_scan = lookupService.getLookUpByName("PENDING_SCAN");
		List<Lookup> lookup_qc = lookupService.getLookUpByName("PENDING_QC");
		SimpleDateFormat  sdf = new SimpleDateFormat("dd-M-yyyy");//change m to mm rup
		try {
			if (document != null) {
				if (document.getQcOn() != null) {
					if (lookup_qc != null) {
						String qcDate = sdf.format(document.getQcOn());
						doc_path = lookup_qc.get(0).getLookupValue() + File.separator + qcDate + File.separator+ document.getCaseType() + File.separator + document.getCaseCategory() + "_"+ document.getCaseNo() + "_" + document.getCaseYear();
						System.out.println(" qc doc_path" + doc_path);
					}
				} else {
					if (lookup_scan != null) {
						String scanDate = sdf.format(document.getScanningOn());
						doc_path = lookup_scan.get(0).getLookupValue() + File.separator + scanDate + File.separator+ document.getCaseType() + File.separator + document.getCaseCategory() + "_"+ document.getCaseNo() + "_" + document.getCaseYear();
						System.out.println(" scan doc_path" + doc_path);
					}
				}
				FileUtility.copyFileToServer(doc_path, destinationPath, document.getDocumentName());
			}

		} catch (Exception e) {
			logger.error("Exception at document veiw "+e);
			e.printStackTrace();
			System.out.println("");
		}
		mav.addObject("documents", document);
		mav.setViewName("pendingcase/viewDocument");
		return mav;

	}
	

	@RequestMapping(value="/getProductivityReport", method=RequestMethod.GET)
	public ModelAndView getProductivityReport(HttpSession session) {
		
		activityLog.info("getProductivityReport");
		ModelAndView mav=new ModelAndView();
		mav.setViewName("report/productivityReport");
		return mav;
	}
	
	@RequestMapping(value="/searchProductivityReport", method= {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView searchProductivityReport(@RequestParam("case") String casename,
			   @RequestParam("fromDate") String fromDate, 
			   @RequestParam("toDate") String toDate, String caseStage, HttpServletRequest request) throws Exception {
		
		activityLog.info("searchProductivityReport");
		ModelAndView mav=new ModelAndView();
		Date startDate=null;
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		if(fromDate!=null) {
		 startDate = df.parse(fromDate);
		}
		Calendar cal = Calendar.getInstance();
		if(toDate!=null)
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE, 1);
		Date endDate = cal.getTime();
		
	
		List<Object> pendingCaseList=null;
           if(casename.equals("FRESH")) {
    	      
        	   pendingCaseList=allReportService.getProductivityReportFresh(caseStage, startDate, endDate);
           }
           else {
    	       pendingCaseList=allReportService.getProductivityReportPending(caseStage, startDate, endDate);
  
           }
		mav.addObject("pendingCaseList", pendingCaseList);
		mav.addObject("caseStage", caseStage);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.setViewName("report/productivityReport");
		return mav;
	}
	
	
	
	
	
}