package com.dms.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.enumm.CaseStage;
import com.dms.model.DeleteFilesLocal;
import com.dms.model.Folder;
import com.dms.model.FreshCase;
import com.dms.model.InvoiceDetail;
import com.dms.model.Lookup;
import com.dms.model.PendingCaseDocument;
import com.dms.model.User;
import com.dms.service.DeleteFilesLocalService;
import com.dms.service.FolderService;
import com.dms.service.FreshCaseService;
import com.dms.service.InvoiceDetailService;
import com.dms.service.LookupService;
import com.dms.service.RepositoryDetailService;
import com.dms.utility.FileUtility;
import com.dms.utility.FolderPathUtility;
import com.dms.utility.JsonUtility;
import com.dms.utility.PdfDocUtility;
import com.dms.validator.FreshCaseValidator;

/**
 * 
 * @author RAJKUMAR GIRI
 *
 */
@Controller
public class FreshCaseController {
	
	private static Logger logger=Logger.getLogger(FreshCaseController.class);
	
	@Autowired
	private FreshCaseValidator freshCaseValidator;
	
	@Autowired
	LookupService lookupService;
	
	@Autowired
	RepositoryDetailService repositoryDetailService;
	
	@Autowired
	FolderService folderService;
	
	@Autowired
	FreshCaseService freshCaseService;
	
	@Autowired
	FolderPathUtility folderPathUtility;
	
	@Autowired
	DeleteFilesLocalService deleteFilesLocalService;
	
	@Autowired
	ActivityLog activityLog;
	
	@Autowired
	InvoiceDetailService invoiceDetailService;
	
	@RequestMapping(value="/outarwdPendingCase", method=RequestMethod.GET)
	public ModelAndView outwardPendingCase(HttpSession session) {
		
		logger.info("");
		activityLog.info("freshCaseScanning");
		User user=(User)session.getAttribute("user");
		logger.info("Fresh Case Scanning "+user.getUserFullName());
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.setViewName("pendingcase/outwardPendingCase");
		return mav;
	}
	
	@ResponseBody
	@RequestMapping(value="/getFreshCaseTypeByAutoNo", method=RequestMethod.POST)
	public String getAllInvoiceDetailListByCaseType(@RequestParam("autoNo") Long autoNo) {
		
		String josnList=null;
		activityLog.info("  Post getAllInvoiceDetailListByCaseType");
		InvoiceDetail invoiceDetail=invoiceDetailService.getInvoiceDetailByAutoNo(autoNo);
		System.out.println("Fresh Case Details: "+invoiceDetail);
		if(invoiceDetail!=null)
		{
			josnList=JsonUtility.getJosn(invoiceDetail);
		}
		else{
			josnList=JsonUtility.getJosn("not found");
		}
		return josnList;
	}
	
	@RequestMapping(value="/getAllfreshCaseList", method=RequestMethod.GET)
	public ModelAndView getAllfreshCaseList() {
		
		ModelAndView mav=new ModelAndView();
		List<FreshCase> freshCaseList=freshCaseService.getAllFreshCaseList();
		mav.addObject("freshCaseList", freshCaseList);
		mav.setViewName("freshcase/freshCaseList");
		return mav;
	}
	
	@RequestMapping(value="/freshCaseScanning", method=RequestMethod.GET)
	public ModelAndView freshCaseScanning(FreshCase freshCase,HttpSession session) {
		
		logger.info("");
		activityLog.info("freshCaseScanning");
		User user=(User)session.getAttribute("user");
		logger.info("Fresh Case Scanning "+user.getUserFullName());
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("freshCase", freshCase);
		mav.setViewName("freshcase/freshCaseScanning");
		return mav;
	}
	
	/*@RequestMapping(value="/saveFreshCaseDocument", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView saveFreshCaseDocument(@ModelAttribute("freshCase") FreshCase freshCase, Errors errors, HttpSession session) throws Exception {
		
		activityLog.info("saveFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		DeleteFilesLocal deleteFilesLocal=null;
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			freshCaseValidator.validate(freshCase, errors);
			if(errors.hasErrors()) {
				mav.addObject("lookupList", lookupList);
				mav.setViewName("freshcase/freshCaseScanning");
			}else {
				String destinationPath="";
				Folder folder=folderService.getFolderByFolderName(freshCase.getCaseType(), "SCAN");
				if(folder!=null) {
					destinationPath=folderPathUtility.getFreshCaseFolderPath(folder.getFolderId());
					MultipartFile multipartFile=freshCase.getDocFile();
					boolean flag=FileUtility.uploadFileToServer(multipartFile, multipartFile.getOriginalFilename(), destinationPath);
					if (flag) {
						freshCase.setDocumentName(multipartFile.getOriginalFilename());
						freshCase.setScanningPage(PdfDocUtility.getPdfPageCount(destinationPath, multipartFile.getOriginalFilename()));
						freshCase.setScanningFolderId(folder.getFolderId());
						freshCase.setStatus(true);
						freshCase.setScanningBy(user.getUsername());
						freshCase.setScanningOn(new Date());
						freshCase.setQcDownloaded(false);
						freshCase.setBookmarkingDownloaded(false);
						freshCase.setCaseStage(CaseStage.SCANNED.getStage());
						Long id=freshCaseService.saveFreshCase(freshCase);
						if(id!=null) {
							deleteFilesLocal=new DeleteFilesLocal();
							deleteFilesLocal.setFileName(multipartFile.getOriginalFilename());
							deleteFilesLocal.setIsDeleted(false);
							deleteFilesLocal.setCreatedOn(new Date());
							deleteFilesLocal.setUserId(user.getId());
							deleteFilesLocalService.saveDeleteFileFromScanFolder(deleteFilesLocal);
							mav.addObject("successMessage", id);
						}
					}
				}
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("documentDetail", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				mav.setViewName("freshcase/freshCaseScanning");
		     }
		}
		return mav;
	}*/
	
	@RequestMapping(value="/saveFreshCaseDocument", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView saveFreshCaseDocument(@ModelAttribute("freshCase") FreshCase freshCase, Errors errors, HttpSession session) throws Exception {
		
		System.out.println("freshhhhhhhhhhhhhhhhhhhhhhh"+freshCase);
		
		activityLog.info("saveFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		 
		logger.info("Save Fresh Case Document: "+user.getUserFullName());
		FreshCase fs =freshCaseService.getFreshCaseByAutoNo(freshCase.getAutoNo());
	
		
		if(fs != null){
			mav.addObject("freshCase", freshCase);
			//mav.addObject("freshCase", freshCase);
				mav.addObject("freshCaseDetail",null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "AUTO NO ALREADY EXISTS");

			mav.setViewName("freshcase/freshCaseScanning");
			return mav;
			
		}
		else {
		
		if(user!=null) {
			freshCaseValidator.validate(freshCase, errors);
			if(errors.hasErrors()) {
				mav.addObject("lookupList", lookupList);
				mav.setViewName("freshcase/freshCaseScanning");
			}else {
				List<Lookup> l =lookupService.getAllLookupListByLookupName("COUNTER_SCAN");
				String destPath =l.get(0).getLookupValue()+File.separator+user.getUsername();
				File theDir = new File(destPath);

				if (theDir.exists()) {
					
					
					if(freshCase.getDocumentType() != null && freshCase.getCaseType() != null){
									    String casefolder =theDir+File.separator+freshCase.getCaseType();
				  
				    File caseDir = new File(casefolder);
				    
				    if(!caseDir.exists()){
				    	try{
					    	caseDir.mkdir();
					    	String caseWithDocType =casefolder+File.separator+freshCase.getDocumentType();
					    	 File caseDir2 = new File(caseWithDocType);
					    	 if(!caseDir2.exists()){
							    	try{
								    	caseDir2.mkdir();
								    	//Long id
								      
								    }
							    	
								    catch(Exception se){
								        logger.info(se);
								    } 
					    	 }
					    	//Long id
					      
					    }
				    	
					    catch(Exception se){
					        logger.info(se);
					    }  
				    	
				    	
				    	
				    }
				    else if(caseDir.exists()){
				    	String caseWithDocType =casefolder+File.separator+freshCase.getDocumentType();
				    	 File caseDir2 = new File(caseWithDocType);
				    	 if(!caseDir2.exists()){
						    	try{
							    	caseDir2.mkdir();
							    	//Long id
							      
							    }
						    	
							    catch(Exception se){
							        logger.info(se);
							    }  
				    	
			    	}
				    

				}
				    }
					else if(freshCase.getCaseType() != null){
						
						    
						    String casefolder =theDir+File.separator+File.separator+freshCase.getCaseType();
						  
						    File caseDir = new File(casefolder);
						    
						    if(!caseDir.exists()){
						    	try{
							    	caseDir.mkdir();
							    	//Long id
							      
							    } 
							    catch(Exception se){
							        logger.info(se);
							    }   
						    	
						    	
						    }
						
					}
				    
				}
				
				
				
				
					if (true) {
						freshCase.setStatus(true);
						freshCase.setScanningBy(user.getUsername());
						freshCase.setScanningOn(new Date());
						freshCase.setQcDownloaded(false);
						freshCase.setBookmarkingDownloaded(false);
						freshCase.setCaseStage(CaseStage.PENDINGSCAN.getStage());
						freshCase.setAcceptedByScanner(true);
						
						InvoiceDetail in =invoiceDetailService.getInvoiceDetailByAutoNo(freshCase.getAutoNo());
						
						if(in != null){
							if(!in.getCaseType().equals(freshCase.getCaseType())){
								freshCase.setOldCaseType(in.getCaseType());
							}
							
						}
						
						Long id=freshCaseService.saveFreshCase(freshCase);
						
						freshCase.setId(id);
						
						
						
					}
					
					List<FreshCase> freshCaseDetail = new ArrayList<FreshCase>();
					freshCaseDetail.add(freshCase);
				//}
					
					
				mav.addObject("freshCase", new FreshCase());
				//mav.addObject("freshCase", freshCase);
					mav.addObject("freshCaseDetail",freshCase);
					
				mav.addObject("documentDetail", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				mav.addObject("successMessage", "CASE SAVED SUCCESSFULLY");

				mav.setViewName("freshcase/freshCaseScanning");
		     }
		}
		}
		return mav;
	}
	
	
	@RequestMapping(value="/acceptFreshCase", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView acceptFreshCaseByScanner(@RequestParam("id") Long id,  HttpSession session) throws Exception {
		
		
		activityLog.info("saveFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		
		logger.info("Accept Fresh Case: "+user.getUserFullName());
		
		if(user!=null) {
		
			
			
				List<Lookup> l =lookupService.getAllLookupListByLookupName("COUNTER_SCAN");
				String destPath =l.get(0).getLookupValue()+File.separator+user.getUsername();
				File theDir = new File(destPath);

				FreshCase freshCase = freshCaseService.getFreshCaseById(id);
				if(freshCase != null){
					
						if(freshCase.getAcceptedByScanner()){
				freshCase.setCaseStage(CaseStage.READYTOMOVESCANNEDFILES.getStage());
				freshCase.setScannerFullname(user.getUserFullName());
				
				
						
						
						Boolean id1=freshCaseService.updateFreshCase(freshCase);
						
						
					}
						else if(!freshCase.getAcceptedByScanner()){
							
							mav.addObject("freshCase", new FreshCase());
							mav.addObject("documentDetail", new PendingCaseDocument());
							mav.addObject("lookupList", lookupList);
							mav.addObject("successMessage", "PLEASE ACCEPT FIRST");

							mav.setViewName("freshcase/freshCaseScanning");
							
						}
				}
				
				//}
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("documentDetail", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				mav.addObject("successMessage", "SCANNING DONE SUCCESSFULLY");

				mav.setViewName("freshcase/freshCaseScanning");
		}   
	
		return mav;
	}
	
	@RequestMapping(value="/bookmarkingDoneByBookmarkUser", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView bookmarkingDoneByBookmarkUser(@RequestParam("id") Long id,  HttpSession session) throws Exception {		
		activityLog.info("saveFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		
		logger.info("Bookmarking Done  BY Bookmark User: "+user.getUserFullName());
		
		if(user!=null) {

				FreshCase freshCase = freshCaseService.getFreshCaseById(id);
				
				if(freshCase != null){
					if(freshCase.getCaseStage().equals(CaseStage.QC.getStage()) && freshCase.getDocumentType() == null){
					freshCase.setCaseStage(CaseStage.READYTOMOVEBOOKMARKFILES.getStage());
					freshCase.setBookmarkingOn(new Date());
					freshCase.setBookmarkingBy(user.getUsername());
					freshCase.setBookMarkFullname(user.getUserFullName());
					Boolean id1=freshCaseService.updateFreshCase(freshCase);
					if(id1){
						mav.addObject("freshCase", new FreshCase());
						mav.addObject("documentDetail", new PendingCaseDocument());
						mav.addObject("lookupList", lookupList);
						mav.addObject("successMessage", "BOOKMARK DONE SUCCESSFULLY");

						mav.setViewName("freshcase/freshCaseBookmarking");
						
						return mav;
						
					}
					mav.addObject("freshCase", new FreshCase());
					mav.addObject("documentDetail", new PendingCaseDocument());
					mav.addObject("lookupList", lookupList);
					mav.addObject("successMessage", "SOME ISSUE IN BOOKMARKDONE");

					mav.setViewName("freshcase/freshCaseBookmarking");
					}
					
				}
				else{
					mav.addObject("freshCase", new FreshCase());
					mav.addObject("documentDetail", new PendingCaseDocument());
					mav.addObject("lookupList", lookupList);
					mav.addObject("successMessage", "FILE NOT FOUND");

					mav.setViewName("freshcase/freshCaseBookmarking");
					return mav;
					
				}
					
				
				
				//}
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("documentDetail", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				mav.addObject("successMessage", "STAGE CHANGED SUCCESSFULLY");

				mav.setViewName("freshcase/freshCasebBookmarking");
		  
		}
		return mav;
	}
	
	
	/*@RequestMapping(value="/bookmarkingDoneByBookmarkUser", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView bookmarkingDoneByBookmarkUser(@RequestParam("id") Long id,  HttpSession session) throws Exception {
		
		System.out.println("freshhhhhhhhhhhhhhhhhhhhhhh"+id);
		
		activityLog.info("saveFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		
		if(user!=null) {

				FreshCase freshCase = freshCaseService.getFreshCaseById(id);
				
				if(freshCase != null){
					if(freshCase.getCaseStage().equals(CaseStage.SCANNED.getStage())){
					freshCase.setCaseStage(CaseStage.READYTOMOVEQCFILES.getStage());
					freshCase.setQcOn(new Date());
					freshCase.setQcBy(user.getUsername());
					Boolean id1=freshCaseService.updateFreshCase(freshCase);
					if(id1){
						mav.addObject("freshCase", new FreshCase());
						mav.addObject("documentDetail", new PendingCaseDocument());
						mav.addObject("lookupList", lookupList);
						mav.addObject("successMessage", "QC STAGE CHANGED SUCCESSFULLY");

						mav.setViewName("freshcase/freshCaseQc");
						
						return mav;
						
					}
					}
				}
					
				
				
				//}
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("documentDetail", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				mav.addObject("successMessage", "STAGE CHANGED SUCCESSFULLY");

				mav.setViewName("freshcase/freshCaseQc");
		  
		}
		return mav;
	}*/
	@RequestMapping(value="/qcDoneByQcUser", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView qcDoneByQcUser(@RequestParam("id") Long id,  HttpSession session) throws Exception {
		
		System.out.println("freshhhhhhhhhhhhhhhhhhhhhhh"+id);
		
		activityLog.info("saveFreshCaseDocument");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		
		logger.info("QC Done By QC User: "+user.getUserFullName());
		
		if(user!=null) {

				FreshCase freshCase = freshCaseService.getFreshCaseById(id);
				
				if(freshCase != null){
					if(freshCase.getCaseStage().equals(CaseStage.SCANNED.getStage())){
					freshCase.setCaseStage(CaseStage.READYTOMOVEQCFILES.getStage());
					freshCase.setQcOn(new Date());
					freshCase.setQcBy(user.getUsername());
					freshCase.setQcFullname(user.getUserFullName());
					Boolean id1=freshCaseService.updateFreshCase(freshCase);
					if(id1){
						mav.addObject("freshCase", new FreshCase());
						mav.addObject("documentDetail", new PendingCaseDocument());
						mav.addObject("lookupList", lookupList);
						mav.addObject("successMessage", "QC DONE SUCCESSFULLY");

						mav.setViewName("freshcase/freshCaseQc");
						
						return mav;
						
					}
					}
				}
					
				
				
				//}
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("documentDetail", new PendingCaseDocument());
				mav.addObject("lookupList", lookupList);
				mav.addObject("successMessage", "STAGE CHANGED SUCCESSFULLY");

				mav.setViewName("freshcase/freshCaseQc");
		  
		}
		return mav;
	}
	
	@RequestMapping(value="/getFreshCaseScanByDiaryNo", method= {RequestMethod.POST,RequestMethod.GET})
	//public ModelAndView getAllPendingCaseListForScanning(@ModelAttribute("pendingCase") PendingCase pendingCase) {
	public ModelAndView getFreshCaseScanByDiaryNo(@ModelAttribute("pendingCase") FreshCase freshCase) {
	
		Date date =new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		String year = df.format(date);
		
		activityLog.info("Get getAllPendingCaseListForScanning");
		
		
		ModelAndView mav=new ModelAndView();// for multiple records changed by deepak
		List<Lookup> lookupList=lookupService.getAllLookupList();
		FreshCase freshCase1=freshCaseService.getFreshCaseByDiaryNoAndYear(freshCase.getDiaryNo(),freshCase.getYear());

		//FreshCase freshCase1=freshCaseService.getFreshCaseByDiaryNo(freshCase.getDiaryNo());
		if(freshCase1 != null){
			
			if(freshCase1.getCaseStage().equals(CaseStage.PENDINGSCAN.getStage()) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", freshCase1);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "");
		//	mav.addObject("", "File Scanned Successfully");

			

			mav.setViewName("freshcase/freshCaseScanning");
	     
	
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.READYTOMOVESCANNEDFILES.getStage()  ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "READY TO MOVE FOR SCAN");
			mav.setViewName("freshcase/freshCaseScanning");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.SCANNED.getStage()  ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "FILE IS AT SCANNING DONE");
			mav.setViewName("freshcase/freshCaseScanning");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.QC.getStage()) || freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEQCFILES.getStage()) || freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEBOOKMARKFILES.getStage()) || freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEBOOKMARKFILES.getStage()) || freshCase1.getCaseStage().equals(CaseStage.BOOKMARK.getStage())&& freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "FILE IS ALREADY PROCESSED");
			mav.setViewName("freshcase/freshCaseScanning");
				
			}
			else {
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "DIARY NO NOT EXISTS");
			mav.setViewName("freshcase/freshCaseScanning");

				
			}
			
		}
		else{
			mav.addObject("freshCase", new FreshCase());
			mav.addObject("freshCaseDetail", null);
			
		mav.addObject("documentDetail", new PendingCaseDocument());
		mav.addObject("lookupList", lookupList);
		mav.addObject("successMessage", "DIARY NO NOT EXISTS");
		mav.setViewName("freshcase/freshCaseScanning");

		
			return mav;
		}
		//return mav;
	return mav;
	}
	
	
	
	@RequestMapping(value="/getFreshCaseBookMarkByDiaryNo", method= {RequestMethod.POST,RequestMethod.GET})
	//public ModelAndView getAllPendingCaseListForScanning(@ModelAttribute("pendingCase") PendingCase pendingCase) {
	public ModelAndView getFreshCaseBookMarkByDiaryNo(@ModelAttribute("pendingCase") FreshCase freshCase) {
	
		
		activityLog.info("Get getAllPendingCaseListForScanning");
		ModelAndView mav=new ModelAndView();// for multiple records changed by deepak
		List<Lookup> lookupList=lookupService.getAllLookupList();
		Date date =new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		String year = df.format(date);
		

		FreshCase freshCase1=freshCaseService.getFreshCaseByDiaryNoAndYear(freshCase.getDiaryNo(),freshCase.getYear());		
		if(freshCase1 != null){
			
			if(freshCase1.getCaseStage().equals(CaseStage.QC.getStage()) && freshCase1.getAcceptedByScanner() && freshCase1.getDocumentType() == null){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", freshCase1);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "");
		//	mav.addObject("", "File Scanned Successfully");

			

			mav.setViewName("freshcase/freshCaseBookmarking");
	     
	
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.QC.getStage()) && freshCase1.getAcceptedByScanner() && freshCase1.getDocumentType() != null) {
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "Not Found");
			mav.setViewName("freshcase/freshCaseBookmarking");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEBOOKMARKFILES.getStage()) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "READY TO MOVE FOR BOOKMARKING");
			mav.setViewName("freshcase/freshCaseBookmarking");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEQCFILES.getStage() ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "WAIT FOR SOME TIME SERVICE IS IN PROCESS");
			mav.setViewName("freshcase/freshCaseQc");
				
			}
			else {
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "FILE IS NOT REACHED AT YOUR STAGE YET");
			mav.setViewName("freshcase/freshCaseBookmarking");
				
			}
		}
		else{
			mav.addObject("freshCase", new FreshCase());
			mav.addObject("freshCaseDetail", null);
			
		mav.addObject("documentDetail", new PendingCaseDocument());
		mav.addObject("lookupList", lookupList);
		mav.addObject("successMessage", "DIARY NO NOT EXISTS");
		mav.setViewName("freshcase/freshCaseBookmarking");

		
			return mav;
		}
		//return mav;
	return mav;
	}
	
	
	@RequestMapping(value="/getFreshCaseQcByDiaryNo", method= {RequestMethod.POST,RequestMethod.GET})
	//public ModelAndView getAllPendingCaseListForScanning(@ModelAttribute("pendingCase") PendingCase pendingCase) {
	public ModelAndView getFreshCaseQcByDiaryNo(@ModelAttribute("pendingCase") FreshCase freshCase,HttpSession session) {
	
		
		activityLog.info("Get getAllPendingCaseListForScanning");
		ModelAndView mav=new ModelAndView();// for multiple records changed by deepak
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		Date date =new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		String year = df.format(date);
		System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"+freshCase);
		System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"+year);
		
		logger.info("Get Fresh Case QC Diary No: "+user.getUserFullName());

		FreshCase freshCase1=freshCaseService.getFreshCaseByDiaryNoAndYear(freshCase.getDiaryNo(),freshCase.getYear());		
		
		if(freshCase1 != null){
			
			
			if(freshCase1.getCaseStage().equals(CaseStage.SCANNED.getStage()) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", freshCase1);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			//mav.addObject("successMessage", "");
		//	mav.addObject("", "File Scanned Successfully");

			

			mav.setViewName("freshcase/freshCaseQc");
	     
	
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEQCFILES.getStage()  ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "READY TO MOVE FOR QC");
			mav.setViewName("freshcase/freshCaseQc");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.QC.getStage()  ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "FILE IS AT QC DONE STAGE");
			mav.setViewName("freshcase/freshCaseQc");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.READYTOMOVEBOOKMARKFILES.getStage() ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "FILE IS ALREADY PROCESSED");
			mav.setViewName("freshcase/freshCaseQc");
				
			}
			else if(freshCase1.getCaseStage().equals(CaseStage.READYTOMOVESCANNEDFILES.getStage() ) && freshCase1.getAcceptedByScanner()){
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "WAIT FOR SOME TIME SERVICE IS IN PROCESS");
			mav.setViewName("freshcase/freshCaseQc");
				
			}
			else {
				mav.addObject("freshCase", new FreshCase());
				mav.addObject("freshCaseDetail", null);
				
			mav.addObject("documentDetail", new PendingCaseDocument());
			mav.addObject("lookupList", lookupList);
			mav.addObject("successMessage", "FILE IS NOT REACHED AT YOUR STAGE YET");
			mav.setViewName("freshcase/freshCaseQc");
				
				
			}
			
			
		}
		else{
			mav.addObject("freshCase", new FreshCase());
			mav.addObject("freshCaseDetail", null);
			
		mav.addObject("documentDetail", new PendingCaseDocument());
		mav.addObject("lookupList", lookupList);
		mav.addObject("successMessage", "DIARY NO NOT EXISTS");
		mav.setViewName("freshcase/freshCaseQc");

		
			return mav;
		}
		//return mav;
	return mav;
	}
	
	
	@RequestMapping(value="/searchFreshCase", method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView searchFreshCase(FreshCase freshCase) {
		
		activityLog.info("searchFreshCase");
		ModelAndView mav=new ModelAndView();
		List<FreshCase> freshCaseList=freshCaseService.getAllFreshCaseListByDiaryNoAndAutoNo(freshCase.getAutoNo(), freshCase.getDiaryNo(), freshCase.getYear());
		mav.addObject("freshCaseList", freshCaseList);
		mav.setViewName("freshcase/freshCaseList");
		return mav;
	}
	
	@RequestMapping(value="/freshCaseQc", method=RequestMethod.GET)
	public ModelAndView pendingCaseQc(FreshCase freshCase) {
		
		activityLog.info("freshCaseQc");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("freshCase", freshCase);
		mav.setViewName("freshcase/freshCaseQc");
		return mav;
	}
	
	/*@ResponseBody
	@RequestMapping(value="/getfreshCaseForQcByDiaryNo", method=RequestMethod.POST)
	public String getfreshCaseForQcByDiaryNo(@RequestParam("diaryNo") String diaryNo) {
		
		FreshCase freshCase=freshCaseService.getFreshCaseByDiaryNoAndCaseStage(diaryNo, CaseStage.SCANNED.getStage());
		String str=JsonUtility.getJosn(freshCase);
		return str;
	}*/
	
	@ResponseBody
	@RequestMapping(value="/getAllFreshcaseListByForQc", method=RequestMethod.POST)
	public String getAllFreshcaseListByForQc(@RequestParam("caseType") String caseType) {
		
		activityLog.info("getAllFreshcaseListByForQc");
		List<FreshCase> freshCaseList=freshCaseService.getAllFreshCaseListByCaseTypeAndCaseStage(caseType, CaseStage.SCANNED.getStage());
		String str=JsonUtility.getJosn(freshCaseList);
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="/freshcaseQcDone", method=RequestMethod.POST)
	public ModelAndView freshcaseQcDone(FreshCase freshCase, HttpSession session) throws Exception {
		
		activityLog.info("freshcaseQcDone");
		ModelAndView mav=new ModelAndView();
		DeleteFilesLocal deleteFilesLocal=null;
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		logger.info("Fresh Case QC Done: "+user.getUserFullName());
		if(user!=null) {
			FreshCase freshCaseDetail=freshCaseService.getFreshCaseById(freshCase.getId());
			if(freshCaseDetail!=null) {
				Folder folder=folderService.getFolderByFolderName(freshCaseDetail.getCaseType(), "QC");
				if(folder!=null) {
					MultipartFile multipartFile=freshCase.getDocFile();
					if(multipartFile!=null) {
						String destinationPath=folderPathUtility.getFreshCaseFolderPath(folder.getFolderId());
						boolean uploadflag=FileUtility.uploadFileToServer(multipartFile, multipartFile.getOriginalFilename(), destinationPath);
						if (uploadflag) {
							freshCaseDetail.setQcFolderId(folder.getFolderId());
							freshCaseDetail.setQcPage(PdfDocUtility.getPdfPageCount(destinationPath, multipartFile.getOriginalFilename()));
							freshCaseDetail.setCaseStage(CaseStage.QC.getStage());
							freshCaseDetail.setQcBy(user.getUsername());
							freshCaseDetail.setQcOn(new Date());
							boolean flag=freshCaseService.updateFreshCase(freshCaseDetail);
							if(flag) {
								deleteFilesLocal=new DeleteFilesLocal();
								deleteFilesLocal.setFileName(multipartFile.getOriginalFilename());
								deleteFilesLocal.setIsDeleted(false);
								deleteFilesLocal.setCreatedOn(new Date());
								deleteFilesLocal.setUserId(user.getId());
								deleteFilesLocalService.saveDeleteFileFromScanFolder(deleteFilesLocal);
								mav.addObject("successMessage", "File Scanned Successfully");
							}
						}
					}
					mav.addObject("lookupList", lookupList);
					mav.setViewName("freshcase/freshCaseQc");
				}
			}
		}
		return mav;
	}
	
	@RequestMapping(value="/freshCaseBookmarking", method=RequestMethod.GET)
	public ModelAndView freshCaseBookmarking(InvoiceDetail freshCase) {
		
		activityLog.info("freshCaseBookmarking");
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("freshCase", freshCase);
		mav.setViewName("freshcase/freshCaseBookmarking");
		return mav;
	}
	
	@ResponseBody
	@RequestMapping(value="/getfreshCaseForBookmarkingByDiaryNo", method=RequestMethod.POST)
	public String getfreshCaseByDiaryNo(@RequestParam("diaryNo") String diaryNo) {
		
		activityLog.info("getfreshCaseForBookmarkingByDiaryNo");
		FreshCase freshCase=freshCaseService.getFreshCaseByDiaryNoAndCaseStage(diaryNo, CaseStage.QC.getStage());
		String str=JsonUtility.getJosn(freshCase);
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="/getAllFreshcaseListByForBookmarking", method=RequestMethod.POST)
	public String getAllFreshcaseListByForBookmarking(@RequestParam("caseType") String caseType) {
		
		activityLog.info("getAllFreshcaseListByForBookmarking");
		List<FreshCase> freshCaseList=freshCaseService.getAllFreshCaseListByCaseTypeAndCaseStage(caseType, CaseStage.QC.getStage());
		String str=JsonUtility.getJosn(freshCaseList);
		return str;
	}
	
	@RequestMapping(value="/freshcaseBookmarkingDone", method=RequestMethod.POST)
	public ModelAndView freshcaseBookmarkingDone(FreshCase freshCase, HttpSession session) throws Exception {
		
		activityLog.info("freshcaseBookmarkingDone");
		ModelAndView mav=new ModelAndView();
		DeleteFilesLocal deleteFilesLocal=null;
		List<Lookup> lookupList=lookupService.getAllLookupList();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			FreshCase freshCaseDetail=freshCaseService.getFreshCaseById(freshCase.getId());
			if(freshCaseDetail!=null) {
				Folder folder=folderService.getFolderByFolderName(freshCaseDetail.getCaseType(), "BOOKMARK");
				if(folder!=null) {
					MultipartFile multipartFile=freshCase.getDocFile();
					String destinationPath=folderPathUtility.getFreshCaseFolderPath(folder.getFolderId());
					boolean uploadflag=FileUtility.uploadFileToServer(multipartFile, multipartFile.getOriginalFilename(), destinationPath);
					if (uploadflag) {
						freshCaseDetail.setCaseStage(CaseStage.BOOKMARK.getStage());
						freshCaseDetail.setBookmarkingPage(PdfDocUtility.getPdfPageCount(destinationPath, multipartFile.getOriginalFilename()));
						freshCaseDetail.setBookmarkingBy(user.getUsername());
						freshCaseDetail.setBookmarkingFolderId(folder.getFolderId());
						freshCaseDetail.setBookmarkingOn(new Date());
						boolean flag=freshCaseService.updateFreshCase(freshCaseDetail);
						if(flag) {
							deleteFilesLocal=new DeleteFilesLocal();
							deleteFilesLocal.setFileName(multipartFile.getOriginalFilename());
							deleteFilesLocal.setIsDeleted(false);
							deleteFilesLocal.setCreatedOn(new Date());
							deleteFilesLocal.setUserId(user.getId());
							deleteFilesLocalService.saveDeleteFileFromScanFolder(deleteFilesLocal);
							mav.addObject("successMessage", "File Scanned Successfully");
						}
					}
					mav.addObject("lookupList", lookupList);
					mav.setViewName("freshcase/freshCaseBookmarking");
				}
			}
		}
		return mav;
	}
	
	// Commented because the method below reads only current date folders
/*	
	@RequestMapping(value="/downloadFreshCaseDocument", method=RequestMethod.GET)
	public void downloadFreshCaseDocument(@RequestParam("id") Long id, 
						@RequestParam("action") String action, HttpServletResponse response) throws Exception {
		
		String destinationPath="";
		FreshCase freshCase=freshCaseService.getFreshCaseById(id);
		if(freshCase!=null) {
			if(action.equalsIgnoreCase("QC")) {
        		destinationPath=folderPathUtility.getFreshCaseFolderPath(freshCase.getScanningFolderId());
        		freshCase.setQcDownloaded(true);
        	}else {
        		destinationPath=folderPathUtility.getFreshCaseFolderPath(freshCase.getQcFolderId());
        		freshCase.setBookmarkingDownloaded(true);
        	}
			Path file=Paths.get(destinationPath, freshCase.getDocumentName());
	        if (Files.exists(file)){
	        	boolean flag=freshCaseService.updateFreshCase(freshCase);
	        	if(flag) {
	                response.setContentType("application/pdf");
		            response.addHeader("Content-Disposition", "attachment; filename="+freshCase.getDocumentName());
		            try {
		                Files.copy(file, response.getOutputStream());
		                response.getOutputStream().flush();
		            }catch (Exception ex) {
		                ex.printStackTrace();
		            }
	        	}
	        }
		}
	}
*/
	// New Method to pick files from old location --RajKumar and Rup
	@RequestMapping(value="/downloadFreshCaseDocument", method=RequestMethod.GET)
	public void downloadFreshCaseDocument(@RequestParam("id") Long id, @RequestParam("action") String action,
														HttpServletResponse response) throws Exception {
		
		activityLog.info("downloadFreshCaseDocument");
		String destinationPath="";
		FreshCase freshCase=freshCaseService.getFreshCaseById(id);
		if(freshCase!=null) {
			if(action.equalsIgnoreCase("QC")) {
        		destinationPath=folderPathUtility.getFreshCaseDownloadFolderPath(freshCase.getScanningOn(), freshCase.getScanningFolderId());
        		freshCase.setQcDownloaded(true);
        	}else if (action.equalsIgnoreCase("BOOKMARKING")) {
        		destinationPath=folderPathUtility.getFreshCaseDownloadFolderPath(freshCase.getQcOn(), freshCase.getQcFolderId());
        		freshCase.setBookmarkingDownloaded(true);
        	}
			Path file=Paths.get(destinationPath, freshCase.getDocumentName());
	        if (Files.exists(file)){
	        	boolean flag=freshCaseService.updateFreshCase(freshCase);
	        	if(flag) {
	                response.setContentType("application/pdf");
		            response.addHeader("Content-Disposition", "attachment; filename="+freshCase.getDocumentName());
		            try {
		                Files.copy(file, response.getOutputStream());
		                response.getOutputStream().flush();
		            }catch (Exception ex) {
		                ex.printStackTrace();
		            }
	        	}
	        }
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/checkQcDone", method=RequestMethod.POST)
	public String checkQcDone(@RequestParam("id") Long id, @RequestParam("action") String action) {
		
		String str="";
		activityLog.info("checkQcDone");
		FreshCase freshCase=freshCaseService.getFreshCaseById(id);
		if(freshCase!=null) {
			if(action.equalsIgnoreCase("QC")) {
				if(freshCase.getQcDownloaded()) {
					str="true";
				}else {
					str="false";
				}
			}else {
				if(freshCase.getBookmarkingDownloaded()) {
					str="true";
				}else {
					str="false";
				}
			}
		}
		return str;
	}
	
	
				}
	

