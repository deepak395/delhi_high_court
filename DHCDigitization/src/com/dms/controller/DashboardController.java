package com.dms.controller;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.form.MenuForm;
import com.dms.model.FreshCase;
import com.dms.model.Lookup;
import com.dms.model.Menu;
import com.dms.model.PendingCase;
import com.dms.model.Submenu;
import com.dms.model.UserRights;
import com.dms.model.UserRole;
import com.dms.model.User;
import com.dms.service.FreshCaseService;
import com.dms.service.InvoiceDetailService;
import com.dms.service.LookupService;
import com.dms.service.MenuService;
import com.dms.service.PendingCaseService;
import com.dms.service.SubmenuService;
import com.dms.service.UserRightsService;
import com.dms.service.UserRoleService;

@Controller
public class DashboardController {
	
	private static Logger logger=Logger.getLogger(DashboardController.class);
	
	@Autowired
	InvoiceDetailService invoiceDetailService;
	
	@Autowired
	PendingCaseService pendingCaseService;
	
	@Autowired
	FreshCaseService freshCaseService;
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	SubmenuService submenuService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserRightsService userRightsService;
	
	@Autowired
	LookupService lookupService;
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public ModelAndView home(HttpSession session) {
		
		
		activityLog.info("dashboard");
		ModelAndView mav=new ModelAndView();
		List<MenuForm> menuFormList=new ArrayList<MenuForm>();
		MenuForm menuForm=null;
		UserRole userRole=null;
		List<Menu> menuList=null;
		List<Submenu> subMenuList=null;
		List<UserRights> userRightsList=null;
		Submenu submenu=null;
		User user=(User)session.getAttribute("user");
		logger.info("Session Logged in by: "+ user.getUserFullName());
		if(user!=null) {
			if(user.getRoleName().equalsIgnoreCase("SUPER ADMIN")) {
				menuList=menuService.getMenuList();
				if(menuList!=null) {
					for (Menu menu : menuList) {
						 menuForm=new MenuForm();
						 menuForm.setMenu(menu);
						 subMenuList=submenuService.getAllSubmenuListByMenuId(menu.getMenuId());
						 if(subMenuList!=null) {
							 menuForm.setSubmenuList(subMenuList);
						 }
						 menuFormList.add(menuForm);
					}
				}
			}else {
				menuList=menuService.getMenuList();
				if(menuList!=null) {
				for (Menu menu : menuList) {
					 subMenuList=new ArrayList<Submenu>();
					 menuForm=new MenuForm();
					 userRightsList=userRightsService.getAllUserRightsListByUsername(user.getUsername(), menu.getMenuId());
					 if(userRightsList!=null) {
						menuForm.setMenu(menu);
						for (UserRights userRights : userRightsList) {
							submenu=submenuService.getSubmenuBySubmenuId(userRights.getSubmenuId());
							if(submenu!=null) {
								 subMenuList.add(submenu);
							 }
						}
						 menuForm.setSubmenuList(subMenuList);
						 menuFormList.add(menuForm);
					 }
					}
				}
				session.setAttribute("userRole", userRole);
			}
		}
		session.setAttribute("menuFormList", menuFormList);
		mav.setViewName("redirect:/dashboard");
		return mav;
	}
	
	@RequestMapping(value="/dashboard", method=RequestMethod.GET)
	public ModelAndView dashboard(HttpSession session) {
		
		ModelAndView mav=new ModelAndView();
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingCase", new PendingCase());
		mav.setViewName("common/dashboard");
		return mav;
	}
	
	@RequestMapping(value="/searchAllPendingCase", method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView searchAllPendingCase(PendingCase pendingCase,HttpSession session) {
		activityLog.info("searchAllPendingCase");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Search All Pending Case"+user.getUserFullName());
		PendingCase pendingcasedetail=pendingCaseService.getPendingCaseByCaseNoAndCaseYearAndCaseCategory(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getCaseCategory());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("pendingcasedetail", pendingcasedetail);
		mav.setViewName("common/dashboard");
		return mav;
	}
	
	@RequestMapping(value="/searchAllFreshCase", method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView searchAllFreshCase(FreshCase freshCase,HttpSession session) {
		
		activityLog.info("searchAllFreshCase");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		logger.info("Search All Fresh Case "+user.getUserFullName());
		List<FreshCase> freshCaseList=freshCaseService.getAllFreshCaseListByDiaryNoAndAutoNo(freshCase.getAutoNo(), freshCase.getDiaryNo(), freshCase.getYear());
		List<Lookup> lookupList=lookupService.getAllLookupList();
		mav.addObject("lookupList", lookupList);
		mav.addObject("freshCaseList", freshCaseList);
		mav.setViewName("common/dashboard");
		return mav;
	}

}
