package com.dms.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dms.model.FreshCase;
import com.dms.service.AllReportService;
import com.dms.service.FreshCaseService;
import com.dms.taxiinvoice.TaxInvoicePdfGenerator;

@Controller
public class TaxInvoiceController {
	
	@Autowired
	private FreshCaseService freshCaseService;
	
	@Autowired
	private AllReportService allReportService;
	
	@Autowired
	private TaxInvoicePdfGenerator taxInvoicePdfGenerator;
	
	@RequestMapping(value="/getTaxInvoice", method=RequestMethod.GET)
	public String getTaxInvoice(){
		
		return "report/taxInvoiceReport";
		
	}
	
	
	
	@RequestMapping(value="/viewPdfFormat", method=RequestMethod.GET)
	public ModelAndView viewPdfFormat(@RequestParam("fromDate") String fromDate, 
			@RequestParam("toDate") String toDate, HttpSession session) throws Exception {
		System.out.println("fromDate"+fromDate +"toDate"+toDate);
		ModelAndView mav=new ModelAndView();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date startDate=df.parse(fromDate);
		Calendar cal=Calendar.getInstance();
		cal.setTime(df.parse(toDate));
		cal.add(Calendar.DATE,1);
		Date endDate=cal.getTime();
		
		SimpleDateFormat df2=new SimpleDateFormat("yyyy=MM-dd");
		String strdate=df2.format(startDate);
		String endate=df2.format(endDate);
		
		List<FreshCase> freshCaseList=allReportService.getAllFreshCaseForTaxInvoice(strdate, endate);
		if(freshCaseList!=null){
				for (FreshCase freshCase : freshCaseList) {
					
					taxInvoicePdfGenerator.generateTaxInvoice(freshCase);
				}
			}
		mav.setViewName("report/taxInvoiceReport");
		return mav;
		
	}
	
	/*@RequestMapping(value="/getTaxInvoiceList", method=RequestMethod.GET)
	public ModelAndView getTaxInvoiceList(HttpSession session) throws ParseException {
		
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
		
			InvoiceDetail invoiceDetailList=invoiceDetailService.getInvoiceDetailByAutoNo(12l);
			mav.addObject("invoiceDetailList", invoiceDetailList);
			mav.addObject("invoiceDetail", new InvoiceDetail());
			mav.setViewName("invoicedetail/invoiceDetailList");
		}
		return mav;
	}*/

}
