package com.dms.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.model.Submenu;
import com.dms.model.User;
import com.dms.model.UserRights;
import com.dms.model.UserRole;
import com.dms.service.SubmenuService;
import com.dms.service.UserRightsService;
import com.dms.service.UserRoleService;
import com.dms.service.UserService;
import com.dms.utility.FileUtility;
import com.dms.utility.MD5Data;
import com.dms.utility.UploadUser;
import com.dms.validator.PasswordValidator;
import com.dms.validator.UserValidator;

/**
 * 
 * @author Rajkumar Giri
 *
 */
@Controller
public class UserController {
	
	private static Logger logger=Logger.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserValidator userValidator;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserRightsService userRightsService;
	
	@Autowired
	private SubmenuService submenuService;
	
	@Autowired
	PasswordValidator passwordValidator;
	
	@Autowired
	ActivityLog activityLog;
	
	//Get all user list
	@RequestMapping(value="/geAllUserList", method=RequestMethod.GET)
	public ModelAndView geAllUserList(HttpServletRequest request){

		logger.info("");
		activityLog.info("geAllUserList");
		ModelAndView mav=new ModelAndView();
		List<User> userList=userService.getAllUserList();
		List<UserRole> userRoleList=userRoleService.getAllUserRoleList();
		mav.addObject("userList", userList);
		mav.addObject("userRoleList", userRoleList);
		mav.addObject("userDetail", new User());
		mav.setViewName("user/userList");
		return mav;
	}
	
	 /*	Save user  */
	@RequestMapping(value="/saveUser", method= {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView saveUser(@ModelAttribute("userDetail") User userDetail, Errors errors, HttpSession session) {
		
		activityLog.info("saveUser");
		ModelAndView mav=new ModelAndView();
		List<User> userList=null;
		List<Submenu> submenuList=null;
		UserRights userRights=null;
		List<UserRole> userRoleList=null;
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			userValidator.validate(userDetail, errors);
			if(errors.hasErrors()){
				userRoleList=userRoleService.getAllUserRoleList();
			    userList=userService.getAllUserList();
				mav.addObject("userList", userList);
				mav.addObject("userRoleList", userRoleList);
				mav.setViewName("user/userList");
			}else {
				userDetail.setPassword(MD5Data.getMD5(userDetail.getPassword()));
				userDetail.setStatus(true);
				userDetail.setCreatedOn(new Date());
				userDetail.setCreatedBy(user.getUsername());
				boolean flag=userService.saveOrUpdateUser(userDetail);
				if (flag) {
					submenuList=submenuService.getAllSubmenuListByRoleName(userDetail.getRoleName());
					if(submenuList!=null) {
						for (Submenu submenu : submenuList) {
							userRights=new UserRights();
							userRights.setUsername(userDetail.getUsername());
							userRights.setMenuId(submenu.getMenuId());
							userRights.setSubmenuId(submenu.getSubmenuId());
							userRights.setStatus(true);
							userRights.setCreatedOn(new Date());
							userRights.setCreatedBy(user.getUsername());
							userRightsService.saveUserRights(userRights);
						}
					}
					mav.addObject("userDetail", new User());
					mav.addObject("successMessage", "User saved successfully");
					mav.setViewName("redirect:/geAllUserList");
				}
			}
		}
		return mav;
	}
	
	@RequestMapping(value="/downloadUserTemplate", method=RequestMethod.GET)
	public void downloadUserTemplate(HttpServletResponse response, HttpSession session) throws Exception {
		
		activityLog.info("downloadUserTemplate");
		String path=session.getServletContext().getRealPath("/resources/upload"); 
		Path file = Paths.get(path, "USER_LIST.xls");
        if (Files.exists(file)){
            response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition", "attachment; filename=USER_LIST.xlsx");
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
	}
	
	//view or edit show
	@RequestMapping(value="/uploadUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView uploadUser(User user, HttpSession session) throws Exception {
		
		activityLog.info("uploadUser");
		String path=session.getServletContext().getRealPath("/resources/upload"); 
		ModelAndView mav=new ModelAndView();
		User userDetail=(User)session.getAttribute("user");
		if(userDetail!=null) {
			MultipartFile multipartFile=user.getDocFile();
			String fileName="USER_LIST.xls";
			FileUtility.uploadFileToServer(multipartFile, fileName, path);
			List<User> userList=UploadUser.readUserFile(fileName, path);
			for (User user2 : userList) {
				User userexits=userService.getUserByUsername(user2.getUsername());
				if(userexits==null) {
					user2.setPassword(MD5Data.getMD5(user2.getPassword()));
					user2.setLocalFileLocation("D:\\DHC_DOWNLOAD");
					user2.setStatus(true);
					user2.setCreatedOn(new Date());
					user2.setCreatedBy(userDetail.getUsername());
					userService.saveUser(user2);
				}
			}
			mav.setViewName("redirect:/geAllUserList");
		}
		return mav;
	}
		
	//view or edit user
	@RequestMapping(value="/editUser", method=RequestMethod.GET)
	public ModelAndView viewOrEditUser(@RequestParam("id") Long id) {
		
		activityLog.info("editUser");
		ModelAndView mav=new ModelAndView();
		List<UserRole> userRoleList=userRoleService.getAllUserRoleList();
		List<User> userList=userService.getAllUserList();
		User userDetail=userService.getUserById(id);
		if(userDetail!=null) {
			mav.addObject("userList", userList);
			mav.addObject("userRoleList", userRoleList);
			mav.addObject("userDetail", userDetail);
			mav.setViewName("user/userList");
		}
		return mav;
	}
	
	@RequestMapping(value="/changePassword", method=RequestMethod.GET)
	public ModelAndView changePassword() throws Exception {
		
		activityLog.info("changePassword");
		ModelAndView mav=new ModelAndView();
		mav.addObject("users", new User());
		mav.setViewName("user/changePassword");
		return mav;
	}
	
	//view or edit show
	@RequestMapping(value="/saveChangePassword", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView saveChangePassword(@ModelAttribute("users") User users, Errors errors, HttpSession session) throws Exception {
		
		activityLog.info("saveChangePassword");
		ModelAndView mav=new ModelAndView();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			user.setPassword(users.getPassword());
			user.setReconfirmPassword(users.getReconfirmPassword());
			passwordValidator.validate(user, errors);
			if(errors.hasErrors()) {
				mav.setViewName("user/changePassword");
			}else {
				user.setPassword(MD5Data.getMD5(users.getPassword()));
				user.setUpdatedOn(new Date());
				user.setUpdatedBy(user.getUsername());
				boolean flag=userService.updateUser(user);
				if(flag) {
					mav.addObject("passwordmsg", "Password change successfully");
				}
			}
			mav.setViewName("user/changePassword");
		}
		return mav;
	}
	
}
