package com.dms.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dms.model.UserLog;
import com.dms.enumm.ActivityLog;
import com.dms.model.User;
import com.dms.service.UserLogService;
import com.dms.service.UserService;
import com.dms.utility.BrowserDetailUtility;
import com.dms.utility.MD5Data;

/**
 * 
 * @author Rajkumar Giri
 *
 */
@Controller
public class LoginController {
	
	private static Logger logger=Logger.getLogger(LoginController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserLogService userLogService; 
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String login(HttpSession session) {
		logger.info("Login page");
		activityLog.info("Get Login page");
		return "common/login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView login(@RequestParam("username") String username, @RequestParam("password") String password,
			 				HttpSession session, HttpServletRequest request) {
		
		activityLog.info("Post Login page");
		ModelAndView mav=new ModelAndView();
		session.removeAttribute("user");
		session.removeAttribute("logId");
		UserLog userLog=null;
		User user=userService.getUserByUsernameAndPassword(username, MD5Data.getMD5(password));
		if(user!=null){
			userLog=new UserLog();
			userLog.setUsername(user.getUsername());
			userLog.setIpAddress(request.getRemoteAddr());
			userLog.setBrowserDetails(BrowserDetailUtility.getBrowser(request));
			userLog.setInTime(new Date());
			
			
			
			Long userId=userLogService.saveUserLog(userLog);
			
			
			if(userId!=null) {
				session.setAttribute("userId", userId);
			}
			session.setAttribute("user", user);
			mav.setViewName("redirect:/home");
			logger.info("Logged in");
		}else{
			mav.addObject("errormsg","INVALID CREDENTIALS");
			mav.setViewName("common/login");
		}
		return mav;
	}
	
	/*user logout session invalidate*/
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {
		
		activityLog.info("Get Logout page");
		ModelAndView mav=new ModelAndView();
		Long logId=null;
		UserLog userLog=null;
		User user=(User)session.getAttribute("user");
		if(user!=null){
			logId=(Long)session.getAttribute("userId");
			if(logId!=null) {
				userLog=userLogService.getUserLogById(logId);
				if(userLog!=null) {
					userLog.setUsername(user.getUsername());
					userLog.setOutTime(new Date());
					userLogService.updateUserLog(userLog);
				}
			}
			session.invalidate();
		}
		mav.setViewName("redirect:/");
		return mav;
	}
	
}
