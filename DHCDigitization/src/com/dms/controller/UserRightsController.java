package com.dms.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.form.MenuForm;
import com.dms.model.Menu;
import com.dms.model.Submenu;
import com.dms.model.User;
import com.dms.model.UserRights;
import com.dms.service.MenuService;
import com.dms.service.SubmenuService;
import com.dms.service.UserRightsService;
import com.dms.service.UserRoleService;
import com.dms.service.UserService;

/**
 * 
 * @author Rajkumar Giri
 *
 */
@Controller
public class UserRightsController {
	
	private static Logger logger=Logger.getLogger(UserRightsController.class);
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private SubmenuService submenuService;
	
	@Autowired
	UserRightsService userRightsService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ActivityLog activityLog;
	
	@RequestMapping(value="/userRights", method=RequestMethod.GET)
	public ModelAndView userRights() {
		
		logger.info("");
		activityLog.info("userRights");
		ModelAndView mav=new ModelAndView();
		List<User> userList=userService.getAllUserList();
		mav.addObject("userList", userList);
		mav.setViewName("user/userRights");
		return mav;
	}
	
	@RequestMapping(value="/getUserRightsByUsername", method=RequestMethod.POST)
	public ModelAndView getUserRightsByUsername(@RequestParam("username") String username) {
		
		activityLog.info("getUserRightsByUsername");
		ModelAndView mav=new ModelAndView();
		MenuForm menuForm=null;
		List<Submenu> submenuList=null;
		List<Submenu> submenuList2=null;
		Submenu submenu=null;
		UserRights userRights=null;
		List<MenuForm> menuFormList=new ArrayList<MenuForm>();
		List<Menu> menuList=menuService.getMenuList();
		if(menuList!=null) {
			for (Menu menu : menuList) {
				submenuList2=new ArrayList<Submenu>();
				 menuForm=new MenuForm();
				 menuForm.setMenu(menu);
				 submenuList=submenuService.getAllSubmenuListByMenuId(menu.getMenuId());
				 for (Submenu submenu2 : submenuList) {
					 submenu=new Submenu();
				 	 userRights=userRightsService.getUserRightsByUsernameAndsubmenuId(username, menu.getMenuId(), submenu2.getSubmenuId());
				 	 submenu.setSubmenuId(submenu2.getSubmenuId());
				 	 submenu.setSubmenuName(submenu2.getSubmenuName());
				 	 if(userRights!=null) {
						 submenu.setStatus(userRights.getStatus());
					 }else {
						 submenu.setStatus(false);
					 }
				 	submenuList2.add(submenu);
				 }
				 menuForm.setSubmenuList(submenuList2);
				 menuFormList.add(menuForm);
			}
		}
		List<User> userList=userService.getAllUserList();
		mav.addObject("userList", userList);
		mav.addObject("username", username);
		mav.addObject("menuFormLists", menuFormList);
		mav.setViewName("user/userRights");
		return mav;
	}
	
	
	@ResponseBody
	@RequestMapping(value="/updateUserRights", method=RequestMethod.POST)
	public String updateUserRights(@RequestParam("username") String username, @RequestParam("menuId") Long menuId,
			 					@RequestParam("submenuId") Long submenuId, HttpSession session) {
		
		activityLog.info("updateUserRights");
		boolean flag=false;
		String msg="";
		UserRights userRights=null;
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			userRights=userRightsService.getUserRightsByUsernameAndsubmenuId(username, menuId, submenuId);
			if(userRights!=null) {
				if(userRights.getStatus().equals(true)) {
				   userRights.setStatus(false);
				}else {
					userRights.setStatus(true);
				}
				userRights.setUpdatedBy(user.getUsername());
				userRights.setUpdatedOn(new Date());
				flag=userRightsService.updateUserRights(userRights);
				if(flag) {
					msg="Updated successfully";
				}
			}else {
				userRights=new UserRights();
				userRights.setUsername(username);
				userRights.setMenuId(menuId);
				userRights.setSubmenuId(submenuId);
				userRights.setStatus(true);
				userRights.setCreatedOn(new Date());
				userRights.setCreatedBy(user.getUsername());
				Long id=userRightsService.saveUserRights(userRights);
				if(id>0) {
					msg="Saved successfully";
				}
			}
		}
		String str=String.valueOf(msg);
		return str;
	}
	
	
}
