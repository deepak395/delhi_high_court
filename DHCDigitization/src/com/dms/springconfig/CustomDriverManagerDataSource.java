package com.dms.springconfig;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import sun.misc.BASE64Decoder;

public class CustomDriverManagerDataSource extends DriverManagerDataSource {
	
	@Override
	public String getUsername() {
		String username=super.getUsername();
		return decode(username);
	}
	
	@Override
	public String getPassword() {
		String password = super.getPassword();
		return decode(password);
	}
	
	/***
	 * Decode username and password
	 */
	private String decode(String decode) {
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			decode = new String(decoder.decodeBuffer(decode));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decode;
	}

}
