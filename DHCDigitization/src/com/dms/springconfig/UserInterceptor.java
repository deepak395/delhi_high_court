package com.dms.springconfig;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.dms.enumm.ActivityLog;
import com.dms.model.User;
import com.dms.model.UserActivities;
import com.dms.service.UserActivitiesService;
import com.dms.utility.BrowserDetailUtility;


public class UserInterceptor implements HandlerInterceptor {
	
	@Autowired
	UserActivitiesService userActivitiesService;
	
	@Autowired
	ActivityLog activityLog;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
		
		HttpSession session=request.getSession();
		User user=(User)session.getAttribute("user");
		String homeURI = request.getContextPath() + "/";
	    String loginURI = request.getContextPath() + "/login";
        boolean loggedIn = session != null &&  user!= null;
        boolean homeRequest = request.getRequestURI().equals(homeURI);
        boolean loginRequest = request.getRequestURI().equals(loginURI);
        if(loggedIn || homeRequest || loginRequest) {
        	return true;
        }else {
        	response.sendRedirect(request.getContextPath());
        	return false;
        }
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj, ModelAndView mav) throws Exception {
		
	/*	HttpSession session=request.getSession();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			String path = request.getRequestURI().substring(request.getContextPath().length()+1);
			if(path!="") {
				String activities=activityLog.getMess(path);
				if(activities!="") {
					UserActivities userActivities=new UserActivities();
			    	userActivities.setUserName(user.getUsername());
			    	userActivities.setActivity(activityLog.getMess(path));
			    	userActivities.setIpAdress(request.getRemoteHost());
			    	userActivities.setBrowserDetails(BrowserDetailUtility.getBrowser(request));
			    	userActivities.setCreatedOn(new Date());
			     	if(userActivities!=null) {
			     		userActivitiesService.saveUserActivities(userActivities);
			     	}
				}
			}
		}*/
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception exception) throws Exception {
		
	}

}
