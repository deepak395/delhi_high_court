package com.dms.springconfig;

import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;


@Configuration
@EnableWebMvc
@EnableTransactionManagement
@EnableScheduling
@PropertySource("classpath:resources/database.properties")
@ComponentScan(basePackages="com.dms.*")
public class AppConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
	private Environment environment;
	 
	@Bean
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver viewResolver=new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
	@Bean
	public ResourceBundleViewResolver getResourceBundleViewResolver() {
		ResourceBundleViewResolver viewResolver=new ResourceBundleViewResolver();
		viewResolver.setBasename("resources/views");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
	@Bean
	public ResourceBundleMessageSource addResourceBundleMessageSource() {
		ResourceBundleMessageSource resourceBundleMessageSource=new ResourceBundleMessageSource();
		resourceBundleMessageSource.setBasename("resources/messages");
		return resourceBundleMessageSource;
	}
	
	@Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver cmr = new CommonsMultipartResolver();
        return cmr;
    }
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }    
	
	@Bean
	public BasicDataSource getEncryptedDataSource() {
	BasicDataSource ds=new BasicDataSource();
	ds.setDriverClassName(environment.getProperty("database.driver"));
	ds.setUrl(environment.getProperty("database.url"));
	ds.setUsername(environment.getProperty("database.user"));
	ds.setPassword(environment.getProperty("database.password"));
	ds.setInitialSize(10);
	ds.setMaxIdle(5);
	ds.setMinIdle(2);
	ds.setMaxTotal(10);
/*	ds.setTimeBetweenEvictionRunsMillis(30000);// evictor wait period
	ds.setRemoveAbandonedTimeout(150);
	ds.setMinEvictableIdleTimeMillis(30000);*/
	
	ds.setMaxOpenPreparedStatements(100);
	return ds;
	}
	/*
	@Bean
	public CustomDriverManagerDataSource getEncryptedDataSource() {
		CustomDriverManagerDataSource encryptedDataSource=new CustomDriverManagerDataSource();
		encryptedDataSource.setDriverClassName(environment.getProperty("database.driver"));
		encryptedDataSource.setUrl(environment.getProperty("database.url"));
		encryptedDataSource.setUsername(environment.getProperty("database.user"));
		encryptedDataSource.setPassword(environment.getProperty("database.password"));
		
		return encryptedDataSource;
	}
	*/
	@Bean
	public LocalSessionFactoryBean getLocalSessionFactoryBean() {
		LocalSessionFactoryBean localSessionFactoryBean=new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(getEncryptedDataSource());
		localSessionFactoryBean.setPackagesToScan("com.dms.model");
		Properties properties=new Properties();
		properties.put("hibernate.dialect", environment.getProperty("hibernate.dialect"));
	    properties.put("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
	    properties.put("hibernate.format_sql", environment.getProperty("hibernate.format_sql"));
	    properties.put("hibernate.connection.release_mode", environment.getProperty("hibernate.connection.release_mode"));
	   // properties.put("hibernate.c3p0.max_size", environment.getProperty("hibernate.c3p0.max_size"));
	   // properties.put("hibernate.c3p0.timeout", environment.getProperty("hibernate.c3p0.timeout"));
	   // properties.put("hibernate.c3p0.min_size", environment.getProperty("hibernate.c3p0.min_size"));
	   // properties.put("hibernate.c3p0.acquire_increment", environment.getProperty("hibernate.c3p0.acquire_increment"));
	   // properties.put("hibernate.connection.provider_class", environment.getProperty("hibernate.connection.provider_class"));
	    properties.put("hibernate.hbm2ddl.auto", "update");
	    localSessionFactoryBean.setHibernateProperties(properties);
		return localSessionFactoryBean;
	}
	
	@Bean
	public HibernateTransactionManager getHibernateTransactionManager() {
		HibernateTransactionManager hibernateTransactionManager=new HibernateTransactionManager();
		hibernateTransactionManager.setSessionFactory(getLocalSessionFactoryBean().getObject());
		return hibernateTransactionManager;
	}
	
	@Bean
	public UserInterceptor getUserInterceptor() {
		return new UserInterceptor();
	}
	
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(getUserInterceptor()).addPathPatterns("/**");
    }
	
}
