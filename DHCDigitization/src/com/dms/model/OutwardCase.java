package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="outward_case")
public class OutwardCase{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="outward_case_seq")
	@SequenceGenerator(name="outward_case_seq", sequenceName="outward_case_seq",allocationSize=1)
	@Column(name="oc_id")
	private Long outwardCaseId;
	
	@Column(name="case_no")
	private String caseNo;
	
	
	@Column(name="case_year")
	private String caseYear;
	
	
	@Column(name="case_category")
	private String caseCategory;
	
	
	@Column(name="outward_date")
	private String outward_date;
	
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	

	public Long getOutwardCaseId() {
		return outwardCaseId;
	}

	public void setOutwardCaseId(Long outwardCaseId) {
		this.outwardCaseId = outwardCaseId;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	
	public String getCaseYear() {
		return caseYear;
	}

	public void setCaseYear(String caseYear) {
		this.caseYear = caseYear;
	}

	
	public String getCaseCategory() {
		return caseCategory;
	}

	public void setCaseCategory(String caseCategory) {
		this.caseCategory = caseCategory;
	}


	public String getOutward_date() {
		return outward_date;
	}

	public void setOutward_date(String outward_date) {
		this.outward_date = outward_date;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
}