package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="delete_files_local")
public class DeleteFilesLocal {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="delete_files_local_generator")
	@SequenceGenerator(name="delete_files_local_generator", sequenceName="delete_files_local_sequence",allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="filename")
	private String fileName;
	
	@Column(name="isdeleted")
	private Boolean isDeleted;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="userid")
	private Long userId;
	
	@Column(name="mac_address")
	private String macAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Override
	public String toString() {
		return "DeleteFilesLocal [id=" + id + ", fileName=" + fileName + ", isDeleted=" + isDeleted + ", createdOn="
				+ createdOn + ", userId=" + userId + ", macAddress=" + macAddress + "]";
	}

	
	 

}
