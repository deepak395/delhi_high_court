package com.dms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="pending_case_document")
public class PendingCaseDocument {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pending_case_document_sequence")
	@SequenceGenerator(name="pending_case_document_sequence", sequenceName="pending_case_document_sequence",allocationSize=1)
	@Column(name="document_id")
	private Long documentId;
	
	@Column(name="case_no")
	private String caseNo;
	
	@Column(name="case_year")
	private String caseYear;
	
	@Column(name="case_category")
	private String caseCategory;
	
	@Column(name="case_type")
	private String caseType;
	
	@Column(name="document_name")
	private String documentName;
	
	@Column(name="scanning_pages")
	private Integer scanningPage;
	
	@Column(name="scanning_on")
	private Date scanningOn;
	
	@Column(name="scanning_by")
	private String scanningBy;
	
	@Column(name="qc_pages")
	private Integer qcPage;
	
	@Column(name="qc_on")
	private Date qcOn;
	
	@Column(name="qc_by")
	private String qcBy;
	
	@Column(name="qc_downloaded")
	private Boolean qcDownloaded;
	
	@Column(name="scanning_folder_id")
	private Long scanningFolderId;
	
	@Column(name="qc_folder_id")
	private Long qcFolderId;
	
	@Column(name="bookmarking_folder_id")
	private Long bookmarkingFolderId;
	
	@Column(name="case_stage")
	private String caseStage;
	
	@Column(name="status")
	private Boolean status;
	
	
	@Column(name="pending_case_id")
	private Long pendingCaseId;
	
	@Transient
	private List<MultipartFile> docFile;
	
	@Transient
	private String caseStageReport;

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getCaseYear() {
		return caseYear;
	}

	public void setCaseYear(String caseYear) {
		this.caseYear = caseYear;
	}

	public String getCaseCategory() {
		return caseCategory;
	}

	public void setCaseCategory(String caseCategory) {
		this.caseCategory = caseCategory;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Integer getScanningPage() {
		return scanningPage;
	}

	public void setScanningPage(Integer scanningPage) {
		this.scanningPage = scanningPage;
	}

	public Date getScanningOn() {
		return scanningOn;
	}

	public void setScanningOn(Date scanningOn) {
		this.scanningOn = scanningOn;
	}

	public String getScanningBy() {
		return scanningBy;
	}

	public void setScanningBy(String scanningBy) {
		this.scanningBy = scanningBy;
	}

	public Integer getQcPage() {
		return qcPage;
	}

	public void setQcPage(Integer qcPage) {
		this.qcPage = qcPage;
	}

	public Date getQcOn() {
		return qcOn;
	}

	public void setQcOn(Date qcOn) {
		this.qcOn = qcOn;
	}

	public String getQcBy() {
		return qcBy;
	}

	public void setQcBy(String qcBy) {
		this.qcBy = qcBy;
	}

	public Boolean getQcDownloaded() {
		return qcDownloaded;
	}

	public void setQcDownloaded(Boolean qcDownloaded) {
		this.qcDownloaded = qcDownloaded;
	}

	public Long getScanningFolderId() {
		return scanningFolderId;
	}

	public void setScanningFolderId(Long scanningFolderId) {
		this.scanningFolderId = scanningFolderId;
	}

	public Long getQcFolderId() {
		return qcFolderId;
	}

	public void setQcFolderId(Long qcFolderId) {
		this.qcFolderId = qcFolderId;
	}

	public Long getBookmarkingFolderId() {
		return bookmarkingFolderId;
	}

	public void setBookmarkingFolderId(Long bookmarkingFolderId) {
		this.bookmarkingFolderId = bookmarkingFolderId;
	}

	public String getCaseStage() {
		return caseStage;
	}

	public void setCaseStage(String caseStage) {
		this.caseStage = caseStage;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<MultipartFile> getDocFile() {
		return docFile;
	}

	public void setDocFile(List<MultipartFile> docFile) {
		this.docFile = docFile;
	}

	public String getCaseStageReport() {
		return caseStageReport;
	}

	public void setCaseStageReport(String caseStageReport) {
		this.caseStageReport = caseStageReport;
	}

	public Long getPendingCaseId() {
		return pendingCaseId;
	}

	public void setPendingCaseId(Long pendingCaseId) {
		this.pendingCaseId = pendingCaseId;
	}

	@Override
	public String toString() {
		return "PendingCaseDocument [documentId=" + documentId + ", caseNo=" + caseNo + ", caseYear=" + caseYear
				+ ", caseCategory=" + caseCategory + ", caseType=" + caseType + ", documentName=" + documentName
				+ ", scanningPage=" + scanningPage + ", scanningOn=" + scanningOn + ", scanningBy=" + scanningBy
				+ ", qcPage=" + qcPage + ", qcOn=" + qcOn + ", qcBy=" + qcBy + ", qcDownloaded=" + qcDownloaded
				+ ", scanningFolderId=" + scanningFolderId + ", qcFolderId=" + qcFolderId + ", bookmarkingFolderId="
				+ bookmarkingFolderId + ", caseStage=" + caseStage + ", status=" + status + ", pendingCaseId="
				+ pendingCaseId + ", docFile=" + docFile + ", caseStageReport=" + caseStageReport + "]";
	}

	
	

}
