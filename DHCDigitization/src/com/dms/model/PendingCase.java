package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pending_case")
public class PendingCase{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pending_case_sequence")
	@SequenceGenerator(name="pending_case_sequence", sequenceName="pending_case_sequence",allocationSize=1)
	@Column(name="pending_case_id")
	private Long pendingCaseId;
	
	@Column(name="case_no")
	private String caseNo;
	
	@Column(name="court_no")
	private String courtNo;
	
	@Column(name="case_year")
	private String caseYear;
	
	@Column(name="total_file")
	private String totalFile;
	
	@Column(name="case_category")
	private String caseCategory;
	
	@Column(name="case_type")
	private String caseType;
	
	@Column(name="hearing_date")
	private String hearingDate;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="dealing")
	private String dealing;
	
	@Column(name="scan_file")
	private Integer scanFile;
	
	@Column(name="qc_file")
	private Integer qcFile;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="case_stage")
	private String caseStage;
	
	@Column(name="status")
	private Boolean status;
	
	@Column(name="scanned_on")
	private Date scannedOn;
	
	@Column(name="scanned_by")
	private String scannedBy;
	
	@Column(name="qc_on")
	private Date qcOn;
	
	@Column(name="qc_by")
	private String qcBy;
	
	@Column(name="scanner_fullname")
	private String scannerFullname;
	
	@Column(name="qc_fullname")
	private String qcFullname;
	
	@Column(name="assign_to")
	private String assignTo;
	
	@Column(name ="accepted_by_scanner")
	private Boolean acceptedByScanner;
	
	
	
	@Column(name="hearing_report")
	private Date hearing_report;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Date getHearing_report() {
		return hearing_report;
	}

	public void setHearing_report(Date hearing_report) {
		this.hearing_report = hearing_report;
	}

	@Transient
	private String ifRegulor;
	
	@Transient
	private String errormMsg;
	
	
	

	
	
	
	

	

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public Date getScannedOn() {
		return scannedOn;
	}

	public void setScannedOn(Date scannedOn) {
		this.scannedOn = scannedOn;
	}

	public String getScannedBy() {
		return scannedBy;
	}

	public void setScannedBy(String scannedBy) {
		this.scannedBy = scannedBy;
	}

	public Date getQcOn() {
		return qcOn;
	}

	public void setQcOn(Date qcOn) {
		this.qcOn = qcOn;
	}

	public String getQcBy() {
		return qcBy;
	}

	public void setQcBy(String qcBy) {
		this.qcBy = qcBy;
	}

	public String getScannerFullname() {
		return scannerFullname;
	}

	public void setScannerFullname(String scannerFullname) {
		this.scannerFullname = scannerFullname;
	}

	public String getQcFullname() {
		return qcFullname;
	}

	public void setQcFullname(String qcFullname) {
		this.qcFullname = qcFullname;
	}

	public Long getPendingCaseId() {
		return pendingCaseId;
	}

	public void setPendingCaseId(Long pendingCaseId) {
		this.pendingCaseId = pendingCaseId;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getCourtNo() {
		return courtNo;
	}

	public void setCourtNo(String courtNo) {
		this.courtNo = courtNo;
	}

	public String getCaseYear() {
		return caseYear;
	}

	public void setCaseYear(String caseYear) {
		this.caseYear = caseYear;
	}

	public String getTotalFile() {
		return totalFile;
	}

	public void setTotalFile(String totalFile) {
		this.totalFile = totalFile;
	}

	public String getCaseCategory() {
		return caseCategory;
	}

	public void setCaseCategory(String caseCategory) {
		this.caseCategory = caseCategory;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getHearingDate() {
		return hearingDate;
	}

	public void setHearingDate(String hearingDate) {
		this.hearingDate = hearingDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDealing() {
		return dealing;
	}

	public void setDealing(String dealing) {
		this.dealing = dealing;
	}

	public Integer getScanFile() {
		return scanFile;
	}

	public void setScanFile(Integer scanFile) {
		this.scanFile = scanFile;
	}

	public Integer getQcFile() {
		return qcFile;
	}

	public void setQcFile(Integer qcFile) {
		this.qcFile = qcFile;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCaseStage() {
		return caseStage;
	}

	public void setCaseStage(String caseStage) {
		this.caseStage = caseStage;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getIfRegulor() {
		return ifRegulor;
	}

	public void setIfRegulor(String ifRegulor) {
		this.ifRegulor = ifRegulor;
	}

	public String getErrormMsg() {
		return errormMsg;
	}

	public void setErrormMsg(String errormMsg) {
		this.errormMsg = errormMsg;
	}
	

	public Boolean getAcceptedByScanner() {
		return acceptedByScanner;
	}

	public void setAcceptedByScanner(Boolean acceptedByScanner) {
		this.acceptedByScanner = acceptedByScanner;
	}

	@Override
	public String toString() {
		return "PendingCase [pendingCaseId=" + pendingCaseId + ", caseNo=" + caseNo + ", courtNo=" + courtNo
				+ ", caseYear=" + caseYear + ", totalFile=" + totalFile + ", caseCategory=" + caseCategory
				+ ", caseType=" + caseType + ", hearingDate=" + hearingDate + ", remarks=" + remarks + ", dealing="
				+ dealing + ", scanFile=" + scanFile + ", qcFile=" + qcFile + ", createdOn=" + createdOn
				+ ", createdBy=" + createdBy + ", updatedOn=" + updatedOn + ", updatedBy=" + updatedBy + ", caseStage="
				+ caseStage + ", status=" + status + ", scannedOn=" + scannedOn + ", scannedBy=" + scannedBy + ", qcOn="
				+ qcOn + ", qcBy=" + qcBy + ", scannerFullname=" + scannerFullname + ", qcFullname=" + qcFullname
				+ ", assignTo=" + assignTo + ", acceptedByScanner=" + acceptedByScanner + ", ifRegulor=" + ifRegulor
				+ ", errormMsg=" + errormMsg + "]";
	}

	

	
	

	

}