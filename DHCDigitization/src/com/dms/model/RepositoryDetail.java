package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="repository_master")
public class RepositoryDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="repository_generator")
	@SequenceGenerator(name="repository_generator", sequenceName="repository_sequence",allocationSize=1)
	@Column(name="repository_id")
	private Long repositoryId;
	
	@Column(name="repository_name")
	private String repositoryName;
	
	@Column(name="repository_path")
	private String repositoryPath;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="status")
	private Boolean status;

	public Long getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(Long repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public String getRepositoryPath() {
		return repositoryPath;
	}

	public void setRepositoryPath(String repositoryPath) {
		this.repositoryPath = repositoryPath;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "RepositoryDetail [repositoryId=" + repositoryId + ", repositoryName=" + repositoryName
				+ ", repositoryPath=" + repositoryPath + ", createdOn=" + createdOn + ", createdBy=" + createdBy
				+ ", status=" + status + "]";
	}


}