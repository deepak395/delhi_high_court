package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="user_log")
public class UserLog {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="userlog_sequence")
	@SequenceGenerator(name="userlog_sequence", sequenceName="userlog_sequence",allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="username")
	private String username;
	
	@Column(name="ip_address")
	private String ipAddress;
	
	@Column(name="in_time")
	private Date inTime;
	
	@Column(name="out_time")
	private Date outTime;
	
	@Column(name="browser_details")
	private String browserDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public String getBrowserDetails() {
		return browserDetails;
	}

	public void setBrowserDetails(String browserDetails) {
		this.browserDetails = browserDetails;
	}

	@Override
	public String toString() {
		return "UserLog [id=" + id + ", username=" + username + ", ipAddress=" + ipAddress + ", inTime=" + inTime
				+ ", outTime=" + outTime + ", browserDetails=" + browserDetails + "]";
	}

}
