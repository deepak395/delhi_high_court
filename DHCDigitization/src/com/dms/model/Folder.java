package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * 
 * @author Rajkumar Giri
 *
 */
@Entity
@Table(name="folder_master")
public class Folder {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="folder_generator")
	@SequenceGenerator(name="folder_generator", sequenceName="folder_sequence",allocationSize=1)
	@Column(name="folder_id")
	private Long folderId;
	
	@Column(name="folder_name")
	private String folderName;
	
	@Column(name="parent_folder_id")
	private Long parentFolderId;
	
	@Column(name="repository_id")
	private Long repositoryId;
	
	@Column(name="folder_type")
	private String folderType;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="status")
	private Boolean status;

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public Long getParentFolderId() {
		return parentFolderId;
	}

	public void setParentFolderId(Long parentFolderId) {
		this.parentFolderId = parentFolderId;
	}

	public Long getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(Long repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getFolderType() {
		return folderType;
	}

	public void setFolderType(String folderType) {
		this.folderType = folderType;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Folder [folderId=" + folderId + ", folderName=" + folderName + ", parentFolderId=" + parentFolderId
				+ ", repositoryId=" + repositoryId + ", folderType=" + folderType + ", createdOn=" + createdOn
				+ ", createdBy=" + createdBy + ", status=" + status + "]";
	}

}
