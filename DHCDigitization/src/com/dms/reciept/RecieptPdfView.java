package com.dms.reciept;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dms.model.InvoiceDetail;
import com.dms.utility.FinancialYearCalculator;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;


/**
 * 
 * @author Rajkumar Giri and Rupnarayan
 *
 */
public class RecieptPdfView extends AbstractITextPdfView{
	

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		SimpleDateFormat dateformat=new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat timeformat=new SimpleDateFormat("hh:mm:ss");
		DecimalFormat decimalFormat = new DecimalFormat("0.#####");

		String finYear=FinancialYearCalculator.getCurrentFinancialYear();
		String first2 = String.valueOf(finYear).substring(2,4);
		String last2 = String.valueOf(finYear).substring(7,9);
		String invoice="DL"+first2+last2+"DI";
		
		Paragraph paragraph=null;
		InvoiceDetail freshCase=(InvoiceDetail)model.get("invoiceDetail");
		
        Font smallFont = FontFactory.getFont(FontFactory.HELVETICA);
        smallFont.setSize(9);
        
        Font mediaumFont = FontFactory.getFont(FontFactory.HELVETICA);
        mediaumFont.setSize(10);
        
        Font mediaumBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        mediaumBoldFont.setSize(10);
        
        paragraph=new Paragraph(new Chunk("* To be filed with the Pet./Appl./Doc *", smallFont));
        paragraph.setAlignment(Element.ALIGN_CENTER);				
		document.add(paragraph);
        
		paragraph=new Paragraph(new Chunk("Scanning Fee Charges", mediaumBoldFont));
		paragraph.setAlignment(Element.ALIGN_CENTER);	
		document.add(paragraph);
		document.add(new Chunk(new LineSeparator()));
		
		paragraph = new Paragraph(new Chunk("STOCKHOLDING DMS LTD.",mediaumBoldFont));
		paragraph.setAlignment(Element.ALIGN_CENTER);
		document.add(paragraph);
		
		paragraph = new Paragraph(new Chunk("HIGH COURT OF DELHI",mediaumBoldFont));
		paragraph.setAlignment(Element.ALIGN_CENTER);
		document.add(paragraph);
		
		paragraph=new Paragraph(new Chunk("NEW DELHI-110003",mediaumBoldFont));
		paragraph.setAlignment(Element.ALIGN_CENTER);				
		document.add(paragraph);

		paragraph=new Paragraph(new Chunk("CASH MEMO",mediaumBoldFont));
		paragraph.setAlignment(Element.ALIGN_CENTER);
		document.add(paragraph);
		document.add(new Chunk(new LineSeparator()));
         
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100.0f);
		table.setWidths(new float[] {30.0f,30.0f,20.0f,20.0f});
		table.setSpacingBefore(10);
         
		table.addCell(getCell("Inovice No", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(invoice+freshCase.getInvoiceNo(), PdfPCell.ALIGN_LEFT, smallFont)).setColspan(3);
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("HC Bill No", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getBillNo(), PdfPCell.ALIGN_LEFT, smallFont)).setColspan(3);
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Date", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(dateformat.format(freshCase.getCreatedOn()), PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("Time   "+timeformat.format(freshCase.getCreatedOn()), PdfPCell.ALIGN_LEFT, smallFont)).setColspan(2);;
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Case Type", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getCaseType(), PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getCreatedBy(), PdfPCell.ALIGN_LEFT, smallFont)).setColspan(2);;
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Name", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getClientName(), PdfPCell.ALIGN_LEFT, smallFont)).setColspan(3);
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Case No", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getCaseNo(), PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Pages", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getNoOfPage().toString(), PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Total Amount", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("Rs. "+decimalFormat.format(freshCase.getAmount())+" /- (Inclusive of GST)", PdfPCell.ALIGN_LEFT, smallFont)).setColspan(3);
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Auto", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getAutoNo().toString(), PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		table.addCell(getCell("Payment Mode", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell(freshCase.getPaymentMode(), PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		table.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		
		document.add(table);
		document.add(new Chunk(new LineSeparator()));
		
		paragraph=new Paragraph(new Chunk("**--THANK YOU--**", mediaumFont));
	    paragraph.setAlignment(Element.ALIGN_CENTER);				
	    document.add(paragraph);

	}
	
	public PdfPCell getCell(String text, int alignment, Font textFontSize) {
	    PdfPCell cell = new PdfPCell(new Phrase(text,textFontSize));
	    cell.setPaddingTop(5);
	    cell.setHorizontalAlignment(alignment);
	    cell.setBorder(PdfPCell.NO_BORDER);
	    return cell;
	}

	

}
