package com.dms.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dms.model.PendingCase;
import com.dms.service.PendingCaseService;


@Component
public class PendingCaseValidator implements Validator{
	
	@Autowired
	PendingCaseService pendingCaseService;
	
	@Override
	public boolean supports(Class<?> pendingCaseclz) {
		return PendingCase.class.equals(pendingCaseclz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PendingCase pendingCase=(PendingCase)target;
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		Date date=new Date();
	
		String NUM_REGEX = "^[0-9]+$";
		if (StringUtils.isEmpty(pendingCase.getCaseNo())) {
			errors.rejectValue("caseNo", null, "Please enter case no !");
		}
		/*else if (!Pattern.matches(NUM_REGEX, pendingCase.getCaseNo().toString())) {
			errors.rejectValue("caseNo", null, "please enter numaric value only");
		}*/
		if(StringUtils.isEmpty(pendingCase.getCaseType())){
			errors.rejectValue("caseType", null, "Please enter case type !");
		}
		if(StringUtils.isEmpty(pendingCase.getCaseCategory())){
			errors.rejectValue("caseCategory", null, "Please select one !");
		}
		if(StringUtils.isEmpty(pendingCase.getCaseYear())){
			errors.rejectValue("caseYear", null, "Please enter case year !");
		}else if (!Pattern.matches(NUM_REGEX, pendingCase.getCaseYear().toString())) {
			errors.rejectValue("caseYear", null, "please enter numaric value only");
		}
		if(StringUtils.isEmpty(pendingCase.getHearingDate())){
			errors.rejectValue("hearingDate", null, "Please enter hearing date !");
		}else if(pendingCase.getIfRegulor()==null){
			try {
				Date todayDate=df.parse(df.format(date));
				Date hearingdate=df.parse(pendingCase.getHearingDate());
				if(todayDate.after(hearingdate)) {
					errors.rejectValue("hearingDate", null, "Invalid hearing date !");
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		/*if(pendingCaseService.checkPendingCase(pendingCase.getCaseNo(), pendingCase.getCaseYear(), pendingCase.getHearingDate())) {
			errors.rejectValue("errormMsg", null, "case already exit !");
		}*/
	}

}
