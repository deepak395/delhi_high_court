package com.dms.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dms.model.User;


@Component
public class PasswordValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> userclz) {
		return User.class.equals(userclz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user=(User)target;
		
		if (!StringUtils.hasText(user.getUsername().trim())) {
			errors.rejectValue("username", null, "Please enter your name !");
		} 
		if (StringUtils.isEmpty(user.getPassword())) {
			errors.rejectValue("password", null, "Please Enter password!!");
		}
		if (!StringUtils.pathEquals(user.getPassword(), user.getReconfirmPassword())) {
			errors.rejectValue("reconfirmPassword", null, "Password not match!!");
		}
			
	}

}
