package com.dms.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dms.model.FreshCase;
import com.dms.service.FreshCaseService;

@Component
public class FreshCaseValidator implements Validator{
	
	@Autowired
	FreshCaseService freshCaseService;
	
	@Override
	public boolean supports(Class<?> freshCaseclz) {
		return FreshCase.class.equals(freshCaseclz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		FreshCase freshCase=(FreshCase)target;
		
		if (StringUtils.isEmpty(freshCase.getAutoNo())) {
			errors.rejectValue("autoNo", null, "Please enter auto no");
		}
		if(StringUtils.isEmpty(freshCase.getCaseType())){
			errors.rejectValue("caseType", null, "Please select case type");
		}
		if (StringUtils.isEmpty(freshCase.getDiaryNo())) {
			errors.rejectValue("diaryNo", null, "Filename doesn't exist diary no");
		}
		if (StringUtils.isEmpty(freshCase.getYear())) {
			errors.rejectValue("year", null, "Filename doesn't exist year");
		}
		if (freshCaseService.getFreshCaseByDiaryNo(freshCase.getDiaryNo())!=null) {
			errors.rejectValue("diaryNo", null, "This file already exist please select another file");
		}
	/*	if(freshCase.getDocFile()!=null) {
			if(freshCase.getDocFile().getSize()==0) {
				errors.rejectValue("docFile", null, "Please select file");
			}
		}*/
    }
}