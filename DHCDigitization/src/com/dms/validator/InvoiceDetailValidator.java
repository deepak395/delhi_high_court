package com.dms.validator;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dms.model.InvoiceDetail;


@Component
public class InvoiceDetailValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> freshCaseclz) {
		return InvoiceDetail.class.equals(freshCaseclz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		InvoiceDetail freshCase=(InvoiceDetail)target;
		
		if(StringUtils.isEmpty(freshCase.getCaseType())){
			errors.rejectValue("caseType", null, "Please enter case type");
		}
		String CHAR_REGEX = "[a-zA-Z\\s]+";
		if(StringUtils.isEmpty(freshCase.getClientName())){
			errors.rejectValue("clientName", null, "Please enter client name");
		}/*else if (!Pattern.matches(CHAR_REGEX, freshCase.getClientName())) {
			errors.rejectValue("clientName", null, "please enter character only");
		}*/
		if(StringUtils.isEmpty(freshCase.getCaseNo())){
			errors.rejectValue("caseNo", null, "Please enter case no");
		}
		String NUM_REGEX = "^[0-9]+$";
		if(StringUtils.isEmpty(freshCase.getNoOfPage())){
			errors.rejectValue("noOfPage", null, "Please enter no of page");
		}else if (!Pattern.matches(NUM_REGEX, freshCase.getNoOfPage().toString())) {
			errors.rejectValue("noOfPage", null, "please enter numaric only");
		}
		if (StringUtils.isEmpty(freshCase.getAmount())) {
			errors.rejectValue("amount", null, "Please enter Amount");
		}
	}

}
