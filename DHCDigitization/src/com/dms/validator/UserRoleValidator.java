package com.dms.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dms.model.UserRole;


@Component
public class UserRoleValidator implements Validator{

	@Override
	public boolean supports(Class<?> userRoleClass) {
		return UserRole.class.equals(userRoleClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		
		UserRole userRole=(UserRole)obj;
		if (StringUtils.isEmpty(userRole.getRoleName())) {
			errors.rejectValue("roleName", null, "Please Enter User Role!!");
		}
				
				
		
	}

}
