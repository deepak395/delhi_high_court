package com.dms.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dms.model.User;


@Component
public class UserValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> userclz) {
		return User.class.equals(userclz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user=(User)target;
		
    	//check id type (drop down) is empty or not
		if (StringUtils.isEmpty(user.getRoleName())) {
			errors.rejectValue("roleName", null, "Please select choose any one !");
		} 
		//check id type (drop down) is empty or not
		if (StringUtils.isEmpty(user.getUserFullName())) {
			errors.rejectValue("userFullName", null, "Please enter name!");
		} 
		// checking userRole type is empty or not
		if (!StringUtils.hasText(user.getUsername().trim())) {
			errors.rejectValue("username", null, "Please enter your name !");
		} 
		if (StringUtils.isEmpty(user.getPassword())) {
			errors.rejectValue("password", null, "please Enter password!!");
		}
		if (!StringUtils.pathEquals(user.getPassword(), user.getReconfirmPassword())) {
			errors.rejectValue("reconfirmPassword", null, "Password not match!!");
		}
		if (StringUtils.isEmpty(user.getLocalFileLocation())) {
			errors.rejectValue("localFileLocation", null, "Please Enter Source Pdf Path");
		}
				
			
	}


}
