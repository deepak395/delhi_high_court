package com.dms.report;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.dms.form.PendingCaseForm;
import com.itextpdf.text.Element;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * 
 * @author Rajkumar Giri and Rupnarayan
 *
 */
public class PendingCasePdfView extends AbstractPdfView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		    
		  
		    // read date from model fdata
			List<PendingCaseForm> pendingCaseFormList=(List<PendingCaseForm>) model.get("pendingCaseFormList");
		   //set file name
	    	response.setHeader("Content-Disposition", "attachment;filename=PENDING_CASE.pdf");
			//create any element
	    	Paragraph paragraph=null;
	    	paragraph = new Paragraph(new Chunk("HIGH COURT OF DELHI"));
			paragraph.setAlignment(Element.ALIGN_CENTER);
			document.add(paragraph);
			document.add(new Paragraph("PENDING CASE"));
				
				PdfPTable pdfPTable=new PdfPTable(5);
				pdfPTable.addCell("CASE NO");
				pdfPTable.addCell("CASE YEAR");
				pdfPTable.addCell("CATAGORY");
				pdfPTable.addCell("TYPE");
				pdfPTable.addCell("DOC NAME");
				
				for(PendingCaseForm pendingCaseForm:pendingCaseFormList){
					pdfPTable.addCell(pendingCaseForm.getPendingCase().getCaseNo());
					pdfPTable.addCell(pendingCaseForm.getPendingCase().getCaseYear());
					pdfPTable.addCell(pendingCaseForm.getPendingCase().getCaseCategory());
					pdfPTable.addCell(pendingCaseForm.getPendingCase().getCaseType());
					pdfPTable.addCell(pendingCaseForm.getPendingCaseDocument().getDocumentName());
					
				}
				document.add(pdfPTable);
				document.add(new Paragraph("Genarated on "+new Date().toString()));
	}

}
