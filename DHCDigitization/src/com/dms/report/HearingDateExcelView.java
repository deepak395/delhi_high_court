package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.model.PendingCase;

public class HearingDateExcelView  extends AbstractExcelView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, 
			HttpServletResponse response)throws Exception {
			
        response.addHeader("Content-Disposition", "attachment; filename=HEARING_REPORT.xls");
		List<PendingCase> pendingCaseList=(List<PendingCase>) model.get("pendingCaseList");
		HSSFSheet excelSheet = workbook.createSheet("PendingCaseList");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, pendingCaseList);
		
	}
	
	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
   		excelHeader.createCell(1).setCellValue("CASE NO");
		excelHeader.createCell(2).setCellValue("CASE YEAR");
		excelHeader.createCell(3).setCellValue("CASE CATEGORY");
		excelHeader.createCell(4).setCellValue("CASE TYPE");
		excelHeader.createCell(5).setCellValue("COURT NO");
		excelHeader.createCell(6).setCellValue("HEARING DATE");
		excelHeader.createCell(7).setCellValue("DATA ENTRY DATE");
		excelHeader.createCell(8).setCellValue("SCANNED DATE");
		excelHeader.createCell(9).setCellValue("SCANNED BY");
		excelHeader.createCell(10).setCellValue("QC DATE");
		excelHeader.createCell(11).setCellValue("QC BY");
	

		
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<PendingCase> PendingCaseList){
		int record = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");  
	    
           String scandate=null;
           String qcdate=null;
           String dataEntry=null;
           
           if(PendingCaseList!=null) {
		for (PendingCase pendingCase: PendingCaseList) {
			HSSFRow excelRow = excelSheet.createRow(record++);
		           qcdate= new String();
		           scandate= new String();
		           dataEntry= new String();
		           if(pendingCase.getScannedOn()!=null)
		           scandate=formatter.format(pendingCase.getScannedOn());
		           if(pendingCase.getQcOn()!=null)
		           qcdate=formatter.format((pendingCase.getQcOn()));
		           dataEntry=formatter.format(pendingCase.getCreatedOn());
			excelRow.createCell(1).setCellValue(pendingCase.getCaseNo());
			excelRow.createCell(2).setCellValue(pendingCase.getCaseYear());
			excelRow.createCell(3).setCellValue(pendingCase.getCaseCategory());
			excelRow.createCell(4).setCellValue(pendingCase.getCaseType());
			if(pendingCase.getCourtNo()!=null)
			excelRow.createCell(5).setCellValue(pendingCase.getCourtNo());
			excelRow.createCell(6).setCellValue(pendingCase.getHearingDate());
			excelRow.createCell(7).setCellValue(dataEntry);
			excelRow.createCell(8).setCellValue(scandate);
			excelRow.createCell(9).setCellValue(pendingCase.getScannerFullname());
			excelRow.createCell(10).setCellValue(qcdate);
			excelRow.createCell(11).setCellValue(pendingCase.getQcFullname());
			
		}
			

		}
	}
	
}
	
