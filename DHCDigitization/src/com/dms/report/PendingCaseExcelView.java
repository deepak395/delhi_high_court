package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.form.PendingCaseForm;
import com.dms.model.PendingCaseDocument;



public class PendingCaseExcelView extends AbstractExcelView{

	@Autowired
	static PendingCaseDocument pendingCaseDocument;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = simpleDateFormat.format( new Date());
		
        response.addHeader("Content-Disposition", "attachment; filename=PENDING_CASE_"+currentDate+".xls");
		List<PendingCaseForm> pendingCaseFormList=(List<PendingCaseForm>) model.get("pendingCaseFormList");
		pendingCaseDocument = pendingCaseFormList.get(0).getPendingCaseDocument();
		System.out.println("Pending Case reporting stage " + pendingCaseDocument.getCaseStageReport());
		
		HSSFSheet excelSheet = workbook.createSheet("Pending Case List");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, pendingCaseFormList);
		
	}
	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("CASE NO");
		excelHeader.createCell(1).setCellValue("CASE YEAR");
		excelHeader.createCell(2).setCellValue("CASE CATEGORY");
		excelHeader.createCell(3).setCellValue("CASE TYPE");
		excelHeader.createCell(4).setCellValue("DOCUMENT NAME");
		excelHeader.createCell(5).setCellValue("DEALING");
		excelHeader.createCell(6).setCellValue("TOTAL FILE");
		
		int  columnIndex = 7;
		if (pendingCaseDocument.getCaseStageReport().contains("SCANNING"))
		{
			excelHeader.createCell(columnIndex++).setCellValue("SCANNED DATE");
	    	excelHeader.createCell(columnIndex++).setCellValue("SCANNED BY");
	    	excelHeader.createCell(columnIndex++).setCellValue("NO OF PAGES SCANNED");
		}
		if (pendingCaseDocument.getCaseStageReport().contains("QC"))
		{
		excelHeader.createCell(columnIndex++).setCellValue("QC DATE");
		excelHeader.createCell(columnIndex++).setCellValue("QC BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF PAGES QCED");
		}
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<PendingCaseForm> pendingCaseFormList){
		int record = 1;
		for (PendingCaseForm pendingCaseForm : pendingCaseFormList) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			if(pendingCaseForm.getPendingCase().getCaseNo()!=null)
			excelRow.createCell(0).setCellValue(pendingCaseForm.getPendingCase().getCaseNo());
			excelRow.createCell(1).setCellValue(pendingCaseForm.getPendingCase().getCaseYear());
			excelRow.createCell(2).setCellValue(pendingCaseForm.getPendingCase().getCaseCategory());
			excelRow.createCell(3).setCellValue(pendingCaseForm.getPendingCase().getCaseType());
			excelRow.createCell(4).setCellValue(pendingCaseForm.getPendingCaseDocument().getDocumentName());
			excelRow.createCell(5).setCellValue(pendingCaseForm.getPendingCase().getDealing());
			excelRow.createCell(6).setCellValue(pendingCaseForm.getPendingCase().getTotalFile());
			int  columnIndex = 7;
			if (pendingCaseDocument.getCaseStageReport().contains("SCANNING"))
			{
			excelRow.createCell(columnIndex++).setCellValue(pendingCaseForm.getPendingCaseDocument().getScanningOn()!=null ? pendingCaseForm.getPendingCaseDocument().getScanningOn().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(pendingCaseForm.getPendingCaseDocument().getScanningBy()!=null ? pendingCaseForm.getPendingCaseDocument().getScanningBy().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(pendingCaseForm.getPendingCaseDocument().getScanningPage()!=null ? pendingCaseForm.getPendingCaseDocument().getScanningPage().toString():" ");

			}
			if (pendingCaseDocument.getCaseStageReport().contains("QC"))
			{
			excelRow.createCell(columnIndex++).setCellValue(pendingCaseForm.getPendingCaseDocument().getQcOn()!=null ? pendingCaseForm.getPendingCaseDocument().getQcOn().toString() : " ");
			excelRow.createCell(columnIndex++).setCellValue(pendingCaseForm.getPendingCaseDocument().getQcBy()!=null ?pendingCaseForm.getPendingCaseDocument().getQcBy().toString() : " ");
			excelRow.createCell(columnIndex++).setCellValue(pendingCaseForm.getPendingCaseDocument().getQcPage()!=null ?pendingCaseForm.getPendingCaseDocument().getQcPage().toString() : " ");
			}
		}
	}
	
}
	
