package com.dms.report;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.dms.model.InvoiceDetail;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * 
 * @author Rajkumar Giri and Rupnarayan
 *
 */
public class InvoiceCancelDetailReportPdfView_Sameer extends AbstractPdfView{
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	String currentDate = simpleDateFormat.format( new Date());

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		    
		  
		    // read date from model fdata
			List<InvoiceDetail> invoiceDetailList=(List<InvoiceDetail>) model.get("invoiceDetailList");
			DecimalFormat decimalFormat = new DecimalFormat("0.#####");
			int totalPage=0;
			Double totalAmount=0d;
			for (InvoiceDetail invoiceDetail : invoiceDetailList) {
				totalPage=totalPage+invoiceDetail.getNoOfPage();
				totalAmount=totalAmount+invoiceDetail.getAmount();
			}
			
		   //set file name
	    	response.setHeader("Content-Disposition", "attachment;filename=BILL_"+currentDate+".pdf");
				//create any element
	    	 Font smallFont = FontFactory.getFont(FontFactory.HELVETICA);
	         smallFont.setSize(9);
	    	
	    	Paragraph paragraph=null;
	    	paragraph = new Paragraph(new Chunk("HIGH COURT OF DELHI"));
			paragraph.setAlignment(Element.ALIGN_CENTER);
			document.add(paragraph);
			document.add(new Paragraph("CANCELLED INVOICE"));
				
				PdfPTable pdfPTable=new PdfPTable(9);
				pdfPTable.setWidthPercentage(100.0f);
				pdfPTable.setWidths(new float[] {1.5f, 3.0f, 2.0f, 1.0f, 2.0f, 2.0f, 2.0f,1.5f, 1.0f});
				pdfPTable.setSpacingBefore(10);
				pdfPTable.addCell("AUTO NO");
				pdfPTable.addCell("BILL NO");
				pdfPTable.addCell("CLIENT NAME");
				pdfPTable.addCell("CASE NO");
				pdfPTable.addCell("CASE TYPE");
				pdfPTable.addCell("CANCELLED BY");
				pdfPTable.addCell("CANCELLED ON");
				pdfPTable.addCell("NO OF PAGES");
				pdfPTable.addCell("AMOUNT");
				
				
				
				for(InvoiceDetail invoice:invoiceDetailList){
					pdfPTable.addCell(invoice.getAutoNo().toString());
					pdfPTable.addCell(invoice.getBillNo());
					pdfPTable.addCell(invoice.getClientName());
					pdfPTable.addCell(invoice.getCaseNo());
					pdfPTable.addCell(invoice.getCaseType());
					pdfPTable.addCell(invoice.getCanceledBy());
					pdfPTable.addCell(simpleDateFormat.format(invoice.getCanceledOn()).toString());
					pdfPTable.addCell(invoice.getNoOfPage().toString());
					pdfPTable.addCell(decimalFormat.format(invoice.getAmount()));
				}
				
				document.add(pdfPTable);
				document.add(new Paragraph("TOTAL PAGE     "+totalPage));
				document.add(new Paragraph("TOTAL AMOUNT   "+decimalFormat.format(totalAmount)));
	}

}
