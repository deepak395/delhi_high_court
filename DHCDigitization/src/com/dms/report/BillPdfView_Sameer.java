package com.dms.report;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.dms.model.InvoiceDetail;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * 
 * @author Rajkumar Giri and Rupnarayan
 *
 */
// Radhika -- for total calculation
public class BillPdfView_Sameer extends AbstractPdfView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		    
		  
		    // read date from model fdata
			List<InvoiceDetail> invoiceDetailList=(List<InvoiceDetail>) model.get("invoiceDetailList");
		   //set file name
	    	response.setHeader("Content-Disposition", "attachment;filename=BILL.pdf");
				//create any element
	    	 Font smallFont = FontFactory.getFont(FontFactory.HELVETICA);
	         smallFont.setSize(9);
	    	
	    	Paragraph paragraph=null;
	    	paragraph = new Paragraph(new Chunk("HIGH COURT OF DELHI"));
			paragraph.setAlignment(Element.ALIGN_CENTER);
			document.add(paragraph);
			// Summation of pages and Amount
			Double sumAmount =0.0;
			Integer sumTotalPages = 0;
			
			document.add(new Paragraph("         INVOICE"));
				
				PdfPTable pdfPTable=new PdfPTable(8);
				pdfPTable.addCell("AUTO NO");
				pdfPTable.addCell("BILL NO");
				pdfPTable.addCell("CLIENT NAME");
				pdfPTable.addCell("CASE NO");
				pdfPTable.addCell("CASE TYPE");
				pdfPTable.addCell("BILL BY");
				pdfPTable.addCell("NO OF PAGES");
				pdfPTable.addCell("AMOUNT");
				
				
				for(InvoiceDetail invoice:invoiceDetailList){
					pdfPTable.addCell(invoice.getAutoNo().toString());
					pdfPTable.addCell(invoice.getBillNo());
					pdfPTable.addCell(invoice.getClientName());
					pdfPTable.addCell(invoice.getCaseNo());
					pdfPTable.addCell(invoice.getCaseType());
					pdfPTable.addCell(invoice.getCreatedBy());
					pdfPTable.addCell(invoice.getNoOfPage().toString());
					sumTotalPages =sumTotalPages+invoice.getNoOfPage();
					pdfPTable.addCell(invoice.getAmount().toString());
					sumAmount = sumAmount +invoice.getAmount();
				}
				
				document.add(pdfPTable);
				
				document.add(new Paragraph("         Total Pages " + sumTotalPages));
				document.add(new Paragraph("         Total Amount " + sumAmount));
				
				document.add(new Paragraph("Genarated on "+new Date().toString()));
	}

}
