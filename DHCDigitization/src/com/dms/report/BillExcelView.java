package com.dms.report;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.model.InvoiceDetail;
import com.dms.utility.FinancialYearCalculator;



public class BillExcelView extends AbstractExcelView{
	
	/*double totalAmount=0.0;
	int totalPageCount=0;*/
	
	DecimalFormat decimalFormat=new DecimalFormat("0.00");
    DecimalFormat decimalFormat1 = new DecimalFormat("0.#####");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");


	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String currentDate = simpleDateFormat.format( new Date());

        response.addHeader("Content-Disposition", "attachment; filename=BILL_REPORT_"+currentDate+".xls");
		List<InvoiceDetail> invoiceDetailList=(List<InvoiceDetail>) model.get("invoiceDetailList");
		//get gst rate from lookup table
		String lookupGST=(String) model.get("lookupGST");
		double gstRate=Double.parseDouble(lookupGST);
		
		HSSFSheet excelSheet = workbook.createSheet("INVOICE");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, invoiceDetailList ,gstRate);
		setExcelFooter(excelSheet, invoiceDetailList);
		
	}
	
	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("INVOICE NO.");
		excelHeader.createCell(1).setCellValue("AUTO NO.");
		excelHeader.createCell(2).setCellValue("BILL NO.");
		excelHeader.createCell(3).setCellValue("CLIENT NAME");
		excelHeader.createCell(4).setCellValue("CASE NO");
		excelHeader.createCell(5).setCellValue("CASE TYPE");
		excelHeader.createCell(6).setCellValue("BILL BY");
		excelHeader.createCell(7).setCellValue("CREATED  ON");
		excelHeader.createCell(8).setCellValue("NO OF PAGE");
		excelHeader.createCell(9).setCellValue("BASIC TOTAL");
		excelHeader.createCell(10).setCellValue("CGST (9%)");
		excelHeader.createCell(11).setCellValue("SGST (9%)");
		excelHeader.createCell(12).setCellValue("TOTAL AMOUNT");
	
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<InvoiceDetail> invoiceDetailList, double gstRate){
		int record = 1;
		for (InvoiceDetail invoiceDetail : invoiceDetailList) {
			
			double basicAmnt= ((invoiceDetail.getAmount()*100)/(gstRate+100));
        	//Calculate gst based on basic amount
			double gst=((basicAmnt*gstRate)/100);
			
        	String cgstAmount=decimalFormat.format(gst/2);
        	String sgstAmount=decimalFormat.format(gst/2);
        	
        	//final basic amount
        	String basicAmount=decimalFormat.format(basicAmnt);
        	
        	String finYear=FinancialYearCalculator.getCurrentFinancialYear();
    		String first2 = String.valueOf(finYear).substring(2,4);
    		String last2 = String.valueOf(finYear).substring(7,9);
    		String invoice="DL"+first2+last2+"DI";
			
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(invoice+invoiceDetail.getInvoiceNo());
			excelRow.createCell(1).setCellValue(invoiceDetail.getAutoNo());
			excelRow.createCell(2).setCellValue(invoiceDetail.getBillNo());
			excelRow.createCell(3).setCellValue(invoiceDetail.getClientName());
			excelRow.createCell(4).setCellValue(invoiceDetail.getCaseNo());
			excelRow.createCell(5).setCellValue(invoiceDetail.getCaseType());
			excelRow.createCell(6).setCellValue(invoiceDetail.getCreatedBy());
			excelRow.createCell(7).setCellValue(simpleDateFormat.format(invoiceDetail.getCreatedOn()));
			excelRow.createCell(8).setCellValue(invoiceDetail.getNoOfPage());
			excelRow.createCell(9).setCellValue(basicAmount);
			excelRow.createCell(10).setCellValue(cgstAmount);
			excelRow.createCell(11).setCellValue(sgstAmount);
			excelRow.createCell(12).setCellValue(invoiceDetail.getAmount());
		}
			
	}
	public void setExcelFooter(HSSFSheet excelSheet, List<InvoiceDetail> invoiceDetailList) {
		int totalPage=0;
		Double totalAmount=0d;
		for (InvoiceDetail invoiceDetail : invoiceDetailList) {
			totalPage=totalPage+invoiceDetail.getNoOfPage();
			totalAmount=totalAmount+invoiceDetail.getAmount();
		}
		HSSFRow excelHeader = excelSheet.createRow(invoiceDetailList.size()+1);
		excelHeader.createCell(0).setCellValue("");
		excelHeader.createCell(1).setCellValue("");
		excelHeader.createCell(2).setCellValue("");
		excelHeader.createCell(3).setCellValue("");
		excelHeader.createCell(4).setCellValue("");
		excelHeader.createCell(5).setCellValue("");
		excelHeader.createCell(6).setCellValue("");
		excelHeader.createCell(7).setCellValue("");
		excelHeader.createCell(8).setCellValue("");
		excelHeader.createCell(9).setCellValue("");
		excelHeader.createCell(10).setCellValue("");
		excelHeader.createCell(11).setCellValue("TOTAL PAGE");
		excelHeader.createCell(12).setCellValue(totalPage);
		
		HSSFRow excelHeader1 = excelSheet.createRow(invoiceDetailList.size()+2);
		excelHeader.createCell(0).setCellValue("");
		excelHeader1.createCell(1).setCellValue("");
		excelHeader1.createCell(2).setCellValue("");
		excelHeader1.createCell(3).setCellValue("");
		excelHeader1.createCell(4).setCellValue("");
		excelHeader1.createCell(5).setCellValue("");
		excelHeader1.createCell(6).setCellValue("");
		excelHeader1.createCell(7).setCellValue("");
		excelHeader1.createCell(8).setCellValue("");
		excelHeader1.createCell(9).setCellValue("");
		excelHeader1.createCell(10).setCellValue("");
		excelHeader1.createCell(11).setCellValue("TOTAL AMOUNT");
		excelHeader1.createCell(12).setCellValue(totalAmount);
	}
}
	
