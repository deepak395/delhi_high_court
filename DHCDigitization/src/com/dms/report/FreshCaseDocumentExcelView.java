package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.model.FreshCase;



public class FreshCaseDocumentExcelView extends AbstractExcelView{
  
	@Autowired
	static FreshCase freshCaseReport;
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = simpleDateFormat.format( new Date());
	
        response.addHeader("Content-Disposition", "attachment; filename=FRESH_CASE_DOCUMENT_"+currentDate+".xls");
		List<FreshCase> freshCaseList=(List<FreshCase>) model.get("freshCaseList");
		HSSFSheet excelSheet = workbook.createSheet("Fresh Case List");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, freshCaseList);
		
	}
	public void setExcelHeader(HSSFSheet excelSheet) {
		
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("DIARY NO");
		excelHeader.createCell(1).setCellValue("YEAR");
		excelHeader.createCell(2).setCellValue("DOCUMENT TYPE");
		excelHeader.createCell(3).setCellValue("CASE STAGE");
		excelHeader.createCell(4).setCellValue("DOCUMENT NAME");
		excelHeader.createCell(5).setCellValue("CASE TYPE");
		int  columnIndex = 6;
		
			excelHeader.createCell(columnIndex++).setCellValue("SCANNED DATE");
			excelHeader.createCell(columnIndex++).setCellValue("SCANNED BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF PAGES SCANNED");
		
		
		excelHeader.createCell(columnIndex++).setCellValue("QC DATE");
		excelHeader.createCell(columnIndex++).setCellValue("QC BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF PAGES QC");
		
		excelHeader.createCell(columnIndex++).setCellValue("BOOKMARKING DATE");
		excelHeader.createCell(columnIndex++).setCellValue("BOOKMARKED BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF  PAGES BOOKMARKED");
		
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<FreshCase> freshCaseList){
		int record = 1;
		for (FreshCase freshCase : freshCaseList) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(freshCase.getDiaryNo());
			excelRow.createCell(1).setCellValue(freshCase.getYear());
			excelRow.createCell(2).setCellValue(freshCase.getDocumentType());
			excelRow.createCell(3).setCellValue(freshCase.getCaseStage());
			excelRow.createCell(4).setCellValue(freshCase.getDocumentName());
			excelRow.createCell(5).setCellValue(freshCase.getCaseType());
			int  columnIndex = 6;
			
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getScanningOn().toString());
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getScanningBy());
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getScanningPage());
			
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getQcOn()!=null ?freshCase.getQcOn().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getQcBy()!=null ? freshCase.getQcBy() : " ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getQcPage()!=null ? freshCase.getQcPage().toString():" ");
			
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getBookmarkingOn()!=null ? freshCase.getBookmarkingOn().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getBookmarkingBy()!=null ? freshCase.getBookmarkingBy().toString(): " ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getBookmarkingPage()!=null ? freshCase.getBookmarkingPage().toString() :" ");
			
		}
	}
	
}
	
