package com.dms.report;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.dms.model.FreshCase;
import com.itextpdf.text.Element;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * 
 * @author Rajkumar Giri and Rupnarayan
 *
 */
public class FreshCasePdfView extends AbstractPdfView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		    
		  
		    // read date from model fdata
			List<FreshCase> freshCaseList=(List<FreshCase>) model.get("freshCaseReportList");
		   //set file name
	    	response.setHeader("Content-Disposition", "attachment;filename=FRESH_CASE.pdf");
				//create any element
	    	Paragraph paragraph=null;
	    	paragraph = new Paragraph(new Chunk("HIGH COURT OF DELHI"));
			paragraph.setAlignment(Element.ALIGN_CENTER);
			document.add(paragraph);
			document.add(new Paragraph("FRESH CASE"));
				
				PdfPTable pdfPTable=new PdfPTable(5);
				pdfPTable.addCell("CASE TYPE");
				pdfPTable.addCell("DIARY NO");
				pdfPTable.addCell("CASE YEAR");
				pdfPTable.addCell("DOCUMENT NAME");
				pdfPTable.addCell("AUTO NO");
				
				for(FreshCase freshCase:freshCaseList){
					pdfPTable.addCell(freshCase.getCaseType());
					pdfPTable.addCell(freshCase.getDiaryNo());
					pdfPTable.addCell(freshCase.getYear());
					pdfPTable.addCell(freshCase.getDocumentName());
					pdfPTable.addCell(freshCase.getAutoNo().toString());
				}
				document.add(pdfPTable);
				document.add(new Paragraph("Genarated on "+new Date().toString()));
	}

}
