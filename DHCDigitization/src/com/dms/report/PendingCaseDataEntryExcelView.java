package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.model.PendingCase;



public class PendingCaseDataEntryExcelView extends AbstractExcelView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, 
			HttpServletResponse response)throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = simpleDateFormat.format( new Date());
			
        response.addHeader("Content-Disposition", "attachment; filename=PENDING_ENTRY_"+currentDate+".xls");
		List<PendingCase> pendingCaseList=(List<PendingCase>) model.get("pendingCaseDataEntryReportList");
		HSSFSheet excelSheet = workbook.createSheet("Pending Case List");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, pendingCaseList);
		
	}
	
	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("CASE NO");
		excelHeader.createCell(1).setCellValue("COURT NO");
		excelHeader.createCell(2).setCellValue("CASE YEAR");
		excelHeader.createCell(3).setCellValue("TOTAL FILE");
		excelHeader.createCell(4).setCellValue("CASE CATEGORY");
		excelHeader.createCell(5).setCellValue("CASE TYPE");
		excelHeader.createCell(6).setCellValue("HEARING DATE");
		excelHeader.createCell(7).setCellValue("ENTERED BY");
		excelHeader.createCell(8).setCellValue("ENTERED ON");
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<PendingCase> pendingCaseList){
		int record = 1;
		for (PendingCase pendingCase : pendingCaseList) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(pendingCase.getCaseNo());
			excelRow.createCell(1).setCellValue(pendingCase.getCourtNo());
			excelRow.createCell(2).setCellValue(pendingCase.getCaseYear());
			excelRow.createCell(3).setCellValue(pendingCase.getTotalFile());
			excelRow.createCell(4).setCellValue(pendingCase.getCaseCategory());
			excelRow.createCell(5).setCellValue(pendingCase.getCaseType());
			excelRow.createCell(6).setCellValue(pendingCase.getHearingDate());
			excelRow.createCell(7).setCellValue(pendingCase.getCreatedBy());
			excelRow.createCell(8).setCellValue(pendingCase.getCreatedOn().toString());
		}
	}
	
}
	
