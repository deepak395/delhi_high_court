package com.dms.report;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.dms.model.InvoiceDetail;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class InvoiceCancelDetailReportPdfView extends AbstractPdfView{
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	String currentDate = simpleDateFormat.format( new Date());

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		    
		  
		    // read date from model fdata
			List<InvoiceDetail> invoiceDetailList=(List<InvoiceDetail>) model.get("invoiceDetailList");
			DecimalFormat decimalFormat = new DecimalFormat("0.#####");
			int totalPage=0;
			Double totalAmount=0d;
			for (InvoiceDetail invoiceDetail : invoiceDetailList) {
				totalPage=totalPage+invoiceDetail.getNoOfPage();
				totalAmount=totalAmount+invoiceDetail.getAmount();
			}
		   //set file name
	    	response.setHeader("Content-Disposition", "attachment;filename=BILL_CANCELLED"+currentDate+".pdf");
	         
	    	Paragraph paragraph=null;
	    	paragraph = new Paragraph(new Chunk("HIGH COURT OF DELHI"));
			paragraph.setAlignment(Element.ALIGN_CENTER);
			document.add(paragraph);
			document.add(new Paragraph("CANCELLED INVOICE"));
				
				PdfPTable pdfPTable=new PdfPTable(10);
				pdfPTable.setWidthPercentage(100.0f);
				pdfPTable.setWidths(new float[] {1.4f, 2.5f, 2.1f, 1.5f, 2.3f, 2.5f, 2.5f, 2.0f, 1.5f, 2.0f});
				pdfPTable.setSpacingBefore(10);
				pdfPTable.addCell(new Phrase("AUTO NO",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("BILL NO",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CLIENT NAME",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CASE NO",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CASE TYPE",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CANCELLED BY",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CANCELLED ON",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CANCEL REMARK",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("NO OF PAGES",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("AMOUNT",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				
				for(InvoiceDetail invoice:invoiceDetailList){
					pdfPTable.addCell(new Phrase(invoice.getAutoNo().toString(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getBillNo(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getClientName(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getCaseNo(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getCaseType(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getCanceledBy(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase((simpleDateFormat.format(invoice.getCanceledOn())).toString(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getCanceledRemark(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getNoOfPage().toString(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(decimalFormat.format(invoice.getAmount()),FontFactory.getFont(FontFactory.HELVETICA, 8)));
				}
				document.add(pdfPTable);
				document.add(new Paragraph(new Phrase("TOTAL PAGE     "+totalPage,FontFactory.getFont(FontFactory.HELVETICA, 10))));
				document.add(new Paragraph(new Phrase("TOTAL AMOUNT   "+decimalFormat.format(totalAmount),FontFactory.getFont(FontFactory.HELVETICA, 10))));
	}

}
