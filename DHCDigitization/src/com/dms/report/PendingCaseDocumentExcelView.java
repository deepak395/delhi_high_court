package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.form.PendingCaseDocumentForm;
import com.dms.model.PendingCaseDocument;



public class PendingCaseDocumentExcelView extends AbstractExcelView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = simpleDateFormat.format( new Date());
	
        response.addHeader("Content-Disposition", "attachment; filename=FRESH_CASE_DOCUMENT_"+currentDate+".xls");
		List<PendingCaseDocumentForm> pendingCaseList=(List<PendingCaseDocumentForm>) model.get("pendingCaseList");
		
		HSSFSheet excelSheet = workbook.createSheet("Fresh Case List");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, pendingCaseList);
		
	}
	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("CASE NO");
		excelHeader.createCell(1).setCellValue("CASE YEAR");
		excelHeader.createCell(2).setCellValue("CASE CATEGORY");
		excelHeader.createCell(3).setCellValue("CASE TYPE");
		excelHeader.createCell(4).setCellValue("DEALING");
		excelHeader.createCell(5).setCellValue("TOTAL FILE");
		excelHeader.createCell(6).setCellValue("CASE STAGE");
		HSSFRow excelHeader3 = excelSheet.createRow(3);
		excelHeader3.createCell(0).setCellValue("DOCUMENT NAME");
		excelHeader3.createCell(1).setCellValue("TOTAL FILE");
		excelHeader3.createCell(2).setCellValue("SCAN DATE");
		excelHeader3.createCell(3).setCellValue("SCAN BY");
		excelHeader3.createCell(4).setCellValue("SCAN PAGE");
		excelHeader3.createCell(5).setCellValue("QC DATE");
		excelHeader3.createCell(6).setCellValue("QC BY");
		excelHeader3.createCell(7).setCellValue("QC PAGE");
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<PendingCaseDocumentForm> pendingCaseList){
		int record = 4;
		for (PendingCaseDocumentForm pendingCaseDocumentForm : pendingCaseList) {
			HSSFRow excelRow2 = excelSheet.createRow(1);
			excelRow2.createCell(0).setCellValue(pendingCaseDocumentForm.getPendingCase().getCaseNo());
			excelRow2.createCell(1).setCellValue(pendingCaseDocumentForm.getPendingCase().getCaseYear());
			excelRow2.createCell(2).setCellValue(pendingCaseDocumentForm.getPendingCase().getCaseCategory());
			excelRow2.createCell(3).setCellValue(pendingCaseDocumentForm.getPendingCase().getCaseType());
			excelRow2.createCell(4).setCellValue(pendingCaseDocumentForm.getPendingCase().getDealing());
			excelRow2.createCell(5).setCellValue(pendingCaseDocumentForm.getPendingCase().getTotalFile());
			excelRow2.createCell(6).setCellValue(pendingCaseDocumentForm.getPendingCase().getCaseStage());
			if(pendingCaseDocumentForm.getPendingCaseDocumentList()!=null) {
				for (PendingCaseDocument pendingCaseDocument : pendingCaseDocumentForm.getPendingCaseDocumentList()) {
					HSSFRow excelRow = excelSheet.createRow(record++);
					excelRow.createCell(0).setCellValue(pendingCaseDocument.getDocumentName());
					excelRow.createCell(1).setCellValue(pendingCaseDocumentForm.getPendingCase().getCaseYear());
					excelRow.createCell(2).setCellValue(pendingCaseDocument.getScanningOn()!=null ? pendingCaseDocument.getScanningOn().toString():" ");
					excelRow.createCell(3).setCellValue(pendingCaseDocument.getScanningBy());
					excelRow.createCell(4).setCellValue(pendingCaseDocument.getScanningPage()!=null ?pendingCaseDocument.getScanningPage().toString():" ");
					excelRow.createCell(5).setCellValue(pendingCaseDocument.getQcOn()!=null? pendingCaseDocument.getQcOn().toString():" ");
					excelRow.createCell(6).setCellValue(pendingCaseDocument.getQcBy());
					excelRow.createCell(7).setCellValue(pendingCaseDocument.getQcPage()!=null ? pendingCaseDocument.getQcPage().toString():" ");
					
				}
			}
		}
	}
	
}
	
