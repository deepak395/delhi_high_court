package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.model.InvoiceDetail;



public class InvoiceCancelDetailReport extends AbstractExcelView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = simpleDateFormat.format( new Date());

        response.addHeader("Content-Disposition", "attachment; filename=CANCEL_BILL_REPORT"+currentDate+".xls");
		List<InvoiceDetail> invoiceDetailList=(List<InvoiceDetail>) model.get("invoiceDetailList");
		
		HSSFSheet excelSheet = workbook.createSheet("User List");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, invoiceDetailList);
		setExcelFooter(excelSheet, invoiceDetailList);
	}
	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("AUTO NO.");
		excelHeader.createCell(1).setCellValue("BILL NO.");
		excelHeader.createCell(2).setCellValue("CLIENT NAME");
		excelHeader.createCell(3).setCellValue("NO OF PAGES");
		excelHeader.createCell(4).setCellValue("AMOUNT");
		excelHeader.createCell(5).setCellValue("REMARK");
		excelHeader.createCell(6).setCellValue("CANCELLED ON");
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<InvoiceDetail> invoiceDetailList){
		int record = 1;
		for (InvoiceDetail invoiceDetail : invoiceDetailList) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(invoiceDetail.getAutoNo());
			excelRow.createCell(1).setCellValue(invoiceDetail.getBillNo());
			excelRow.createCell(2).setCellValue(invoiceDetail.getClientName());
			excelRow.createCell(3).setCellValue(invoiceDetail.getNoOfPage());
			excelRow.createCell(4).setCellValue(invoiceDetail.getAmount());
			excelRow.createCell(5).setCellValue(invoiceDetail.getCanceledRemark());
			excelRow.createCell(6).setCellValue(invoiceDetail.getCanceledOn()!=null?invoiceDetail.getCanceledOn().toString():" ");
		}
	}
	
	public void setExcelFooter(HSSFSheet excelSheet, List<InvoiceDetail> invoiceDetailList) {
		int totalPage=0;
		Double totalAmount=0d;
		for (InvoiceDetail invoiceDetail : invoiceDetailList) {
			totalPage=totalPage+invoiceDetail.getNoOfPage();
			totalAmount=totalAmount+invoiceDetail.getAmount();
		}
		HSSFRow excelHeader = excelSheet.createRow(invoiceDetailList.size()+1);
		excelHeader.createCell(0).setCellValue("");
		excelHeader.createCell(1).setCellValue("");
		excelHeader.createCell(2).setCellValue("TOTAL");
		excelHeader.createCell(3).setCellValue(totalPage);
		excelHeader.createCell(4).setCellValue(totalAmount);
	}
}
	
