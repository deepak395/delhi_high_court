package com.dms.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.dms.model.FreshCaseDocument;



public class FreshCaseExcelView extends AbstractExcelView{
  
	@Autowired
	static FreshCaseDocument freshCaseReport;
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = simpleDateFormat.format( new Date());
	
        response.addHeader("Content-Disposition", "attachment; filename=FRESH_CASE_DOCUMENT_"+currentDate+".xls");
        List<FreshCaseDocument> freshCaseList=(List<FreshCaseDocument>) model.get("freshCaseReportList");
		freshCaseReport = freshCaseList.get(0);
		HSSFSheet excelSheet = workbook.createSheet("Fresh Case List");
		setExcelHeader(excelSheet);
		setExcelBody(excelSheet, freshCaseList);
		
	}
	public void setExcelHeader(HSSFSheet excelSheet) {
		
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("DIARY NO");
		excelHeader.createCell(1).setCellValue("YEAR");
		excelHeader.createCell(2).setCellValue("DOCUMENT TYPE");
		excelHeader.createCell(3).setCellValue("CASE STAGE");
		excelHeader.createCell(4).setCellValue("DOCUMENT NAME");
		excelHeader.createCell(5).setCellValue("CASE TYPE");
		int  columnIndex = 6;
		
		if (freshCaseReport.getCaseStage().contains("SCANNING"))
		{	excelHeader.createCell(columnIndex++).setCellValue("SCANNED DATE");
			excelHeader.createCell(columnIndex++).setCellValue("SCANNED BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF PAGES SCANNED");
		excelHeader.createCell(columnIndex++).setCellValue("TOTAL SCANNED");
		
		}
		
		
		if (freshCaseReport.getCaseStage().contains("QC"))
		{
		excelHeader.createCell(columnIndex++).setCellValue("QC DATE");
		excelHeader.createCell(columnIndex++).setCellValue("QC BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF PAGES QC");
		excelHeader.createCell(columnIndex++).setCellValue("TOTAL QC");
	}
		if (freshCaseReport.getCaseStage().contains("BOOKMARKING"))
		{
		excelHeader.createCell(columnIndex++).setCellValue("BOOKMARKING DATE");
		excelHeader.createCell(columnIndex++).setCellValue("BOOKMARKED BY");
		excelHeader.createCell(columnIndex++).setCellValue("NO OF  PAGES BOOKMARKED");
		excelHeader.createCell(columnIndex++).setCellValue("TOTAL BOOKMARK");
		}
		
		
	}
	
	public void setExcelBody(HSSFSheet excelSheet, List<FreshCaseDocument> freshCaseList){
		int record = 1;
		int scan_page=0;
		int qc_page=0;
		int bookmark_page=0;
		int  columnIndex=0;
		HSSFRow excelRow=null;
		for (FreshCaseDocument freshCase : freshCaseList) {
			 excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(freshCase.getDiaryNo());
			excelRow.createCell(1).setCellValue(freshCase.getYear());
			excelRow.createCell(2).setCellValue(freshCase.getDocumentType());
			excelRow.createCell(3).setCellValue(freshCase.getCaseStage());
			excelRow.createCell(4).setCellValue(freshCase.getDocumentName());
			excelRow.createCell(5).setCellValue(freshCase.getCaseType());
			 columnIndex = 6;
			
			if (freshCaseReport.getCaseStage().contains("SCANNING"))
			{
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getScanningOn()!=null ? freshCase.getScanningOn().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getScanningfullname()!=null ? freshCase.getScanningfullname():" ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getScanningPage()!=null ? freshCase.getScanningPage().toString():"");
			if(freshCase.getScanningPage()!=0) 
			scan_page=scan_page+freshCase.getScanningPage();
			excelRow.createCell(columnIndex++).setCellValue(scan_page);
			
			}
			if (freshCaseReport.getCaseStage().contains("QC"))
			{
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getQcOn()!=null ?freshCase.getQcOn().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getQcfullname()!=null ? freshCase.getQcfullname() : " ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getQcPage()!=null ? freshCase.getQcPage().toString():" ");
			if(freshCase.getQcPage()!=0) 
				qc_page=qc_page+freshCase.getQcPage();
			excelRow.createCell(columnIndex++).setCellValue(qc_page);
			
			}
			if (freshCaseReport.getCaseStage().contains("BOOKMARKING"))
			{
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getBookmarkingOn()!=null ? freshCase.getBookmarkingOn().toString():" ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getBookmarkingfullname()!=null ? freshCase.getBookmarkingfullname().toString(): " ");
			excelRow.createCell(columnIndex++).setCellValue(freshCase.getBookmarkingPage()!=null ? freshCase.getBookmarkingPage().toString() :" ");
			if(freshCase.getBookmarkingPage()!=0) 
				bookmark_page=bookmark_page+freshCase.getBookmarkingPage();
			excelRow.createCell(columnIndex++).setCellValue(bookmark_page);
			}
			
			}
		
		
	
	
			
		}
}
	
	
	/*public void setExcelFooter(HSSFSheet excelSheet, List<FreshCaseDocument> freshcaseDoc) {
		int scan_Page=0;
		int qc_page=0;
		int bookmark_page=0;
		for (FreshCaseDocument freshDoc : freshcaseDoc) {
			if (freshCaseReport.getCaseStage().contains("SCANNING")) {
				
			
				scan_Page=scan_Page+freshDoc.getScanningPage();
			}
			else if (freshCaseReport.getCaseStage().contains("QC")) {
			
				qc_page=qc_page+freshDoc.getQcPage();
			}
			else
			{
				bookmark_page=bookmark_page+freshDoc.getBookmarkingPage();
				
			}
			
		}
		HSSFRow excelHeader = excelSheet.createRow(freshcaseDoc.size()+1);
		excelHeader.createCell(9).setCellValue("");
		excelHeader.createCell(10).setCellValue("");
		excelHeader.createCell(11).setCellValue("TOTAL");
		if(scan_Page!=0) {
		excelHeader.createCell(11).setCellValue(scan_Page);
		}
		if(qc_page!=0)
			excelHeader.createCell(11).setCellValue(qc_page);
		
		  if(bookmark_page!=0)
			
			excelHeader.createCell(11).setCellValue(bookmark_page);
		}
			
	}
	*/
	
	
