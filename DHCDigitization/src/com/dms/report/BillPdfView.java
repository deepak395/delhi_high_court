package com.dms.report;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.dms.model.InvoiceDetail;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class BillPdfView extends AbstractPdfView{
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	String currentDate = simpleDateFormat.format( new Date());
	

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		  
			DecimalFormat decimalFormat=new DecimalFormat("0.00");
		    DecimalFormat decimalFormat1 = new DecimalFormat("0.#####");
		
			List<InvoiceDetail> invoiceDetailList=(List<InvoiceDetail>) model.get("invoiceDetailList");
			String date=(String) model.get("dateRange");
			String lookupGST=(String) model.get("lookupGST");
			double gstRate=Double.parseDouble(lookupGST);
			
			double totalAmount=0.0;
			int totalPageCount=0;
			
			if(invoiceDetailList!=null){
		    	response.setHeader("Content-Disposition", "attachment;filename=ALL_BILL_"+currentDate+".pdf");
		    	Paragraph paragraph=null;
		    	paragraph = new Paragraph("HIGH COURT OF DELHI",FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12));
				paragraph.setAlignment(Element.ALIGN_CENTER);
				document.add(paragraph);
				document.add(new Paragraph("INVOICE REPORT FROM "+date, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10)));
					
				PdfPTable pdfPTable=new PdfPTable(10);
				pdfPTable.setWidthPercentage(100.0f);
				pdfPTable.setWidths(new float[] {1.5f, 2.0f, 2.5f, 2.0f, 1.5f, 1.2f, 1.5f, 1.0f, 1.0f, 1.5f});
				pdfPTable.setSpacingBefore(10);
				pdfPTable.addCell(new Phrase("AUTO NO",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("BILL NO",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CLIENT NAME",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("BILL BY",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CREATED ON",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("NO OF PAGES",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("BASIC TOTAL",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("CGST (9%)",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("SGST (9%)",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				pdfPTable.addCell(new Phrase("TOTAL AMOUNT",FontFactory.getFont(FontFactory.HELVETICA, 10)));
				for(InvoiceDetail invoice:invoiceDetailList){
					
					totalAmount=totalAmount+invoice.getAmount();
					totalPageCount=totalPageCount+invoice.getNoOfPage();
					
					double basicAmnt= ((invoice.getAmount()*100)/(gstRate+100));
					
					// calculate gst on basic amount
		        	double gst= ((basicAmnt*18)/100);
		        	String cgstAmount=decimalFormat.format(gst/2);
		        	String sgstAmount=decimalFormat.format(gst/2);
		        	//final basic with 2 decimal point
		        	String basicAmount=decimalFormat.format(basicAmnt);
		        	
					pdfPTable.addCell(new Phrase(invoice.getAutoNo().toString(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getBillNo(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getClientName(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getCreatedBy(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase((simpleDateFormat.format(invoice.getCreatedOn())).toString(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(invoice.getNoOfPage().toString(),FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(basicAmount,FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(sgstAmount,FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(cgstAmount,FontFactory.getFont(FontFactory.HELVETICA, 8)));
					pdfPTable.addCell(new Phrase(decimalFormat1.format(invoice.getAmount()),FontFactory.getFont(FontFactory.HELVETICA, 8)));
				}
				document.add(pdfPTable);
				document.add(new Paragraph(new Phrase("Total PAGE :  "+totalPageCount,FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12))));
				document.add(new Paragraph(new Phrase("Total AMOUNT : "+totalAmount,FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12))));
			}
			
           
	}

}
