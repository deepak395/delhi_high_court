package com.dms.service;

import java.util.List;

import com.dms.model.Lookup;

public interface LookupService {
	
	public Long saveLookup(Lookup lookup);
	
	public List<Lookup> getAllLookupList();
	
	public Lookup getLookupById(Long lookupId);
	
	public List<Lookup> getAllLookupListByLookupName(String lookupName);

	public List<Lookup> getLookUpByName(String string);

	public List<Lookup> getAllCaseCategory(String string);

}
