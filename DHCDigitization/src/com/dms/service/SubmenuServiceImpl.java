package com.dms.service;

import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.Submenu;

@Service
@Transactional
public class SubmenuServiceImpl implements SubmenuService {
	
	@Autowired
	private GenericDao<Submenu> submenuDao;

	@Override
	public List<Submenu> getAllSubmenuListByMenuId(long menuId) {
		return submenuDao.getAllListByFieldNameAndOrder(Submenu.class, "menuId", menuId, Order.asc("submenuId"));
	}

	@Override
	public Submenu getSubmenuBySubmenuId(long submenuId) {
		return submenuDao.getById(Submenu.class, submenuId);
	}

	@Override
	public List<Submenu> getAllSubmenuListByRoleName(String roleName) {
		return submenuDao.getAllListByFieldName(Submenu.class, "roleName", roleName);
	}

}
