package com.dms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.DeleteFilesLocal;

@Transactional
@Service
public class DeleteFilesLocalServiceImpl implements DeleteFilesLocalService {
	
	@Autowired
	private GenericDao<DeleteFilesLocal> deleteFileFromScanFolderDao;

	@Override
	public Long saveDeleteFileFromScanFolder(DeleteFilesLocal deleteFileFromScanFolder) {
		return deleteFileFromScanFolderDao.save(deleteFileFromScanFolder);
	}

}
