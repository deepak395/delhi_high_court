package com.dms.service;

import java.util.List;

import com.dms.model.OutwardCase;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

public interface PendingCaseService {
	
	public Long savePendingCase(PendingCase pendingCase);
	
	public boolean updatePendingCase(PendingCase pendingCase);
	
	public List<PendingCase> getAllPendingCaseList();
	
	public PendingCase getPendingCaseByPendingCaseId(Long pendingCaseId);
	
	public PendingCase getPendingCaseByCaseNoAndCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory);
	
	public List<PendingCase> getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit(String caseNo, String caseYear, String caseCategory);
	public List<PendingCase> getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit1(String caseNo, String caseYear, String caseCategory);


	
	public List<PendingCase> getAllPendingCaseListByPagination(int pageNo, int maxRecord);
	
	public boolean checkPendingCase(String caseNo, String caseYear, String hearingDate);
	
	public List<PendingCaseDocument> getPendingCaseDocumentByPendingCaseId(Long pendingCaseId);
	
	public List<PendingCase> getAllPendingCaseByCaseNoAndCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory);

	public PendingCase getPendingCaseByCaseNoAndCaseYearAndCaseCategoryAndcaseStageAndcaseType(String caseNo,
			String caseYear, String caseCategory, String caseStage, String caseType);

	public List<PendingCase> getPendingCaseByCaseNoAndCaseYearAndCaseCategoryForQc(String caseNo, String caseYear,
			String caseCategory);

	public Long saveOutwardCase(OutwardCase outwardCase);
	
	public List<OutwardCase> getAllOutwardCaseByCaseNoAndCaseYearAndCaseCategoryEdit(String caseNo, String caseYear, String caseCategory);

	
	
}
