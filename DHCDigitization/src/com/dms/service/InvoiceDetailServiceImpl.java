package com.dms.service;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.InvoiceDetail;

@Transactional
@Service
public class InvoiceDetailServiceImpl implements InvoiceDetailService {
	
	@Autowired
	private GenericDao<InvoiceDetail> invoiceDetailDao;
	
	@Override
	public Long saveInvoiceDetail(InvoiceDetail invoiceDetail) {
		return invoiceDetailDao.save(invoiceDetail);
	}

	@Override
	public boolean updateInvoiceDetail(InvoiceDetail invoiceDetail) {
		return invoiceDetailDao.update(invoiceDetail);
	}

	@Override
	public List<InvoiceDetail> getAllInvoiceDetailList() {
		return invoiceDetailDao.getAllList(InvoiceDetail.class);
	}

	@Override
	public InvoiceDetail getInvoiceDetailByAutoNo(Long autoNo) {
		return invoiceDetailDao.getById(InvoiceDetail.class, autoNo);
	}

	@Override
	public List<InvoiceDetail> getAllInvoiceDetailListByCaseType(String caseType) {
		return invoiceDetailDao.getAllListByFieldName(InvoiceDetail.class, "caseType", caseType, "isCanceled", false);
	}

	/*@Override
	public List<Object> getAllInvoiceDetailUsingGroupBy(Date fromDate, Date toDate) {
		return invoiceDetailDao.getBillReportUsingGroupBy(fromDate, toDate);
	}*/
	
	@Override
	public List<Object> getAllInvoiceDetailUsingGroupBy(String fromDate, String toDate) {
		return invoiceDetailDao.getBillReportUsingGroupBy(fromDate, toDate);
	}

	@Override
	public InvoiceDetail getInvoiceDetailByBillNo(String billNo) {
		List<InvoiceDetail> invoiceDetailList=invoiceDetailDao.getAllListByFieldName(InvoiceDetail.class, "billNo", billNo);
		if(invoiceDetailList!=null) {
			return invoiceDetailList.get(0);
		}
		return null;
	}

	@Override
	public List<InvoiceDetail> getAllInvoiceDetailListByDateAndUsername(Date fromDate, Date toDate, String userFullName) {
		return invoiceDetailDao.getAllListByDateAndField(InvoiceDetail.class, "createdOn", fromDate, toDate, "createdBy", userFullName);
	}

		
	@Override
	public List<InvoiceDetail> getAllInvoiceDetailListByUsername(String username) {
		return invoiceDetailDao.getAllListByFieldNameAndOrder(InvoiceDetail.class, "createdBy", username, Order.desc("autoNo"));
	}

	@Override
	public List<InvoiceDetail> getAllInvoiceDetailListByAutoNoAndCaseType(Long autoNo, String caseType, String caseNo) {
		List<InvoiceDetail> invoiceDetailList=null;
		if(autoNo!=null) {
			invoiceDetailList=invoiceDetailDao.getAllListByFieldName(InvoiceDetail.class, "autoNo", autoNo);
		}else if(caseType!=""){
			invoiceDetailList=invoiceDetailDao.getAllListByFieldName(InvoiceDetail.class, "caseType", caseType);
		}else {
			invoiceDetailList=invoiceDetailDao.getAllListByFieldName(InvoiceDetail.class, "caseNo", autoNo);
		}
		return invoiceDetailList;
	}
	
	@Override
	public List<InvoiceDetail> getAllCancelledInvoiceDetailListByDate(Date fromDate, Date toDate) {
		return invoiceDetailDao.getAllListByDate(InvoiceDetail.class, "canceledOn", fromDate, toDate);
	}

	@Override
	public List<InvoiceDetail> getAllCancelledInvoiceDetailListByDateAndUsername(Date fromDate, Date toDate, String userFullName) {
		return invoiceDetailDao.getAllListByDateAndField(InvoiceDetail.class, "canceledOn", fromDate, toDate, "createdBy", userFullName);
	}
	
	@Override
	public List<InvoiceDetail> getAllInvoiceDetailListByDate(Date fromDate, Date toDate) {
		return invoiceDetailDao.getAllListByDate(InvoiceDetail.class, "createdOn", fromDate, toDate);
	}
	

	@Override
	public String getInvoiceNo() {
		Object obj=invoiceDetailDao.getByNativeQuery("select lpad(CAST(nextval('invoice_no_sequence') AS varchar),8,'0') ");
		return obj.toString();
	}

}
