package com.dms.service;

import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.Menu;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	private GenericDao<Menu> menuDao;

	@Override
	public List<Menu> getMenuList() {
		return menuDao.getAllListByOrder(Menu.class, Order.asc("menuId"));
	}

}
