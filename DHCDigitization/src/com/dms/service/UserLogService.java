package com.dms.service;

import com.dms.model.UserLog;

public interface UserLogService {
	
	
	public Long saveUserLog(UserLog userLog);
	
	public boolean updateUserLog(UserLog userLog);
	
	public UserLog getUserLogById(Long id);

}
