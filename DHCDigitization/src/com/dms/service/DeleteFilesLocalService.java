package com.dms.service;

import com.dms.model.DeleteFilesLocal;

public interface DeleteFilesLocalService {
	
	
	public Long saveDeleteFileFromScanFolder(DeleteFilesLocal deleteFileFromScanFolder);

}
