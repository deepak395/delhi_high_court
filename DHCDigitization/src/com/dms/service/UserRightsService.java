package com.dms.service;

import java.util.List;

import com.dms.model.UserRights;

public interface UserRightsService {
	
	public Long saveUserRights(UserRights userRights);
	
	public boolean updateUserRights(UserRights userRights);
	
	public List<UserRights> getAllUserRightsListByUsername(String username);
	
	public UserRights getUserRightsByUsernameAndsubmenuId(String username, Long menuId, Long submenuId);
	
	public List<UserRights> getAllUserRightsListByUsername(String username, Long menuId);

}
