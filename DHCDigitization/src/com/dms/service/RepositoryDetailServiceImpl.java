package com.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.RepositoryDetail;


@Service
@Transactional
public class RepositoryDetailServiceImpl implements RepositoryDetailService {
	
	@Autowired
	private GenericDao<RepositoryDetail> repositoryDetailDao;
	

	@Override
	public long saveRepositoryDetail(RepositoryDetail repositoryDetail) {
		return repositoryDetailDao.save(repositoryDetail);
	}

	@Override
	public boolean updateRepositoryDetail(RepositoryDetail repositoryDetail) {
		return repositoryDetailDao.update(repositoryDetail);
	}

	@Override
	public List<RepositoryDetail> getAllRepositoryDetailList() {
		return repositoryDetailDao.getAllList(RepositoryDetail.class);
	}

	@Override
	public RepositoryDetail getRepositoryDetailByRepositoryId(Long repositoryId) {
		return repositoryDetailDao.getById(RepositoryDetail.class, repositoryId);
	}


}
