package com.dms.service;

import java.util.List;

import com.dms.model.Menu;

public interface MenuService {

	public List<Menu> getMenuList();
}
