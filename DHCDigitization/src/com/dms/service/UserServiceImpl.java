package com.dms.service;

import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private GenericDao<User> userDao;
	
	@Override
	public Long saveUser(User user) {
		return userDao.save(user);
	}
	
	@Override
	public boolean updateUser(User user) {
		return userDao.update(user);
	}

	@Override
	public boolean saveOrUpdateUser(User user) {
		return userDao.saveOrUpdate(user);
	}
	
	@Override
	public User getUserByUsernameAndPassword(String username, String password) {
		List<User> userList=userDao.getAllListByFieldName(User.class, "username", username, "password", password);
		if(userList!=null) {
			return userList.get(0);
		}
		return null;
	}

	@Override
	public List<User> getAllUserListByPagination(int pageNo, int maxRecord) {
		return userDao.getAllListByPaginationAndOrder(User.class, pageNo, maxRecord, Order.desc("id"));
	}

	@Override
	public List<User> getAllUserList() {
		return userDao.getAllList(User.class);
	}

	@Override
	public User getUserById(Long id) {
		return userDao.getById(User.class, id);
	}

	@Override
	public User getUserByUsername(String username) {
		List<User> usersList=userDao.getAllListByFieldName(User.class, "username", username);
		if(usersList!=null) {
			return usersList.get(0);
		}
		return null;
	}


	
	@Override
	public List<User> getUserListByRole(String role) {
		List<User> usersList=userDao.getAllListByFieldName(User.class, "roleName", role);
		if(usersList!=null) {
			return usersList;
		}
		return null;
	}
	
	
}
