package com.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.Folder;

@Service
@Transactional
public class FolderServiceImpl implements FolderService {
	
	@Autowired
	private GenericDao<Folder> folderDao;

	@Override
	public long saveFolder(Folder folder) {
		return folderDao.save(folder);
	}

	@Override
	public boolean updateFolder(Folder folder) {
		return folderDao.update(folder);
	}

	@Override
	public Folder getFolderByFolderId(long folderId) {
		return folderDao.getById(Folder.class, folderId);
	}

	@Override
	public List<Folder> getAllFolderList() {
		return folderDao.getAllList(Folder.class);
	}

	@Override
	public List<Folder> getAllFolderListByFolderName(String folderName) {
		return folderDao.getAllListByFieldName(Folder.class, "folderName", folderName);
	}

	@Override
	public Folder getFolderByFolderName(String folderName, String folderType) {
		List<Folder> folderList=folderDao.getAllListByFieldName(Folder.class, "folderName", folderName, "folderType", folderType);
		if(folderList!=null) {
			return folderList.get(0);
		}
		return null;
	}


}
