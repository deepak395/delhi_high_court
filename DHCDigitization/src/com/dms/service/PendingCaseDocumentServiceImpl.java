package com.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.PendingCaseDocument;

@Transactional
@Service
public class PendingCaseDocumentServiceImpl implements PendingCaseDocumentService {
	
	@Autowired
	private GenericDao<PendingCaseDocument> documentDetailDao;

	@Override
	public Long savePendingCaseDocument(PendingCaseDocument documentDetail) {
		return documentDetailDao.save(documentDetail);
	}
	
	@Override
	public boolean updatePendingCaseDocument(PendingCaseDocument documentDetail) {
		return documentDetailDao.update(documentDetail);
	}

	@Override
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByPendingCaseId(Long pendingCaseId, String caseStage) {
		return documentDetailDao.getAllListByFieldName(PendingCaseDocument.class, "pendingCaseId", pendingCaseId, "caseStage", caseStage);
	}

	@Override
	public PendingCaseDocument getPendingCaseDocumentByDocumentId(Long documentId) {
		return documentDetailDao.getById(PendingCaseDocument.class, documentId);
	}

	@Override
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory, String caseStage) {
			
		String hql="";
		if(caseCategory!="") {
			 hql="from PendingCaseDocument where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"' and caseStage='"+caseStage+"'";
		}else {
			 hql="from PendingCaseDocument where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseStage='"+caseStage+"'";
		}
		List<PendingCaseDocument> pendingCaseDocumentList=documentDetailDao.getAllListByQuery(hql);
		return pendingCaseDocumentList;
	}

	@Override
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory) {
			
		String hql="";
		if(caseCategory!="") {
			 hql="from PendingCaseDocument where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"'";
		}else {
			 hql="from PendingCaseDocument where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'";
		}
		List<PendingCaseDocument> pendingCaseDocumentList=documentDetailDao.getAllListByQuery(hql);
		return pendingCaseDocumentList;
	}

	@Override
	public PendingCaseDocument getPendingCaseDocumentByDocumentName(String documentName, String caseNo, String caseYear, String caseCategory) {
			
		 String  hql="from PendingCaseDocument where documentName='"+documentName+"' and caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"' and qcDownloaded=true and qcOn is null";
		 List<PendingCaseDocument> pendingCaseDocumentList=documentDetailDao.getAllListByQuery(hql);
		 if(pendingCaseDocumentList!=null) {
			return pendingCaseDocumentList.get(0);
		 }
		return null;
	}

	
	@Override
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByPendingCaseId(Long pendingCaseId) {
		return documentDetailDao.getAllListByFieldName(PendingCaseDocument.class, "pendingCaseId", pendingCaseId);
	}

	
	
}
