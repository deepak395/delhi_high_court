package com.dms.service;

import java.util.List;

import com.dms.model.User;

public interface UserService {
	
	public Long saveUser(User user);
	
	public boolean updateUser(User user);
	
	public boolean saveOrUpdateUser(User user);
	
	public User getUserByUsernameAndPassword(String username, String password);

	public List<User> getAllUserListByPagination(int pageNo, int maxRecord);

	public List<User> getAllUserList();

	public User getUserById(Long id);
	
	public User getUserByUsername(String username);
	
	
	public List<User> getUserListByRole(String role);
	
}
