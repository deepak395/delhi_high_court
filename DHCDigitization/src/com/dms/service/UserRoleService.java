package com.dms.service;

import java.util.List;

import com.dms.model.UserRole;

public interface UserRoleService {
	
	public Long saveUserRole(UserRole userRole);
	
	public boolean updateUserRole(UserRole userRole);
	
	public boolean saveOrUpdateUserRole(UserRole userRole);
	
	public List<UserRole> getAllUserRoleList();
	
	public UserRole getUserRoleByRoleId(Long userRoleId);
	
	public UserRole getUserRoleByRoleName(String roleName);
	

}
