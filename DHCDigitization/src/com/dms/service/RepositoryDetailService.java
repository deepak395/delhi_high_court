package com.dms.service;

import java.util.List;

import com.dms.model.RepositoryDetail;

public interface RepositoryDetailService {
	
	public long saveRepositoryDetail(RepositoryDetail repositoryDetail);
	
	public boolean updateRepositoryDetail(RepositoryDetail repositoryDetail);
	
	public List<RepositoryDetail> getAllRepositoryDetailList();
	
	public RepositoryDetail getRepositoryDetailByRepositoryId(Long repositoryId);

}
