package com.dms.service;

import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.OutwardCase;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

@Transactional
@Service
public class PendingCaseServiceImpl implements PendingCaseService {
	
	@Autowired
	private GenericDao<PendingCase> pendingCaseDao;
	
	
	
	
	@Autowired
	private GenericDao<PendingCaseDocument> pendingCaseDoc;
	
	
	
	@Autowired
	private GenericDao<OutwardCase> outwardCaseDao;
	
	
	@Override
	public Long savePendingCase(PendingCase pendingCase) {
		return pendingCaseDao.save(pendingCase);
	}
	
	@Override
	public boolean updatePendingCase(PendingCase pendingCase) {
		return pendingCaseDao.update(pendingCase);
	}

	@Override
	public List<PendingCase> getAllPendingCaseList() {
		return pendingCaseDao.getAllListByOrder(PendingCase.class, Order.desc("pendingCaseId"));
	}

	@Override
	public PendingCase getPendingCaseByPendingCaseId(Long pendingCaseId) {
		return pendingCaseDao.getById(PendingCase.class, pendingCaseId);
	}

	@Override
	public PendingCase getPendingCaseByCaseNoAndCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory) {
		String hql="";
		if(caseCategory!="") {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"'";
		}else {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'";
		} 
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return pendingCaseList.get(0);
		}
		return null;
	}
	

//	By Bilal
	@Override
	public PendingCase getPendingCaseByCaseNoAndCaseYearAndCaseCategoryAndcaseStageAndcaseType(String caseNo, String caseYear, String caseCategory,String caseStage, String caseType) {
		String hql="";
		
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseStage='"+caseStage+"' and  caseType='"+caseType+"' and caseCategory='"+caseCategory+"'";
			 
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return pendingCaseList.get(0);
		}
		return null;
	}

	@Override
	public List<PendingCase> getAllPendingCaseListByPagination(int pageNo, int maxRecord) {
		return pendingCaseDao.getAllListByPaginationAndOrder(PendingCase.class, pageNo, maxRecord, Order.desc("pendingCaseId"));
	}

	@Override
	public boolean checkPendingCase(String caseNo, String caseYear, String hearingDate) {
		String hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'";
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return true;
		}
		return false;
	}
	
	


	public List<PendingCase> getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit(String caseNo, String caseYear, String caseCategory) {
		String hql="";
		if(caseCategory!="" ) {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"'";
		}else {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'";
		}
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return pendingCaseList;
		}
		return null;
	}
	
	
	@Override
	public List<PendingCase> getAllPendingCaseByCaseNoAndCaseYearAndCaseCategoryEdit1(String caseNo, String caseYear, String caseCategory) {
		String hql="";
		if(caseCategory != null ) {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"'";
		}else {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'";
		}
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return pendingCaseList;
		}
		return null;
	}


	@Override
	public List<PendingCaseDocument> getPendingCaseDocumentByPendingCaseId(Long pendingCaseId) {
		return pendingCaseDoc.getAllListByFieldName(PendingCaseDocument.class, "pendingCaseId", pendingCaseId);
	}
	
	
	@Override
	public List<PendingCase> getAllPendingCaseByCaseNoAndCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory) {
		String hql="";
		String caseStage ="PENDING FOR SCAN";
		
		if(caseCategory!="") {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'  and caseStage = '"+caseStage+"' and caseCategory='"+caseCategory+"'";
		}/*else {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseStage = '"+caseStage+"' and caseYear='"+caseYear+"'";
		}*/
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return pendingCaseList;
		}
		return null;
	}

	@Override
	public List<PendingCase> getPendingCaseByCaseNoAndCaseYearAndCaseCategoryForQc(String caseNo, String caseYear,
			String caseCategory) {
		String hql="";
		String caseStage ="SCANNING DONE";
		
		if(caseCategory!="") {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'  and caseStage = '"+caseStage+"' and caseCategory='"+caseCategory+"'";
		}/*else {
			 hql="from PendingCase where caseNo='"+caseNo+"' and caseStage = '"+caseStage+"' and caseYear='"+caseYear+"'";
		}*/
		List<PendingCase> pendingCaseList=pendingCaseDao.getAllListByQuery(hql);
		if(pendingCaseList!=null) {
			return pendingCaseList;
		}
		return null;
	}

	@Override
	public Long saveOutwardCase(OutwardCase outwardCase) {
		return outwardCaseDao.save(outwardCase);
	}

	@Override
	public List<OutwardCase> getAllOutwardCaseByCaseNoAndCaseYearAndCaseCategoryEdit(String caseNo, String caseYear,
			String caseCategory) {

		String hql="";
		
		if(caseCategory!="") {
			 hql="from OutwardCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"' and caseCategory='"+caseCategory+"'";
		}else {
			 hql="from OutwardCase where caseNo='"+caseNo+"' and caseYear='"+caseYear+"'";
		}
		List<OutwardCase> outwardCaseList=outwardCaseDao.getAllListByQuery(hql);
		if(outwardCaseList!=null) {
			return outwardCaseList;
		}
	
		return null;
	}
	

}
