package com.dms.service;

import java.util.Date;
import java.util.List;

import com.dms.model.InvoiceDetail;

public interface InvoiceDetailService {
	
	public Long saveInvoiceDetail(InvoiceDetail invoiceDetail);
	
	public boolean updateInvoiceDetail(InvoiceDetail invoiceDetail);
	
	public List<InvoiceDetail> getAllInvoiceDetailList();
	
	public InvoiceDetail getInvoiceDetailByAutoNo(Long autoNo);
	
	public List<InvoiceDetail> getAllInvoiceDetailListByCaseType(String caseType);
	
	public List<InvoiceDetail> getAllInvoiceDetailListByDate(Date fromDate, Date toDate);
	
	public List<InvoiceDetail> getAllCancelledInvoiceDetailListByDate(Date fromDate, Date toDate);
	
	public List<InvoiceDetail> getAllInvoiceDetailListByDateAndUsername(Date fromDate, Date toDate, String userFullName);
	
	public List<InvoiceDetail> getAllCancelledInvoiceDetailListByDateAndUsername(Date fromDate, Date toDate, String userFullName);

	public InvoiceDetail getInvoiceDetailByBillNo(String billNo);
	
	public List<InvoiceDetail> getAllInvoiceDetailListByUsername(String username);
	
	public List<InvoiceDetail> getAllInvoiceDetailListByAutoNoAndCaseType(Long autoNo, String caseType, String caseNo);
	
	/*public List<Object> getAllInvoiceDetailUsingGroupBy(Date fromDate, Date toDate);*/
	public List<Object> getAllInvoiceDetailUsingGroupBy(String fromDate, String toDate);
	
	public String getInvoiceNo();
	

}
