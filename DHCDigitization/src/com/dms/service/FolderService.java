package com.dms.service;

import java.util.List;

import com.dms.model.Folder;

public interface FolderService {
	
	public long saveFolder(Folder folder);
	
	public boolean updateFolder(Folder folder);
	
	public Folder getFolderByFolderId(long folderId);
	
	public List<Folder> getAllFolderList();
	
	public List<Folder> getAllFolderListByFolderName(String folderName);
	
	public Folder getFolderByFolderName(String folderName, String folderType);
	
}
