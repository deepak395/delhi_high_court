package com.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.UserRights;

@Transactional
@Service
public class UserRightsServiceImpl implements UserRightsService {
	
	@Autowired
	private GenericDao<UserRights> userRightsDao;

	@Override
	public Long saveUserRights(UserRights userRights) {
		return userRightsDao.save(userRights);
	}

	@Override
	public boolean updateUserRights(UserRights userRights) {
		return userRightsDao.update(userRights);
	}

	@Override
	public List<UserRights> getAllUserRightsListByUsername(String username) {
		return userRightsDao.getAllListByFieldName(UserRights.class, "username", username);
	}

	@Override
	public UserRights getUserRightsByUsernameAndsubmenuId(String username, Long menuId, Long submenuId) {
		String hql="from UserRights where username='"+username+"' and menuId="+menuId+" and submenuId="+submenuId;
		List<UserRights> userRightsList=userRightsDao.getAllListByQuery(hql);
		if(userRightsList!=null) {
			return userRightsList.get(0);
		}
		return null;
	}

	@Override
	public List<UserRights> getAllUserRightsListByUsername(String username, Long menuId) {
		return userRightsDao.getAllListByFieldName(UserRights.class, "username", username, "menuId", menuId);
	}

}