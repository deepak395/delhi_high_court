package com.dms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.UserLog;

@Service
@Transactional
public class UserLogServiceImpl implements UserLogService {
	
	@Autowired
	private  GenericDao<UserLog> userLogDao;

	@Override
	public Long saveUserLog(UserLog userLog) {
		
		
		return  userLogDao.save(userLog);
		
		 
		
		
		
		
	}
	
	@Override
	public boolean updateUserLog(UserLog userLog) {
		return userLogDao.update(userLog);
	}

	@Override
	public UserLog getUserLogById(Long id) {
		return userLogDao.getById(UserLog.class, id);
	}

}
