package com.dms.service;

import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.Lookup;

@Transactional
@Service
public class LookupServiceImpl implements LookupService {
	
	@Autowired
	private GenericDao<Lookup> lookupDao;

	@Override
	public Long saveLookup(Lookup lookup) {
		return lookupDao.save(lookup);
	}

	@Override
	public List<Lookup> getAllLookupList() {
		return lookupDao.getAllListByOrder(Lookup.class, Order.asc("lookupValue"));
	}

	@Override
	public Lookup getLookupById(Long lookupId) {
		return lookupDao.getById(Lookup.class, lookupId);
	}

	@Override
	public List<Lookup> getAllLookupListByLookupName(String lookupName) {
		return lookupDao.getAllListByFieldName(Lookup.class, "lookupName", lookupName);
	}

	@Override
	public List<Lookup> getLookUpByName(String name) {
		// TODO Auto-generated method stub
		return lookupDao.getLookUpByName(name);
	}

	@Override
	public List<Lookup> getAllCaseCategory(String string) {
		// TODO Auto-generated method stub
		return lookupDao.getAllCaseCategory(string);
	}

	

}
