package com.dms.service;

import java.util.List;

import com.dms.model.Submenu;

public interface SubmenuService {
	
	public List<Submenu> getAllSubmenuListByMenuId(long menuId);
	
	public Submenu getSubmenuBySubmenuId(long submenuId);
	
	public List<Submenu> getAllSubmenuListByRoleName(String roleName);

}
