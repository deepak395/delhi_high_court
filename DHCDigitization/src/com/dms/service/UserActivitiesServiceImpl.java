package com.dms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.UserActivities;

@Service
@Transactional
public class UserActivitiesServiceImpl implements UserActivitiesService {
	
	@Autowired
	private GenericDao<UserActivities> userActivitiesDao;

	@Override
	public Long saveUserActivities(UserActivities activity) {
		return userActivitiesDao.save(activity);
	}

}
