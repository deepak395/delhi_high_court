package com.dms.service;

import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.UserRole;

@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	private GenericDao<UserRole> userRoleDao;

	@Override
	public Long saveUserRole(UserRole userRole) {
		return userRoleDao.save(userRole);
	} 
	
	@Override
	public boolean updateUserRole(UserRole userRole) {
		return userRoleDao.update(userRole);
	}
	
	@Override
	public boolean saveOrUpdateUserRole(UserRole userRole) {
		return userRoleDao.saveOrUpdate(userRole);
	}

	@Override
	public List<UserRole> getAllUserRoleList() {
		return userRoleDao.getAllListByOrder(UserRole.class, Order.desc("roleId"));
	}

	@Override
	public UserRole getUserRoleByRoleId(Long userRoleId) {
		return userRoleDao.getById(UserRole.class, userRoleId) ;
	}

	@Override
	public UserRole getUserRoleByRoleName(String roleName) {
		return userRoleDao.getAllListByFieldName(UserRole.class, roleName, roleName).get(0);
	}

}
