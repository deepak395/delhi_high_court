package com.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.FreshCase;

@Transactional
@Service
public class FreshCaseServiceImpl implements FreshCaseService {
	
	@Autowired
	private GenericDao<FreshCase> freshCaseDao;

	@Override
	public Long saveFreshCase(FreshCase freshCase) {
		return freshCaseDao.save(freshCase);
	}
	
	@Override
	public boolean updateFreshCase(FreshCase freshCase) {
		return freshCaseDao.update(freshCase);
	}

	@Override
	public List<FreshCase> getAllFreshCaseList() {
		return freshCaseDao.getAllList(FreshCase.class);
	}

	@Override
	public FreshCase getFreshCaseById(Long id) {
		return freshCaseDao.getById(FreshCase.class, id);
	}

	@Override
	public FreshCase getFreshCaseByDiaryNoAndCaseStage(String diaryNo, String caseStage) {
		List<FreshCase> freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "diaryNo", diaryNo, "caseStage", caseStage);
		if(freshCaseList!=null) {
			return freshCaseList.get(0);
		}
		return null;
	}

	@Override
	public List<FreshCase> getAllFreshCaseListByCaseType(String caseType) {
		return freshCaseDao.getAllListByFieldName(FreshCase.class, "caseType", caseType);
	}

	@Override
	public List<FreshCase> getAllFreshCaseListByCaseTypeAndCaseStage(String caseType, String caseStage) {
		return freshCaseDao.getAllListByFieldName(FreshCase.class, "caseType", caseType, "caseStage", caseStage);
	}

	@Override
	public FreshCase getFreshCaseByDiaryNo(String diaryNo) {
		List<FreshCase> freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "diaryNo", diaryNo);
		if(freshCaseList!=null) {
			return freshCaseList.get(0);
		}
		return null;
	}

	@Override
	public FreshCase getFreshCaseByAutoNo(Long autoNo) {
		List<FreshCase> freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "autoNo", autoNo);
		if(freshCaseList!=null) {
			return freshCaseList.get(0);
		}
		return null;
	}

	@Override
	public List<FreshCase> getAllFreshCaseListByDiaryNoAndAutoNo(Long autoNo, String diaryNo, String year) {
		List<FreshCase> freshCaseList=null;
		if(autoNo!=null) {
			freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "autoNo", autoNo);
		}else if(diaryNo!="") {
			freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "diaryNo", diaryNo);
		}else if(year!="") {
			freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "year", year);
		}
		return freshCaseList;
	}

	@Override
	public FreshCase getFreshCaseByDiaryNo(String diaryNo, String documentName) {
		List<FreshCase> freshCaseList=freshCaseDao.getAllListByFieldName(FreshCase.class, "diaryNo", diaryNo, "documentName", documentName);
		if(freshCaseList!=null) {
			return freshCaseList.get(0);
		}
		return null;
	}

	@Override
	public FreshCase getFreshCaseByDiaryNoAndYear(String diaryNo, String year) {
		List<FreshCase> fc =null;
		try {
			fc = freshCaseDao.getAllListByFieldName(FreshCase.class, "diaryNo", diaryNo, "year", year);
			if(fc != null){
				return fc.get(0);
				
			}
		}
		
		catch(Exception e){
			return null;
		}
		
		
		return null;
	}


}
