package com.dms.service;

import java.util.List;

import com.dms.model.PendingCaseDocument;

public interface PendingCaseDocumentService {
	
	public Long savePendingCaseDocument(PendingCaseDocument documentDetail);
	
	public boolean updatePendingCaseDocument(PendingCaseDocument documentDetail);
	
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByPendingCaseId(Long pendingCaseId, String caseStage);
	
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByPendingCaseId(Long pendingCaseId);
	
	public PendingCaseDocument getPendingCaseDocumentByDocumentId(Long documentId);
	
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory, String caseStage);
	
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByCaseNoAnsCaseYearAndCaseCategory(String caseNo, String caseYear, String caseCategory);
	
	public PendingCaseDocument getPendingCaseDocumentByDocumentName(String documentName, String caseNo, String caseYear, String caseCategory);

}
