package com.dms.service;

import java.util.List;

import com.dms.model.FreshCase;

public interface FreshCaseService {
	
	public Long saveFreshCase(FreshCase freshCase);
	
	public boolean updateFreshCase(FreshCase freshCase);
	
	public List<FreshCase> getAllFreshCaseList();
	
	public FreshCase getFreshCaseById(Long id);
	
	public FreshCase getFreshCaseByDiaryNoAndCaseStage(String diaryNo, String caseStage);
	
	public List<FreshCase> getAllFreshCaseListByCaseType(String caseType);
	
	public List<FreshCase> getAllFreshCaseListByCaseTypeAndCaseStage(String caseType, String caseStage);
	
	public FreshCase getFreshCaseByDiaryNo(String diaryNo);
	
	public FreshCase getFreshCaseByAutoNo(Long autoNo);
	
	public List<FreshCase> getAllFreshCaseListByDiaryNoAndAutoNo(Long autoNo, String diaryNo, String year);
	
	public FreshCase getFreshCaseByDiaryNo(String diaryNo, String documentName);

	public FreshCase getFreshCaseByDiaryNoAndYear(String diaryNo, String year);

}
