package com.dms.service;

import java.util.Date;
import java.util.List;

import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

public interface AllReportService {
	
	public List<FreshCaseDocument> getAllFreshReportListByDateAndStage(Date fromDate, Date toDate, String caseStage);
	
	public List<FreshCaseDocument> getAllFreshReportListByDateAndStageAndUsername(Date fromDate, Date toDate, String caseStage, String username);
	
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByDateAndStage(Date fromDate, Date toDate, String caseStage);
	
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByDateAndStageAndUsername(Date fromDate, Date toDate, String caseStage, String username);
   
	public List<PendingCase> getAllPendingCaseDataEntryDetailListByDate(Date fromDate, Date toDate);
	
	public List<PendingCase> getAllPendingCaseDataEntryDetailListByDateAndUsername(Date fromDate, Date toDate, String username);
	
	public List<PendingCase> getAllPendingHearingReportByDate(String fromDate, String toDate,String courtNo,String username);
	
	public List<PendingCase> getDataEntryReportByDate(String fromDate, String toDate,String username);

	
	
	public List<PendingCase> getUserWiseBacklog(Date fromDate, Date toDate,String courtNo);
	
	public List<FreshCase> getAllFreshCaseForTaxInvoice(String fromDate, String toDate);
	
	public List<Object> getProductivityReportPending(String stage,Date fromDate, Date toDate);
	
	public List<Object> getProductivityReportFresh(String stage,Date fromDate, Date toDate);


	
}
