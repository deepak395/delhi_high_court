package com.dms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.dao.GenericDao;
import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

@Transactional
@Service
public class AllReportServiceImpl implements AllReportService {

	@Autowired
	private GenericDao<FreshCaseDocument> freshCaseReportDao;
	
	@Autowired
	private GenericDao<PendingCaseDocument> pendingReportDao;
                                       
	@Autowired
	private GenericDao<PendingCase> pendingDataEntryReportDao;
	
	@Autowired
	private GenericDao<FreshCase> freshCaseDao;

	@Autowired
	SessionFactory sessionFactory=null;
	
	Session session=null;
	
	Criteria criteria=null;
	
	
	@Override
	public List<FreshCaseDocument> getAllFreshReportListByDateAndStage(Date fromDate, Date toDate, String caseStage) {

		List<FreshCaseDocument> freshCases = new ArrayList<FreshCaseDocument>();
		List<FreshCaseDocument> freshCaseList = null;
		if (caseStage.contains("SCANNING DONE")) {
			freshCaseList = freshCaseReportDao.getAllListByDate(FreshCaseDocument.class, "scanningOn", fromDate, toDate);
			if (freshCaseList != null) {
				freshCases.addAll(freshCaseList);
			}
		}
		if (caseStage.contains("QC DONE")) {
			freshCaseList = freshCaseReportDao.getAllListByDate(FreshCaseDocument.class, "qcOn", fromDate, toDate);
			if (freshCaseList != null) {
				freshCases.addAll(freshCaseList);
			}
		}
		if (caseStage.contains("BOOKMARKING DONE")) {
			freshCaseList = freshCaseReportDao.getAllListByDate(FreshCaseDocument.class, "bookmarkingOn", fromDate, toDate);
			if (freshCaseList != null) {
				freshCases.addAll(freshCaseList);
			}
		}
		return freshCases;
	}

	@Override
	public List<FreshCaseDocument> getAllFreshReportListByDateAndStageAndUsername(Date fromDate, Date toDate, String caseStage,
			String username) {

		List<FreshCaseDocument> freshCases = new ArrayList<FreshCaseDocument>();
		List<FreshCaseDocument> freshCaseList = null;
		if (caseStage.contains("SCANNING DONE")) {
			freshCaseList = freshCaseReportDao.getAllListByDateAndField(FreshCaseDocument.class, "scanningOn", fromDate, toDate,
					"scanningfullname", username);
			if (freshCaseList != null) {
				freshCases.addAll(freshCaseList);
			}
		}
		if (caseStage.contains("QC DONE")) {
			freshCaseList = freshCaseReportDao.getAllListByDateAndField(FreshCaseDocument.class, "qcOn", fromDate, toDate,
					"qcfullname", username);
			if (freshCaseList != null) {
				freshCases.addAll(freshCaseList);
			}
		}
		if (caseStage.contains("BOOKMARKING DONE")) {
			freshCaseList = freshCaseReportDao.getAllListByDateAndField(FreshCaseDocument.class, "bookmarkingOn", fromDate,
					toDate, "bookmarkingfullname", username);
			if (freshCaseList != null) {
				freshCases.addAll(freshCaseList);
			}
		}
		return freshCases;
	}

	@Override
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByDateAndStage(Date fromDate, Date toDate,
			String caseStage) {

		List<PendingCaseDocument> pendingCases = new ArrayList<PendingCaseDocument>();
		List<PendingCaseDocument> pendingCaseList = null;
		if (caseStage.contains("SCANNING DONE")) {
			pendingCaseList = pendingReportDao.getAllListByDate(PendingCaseDocument.class, "scanningOn", fromDate,
					toDate);
			if (pendingCaseList != null) {
				pendingCases.addAll(pendingCaseList);
			}
		}
		if (caseStage.contains("QC DONE")) {
			pendingCaseList = pendingReportDao.getAllListByDate(PendingCaseDocument.class, "qcOn", fromDate, toDate);
			if (pendingCaseList != null) {
				pendingCases.addAll(pendingCaseList);
			}
		}
		return pendingCases;
	}

	@Override
	public List<PendingCaseDocument> getAllPendingCaseDocumentListByDateAndStageAndUsername(Date fromDate, Date toDate,
			String caseStage, String username) {

		List<PendingCaseDocument> pendingCases = new ArrayList<PendingCaseDocument>();
		List<PendingCaseDocument> pendingCaseDocumentList = null;
		if (caseStage.contains("SCANNING DONE")) {
			pendingCaseDocumentList = pendingReportDao.getAllListByDateAndField(PendingCaseDocument.class, "scanningOn",
					fromDate, toDate, "scanningBy", username);
			if (pendingCaseDocumentList != null) {
				pendingCases.addAll(pendingCaseDocumentList);
			}
		}
		if (caseStage.contains("QC DONE")) {
			pendingCaseDocumentList = pendingReportDao.getAllListByDateAndField(PendingCaseDocument.class, "qcOn",
					fromDate, toDate, "qcBy", username);
			if (pendingCaseDocumentList != null) {
				pendingCases.addAll(pendingCaseDocumentList);
			}
		}
		return pendingCases;
	}

	@Override
	public List<PendingCase> getAllPendingCaseDataEntryDetailListByDate(Date fromDate, Date toDate) {
		return pendingDataEntryReportDao.getAllListByDate(PendingCase.class, "createdOn", fromDate, toDate);
	}

	@Override
	public List<PendingCase> getAllPendingCaseDataEntryDetailListByDateAndUsername(Date fromDate, Date toDate,
			String username) {
		return pendingDataEntryReportDao.getAllListByDateAndField(PendingCase.class, "createdOn", fromDate, toDate,
				"createdBy", username);

	}

	

		  
	@Override
	public List<PendingCase> getUserWiseBacklog(Date fromDate, Date toDate, String userFullname) {
		 return pendingDataEntryReportDao.getAllListByDateAndField(PendingCase.class, "scannedOn", fromDate, toDate,"scannerFullname",userFullname);
	  }

	@Override
	public List<PendingCase> getAllPendingHearingReportByDate(String fromDate, String toDate, String courtNo,String username) {
		 if(fromDate!=null && toDate!=null && courtNo!=null && courtNo!="" ) {
				return pendingDataEntryReportDao.getAllListByQuery("from PendingCase where hearing_report>='"+fromDate+"' and  hearing_report<='"+toDate+"' and courtNo='"+courtNo+"'");
		 }
		 
		 else if((username!=null && !username.equals(""))&&(fromDate!=null && toDate!=null)) {
        return pendingDataEntryReportDao.getAllListByQuery("from PendingCase where hearing_report>='"+fromDate+"' and  hearing_report<='"+toDate+"' and createdBy='"+username+"'");
			 
		 }
		  else {
				return pendingDataEntryReportDao.getAllListByQuery("from PendingCase where hearing_report>='"+fromDate+"' and  hearing_report<='"+toDate+"'");

		  }

	}

	@Override
	public List<FreshCase> getAllFreshCaseForTaxInvoice(String fromDate, String toDate) {
		return freshCaseDao.getAllListByQuery("from FreshCase where scanningOn is not null and scanningOn>='"+fromDate+"' and scanningOn<='"+toDate +"' and status=true");
	}

	
	
	@Override
	public List<Object> getProductivityReportPending(String stage,Date from_date,Date to_date) {
		List<Object> list=null;
		String hql=null;
		if(stage.equals("SCANNING DONE")) {
		hql ="select pcd.scanningBy,u.userFullName,sum(pcd.scanningPage)  FROM PendingCaseDocument pcd,User u WHERE u.username=pcd.scanningBy and pcd.scanningOn>=:from_date and pcd.scanningOn<=:to_date group by pcd.scanningBy,u.userFullName order by pcd.scanningBy  ";
		}
		else if(stage.equals("QC DONE")) {
			hql ="select pcd.qcBy,u.userFullName,sum(pcd.qcPage)"+ "FROM PendingCaseDocument pcd,User u WHERE u.username=pcd.qcBy and pcd.qcOn>=:from_date and pcd.qcOn<=:to_date group by pcd.qcBy,u.userFullName order by  pcd.qcBy  ";
		}
		
		else  {
			hql ="select pc.createdBy,createdBy,count(*)"+ "FROM PendingCase pc WHERE  pc.createdOn>=:from_date and pc.createdOn<=:to_date group by pc.createdBy order by  pc.createdBy  ";
		}
		
		try {
			session=sessionFactory.getCurrentSession();
			Query query=session.createQuery(hql);
			query.setParameter("from_date",from_date);
			query.setParameter("to_date",to_date);
			list=query.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return list;

}
	
	
	@Override
	public List<Object> getProductivityReportFresh(String stage,Date from_date,Date to_date) {
		List<Object> list=null;
		String hql=null;
		if(stage.equals("SCANNING DONE")) {
		hql ="select fcd.scanningBy,u.userFullName,sum(fcd.scanningPage)  FROM FreshCaseDocument fcd,User u WHERE u.username=fcd.scanningBy and fcd.scanningOn>=:from_date and fcd.scanningOn<=:to_date group by fcd.scanningBy,u.userFullName order by fcd.scanningBy  ";
		}
		else {
			hql ="select fcd.qcBy,u.userFullName,sum(fcd.qcPage) FROM FreshCaseDocument fcd,User u WHERE u.username=fcd.qcBy and fcd.qcOn>=:from_date and fcd.qcOn<=:to_date group by fcd.qcBy,u.userFullName order by  fcd.qcBy  ";
		}
		
		try {
			session=sessionFactory.getCurrentSession();
			Query query=session.createQuery(hql);
			query.setParameter("from_date",from_date);
			query.setParameter("to_date",to_date);
			list=query.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return list;

}

	@Override
	public List<PendingCase> getDataEntryReportByDate(String fromDate, String toDate, String username) {
		// TODO Auto-generated method stub
			 if(fromDate!=null && toDate!=null && username!=null) {
				 
			        return pendingDataEntryReportDao.getAllListByQuery("from PendingCase where createdOn>='"+fromDate+"' and  createdOn<='"+toDate+"' and createdBy='"+username+"'");
				 
			 }
			 else {
					return pendingDataEntryReportDao.getAllListByQuery("from PendingCase where createdOn>='"+fromDate+"' and  createdOn<='"+toDate+"'");

				 
			 }
			 

		}
	
	
	
}