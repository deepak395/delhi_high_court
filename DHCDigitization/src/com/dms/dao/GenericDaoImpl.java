package com.dms.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dms.model.Lookup;
/**
 * 
 * @author Rajkumar Giri
 *
 * 
 */
@Repository
public class GenericDaoImpl<T> implements GenericDao<T> {
	
	private static Logger logger=Logger.getLogger(GenericDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory=null;
	
	Session session=null;
	
	Criteria criteria=null;
	
	@Override
	public Long save(T obj) {
		try {
			
			Long id=null;
				session=sessionFactory.getCurrentSession();
				 id=(Long)session.save(obj);
					if(id>0) {
				return id;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@Override
	public boolean update(T obj) {
		try {
			session=sessionFactory.getCurrentSession();
			session.update(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return true;
	}

	@Override
	public boolean saveOrUpdate(T obj) {
		try {
			session=sessionFactory.getCurrentSession();
			session.saveOrUpdate(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getById(final Class<T> typeClass, Long id) {
		try {
			session=sessionFactory.getCurrentSession();
			T obj=(T)session.get(typeClass, id);
			if(obj!=null) {
				return obj;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllList(final Class<T> typeClass) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			criteria.add(Restrictions.eq("status", true));
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByOrder(Class<T> typeClass, Order order) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			criteria.add(Restrictions.eq("status", true));
			criteria.addOrder(order);
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByPaginationAndOrder(final Class<T> typeClass, int pageNo, int maxRecord, Order order) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			criteria.setFirstResult(maxRecord*(pageNo-1));
			criteria.setMaxResults(maxRecord);
			/*criteria.add(Restrictions.eq("status", true));
			criteria.add(Restrictions.eq("status", true));*/
			criteria.add(Restrictions.and(Restrictions.eq("caseStage", "PENDING FOR SCAN"), Restrictions.eq("status", true)));
			
			criteria.addOrder(order.desc("pendingCaseId"));
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByFieldName(final Class<T> typeClass, String fieldName, Object fieldValue) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			criteria.add(Restrictions.and(Restrictions.eq(fieldName, fieldValue), Restrictions.eq("status", true)));
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByFieldName(Class<T> typeClass, String fieldName1, Object fieldValue1, String fieldName2, Object fieldValue2) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			Criterion criterion=Restrictions.and(Restrictions.eq(fieldName1, fieldValue1), Restrictions.eq(fieldName2, fieldValue2));
			criteria.add(Restrictions.and(criterion, Restrictions.eq("status", true)));
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByFieldNameAndOrder(Class<T> typeClass, String fieldName, Object fieldValue, Order order) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			criteria.add(Restrictions.and(Restrictions.eq(fieldName, fieldValue), Restrictions.eq("status", true)));
			criteria.addOrder(order);
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByDate(Class<T> typeClass, String dateFieldName, Object fieldValue1, Object fieldValue2) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			Criterion criterion=Restrictions.and(Restrictions.ge(dateFieldName, fieldValue1), Restrictions.le(dateFieldName, fieldValue2));
			criteria.add(Restrictions.and(criterion, Restrictions.eq("status", true)));
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByDateAndField(Class<T> typeClass, String dateFieldName, Object fromDate, Object toDate, String fieldName, Object fieldValue) {
		try {
			session=sessionFactory.getCurrentSession();
			criteria=session.createCriteria(typeClass);
			Criterion criterion1=Restrictions.and(Restrictions.ge(dateFieldName, fromDate), Restrictions.le(dateFieldName, toDate));
			Criterion criterion2=Restrictions.and(Restrictions.eq(fieldName, fieldValue), Restrictions.eq("status", true));
			criteria.add(Restrictions.and(criterion1, criterion2));
			List<T> list=criteria.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
		
			e.printStackTrace();
			return null;
		}
		finally{
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllListByQuery(String hql) {
		try {
			session=sessionFactory.getCurrentSession();
			Query query=session.createQuery(hql);
			List<T> list=query.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@Override
	public T getByField(Class<T> typeClass, String fieldName, Object fieldValue) {
		try {		
		session=sessionFactory.getCurrentSession();
				criteria=session.createCriteria(typeClass);
				Criterion criterion=Restrictions.and(Restrictions.eq(fieldName, fieldValue));
				criteria.add(Restrictions.and(criterion, Restrictions.eq("status", true)));
				 T obj=(T) criteria.setMaxResults(1);
				if(obj!=null) {
					return obj;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		finally{
			
		}
			return null;
		}

	@Override
	public List<Lookup> getLookUpByName(String name) {
		String hql ="FROM Lookup l WHERE l.lookupName =:string ORDER BY l.lookupValue ASC";
		try {
			session=sessionFactory.getCurrentSession();
			Query query=session.createQuery(hql);
			query.setParameter("string",name);
			List<Lookup> list=query.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	@Override
	public List<Lookup> getAllCaseCategory(String string) {
		
		String hql ="FROM Lookup l WHERE l.lookupName =:string ORDER BY l.lookupValue ASC";
		try {
			session=sessionFactory.getCurrentSession();
			Query query=session.createQuery(hql);
			query.setParameter("string",string);
			List<Lookup> list=query.list();
			if(list.size()>0) {
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	

	
	
	
@SuppressWarnings("unchecked")
@Override
public List<Object> getBillReportUsingGroupBy(String fromDate,String toDate) {
	
	String hql ="select createdBy,COALESCE(sum(cast(noOfPage as bigint)),0) as page,COALESCE(sum(cast(amount as bigint)),0) as amount  from InvoiceDetail WHERE to_date(to_char(createdOn,'dd-mm-yyyy'),'dd-mm-yyyy') between to_date('"+fromDate+"','dd-mm-yyyy') and to_date('"+toDate+"','dd-mm-yyyy') group by createdBy";
	try {
		session=sessionFactory.getCurrentSession();
		Query query=session.createQuery(hql);
		List<Object> list=query.list();
		if(list.size()>0) {
			return list;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	finally{
		
	}
	return null;
}

@SuppressWarnings("unchecked")
@Override
public Object getByNativeQuery(String sql) {
	try {
		session=sessionFactory.getCurrentSession();
		SQLQuery sqlquery=session.createSQLQuery(sql);
		List<Object> list=sqlquery.list();
		if(list.size()>0) {
			return list.get(0);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
	


@Override
public List<Object> getProductivityReport(String stage,Date from_date,Date to_date) {
	
	String hql=null;
	if(stage.equals("SCAN")) {
	hql ="select pcd.scanningBy,sum(pcd.scanningPage)  FROM PendingCaseDocument pcd WHERE pcd.scanningOn>=:from_date and pcd.scanningOn<=:to_date group by pcd.scanningBy  ";
	}
	else {
		hql ="select pcd.qcBy,sum(pcd.qcPage)  FROM PendingCaseDocument pcd WHERE pcd.qcOn>=:from_date and pcd.qcOn<=:to_date group by pcd.qcBy  ";
	}
	
	try {
		session=sessionFactory.getCurrentSession();
		Query query=session.createQuery(hql);
		query.setParameter("from_date",from_date);
		query.setParameter("to_date",to_date);
		List<Object> list=query.list();
		if(list.size()>0) {
			return list;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	finally{
		
	}
	return null;
}
}













	/*@Override
	public Lookup getLookUpByName(String name) {
		
		Lookup l =null;
		// TODO Auto-generated method stub
		try {		
			session=sessionFactory.getCurrentSession();
					
				l= session.createQuery("select l from Lookup l where l.lookupName=:name").setParameter("name",name).get
				} catch (Exception e) {
					e.printStackTrace();
				}
			finally{
				
			}
				return null;
			}*/
	


