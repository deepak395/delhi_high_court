package com.dms.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Order;

import com.dms.model.Lookup;

/**
 * 
 * @author Rajkumar Giri
 *
 * 
 */
public interface GenericDao<T> {
	
	public Long save(T obj);
	
	public boolean update(T obj);
	
	public boolean saveOrUpdate(T obj);
	
	public T getById(final Class<T> typeClass, Long id);
	
	public List<T> getAllList(final Class<T> typeClass);
	
	public List<T> getAllListByOrder(final Class<T> typeClass, Order order);
	
	public List<T> getAllListByPaginationAndOrder(final Class<T> typeClass, int pageNo, int maxRecord, Order order);
	
	public List<T> getAllListByFieldName(final Class<T> typeClass, String fieldName, Object fieldValue);
	
	public List<T> getAllListByFieldName(final Class<T> typeClass, String fieldName1, Object fieldValue1, String fieldName2, Object fieldValue2);

	public List<T> getAllListByFieldNameAndOrder(final Class<T> typeClass, String fieldName, Object fieldValue, Order order);
	
	public List<T> getAllListByDate(final Class<T> typeClass, String dateFieldName, Object fieldValue1, Object fieldValue2);
	
	public List<T> getAllListByDateAndField(final Class<T> typeClass, String dateFieldName, Object fromDate, Object toDate, String fieldName, Object fieldValue);
	
	public List<T> getAllListByQuery(String hql);
	
	public T  getByField(final Class<T> typeClass, String fieldName, Object fieldValue);

	public List<Lookup> getLookUpByName(String name);

	public List<Lookup> getAllCaseCategory(String string);
	/*public List<Object> getBillReportUsingGroupBy(Date fromDate,Date toDate);*/
	public List<Object> getBillReportUsingGroupBy(String fromDate,String toDate);
	
	public Object getByNativeQuery(String sql);
	
	
	public List<Object> getProductivityReport(String Stage, Date startDate, Date endDate);

	
}
