package com.dms.taxiinvoice;


import java.io.FileOutputStream;
import java.text.DecimalFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dms.model.FreshCase;
import com.dms.model.InvoiceDetail;
import com.dms.model.Lookup;
import com.dms.service.InvoiceDetailService;
import com.dms.service.LookupService;
import com.dms.utility.FinancialYearCalculator;
import com.dms.utility.NumberToWords;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author Rajkumar Giri and Rupnarayan
 *
 */

@Component
public class TaxInvoicePdfGenerator{
	
	@Autowired
	private InvoiceDetailService invoiceDetailService;
	
	@Autowired
	private LookupService lookupService;

	public void generateTaxInvoice(FreshCase freshCase ) throws Exception {
		
		java.util.List<Lookup> gstRateList=lookupService.getAllLookupListByLookupName("GST_RATE");
		Lookup lookupGST=gstRateList.get(0);
		if(lookupGST!=null){
			InvoiceDetail invoiceDetail=invoiceDetailService.getInvoiceDetailByAutoNo(freshCase.getAutoNo());
			
			Font smallFont = FontFactory.getFont(FontFactory.HELVETICA);
		    smallFont.setSize(10);
		    
		    Font smallBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		    smallBoldFont.setSize(10);
		    
		    Font smallFontBoldUnderline = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		    smallFontBoldUnderline.setStyle(Font.UNDERLINE);
		    smallFontBoldUnderline.setSize(9);
		    
		    Font mediumFontBoldUnderLine = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		    mediumFontBoldUnderLine.setStyle(Font.UNDERLINE);
		    mediumFontBoldUnderLine.setSize(12);
		    
		    Font mediaumFont = FontFactory.getFont(FontFactory.HELVETICA);
		    mediaumFont.setSize(12);
		    
		    Font mediaumBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		    mediaumBoldFont.setSize(12);
		    
		    DecimalFormat decimalFormat=new DecimalFormat("0.00");
		    DecimalFormat decimalFormat1 = new DecimalFormat("0.#####");

	        if(invoiceDetail!=null){
	        	
	        	String finYear=FinancialYearCalculator.getCurrentFinancialYear();
	    		String first2 = String.valueOf(finYear).substring(2,4);
	    		String last2 = String.valueOf(finYear).substring(7,9);
	    		String invoice="DL"+first2+last2+"DI";
	        	
	        	double gstRate=Double.parseDouble(lookupGST.getLookupValue());
	        	double basicAmount= ((invoiceDetail.getAmount()*100)/(gstRate+100));
	        	double gst=((basicAmount*gstRate)/100); //gstRate 18% from lookup
	        	
	        	String cgstAmount=decimalFormat.format(gst/2);
	        	String sgstAmount=decimalFormat.format(gst/2);
	        	
	        	String amount=decimalFormat.format(basicAmount);
	        	String amt_cgst_sgst=decimalFormat.format(basicAmount+gst);
				
		    	Document document = new Document(PageSize.A4);
		 		PdfWriter.getInstance(document, new FileOutputStream("D:/DELHI_HIGH_COURT_TFS/Tax_invoice/"+invoice+invoiceDetail.getInvoiceNo()+".pdf"));
		 	    document.open();
		 	    
		        Image image = Image.getInstance("classpath:resources/Letter-header.jpg");
		        image.scaleToFit(500, 100);
		        image.setSpacingAfter(10);
		        document.add(image);
		         
		        Rectangle rect= new Rectangle(570,710,25,50); // you can resize rectangle 
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        document.add(rect);
		        	
		        Paragraph center = new Paragraph(); 
		        center.setAlignment(Element.ALIGN_CENTER);
		        center.setSpacingBefore(10);
		        center.add(new Phrase("Tax Invoice",mediumFontBoldUnderLine));
		        document.add(center);
		        
		        PdfPTable pdfPTable5=new PdfPTable(2);
		        pdfPTable5.setWidthPercentage(100.0f);
		        pdfPTable5.setWidths(new float[] {50.0f,50.0f});
		        pdfPTable5.setSpacingBefore(10);
			    
			    PdfPTable pdfPTable3=new PdfPTable(2);
			    pdfPTable3.setWidthPercentage(100.0f);
			    pdfPTable3.setWidths(new float[] {50.0f,50.0f});
			    pdfPTable3.addCell(getCellWithoutBorder("Client Details", PdfPCell.ALIGN_LEFT, smallFontBoldUnderline));
			    pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable3.addCell(getCellWithoutBorder("Client name", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable3.addCell(getCellWithoutBorder(": "+invoiceDetail.getClientName(), PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable3.addCell(getCellWithoutBorder("State ", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable3.addCell(getCellWithoutBorder(": Delhi", PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable3.addCell(getCellWithoutBorder("Address ", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable3.addCell(getCellWithoutBorder(": Shershah Road, Justice SB Marg, New Delhi, Delhi 110503", PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable3.addCell(getCellWithoutBorder("GST/TIN ", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable5.addCell(pdfPTable3);
		        
		        PdfPTable pdfPTable4=new PdfPTable(2);
		        pdfPTable4.setWidthPercentage(100.0f);
		        pdfPTable4.setWidths(new float[] {50.0f,50.0f});
		        pdfPTable4.addCell(getCellWithoutBorder("Invoice No :", PdfPCell.ALIGN_LEFT, smallBoldFont));
			    pdfPTable4.addCell(getCellWithoutBorder(": "+invoice+invoiceDetail.getInvoiceNo(), PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable4.addCell(getCellWithoutBorder("Bill No :", PdfPCell.ALIGN_LEFT, smallBoldFont));
			    pdfPTable4.addCell(getCellWithoutBorder(": "+invoiceDetail.getBillNo(), PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable4.addCell(getCellWithoutBorder("Invoice Date", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable4.addCell(getCellWithoutBorder(": "+invoiceDetail.getCreatedOn(), PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable4.addCell(getCellWithoutBorder("Place of supply :", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable4.addCell(getCellWithoutBorder(": 7", PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable4.addCell(getCellWithoutBorder("Service :", PdfPCell.ALIGN_LEFT, smallBoldFont));
		        pdfPTable4.addCell(getCellWithoutBorder(": Digitization", PdfPCell.ALIGN_LEFT, smallFont));
		        pdfPTable5.addCell(pdfPTable4);
		        document.add(pdfPTable5);
		        
		        PdfPTable pdfPTable=new PdfPTable(8);
				pdfPTable.setWidthPercentage(100.0f);
				pdfPTable.setWidths(new float[] {8.0f,18.0f,15.0f,14.0f,10.0f,10.0f,15.0f,10.0f});
				pdfPTable.setSpacingBefore(10);
				pdfPTable.addCell(getCell("Sr. No.", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("Dairy No.", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("Particulars", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("Size/Services", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("Quantity", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("Rate", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("No of pages", PdfPCell.ALIGN_LEFT, smallBoldFont));
				pdfPTable.addCell(getCell("Amount", PdfPCell.ALIGN_LEFT, smallBoldFont));
				
				pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(freshCase.getDiaryNo(), PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("SCANNING", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("A4/Legal", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("1", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("0.49", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(String.valueOf(invoiceDetail.getNoOfPage()), PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(String.valueOf(amount), PdfPCell.ALIGN_LEFT, smallFont));
				
				pdfPTable.addCell(getCell("Total", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(amount, PdfPCell.ALIGN_LEFT, smallFont));
				
				pdfPTable.addCell(getCell("CGST-Delhi@9.0%", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(cgstAmount, PdfPCell.ALIGN_LEFT, smallFont));
				
				pdfPTable.addCell(getCell("SGST-Delhi@9.0%", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(sgstAmount, PdfPCell.ALIGN_LEFT, smallFont));
				
				pdfPTable.addCell(getCell("Total(Total+CGST+SGST)", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell(amt_cgst_sgst, PdfPCell.ALIGN_LEFT, smallFont));
				
				pdfPTable.addCell(getCell("Grand Total", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("\u20B9 "+decimalFormat1.format(invoiceDetail.getAmount()), PdfPCell.ALIGN_LEFT, smallFont));
				
				pdfPTable.addCell(getCell("RUPEES "+(NumberToWords.convertToIndianCurrency(String.valueOf(invoiceDetail.getAmount()))).toUpperCase(), PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(8);
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
				document.add(pdfPTable);
				
				PdfPTable pdfPTable7=new PdfPTable(1);
				pdfPTable7.setWidthPercentage(100.0f);
				pdfPTable7.setWidths(new float[] {100.0f});
				pdfPTable7.setSpacingBefore(10);
		        
			    PdfPTable pdfPTable2=new PdfPTable(3);
			    pdfPTable2.setWidthPercentage(100.0f);
			    pdfPTable2.setWidths(new float[] {30.0f,30.0f,40.0f});
			    pdfPTable2.addCell(getCellWithoutBorder("Kindly find the same in order and remit amount of Rs  \u2609 "+invoiceDetail.getAmount(), PdfPCell.ALIGN_LEFT, smallFont)).setColspan(3);;
			    pdfPTable2.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
			    pdfPTable2.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
			    pdfPTable2.addCell(getCellWithoutBorder("PAN No. : AAKCS1549P", PdfPCell.ALIGN_LEFT, smallBoldFont));
			    pdfPTable2.addCell(getCellWithoutBorder("TAN No. : MUMS53752G", PdfPCell.ALIGN_LEFT, smallBoldFont));
			    pdfPTable2.addCell(getCellWithoutBorder("GST/TIN No. : 07AAKCS1549P1ZP", PdfPCell.ALIGN_LEFT, smallBoldFont));
			    pdfPTable7.addCell(pdfPTable2);
			    document.add(pdfPTable7);
			    
			    document.add(new Paragraph(new Phrase("For StockHolding DMS", smallBoldFont)));
			    document.add(new Paragraph(new Phrase("Signatory", smallFont)));
			    document.add(new Paragraph(new Phrase("Note", smallBoldFont)));
			    List list = new List(List.UNORDERED);
			    list.setListSymbol("\u2022");
		        ListItem item = new ListItem(new Phrase("This is computer generated invoice", smallFont));
		        item.setAlignment(Element.ALIGN_JUSTIFIED);
		        list.add(item);
			    document.add(list);
			    Paragraph paragraph=new Paragraph(new Phrase("Plot No.P-51, T.T.C. Industrial Area, MIDC, Mahape, Navi Mumbai-400710 Telephone- 022-61778703/26", smallFont));
			    paragraph.setAlignment(Element.ALIGN_CENTER);
			    paragraph.setSpacingBefore(190);
			    document.add(paragraph);
		        document.close();
	        }
		}
		
	}
	
	public PdfPCell getCell(String text, int alignment, Font textFontSize) {
	    PdfPCell cell = new PdfPCell(new Phrase(text,textFontSize));
	    cell.setPaddingTop(10);
	    cell.setHorizontalAlignment(alignment);
	    return cell;
	}
	
	public PdfPCell getCellWithoutBorder(String text, int alignment, Font textFontSize) {
	    PdfPCell cell = new PdfPCell(new Phrase(text,textFontSize));
	    cell.setPaddingTop(10);
	    cell.setHorizontalAlignment(alignment);
	    cell.setBorder(Rectangle.NO_BORDER);
	    return cell;
	}

	

}
