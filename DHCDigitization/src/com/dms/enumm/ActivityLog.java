package com.dms.enumm;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.dms.model.User;
import com.dms.model.UserActivities;
import com.dms.service.UserActivitiesService;
import com.dms.utility.BrowserDetailUtility;

@Component
@PropertySource("classpath:resources/messages_en.properties")
public class ActivityLog {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	HttpServletRequest request;
	
	public void info(String loginfo) {
		HttpSession session=request.getSession();
		User user=(User)session.getAttribute("user");
		if(user!=null) {
			String activity=environment.getProperty(loginfo);
			UserActivities userActivities=new UserActivities();
			userActivities.setUserName(user.getUsername());
	    	userActivities.setActivity(activity);
	    	userActivities.setIpAdress(request.getRemoteHost());
	    	userActivities.setBrowserDetails(BrowserDetailUtility.getBrowser(request));
	    	userActivities.setCreatedOn(new Date());
	     	if(userActivities!=null) {
	     		userActivitiesService.saveUserActivities(userActivities);
	     	}
		}
	}

}
