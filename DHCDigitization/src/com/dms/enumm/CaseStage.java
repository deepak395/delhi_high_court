package com.dms.enumm;

public enum CaseStage {
	
	PENDINGSCAN("PENDING FOR SCAN"),
	READYTOMOVESCANNEDFILES("READY TO MOVE SCANNED FILES"),
	SCANNED("SCANNING DONE"),
	READYTOMOVEQCFILES("READY TO MOVE QC FILES"),
	READYTOMOVEBOOKMARKFILES("READY TO MOVE BOOKMARK FILES"),
	QC("QC DONE"),
	BOOKMARK("BOOKMARKING DONE");
	
	private String stage;
	 
	CaseStage(String stage){
		this.stage=stage;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}
	 
	 
	 
	
	
	

}
