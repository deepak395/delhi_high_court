package com.dms.utility;

import java.io.File;
import java.util.List;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PdfDocUtility {
	
	public static void splitPdf(String sourcePath, String destinationPath, String fileName,  int startPage, int endPage) {
		
		String fileSourcePath=sourcePath+File.separator+fileName;
		
		String fileDestinationPath=destinationPath+File.separator+fileName;
		
		try (PDDocument document = PDDocument.load(new File(fileSourcePath))) {

            // Instantiating Splitter class
            Splitter splitter = new Splitter();
          
            splitter.setStartPage(startPage);
            splitter.setEndPage(endPage);
            splitter.setSplitAtPage(endPage);

            // splitting the pages of a PDF document
            List<PDDocument> pages = splitter.split(document);
            if(pages!=null) {
            	for (PDDocument pdDocument : pages) {
 	        	   pdDocument.save(fileDestinationPath);
            	}
            }
            // Saving each page as an individual document
          /*  int i = 1;
            while (iterator.hasNext()) {
                PDDocument pd = iterator.next();
                pd.save(fileDestinationPath + i + ".pdf");
                i++;
            }*/

        } catch (Exception e){
            System.err.println("Exception while trying to read pdf document - " + e);
        }
		
	}
	
	public static int getPdfPageCount(String path, String fileName) throws Exception {
		String sourcePath=path+File.separator+fileName;
		
		PDDocument doc = PDDocument.load(new File(sourcePath));
		try{
		int count = doc.getNumberOfPages();
		return count;
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			doc.close();
		}
		return 0;
	}

}
