package com.dms.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.pdfbox.io.IOUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;



public class FileUtility {
	
	public static boolean uploadFileToServer(MultipartFile multipartFile, String fileName, String path) {
			
		 if (null != multipartFile){
	            File imageFile = new File(path, fileName);
	            try{
	                multipartFile.transferTo(imageFile);
	            } catch (IOException e){
	                e.printStackTrace();
	            }
	        }
			return true;
		}
	
	public static boolean copyFileToServer(String sourcePath, String destinationPath, String fileName) throws Exception {
		
		String fileSourcePath=sourcePath + File.separator + fileName; 
		File file = new File(fileSourcePath);
	    FileInputStream input = new FileInputStream(file);
	    MultipartFile multipartFile = new MockMultipartFile("file",file.getName(), "text/plain", IOUtils.toByteArray(input));
		if (null != multipartFile){
            File imageFile = new File(destinationPath, fileName);
            try{
                multipartFile.transferTo(imageFile);
            } catch (IOException e){
                e.printStackTrace();
            }
	     }
		
	/*	byte fileBarr[] = convertFileData(fileSourcePath);
		File serverFile = new File(fileDestinationPath);
		FileOutputStream outputStream = null;
		BufferedOutputStream bout = null;
		try {
			outputStream = new FileOutputStream(serverFile);
			bout = new BufferedOutputStream(outputStream);
			bout.write(fileBarr);
			bout.flush();
			bout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return true;
	}
	
	public static String getFileExtension(String fileName) {
		
		if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
			return fileName.substring(fileName.lastIndexOf(".")+1);
		}
		return "";
	}
	
	public static boolean createFolder(String folderPath, String folderName) {

		boolean flag = false;
		File newFolder = new File(folderPath + File.separator + folderName);
		if (!newFolder.exists()) {
			flag = newFolder.mkdir();
		}
		return flag;
	}
	
	public static byte[] convertFileData(String filePath) throws IOException {
		// create file object
		File file = new File(filePath);
		// initialize a byte array of size of the file
		byte[] fileContent = new byte[(int)file.length()];
		FileInputStream inputStream = null;
		try {
			// create an input stream pointing to the file
			inputStream = new FileInputStream(file);
			// read the contents of file into byte array
			inputStream.read(fileContent);
		} catch (IOException e) {
			throw new IOException("Unable to convert file to byte array. " + e.getMessage());
		} finally {
			// close input stream
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return fileContent;
	}
	

	

	

}
