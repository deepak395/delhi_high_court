package com.dms.utility;

import java.util.Calendar;

public class FinancialYearCalculator {
	
	public static String getCurrentFinancialYear() {
		
		String finYear=null;
		int year = Calendar.getInstance().get(Calendar.YEAR);
	    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
	    
	    if (month <= 3) {
	    	finYear=((year - 1) + "-" + year);
	    } 
	    else {
	    	finYear= year + "-" + (year + 1);
	    }
		return finYear;
	}
}
