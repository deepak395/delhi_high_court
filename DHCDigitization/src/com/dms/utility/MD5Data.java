package com.dms.utility;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Data {
	
	public static String getMD5(String password){
	
		MessageDigest m;
		try {
			m = MessageDigest.getInstance("MD5");
			m.update(password.getBytes(),0,password.length());
		    String encPassword = new BigInteger(1,m.digest()).toString(16);
		    if(encPassword!=null) {
		    	return encPassword;
		    }
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

}
