package com.dms.utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dms.model.Folder;
import com.dms.model.RepositoryDetail;
import com.dms.service.FolderService;
import com.dms.service.FreshCaseService;
import com.dms.service.PendingCaseService;
import com.dms.service.RepositoryDetailService;

@Component
public class FolderPathUtility {
	
	@Autowired
	RepositoryDetailService repositoryDetailService;
	
	@Autowired
	FolderService folderService;
	
	@Autowired
	PendingCaseService pendingCaseService;
	
	@Autowired
	FreshCaseService freshCaseService;
	
	
	public String getFreshCaseFolderPath(Long folderId) {
		
		Long parentId=null;
		String repoPath="";
		String dayPath="";
		RepositoryDetail repositoryDetail=null;
		Folder subfolder=null;
		
		Date date=new Date();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat monthdf=new SimpleDateFormat("MMMM");
		SimpleDateFormat yaerdf=new SimpleDateFormat("YYYY");
		String currentDate=df.format(date);
		String currentMonth=monthdf.format(date);
		String currentYear=yaerdf.format(date);
		
		Folder folder=folderService.getFolderByFolderId(folderId);
		if(folder!=null) {
			String folderPath="";
			repositoryDetail=repositoryDetailService.getRepositoryDetailByRepositoryId(folder.getRepositoryId());
			if(repositoryDetail!=null) {
				repoPath=repositoryDetail.getRepositoryPath();
			}
			parentId=folder.getParentFolderId();
			while(parentId>0) {
				subfolder=folderService.getFolderByFolderId(parentId);
				if(subfolder!=null) {
					folderPath=subfolder.getFolderName()+File.separator+folderPath;
					parentId=subfolder.getParentFolderId();
				}
			}
			String destinationPath=repoPath+File.separator+folderPath+folder.getFolderName();
			String yearPath=destinationPath+File.separator+currentYear;
			String monthPath=yearPath+File.separator+currentMonth;
			dayPath=monthPath+File.separator+currentDate;
			if(!new File(yearPath).exists()) {
				FileUtility.createFolder(destinationPath, currentYear);
			}
			if(!new File(monthPath).exists()) {
				FileUtility.createFolder(yearPath, currentMonth);
			}
			if(!new File(dayPath).exists()) {
				FileUtility.createFolder(monthPath, currentDate);
			}
		}
		return dayPath;
		
	}
	
	
	public String getFreshCaseDownloadFolderPath(Date date, Long folderId) {
		
		Long parentId=null;
		String repoPath="";
		String dayPath="";
		RepositoryDetail repositoryDetail=null;
		Folder subfolder=null;
		
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat monthdf=new SimpleDateFormat("MMMM");
		SimpleDateFormat yaerdf=new SimpleDateFormat("YYYY");
		String currentDate=df.format(date);
		String currentMonth=monthdf.format(date);
		String currentYear=yaerdf.format(date);
		Folder folder=folderService.getFolderByFolderId(folderId);
		if(folder!=null) {
			String folderPath="";
			repositoryDetail=repositoryDetailService.getRepositoryDetailByRepositoryId(folder.getRepositoryId());
			if(repositoryDetail!=null) {
				repoPath=repositoryDetail.getRepositoryPath();
			}
			parentId=folder.getParentFolderId();
			while(parentId>0) {
				subfolder=folderService.getFolderByFolderId(parentId);
				if(subfolder!=null) {
					folderPath=subfolder.getFolderName()+File.separator+folderPath;
					parentId=subfolder.getParentFolderId();
				}
			}
			String destinationPath=repoPath+File.separator+folderPath+folder.getFolderName();
			String yearPath=destinationPath+File.separator+currentYear;
			String monthPath=yearPath+File.separator+currentMonth;
			dayPath=monthPath+File.separator+currentDate;
		}
		return dayPath;
		
	}
	
public String getPendingCaseFolderPath(Long folderId, String caseNo, String caseYear, String caseCategory) {
		
	Long parentId=null;
	RepositoryDetail repositoryDetail=null;
	Folder subfolder=null;
	String repoPath="";
	String casenoPath="";
	String folderPath="";
	
	Date date=new Date();
	SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat monthdf=new SimpleDateFormat("MMMM");
	SimpleDateFormat yaerdf=new SimpleDateFormat("YYYY");
	String currentDate=df.format(date);
	String currentMonth=monthdf.format(date);
	String currentYear=yaerdf.format(date);
	String casenoFolderName=caseCategory+"_"+caseNo+"_"+caseYear;
	Folder folder=folderService.getFolderByFolderId(folderId);
	if(folder!=null) {
		repositoryDetail=repositoryDetailService.getRepositoryDetailByRepositoryId(folder.getRepositoryId());
		if(repositoryDetail!=null) {
			repoPath=repositoryDetail.getRepositoryPath();
		}
		parentId=folder.getParentFolderId();
		while(parentId>0) {
			subfolder=folderService.getFolderByFolderId(parentId);
			if(subfolder!=null) {
				folderPath=subfolder.getFolderName()+File.separator+folderPath;
				parentId=subfolder.getParentFolderId();
			}
		}
		String destinationPath=repoPath+File.separator+folderPath+folder.getFolderName();
		String yearPath=destinationPath+File.separator+currentYear;
		String monthPath=yearPath+File.separator+currentMonth;
		String dayPath=monthPath+File.separator+currentDate;
		casenoPath=dayPath+File.separator+casenoFolderName;
		if(!new File(yearPath).exists()) {
			FileUtility.createFolder(destinationPath, currentYear);
		}
		if(!new File(monthPath).exists()) {
			FileUtility.createFolder(yearPath, currentMonth);
		}
		if(!new File(dayPath).exists()) {
			FileUtility.createFolder(monthPath, currentDate);
		}
		if(!new File(casenoPath).exists()) {
			FileUtility.createFolder(dayPath, casenoFolderName);
		}
			
		}
		return casenoPath;
		
	}

public String getPendingCaseDownloadFolderPath(Date date, Long folderId, String caseNo, String caseYear, String caseCategory) {
	
	Long parentId=null;
	RepositoryDetail repositoryDetail=null;
	Folder subfolder=null;
	String repoPath="";
	String casenoPath="";
	String folderPath="";
	
	SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat monthdf=new SimpleDateFormat("MMMM");
	SimpleDateFormat yaerdf=new SimpleDateFormat("YYYY");
	String currentDate=df.format(date);
	String currentMonth=monthdf.format(date);
	String currentYear=yaerdf.format(date);
	String casenoFolderName=caseCategory+"_"+caseNo+"_"+caseYear;
	Folder folder=folderService.getFolderByFolderId(folderId);
	if(folder!=null) {
		repositoryDetail=repositoryDetailService.getRepositoryDetailByRepositoryId(folder.getRepositoryId());
		if(repositoryDetail!=null) {
			repoPath=repositoryDetail.getRepositoryPath();
		}
		parentId=folder.getParentFolderId();
		while(parentId>0) {
			subfolder=folderService.getFolderByFolderId(parentId);
			if(subfolder!=null) {
				folderPath=subfolder.getFolderName()+File.separator+folderPath;
				parentId=subfolder.getParentFolderId();
			}
		}
		String destinationPath=repoPath+File.separator+folderPath+folder.getFolderName();
		String yearPath=destinationPath+File.separator+currentYear;
		String monthPath=yearPath+File.separator+currentMonth;
		String dayPath=monthPath+File.separator+currentDate;
		casenoPath=dayPath+File.separator+casenoFolderName;
		}
		return casenoPath;
		
	}

}
