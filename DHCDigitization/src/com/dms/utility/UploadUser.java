package com.dms.utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.dms.model.User;


public class UploadUser {
	
	public static List<User> readUserFile(String fileName, String filePath) throws Exception {
		
		Cell cell=null;
        User user=null;
		String path=filePath + File.separator + fileName; 
        List<User> userList=new ArrayList<User>();
        FileInputStream inputStream = new FileInputStream(new File(path));
        Iterator<Row> rowIterator =null;
        Workbook workbook = WorkbookFactory.create(inputStream); 
    	Sheet sheet = workbook.getSheetAt(0);
    	rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
        	  user=new User();
        	  Row row = rowIterator.next();
              if(row.getRowNum()>0){
              	cell=row.getCell(0);
              	if(cell!=null) {
              		cell.setCellType(CellType.STRING);
	              	String userFullName=cell.getStringCellValue();
	              	user.setUserFullName(userFullName);
              	}
              	cell=row.getCell(1);
             	if(cell!=null) {
             		cell.setCellType(CellType.STRING);
	              	String username=cell.getStringCellValue();
	              	user.setUsername(username);
             	}
              	cell=row.getCell(2);
             	if(cell!=null) {
             		cell.setCellType(CellType.STRING);
	              	String password=cell.getStringCellValue();
	              	user.setPassword(password);
             	}
              	cell=row.getCell(3);
             	if(cell!=null) {
             		cell.setCellType(CellType.STRING);
	              	String roleName=cell.getStringCellValue();
	              	user.setRoleName(roleName);
             	}
             	userList.add(user);
              }
        }
        if(userList.size()>0) {
        	return userList;
        }
        workbook.close();
        inputStream.close();
		return null;
		
	}

}
