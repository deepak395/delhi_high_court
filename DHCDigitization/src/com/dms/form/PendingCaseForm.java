package com.dms.form;

import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

public class PendingCaseForm {
	
	private PendingCase pendingCase;
	private PendingCaseDocument pendingCaseDocument;
	
	public PendingCase getPendingCase() {
		return pendingCase;
	}
	public void setPendingCase(PendingCase pendingCase) {
		this.pendingCase = pendingCase;
	}
	public PendingCaseDocument getPendingCaseDocument() {
		return pendingCaseDocument;
	}
	public void setPendingCaseDocument(PendingCaseDocument pendingCaseDocument) {
		this.pendingCaseDocument = pendingCaseDocument;
	}
	
	@Override
	public String toString() {
		return "PendingCaseForm [pendingCase=" + pendingCase + ", pendingCaseDocument=" + pendingCaseDocument + "]";
	}
	
}
