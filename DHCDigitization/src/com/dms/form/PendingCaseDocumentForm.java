package com.dms.form;

import java.util.List;

import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

public class PendingCaseDocumentForm {
	
	private PendingCase pendingCase;
	private List<PendingCaseDocument> pendingCaseDocumentList;
	
	public PendingCase getPendingCase() {
		return pendingCase;
	}
	public void setPendingCase(PendingCase pendingCase) {
		this.pendingCase = pendingCase;
	}
	public List<PendingCaseDocument> getPendingCaseDocumentList() {
		return pendingCaseDocumentList;
	}
	public void setPendingCaseDocumentList(List<PendingCaseDocument> pendingCaseDocumentList) {
		this.pendingCaseDocumentList = pendingCaseDocumentList;
	}
	@Override
	public String toString() {
		return "PendingCaseDocumentForm [pendingCase=" + pendingCase + "]";
	}
	
	
	
}
