package com.dms.form;

import com.dms.model.FreshCase;
import com.dms.model.InvoiceDetail;

public class FreshCaseForm {
	
	private InvoiceDetail invoiceDetail;
	private FreshCase freshCase;
	
	public InvoiceDetail getInvoiceDetail() {
		return invoiceDetail;
	}
	public void setInvoiceDetail(InvoiceDetail invoiceDetail) {
		this.invoiceDetail = invoiceDetail;
	}
	public FreshCase getFreshCase() {
		return freshCase;
	}
	public void setFreshCase(FreshCase freshCase) {
		this.freshCase = freshCase;
	}
	
	@Override
	public String toString() {
		return "FreshCaseForm [invoiceDetail=" + invoiceDetail + ", freshCase=" + freshCase + "]";
	}
	
	
	
	

}
