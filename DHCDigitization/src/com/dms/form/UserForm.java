package com.dms.form;

import com.dms.model.User;
import com.dms.model.UserRole;

public class UserForm {
	
	private UserRole userRole;
	private User user;
	
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return "UserForm [userRole=" + userRole + ", user=" + user + "]";
	}
	
	

}
