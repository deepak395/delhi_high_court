package com.dms.form;

import java.io.Serializable;
import java.util.List;

import com.dms.model.Menu;
import com.dms.model.Submenu;

public class MenuForm implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Menu menu;
	private List<Submenu> submenuList;
	
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public List<Submenu> getSubmenuList() {
		return submenuList;
	}
	public void setSubmenuList(List<Submenu> submenuList) {
		this.submenuList = submenuList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return "MenuForm [menu=" + menu + ", submenuList=" + submenuList + "]";
	}
	
	
}
