package com.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class PdfDemo {
	

	public static void main(String[] args) throws DocumentException, MalformedURLException, IOException {
		
		Font smallFont = FontFactory.getFont(FontFactory.HELVETICA);
	    smallFont.setSize(10);
	    
	    Font smallBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
	    smallBoldFont.setSize(10);
	    
	    Font smallFontBoldUnderline = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
	    smallFontBoldUnderline.setStyle(Font.UNDERLINE);
	    smallFontBoldUnderline.setSize(9);
	    
	    Font mediumFontBoldUnderLine = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
	    mediumFontBoldUnderLine.setStyle(Font.UNDERLINE);
	    mediumFontBoldUnderLine.setSize(12);
	    
	    Font mediaumFont = FontFactory.getFont(FontFactory.HELVETICA);
	    mediaumFont.setSize(12);
	    
	    Font mediaumBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
	    mediaumBoldFont.setSize(12);
		
		Document document = new Document(PageSize.A4);
		
        PdfWriter.getInstance(document, new FileOutputStream("D:/result.pdf"));
        document.open();
        
        Image image = Image.getInstance(ClassLoader.getSystemResource("resources/Letter-header.jpg"));
        image.scaleToFit(500, 100);
        image.setSpacingAfter(10);
        document.add(image);
        
        Rectangle rect= new Rectangle(570,710,25,50); // you can resize rectangle 
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(1);
        document.add(rect);
     
        Paragraph center = new Paragraph(); 
        center.setAlignment(Element.ALIGN_CENTER);
        center.setSpacingBefore(10);
        center.add(new Phrase("Tax Invoice",mediumFontBoldUnderLine));
        document.add(center);
        
        PdfPTable pdfPTable5=new PdfPTable(2);
        pdfPTable5.setWidthPercentage(100.0f);
        pdfPTable5.setWidths(new float[] {50.0f,50.0f});
        pdfPTable5.setSpacingBefore(10);
	    
	    PdfPTable pdfPTable3=new PdfPTable(2);
	    pdfPTable3.setWidthPercentage(100.0f);
	    pdfPTable3.setWidths(new float[] {50.0f,50.0f});
	    pdfPTable3.addCell(getCellWithoutBorder("Client Details", PdfPCell.ALIGN_LEFT, smallFontBoldUnderline));
	    pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable3.addCell(getCellWithoutBorder("File name", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable3.addCell(getCellWithoutBorder("Phone No. :", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable3.addCell(getCellWithoutBorder("State :", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable3.addCell(getCellWithoutBorder("Address :", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable3.addCell(getCellWithoutBorder("GST/TIN :", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable3.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable5.addCell(pdfPTable3);
        
        PdfPTable pdfPTable4=new PdfPTable(2);
        pdfPTable4.setWidthPercentage(100.0f);
        pdfPTable4.setWidths(new float[] {50.0f,50.0f});
        pdfPTable4.addCell(getCellWithoutBorder("Invoice No :", PdfPCell.ALIGN_LEFT, smallBoldFont));
	    pdfPTable4.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable4.addCell(getCellWithoutBorder("Invoice Date", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable4.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable4.addCell(getCellWithoutBorder("Place of supply :", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable4.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable4.addCell(getCellWithoutBorder("Service :", PdfPCell.ALIGN_LEFT, smallBoldFont));
        pdfPTable4.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
        pdfPTable5.addCell(pdfPTable4);
        document.add(pdfPTable5);
        
        PdfPTable pdfPTable=new PdfPTable(8);
		pdfPTable.setWidthPercentage(100.0f);
		pdfPTable.setWidths(new float[] {10.0f,20.0f,15.0f,10.0f,10.0f,10.0f,15.0f,10.0f});
		pdfPTable.setSpacingBefore(10);
		pdfPTable.addCell(getCell("Sr. No.", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("File name", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("Particulars", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("Size/Services", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("Quantity", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("Rate", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("No of pages", PdfPCell.ALIGN_LEFT, smallBoldFont));
		pdfPTable.addCell(getCell("Amount", PdfPCell.ALIGN_LEFT, smallBoldFont));
		
		pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("ABCD", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("SCANNING", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("100MB", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("5", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("100", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("21", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("456", PdfPCell.ALIGN_LEFT, smallFont));
		
		
		pdfPTable.addCell(getCell("Total", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
		
		pdfPTable.addCell(getCell("CGST-Delhi@9.0%", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
		
		pdfPTable.addCell(getCell("SGST-Delhi@9.0%", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
		
		pdfPTable.addCell(getCell("Total(Total+CGST+SGST)", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
		
		pdfPTable.addCell(getCell("Grand Total", PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(7);
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("1.", PdfPCell.ALIGN_LEFT, smallFont));
		
		pdfPTable.addCell(getCell("RUPEES "+NumberToWords.convertToIndianCurrency("5465"), PdfPCell.ALIGN_LEFT, smallBoldFont)).setColspan(8);
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		pdfPTable.addCell(getCell("", PdfPCell.ALIGN_LEFT, smallFont));
		document.add(pdfPTable);
		
		PdfPTable pdfPTable7=new PdfPTable(1);
		pdfPTable7.setWidthPercentage(100.0f);
		pdfPTable7.setWidths(new float[] {100.0f});
		pdfPTable7.setSpacingBefore(10);
        
	    PdfPTable pdfPTable2=new PdfPTable(3);
	    pdfPTable2.setWidthPercentage(100.0f);
	    pdfPTable2.setWidths(new float[] {30.0f,30.0f,40.0f});
	    pdfPTable2.addCell(getCellWithoutBorder("Kindly find the same in order and remit amount of", PdfPCell.ALIGN_LEFT, smallFont)).setColspan(3);;
	    pdfPTable2.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
	    pdfPTable2.addCell(getCellWithoutBorder("", PdfPCell.ALIGN_LEFT, smallFont));
	    pdfPTable2.addCell(getCellWithoutBorder("PAN No. :", PdfPCell.ALIGN_LEFT, smallBoldFont));
	    pdfPTable2.addCell(getCellWithoutBorder("TAN No. :", PdfPCell.ALIGN_LEFT, smallBoldFont));
	    pdfPTable2.addCell(getCellWithoutBorder("GST/TIN No. :", PdfPCell.ALIGN_LEFT, smallBoldFont));
	    pdfPTable7.addCell(pdfPTable2);
	    document.add(pdfPTable7);
	    
	    document.add(new Paragraph(new Phrase("For StockHolding DMS", smallBoldFont)));
	    document.add(new Paragraph(new Phrase("Signatory", smallFont)));
	    document.add(new Paragraph(new Phrase("Note", smallBoldFont)));
	    List list = new List(List.UNORDERED);
	    list.setListSymbol("\u2022");
        ListItem item = new ListItem(new Phrase("This is computer generated invoice", smallFont));
        item.setAlignment(Element.ALIGN_JUSTIFIED);
        list.add(item);
	    document.add(list);
	    Paragraph paragraph=new Paragraph(new Phrase("Plot No.P-51, T.T.C. Industrial Area, MIDC, Mahape, NAvi Mumbai-400710 Telephone- 022-61778703/26", smallFont));
	    paragraph.setAlignment(Element.ALIGN_CENTER);
	    paragraph.setSpacingBefore(170);
	    document.add(paragraph);
        document.close();

	}
	
	public static PdfPCell getCell(String text, int alignment, Font textFontSize) {
	    PdfPCell cell = new PdfPCell(new Phrase(text,textFontSize));
	    cell.setPaddingTop(10);
	    cell.setHorizontalAlignment(alignment);
	    return cell;
	}
	
	public static PdfPCell getCellWithoutBorder(String text, int alignment, Font textFontSize) {
	    PdfPCell cell = new PdfPCell(new Phrase(text,textFontSize));
	    cell.setPaddingTop(10);
	    cell.setHorizontalAlignment(alignment);
	    cell.setBorder(Rectangle.NO_BORDER);
	    return cell;
	}
	
	public static PdfPCell getTopCell(String text, int alignment, Font textFontSize) {
	    PdfPCell cell = new PdfPCell(new Phrase(text,textFontSize));
	    cell.setPaddingTop(5);
	    cell.setHorizontalAlignment(alignment);
	    return cell;
	}

}
