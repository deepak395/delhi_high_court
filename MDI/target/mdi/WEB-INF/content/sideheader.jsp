<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.mdi.model.User"%> 
<%@ page import="com.mdi.model.ObjectMaster"%>
<%@page import="com.mdi.model.UserRole"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script>
function goBack() {
    window.history.back();
}
</script>
<body>
<% 
User user = null;
if(session.getAttribute("USER")!=null)
user = (User)session.getAttribute("USER");
List<UserRole> userroles=user.getUserroles();
Boolean dataentryop=false;
UserRole userr=null;
for(UserRole userrole:userroles){
userr=userrole;
}

%> 


<div class="page-container sidebar-collapsed">
<!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
<div class="sidebar-menu">
<div class="sidebar-menu-inner">
<header class="logo-env">
<!-- logo -->
<div class="logo">
<a href="/Home/Index">
<!--<img src="~/Images/Dms-Logo.png" alt="" style="height: 60px;width: 130px;" />-->
<strong style="font-size: xx-large;color: aliceblue;">EDMS</strong>
</a>
</div>
<!-- logo collapse icon -->
<div class="sidebar-collapse">
<a class="sidebar-collapse-icon">
<!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
<i class="entypo-menu"></i>
</a>
</div>
<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
<div class="sidebar-mobile-menu visible-xs">
<a class="with-animation">
<!-- add class "with-animation" to support animation -->
<i class="entypo-menu"></i>
</a>
</div>
</header>
<!--<div class="sidebar-user-info">
<div class="sui-normal">
<a class="user-link">
<span style="color: #fad839;">
Welcome,
<br />
@FullName,
<br /> @RoleName
</span>
</a>
</div>
</div>-->


<ul id="main-menu" class="main-menu">
<!-- add class "multiple-expanded" to allow multiple submenus to open -->
<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
<%if(userr.getLk().getLk_longname().equals("System Admin")) {%>

<li>
<a id="Reports" >
<i class="entypo-doc-text"></i>
<span class="title">Reports</span>
</a>
<ul>
<li>
<a href="${pageContext.request.contextPath}/reports/LoginReport">
<span class="title">LoginReport </span>
</a>
</li>
<li>
<a href="${pageContext.request.contextPath}/reports/DownloadReport">
<span class="title">DownloadReport </span>
</a>
</li>
<li>
<a href="${pageContext.request.contextPath}//reports/UploadedDocumentRpt">
<span class="title">Upload File Report</span>
</a>
</li>
<li>
<a href="${pageContext.request.contextPath}//reports/MetaDataReport">
<span class="title">Meta Data Report</span>
</a>
</li>
<li>
<a href="${pageContext.request.contextPath}//reports/QCReport">
<span class="title">QC Report</span>
</a>
</li>
<li>
<a href="${pageContext.request.contextPath}//reports/StatusReport">
<span class="title">Status Report</span>
</a>
</li>
<li>
<a href="${pageContext.request.contextPath}//reports/DepartmentWiseMetaDataReport">
<span class="title">Department Wise Meta Data Report</span>
</a>
</li>
</ul>
</li>
<% } %>

<% 
List<ObjectMaster> parent_list = (List<ObjectMaster>)session.getAttribute("ob_list");
List<ObjectMaster> child_list = (List<ObjectMaster>)session.getAttribute("ob_list");

System.out.println(parent_list);
for(int i=0;i<parent_list.size();i++)
{
ObjectMaster om = parent_list.get(i);
// System.out.println(om.getOm_object_link()+" "+om.getOm_object_stages());
if(om.getOm_object_stages()==0)
{
%>
<li> 
<% if(om.getOm_object_name().equals("Dashboard")) {%>

<a id="ADashboard" href="<%=om.getOm_object_link() %>" > <i class="entypo-gauge"></i> <span class="title"><%=om.getOm_object_name() %></span></a>

<% } else if(om.getOm_object_name().equals("Administration")){ %>


<a id="Admin" href="<%=om.getOm_object_link() %>"> <i class="entypo-cog"></i> <span class="title"><%=om.getOm_object_name() %></span></a>

<% } else if(om.getOm_object_name().equals("Manage")) {%>


<a id="manage" href="<%=om.getOm_object_link() %>"><i class="entypo-tools"></i><span class="title"> <%=om.getOm_object_name() %></span></a>

<%-- <% }else if(om.getOm_object_name().equals("Report")){ %> 


<a id="report" href="<%=om.getOm_object_link() %>"><i class="entypo-doc-text"></i><span class="title"> <%=om.getOm_object_name() %></span></a>

--%>
<% }else if(om.getOm_object_name().equals("Search")){ %>


<a id="search" href="<%=om.getOm_object_link() %>"><i class="entypo-search"></i><span class="title"> <%=om.getOm_object_name() %> </span></a>

<% } %>

<ul> <% 
for(int j=0;j<child_list.size();j++)
{

ObjectMaster ch_om = child_list.get(j);

if(om.getOm_id()==ch_om.getOm_parent_id())
{
//add child
%>
<li><a href="${pageContext.request.contextPath}/<%=ch_om.getOm_object_link() %>"> <%=ch_om.getOm_object_name() %> </a></li>
<%
}
}
%> </ul> </li> <%
}
}
%>


</ul>
</div>
</div>
</body>
</html>