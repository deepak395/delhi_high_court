<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/DepartmentUploadedReportController.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/jspdf.js"></script>
	
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/tableExport.js"></script>
		
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/sprintf.js"></script>
		
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/base64.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-csv.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/angular-sanitize.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/dirPagination.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-csv.js"></script>


<%@include file="../../content/header.jsp" %>
</head>
<body ng-controller="DepartmentUploadedReportCtl">
<%@include file="../../content/sideheader.jsp" %>
			 <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context }/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <!-- Profile Info -->
                        <li>Welcome : Admin</li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           	<div class="row">
                <div class="col-md-12">
                <div class="panel panel-primary" style="border-color: #c2c9dc;">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Document Uploaded Report</div>
            
                       <!--  <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
                    </div>
                    <div class="panel-body">
                    <form id="documentupload" enctype="multipart/form-data">
                        <div class="row">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-2">
                                    <label id="metadata" name="metadata">Select Department</label>
                                </div>
                                <div class="col-md-2">
                 
			   
                                 
                                     <select id="dept_id"  class="form-control" name="dept_id"  ng-model="dept_id" >
                                        <option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>
                                     </select>
                           
                                </div>
                                <div class="col-md-2">
                                    <label id="folder" name="folder">From Date</label>
                                </div>
                                <div class="col-md-2">
                                   <div class="input-group date">
													<input type="text" class="form-control" datepicker-popup="{{format}}" name="fromDate" ng-model="model.fromDate" required is-open="fromDate" max-date="maxDate"  datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" show-button-bar="false" />
				            						<span class="input-group-addon" ng-click="open($event,'fromDate')"><i class="glyphicon glyphicon-calendar"></i></span>											</div>
								   </div>
                                </div>
                                <div class="col-md-2">
                                    <label id="folder" name="folder">To Date</label>
                                </div>
                                <div class="col-md-2">
                                  <div class="input-group date">
												<input type="text" class="form-control" datepicker-popup="{{format}}" name="toDate" ng-model="model.toDate" required is-open="toDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" show-button-bar="false" />
				            					<span class="input-group-addon" ng-click="open($event,'toDate')"><i class="glyphicon glyphicon-calendar"></i></span>											</div>
								  </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-4" style="text-align:right;">
                                	<input type="submit" id="btnsubmit" data-toggle="modal"  ng-disabled="buttonDisabled" value="SUBMIT" class="btn btn-primary btn-blue"  ng-click="getUploadedData()"/>
                                  </div> 
                                <div class="col-md-4">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" onclick="goBack()"/>
                                </div>
                                 <div class="col-md-4">
                                <button type="button" id="btnback" class="btn btn-primary btn-blue" style="text-align: center;"
												name="ExportToCsv1" ng-csv="dailyreportListData"
											 	filename="UploadReport.csv" data-toggle="modal">
												<i class="fa fa-edit"></i> Excel
								</button> 
								</div>
                            </div>
                        </div>
            		</form>
                    </div>
                     <table id="myTable1" class="table table-bordered table-responsive" st-table="displayedCollection"
							st-safe-src="UploadedList"
							class="table table-striped table-bordered nowrap table-hover" width="100%">
                                    <thead>
                                        <tr>
                                        	<th width:12%">SN</th>
                                            <th>
                                                Document Name
                                            </th>
                                            <th>
                                                Department
                                            </th>
                                            <th style="text-align:center;">
                                                View
                                            </th>
            
                                        </tr>
                                      
                                    </thead>
                                    <tbody>
                                            <tr id="dfd" ng-repeat="dfd in displayedCollection" class="odd gradeX">
                                            	<td align="center">{{$index+1}}</td>
                                                <td ng-model="dfd.name">
                                                    {{dfd.dfd_name}}
                                                </td>
                                                <td ng-model="dfd.setname">
                                                    {{dfd.departments.dept_name}}->{{dfd.subdepartments.sub_dept_name}}
                                                </td>
                                                 <td style="text-align:center;">
                                                    <input type="submit" id="btnsubmit" value="View" data-toggle="modal" data-target="#user_Modal"  class="btn btn-primary btn-blue" ng-click="ViewFile(dfd.dfd_id)"/>
                                                </td>
                                            </tr>
                                     </tbody>
                                     <tfoot>
									<tr>
										<td colspan="6" class="text-center">
											<div st-pagination="" st-items-by-page="5"
											st-displayed-pages="8">
											</div>
										</td>
									</tr>
								</tfoot>
                                </table>
                   <!--  <table id="data_table" st-table="logindata" st-safe-src="modelData" 
							class="table table-striped table-bordered nowrap table-hover" style="text-align: center;">
					<thead >	
							<tr>
								<th style="text-align: center; width:12%">SN</th>
								<th style="text-align: center;">Department</th>
								<th style="text-align: center;">Uploaded File Count</th>								
		
							</tr>
					</thead>
							<tbody>
								<tr  class="odd gradeX" ng-repeat="data in UploadedList" >
								 	<td align="center">{{$index+1}}</td>
								 	<td  >{{data[0]}}</td>
								 	<td  align="center"><a ng-click="getFiles(data[0])" data-toggle="modal" data-target="#CaseFile_Modal">
									<span aria-hidden="true"></span>{{data[1]}}</a></td>
								</tr>
				   </table> -->
                </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>
    
    <!-- view File  -->
				<!-- <div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true">

					<div class="modal-dialog modal-lg">
						<div class="modal-content" style="width: 606px;">
							<div class="modal-header"
								style="background-color: black; width: 881px;">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel" align="center">
									<strong style="color: #FBFCFD;">{{viewDfdData.dfd_name}}</strong>
								</h4>
							</div>


							<div class="modal-header" style="width: 881px;">

								<object type="application/pdf"
									data="{{fileurl}}#toolbar=0&navpanes=0&scrollbar=0"
									width="100%" height="600"> </object>
							</div>

						</div>
					</div>
				</div> -->
</body>
</html>			 