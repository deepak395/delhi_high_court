<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MDI</title>

<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;  
}

#customers tr:nth-child(even){background-color: #ffffe6;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #03577a;
  color: white;
}
</style>

<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/DepartmentWiseMetaDataReport.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/jspdf.js"></script>
	
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/tableExport.js"></script>
		
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/sprintf.js"></script>
		
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/base64.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-csv.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/angular-sanitize.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/dirPagination.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>


<%@include file="../../content/header.jsp" %>
</head>
<body ng-controller="DepartmentWiseMetaDataReportCtl">
<%@include file="../../content/sideheader.jsp" %>
			 <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context }/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <!-- Profile Info -->
                        <li>Welcome : Admin</li>
                    </ul>
                </div>
            </div>
            <hr />
           	<div class="row">
                <div class="col-md-12">
                <div class="panel panel-primary" style="border-color: #c2c9dc;">
                    <marquee width="100%" behavior="scroll" direction="right" scrollamount="12" height="100px" class="panel-heading" style="background-color: #03577a;">
                        <div class="panel-title" style="font-weight:bold; font-size:200%;">Department Wise Meta Data Report</div>
                    </marquee>
                    <div class="panel-body">
                        <div class="row">
                        	<div class="form-group" style="margin:1px 1px;font-size:14px;color:#808080;">
                        	
                        	<div class="col-md-2">
                                    <label id="metadata" name="metadata">Select Department</label>
                               <!--  </div>
                                <div class="col-md-2"> -->
                                   <select id="dept_id" class="form-control" name="dept_id" ng-model="document.dfd_dept_mid" ng-change="getSubDept(document.dfd_dept_mid)">
							<option value="" selected="selected">Select Department</option>
							<option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>
							</select>
                                </div>
								
								<div class="col-md-2">
                                    <label id="metadata" name="metadata">Select Sub Department</label>
                                <!-- </div>
								<div class="col-md-2"> -->
                                     <select id="sub_dept_id" class="form-control" name="sub_dept_id" ng-model="document.dfd_sub_dept_mid" >
								<option value="" selected="selected">Select Sub Department</option>
								<option value="{{subdata.sub_dept_id}}" ng-repeat="subdata in subdepartments">{{subdata.sub_dept_name}}</option>
								</select>
                                </div>
								
								                              
                                <div class="col-md-2">
                                    <label id="folder" name="folder">From Date</label>
                               <!--  </div>
                                <div class="col-md-2"> -->
                                   <div class="input-group date">
													<input type="text" class="form-control" datepicker-popup="{{format}}" name="fromDate" ng-model="model.fromDate" required is-open="fromDate" max-date="maxDate"  datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" show-button-bar="false" />
				            						<span class="input-group-addon" ng-click="open($event,'fromDate')"><i class="glyphicon glyphicon-calendar"></i></span>											</div>
								   </div>
                                </div>
                                <div class="col-md-2">
                                    <label id="folder" name="folder">To Date</label>
                               <!--  </div>
                                <div class="col-md-2"> -->
                                  <div class="input-group date">
												<input type="text" class="form-control" datepicker-popup="{{format}}" name="toDate" ng-model="model.toDate" required is-open="toDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" show-button-bar="false" />
				            					<span class="input-group-addon" ng-click="open($event,'toDate')"><i class="glyphicon glyphicon-calendar"></i></span>											</div>
								  </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-4" style="text-align:right;">
                                	<input type="submit" id="btnsubmit" data-toggle="modal"  ng-disabled="buttonDisabled" value="SUBMIT" class="btn btn-primary btn-blue"  ng-click="getMetaEntryData()"/>
                                  </div> 
                                <div class="col-md-4">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" onclick="goBack()""/>
                                </div>
                                <div class="col-md-4">
                                <button type="button" id="btnback" class="btn btn-primary btn-blue" style="text-align: center;"
												name="ExportToCsv1" ng-csv="dailyreportListData"
											 	filename="MetaDataReport.csv" data-toggle="modal">
												<i class="fa fa-edit"></i> Excel
								</button> 
								</div>
                            </div>
                        </div>
            		</form>
                    </div>
                    
                     <table id="customers" class="table table-bordered table-responsive" st-table="displayedCollection"
							st-safe-src="MetaDataList"
							class="table table-striped table-bordered nowrap table-hover" width="50%">
                                    <thead>
                                       <tr>
                                       <th colspan="2">
                                        <span style=" font-size: 150%;">Total Meta Data Count = </span><span style=" font-size: 150%;">{{count1}}</span>
                                        <th>
                                       </tr>
                                        <tr>
                                        	<th width:1%">SN</th>
                                        	<th>File Name</th>
                                        	<th>Meta Data Count</th>
                                            <!-- <th>Department Name</th>
                                            <th>Sub Department Name</th>
                                            <th>Document Type</th>
                                            <th>Field Type</th>
                                            <th>Meta Data Count</th> -->
                                           
            
                                        </tr>
                                       <!--  <tr>
										<th colspan="3"><input 
										st-search="" class="form-control" 
										placeholder="global search ..." type="text"/></th>
										</tr> -->
                                    </thead>
                                    <tbody>
                                            <tr id="dfd" ng-repeat="dfd in displayedCollection" class="odd gradeX">
                                            	<td align="center">{{$index+1}}</td>
                                                <td ng-model="dfd.name">{{dfd[0]}}</td>
                                                <td ng-model="dfd.setname">{{dfd[1]}} </td>
                                              <!--  <td ng-model="dfd.setname">{{dfd[2]}} </td>
                                                <td ng-model="dfd.setname">{{dfd[3]}}</td>
                                                  <td ng-model="dfd.setname">{{dfd[4]}} -->
                                                </td>
                                            </tr>
                                     </tbody>
                                     <tfoot>
									<!-- <th colspan="5";>Total: </th>
									<th style="color: white;">{{count1}}</th> -->
									<td colspan="3" class="text-center">
											<div st-pagination="" st-items-by-page="10"
											st-displayed-pages="8">
											</div>
										</td>
								</tfoot>
                                </table>
                
                </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>Stockholding DMS</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>
</body>
</html>			 