<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" data-ng-app="app" ng-controller="AppCtrl">
    <head>
        <meta charset="utf-8"/>
        <title>Angular PDF.js demo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="${pageContext.request.contextPath}/js/pdfjs/build/pdf.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/js/pdfjs/web/viewer.css">

        <script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
        <script src="${pageContext.request.contextPath}/js/angularJs/angular-pdfjs-viewer.js"></script>
        <script src="${pageContext.request.contextPath}/js/scripts/mdicontrollers/app.js"></script>

        <style>
          html, body {
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
          }

          .some-pdf-container {
            width: 100%;
            height: 100%;
          }
        </style>
    </head>
    <body>
        <div class='some-pdf-container'>
            <!-- Example of loading pdf from URL string. Note that this must be an interpolation string -->
            <!-- <pdfjs-viewer src="{{ pdf.src }}"></pdfjs-viewer> -->
            <P>{{pdf.src}}</P>
            <a href="${pageContext.request.contextPath}/js/pdfjs/web/viwer.html?file={{pdf.src}}">Hello</a>
            <%-- <iframe scrolling="no" src="${pageContext.request.contextPath}/js/pdfjs/web/viwer.html?file={{pdf.src}}" id="pdf" /> --%>
            
			<P>{{pdf.src}}</P>
            <!-- 
                Example of loading pdf from raw binary data. Note that this must be a scope variable.
                Comment upper viewer out if you use this viewer (because multi pdf viewers are currently not supported)
            -->
            <!-- <pdfjs-viewer data="pdf.data"></pdfjs-viewer> -->
        </div>
    </body>
</html>
