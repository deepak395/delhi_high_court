<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>
<script type="text/javascript"  
	src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload-shim.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/dirPagination.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-csv.js"></script>
<script type="text/javascript"

	src="${pageContext.request.contextPath}/js/angularJs/angular-sanitize.min.js">
	var download = document.getElementById('userDownload').value;
	
	</script>


<%@include file="../../content/header.jsp"%>
</head>
<body ng-controller="UploadDocumentController">
	<%@include file="../../content/sideheader.jsp"%>
	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="../images/Dms-Logo.png" alt="" />
				<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<!-- Profile Info -->
					<li>Welcome : <%=user.getUm_fullname()%></li>
					
				</ul>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="border-color: #c2c9dc;">
					<div class="panel-heading" style="background-color: #373e4a;">
						<div class="panel-title" style="font-weight: bold;">Basic
							Search</div>

					</div>
					<div class="panel-body">
						<%
							if (userr.getLk().getLk_longname().equals("System Admin") || userr.getLk().getLk_longname().equals("Search Admin")) {
						%>
						<form id="metadataentry">
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="MTfolder" name="MTfolder">Department</label>
									</div>
									<div class="col-md-4">

										<%--  <%if(userr.getLk().getLk_longname().equals("System Admin")) {%>  --%>
										<select id="dept_id" class="form-control" name="dept_id"
											ng-model="document.dfd_dept_mid"
											ng-change="getSubDept(document.dfd_dept_mid)">
											<option value="{{dept.dept_id}}"
												ng-repeat="dept in departments">{{dept.dept_name}}</option>
										</select>
										<%--   <% } else {%> --%>
										<%--  <span>${DEPT.dept_name }</span>
                               <span style="display: none;" ng-init="getSubDept(${DEPT.dept_id })"></span> --%>
										<%--  <% } %> --%>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="folderid" name="subfolder">Select Sub
											Department</label>
									</div>
									<div class="col-md-4" id="hidden_div">
										<select id="sub_dept_id" class="form-control"
											name="sub_dept_id" ng-model="document.dfd_sub_dept_mid"
											ng-change="getDocumentTypes(document.dfd_sub_dept_mid)">
											<option value="{{subdata.sub_dept_id}}"
												ng-repeat="subdata in subdepartments">{{subdata.sub_dept_name}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="dt_id" name="dept_type">Select Document
											Type</label>
									</div>
									<div class="col-md-4" id="hidden_div">
										<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid"
											ng-model="document.dfd_dt_mid" ng-change="getyears(document.dfd_dt_mid)">
											<option value="{{subdata.dt_id}}"
												ng-repeat="subdata in documentTypeList">{{subdata.dt_name}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="dt_id" name="dept_type">Select Folder </label>
									</div>
									<div class="col-md-4" id="hidden_div">
										<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid" ng-init="document.dfd_fd_mid=0"
											ng-model="document.dfd_fd_mid" ng-change="">
											<option value="0" >---Select---</option>
											<option value="{{subdata.id}}"
												ng-repeat="subdata in foldersList">{{subdata.folder_name}}</option>
										</select>
									</div>
								</div>
							</div>
							
							<!--  		<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="subfolder" name="subfolder">Year</label>
									</div>
									<div class="col-md-3" id="hidden_div3">
										<select id="year" name="year" class="form-control"
											ng-model="document.dfd_year">
											<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
										</select>
									</div>
								</div>
							</div> -->
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									
									<div class="col-md-3">
										<label id="subfolder" name="subfolder">Year</label>
									</div>
									<div class="col-md-3" id="hidden_div3">
										<select id="year" name="year" class="form-control"
											ng-model="document.dfd_year">
											<option>---Select---</option>
											<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<!-- <div class="col-md-3">
                                <label id="rep-name" for="Rep-name">Repository Name</label>
                            </div>
                            <div class="col-md-4">
                                <div class="box">
                                  <div class="container-1">
                                      <span class="icon"><i class="fa fa-search"></i></span>
                                      <input type="search" id="search" placeholder="Search..."  class="form-control" ng-model="document.dfd_name"/>
                                  </div>
                                </div>
                            </div> -->
									<div class="col-md-2" style="text-align: left;">
										<input type="button" id="submit" value="Search"
											ng-click="getfiles(document.dfd_sub_dept_mid,document.dfd_year,document.dfd_dept_mid,document.dfd_dt_mid,document.dfd_fd_mid)" />
									</div>
								</div>
							</div>

						</form>
						<%
							} else {
						%>
						<form id="metadataentry">
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="MTfolder" name="MTfolder">Department</label>
									</div>
									<div class="col-md-4">

										<span>${DEPT.dept_name }</span> <span style="display: none;"
											ng-init="getSubDept(${DEPT.dept_id })"></span>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="folderid" name="subfolder">Select Sub Department</label>
									</div>
									<div class="col-md-4" id="hidden_div">
										<select id="sub_dept_id" class="form-control"
											name="sub_dept_id" ng-model="document.dfd_sub_dept_mid"
											ng-change="getDocumentTypes(document.dfd_sub_dept_mid)">
											<option value="{{subdata.sub_dept_id}}"
												ng-repeat="subdata in subdepartments">{{subdata.sub_dept_name}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="dt_id" name="dept_type">Select Document
											Type</label>
									</div>
									<div class="col-md-4" id="hidden_div">
										<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid"
											ng-model="document.dfd_dt_mid" ng-change="getfolders(document.dfd_dt_mid)">
											<option value="{{subdata.dt_id}}"
												ng-repeat="subdata in documentTypeList">{{subdata.dt_name}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="dt_id" name="dept_type">Select Folder </label>
									</div>
									<div class="col-md-4" id="hidden_div">
										<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid" ng-init="document.dfd_fd_mid=0"
											ng-model="document.dfd_fd_mid" ng-change="">
											<option value="0" >---Select---</option>
											<option value="{{subdata.id}}"
												ng-repeat="subdata in foldersList">{{subdata.folder_name}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<div class="col-md-3">
										<label id="subfolder" name="subfolder">Year</label>
									</div>
									<div class="col-md-3" id="hidden_div3">
										<select id="year" name="year" class="form-control"
											ng-model="document.dfd_year">
											<option>---Select---</option>
											<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="margin: 15px 15px; font-size: 14px; color: #808080;">
									<!-- <div class="col-md-3">
                                <label id="rep-name" for="Rep-name">Repository Name</label>
                            </div>
                            <div class="col-md-4">
                                <div class="box">
                                  <div class="container-1">
                                      <span class="icon"><i class="fa fa-search"></i></span>
                                      <input type="search" id="search" placeholder="Search..."  class="form-control" ng-model="document.dfd_name"/>
                                  </div>
                                </div>
                            </div> -->
									<div class="col-md-2" style="text-align: left;">
										<input type="button" id="submit" value="Search"
											ng-click="getfiles(document.dfd_sub_dept_mid,document.dfd_year,${DEPT.dept_id },document.dfd_dt_mid,document.dfd_fd_mid)" />
									</div>
								</div>
							</div>

						</form>
						<%
							}
						%>
					</div>

					<div class="panel-body">
						<div class="tab-content">
							<div class="bs-component" id="profile-7">
							<span>Total Count = </span><span>{{total_count}}</span>
								<table id="myTable1"
									class="table table-bordered table-responsive"
									st-table="displayedCollection"
									st-safe-src="documentfiledetails"
									class="table table-striped table-bordered nowrap table-hover"
									width="100%">
									<thead>
										<tr>
											<th>Document Name</th>
											<th>Department</th>
											 <th ng-if="user.um_download_status=='1'" style="text-align: center;">Download</th>
											<th style="text-align: center;">View</th>
											
											
 
										</tr>

									</thead>
									<tbody>
										<tr id="dfd" ng-repeat="dfd in displayedCollection"
											class="odd gradeX">
											<td ng-model="dfd.name">{{dfd.dfd_name}}</td>
											<td ng-model="dfd.setname">
												{{dfd.departments.dept_name}}->{{dfd.subdepartments.sub_dept_name}}
											</td>
											<td ng-if="user.um_download_status=='1'" style="text-align: center;">
								            <a class="btn btn-info btn-xs" ng-click="downloadFile(dfd.dfd_id)">Download</a>
								            </td>
																						
											<td style="text-align: center;">
											
											<a href="#"   class="btn btn-info btn-xs" ng-click="ViewFile(dfd.dfd_id)">View</a>
											
											<!-- <input
												class="btn btn-info btn-xs" data-toggle="modal"
												data-target="#user_Modal" type="button" value="view"
												ng-click="ViewFile(dfd.dfd_id)"> --> <!-- <input type="submit" id="btnsubmit" value="View" class="btn btn-primary btn-blue" ng-click="ViewFile(dfd.dfd_id)"/> -->
											</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="6" class="text-center">
												<div st-pagination="" st-items-by-page="5"
													st-displayed-pages="8"></div>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>

					</div>
				</div>


				<!-- view File  -->
				<%-- <div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true">

					<div class="modal-dialog modal-lg">
						<div class="modal-content" style="width: 606px;">
							<div class="modal-header"
								style="background-color: black; width: 881px;">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel" align="center">
									<strong style="color: #FBFCFD;">{{viewDfdData.dfd_name}}</strong>
								</h4>
							</div>


							<div class="modal-header" style="width: 881px;">
							
							
							<%@include file="../../views/document/viewer.jsp"%>
							<jsp:include page="viewer.jsp"></jsp:include>

								<object type="application/pdf"
									data="{{fileurl}}#toolbar=0&navpanes=0&scrollbar=0"
									width="100%" height="600"> </object>
							</div>

						</div>
					</div>
				</div> --%>


			</div>
		</div>
		<!-- Footer -->
		<footer class="main"> Copyright � <strong>stockholdingdms</strong>2018.
		All Rights Reserved </footer>
	</div>

	<script type="text/javascript">
	
	
		document.getElementById('test').addEventListener('change', function() {
			var style = this.value == 2 ? 'block' : 'none';
			document.getElementById('hidden_div2').style.display = style;
		});
		document.getElementById('test').addEventListener('change', function() {
			var style = this.value == 1 ? 'block' : 'none';
			document.getElementById('hidden_div').style.display = style;
		});
		document.getElementById('test1').addEventListener('change', function() {
			var style = this.value == 2 ? 'block' : 'none';
			document.getElementById('hidden_div3').style.display = style;
		});
		document.getElementById('test1').addEventListener('change', function() {
			var style = this.value == 1 ? 'block' : 'none';
			document.getElementById('hidden_div4').style.display = style;
		});
	</script>
	<script type="text/javascript"
	src="${context}/mdi/js/scripts/mdicontrollers/UploadDocumentController.js"></script>
</body>
</html>