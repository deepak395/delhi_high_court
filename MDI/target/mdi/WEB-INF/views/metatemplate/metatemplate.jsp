<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="mdiApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript"
	src="${context}/mdi/js/scripts/mdicontrollers/MetaTemplateController.js"></script>

<%@include file="../../content/header.jsp"%>


</head>
<body class="page-body skin-facebook" data-url="http://neon.dev"
	ng-controller="MetaTemplateController">
	<%@include file="../../content/sideheader.jsp"%>
	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="${context}/mdi/images/Dms-Logo.png" alt="" />
				<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<li>Welcome : <%= user.getUm_fullname() %></li>
				</ul>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-12">
				<div id="divAction" style="float: right; margin-bottom: 5px">
					<button type="button" class="btn btn-primary btn-sm pull-right"
						data-toggle="modal" data-target="#user_Modal"
						ng-click="resetdata()">
						<span class="glyphicon glyphicon-plus-sign"></span> Add New
						Template
					</button>

				</div>

				<div id="divAction" style="float: right; margin-bottom: 5px">
					<button type="button" class="btn btn-primary btn-sm pull-right"
						data-toggle="modal" data-target="#doc_type" ng-click="resetdata()">
						<span class="glyphicon glyphicon-plus-sign"></span> Add Document
						Type
					</button>

				</div>
				
				<div id="divAction" style="float: right; margin-bottom: 5px">
					<button type="button" class="btn btn-primary btn-sm pull-right"
						data-toggle="modal" data-target="#folder_create" ng-click="resetdatafolder()">
						<span class="glyphicon glyphicon-plus-sign"></span> Add Folder
					</button>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="border-color: #c2c9dc;">

					<div class="panel-heading" style="background-color: #373e4a;">
						<div class="panel-title" style="font-weight: bold;">Meta
							Template Creation</div>


					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane active" id="profile-7">
								<table id="myTable1"
									class="table table-bordered table-responsive">
									<thead>
										<tr>
											<th>Repository Name</th>
											<th>Meta-template</th>
											<th style="text-align: center;"></th>

										</tr>
									</thead>
									<tbody ng-repeat="dept in dep_details">
										<tr>
											<td>{{dept.dept_repo_name}}</td>
											<td>{{dept.dept_name}}</td>
											<td style="text-align: center;">
												<button type="button"
													class="btn btn-primary btn-sm pull-right"
													data-toggle="modal" data-target="#SubDepartment_Modal"
													ng-click="test($index)">
													<span class="glyphicon glyphicon-plus-sign"></span> Add Sub
													Department
												</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- Footer -->
		<footer class="main"> Copyright � <strong>stockholdingdms</strong>2018.
		All Rights Reserved </footer>
	</div>
	</div>

	<div class="modal fade" id="modal-ChangePassword"
		data-backdrop="static">
		<div class="modal-dialog" style="width: 98%">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Metadata Entry</h4>
				</div>

				<div id="divBodyPE" class="modal-body">
					<div class="row">
						<div class="col-md-7">
							<img src="${context}/mdi/images/rough-pdfimg.jpg">
						</div>
						<div class="col-md-5">
							<div class="row">
								<div class="form-group"
									style="font-size: 14px; color: #808080; padding: 10px 0px;">
									<div class="col-md-3"
										style="text-align: left; font-size: 14px; color: #4a4a4a;">
										<label id="frt-pet" for="Ft-petit">First Petitioner</label>
									</div>
									<div class="col-md-8">
										<input type="text" name="Ft-petit" class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="font-size: 14px; color: #808080; padding: 10px 0px;">
									<div class="col-md-3"
										style="text-align: left; font-size: 14px; color: #4a4a4a;">
										<label id="Benchtype" for="Ft-petit">Bench Type</label>
									</div>
									<div class="col-md-8">
										<select id="metadata" class="form-control">
											<option value="0">--Select--</option>
											<option value="1">Single Bench</option>
											<option value="1">Division Bench</option>
											<option value="1">Full Bench</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="font-size: 14px; color: #808080; padding: 10px 0px;">
									<div class="col-md-3"
										style="text-align: left; font-size: 14px; color: #4a4a4a;">
										<label id="coc" for="coc">Contempt of Court</label>
									</div>
									<div class="col-md-8">
										<input type="checkbox" id="COC" value="Contempt of Court" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="font-size: 14px; color: #808080; padding: 10px 0px;">
									<div class="col-md-3"
										style="text-align: left; font-size: 14px; color: #4a4a4a;">
										<label id="caseyear" for="Ft-petit">Case Year</label>
									</div>
									<div class="col-md-7">
										<input type="text" name="Ft-petit" class="form-control" />
									</div>
									<div class="col-md-1">
										<i class="entypo-calendar"></i>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="font-size: 14px; color: #808080; padding: 10px 0px;">
									<div class="col-md-3"
										style="text-align: left; font-size: 14px; color: #4a4a4a;">
										<label id="caseno" for="Ft-petit">Case Number</label>
									</div>
									<div class="col-md-8">
										<input type="text" name="Ft-petit" class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group"
									style="font-size: 14px; color: #808080; padding: 10px 0px;">
									<div class="col-md-3"
										style="text-align: left; font-size: 14px; color: #4a4a4a;">
										<label id="casetype" for="Ft-petit">Case Type</label>
									</div>
									<div class="col-md-8">
										<select id="metadata" class="form-control">
											<option value="0">--Select--</option>
											<option value="1">Case Type 1</option>
											<option value="1">Case Type 2</option>
											<option value="1">Case Type 3</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row" style="padding: 15px 0px;">
								<div class="form-group" style="font-size: 14px; color: #808080;">
									<div class="col-md-6" style="text-align: right;">
										<input type="submit" id="btnsubmit" value="SUBMIT"
											class="btn btn-primary btn-blue" />
									</div>
									<div class="col-md-6">
										<input type="Button" id="btnback" value="BACK"
											class="btn btn-primary btn-blue" onclick="goBack()" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-PasswordSent" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Client Tariff Master</h4>
				</div>

				<div id="divBodySent" class="modal-body"></div>

				<div class="modal-footer">
					<button type="button" id="btnOk" class="btn btn-info"
						data-dismiss="modal">Ok</button>
				</div>
			</div>
		</div>
	</div>

	<!-- ADD DEPARTMENT -->
	<div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">
						<span ng-if="!masterentity.um_id"><strong> Add New
								Template</strong></span> <span ng-if="masterentity.um_id"><Strong>Update
								User</Strong></span>
					</h4>
				</div>
				<form id="documentupload">
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="rep-name" for="Rep-name">Repository Name</label>
							</div>
							<div class="col-md-6">
								<select id="rep-id" class="form-control"
									ng-model="department.dept_repo_id">
									<option value="{{repository.id}}"
										ng-repeat="repository in repositories">{{repository.name}}</option>

								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="metadata" for="metadata">Meta Template Name</label>
							</div>
							<div class="col-md-6">
								<input type="text" id="metatemp" name="metatemp"
									class="form-control" ng-model="department.dept_name" />
								<div ng-if="sho">
									<span style="color: red; font-weight: bold">MINIMUM TWO
										CHAR</span>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="folder" for="folder">Description</label>
							</div>
							<div class="col-md-6">
								<textarea name="metatemp" class="form-control" row="4"
									ng-model="department.dept_description"></textarea>
							</div>
						</div>

					</div>
					<div class="row" style="padding-top: 15px;">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6" style="text-align: right;">
								<input type="submit" id="btnsubmit" value="SUBMIT"
									class="btn btn-primary btn-blue" ng_click="createDepartment()" />
							</div>
							<div class="col-md-6">
								<input type="Button" id="btnback" value="BACK"
									class="btn btn-primary btn-blue" ng-click="backButton()" />
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
	<!-- ADD SUB DEPARTMENT -->
	<div class="modal fade" id="SubDepartment_Modal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<%@ include file="../../views/metatemplate/_addSubDepartment.jsp"%>
	</div>


	<!-- add doc type -->
	<div class="modal fade" id="doc_type" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">
						<span ng-if="!masterentity.um_id"><strong> Add New
								Document Type</strong></span> <span ng-if="masterentity.um_id"><Strong>Update
								User</Strong></span>
					</h4>
				</div>
				<form id="doc_type">
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="rep-name" for="Rep-name">Repository</label>
							</div>
							<div class="col-md-6">
								<span class="form-control">{{repositories[0].name}}</span>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="metadata" for="metadata">Select Template</label>
							</div>
							<div class="col-md-6">
								<select id="rep-id" class="form-control"
									ng-model="doc.dept_id"
									ng-change="getAllSubDept(doc.dept_id)">
									<option value="{{dept.dept_id}}"
										ng-repeat="dept in dep_details">{{dept.dept_name}}</option>

								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="folder" for="folder">Select Sub Department</label>
							</div>
							<div class="col-md-6">
								<select id="rep-id" class="form-control"
									ng-model="doc_type.dt_sub_mid" >
									<option value="{{sub_dept.sub_dept_id}}"
										ng-repeat="sub_dept in subdepartments">{{sub_dept.sub_dept_name}}</option>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="folder" for="folder">Input Document Type</label>
							</div>
							<div class="col-md-6">
								<input class="form-control" type="text" ng-model="doc_type.dt_name">
							</div>
						</div>

					</div>
					<div class="row" style="padding-top: 15px;">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6" style="text-align: right;">
								<input type="submit" id="btnsubmit" value="SUBMIT"
									class="btn btn-primary btn-blue" ng_click="createDocType()" />
							</div>
							<div class="col-md-6">
								<input type="Button" id="btnback" value="BACK"
									class="btn btn-primary btn-blue" ng-click="backButton()" />
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
	
	<!-- Folder -->
	<div class="modal fade" id="folder_create" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">
						<span ng-if="!masterentity.um_id"><strong> Add New
								Document Type</strong></span> <span ng-if="masterentity.um_id"><Strong>Update
								User</Strong></span>
					</h4>
				</div>
				<form id="doc_type">
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="rep-name" for="Rep-name">Repository</label>
							</div>
							<div class="col-md-6">
								<span class="form-control">{{repositories[0].name}}</span>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="metadata" for="metadata">Select Template</label>
							</div>
							<div class="col-md-6">
								<select id="rep-id" class="form-control"
									ng-model="doc.dept_id"
									ng-change="getAllSubDept(doc.dept_id)">
									<option value="{{dept.dept_id}}"
										ng-repeat="dept in dep_details">{{dept.dept_name}}</option>

								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="folder" for="folder">Select Sub Department</label>
							</div>
							<div class="col-md-6">
								<select id="rep-id" class="form-control"
									ng-model="doc.sub_dept_id"
									ng-change="getDocumentType(doc.sub_dept_id)" >
									<option value="{{sub_dept.sub_dept_id}}"
										ng-repeat="sub_dept in subdepartments">{{sub_dept.sub_dept_name}}</option>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="folder" for="folder">Select Document Type</label>
							</div>
							<div class="col-md-6">
								<select id="rep-id" class="form-control"
									ng-model="doc.dt_id" >
									<option value="{{dt.dt_id}}"
										ng-repeat="dt in documenttypes">{{dt.dt_name}}</option>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6">
								<label id="folder" for="folder">Enter Folder Name</label>
							</div>
							<div class="col-md-6">
								<input class="form-control" type="text" ng-model="doc.fd_name">
							</div>
						</div>

					</div>
					<div class="row" style="padding-top: 15px;">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-6" style="text-align: right;">
								<input type="submit" id="btnsubmit" value="SUBMIT"
									class="btn btn-primary btn-blue" ng_click="createFolder()" />
							</div>
							<div class="col-md-6">
								<input type="Button" id="btnback" value="BACK"
									class="btn btn-primary btn-blue" ng-click="backButton()" />
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</body>
</html>