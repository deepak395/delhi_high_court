<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="${context}/mdi/assets/css/Neon.css" rel="stylesheet">
<link href="${context}/mdi/css/Style-Main.css" rel="stylesheet">
  
    <script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
    <script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/FolderController.js"></script>
   
</head>
<body ng-app="mdiApp" class="page-body skin-facebook" data-url="http://neon.dev"   ng-controller="FolderController">
    
     <%@include file="../../content/sideheader.jsp" %>
        <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context}/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <li>Welcome : <%= user.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           	<div class="row">
            	<div class="col-md-3"></div>
                <div class="col-md-6">
                	<div class="panel panel-primary" style="border-color: #c2c9dc;">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Folder OR Sub Folder Creation</div>
            
                       <!--  <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
                    </div>
                    <div class="panel-body">
                    <form id="documentupload">
                             <div class="row">
                                    <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                        <div class="col-md-6">
                                            <label id="rep-name" for="Rep-name">Repository Name</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select id="rep-id" class="form-control" ng-model="folderform.rep_id">
                                                <option value="{{repository.id}}" ng-repeat="repository in repositories">{{repository.name}}</option>
                                                
                                            </select>
                                        </div>
                                     </div>
                                  </div>
                             <div class="row">
                                    <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                        <div class="col-md-6">
                                            <label id="metadata" for="metadata">Meta Template Name</label>
                                        </div>
                                        <div class="col-md-6">
                                           <select id="rep-id" class="form-control" ng-model="folderform.dept_id" ng-change="getFolderByDeptId(folderform.dept_id)">
                                                <option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>
                                                
                                            </select>
                                        </div>
                                     </div>
                                  </div>
                                  
                                <div class="row" >
                                <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                    <div class="col-md-6">
                                        <label id="metadata" for="metadata">Select Folder Or Sub Folder</label>
                                    </div>
                                    <div class="col-md-6">
                                      Folder : <input type="radio" value="0" name="foder" ng-model="content"> Sub Folder :<input type="radio" name="foder" value="1" ng-model="content">
                                    </div>
                                 </div>
                              </div>
                                  
                              <div class="row" ng-show="content == '0'" >
                                <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                    <div class="col-md-6">
                                        <label id="metadata" for="metadata">Folder Name</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="metatemp" class="form-control" ng-model="folderform.folder_name"  />
                                    </div>
                                 </div>
                              </div>
                              <div class="row" ng-show="content == '1'">
                                <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                    <div class="col-md-6">
                                        <label id="metadata" for="metadata">Parent Folder Name</label>
                                    </div>
                                    <div class="col-md-6">
                                       <select id="rep-id" class="form-control" ng-model="folderform.parent_id">
                                                <option value="{{fold.id}}" ng-repeat="fold in folders2">{{fold.folder_name}}</option>
                                        </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="row" ng-show="content == '1'">
                                <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                    <div class="col-md-6">
                                        <label id="metadata" for="metadata">Sub Folder Name</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="metatemp" class="form-control" ng-model="folderform.folder_name"  />
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="row">
                                <div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                    <div class="col-md-6">
                                        <label id="folder" for="folder">Description</label>
                                    </div>
                                    <div class="col-md-6">
                                        <textarea name="metatemp" class="form-control" row="4" ng-model="folderform.description"></textarea>
                                    </div>
                                </div>
                            </div>
                        	<div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-6" style="text-align:right;">
                                	<input type="submit" id="btnsubmit" value="SUBMIT" class="btn btn-primary btn-blue" ng-click="createFolder()" />
                                  </div> 
                                <div class="col-md-6">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" onclick="newPage();"/>
                                </div>
                            </div>
                        </div>
            		</form>
                    </div>
                </div>
            	</div>
                <div class="col-md-3"></div>
            </div>
			<!-- Footer -->
           <footer class="main" style="position:fixed;bottom:0px;">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>

    <div class="modal fade" id="modal-ChangePassword" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>

                <div id="divBodyPE" class="modal-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label for="lblOldPassword" class="col-sm-3 control-label">Old Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtOldPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblNewPassword" class="col-sm-3 control-label">New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtNewPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblConfirmPassword" class="col-sm-3 control-label">Confirm Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtConfirmPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="lblErrorPassword" style="display:none;color:red;"></label>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="button" id="btnChangePassword" class="btn btn-info" value="Change" />
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-PasswordSent" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Client Tariff Master</h4>
                </div>

                <div id="divBodySent" class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" id="btnOk" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>