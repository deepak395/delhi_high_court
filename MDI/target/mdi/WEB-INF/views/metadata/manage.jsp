<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>
<script type="text/javascript"
	src="${context}/mdi/js/scripts/mdicontrollers/UploadDocumentController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload-shim.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/dirPagination.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-csv.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/angular-sanitize.min.js"></script>
<!-- <script type="text/javascript">


</script> -->
	
<%@ page import="com.mdi.model.User"%>
<%@page import="com.mdi.model.UserRole"%>
<%@ page import="java.util.List"%>
</head>
<%@include file="../../content/header.jsp"%>



<body ng-controller="UploadDocumentController" ng-init="getUserWiseData()">
	<%@include file="../../content/sideheader.jsp"%>

	<%-- <% 
User user1 = null;
if(session.getAttribute("USER")!=null)
	 user1 = (User)session.getAttribute("USER");

%>
<% 
				List<UserRole> userroles=user1.getUserroles();
				Boolean dataentryop=false;
				UserRole userr=null;
				//out.println(userroles);
				for(UserRole userrole:userroles){
					 userr=userrole;                                        /* || userrole.getLk().getLk_longname().equals("Data Entry Operator (I/O)") */
				}
%> --%>
	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="../images/Dms-Logo.png" alt="" />
				<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<!-- Profile Info -->
					<li>Welcome : <%= user.getUm_fullname() %></li>
				</ul>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="border-color: #c2c9dc;">
					<div class="panel-heading" style="background-color: #373e4a;">
						<div class="panel-title" style="font-weight: bold;">Metadata
							Entry</div>

						<!--    <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane active" id="profile-7">
							
		<div>
		 <div class="row">
			<div class="col-md-4">
						<%--  <%if(userr.getLk().getLk_longname().equals("System Admin")) {%>  --%>
				<select id="dept_id" class="form-control" name="dept_id" ng-model="document.dfd_dept_mid" ng-change="getSubDept(document.dfd_dept_mid)">
				<option value="0" selected="selected">Select Department</option>
							<option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>
				</select>
						
			</div>
		
				<div class="col-md-4" id="hidden_div">
					<select id="sub_dept_id" class="form-control" name="sub_dept_id" ng-model="document.dfd_sub_dept_mid" ng-change="getDocumentTypes(document.dfd_sub_dept_mid)">
						<option value="0" selected="selected">Select Sub Department</option>
						<option value="{{subdata.sub_dept_id}}" ng-repeat="subdata in subdepartments">{{subdata.sub_dept_name}}</option>
					</select>
				</div>
				
				
				<div class="col-md-4" id="hidden_div">
					<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid" ng-model="document.dfd_dt_mid" ng-change="getyears(document.dfd_dt_mid)">
						<option value="0" selected="selected">Select Document Type</option>
						<option value="{{subdata.dt_id}}" ng-repeat="subdata in documentTypeList">{{subdata.dt_name}}</option>
					</select>
				</div>
				</div><br>
				  <div class="row">
				<div class="col-md-4" id="hidden_div">
					<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid" ng-init="document.dfd_fd_mid=0" ng-model="document.dfd_fd_mid">
						<option value="0" >---Select---</option>
						<option value="{{subdata.id}}" ng-repeat="subdata in foldersList">{{subdata.folder_name}}</option>
					</select>
				</div> 
				
				<div class="col-md-3" id="hidden_div3">
					<select id="year" name="year" class="form-control" ng-model="document.dfd_year">
						<option value="" selected="selected">Year</option>
						<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
					</select>
				</div>
				<div class="col-md-3" id="hidden_div3">
				<button type="button" class="btn btn-primary" data-ng-click="getFileByDeptAndSubDeptAndDocTypeUserWiseData(document)">SEARCH</button>
				</div>
				</div><br>
							
							
			</div>
							
							
							
		<table id="myTable1" id="myTable1" class="table table-bordered table-responsive" st-table="displayedCollection"
					st-safe-src="documentfiledetails" width="100%">
			<thead>
				<tr>
					<th st-sort="documentName">Document Name</th>
					<th st-sort="department">Department</th>
					<th st-sort="subDepartment">Sub Department</th>
					<th st-sort="documentType">Document Type</th>
					<th st-sort="year">Year</th>
					<%if((userr.getLk().getLk_longname().equals("MDIChecker"))) {%>
					<th style="text-align: center";><input type="submit" id="btnsubmit" value="Approve Selected"
					class="btn btn-primary btn-blue" data-ng-click="approveAll()" />

					<input id=data.dfd_id type="checkbox" value="data.dfd_id" name="dfd_id" data-ng-click="checkAll()" data-ng-model="selectedAll" />
					</th>
					<% } %>
					<th style="text-align: center;">Data Entry</th>

				</tr>
					</thead>
					<tbody>
								<!-- <tr>

									<th><input st-search="dfd_name"
										placeholder="Document Name"
										class="input-sm form-control search-input" type="search" /></th>
										
									<th><input st-search="departments.dept_name"
										placeholder="Department"
										class="input-sm form-control search-input" type="search" /></th>
										
									<th><input st-search="subdepartments.sub_dept_name"
										placeholder="Sub Department"
										class="input-sm form-control search-input" type="search" /></th>
										
									<th><input st-search="doctype.dt_name"
										placeholder="Document Type"
										class="input-sm form-control search-input" type="search" />
									</th>
									<th><input st-search="dfd_year" placeholder="Year"
										class="input-sm form-control search-input" type="search" />
									</th>
								</tr> -->
								
								
						<!-- <tr id="dfd" data-ng-repeat="dfd in displayedCollection | orderBy : 'dfd_name'"class="odd gradeX">	 -->	
						
						<tr id="dfd" data-ng-repeat="dfd in documentDetailList | orderBy : 'dfd_name'"class="odd gradeX">
							<td data-ng-model="dfd.name">{{dfd.dfd_name}}</td>
							<td data-ng-model="dfd.setname">{{dfd.departments.dept_name}}</td>
							<td data-ng-model="dfd.setname">{{dfd.subdepartments.sub_dept_name}}</td>
							<td data-ng-model="dfd.setname">{{dfd.doctype.dt_name}}</td>
							<td data-ng-model="dfd.setname">{{dfd.dfd_year}}</td>
							<%if((userr.getLk().getLk_longname().equals("MDIMetaData"))) {%>
							<td style="text-align: center;"><input type="submit" id="btnsubmit" value="DATA ENTRY"
								class="btn btn-primary btn-blue" data-ng-click="gotoDataEntry(dfd.dfd_id)" /></td>
							<% } %>
							<%if((userr.getLk().getLk_longname().equals("MDIChecker"))) {%>
							<td style="text-align: center";><input type="checkbox"
								name="dfd" id="dfd" value={{dfd}}
								data-ng-click="checkedIndex(dfd)" data-ng-model="dfd.checkbox" />
							<td style="text-align: center;"><input type="submit" id="btnsubmit" value="Verify"
								class="btn btn-primary btn-blue" data-ng-click="gotoDataEntry(dfd.dfd_id)" /></td>
							<% } %>
						</tr>
					</tbody>
				<tfoot>
				<tr>
					<td colspan="6" class="text-center">
					<div st-pagination="" st-items-by-page="5" st-displayed-pages="8"></div>
					</td>
			   </tr>
			</tfoot>
		</table>
					</div>
				</div>

			</div>
		</div>
	</div>
		</div>
		<!-- Footer -->
		<footer class="main"> Copyright � <strong>stockholdingdms</strong>2018.
		All Rights Reserved </footer>
	</div>
	</div>
	<script type="text/javascript">
	  document.getElementById('test').addEventListener('change', function () {
		var style = this.value == 2 ? 'block' : 'none';
		document.getElementById('hidden_div2').style.display = style;
	});
	document.getElementById('test').addEventListener('change', function () {
		var style = this.value == 1 ? 'block' : 'none';
		document.getElementById('hidden_div').style.display = style;
	});
	document.getElementById('test1').addEventListener('change', function () {
		var style = this.value == 2 ? 'block' : 'none';
		document.getElementById('hidden_div3').style.display = style;
	});
	document.getElementById('test1').addEventListener('change', function () {
		var style = this.value == 1 ? 'block' : 'none';
		document.getElementById('hidden_div4').style.display = style;
	});
  </script>
</body>
</html>