
<form class="form-horizontal reduce-gap" name="masterForm" role="form" novalidate>      
<div class="modal-body">
<div ng-show="errorlist" class="alert alert-block alert-danger">
	<ul>
    <span ng-repeat="errors in errorlist"  >
	        <span ng-repeat="n in errors track by $index">
	        	<li>{{(n)}}</li>
	        </span>
	    </span>
    </ul>
</div>

<div class="row">
	<div class="col-md-6" ng-if="!masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="um_bench_code">Branch<span class="star">*</span></label>
		    <div class="col-md-7">
	      		<select id="um_bench_code" ng-options="bcd.lk_id as bcd.lk_longname for bcd in branchDataList"
											class="form-control" id="branchData" name="branchCode"
											ng-model="masterentity.um_bench_code=8" disabled  />
				</select> 
	      	</div>		   
	</div>
	</div>
	
	
	
	<div class="col-md-6" ng-if="masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="um_bench_code">Branch<span class="star">*</span></label>
		    <div class="col-md-7">
	   <input class="form-control" type="text" id="um_bench_code" name="um_bench_code" ng-model="masterentity.lk_benchcode.lk_longname" ng-readonly="true"/> 

	      	</div>		   
	</div>
	</div>
	
	<div class="col-md-6">
	<div class="form-group">
			<label class="col-md-4  control-label" for="um_fullname">Full Name <span class="star">*</span></label>
		    <div class="col-md-7">
		    <!-- ng-pattern="/^[a-zA-Z0-9]*$/" -->
	      		<input class="form-control" type="text" id="um_fullname" name="um_fullname" ng-model="masterentity.um_fullname" placeholder="User Full Name" ng-minlength=1 ng-maxlength=20	/> 
	      	</div>		   
	</div>
	</div>
	
	<div class="col-md-6" ng-if="!masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="username">Username<span class="star">*</span></label>
		    <div class="col-md-7">
		    <!-- ng-pattern="/^[a-zA-Z0-9]*$/" -->
	      		<input type="text" class="form-control" id="username" name="username" placeholder="Login Id" ng-model="masterentity.username"
					/>
			</div>		   
	</div>
	</div>
	
	<div class="col-md-6" ng-if="masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="username">Username<span class="star">*</span></label>
		    <div class="col-md-7">
	      		<input type="text" class="form-control" id="username" name="username" ng-model="masterentity.username"
					ng-pattern="/^[a-zA-Z0-9]*$/" ng-readonly="true"/>
			</div>		   
	</div>
	</div>
	
	<div class="col-md-6" ng-if="!masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="password">Password<span class="star">*</span></label>
		    <div class="col-md-7">
	      		<input type="password" class="form-control" id="password" placeholder="password" ng-model="masterentity.password" />
			</div>		   
	</div>
	</div>

	
	
	<div class="col-md-6" ng-if="!masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="confirmpassword">Confirm Password<span class="star">*</span></label>
		    <div class="col-md-7">
	      		<input type="password" class="form-control" id="confirmpassword" placeholder="password" ng-model="masterentity.confirmpassword" />
			</div>		   
	</div>
	</div>
	
	<div class="col-md-6">
	<div class="form-group">
			<label class="col-md-4  control-label" for="um_email_id">EmailId <span class="star">*</span></label>
		    <div class="col-md-7">
	      		<input class="form-control" type="text" id="um_email_id" name="um_email_id" ng-model="masterentity.um_email_id" placeholder="Email Id" ng-minlength=1 ng-maxlength=50	ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" /> 
	      	</div>		   
	</div>
	</div>
	
	<div class="col-md-6" ng-if="!masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="um_role_id">User Role <span class="star">*</span></label>
		    <div class="col-md-7">
	      		<select id="um_role_id" ng-model="masterentity.um_role_id" ng-change="showDeptDiv(masterentity.um_role_id)"
	      		 class="form-control" id="role_id" required name="role_id" ng-options="rl.lk_id as rl.lk_longname for rl in roleData"></select>
			</div>		   
	</div>
	</div>
	
	<div class="col-md-6" ng-if="masterentity.um_id">
	<div class="form-group">
			<label class="col-md-4  control-label" for="um_role_id">User Role <span class="star">*</span></label>
		    <div class="col-md-7">
	      		<select id="um_role_id" ng-model="masterentity.um_role_id" ng-change="showDeptDiv(masterentity.um_role_id)"
	      		 class="form-control" id="role_id" required name="role_id" ng-options="rl.lk_id as rl.lk_longname for rl in roleData"></select>
			</div>		   
	</div>
	</div>
	
	
	<div class="col-md-6" ng-if="roleid">
		<div class="form-group">
				<label class="col-md-4  control-label" for="um_vendor_id">Department <span class="star">*</span></label>
			    <div class="col-md-7" >
		      		<select ng-model="masterentity.um_department_id" class="form-control"
							id="um_vendor_id" required name="um_vendor_id"
							ng-options="td.dept_id as td.dept_name for td in departmentData"
							required ></select>
				</div>		   
		</div>
	</div>
	
	<div class="col-md-6" ng-if="masterentity.um_role_id=='10000579'|| masterentity.um_role_id=='10000580'|| masterentity.um_role_id=='1'">
		<div class="form-group">
				<label class="col-md-4  control-label" for="um_altemail_id">Alternative EmailId <span class="star">*</span></label>
		    <div class="col-md-7">
	      		<input class="form-control" type="text" id="um_altemail_id" name="um_altemail_id" ng-model="masterentity.um_altemail_id" placeholder="Email Id" ng-minlength=1 ng-maxlength=50	ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/"  required/> 
	      	</div>			   
		</div>
	</div>
	
	
	
	
	
	<div class="col-md-6" ng-if="masterentity.um_id">
	<div class="form-group" >
			<label class="col-md-4  control-label" for="um_pass_validity_date">Validity Date</label>
		    <div class="col-md-7">
	      		<input type="text" class="form-control" id="um_pass_validity_date" name="um_pass_validity_date" ng-model="masterentity.um_pass_validity_date|date:'dd/MM/yyyy'"
					  />
			</div>		   
	</div>
	</div>
	
<!-- 	
	<div class="col-md-6">
		<div class="form-group">
				<label class="col-md-4  control-label" for="um_designation">Designation<span class="star">*</span></label>
			    <div class="col-md-7">
		      		<select ng-options="bcd.lk_id as bcd.lk_longname for bcd in designationData" class="form-control" id="um_designation" name="um_designation" 
		      														ng-model="masterentity.um_designation" >
											</select>
				</div>		   
		</div>
	</div>	 -->
	
	<div class="col-md-6">
		<div class="form-group">
				<label class="col-md-4  control-label" for="um_vendor_id">Status <span class="star">*</span></label>
			    <div class="col-md-7">
		      		<input type="radio" name="Active" value="1"
											ng-model="masterentity.rec_status" /> Active 
					<input type="radio" name="InActive" value="2"
											ng-model="masterentity.rec_status" /> InActive
				</div>		   
		</div>
	</div>
   
   <div class="col-md-6">
		<div class="form-group">
				<label class="col-md-4  control-label" for="um_download_status">Download <span class="star">*</span></label>
			    <div class="col-md-7">
		      		<input type="radio" name="Assign" value="1"
											ng-model="masterentity.um_download_status"/>Active 
					<input type="radio" name="Remove" value="2"
											ng-model="masterentity.um_download_status"/>InActive
				</div>		   
		</div>
	</div>
</div>
</div>

<div class="modal-footer"> 
	<div ng-if="!masterentity.um_id">
			<input type="submit" value="Submit" id="create-masterForm" data-loading-text="Creating..." autocomplete="off" ng-click="user_create(masterentity)" class="btn btn-success"/>      			
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>  
	</div>
	
	<div ng-if="masterentity.um_id">
			<input type="submit" value="Reset" id="update-masterForm" data-loading-text="Creating..." autocomplete="off" ng-click="Date_Reset()" class="btn btn-success"/>
			<input type="submit" value="Update" id="update-masterForm"  data-loading-text="Updating..." autocomplete="off" ng-click="user_update(masterentity)" class="btn btn-success"/>      			
			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>  	
	</div>
	     
</div>

</form>

 