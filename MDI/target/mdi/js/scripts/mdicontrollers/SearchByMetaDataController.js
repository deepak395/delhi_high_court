var mdiApp= angular.module('mdiApp', ['smart-table']);

mdiApp.controller('MetaDataController',function($scope,$http,$window,$filter) 
{
	$scope.tx='Virendra';
	var urlBase='/mdi'
	$scope.viewDfdData={};
	$scope.folderform={};
	$scope.repositories=[];
	$scope.departments=[];
	$scope.subdepartments=[];
	$scope.indexDataList=[];
	$scope.fieldsdata=[];
	$scope.index_field={};
	$scope.updateindexfield={};
	$scope.metadata=[];
	$scope.subdept_id='';
	 $scope.fileurl='';
	var count=0;
	
	 
	
	
	getAllData();
	
	
	/*******************/
	
	
	var year=new Date().getFullYear();
	var data=[];
	data.push(year);
	for(var i=1;i<100;i++)
	{
		data.push(year-i);
	}
	$scope.years=data;
	console.log("Year"+$scope.years);

	/*$scope.ViewFile = function(index)
	{
	
		
		//var doc_id=$scope.documentfiledetails[index].dfd_id;
		window.open(urlBase+'/document/File_Meta?id='+index);
	}*/
	
	$scope.ViewFile =function(value)
	{
		       $http.get('/mdi/search/viewFile?dfd_id='+value).success(function (data) 
				 {
							console.log(data);
							$scope.fileurl='/mdi/uploads/'+data.data;
							$scope.viewDfdData=data.modelData;
							
							
                            var url='/mdi/document/open?dfd_id='+value;
							
							window.open(url, '_blank');
							
							            
				 }).
						  error(function(data, status, headers, config) {
							        	console.log("Error in getting User data");
				});
	}
	
	$scope.MetaFile =function(value)
	{
		       $http.get('/mdi/search/MetaData?dfd_id='+value).success(function (data) 
				 {
							$scope.metadata=data.modelList;		
							console.log("Hello"+data);
				 }).
						  error(function(data, status, headers, config) {
							        	console.log("Error in getting User data");
				});
	}
	function getAllData()
    {
	//get Departments
	var response = $http.get(urlBase+'/metadata/getAllDepartments');
	response.success(function(data, status, headers, config) {		
		console.log("== GET LOOK UP MASTER ==");
		console.log(data);
		$scope.departments=data;
		
	});
	response.error(function(data, status, headers, config) {
		console.log("Error");
	});
    };
    
    
    $scope.getSubDept = function(value)
	{
    	//alert(value);
    	var response = $http.get(urlBase+'/metadata/getSubDept',{params:{'dept_id':value}});
    	response.success(function(data, status, headers, config) {		
    		console.log("== GET Sub Department MASTER ==");
    		console.log(data);
    		$scope.subdepartments=data;
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};
	
	$scope.getdocumentTypes = function(value)
	{
		debugger
		$scope.subdept_id=value;
		//alert(value);
    	var response = $http.get(urlBase+'/metadata/getindexfields',{params:{'sub_dept_id':value}});
    	response.success(function(data, status, headers, config) 
    	{		
    		console.log("== GET Index Fields MASTER ==");
    		console.log(count);
    		console.log(data);
    		$scope.indexDataList=data;
    		count= count+1;
    		$scope.getDocumentTypes(value);
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
    	
    	
    	
    	
    	
	};
	
	
	$scope.getindexFields =function(dt_id)
	{
		/**** index field names****/
		var response = $http.get(urlBase+'/search/indexfieldname',{params:{'dt_id':dt_id}});
    	response.success(function(data, status, headers, config) 
    	{
    		debugger
    		console.log(data+" durgedra");
    		$scope.if_names=data;
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
    	
    	$scope.doument_type_data (dt_id); ///get complete data of document type
    	$scope.getfolders(dt_id); //get folders
    	
	}
	
	$scope.getfielddata = function(value)
	{
		debugger
		//alert(value);
		$scope.subdept_id=value;
    	var response = $http.get(urlBase+'/metadata/getfielddata',{params:{'if_id':value}});
    	response.success(function(data, status, headers, config) {
    		console.log("== GET Field Data MASTER ==");
    		console.log(data);
    		$scope.fieldsdata.push(data);
    		
    		
    		
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};

	$scope.getIndexData= function(index)
	{
		
		$scope.index_field = $scope.indexDataList[index];
		$scope.updateindexfield.if_type=$scope.index_field.if_type;
		$scope.updateindexfield.if_id =$scope.index_field.if_id;
	};
	
	
	
	$scope.updateIndexData= function()
	{
		$scope.updateindexfield.if_name=$scope.index_field.if_name;
		$http.post(urlBase+'/metadata/updatemetadata',$scope.updateindexfield).success(function (data) 
		{
            if(data.response=="TRUE")
            {
            	bootbox.alert("Index Field Updated Successfully!");
            	$('#user_Modal').modal('hide');	
            	$scope.getindexfields($scope.subdept_id);
            }
            else
            {
            	bootbox.alert("Index Field Not Updated Successfully!");
            }
            
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting User data");
        });
	};
	
	$scope.deleteIndexField = function(value)
	{
		$scope.index_field = $scope.indexDataList[value];
		
		var id =$scope.index_field.if_id;
		$http.post(urlBase+'/metadata/deletemetadata',id).success(function (data) 
				{
		            if(data.response=="TRUE")
		            {
		            	bootbox.alert("Index Field Deleted Successfully!");
		            	$('#user_Modal').modal('hide');
		            	$scope.getindexfields($scope.subdept_id);
		            }
		            else
		            {
		            	bootbox.alert("Index Field Not Deleted Successfully!");
		            }
		            
		        }).
		        error(function(data, status, headers, config) {
		        	console.log("Error in getting User data");
		        });
	};
	
	$scope.addIndexData=function(value){
		var dpt_id = $scope.folderform.dept_id;
		var sub_dept_id =$scope.folderform.sub_dept_id;
		
		$scope.updateindexfield.if_parent= sub_dept_id;
		$http.post(urlBase+'/metadata/addmetadata',$scope.updateindexfield).success(function (data) 
				{
		            if(data.response=="TRUE")
		            {
		            	bootbox.alert("Record Added Successfully!");
		            	$scope.getindexfields($scope.subdept_id);
		            	$('#user_New_Modal').modal('hide');
		            }
		            else
		            {
		            	bootbox.alert("Record Not Added Successfully!");
		            }
		            
		        }).
		        error(function(data, status, headers, config) {
		        	console.log("Error in getting User data");
		        });
		
	};
	

	/***  in complete process  **/
	
	$scope.getIndexFieldsData = function()
	{
		$http.get(urlBase+'/metadata/getIndexFieldsFromLookup').success(function (data) 
				{
		            
		            
		        }).
		        error(function(data, status, headers, config) {
		        	console.log("Error in getting User data");
		        });
	};
	
	
	/***************  Search Content   ****************************/
	
	 $scope.lookupdata=[];
	 $scope.if_names=[];
	 $scope.fielddrop='';
	 $scope.fieldoth='';
	 $scope.columns = [{if_id: '', if_type:'', if_value:'',if_values_dropd:[],if_value_drop:'',show_if:''}];
	 
	 $scope.finalMFSearch=[];
	 $scope.dfdDetails=[];
	 
	 $scope.getType = function(value)
	 {
		 
		 var id=$scope.columns[value].if_id;
		 $scope.columns[value].if_value ='';
		 var i;
		 var type='';
		 var id_indexfield='';
		 for(i=0;i<$scope.if_names.length;i++)
		 {
			var id2=$scope.if_names[i].if_id;
			 if(id2==id)
			 {
				 var id_indexfield = id2;
				 type=$scope.if_names[i].if_type;
			 }
		 }
		 console.log(id_indexfield+''+type);
		 
		 if(type=="dropdown")
		 {
			 $scope.fielddrop='Y';
			 $scope.fieldoth='N';
			 $http.get(urlBase+'/search/getindexdropdownlookupdata',{params:{'if_id':id_indexfield}}).success(function (data) 
			  {
				 $scope.lookupdata=data;
				 
				 $scope.columns[value].if_values_dropd=$scope.lookupdata;
				 $scope.columns[value].show_if='D';
				            
			  }).
			  error(function(data, status, headers, config) {
				        	console.log("Error in getting User data");
			 });
		 }
		 if(type=="date")
		 {
			 $scope.fielddrop='Y';
			 $scope.fieldoth='N';
			
			 $scope.columns[value].show_if='DA';            
		 }
		 else
		 {
			 $scope.columns[value].show_if='O';
		  }
		 
	 }
	 
	 $scope.addNewColumn = function() 
	 {
		    $scope.columns.push({if_id: '', if_type:'', if_value:'',if_values_dropd:[],if_value_drop:'',show_if:''});
		    
	 };
	 
	 $scope.removeColumn = function(index)
	 {
		 $scope.columns.splice( index, 1);
		 if ( $scope.columns.length == 0 || $scope.columns.length == null)
		 {
		      alert('no rec');
		      $scope.columns.push({if_id: '', if_type:'', if_value:'',if_values_dropd:[],if_value_drop:'',show_if:''});
		  }
	 }
	 
	 
	 $scope.total_count='';
	$scope.searchByMetaFields = function(dept_id,sub_dept_id,dt_id,fd_id)
	{
		
		//alert($scope.md_year);
		$scope.finalMFSearch.splice(0,$scope.finalMFSearch.length)
		var i;
		for (i=0;i<$scope.columns.length;i++)
		{
			var id =$scope.columns[i].if_id;
			var value =$scope.columns[i].if_value;
			$scope.finalMFSearch.push({ if_id:id ,if_value : value });
			//alert($scope.columns[i].if_value);
			
		}
		if(sub_dept_id==undefined){
			sub_dept_id=0;
		}
		
		if(dt_id==undefined){
			dt_id=0;
		}
		
		if($scope.md_year ==null || $scope.md_year== undefined)
			{
			   $scope.md_year=0;
			}
       var url = '/mdi/search/metadataForSearch?dept_id='+dept_id+'&sub_dept_id='+sub_dept_id+'&dt_id='+dt_id+'&md_year='+$scope.md_year+'&fd_id='+fd_id;
		
		// prepare headers for posting
		var config = {
                headers : {
                	'Content-Type': 'application/json',
                	'Accept': 'text/plain'
                }
        }
		
		
		var dataArr = $scope.finalMFSearch;
		/*var dept_idv =dept_id;
		var dept_sub=sub_dept_id;*/
		$http.post(url, dataArr, config).then(function (response) 
		{
			$scope.dfdDetails=response.data.modelList;
			
			$scope.total_count = $scope.dfdDetails.length;
			if($scope.total_count== null)
				{
				   $scope.total_count = '0';
				}
			console.log(response.data.modelList);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	}
	
	$scope.getIndexofDocList= function(index)
	{
		var id = $scope.dfdDetails[index].dfd_id;
		 $http.get(urlBase+'/search/viewFile?dfd_id='+id).success(function (data) 
		 {
					console.log(data);
					$scope.fileurl='/mdi/uploads/'+data.data;
					            
		 }).
				  error(function(data, status, headers, config) {
					        	console.log("Error in getting User data");
				 });
	}
	
	
	$scope.closeDiv = function()
	{
		$('#user_Modal').modal('hide');	
	}
	
	
	$scope.getSubDept =function(index)
    {
		var response = $http.get(urlBase+'/UploadDcoumet/getSubDept',{params:{'dept_id':index}});
    	response.success(function(data, status, headers, config) {		
    		console.log("== GET LOOK UP MASTER ==");
    		console.log(data);
    		$scope.subdepartments=data;
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
    }
	
	$scope.getDocumentTypes = function (sub_dept_id)
	{
		var response = $http.get(urlBase + '/UploadDcoument/getDocumentType',
		{
					params : 
					{
						'sub_dept_id' : sub_dept_id
					}
		});
		response.success(function(data, status, headers, config) 
		{
			console.log("== GET Document Type MASTER ==");
			console.log(data);
			$scope.documentTypeList = data.modelList;
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	
	
	$scope.temp_doc_type_data={};
	$scope.show_msg = 'PLEASE SELECT DOCUMENT TYPE';
	$scope.show_title="Help";
	$scope.doument_type_data = function(dt_id)
	{
		var i= 0;
		for(i=0;i<$scope.documentTypeList.length;i++)
			{
			   if(dt_id==$scope.documentTypeList[i].dt_id)
				   {
				       $scope.temp_doc_type_data = $scope.documentTypeList[i];
				   }
			}
		   if($scope.temp_doc_type_data.dt_name=='DIPLOMA')
			{
			    $scope.show_msg = 'PLEASE ENTER YEAR OF PASSING';
			    $scope.show_title=$scope.temp_doc_type_data.dt_name;
			}
		   else  if($scope.temp_doc_type_data.dt_name=='TRANSCRIPT')
			{
			    $scope.show_msg = 'PLEASE ENTER YEAR OF PASSING';
			    $scope.show_title=$scope.temp_doc_type_data.dt_name;
			}
		   else   if($scope.temp_doc_type_data.dt_name=='REGISTRATION FORM')
			{
			    $scope.show_msg = 'PLEASE ENTER YEAR OF JOINING';
			    $scope.show_title=$scope.temp_doc_type_data.dt_name;
			}
		   else
			   {
			       $scope.show_msg = 'YEAR IS NOT MANDETORY';
			       $scope.show_title=$scope.temp_doc_type_data.dt_name;
			   }
		   
		   
	};
	
	$scope.getfolders = function(dt_id)
	{
		var response = $http.get(urlBase + '/UploadDcoument/getFolders',
				{
							params : 
							{
								'dt_id' : dt_id
							}
				});
				response.success(function(data, status, headers, config) 
				{
					console.log("== GET Folder Type MASTER ==");
					console.log(data.modelList.length)
					$scope.foldersList= data.modelList;
				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
	};
});