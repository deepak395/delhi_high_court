var DocumentApp = angular.module("mdiApp", ['ui.bootstrap','ng-bootstrap-datepicker','smart-table','ngSanitize', 'ngCsv']);


DocumentApp.controller("QCReportCtl",function($scope, $http,$filter,$document) {
	
	$scope.buttonDisabled=false;
	$scope.departments=[];
	$scope.MetaDataList=[];
	$scope.reportListData=[];
	$scope.documentfileList=[];

	var baseUrl="/mdi/";
	
	getAllDepartment();
	$scope.today = function() {
	    $scope.dt = new Date();
	};
	$scope.today();
	
	$scope.clear = function () {
		$scope.dt = null;
  	};	
	
	$scope.toggleMax = function() {
	    //$scope.minDate = $scope.minDate ? null : new Date();
		$scope.maxDate = new Date();
	};
	$scope.toggleMax();
	
	$scope.open = function($event,type) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    if(type=="fromDate")
	    	$scope.fromDate= true;
	    if(type=="toDate")
	    	$scope.toDate= true;
	};
	
	$scope.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1
	    
	};
	
	$scope.dailyreportListData=[];
	$scope.dailyreportList = 
	{						
							'parameter1':'File Name',
							'parameter2':'Department',
							'parameter3':'Sub Department',
							'parameter4':'Date'
	};
	$scope.dailyreportListData.push($scope.dailyreportList);
	$scope.formats = ['dd-MMMM-yyyy','dd-mm-yyyy', 'yyyy/MM/dd', 'dd-MM-yyyy', 'shortDate'];
	$scope.format = $scope.formats[3];

	function convertDate(inputFormat) 
	{
		  function pad(s) { return (s < 10) ? '0' + s : s; }
		  var d = new Date(inputFormat);
		  return [ d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-');
	}
	function getAllDepartment()
    {
	//get Departments
	var response = $http.get(baseUrl+'/UploadDocument/getAllDepartments');
	response.success(function(data, status, headers, config) {		
		console.log("== GET Departments MASTER ==");
		console.log(data);
		$scope.departments=data;
		
	});
	response.error(function(data, status, headers, config) {
		console.log("Error");
	});

 };
 $scope.getFiles=function(dept_name){
		//alert("1"+ib_bundle +"-----------satge"+stage);
		$http.get(baseUrl+'/report/getDepartmentwiseDocument',{params: {'dept_name': dept_name}}).success(function(data) {	
			$scope.documentfileList = data.modelList;
			if(data.modelList=""){
				bootbox.alert("<B>Something is wrong!!!");
			}
		}).error(function(data, status, headers, config) {
			console.log("Error in getting DailyReportData ");
		});
 }
/* $scope.ViewFile = function(index)
	{
	
		alert(index);
		//var doc_id=$scope.documentfiledetails[index].dfd_id;
		window.open(baseUrl+'/document/File_Meta?id='+index);
	}*/
 
 $scope.viewDfdData={};
	$scope.ViewFile =function(value)
	{
	 debugger
		        $http.get('/mdi/search/viewFile?dfd_id='+value).success(function (data) 
				 {
							console.log(data);
							$scope.fileurl='/mdi/uploads/'+data.data;
							$scope.viewDfdData=data.modelData;
                           var url='/mdi/document/open?dfd_id='+value;
							
							
							window.open(url, '_blank');
							            
				 }).
						  error(function(data, status, headers, config) {
							        	console.log("Error in getting User data");
				});
	}
$scope.getQCData = function() 
{		
	var btn = $("#submit");
	$scope.buttonDisabled = true;
	$scope.displayedCollection=[];
	$scope.UploadedList=[];
	
	if($scope.model.fromDate!=null){
		$scope.model.fromDate=convertDate($scope.model.fromDate);
	}
	else{
		bootbox.alert("<B>Select From Date....!!!");
	}
	if($scope.model.toDate!=null){
		$scope.model.toDate=convertDate($scope.model.toDate);
	}
	else{
		bootbox.alert("<B>Select To Date....!!!");
	}
	if($scope.dept_id == null){
		$scope.dept_id = null;
	}
		
	

		var date = new Date($scope.fromdate);
		var date1=new Date($scope.todate);
			$http.get(baseUrl+'/Reports/getQCData',{params: {'dept_id':$scope.dept_id,
				'fromdate':$scope.model.fromDate,'todate':$scope.model.toDate}}).success(function(data) {
								$scope.buttonDisabled = false;	
								$scope.MetaDataList=data.modelList;	
								console.log($scope.MetaDataList);
								if(data.modelList=="")
									{
									bootbox.alert("<B>Inavalid Department !!!");
									}
												
								for(var i=0;i<data.modelList.length;i++)
								{
									var date2=new Date(parseInt(data.modelList[i][4]));
								  var date123 = $filter('date')(new Date(date2), 'dd/ MM /yyyy');
								
									$scope.dailyreportList = {
											'parameter1':data.modelList[i][1],
											'parameter2':data.modelList[i][2],
											'parameter3':data.modelList[i][3],
											'parameter4':date123
															};
															
									$scope.dailyreportListData.push($scope.dailyreportList);
									console.log(data.modelList[i][0]);	
								};
					}).error(function(data, status, headers, config) {
							console.log("Error in getting search Bundle number");
				});
	
		};
});

//date format
CartonApp.filter('dateFormat1', function($filter)
		{
		 return function(input)
		 {
		  if(input == null){ return ""; } 
		 
		  var _date = $filter('date')(new Date(input), 'dd/ MM /yyyy ');
		 
		  return _date.toUpperCase();

		 };
		});



