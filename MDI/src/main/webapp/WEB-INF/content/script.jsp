	<script
			src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/bootstrap.min.js"></script>
	<script  src="${pageContext.request.contextPath}/js/angularJs/angular.min.js"></script>
	<script  src="${pageContext.request.contextPath}/js/angularJs/moment.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/ui-bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/angularJs/angular-bootstrap-datepicker.js" charset="utf-8"></script>
	<script  src="${pageContext.request.contextPath}/js/angularJs/checklist-model.js"></script>
	<script  src="${pageContext.request.contextPath}/js/bootstrap/bootbox.min.js"></script>
	<script src="${context}/mdi/js/angularJs/angular-idle.min.js"></script>
	
	
	<!-- ================== BEGIN BASE JS ================== -->	
		<script src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	
		<!--[if lt IE 9]>
			<script src="assets/crossbrowserjs/html5shiv.js"></script>
			<script src="assets/crossbrowserjs/respond.min.js"></script>
			<script src="assets/crossbrowserjs/excanvas.min.js"></script>
		<![endif]-->
		<script src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="${pageContext.request.contextPath}/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<!-- ================== END BASE JS ================== -->
		
		
		<!-- ================== BEGIN PAGE LEVEL JS ================== 
		<script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
		<script src="assets/js/ui-modal-notification.demo.min.js"></script>
		<script src="assets/js/apps.min.js"></script>
		 ================== END PAGE LEVEL JS ================== -->
		 
<link rel="stylesheet" href="${context}/mdi/assets/css/font-icons/entypo/css/entypo.css">