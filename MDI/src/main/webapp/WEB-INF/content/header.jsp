<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mdi.model.User"%>	 
<%@ page import="com.mdi.model.ObjectMaster"%>		
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="mdiApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDMS</title>

 <%@ include file="style.jsp"%>
<%@ include file="script.jsp"%>
</head>
<body>
<%

%>
<%-- <% 
User user = null;
if(session.getAttribute("USER")!=null)
	 user = (User)session.getAttribute("USER");

%> --%>
<!-- begin #header -->
		 <div id="header" class="header navbar navbar-default navbar-fixed-top">
		 <!-- <div id="header" ng-controller="editProfileCtrl" class="header navbar navbar-default navbar-fixed-top"> --> 
			<!-- begin container-fluid -->
			<div class="container-fluid">
			<div class="row">
				<!-- begin mobile sidebar expand / collapse button -->
				
				<div class="col-md-1">
				<div class="row">
                    <!-- <img  class="navbar-brand" style="width: 150px; height: 65px;"  alt="" src="/dms/assets/img/highcourt1.png"></img> -->
                    <!-- <img  class="navbar-brand" style="width: 170px; height: 74px; margin-left: -12px; padding: 0px 26px;"  alt="" src="/dms/assets/img/highcourt1.png"></img> -->
                    
                </div>
                </div>
				
				<div class="col-md-1" style="padding: 19px 0 0 16px;">
				<a class="" href="${pageContext.request.contextPath}/user/dashBoard"  style="padding: 0px 18px;">  <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home </a>
				</div>
				
				<div class="col-md-1" style="float:right; padding: 19px 0 0 16px;">
				<li><a class="btn btn-primary btn-xs" style="padding 250px;" href="${pageContext.request.contextPath}/mdi/logout">Log Out</a></li>
				</div>
				
		              <%--    <div class="modal fade" id="pass_Modal" tabindex="-1"
								role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header" style="background-color: black;">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<h4 class="modal-title" id="myModalLabel">
												<strong style="color: #FBFCFD;">Change Password </strong>
											</h4>
										</div>
										<%@ include file="../user/passwordPage.jsp"%>
									</div>
								</div>
							</div>
				 --%>
				
				
				
				<%-- <div class="col-md-4">
				<div class="collapse navbar-collapse pull-left" id="top-navbar">
                    <ul class="nav navbar-nav"  style="margin:8.5px -16px;">
							<% 
							List<ObjectMaster> parent_list  = (List<ObjectMaster>)session.getAttribute("ob_list");
							 List<ObjectMaster> child_list  = (List<ObjectMaster>)session.getAttribute("ob_list");
							 
							 for(int i=0;i<parent_list.size();i++)
							 {
								 ObjectMaster om = parent_list.get(i);
								// System.out.println(om.getOm_object_link()+" "+om.getOm_object_stages());
								if(om.getOm_object_stages()==0)
								{
									%>
									
										 <li >
			                            <a href="<%=om.getOm_object_link() %>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-th-large fa-fw"></i> <%=om.getOm_object_name() %> <b class="caret"></b></a>
			                            
			                        	 <ul class="dropdown-menu" role="menu">		 <%
									 for(int j=0;j<child_list.size();j++)
									 {
										
										 ObjectMaster ch_om = child_list.get(j);
										 
										 if(om.getOm_id()==ch_om.getOm_parent_id())
										 {
											 //add child
											 %>
											 <li><a href="${pageContext.request.contextPath}/<%=ch_om.getOm_object_link() %>"> <%=ch_om.getOm_object_name() %> </a></li>
											 <%
										 }
									 }
			                        	 %>	 </ul>		 </li> <%
								}
							 }
							%>
				 </ul>
                </div>		
				</div> --%>
				
               
              
                   
			 	<div class="col-md-4">
			 	
                <div id="top-navbar">
                    <!-- <img class="navbar-brand" style="margin-right:0px;height:1%;line-height:0;margin-left: 0;" alt="" src="/dms/assets/img/StockHolding-Logo.png"></img> -->
<!--                    <img class="navbar-brand" style="margin-right:-15px;height:55px;line-height:30px;margin-left: 30px;" alt="" src="/dms/assets/img/StockHolding-Logo.png"></img>
 -->                   
                </div>
<!--                 <div class="navbar-header" style="margin: 2px 60px; "> -->
<!--                                         <a href="#" class="navbar-brand text-right" style="padding: 18px 20px;margin:-36px 0px;"><span class="navbar-logo"></span>	 -->
<!--                                         <span>Document Management System</span> -->
<!-- 										</a> -->
<!--                                         <button type="button" class="navbar-toggle" data-click="sidebar-toggled"> -->
<!--                                                 <span class="icon-bar"></span> -->
<!--                                                 <span class="icon-bar"></span> -->
<!--                                                 <span class="icon-bar"></span> -->
<!--                                         </button> -->
<!--                  </div> -->
				</div>
				<!-- begin header navigation right -->
				
				<%-- <div class="col-md-2">
				<ul class="nav navbar-nav navbar-right">
					<!-- <li>
						<form class="navbar-form full-width">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Enter keyword">
								<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
							</div>
						</form>
					</li> -->
					
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<img src="${pageContext.request.contextPath}/assets/img/user-13.jpg" alt=""> 
							<span class="hidden-xs"><%=user.getUm_fullname() %> (<%=user.getUserroles().get(0).getLk().getLk_longname() %>) </span> <b class="caret"></b>
							
							<span ><b><%=user.getLk_benchcode().getLk_longname() %> </b></span>
							
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							  <li><a href="${pageContext.request.contextPath}/user/edit">Edit Profile</a></li> 
							<li class="divider"></li>
							<li><a href="${pageContext.request.contextPath}/dms/logout">Log Out</a></li>
						</ul>
					</li>
				</ul>
			
				</div> --%>
				<!-- end header navigation right -->
				</div>
			</div>
			<!-- end container-fluid -->
		</div>
	   	
		
</br>
</br>
</br>


			