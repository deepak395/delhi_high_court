<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ include file="../../content/header.jsp"%>
</head>
<body class="page-body skin-facebook" data-url="http://neon.dev" ng-controller="menuPermissioncontroller">

<%@include file="../../content/sideheader.jsp" %>

    <div class="page-container sidebar-collapsed">
      
        <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="../Images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <li>Welcome : <%= user.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           	<div class="row">
                <div class="col-md-12">
                <div class="panel panel-primary" style="border-color: #c2c9dc;">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Menu Permission</div>
            
                  <!--       <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
                    </div>
                    <div class="panel-body">
                    <form id="documentupload">
                        <div class="row">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-3">
                                    <label id="metadata" name="metadata">Select User</label>
                                </div>
                                <div class="col-md-3">
                                              <select
												ng-options="rList.lk_id as rList.lk_longname for rList in roleList"
												class="form-control" name="lk_id" id="lk_id"
												ng-model="model.lk_id" ng-change="getMDIRoleList()">
											</select>
                                 
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-12">
                                    <div id="page-wrap">
                                    	<!-- <h2>Access Right</h2>-->
                                         <div id="hidden_div">
                                         	<table id="data-table" st-table="displayedCollection"
							st-safe-src="masterdata"
							class="table table-striped table-bordered nowrap table-hover"
							width="100%">
							<thead>
							</thead>
							<tbody>
								<tr>
									<treecontrol class="tree-classic" tree-model="tree_data"
									icon-leaf="icon-file" icon-expand="icon-plus-sign"
									icon-collapse="icon-minus-sign" options="treeOptions"
									selected-nodes="selectedNodes" ng-model="model.ro_om_mid"
									on-selection="showSelected(node, selected)">
								{{node.name}} <input id="{{node.name}}" type="checkbox"
									value="{{node.name}}" ng-model="node.checkbox"
									ng-click="toggleSelection(node)" /> <label
									for="{{node.name}}"></label> </treecontrol>
								</tr>
							</tbody>
						</table>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-6" style="text-align:right;">
                                	<input type="submit" id="btnsubmit" value="SUBMIT"  ng-click="save(tree_data,model)" class="btn btn-primary btn-blue"/>
                                  </div> 
                                <div class="col-md-6">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" onclick="goBack()"/>
                                    
                                </div>
                            </div>
                        </div>
            		</form>
                    </div>
                </div>
            </div>
            </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>

    <div class="modal fade" id="modal-ChangePassword" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>

                <div id="divBodyPE" class="modal-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label for="lblOldPassword" class="col-sm-3 control-label">Old Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtOldPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblNewPassword" class="col-sm-3 control-label">New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtNewPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblConfirmPassword" class="col-sm-3 control-label">Confirm Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtConfirmPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="lblErrorPassword" style="display:none;color:red;"></label>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="button" id="btnChangePassword" class="btn btn-info" value="Change" />
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-PasswordSent" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Client Tariff Master</h4>
                </div>

                <div id="divBodySent" class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" id="btnOk" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
    
   


    
<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/menuPermissionCtrl.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/ui-bootstrap-tpls.0.11.2.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/angular-tree-control.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/tree-grid-directive.js"></script>

<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/tree-control.css'>
<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/tree-control-attribute.css'>	
<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/treeGrid.css'>

</body>
</html>