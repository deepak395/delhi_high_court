<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="mdiApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>

<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/DashBoardController.js"></script>
<%@ page import="com.mdi.model.User"%>	
<%@page import="com.mdi.model.UserRole"%>
<%@ page import="java.util.List" %>
<%@ include file="../../content/header.jsp" %> 
</head>
<body class="page-body skin-facebook" data-url="http://neon.dev" ng-controller="dashboardcontroller"
 ng-init="setUserId(${USER.getUserroles().get(0).ur_role_id },${USER.um_id })">
<% 
User user1 = null;
     if(session.getAttribute("USER")!=null)
	 user1 = (User)session.getAttribute("USER");
     Long role_id = null;
     try
     {
         role_id  = user1.getUserroles().get(0).getUr_role_id();
     }
     catch(Exception ex)
     {
    	 ex.printStackTrace();
     }
     System.out.println(role_id+"   durgend5ra");

%>
<%-- <% 
				List<UserRole> userroles=user1.getUserroles();
				Boolean dataentryop=false;
				UserRole userr=null;
				for(UserRole userrole:userroles){
					 userr=userrole;
				}
%> --%>

  <%@include file="../../content/sideheader.jsp" %>
        <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context}/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#024781;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <!-- Profile Info -->
                        <li>Welcome : <%= user1.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-sm-3 col-xs-6">
        
                    <div class="tile-stats tile-red" style="    height: 123px;">
                        <div class="icon"><i class="entypo-users"></i></div>
                        <%if((role_id.equals(350992l))) {%>     <!--  uploader  -->
                        <h3>Uploaded Document Count</h3>
                        <% } %>
                        <%if((role_id.equals(350994l))) {%>   <!--  meta data  -->
                        <h3>MetaData Entry Count</h3>
                        <% } %>
                        <%if((role_id.equals(350991l))) {%>   <!-- admin -->
                        <h3>Uploaded Document Count</h3>
                        <%} %>
                        <%if((role_id.equals(350993l))) {%>   <!-- checker -->
                        <h3>Checked Document Count</h3>
                        <%} %>
                        <%if((role_id.equals(1L) || role_id.equals(11L)) || role_id.equals(10000580L)) {%>   <!-- checker -->
                        <h3>Uploaded Document Count</h3>
                        <%} %>
                        <div class="num" style="margin-top: 10px;">
                            <table>
                                <tr ><!-- ng-if="masterdata[0].flag1" -->
                                    <td style="color:white;">
                                       PDF
                                    </td>
                                    <td style="padding-left:150px;">
                                        {{doc_count}}
                                    </td>
                                </tr>
                            </table>
        
                        </div>
        
                       
                       <!--  <div class="num" style="margin-top: 10px;">
                            <table>
                                <tr  ng-if="masterdata[0].flag1" >
                                    <td style="color:white;">
                                        PDF
                                    </td>
                                    <td style="padding-left:150px;">
                                        {{masterdata[0].value1}}
                                    </td>
                                </tr>
                            </table>
        
                        </div> -->
                        
                    </div>
        
                </div>
        <%if(role_id !=350992l && role_id !=350993l && role_id !=350994l)
        	{
        	%>
        	
                <div class="col-sm-3 col-xs-6">
                    <div class="tile-stats tile-green" style="    height: 123px;">
                        <div class="icon"><i class="entypo-users"></i></div>
                        <h3>Viewed Document Count</h3>
                        <div class="num" style="margin-top: 10px;">
                             <table>
                                <tr ><!-- ng-if="masterdata[0].flag1" -->
                                    <td style="color:white;">
                                       PDF
                                    </td>
                                    <td style="padding-left:150px;">
                                        {{showmostviewdocdata}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
        
                </div>
        <%
        	
        	}
        %>
                <!-- <div class="col-sm-3 col-xs-6">
        
                    <div class="tile-stats tile-aqua">
                        <div class="icon"><i class="entypo-users"></i></div>
                        <h3>Download Document</h3>
                        <div class="num" style="margin-top: 10px;">
                            <table>
                                <tr>
                                    <td style="color:white;">
                                        PDF
                                    </td>
                                    <td style="padding-left:150px;">
                                        25
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:white;">
                                        Word
                                    </td>
                                    <td style="padding-left:150px;">
                                        30
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:white;">
                                       Excel
                                    </td>
                                    <td style="padding-left:150px;">
                                        05
                                    </td>
                                </tr>
                            </table>
        
                        </div>
                    </div>
        
                </div> -->
        
                <div class="col-sm-3 col-xs-6">
        
                    <div class="tile-stats tile-blue">
                        <div class="icon"><i class="entypo-users"></i></div>
                        <h3>Last Login</h3>
                        <div class="num" style="margin-top: 10px;">
                            <table>
                               <tr>
                                    <td style="color:white;">
                                        NAME
                                    </td>
                                    <td style="padding-left:100px;">
                                        ${USER.username }
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:white;">
                                        Date
                                    </td>
                                    <td style="padding-left:100px;">
                                        ${date}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:white;">
                                       Time
                                    </td>
                                    <td style="padding-left:100px;">
                                        ${Time }
                                    </td>
                                </tr>
                            </table>
        
                        </div>
                    </div>
        
                </div>
    		</div>
            <hr>
            <div class="row">
                <div class="col-md-6">
            
                <div class="panel panel-primary" style="border-color: #c2c9dc;" ng-if="showRDUdiv"> <!-- code -->
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div   class="panel-title" style="font-weight:bold;">Documents uploaded</div>
                      
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile-7">
                                <table id="myTable1" class="table table-bordered table-responsive" st-table="displayedCollection" st-safe-src="recentdata"  width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                Sub Department
                                            </th>
                                            <th>
                                                Department
                                            </th>
                                            <th>
                                                File Count
                                            </th>
            
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                            <tr ng-repeat="dfd in displayedCollection | orderBy: 'parameter1'" class="odd gradeX">
                                                <td>
                                                   {{dfd.parameter1}}
                                                </td>
                                                <td>
                                                    {{dfd.parameter2}}
                                                </td>
                                                <td>
                                                    {{dfd.parameter3}}
                                                </td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                            <tr>
                                                 <td colspan="6" class="text-center">
                                                   <div st-pagination="" st-items-by-page="5"
                                                      st-displayed-pages="8">
                                                   </div>
                                                 </td>
                                            </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
                <div class="col-md-6" >
            
                <div class="panel panel-primary" style="border-color: #c2c9dc;" ng-if="showMVDdiv">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Most viewed document</div>
            
                    </div>
                    <div class="panel-body" style=" margin-bottom: 200px;">
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile-7">
                                <table id="myTable1" class="table table-bordered table-responsive" st-table="displayedCollection" st-safe-src="mostviewed"  width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                Document Name
                                            </th>
                                            <th>
                                                Document Department
                                            </th>
                                            <th>
                                                Uploaded Date
                                            </th>
            
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                            <tr ng-repeat="dfd in displayedCollection" class="odd gradeX">
                                                <td>
                                                   {{dfd.dfd_name}}
                                                </td>
                                                <td>
                                                    {{dfd.departments.dept_name}}->{{dfd.subdepartments.sub_dept_name}}
                                                </td>
                                                <td>
                                                    {{dfd.dfd_cr_date | date:medium}}
                                                </td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                            <tr>
                                                 <td colspan="6" class="text-center">
                                                   <div st-pagination="" st-items-by-page="5"
                                                      st-displayed-pages="8">
                                                   </div>
                                                 </td>
                                            </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
            </div>
            
            <hr>
            <div class="row">
                <div class="col-md-12">
            
                <div class="panel panel-primary" style="border-color: #c2c9dc;" ng-if="showdiv">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div  ng-if="showRUFdiv" class="panel-title" style="font-weight:bold;">Recent UPLOADED Files</div>
                        <div  ng-if="showRMEdiv" class="panel-title" style="font-weight:bold;">Recent META DATA ENTERED Files</div>
                        <div  ng-if="showCHFdiv" class="panel-title" style="font-weight:bold;">Recent CHECKED Files</div>
                        
                     
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile-7">
                                <table id="myTable1"  class="table table-bordered table-responsive" st-table="displayedCollection" st-safe-src="recentdata"  width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                Document Name
                                            </th>
                                            <th>
                                                Document Department
                                            </th>
                                            <th>
                                                Uploaded Date
                                            </th>
            
                                        </tr>
                                    </thead>
                                    <tbody>
                                              <tr ng-repeat="dfd in displayedCollection" class="odd gradeX">
                                                <td>
                                                   {{dfd.parameter1}}
                                                </td>
                                                <td>
                                                    {{dfd.parameter3}}->{{dfd.parameter4}}
                                                </td>
                                                <td>
                                                    {{dfd.parameter2}}
                                                </td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                            <tr>
                                                 <td colspan="6" class="text-center">
                                                   <div st-pagination="" st-items-by-page="5"
                                                      st-displayed-pages="8">
                                                   </div>
                                                 </td>
                                            </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
            </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>

    <div class="modal fade" id="modal-ChangePassword" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>

                <div id="divBodyPE" class="modal-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label for="lblOldPassword" class="col-sm-3 control-label">Old Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtOldPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblNewPassword" class="col-sm-3 control-label">New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtNewPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblConfirmPassword" class="col-sm-3 control-label">Confirm Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtConfirmPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="lblErrorPassword" style="display:none;color:red;"></label>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="button" id="btnChangePassword" class="btn btn-info" value="Change" />
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-PasswordSent" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Client Tariff Master</h4>
                </div>

                <div id="divBodySent" class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" id="btnOk" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
    
              <div class="modal fade" id="pass_Modal" tabindex="-1"
								role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header" style="background-color: black;">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<h4 class="modal-title" id="myModalLabel">
												<strong style="color: #FBFCFD;">Change Password </strong>
											</h4>
										</div>
										<%@ include file="../user/passwordPage.jsp"%> 
									</div>
								</div>
	</div>
							
							
</body>
</html>