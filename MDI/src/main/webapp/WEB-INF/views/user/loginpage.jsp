<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MDI</title>
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/styles-login.css">
    <link rel="stylesheet" href="css/util.css">
	<link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="assets/animate/animate.css">
    <link rel="stylesheet" href="assets/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/select2/select2.min.css">
    <script src="../assets/js/bootstrap.js"></script>
</head>
<body style="background-image:url(images/MDI-Gurgaon.jpg);margin:0px;">
<div>
<div style="width:100%;margin:0 auto;float:left;clear:none;">
	<div id="header">
    	<div style="width:100%;height:90px;border-bottom:1px solid #000000;background-color:#ffffff;">
        	<div style="float:left;width:420px;"><img src="images/Dms-Logo.png" /></div>
            <div style="padding-top:30px;font-size:22px;color:#024488;font-weight:600;letter-spacing: 1pt;font-family:Arial;">Electronic Document Management Solution</div>
        </div>
    </div>
    <div id="container">
		
		<form:form action="userlogin" method="post" commandName="user" style="margin-top: 163px;">
		
		
		<div><font color="red"><c:out value="${message}"></c:out></font></div>
		
		
        <!-- <div class="login100-form-avatar">
			<img src="images/SHCIL-logo.png" alt="AVATAR">
		</div>
        <span class="login100-form-title p-t-20 p-b-25">
			STOCKHOLDING DMS
		</span> -->
        
        <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
            <!-- <input class="input100" type="text" name="username" placeholder="Username"> -->
            <form:input class="input100" path="username"  placeholder="Username" />
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-user"></i>
            </span>
        </div>

        <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
            <!-- <input class="input100" type="password" name="pass" placeholder="Password"> -->
            <form:password  class="input100" path="password" placeholder="Password" />
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-lock"></i>
            </span>
        </div>
         <span class="login100-form-title p-t-5 p-b-4" style="text-align:left;padding-left:20px;">
			<img id="captcha_id" name="imgCaptcha" src="captcha.jpg">
		
		<a href="javascript:;" title="change captcha text"
                        onclick="document.getElementById('captcha_id').src = 'captcha.jpg?' + Math.random();  return false">
                            <img src="images/refresh.png" />
         </a>
		</span>
		
		
        <div class="wrap-input100 validate-input m-b-10" data-validate = "captcha is required" >
            <input class="input100" type="text" name="captcha" placeholder="Captcha" autocomplete="off">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-code"></i>
            </span>
        </div>

        <div class="container-login100-form-btn p-t-10">
            <button class="login100-form-btn">
                Login
            </button>
        </div>

        <div class="text-center w-full p-t-25 p-b-230">
            <a href="./forgetPassword" class="txt1">
            Forgot Username / Password?
                <!-- Forgot Username / Password? -->
            </a>
        </div>
		
		</form:form>
		
	</div>
</div>
    
 

<!-- <script language="javascript">
function check(form)
{

if(form.userid.value == "Roseindia" && form.pwd.value == "Roseindia")
{
	return true;
}
else
{
	alert("Error Password or Username")
	return false;
}
}
</script> -->
</div>
</body>

</html>