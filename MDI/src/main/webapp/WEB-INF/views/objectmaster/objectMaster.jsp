<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ include file="../../content/header.jsp"%>


<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
</head>
<body class="page-body skin-facebook" data-url="http://neon.dev" ng-controller="objectMasterCtrl">

<%@include file="../../content/sideheader.jsp" %>
{{test}}
    <div class="page-container sidebar-collapsed">
        <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
      
        <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="../Images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">MDI </h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <li>Welcome : <%= user.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           	<div class="row">
                <div class="col-md-12">
                <div class="panel panel-primary" style="border-color: #c2c9dc;">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Object Master</div>
            
                        <!-- <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
                    </div>
                    <div class="panel-body">
                  <form class="form-horizontal reduce-gap" name="addobjectForm"
					novalidate role="form">
					<div ng-show="errorlist.length!=0"
						class="alert alert-block alert-danger">
						<ul>
							<span ng-repeat="errors in errorlist"> <span
								ng-repeat="n in errors track by $index">
									<li>{{(n)}}</li>
							</span>
							</span>
						</ul>
					</div>
					<div class="panel-body" style="float: left; width: 50%;">
						<table id="data-table" st-table="displayedCollection"
							st-safe-src="masterdata"
							class="table table-striped table-bordered nowrap table-hover"
							width="100%">
							<thead>
							</thead>
							<tbody>
								<tr>
									<treecontrol class="tree-classic" tree-model="tree_data"
										on-selection="showSelected(node)" icon-leaf="icon-file"
										icon-expand="icon-plus-sign" icon-collapse="icon-minus-sign">
									{{node.om_object_name}} 
								</tr>
							</tbody>
						</table>
					</div>
					<div>
						<div id="content" class="content" style="width: 150%;">
							<div class="row">
								<div class="col-md-4 ui-sortable">
									<div data-sortable-id="form-validation-1">

										<div class="form-group">
											<label class="control-label col-md-4 col-sm-4"
												for="om_object_name">Object name *</label>
											<div class="col-md-4 col-sm-4">
												<ng-form name="addobjectForm"> <input required
													type="text" name="om_object_name" id="om_object_name"
													class="form-control" placeholder="Object Name"
													ng-pattern="/^[ a-zA-Z]*$/" ng-maxlength=30
													ng-model="addobject.om_object_name"
													id="{{selectedNode.om_object_name}}"
													value="{{selectedNode.om_object_name}}" />
												<span style="color: red"
													ng-show="addobjectForm.om_object_name.$dirty && addobjectForm.om_object_name.$invalid && addobjectForm.om_object_name.$error ">
													<!-- 				<span
														ng-show="addobjectForm.om_object_name.$error.required">Object
															name is required.</span> <small class="error"
														ng-show="addobjectForm.om_object_name.$error.minlength">Object
															Name is required to be at least 1 characters</small> <small
														class="error"
														ng-show="addobjectForm.om_object_name.$error.maxlength">Object
															name cannot be longer than 20 characters</small>
													</span>  --></ng-form>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4 col-sm-4"
												for="om_object_link">Object Link *</label>
											<div class="col-md-4 col-sm-4">
												<ng-form name="addobjectForm"> <input required
													type="text" name="om_object_link" id="om_object_link"
													class="form-control" placeholder="Object Link"
													ng-pattern="/^[ _#/a-zA-Z]*$/" ng-maxlength=30
													ng-model="addobject.om_object_link"
													id="{{selectedNode.om_object_link}}"
													value="{{selectedNode.om_object_link}}" /> <span
													style="color: red"
													ng-show="addobjectForm.om_object_link.$dirty && addobjectForm.om_object_link.$invalid && addobjectForm.om_object_link.$error ">
													<!-- <span
													ng-show="addobjectForm.om_object_link.$error.required">Object
														name is required.</span> <small class="error"
													ng-show="addobjectForm.om_object_link.$error.minlength">Object
														Name is required to be at least 1 characters</small>  -->
													<!--<small
													class="error"
											 	ng-show="addobjectForm.om_object_link.$error.maxlength">Object
														Name cannot be longer than 20 characters</small>
 -->
												</span> </ng-form>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4 col-sm-4" for="usr">
												Object Parent *</label>
											 <div class="col-md-4 col-sm-4">
												<select ng-model="addobject.om_parent_id"
													id={{selectedNode.om_parent_id}} class="form-control"
													required name="om_parent_id"
													ng-options="objetctype.om_id as objetctype.om_object_name for objetctype in tree_data  | orderBy: 'name'">
													<option value="">Parent</option>
												</select>
											</div> 
										</div> 
										<div class="form-group">
											<label class="control-label col-md-4 col-sm-4">Object
												Stage </label>
											<div class="col-md-4 col-sm-4">
												<input class="form-control" type="text"
													name="om_object_stages"
													ng-model="addobject.om_object_stages"
													placeholder="Object Stage" ng-maxlength=2
													ng-pattern="/^[0-9]*$/" id="{{selectedNode.objectStage}}"
													value="{{selectedNode.objectStage}}">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4 col-sm-4" for="usr">Object
												Status *</label>
											<div class="col-md-4 col-sm-4"
												ng-init="addobject.om_rec_status=1">
												<input type="radio" name="Active" value="1"
													ng-model="addobject.om_rec_status" /> Active <input
													type="radio" name="InActive" value="2"
													ng-model="addobject.om_rec_status" /> InActive
											</div>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-md-4 col-sm-4"></label>
										<!-- <div ng-if="!addobjectForm.ib_id" style="padding-left: 107px;"> -->

										<input type="submit" id="submitbtn"
											data-loading-text="Loading..." value="Submit"
											ng-click="save(addobject)" class="btn btn-success"
											ng-disabled="addobjectForm.$invalid" />
										<!-- <button type="button" class="btn btn-danger"
											ng-click="resetModel()" data-dismiss="modal">Cancel</button> -->

										<!-- 				</div> -->

									</div>
								</div>
							</div>
						</div>
				</form>
                    </div>
                </div>
            </div>
            </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>

    <div class="modal fade" id="modal-ChangePassword" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>

                <div id="divBodyPE" class="modal-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label for="lblOldPassword" class="col-sm-3 control-label">Old Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtOldPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblNewPassword" class="col-sm-3 control-label">New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtNewPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblConfirmPassword" class="col-sm-3 control-label">Confirm Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtConfirmPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="lblErrorPassword" style="display:none;color:red;"></label>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="button" id="btnChangePassword" class="btn btn-info" value="Change" />
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-PasswordSent" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Client Tariff Master</h4>
                </div>

                <div id="divBodySent" class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" id="btnOk" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
    
<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/objectMasterController.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/angular-tree-control.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/tree-grid-directive.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>

<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/tree-control.css'>
<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/tree-control-attribute.css'>
<%-- <link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/treeGrid.css'> --%>


</body>
</html>