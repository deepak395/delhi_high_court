
<!-- <div class="form-group">
			<label class="col-md-4  control-label" for="criteria">Criteria<span class="star">*</span></label>
		    <div class="col-md-7">
	      		<select class="form-control" id="criteria" name="criteria" ng-model="searchcriteria.criteria" ng-options="criteria.id as criteria.value for criteria in criterias" ng-change="changeForEdit()">
					<option value="">Select Criteria</option>
				</select>
			</div>		   
</div> -->

<div class="form-group"
	ng-show="searchcriteria.metafield!=12 || searchcriteria.criteria!='between'">
	<label class="col-md-4  control-label" for="searchtext">Data To
		Search<span class="star">*</span>
	</label>
	<div class="col-md-7">
		<div class="col-md-10">
			<textarea name="searchtext" class="form-control"
				ng-keyup="charforEdit()" id="searchtext"
				ng-model="searchcriteria.searchtext"></textarea>
		</div>
		<div class="col-md-2">
			<i class="glyphicon glyphicon-question-sign popper"></i>

		</div>
	</div>

</div>



<!--   unused code from -->
<input type="text" class="form-control" ng-show="false" name="folderid"
	id="folderid" ng-model="searchcriteria.folderid=selectedNode.id" />

<div class="form-group"
	ng-show="searchcriteria.metafield==12 && searchcriteria.criteria=='between'">
	<label class="col-md-4  control-label" for="searchtext">Date To
		Search<span class="star">*</span>
	</label>
	<div class="col-md-7">
		<input type='text' class="form-control" ng-model='date1'
			mask='39/19/9999' id="searchtext" /> <br> <input type='text'
			class="form-control" ng-model='date2' mask='39/19/9999' />
	</div>
</div>
<!--   unused code upto -->



<div class="form-group">
	<div class="col-md-offset-5">
		<!-- <input type="submit" value="AND" ng-click="createquery(searchcriteria,'AND')" class="btn btn-success"/>
<input type="submit" value="OR" ng-click="createquery(searchcriteria,'OR')" class="btn btn-success"/> -->
	</div>
</div>
<div class="row" ng-hide="true">
	<table id="searchformdisplay"
		class="table table-striped table-bordered table-hover">
		<tr>
			<th>Operator</th>
			<!-- 	<th>Field</th> -->
			<th>Criteria</th>
			<!-- <th>Type</th> -->
			<th>Text To Search</th>
			<th>Action</th>
		</tr>
		<tr ng-repeat="data in searchcriterias">
			<td>{{data.operator}}</td>
			<!-- <td>{{data.metafieldname}}</td> -->
			<td>{{data.criteria}}</td>
			<!-- <td>{{data.type}}</td> -->
			<td>{{data.searchtext}}</td>
			<td><a class="btn btn-info btn-xs"
				ng-click="editCondition(data,$index)"><span
					class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
				<a class="btn btn-info btn-xs" ng-click="deleteCondition($index)"><span
					class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
			</td>
		</tr>
	</table>
</div>
<div class="row">
	<input type="button" value="Search" ng-click="searchdocument()"
		class="btn btn-success col-md-offset-5" />
	<!-- <input type="submit" value="Save Query" ng-click="savequery()" class="btn btn-success"/>
<input type="submit" value="New Query" ng-click="newquery()" class="btn btn-success"/>
<input type="submit" value="Load Query" data-toggle="modal" data-target="#queriesModal" class="btn btn-success"/>-->
</div>


<div class="_content" id="myModal">

	<button class="mainhideshow">
		<B>For Student Registration/Degree/Transcript.</B>
	</button>
	<div class="panel-hideshow">
		<p>User can enter student Roll no, Name, Date of Birth, Course,
			Last name, Father's name etc.</p>
	</div>
	<button class="mainhideshow">
		<B>For Question Bank.</B>
	</button>
	<div class="panel-hideshow">
		<p>User can enter Subject and any topic.</p>
	</div>
	<button class="mainhideshow">
		<b>For Course outline.</b>
	</button>
	<div class="panel-hideshow">
		<p>User can enter Subject or Course.</p>
	</div>
</div>

<script>
        var acc = document.getElementsByClassName("mainhideshow");
        var i;

        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function() {
            this.classList.toggle("activehideshow");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            } 
          });
        }
</script>

<script type="text/javascript">

$(document).ready(function(){
	
	 $('#myModal').hide();

	    // Initialize our popover
	    //
	    $('.popper').popover({
	        content: $('#myModal'), // set the content to be the 'blah' div
	        placement: 'left',
	        html: true 
	    });
	    // The popover contents will not be set until the popover is shown.  Since we don't 
	    // want to see the popover when the page loads, we will show it then hide it.
	    //
	    $('.popper').popover('show');
	    $('.popper').popover('hide');
	    
	    // Now that the popover's content is the 'blah' dive we can make it visisble again.
	    //
	    $('#myModal').show();
	  	
/* $('.mainhideshow').click(function(){
	$(this).children('.panel-hideshow').show();
}); */
});

</script>