<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%-- <%@include file="../../content/script.jsp"%> --%>
<script src="${pageContext.request.contextPath}/js/js/angular.js"></script>

<script
	src="${pageContext.request.contextPath}/js/scripts/mdicontrollers/OpenFileController.js"></script>
<%@include file="../../content/header.jsp"%>
</head>
<body ng-app="mdiApp" ng-controller="openFileController"
	ng-init="setInitialDetails('${file_url}')">


	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="${context}/mdi/images/Dms-Logo.png" alt="" />
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<li>Welcome : ${USER.username }</li>
				</ul>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-8">




				<div class="modal-dialog" id="modal1"
					style="margin-top: -35px; overflow: auto;">

					<div class="modal-content">

						<div class="row">
							<jsp:include page="viewer.jsp"></jsp:include>

						</div>
						<!-- <div>
						<iframe src="/mdi/uploads/asdf.docx"></iframe>
						</div> -->

					</div>
				</div>


				<%-- <img src="${context}/mdi/uploads/pri.pdf"
					style="width: 720px;"> --%>
			</div>
			<div class="col-md-4">



				<div style="display: none;">
					<input type="text" value='{{doc_complete_data.doc_id}}'
						name="document_id">

				</div>


				<div class="row">
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">File Name</label>
						</div>
						<div class="col-md-7">
							<span>${document.dfd_name}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Department</label>
						</div>
						<div class="col-md-7">
							<span>${document.departments.dept_name}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Sub Department</label>
						</div>
						<div class="col-md-7">${document.subdepartments.sub_dept_name}</div>
					</div>
				</div>






				<!-- Drop Down -->
				<c:forEach items="${metadataValue }" var="data">
					<div class="row">
						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">


							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">

								<label id="metadata" for="metadata"><c:out
										value="${data.if_name }" /> </label>

							</div>
							<div class="col-md-7">
								<span><c:out value="${data.meta_value }" /> </span>
							</div>

						</div>
					</div>
				</c:forEach>

			</div>



		</div>

	</div>
	<!-- Modal -->
	<div class="modal fade" id="Reject_Model" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true"></div>




</body>
</html>