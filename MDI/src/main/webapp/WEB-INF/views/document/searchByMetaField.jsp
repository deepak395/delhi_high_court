<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">
<link rel="stylesheet"
	href="${context}/mdi/assets/css/font-icons/entypo/css/entypo.css">

<%@include file="../../content/header.jsp"%>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript"
	src="${context}/mdi/js/js2/angular.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>

<%-- <script type="text/javascript" src="${context}/mdi/js/js/angular.min.js"></script> --%>
<script type="text/javascript"
	src="${context}/mdi/js/scripts/mdicontrollers/SearchByMetaDataController.js"></script>

<script type="text/javascript">
	
</script>
<style>
[data-tip] {
	position: relative;
}

[data-tip]:before {
	content: '';
	/* hides the tooltip when not hovered */
	display: none;
	content: '';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;
	position: absolute;
	top: 30px;
	left: 35px;
	z-index: 8;
	font-size: 0;
	line-height: 0;
	width: 0;
	height: 0;
}

[data-tip]:after {
	display: none;
	content: attr(data-tip);
	position: absolute;
	top: 35px;
	left: 0px;
	padding: 5px 8px;
	background: #1a1a1a;
	color: #fff;
	z-index: 9;
	font-size: 0.75em;
	height: 18px;
	line-height: 18px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space: nowrap;
	word-wrap: normal;
}

[data-tip]:hover:before, [data-tip]:hover:after {
	display: block;
}
</style>
</head>
<body ng-app="mdiApp" class="page-body skin-facebook"
	ng-controller="MetaDataController">
	<%@include file="../../content/sideheader.jsp"%>
	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="${context}/mdi/images/Dms-Logo.png" alt="" />
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<li>Welcome : <%=user.getUm_fullname()%></li>
				</ul>
			</div>
		</div>
		<hr />

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="border-color: #c2c9dc;">
					<div class="panel-heading" style="background-color: #373e4a;">
						<div class="panel-title" style="font-weight: bold;">Search
							By MetaFields</div>


					</div>

					<%
						if (userr.getLk().getLk_longname().equals("System Admin")
								|| userr.getLk().getLk_longname().equals("Search Admin")) {
					%>
					<div class="panel-body">

						<!--   department  -->

						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">



							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">
								<label id="metadata" for="metadata">Department</label>
							</div>
							<div class="col-md-7">


								<select id="dept-id" class="form-control" style="width: 380px;"
									ng-model="folderform.dept_id"
									ng-change="getSubDept(folderform.dept_id)"
									data-tip="This is the text of the tooltip2">
									<option value="{{dept.dept_id}}"
										ng-repeat="dept in departments">{{dept.dept_name}}</option>
								</select>

								<%-- <span>${DEPT.dept_name }</span> <span style="display: none;"
									ng-init="getSubDept(${DEPT.dept_id })"></span> --%>

							</div>
						</div>
						<!--  sub Dept  -->
						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">
							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">
								<label id="metadata" for="metadata">Select Sub
									Department</label>
							</div>
							<div class="col-md-7">
								<select id="rep-id" class="form-control" style="width: 380px;"
									ng-model="folderform.sub_dept_id"
									ng-change="getdocumentTypes(folderform.sub_dept_id)">
									<option value="{{subdept.sub_dept_id}}"
										ng-repeat="subdept in subdepartments">{{subdept.sub_dept_name}}</option>

								</select>
							</div>
						</div>

						<!--  doc type  -->
						<div class="row">
							<div class="form-group"
								style="margin: 15px 15px; font-size: 14px; color: #808080;">
								<div class="col-md-3">
									<label id="dt_id" name="dept_type">Select Document Type</label>
								</div>
								<div class="col-md-4" id="hidden_div">
									<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid"
										style="margin-left: 100px;" ng-model="folderform.dfd_dt_mid"
										ng-change="getindexFields(folderform.dfd_dt_mid)">
										<option value="{{subdata.dt_id}}"
											ng-repeat="subdata in documentTypeList">{{subdata.dt_name}}</option>
									</select>

								</div>
								<div class="col-md-4"
									style="padding-left: 100px; font-weight: bold; font-size: 20px; color:">

									<!-- <span data-toggle="popover" title="Help"
											data-content="{{show_msg}}">?</span> -->
									<!-- <img  data-toggle="popover" title="Help"
											data-content="{{show_msg}}" src="/mdi/images/question.png" alt="" width="25" height="25"> -->
								</div>
							</div>
						</div>

						<!-- folder -->

						<div class="row">
							<div class="form-group"
								style="margin: 15px 15px; font-size: 14px; color: #808080;">
								<div class="col-md-3">
									<label id="dt_id" name="dept_type">Select Folder</label>
								</div>
								<div class="col-md-4" id="hidden_div">
									<select style="margin-left: 100px;" id="dfd_dt_mid"
										class="form-control" name="dfd_dt_mid"
										ng-init="document.dfd_fd_mid=0" ng-model="document.dfd_fd_mid"
										ng-change="">
										<option value="0">---Select---</option>
										<option value="{{subdata.id}}"
											ng-repeat="subdata in foldersList">{{subdata.folder_name}}</option>
									</select>

								</div>
								<div class="col-md-4"
									style="padding-left: 100px; font-weight: bold; font-size: 20px; color:">

									<!-- <span data-toggle="popover" title="Help"
											data-content="{{show_msg}}">?</span> -->
									<!-- <img  data-toggle="popover" title="Help"
											data-content="{{show_msg}}" src="/mdi/images/question.png" alt="" width="25" height="25"> -->
								</div>
							</div>
						</div>

						<!-- criteria -->
						<div class="row">
							<div class="form-group"
								style="font-size: 14px; color: #808080; padding: 10px 0px;">
								<div class="col-md-4"
									style="text-align: left; font-size: 14px; color: #4a4a4a;">
									<label id="metadata" for="metadata">Criteria</label>
								</div>
								<div class="col-md-7">
									<fieldset ng-repeat="column in columns">
										<select name="if_name" required ng-model="column.if_id"
											ng-change="getType($index)">
											<option ng-repeat="type in if_names" value="{{type.if_id}}">{{type.if_name}}</option>
										</select> <select ng-if="column.show_if=='D'" name="if_value" required
											ng-model="column.if_value">
											<option value="" disabled selected>All</option>
											<option ng-repeat="lk in column.if_values_dropd"
												value="{{lk.lk_longname}}">{{lk.lk_longname}}</option>
										</select> <input ng-if="column.show_if=='O' " type="text"
											name="if_value" ng-model="column.if_value" name="" required>

										<!-- <input  type="date" name="if_value"  pattern="dd/MM/YYYY" ng-model="DateBirth"  required /> -->
										<div ng-if="column.show_if=='DA' ">
											<input type="date" name="if_value" ng-model='column.if_value' />

										</div>

										<button class="remove" ng-click="removeColumn($index)">x</button>
									</fieldset>
								</div>

							</div>

						</div>
					</div>
					<!--  -->

					<div class="form-group">

						<label class="col-md-4  control-label" for="name">Year</label>
						<div class="col-md-4">
							<select id="year" name="year" class="form-control"
								style="width: 375px;" ng-model="md_year">
								<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
							</select>
						</div>

						<div class="col-md-4">
							<img style="margin-right: 50px;" data-toggle="popover"
								data-title="{{show_title}}" data-content="{{show_msg}}"
								src="/mdi/images/question.png" alt="" width="25" height="25">
						</div>


					</div>

					<!-- buttons -->
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">

						</div>
						<div class="col-md-7"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<button class="addfields" ng-click="addNewColumn()">Add
								Criteria</button>
							<button class="addfields"
								ng-click="searchByMetaFields(folderform.dept_id,folderform.sub_dept_id,folderform.dfd_dt_mid,document.dfd_fd_mid)">Search</button>

						</div>
					</div>

				</div>


				<%
					} else {
				%>

				<div class="panel-body">

					<!--   department  -->

					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">



						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Department</label>
						</div>
						<div class="col-md-7">

							<%-- <%if(userr.getLk().getLk_longname().equals("System Admin")) {%>
										<select id="dept-id" class="form-control" style="width: 380px;"
											ng-model="folderform.dept_id"
											ng-change="getSubDept(dept_id)">
												<option value="{{dept.dept_id}}"
										ng-repeat="dept in departments">{{dept.dept_name}}</option>
										</select>
									<% } else { %> --%>
							<span>${DEPT.dept_name }</span> <span style="display: none;"
								ng-init="getSubDept(${DEPT.dept_id })"></span>

						</div>
					</div>
					<!--  sub Dept  -->
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Select Sub Department</label>
						</div>
						<div class="col-md-7">
							<select id="rep-id" class="form-control" style="width: 380px;"
								ng-model="folderform.sub_dept_id"
								ng-change="getdocumentTypes(folderform.sub_dept_id)">
								<option value="{{subdept.sub_dept_id}}"
									ng-repeat="subdept in subdepartments">{{subdept.sub_dept_name}}</option>

							</select>
						</div>
					</div>

					<!--  doc type  -->
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-3">
								<label id="dt_id" name="dept_type">Select Document Type</label>
							</div>
							<div class="col-md-4" id="hidden_div">
								<select id="dfd_dt_mid" class="form-control" name="dfd_dt_mid"
									style="margin-left: 100px;" ng-model="folderform.dfd_dt_mid"
									ng-change="getindexFields(folderform.dfd_dt_mid)">
									<option value="{{subdata.dt_id}}"
										ng-repeat="subdata in documentTypeList">{{subdata.dt_name}}</option>
								</select>
							</div>
						</div>
					</div>
					<!-- folder -->
					<div class="row">
						<div class="form-group"
							style="margin: 15px 15px; font-size: 14px; color: #808080;">
							<div class="col-md-3">
								<label id="dt_id" name="dept_type">Select Folder</label>
							</div>
							<div class="col-md-4" id="hidden_div">
								<select style="margin-left: 100px;" id="dfd_dt_mid"
									class="form-control" name="dfd_dt_mid"
									ng-init="document.dfd_fd_mid=0" ng-model="document.dfd_fd_mid"
									ng-change="">
									<option value="0">---Select---</option>
									<option value="{{subdata.id}}"
										ng-repeat="subdata in foldersList">{{subdata.folder_name}}</option>
								</select>

							</div>
							<div class="col-md-4"
								style="padding-left: 100px; font-weight: bold; font-size: 20px; color:">

								<!-- <span data-toggle="popover" title="Help"
											data-content="{{show_msg}}">?</span> -->
								<!-- <img  data-toggle="popover" title="Help"
											data-content="{{show_msg}}" src="/mdi/images/question.png" alt="" width="25" height="25"> -->
							</div>
						</div>
					</div>

					<!-- criteria -->
					<div class="row">
						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">
							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">
								<label id="metadata" for="metadata">Criteria</label>
							</div>
							<div class="col-md-7">
								<fieldset ng-repeat="column in columns">
										<select name="if_name" required ng-model="column.if_id"
											ng-change="getType($index)">
											<option ng-repeat="type in if_names" value="{{type.if_id}}">{{type.if_name}}</option>
										</select> <select ng-if="column.show_if=='D'" name="if_value" required
											ng-model="column.if_value">
											<option value="" disabled selected>All</option>
											<option ng-repeat="lk in column.if_values_dropd"
												value="{{lk.lk_longname}}">{{lk.lk_longname}}</option>
										</select> <input ng-if="column.show_if=='O' " type="text"
											name="if_value" ng-model="column.if_value" name="" required>

										<!-- <input  type="date" name="if_value"  pattern="dd/MM/YYYY" ng-model="DateBirth"  required /> -->
										<div ng-if="column.show_if=='DA' ">
											<input type="date" name="if_value" ng-model='column.if_value' />

										</div>

										<button class="remove" ng-click="removeColumn($index)">x</button>
									</fieldset>
							</div>

						</div>
					</div>

					<!--    -->

					<div class="form-group"
						style="text-align: left; font-size: 14px; color: #4a4a4a;">
						<label class="col-md-4  control-label" for="name">Year</label>
						<div class="col-md-4">
							<select id="year" name="year" class="form-control"
								ng-model="document.dfd_year">
								<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
							</select>
						</div>
						<div class="col-md-4">
							<img style="margin-right: 50px;" data-toggle="popover"
								title="{{show_title}}" data-content="{{show_msg}}"
								src="/mdi/images/question.png" alt="" width="25" height="25">
						</div>
					</div>

					<!-- buttons -->
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">

						</div>
						<div class="col-md-7"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<button class="addfields" ng-click="addNewColumn()">Add
								Criteria</button>
							<button class="addfields"
								ng-click="searchByMetaFields(${DEPT.dept_id },folderform.sub_dept_id,folderform.dfd_dt_mid,document.dfd_fd_mid)">Search</button>


						</div>
					</div>

				</div>

				<%
					}
				%>
				<div class="panel-body">
					<div class="tab-content">
						<div class="bs-component" id="profile-7">
							<span>Total Count = </span><span>{{total_count}}</span>
							<table id="myTable1"
								class="table table-bordered table-responsive"
								st-table="displayedCollection" st-safe-src="dfdDetails"
								class="table table-striped table-bordered nowrap table-hover"
								width="100%">
								<thead>
									<tr id="ifnames">

										<!-- <th>Document Name</th>


											<th>View</th>
											<th>MetaData</th> -->
										<th>Sr.No.</th>
										<th>View</th>
										<th ng-repeat="type in if_names | orderBy : 'if_id' "
											ng-model="colomnName">{{type.if_name}}</th>

									</tr>

								</thead>
								<tbody>
									<tr id="dfd" ng-repeat="dfd in displayedCollection">
										<td>{{$index + 1}}</td>
										<td>
											<button type="button"
												class="btn btn-primary btn-sm pull-right"
												data-target="#user_Modal" data-toggle="modal"
												ng-click="ViewFile(dfd.dfd_id)">
												<span class="glyphicon glyphicon-plus-sign"></span> View
											</button>
										</td>
										<td ng-repeat="dfdmd in dfd.metadata | orderBy: 'md_mf_mid'"
											ng-model="hello">{{dfdmd.md_value}}</td>
										<!-- <td ng-model="dfd.name">{{dfd.dfd_name}}</td> -->


										<!-- <td>
												<button type="button"
													class="btn btn-primary btn-sm pull-right"
													data-target="#meta_Modal" data-toggle="modal"
													ng-click="MetaFile(dfd.dfd_id)">
													<span class="glyphicon glyphicon-plus-sign"></span> MetaData
												</button>
											</td> -->
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6" class="text-center">
											<div st-pagination="" st-items-by-page="5"
												st-displayed-pages="8"></div>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>

				</div>
			</div>
			<!-- ////////////////////////////popUp -->
			<div class="modal fade" id="documentfile_Modal" tabindex="-1"
				role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
				style="z-index: 10500;">
				<div class="modal-dialog"
					style="width: 50%; overflow: auto; right: 298px;">
					<div class="modal-content">
						<div class="modal-header" style="background-color: black;">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">
								<strong style="color: #FBFCFD;">{{title}}</strong>
							</h4>
						</div>
						<div class="row">
							<object type="application/pdf" data="{{fileurl}}" width="100%"
								height="600"> </object>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="meta_Modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">

		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <span style="color:white;font-weight:bold">{{viewDfdData.dfd_name}}</span> -->
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
					<h4 class="modal-title" id="myModalLabel" align="center"
						style="color: white;">
						<strong style="color: white;" class="ng-binding">{{viewDfdData.dfd_name}}</strong>
					</h4>
				</div>
				<div class="modal-body" style="width: 881px;">

					<table id="myTable1" class="table table-bordered table-responsive"
						st-table="metadata" st-safe-src="dfdDetails"
						class="table table-striped table-bordered nowrap table-hover"
						width="100%">
						<thead>
							<tr>
								<th>Meta Field</th>


								<th>Meta Data</th>
							</tr>

						</thead>
						<tbody>
							<tr id="md" ng-repeat="md in metadata">
								<td ng-model="md.md_mf_mid">{{md.indexField.if_name}}</td>
								<td ng-model="md.md_value">{{md.md_value}}</td>
							</tr>
						</tbody>
					</table>
				</div>


			</div>
		</div>
	</div>

	<!-- view doc -->

	<!-- <div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">

		<div class="modal-dialog modal-lg">
			<div class="modal-content" style="width: 606px;">
				<div style="margin-left: 580px;">
					<input align="left" type="button" value="X" ng-click="closeDiv()">
				</div>
				<div class="modal-header"
					style="background-color: black; width: 881px;">
					<span style="color:white;font-weight:bold">{{viewDfdData.dfd_name}}</span>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
					<h4 class="modal-title" id="myModalLabel" align="center"
						style="color: white;">
						<strong style="color: white;" class="ng-binding">{{viewDfdData.dfd_name}}</strong>
					</h4>
				</div>
				<div class="modal-header" style="width: 881px;">

					<object type="application/pdf"
						data="{{fileurl}}#toolbar=0&navpanes=0&scrollbar=0" width="100%"
						height="600"> </object>
				</div>


			</div>
		</div>
	</div> -->
</body>
<Script>
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			placement : 'right',
			trigger : 'hover'
		});
	});
</Script>
</html>