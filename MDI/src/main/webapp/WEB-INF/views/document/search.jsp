<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Management Development Institute</title>

<style type="text/css">
.mainhideshow {
	background-color: #eee;
	color: #444;
	cursor: pointer;
	padding: 18px;
	width: 100%;
	border: none;
	text-align: left;
	outline: none;
	font-size: 15px;
	transition: 0.4s;
}

.activehideshow, .mainhideshow:hover {
	background-color: #ccc;
}

.mainhideshow:after {
	content: '\002B';
	color: #777;
	font-weight: bold;
	float: right;
	margin-left: 5px;
}

.activehideshow:after {
	content: "\2212";
}

.panel-hideshow {
	padding: 0 18px;
	background-color: white;
	max-height: 0;
	overflow: hidden;
	transition: max-height 0.2s ease-out;
}

.btn-circle {
	width: 30px;
	height: 30px;
	padding: 6px 0px;
	border-radius: 15px;
	text-align: center;
	font-size: 12px;
	line-height: 1.42857;
}

.popper-content hide {
	width: 500px;
}

#popover993508 {
	width: 500px;
}
</style>


<%@ include file="../../content/header.jsp"%>

<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>

</head>
<body class="page-body skin-facebook" data-url="http://neon.dev"
	ng-controller="searchController">

	<%@include file="../../content/sideheader.jsp"%>

	<div class="page-container sidebar-collapsed">

		<div class="main-content">
			<div class="row">
				<!-- Profile Info and Notifications -->
				<div class="col-md-4 col-sm-2 clearfix">
					<img src="../images/Dms-Logo.png" alt="" />
					<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
				</div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
					<h2 style="color: #03577a; font-weight: 600;">Management
						Development Institute</h2>
					<!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
				</div>
				<!-- Raw Links -->
				<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

					<ul class="user-info pull-right pull-none-xsm">
						<li>Welcome : <%= user.getUm_fullname() %></li>
					</ul>
				</div>
			</div>
			<hr />
			<!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary" style="border-color: #c2c9dc;">

						<div class="panel-heading" style="background-color: #373e4a;">
							<div class="panel-title" style="font-weight: bold;">Content Search</div>

							<!-- <div class="panel-options">
								<a class="tab" href="#profile-7" data-toggle="tab"
									style="border-color: #ebebeb;"></a> <a class="tab"
									href="#profile-8" data-toggle="tab"
									style="border-color: #ebebeb;"></a> <a class="tab"
									href="#profile-9" data-toggle="tab"
									style="border-color: #ebebeb;"></a> <a href="#"
									data-rel="collapse"><i class="entypo-down-open"></i></a>
							</div> -->
						</div>
						<div class="panel-body">
							<form
								class="form-horizontal reduce-gap ng-pristine ng-invalid ng-invalid-required"
								role="form" novalidate="" name="masterForm">
								<div class="row">
									<div class="col-md-6">

										<div class="form-group">
											<label class="col-md-4  control-label" for="name">Repository<span
												class="star">*</span></label>
											<div class="col-md-7">
												<span class="form-control">{{repositories[0].name }}</span>
												<!-- 			<select class="form-control" id="dfd_rep_mid" name="dfd_rep_mid" ng-change="getAllDepartment()" ng-model="document.dfd_rep_mid" required ng-options="repository.id as repository.name for repository in repositories">
					 				   			<option value="">Select Repository</option>
											</select>      -->
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4  control-label" for="name">Department<span
												class="star">*</span></label>
											<div class="col-md-7">



												<%
							if(userr.getLk().getLk_longname().equals("System Admin") || userr.getLk().getLk_longname().equals("Search Admin")) {
						%>
												<select id="dfd_dept_mid" class="form-control"
													name="dfd_dept_mid" ng-model="document.dfd_dept_mid"
													ng-change="getAllSubDept(document.dfd_dept_mid)">
													<option value="{{dept.dept_id}}"
														ng-repeat="dept in departments">{{dept.dept_name}}</option>
												</select>
												<% } else { %>
												<span class="form-control">${DEPT.dept_name }</span> <span
													style="display: none;"
													ng-init="getSubDept(${DEPT.dept_id })"></span>
												<% } %>

											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4  control-label" for="name">
												Select Sub Department<span class="star">*</span>
											</label>
											<div class="col-md-7">
												<select id="dfd_sub_dept_mid" class="form-control"
													name="dfd_sub_dept_mid"
													ng-model="document.dfd_sub_dept_mid"
													ng-change="getDocumentTypes(document.dfd_sub_dept_mid)">
													<option value="{{subdata.sub_dept_id}}"
														ng-repeat="subdata in subdepartments">{{subdata.sub_dept_name}}</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4  control-label" for="name">
												Select Document Type<span class="star">*</span>
											</label>
											<div class="col-md-7">
												<select id="dt_id" class="form-control" name="dt_id"
													ng-model="document.dt_id">
													<option value="{{dt.dt_id}}"
														ng-repeat="dt in documentTypeList">{{dt.dt_name}}</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4  control-label" for="name">Year<span
												class="star">*</span></label>
											<div class="col-md-7">
												<select id="year" name="year" class="form-control"
													ng-model="document.dfd_year">
													<option value="{{year}}" ng-repeat="year in years">{{year}}</option>
												</select>
											</div>
										</div>

									</div>


									<div class="col-md-6" id="advanced_search">
										<div class="col-md-12" ng-if="error">
											<div class="alert alert-danger alert-dismissible"
												role="alert">
												<div class="row text-center">{{error}}</div>
											</div>
										</div>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#advanced">Content Search</a></li>
										</ul>
										<div class="tab-content">
											<div id="advanced" class="tab-pane fade in active">
												<%@ include file="../document/_advancedsearch.jsp"%>
											</div>
										</div>
									</div>
								</div>

							</form>

							<!--Data To Show  -->
							<div class="row">
								<table id="searchformdisplay" st-table="displayedCollection"
									st-safe-src="documentlist"
									class="table table-striped table-bordered table-hover"
									current-page="setCurrentPage"
									page-items="model.pPageSizeObj.selected">
									<thead>
										<tr>


											<th>File Name</th>
											<th>Action</th>
											<!-- <th>Download</th> -->

										</tr>
									</thead>
									<tbody>

										<tr ng-repeat="data in displayedCollection">
											Total Number of records {{documentlist.length|number}}
											<td>{{data.dfd_name}}</td>
											<!-- <td>{{data.metaDataList[1].md_value}} </td> -->
											<td><input class="btn btn-info btn-xs"
												data-toggle="modal" data-target="#user_Modal" type="button"
												value="view" ng-click="viewfile(data.dfd_id)"> <%-- <a target="_new" href="${pageContext.request.contextPath}/casefile/viewdocs/{{data.id}}" class="btn btn-info btn-xs" ><span class="glyphicon" aria-hidden="true"></span>View</a> --%>
												<%-- <a target="_new" href="${pageContext.request.contextPath}/casefile/dataentry/{{data.id}}" class="btn btn-info btn-xs" ><span class="glyphicon" aria-hidden="true"></span>View</a> --%>
											</td>
											<%-- 	<td><a target="_new" href="${pageContext.request.contextPath}/casefile/filedownload/{{data.id}}" class="btn btn-info btn-xs"><span class="glyphicon" aria-hidden="true"></span>Download</a> --%>

										</tr>

									</tbody>
									<tfoot>
										<tr>
											<td colspan="5" class="text-center">

												</li>
												<div st-pagination="" st-items-by-page="5"
													st-displayed-pages="7"></div>
											</td>

										</tr>
									</tfoot>

								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Footer -->
			<footer class="main"> Copyright � <strong>stockholdingdms</strong>2018.
			All Rights Reserved </footer>
		</div>
	</div>


	<!-- view File  -->
	<!-- view File  -->
	<!-- <div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">

		<div class="modal-dialog modal-lg">
			<div class="modal-content" style="width: 606px;">
				<div class="modal-header"
					style="background-color: black; width: 881px;">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel" align="center">
						<strong style="color: #FBFCFD;">{{viewDfdData.dfd_name}}</strong>
					</h4>
				</div>


				<div class="modal-header" style="width: 881px;">

					<object type="application/pdf"
						data="{{fileurl}}#toolbar=0&navpanes=0&scrollbar=0" width="100%"
						height="600"> </object>
				</div>

			</div>
		</div>
	</div> -->

	<%-- 	<div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">

		<div class="modal-dialog modal-lg">
			<div class="modal-content" style="width: 606px;">
				<div style="margin-left: 580px;">
					<input align="left" type="button" value="X" ng-click="closeDiv()">
				</div>
				<div class="modal-header" style="width: 606px;">

					<object type="application/pdf"
						data="{{fileurl}}#toolbar=0&navpanes=0&scrollbar=0" width="100%"
						height="600"> </object>
				</div>

				<%@ include file="../user/_master_form.jsp"%>
			</div>
		</div>
	</div> --%>



	<script type="text/javascript"
		src="${context}/mdi/js/scripts/mdicontrollers/SearchController.js"></script>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/bootstrap/ui-bootstrap-tpls.0.11.2.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/angularJs/angular-tree-control.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/angularJs/tree-grid-directive.js"></script>

	<link rel='stylesheet'
		href='${pageContext.request.contextPath}/css/tree-control/tree-control.css'>
	<link rel='stylesheet'
		href='${pageContext.request.contextPath}/css/tree-control/tree-control-attribute.css'>
	<%-- <link rel='stylesheet' href='${pageContext.request.contextPath}/css/tree-control/treeGrid.css'> --%>

	<script src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>

</body>
</html>