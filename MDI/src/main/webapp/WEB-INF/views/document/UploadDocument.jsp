<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/Smart-Table-master/dist/smart-table.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>
<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/UploadDocumentController.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload-shim.js"></script> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/dirPagination.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/ng-csv.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/angularJs/angular-sanitize.min.js"></script>

<%@include file="../../content/header.jsp" %>
</head>
<body ng-controller="UploadDocumentController">
<%@include file="../../content/sideheader.jsp" %>
			 <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context }/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                       <li>Welcome : <%= user.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           	<div class="row">
                <div class="col-md-12">
                <div class="panel panel-primary" style="border-color: #c2c9dc;">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Document Upload</div>
            
                      <!--   <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
                    </div>
                    <div class="panel-body">
                    <form id="documentupload" enctype="multipart/form-data">
                        <div class="row">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-3">
                                    <label id="metadata" name="metadata">Select Meta Template</label>
                                </div>
                                <div class="col-md-3">
              
                                     <select id="dept_id"  class="form-control" name="dept_id"  ng-model="document.dept_id" required  ng-change="getSubDept(document.dept_id)">
                                      <option value="" disabled selected>-------Select MetaData---------</option>
                                        <option value="{{dept.dept_id}}" required ng-repeat="dept in departments">{{dept.dept_name}}</option>
                                       
                                     </select>
                                    <span  style="color:red" ng-show="uploadDocument.dept_id.$invalid.required">Select atleast one value</span>
                           
                                </div>
                                <div class="col-md-3">
                                    <label id="folder" name="folder">Select Sub DepartMent</label>
                                </div>
                                <div class="col-md-3">
                                   <select id="sub_dept_id"  class="form-control" name="sub_dept_id"  ng-model="document.sub_dept_id" ng-change="getDocumentTypes(document.sub_dept_id)" required >
                                       <option value="">---------select sub Department----------</option>
                                        <option value="{{subdata.sub_dept_id}}" ng-repeat="subdata in subdepartments">{{subdata.sub_dept_name}}</option>
                                     </select>
                                      <span  style="color:red" ng-show="uploadDocument.sub_dept_id.$invalid.required">Select atleast one value</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                
                                <div class="col-md-3">
                                    <label id="folder" name="folder">Select Document Type</label>
                                </div>
                                <div class="col-md-3">
                                   <select id="dt_id"  class="form-control" name="sub_dept_id"  ng-model="document.dfd_dt_mid" ng-change="getfolders(document.dfd_dt_mid)" required >
                                       <option value="">---------select document type----------</option>
                                        <option value="{{dt.dt_id}}" ng-repeat="dt in documentTypeList">{{dt.dt_name}}</option>
                                     </select>
                                      <span  style="color:red" ng-show="uploadDocument.dt_id.$invalid.required">Select atleast one value</span>
                                </div>
                                
                                <div class="col-md-3">
                                    <label id="subfolder" name="subfolder">Select Year</label>
                                </div>
                                <div class="col-md-3" id="hidden_div4">
                               
                                    <select id="year" name="year" class="form-control" ng-model="document.year" >
                                     <option value="" disabled selected>----Select year---------</option>
                                        <option value="{{year}}" ng-repeat="year in years">{{year}}</option>
                                    </select>
                                    <!-- <span  style="color:red" ng-show="uploadDocument.year.$invalid.required">Select atleast one value</span> -->
                                </div>
                                <br>
                                <br>
                                
                                 <div class="col-md-3">
                                    <label id="subfolder" name="subfolder">Select Folder</label>
                                </div>
                                <div class="col-md-3" id="hidden_div4">
                                                                        <!--  ng-model="document.fdfd_fd_mid"-->
                                    <select id="year" name="year" class="form-control"  ng-model="document.dfd_fd_mid"  >
                                     <option value="0" disabled selected>----Select Folder---------</option>
                                        <option value="{{fd.id}}" ng-repeat="fd in foldersList">{{fd.folder_name}}</option>
                                    </select>
                                   <!--  <span  style="color:red" ng-show="uploadDocument.year.$invalid.required">Select atleast one value</span> -->
                                </div>
                                
                                <!--  <div class="col-md-3">
                                    <label id="upload" name="upload">Upload</label>
                                </div> 
                                <div class="col-md-3">
                                   <input type="file" multiple="multiple"  ngf-select ng-model="file_name" ngf-multiple="false" name="file_name" />
                                  
                                   <div ng-bind="document.file_name.name"></div>
                                </div> -->
                            </div>
                        </div>
                         <div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="form-group">
										<label class="col-md-3  control-label" for="file">Files<span class="star">*</span></label>
										<input type="file" multiple="multiple" ngf-select ng-model="picFile" name="file" accept="application/pdf"  ngf-model-invalid="errorFile"/> 
    									<ul>
       									   <li ng-repeat="file in files">{{file.name}}</li>
   										 </ul>
   										 <span  style="color:red" ng-show="uploadDocument.file.$invalid.required">Select atleast one value</span>
								</div>
                            </div>
                        </div>
                        <div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-6" style="text-align:right;">
                                	<input type="submit" id="btnsubmit" value="SUBMIT" class="btn btn-primary btn-blue"  ng-click="save(document)"/>
                                  </div> 
                                <div class="col-md-6">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" onclick="goBack()""/>
                                </div>
                            </div>
                        </div>
            		</form>
                    </div>
                </div>
            </div>
            </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>
</body>
</html>			 