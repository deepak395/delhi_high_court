<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/DocumentsController.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload-shim.js"></script> 

<%@include file="../../content/header.jsp" %>
</head>
<body ng-controller="DocumentsController">
<%@include file="../../content/sideheader.jsp" %>
			 	 <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context }/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">MDI</h2>
                    <!--<img src="~/Images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                        <!-- Profile Info -->
                        <li>Welcome : <%= user.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           	<div class="row">
                <div class="col-md-12">
                <div class="panel panel-primary" style="border-color: #c2c9dc;">
            
                    <div class="panel-heading" style="background-color: #373e4a;">
                        <div class="panel-title" style="font-weight:bold;">Document Upload</div>
            
                        <!-- <div class="panel-options">
                            <a class="tab" href="#profile-7" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-8" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a class="tab" href="#profile-9" data-toggle="tab" style="border-color: #ebebeb;"></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div> -->
                    </div>
                    <div class="panel-body">
                    <form id="documentupload" enctype="multipart/form-data">
                        <div class="row">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-3">
                                    <label id="metadata" name="metadata">Select Metadata</label>
                                </div>
                                <div class="col-md-3">
                 
			   
                                 
                                     <select id="dept_id"  class="form-control" name="form_select"  ng-model="document.dept_id" ng-change="getFoldersByDept(document.dept_id)">
                                        <option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>
                                     </select>
                           
                                </div>
                                <div class="col-md-3">
                                    <label id="folder" name="folder">Select Folder</label>
                                </div>
                                <div class="col-md-3" id="hidden_div">
                                   <select id="folder_id"  class="form-control" name="form_select"  ng-model="folder_id" ng-change="getSubFolders();" >
                                        <option value="0" >SELECT FOLDER</option>
                                        <option value="{{folder.folder_id}}" ng-repeat="folder in folders">{{folder.folder_name}}</option>
                                     </select>
                                     
                                   <!--  <select id="metadata" class="form-control">
                                        <option value="0">--Select--</option>
                                        <option value="1">Legal Department</option>
                                        <option value="2">Personal File</option>
                                        <option value="3">Circular</option>
                                        <option value="4">Office Order</option>
                                        <option value="5">Grievance Redressal Committee (GRC)</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-3">
                                    <label id="subfolder" name="subfolder">Select Sub Folder</label>
                                </div>
                                <div class="col-md-3" id="hidden_div4">
                                    <select id="metadata" class="form-control" ng-model="document.folder_id">
                                        <option value="{{fold.id}}" ng-repeat="fold in subfolders">{{fold.folder_name}}</option>
                                    </select>
                                </div>
                                
                                <div class="col-md-3">
                                    <label id="upload" name="upload">Upload</label>
                                </div>
                                <div class="col-md-3">
                                   <input type="file" multiple="multiple"  ngf-select ng-model="file_name" ngf-multiple="true" name="file_name" />
                                  
                                   <!-- <div ng-bind="document.file_name.name"></div> -->
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top:15px;">
                        	<div class="form-group" style="margin:15px 15px;font-size:14px;color:#808080;">
                                <div class="col-md-6" style="text-align:right;">
                                	<input type="submit" id="btnsubmit" value="SUBMIT" class="btn btn-primary btn-blue"  ng-click="save(document)"/>
                                  </div> 
                                <div class="col-md-6">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" ng-click="newPage2();"/>
                                </div>
                            </div>
                        </div>
            		</form>
                    </div>
                </div>
            </div>
            </div>
			<!-- Footer -->
           <footer class="main">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>
</body>
</html>