<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">
<link rel="stylesheet"
	href="${context}/mdi/assets/css/font-icons/entypo/css/entypo.css">

<%@include file="../../content/header.jsp"%>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript" src="${context}/mdi/js/js/angular.min.js"></script>
<script type="text/javascript"
	src="${context}/mdi/js/scripts/mdicontrollers/MetaDataController.js"></script>
</head>
<body ng-app="mdiApp" class="page-body skin-facebook"
	ng-controller="MetaDataController">
	<%@include file="../../content/sideheader.jsp"%>
	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="${context}/mdi/images/Dms-Logo.png" alt="" />
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<li>Welcome : <%=user.getUm_fullname()%></li>
				</ul>
			</div>
		</div>
		<hr />
		<!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
		<div class="row">

			<div class="col-md-5">
				<div class="row">
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Department</label>
						</div>
						<div class="col-md-7">
							<select id="rep-id" class="form-control"
								ng-model="folderform.dept_id"
								ng-change="getSubDept(folderform.dept_id)">
								<option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>

							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Sub Department</label>
						</div>
						<div class="col-md-7">
							<select id="rep-id" class="form-control"
								ng-model="folderform.sub_dept_id"
								ng-change="getDocumentTypes(folderform.sub_dept_id)">
								<!-- ng-change="getindexfields(folderform.sub_dept_id)"> -->
								<option value="{{subdept.sub_dept_id}}"
									ng-repeat="subdept in subdepartments">{{subdept.sub_dept_name}}</option>

							</select>
						</div>
					</div>
				</div>

				<!-- Document type  -->
				<div class="row">
					<div class="form-group"
						style="font-size: 14px; color: #808080; padding: 10px 0px;">
						<div class="col-md-4"
							style="text-align: left; font-size: 14px; color: #4a4a4a;">
							<label id="metadata" for="metadata">Document Type</label>
						</div>
						<div class="col-md-7">
							<select id="rep-id" class="form-control"
								ng-model="folderform.dt_id"
								ng-change="getindexfields(folderform.dt_id)">
								<option value="{{dt.dt_id}}" ng-repeat="dt in documentTypeList">{{dt.dt_name}}</option>

							</select>
						</div>
					</div>
				</div>
				<!-- <div class="form-group" ng-repeat="data in indexDataList "> -->
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane active" id="profile-7">
							<table id="myTable1"
								class="table table-bordered table-responsive">
								<thead>
									<tr>
										<th>Field Name</th>
										<th>Field Type</th>

										<th style="text-align: center;">Edit</th>
										<th>Delete</th>

									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="data in indexDataList">
										<td>{{data.if_name}}</td>
										<td>{{data.if_type}}</td>

										<td style="text-align: left;">
											<button type="button"
												class="btn btn-primary btn-sm pull-right"
												data-toggle="modal" data-target="#user_Modal"
												ng-click="getIndexData($index)">
												<span class="glyphicon glyphicon-plus-sign"></span> UPDATE
											</button>

										</td>
										<td style="text-align: left;">
											<button type="button"
												class="btn btn-primary btn-sm pull-right"
												data-toggle="modal" ng-click="deleteIndexField($index)">
												<span class="glyphicon glyphicon-plus-sign"></span> DELETE
											</button>
										</td>

									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- add New Model  -->

				<div class="modal fade" id="user_New_Modal" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">
									<span ng-if="!masterentity.um_id"><strong>ADD
											NEW METAFIELD</strong></span>
								</h4>
							</div>
							<form id="documentupload">
								<div class="row">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6">
											<label id="metadata" for="metadata">Field Name</label>
										</div>
										<div class="col-md-6">
											<input type="text" name="metatemp" id="if_name"
												class="form-control" ng-model="updateindexfield.if_name"
												value="{{index_field.if_name}}" autocomplete="off" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6">
											<label id="rep-name" for="Rep-name">Field Type</label>
										</div>
										<div class="col-md-6">
											<select id="rep-id" class="form-control"
												ng-model="updateindexfield.if_type">
												<option value="text">text</option>
												<option value="dropdown">dropdown</option>
												<option value="check box">check box</option>
												<option value="radio button">radio button</option>
											</select>
										</div>

									</div>
								</div>
								<div class="row" ng-if="showpeimarydiv">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6">
											<label id="rep-name" for="Rep-name">Select Primary</label>
										</div>
										<div class="col-md-6">
											Yes<input type="radio" name="primary" value="1" ng-model="updateindexfield.if_add_multiple"> No<input
												value="0"  type="radio" name="primary" ng-model="updateindexfield.if_add_multiple">
										</div>

									</div>
								</div>


								<div class="row" style="padding-top: 15px;">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6" style="text-align: right;">
											<input type="submit" id="btnsubmit" value="SUBMIT"
												class="btn btn-primary btn-blue"
												ng-click="addIndexData(updateindexfield)" />
										</div>
										<div class="col-md-6">
											<input type="Button" id="btnback" value="BACK"
												class="btn btn-primary btn-blue" ng-click="clickBack()" />
										</div>
									</div>
								</div>
							</form>
							<%-- <%@ include file="../user/_master_form.jsp"%> --%>
						</div>
					</div>
				</div>

				<!-- Update Modal -->
				<div class="modal fade" id="user_Modal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">
									<span ng-if="!masterentity.um_id"><strong>
											Update Meta Fields</strong></span> <span ng-if="masterentity.um_id"><Strong>Update
											User</Strong></span>
								</h4>
							</div>
							<form id="documentupload">
								<div class="row">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6">
											<label id="metadata" for="metadata">Field Name</label>
										</div>
										<div class="col-md-6">
											<input type="text" name="metatemp" id="if_name"
												class="form-control" ng-model="updateindexfield.if_name"
												value="{{index_field.if_name}}" autocomplete="off" disabled="disabled"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6">
											<label id="rep-name" for="Rep-name">Field Type</label>
										</div>
										<div class="col-md-6">
											<select id="rep-id" class="form-control"
												ng-model="updateindexfield.if_type">
												<option value="text">text</option>
												<option value="dropdown">dropdown</option>
												<option value="check box">check box</option>
												<option value="radio button">radio button</option>
											</select>
										</div>

									</div>
								</div>
								<div class="row" ng-if="showpeimarydiv">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6">
											<label id="rep-name" for="Rep-name">Select Primary</label>
										</div>
										<div class="col-md-6">
											Yes<input type="radio" name="primary" ng-model="updateindexfield.if_add_multiple" value="1"> No<input
												type="radio" value="0" name="primary" ng-model="updateindexfield.if_add_multiple">
										</div>

									</div>
								</div>


								<div class="row" style="padding-top: 15px;">
									<div class="form-group"
										style="margin: 15px 15px; font-size: 14px; color: #808080;">
										<div class="col-md-6" style="text-align: right;">
											<input type="submit" id="btnsubmit" value="SUBMIT"
												class="btn btn-primary btn-blue"
												ng-click="addIndexData(updateindexfield)" />
										</div>
										<div class="col-md-6">
											<input type="Button" id="btnback" value="BACK"
												class="btn btn-primary btn-blue" ng-click="clickBack()" />
										</div>
									</div>
								</div>
							</form>
							<%-- <%@ include file="../user/_master_form.jsp"%> --%>
						</div>
					</div>
				</div>

				<div class="row" style="padding: 15px 0px;">
					<div class="form-group" style="font-size: 14px; color: #808080;">
						<div class="col-md-6" style="text-align: right;">
							<button type="button" class="btn btn-primary btn-sm pull-right"
								data-toggle="modal" data-target="#user_New_Modal"
								ng-click="getIndexFieldsData()">
								<span class="glyphicon glyphicon-plus-sign"></span> ADD NEW
							</button>
						</div>
						<div class="col-md-6">
							<input type="Button" id="btnback" value="BACK"
								class="btn btn-primary btn-blue" onclick="newPage2();" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer -->
		<div>
			<!-- <footer class="main" style="position: fixed; bottom: 0px;">
				Copyright � <strong>stockholdingdms</strong>2018. All Rights
				Reserved
			</footer> -->
		</div>

	</div>


</body>
</html>