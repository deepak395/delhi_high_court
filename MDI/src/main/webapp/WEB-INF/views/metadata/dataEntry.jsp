<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Entry</title>
<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
<script type="text/javascript"
	src="${context}/mdi/js/scripts/mdicontrollers/dataEntryController.js"></script>
<script type="text/javascript">
	
</script>
<%-- <%@ page import="com.mdi.model.User"%>	
<%@page import="com.mdi.model.UserRole"%>
<%@ page import="java.util.List" %> --%>
<%@include file="../../content/header.jsp"%>
</head>
<%-- <% 
User user1 = null;
if(session.getAttribute("USER")!=null)
	 user1 = (User)session.getAttribute("USER");

%>
<% 
				List<UserRole> userroles=user1.getUserroles();
				Boolean dataentryop=false;
				UserRole userr=null;
				for(UserRole userrole:userroles){
					 userr=userrole;                                        /* || userrole.getLk().getLk_longname().equals("Data Entry Operator (I/O)") */
				}
%> --%>

<body ng-app="mdiApp" ng-controller="dataEntryController"
	ng-init="getDocumentData(${doc_id})">

	<%@include file="../../content/sideheader.jsp"%>
	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-4 col-sm-2 clearfix">
				<img src="${context}/mdi/images/Dms-Logo.png" alt="" />
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>

			<div class="col-md-6 col-sm-4 clearfix" style="padding-top: 16px;">
				<h2 style="color: #03577a; font-weight: 600;">Electronic
					Document Management Solution</h2>
				<!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
			</div>
			<!-- Raw Links -->
			<div class="col-md-2 col-sm-2 clearfix" style="float: right;">

				<ul class="user-info pull-right pull-none-xsm">
					<li>Welcome : <%= user.getUm_fullname() %></li>
				</ul>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-7">




				<div class="modal-dialog" id="modal1"
					style="margin-top: -35px; overflow: auto;">

					<div class="modal-content">

						<div class="row">
						<iframe src="{{fileurl}}" height="600" width="100%"></iframe>
							<!-- <object type="application/pdf" data="{{fileurl}}" width="100%"
								height="600"> </object> -->
							<!--  <embed src="{{fileurl}}"
								style="width: 579px; margin-left: 15px;" width="600"
								height="500" alt="pdf">  -->
							<!--pluginspage="http://www.adobe.com/products/acrobat/readstep2.html"  -->
						</div>
					</div>
				</div>


				<%-- <img src="${context}/mdi/uploads/pri.pdf"
					style="width: 720px;"> --%>
			</div>
			<div class="col-md-5">


				<form action='saveMetaData' method="post">
					<div style="display: none;">
						<input type="text" value='{{doc_complete_data.doc_id}}'
							name="document_id">

					</div>


					<div class="row">
						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">
							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">
								<label id="metadata" for="metadata">File Name</label>
							</div>
							<div class="col-md-7">
								<span>{{doc_complete_data.doc_name}}</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">
							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">
								<label id="metadata" for="metadata">Department</label>
							</div>
							<div class="col-md-7">
								<span>{{doc_complete_data.dept_name}}</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group"
							style="font-size: 14px; color: #808080; padding: 10px 0px;">
							<div class="col-md-4"
								style="text-align: left; font-size: 14px; color: #4a4a4a;">
								<label id="metadata" for="metadata">Sub Department</label>
							</div>
							<div class="col-md-7">{{doc_complete_data.sub_dept_name}}</div>
						</div>
					</div>



					<div class="form-group"
						ng-repeat="data in doc_complete_data.indexfields">


						<!-- Drop Down -->

						<div class="row" ng-if="(data.if_type=='dropdown')">
							<div class="form-group"
								style="font-size: 14px; color: #808080; padding: 10px 0px;">
								<div class="col-md-4"
									style="text-align: left; font-size: 14px; color: #4a4a4a;">
									<label id="metadata" for="metadata">{{data.if_name}}</label>
								</div>
								<div class="col-md-7">
									<input ng-if="data.meta_value !=null && showtxt=='Y'"
										type="text" value="{{data.meta_value}}" disabled="disabled">
									<select id="rep-id" class="form-control" name="datadrop"
										ng-if="data.meta_value ==null || show=='Y'">
										<!-- ng-options="datafield.lk_longname as datafield.lk_longname for datafield in data.if_value_drop" -->
										<option value="" class=""></option>
										<!-- style="display: none;" -->
										<option value="{{datafield.lk_longname}}"
											ng-repeat="datafield in data.if_value_drop">{{datafield.lk_longname}}</option>
									</select>
								</div>
								<!-- <p>{{fieldsdata}}</p> -->
							</div>
						</div>

						<!--     Text  -->
						<div class="row" ng-if="(data.if_type=='text')">
							<div class="form-group"
								style="font-size: 14px; color: #808080; padding: 10px 0px;">
								<div class="col-md-4"
									style="text-align: left; font-size: 14px; color: #4a4a4a;">
									<label id="frt-pet" for="Ft-petit">{{data.if_name}}</label>
								</div>
								<div class="col-md-7">
									<input type="text" name="Ft-petit" class="form-control"
										name="datatext" value="{{data.meta_value}}" />
								</div>
							</div>
						</div>

						<!--  Check Box -->

						<div class="row" ng-if="(data.if_type=='check box')">
							<div class="form-group"
								style="font-size: 14px; color: #808080; padding: 10px 0px;">
								<div class="col-md-4"
									style="text-align: left; font-size: 14px; color: #4a4a4a;">
									<label id="coc" for="coc">{{data.if_name}}</label>
								</div>
								<div class="col-md-7">
									<input type="checkbox" id="COC" value="Contempt of Court"
										name="datachbox" value="{{data.meta_value}}" />
								</div>
							</div>
						</div>

						<!--  Date -->
						<div class="row" ng-if="(data.if_type=='date')">
							<div class="form-group"
								style="font-size: 14px; color: #808080; padding: 10px 0px;">
								<div class="col-md-4"
									style="text-align: left; font-size: 14px; color: #4a4a4a;">
									<label id="coc" for="coc">{{data.if_name}}</label>
								</div>
								<div class="col-md-7">
									<input ng-if="data.meta_value_if_date ==null || show =='Y'"
										type="date" name="datadate" pattern="dd/MM/YYYY"
										valueAsDate="{{data.meta_value}}" /> <input
										ng-if="data.meta_value_if_date !=null && showtxt=='Y'"
										type="text" name="datadate" value="{{data.meta_value}}"
										disabled="disabled" />
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="padding: 15px 0px;">
						<div class="form-group" style="font-size: 14px; color: #808080;">
							<!-- <div class="col-md-6" style="text-align: right;"> -->
							<input type="submit" id="btnsubmit" value="SAVE"
								class="btn btn-primary btn-blue" /> <input type="button"
								id="btnedit" value="EDIT" ng-click="editEnable()"
								class="btn btn-primary btn-blue" /> <input type="Button"
								id="btnnext" value="NEXT STAGE"
								ng-if="doc_complete_data.dfd_md_status=='YES'"
								ng-click="changeDocumentFileStage()"
								class="btn btn-primary btn-blue" />
							<%if((userr.getLk().getLk_longname().equals("MDIChecker"))) {%>
							<input type="Button" id="reject" value="Reject"
								class="btn btn-primary btn-blue" data-toggle="modal"
								data-target="#myModal" />
							<% } %>
							<input type="Button" id="btnback" value="BACK"
								class="btn btn-primary btn-blue" />
						</div>

					</div>
				</form>
			</div>

		</div>
		<!-- Modal -->
		<div class="modal fade" id="Reject_Model" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"></div>

	</div>
	<!-- Footer -->

	



</body>
</html>