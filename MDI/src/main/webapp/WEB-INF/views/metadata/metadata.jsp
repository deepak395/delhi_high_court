<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
   <%@ include file="../../content/header.jsp" %>
    <script type="text/javascript">
    function newPage() {
		window.location.href = "Dashboard.html";
	}
	function newPage2() {
		window.location.href = "Metadata-entry.html";
	}
</script>

<script type="text/javascript" src="${context}/mdi/js/js/angular.js"></script>
    <script type="text/javascript" src="${context}/mdi/js/scripts/mdicontrollers/MetaDataController.js"></script>
</head>
<body ng-app="mdiApp" class="page-body skin-facebook" data-url="http://neon.dev"  ng-controller="MetaDataController">
        <%@include file="../../content/sideheader.jsp" %>
        <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-4 col-sm-2 clearfix">
                    <img src="${context}/mdi/images/Dms-Logo.png" alt="" />
                    <!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
                </div>

				<div class="col-md-6 col-sm-4 clearfix" style="padding-top:16px;">
                    <h2 style="color:#03577a;font-weight:600;">Electronic Document Management Solution</h2>
                    <!--<img src="~/images/headingNew.png" style="margin-left: 110px;" />-->
                </div>
                <!-- Raw Links -->
                <div class="col-md-2 col-sm-2 clearfix" style="float: right;">

                    <ul class="user-info pull-right pull-none-xsm">
                       <li>Welcome : <%= user.getUm_fullname() %></li>
                    </ul>
                </div>
            </div>
            <hr />
            <!--<div class="row">
                <div class="col-md-12">
                	<div id="divAction" style="float: right;margin-bottom:5px">
        <a href="#" class="btn btn-primary"><i class="entypo-user-add"></i>Document Upload</a>
    				</div>
            	</div>
             </div>-->
           		<div class="row">
                    <div class="col-md-7" >
                        <img src="${context}/mdi/images/rough-pdfimg.jpg" style="    width: 720px;">
                    </div>
                    <div class="col-md-5">
                        <div class="row">
                                    <div class="form-group" style="font-size:14px;color:#808080;padding:10px 0px;">
                                        <div class="col-md-4" style="text-align:left;font-size:14px;color:#4a4a4a;">
                                            <label id="metadata" for="metadata">Department</label>
                                        </div>
                                        <div class="col-md-7">
                                           <select id="rep-id" class="form-control" ng-model="folderform.dept_id" ng-change="getSubDept(folderform.dept_id)">
                                                <option value="{{dept.dept_id}}" ng-repeat="dept in departments">{{dept.dept_name}}</option>
                                                
                                            </select>
                                        </div>
                                     </div>
                         </div>
                         <div class="row">
                                    <div class="form-group" style="font-size:14px;color:#808080;padding:10px 0px;">
                                        <div class="col-md-4" style="text-align:left;font-size:14px;color:#4a4a4a;">
                                            <label id="metadata" for="metadata">Sub Department</label>
                                        </div>
                                        <div class="col-md-7">
                                           <select id="rep-id" class="form-control" ng-model="folderform.sub_dept_id" ng-change="getindexfields(folderform.sub_dept_id)">
                                                <option value="{{subdept.sub_dept_id}}" ng-repeat="subdept in subdepartments">{{subdept.sub_dept_name}}</option>
                                                
                                            </select>
                                        </div>
                                     </div>
                         </div>
                         
                         <div><input type="button" value="DataEntry" ng-click="gotoDataEntry()"></div>
                         
                          <div class="form-group" ng-repeat="data in indexDataList ">
                          
                      
                         
                     			<div class="row" ng-if="(data.if_type=='dropdown')" ng-init="getfielddata(data.if_id)">
                                    <div class="form-group" style="font-size:14px;color:#808080;padding:10px 0px;">
                                        <div class="col-md-4" style="text-align:left;font-size:14px;color:#4a4a4a;">
                                            <label id="metadata" for="metadata">{{data.if_name}}</label>
                                           <!--  <p>{{getfielddata(data.if_id)}}</p> -->
                                        </div>
                                        <div class="col-md-7">
                                           <select id="rep-id" class="form-control" ng-model="folderform.if_id">
                                                <option value="{{datafield.lk_id}}" ng-repeat="datafield in fieldsdata">{{datafield.lk_longname}}</option>
                                            </select>
                                        </div>
                                        <!-- <p>{{fieldsdata}}</p> -->
                                     </div>
                         		 </div>
                         <div class="row" ng-if="(data.if_type=='text')">
                            <div class="form-group"  style="font-size:14px;color:#808080;padding:10px 0px;">
                                <div class="col-md-4" style="text-align:left;font-size:14px;color:#4a4a4a;">
                                    <label id="frt-pet" for="Ft-petit">{{data.if_name}}</label>
                                </div>
                                <div class="col-md-7">
                                   <input type="text" name="Ft-petit" class="form-control"/>
                                </div>
                            </div>
                            </div>
                              <div class="row" ng-if="(data.if_type=='check box')">
                            <div class="form-group" style="font-size:14px;color:#808080;padding:10px 0px;">
                                <div class="col-md-4" style="text-align:left;font-size:14px;color:#4a4a4a;">
                                    <label id="coc" for="coc">{{data.if_name}}</label>
                                </div>
                                <div class="col-md-7">
                                   <input type = "checkbox" id="COC" value = "Contempt of Court" />
                                </div>
                            </div>
                        </div>
                        </div>
                         	
                        <div class="row" style="padding:15px 0px;">
                            <div class="form-group" style="font-size:14px;color:#808080;">
                                <div class="col-md-6" style="text-align:right;">
                                    <input type="submit" id="btnsubmit" value="SUBMIT" class="btn btn-primary btn-blue" onclick="alert('Data Sumbited Successfully!'); newPage();" />
                                  </div> 
                                <div class="col-md-6">
                                   <input type="Button" id="btnback" value="BACK" class="btn btn-primary btn-blue" onclick="newPage2();"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<!-- Footer -->
           <footer class="main" style="position:fixed;bottom:0px;">
                Copyright � <strong>stockholdingdms</strong>2018. All Rights Reserved
            </footer>
    	</div>
    </div>

    <!--<div class="modal fade" id="modal-ChangePassword1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>

                <div id="divBodyPE" class="modal-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label for="lblOldPassword" class="col-sm-3 control-label">Old Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtOldPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblNewPassword" class="col-sm-3 control-label">New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtNewPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lblConfirmPassword" class="col-sm-3 control-label">Confirm Password:</label>
                            <div class="col-sm-4">
                                <input type="password" id="txtConfirmPassword" class="form-control input-sm" autocomplete="new-password" />
                                <span id="errorUserEmail" style="color: Red; display: none">*</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="lblErrorPassword" style="display:none;color:red;"></label>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="button" id="btnChangePassword" class="btn btn-info" value="Change" />
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-PasswordSent" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Client Tariff Master</h4>
                </div>

                <div id="divBodySent" class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" id="btnOk" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>-->
</body>
</html>
