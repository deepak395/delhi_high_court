var dashboard = angular.module("mdiApp", ['smart-table']);

dashboard.controller("dashboardcontroller",function($scope,$http) 
{
	
	$scope.userid='';
	$scope.test='Ram';
	getChangeData();
	var urlBase='/mdi/';
	$scope.masterdata = [];
	$scope.recentdata=[];
	$scope.showRDUdiv = true;
	$scope.showMVDdiv = true;
	$scope.showRUFdiv = true;
	$scope.showRMEdiv = true;
	$scope.showCHFdiv = true;
	$scope.showdiv    = true;
	$scope.showmostviewdoc;
	$scope.showmostviewdocdata='';
	$scope.doc_count  = '';
	$scope.mostviewed = [];
	
	/*loadMasterData();*/
	/*loadRecentData();*/
	 $scope.templateCreationUrl = function()
	 {
		 debugger;
		 window.location.href = "mdi/metatemplate/metaTemplateCreation"
	 };
	
	 function getChangeData() 
		{
			$http.get('/mdi/'+'user/getUsersDataById').success(function (data) {
	            	$scope.masterdata = data; 
	            	if($scope.masterdata.isfirstlogin ==true)
	            	{
	            		$('#pass_Modal').modal('show');
	            	}
	            	$scope.displayedCollection = [].concat($scope.masterdata);
	            	console.log("data");
	            }).
	            error(function(data, status, headers, config) {
	            	console.log("Error in getting User data");
	            });
	    };
	 
		function loadMasterData() {
			var response = $http.get(urlBase+'/dashboard/getreport');
			response.success(function(data, status, headers, config) {		
				console.log("doc count");
				$scope.masterdata= data.modelList;
				console.log("-----------*----------------");
				console.log($scope.masterdata);
				$scope.doc_count = $scope.masterdata[0].value1;
				$scope.mostviewed = $scope.masterdata[0].dfd1;
				console.log($scope.masterdata[0].dfd1)
			});
			response.error(function(data, status, headers, config) {
				
			});
			
		};
		
		function loadRecentData() {
			var response = $http.get(urlBase+'/dashboard/getrecentdata');
			response.success(function(data, status, headers, config) {		
				console.log("get Report");
				$scope.recentdata= data.modelList;
				
				console.log("-----------*----------------");
				console.log($scope.recentdata);
			});
			response.error(function(data, status, headers, config) {
				//alert("Error");
			});
			
		};
		
		
		$scope.setUserId = function(role_id,user_id,user)
		{
			$scope.userid=user_id;
			
			if(role_id==350992 )     /*uploader*/
			{
				$scope.showRDUdiv = false;
				$scope.showMVDdiv = false;
				$scope.showRUFdiv = true;
				$scope.showRMEdiv = false;
				$scope.showCHFdiv = false;
				$scope.showdiv    = true;
				loadMasterData();
				
				var response = $http.get(urlBase+'/dashboard/getrecentdata');
				response.success(function(data, status, headers, config) {		
					console.log("get Report");
					$scope.recentdata= data.modelList;
					
					console.log("-----------*----------------");
					console.log($scope.recentdata);
				});
				response.error(function(data, status, headers, config) {
					//alert("Error");
				});
			}
			
			if( role_id==350994)     /*MetaData*/
			{
				debugger
				$scope.showRDUdiv = false;
				$scope.showMVDdiv = false;
				$scope.showRUFdiv = false;
				$scope.showRMEdiv = true;
				$scope.showCHFdiv = false;
				$scope.showdiv    = true;
				loadMasterData();
				
				var response = $http.get(urlBase+'/dashboard/getRecentMetaEntryFiles');
				response.success(function(data, status, headers, config) {		
					console.log("durga");
					$scope.recentdata= data.modelList;
					
					console.log("-----------*----------------");
					console.log($scope.recentdata);
				});
				response.error(function(data, status, headers, config) {
					//alert("Error");
				});
				
				
			}
			
			if(role_id==350993)    /*  checker  */
			{
				$scope.showRDUdiv = false;
				$scope.showMVDdiv = false;
				$scope.showRUFdiv = false;
				$scope.showRMEdiv = false;
				$scope.showCHFdiv = true;
				$scope.showdiv    = true;
				loadMasterData();
				
				var response = $http.get(urlBase+'/dashboard/getRecentCheckedFiles');
				response.success(function(data, status, headers, config) {		
					console.log("durga");
					$scope.recentdata= data.modelList;
					
					console.log("-----------*----------------");
					console.log($scope.recentdata);
				});
				response.error(function(data, status, headers, config) {
					//alert("Error");
				});
			}
			
			if(role_id==350991)    /*   admin  */
			{
				$scope.showRDUdiv = true;
				$scope.showMVDdiv = true;
				$scope.showRUFdiv = false;
				$scope.showRMEdiv = false;
				$scope.showCHFdiv = false;
				$scope.showdiv    = false;
				$scope.showmostviewdoc = true;
				loadMasterData();
				
				var response = $http.get(urlBase+'/dashboard/getUploadedDataAdmin');
				response.success(function(data, status, headers, config) {		
					
					$scope.recentdata= data.modelList;
					
					console.log("-----------*----------------");
					console.log($scope.recentdata);
				});
				response.error(function(data, status, headers, config) {
					//alert("Error");
				});
				
				var response1 = $http.get(urlBase+'/dashboard/getviewedDocCount');
				response1.success(function(data, status, headers, config) {		
					$scope.showmostviewdocdata = data.data;
				});
				response1.error(function(data, status, headers, config) {
					//alert("Error");
				});
			}
			if(role_id==1 || role_id==11 || role_id==10000580)    /*   Sysadmin  */
			{
				$scope.showRDUdiv = true;
				$scope.showMVDdiv = true;
				$scope.showRUFdiv = false;
				$scope.showRMEdiv = false;
				$scope.showCHFdiv = false;
				$scope.showdiv    = false;
				$scope.showmostviewdoc = true;
				loadMasterData();
				
				var response = $http.get(urlBase+'/dashboard/getUploadedDataSysAdmin');
				response.success(function(data, status, headers, config) {		
					console.log("Virendra admin");
					$scope.recentdata= data.modelList;
					
					console.log("-----------*----------------");
					console.log($scope.recentdata);
				});
				response.error(function(data, status, headers, config) {
					//alert("Error");
				});
				
				var response1 = $http.get(urlBase+'/dashboard/getviewedDocCount');
				response1.success(function(data, status, headers, config) {		
					$scope.showmostviewdocdata = data.data;
				});
				response1.error(function(data, status, headers, config) {
					//alert("Error");
				});
			}
			
		};
		
		   $scope.changePassword = function(data) {
				
				console.log(data);
				console.log("********");

						
					$scope.passwordn;
					$scope.passwordcn;
					$scope.entity={'password':$scope.passwordn,'confirmpassword':$scope.passwordcn,'um_id':$scope.userid}
				
					console.log($scope.entity);
				var response = $http.post(urlBase+'user/changepassword',
						$scope.entity);
				response.success(function(data, status, headers, config) {
					if (data.response == "TRUE") {
					
						$('#pass_Modal').modal('hide');		
						$scope.errorlist=null;
						$('.form-group').removeClass('has-error');
						bootbox.alert("Password changed Successfully!");
						
						//$scope.errorlist = [];
						//$scope.ansDetails = {};

					} else {

						$scope.errorlist = data.dataMapList;
						/*$.each($scope.errorlist, function(k, v) {
							$("#" + k).parent().parent().addClass('has-error');
						});*/
						//bootbox.alert("Password and Confirm Password not matched");
						$scope.ansDetails = [];
					
					}

				});
				response.error(function(data, status, headers, config) {
					console.log("Error in getting Master");
				});

		  
		}
		
});