var mdiApp= angular.module('mdiApp', []);

mdiApp.controller('dataEntryController',function($scope,$http,$window) 
{
	var urlBase='/mdi'
	$scope.indexData={};
	$scope.doc_complete_data={};
    $scope.metaDataForm={};
    $scope.helpForm={};
    $scope.show='';
    $scope.showtxt='';
    $scope.showBtnNextStage='';
    $scope.doc_id='';
    $scope.fileurl='';
	$scope.file_viewData='';
    
	$scope.getDocumentData = function(value)
	{
		var response = $http.get(urlBase+'/metadata/getIndexFieldsDataMetaData',{params:{'dept_id':value}});
    	response.success(function(data, status, headers, config) 
    	{		
    		console.log(data);
    		$scope.indexData=data.modelList;
    		$scope.doc_complete_data=data.modelData;
    		$scope.viewfile($scope.doc_complete_data.doc_id);
    		$scope.showtxt='Y';
    		//$scope.fileurl='/mdi/uploads/'+data.data;
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	}
	
	$scope.viewfile =function(value)
	{
		        $http.get('/mdi/search/viewFile?dfd_id='+value).success(function (data) 
				 {
							console.log(data);
							$scope.fileurl='/mdi/uploads/'+data.data;
							//$scope.viewDfdData=data.modelData;
							            
				 }).
						  error(function(data, status, headers, config) {
							        	console.log("Error in getting User data");
				});
	}

	
	$scope.saveMeataData = function()
	{
		console.log($scope.data)
	};
	
	$scope.editEnable = function()
	{
		$scope.show='Y'
		$scope.showtxt='N'
	}
	
	$scope.changeDocumentFileStage= function()
	{
		var response = $http.get(urlBase+'/metadata/changeDocfileStage',{params:{'doc_id':$scope.doc_complete_data.doc_id}});
    	response.success(function(data, status, headers, config) 
    	{		
    		if(data.response=="TRUE")
    	    {
    			alert("FILE MOVED TO NEXT STAGE");
    			window.location.href=urlBase+"/metadata/manage";
    	    }
    		else
    		{
    			bootbox.alert("Something Is Wrong");
    		}
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	}
	
});