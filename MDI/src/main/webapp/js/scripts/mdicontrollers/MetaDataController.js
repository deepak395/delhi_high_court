var mdiApp= angular.module('mdiApp', []);

mdiApp.controller('MetaDataController',function($scope,$http,$window) 
{
	$scope.tx='Virendra';
	var urlBase='/mdi'
	$scope.folderform={};
	$scope.repositories=[];
	$scope.departments=[];
	$scope.subdepartments=[];
	$scope.indexDataList=[];
	$scope.fieldsdata=[];
	$scope.index_field={};
	$scope.updateindexfield={};
	$scope.subdept_id='';
	$scope.documentTypeList=[];
	$scope.primary;
	$scope.if_add_multiple='';
	$scope.showpeimarydiv;
	
	var count=0;
	
	getAllData();
	
	function getAllData()
    {
	//get Departments
	var response = $http.get(urlBase+'/metadata/getAllDepartments');
	response.success(function(data, status, headers, config) {		
		console.log("== GET LOOK UP MASTER ==");
		console.log(data);
		$scope.departments=data;
		
	});
	response.error(function(data, status, headers, config) {
		console.log("Error");
	});
    };
    $scope.getSubDept = function(value)
	{
    	$scope.documentTypeList=[];
    	$scope.indexDataList=[];
    	$scope.folderform.sub_dept_id='';
    	$scope.folderform.dt_id=''
    	//alert(value);
    	var response = $http.get(urlBase+'/metadata/getSubDept',{params:{'dept_id':value}});
    	response.success(function(data, status, headers, config) {		
    		console.log("== GET Sub Department MASTER ==");
    		console.log(data);
    		$scope.subdepartments=data;
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};
	$scope.getindexfields = function(value)
	{
		debugger
		$scope.subdept_id=value;
		//alert(value);
    	var response = $http.get(urlBase+'/metadata/getindexfields',{params:{'sub_dept_id':value}});
    	response.success(function(data, status, headers, config) 
    	{		
    		console.log("== GET Index Fields MASTER ==");
    		console.log(count);
    		console.log(data);
    		$scope.indexDataList=data.modelList;
    		$scope.showpeimarydiv=data.data;
    		if(data.data==false)
    			{
    			  $scope.updateindexfield.if_add_multiple = '0';
    			}
    		/*var i;
    		for(i=0;i<$scope.indexDataList.length;i++)
    		{
    			var pri = $scope.indexDataList[i].if_type_code;
    			
    		}*/
    		count= count+1;
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};
	
	$scope.getfielddata = function(value)
	{
		debugger
		//alert(value);
		$scope.subdept_id=value;
    	var response = $http.get(urlBase+'/metadata/getfielddata',{params:{'if_id':value}});
    	response.success(function(data, status, headers, config) {
    		console.log("== GET Field Data MASTER ==");
    		console.log(data);
    		$scope.fieldsdata.push(data);
    		
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};

	$scope.getIndexData= function(index)
	{
		
		$scope.index_field = $scope.indexDataList[index];
		$scope.updateindexfield.if_type=$scope.index_field.if_type;
		$scope.updateindexfield.if_id =$scope.index_field.if_id;
		$scope.updateindexfield.if_name = $scope.index_field.if_name;
		
		$scope.updateindexfield.if_add_multiple= $scope.index_field.if_add_multiple;
		
	};
	
	
	
	$scope.updateIndexData= function()
	{
		$scope.updateindexfield.if_name=$scope.index_field.if_name;
		$http.post(urlBase+'/metadata/updatemetadata',$scope.updateindexfield).success(function (data) 
		{
            if(data.response=="TRUE")
            {
            	bootbox.alert("Index Field Updated Successfully!");
            	$('#user_Modal').modal('hide');	
            	$scope.getindexfields($scope.subdept_id);
            }
            else
            {
            	bootbox.alert("Index Field Not Updated Successfully!");
            }
            
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting User data");
        });
	};
	
	$scope.deleteIndexField = function(value)
	{
		$scope.index_field = $scope.indexDataList[value];
		
		var id =$scope.index_field.if_id;
		$http.post(urlBase+'/metadata/deletemetadata',id).success(function (data) 
				{
		            if(data.response=="TRUE")
		            {
		            	bootbox.alert("Index Field Deleted Successfully!");
		            	$('#user_Modal').modal('hide');
		            	$scope.getindexfields($scope.subdept_id);
		            }
		            else
		            {
		            	bootbox.alert("Index Field Not Deleted Successfully!");
		            }
		            
		        }).
		        error(function(data, status, headers, config) {
		        	console.log("Error in getting User data");
		        });
	};
	
	$scope.addIndexData=function(value)
	{
		debugger
		alert($scope.if_add_multiple+"     ");
		var dpt_id = $scope.folderform.dept_id;
		var sub_dept_id =$scope.folderform.sub_dept_id;
		var dt_id = $scope.folderform.dt_id;
		$scope.updateindexfield.if_parent= dt_id;
		
		if($scope.updateindexfield.if_add_multiple==undefined || $scope.updateindexfield.if_add_multiple=='')
		{	
		    $scope.updateindexfield.if_add_multiple = '0';
		    
		}
		else
		{
			//$scope.updateindexfield.if_add_multiple =$scope.if_add_multiple;
		}
		
		$http.post(urlBase+'/metadata/addmetadata',$scope.updateindexfield).success(function (data) 
				{
		            if(data.response=="TRUE")
		            {
		            	bootbox.alert("Record Added Successfully!");
		            	$scope.getindexfields($scope.subdept_id);
		            	$('#user_New_Modal').modal('hide');
		            	$('#user_Modal').modal('hide');
		            	$scope.updateindexfield={};
		            }
		            else
		            {
		            	bootbox.alert("Record Not Added Successfully!");
		            }
		            
		        }).
		        error(function(data, status, headers, config) {
		        	console.log("Error in getting User data");
		        });
		
	};
	
	$scope.gotoDataEntry = function()
	{
		debugger
		window.open(urlBase+'/metadata/dataEntry?id=81');
	};
	
	
	/******* will be worked later********/
	$scope.getIndexFieldsData = function()
	{
		
		        /*$http.get(urlBase+'/metadata/getIndexFieldsFromLookup').success(function (data) 
				{
		            
		            
		        }).
		        error(function(data, status, headers, config) {
		        	console.log("Error in getting User data");
		        });*/
	};
	
	
	$scope.getDocumentTypes = function (sub_dept_id)
	{
		var response = $http.get(urlBase + '/UploadDcoument/getDocumentType',
		{
					params : 
					{
						'sub_dept_id' : sub_dept_id
					}
		});
		response.success(function(data, status, headers, config) 
		{
			console.log("== GET Document Type MASTER ==");
			console.log(data);
			$scope.documentTypeList = data.modelList;
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	
	
	$scope.clickBack = function()
	{
		$scope.updateindexfield={};
		$('#user_New_Modal').modal('hide');
    	$('#user_Modal').modal('hide');
	};
    
});