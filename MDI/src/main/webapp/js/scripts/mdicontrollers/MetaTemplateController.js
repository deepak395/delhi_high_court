var mdiApp= angular.module('mdiApp', []);

mdiApp.controller('MetaTemplateController',function($scope,$http,$window) 
{
	
	var urlBase='/mdi';
	$scope.repositories='';
    getRepositoryData();
    $scope.dep_details={};
    $scope.addSubDeptparentId='';
    $scope.sho;
    $scope.sho2;
    $scope.doc_type ={};
    	
    $scope.department=
    {
    		
    		dept_id :'0',
    		dept_name : '',
    		dept_description :'',
    		dept_repo_id : '',
    		dept_createdOn : '',
    		dept_createdBy : '',
    		dept_updatedOn : '',
    		dept_updatedBy : '',
    		status  :''
    };
    $scope.SubDepartment=
    {
    		sub_dept_id         :'',
    		sub_dept_name       :'',
    		sub_dept_cr_by      :'',
    		sub_dept_cr_date    :'',
    		sub_dept_rec_status :'',
    		parent_id           :''
    };
    
    $scope.folder=
    {
    		id : '',
    		parent_id : '',
    		rep_id : '',
    		folder_name :'',
    		display_name :'',
    		status : '',
    		dept_id :''
    }
    $scope.subdepartments=[];
    $scope.documenttypes=[];
   
    getAllDepartmentList();
    
    function getRepositoryData()
    {
    	 $scope.sho=false;
    	debugger
    	//getRepository
    	var response = $http.get(urlBase+'/folder/getRepository');
		response.success(function(data, status, headers, config) {	
			debugger
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			$scope.repositories=data;
			console.log($scope.repositories[0].basepath);
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	
	
	function getAllDepartmentList()
	{
		 $scope.sho=false;
		debugger
		var response = $http.get(urlBase+'/metaTemplate/getAllDepartmentList');
		response.success(function(data, status, headers, config) {
			debugger
			console.log("== GET Dept MASTER ==");
			
			$scope.dep_details=data.modelList;
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	
	$scope.createDepartment = function()
	{
		debugger
		var name=$scope.department.dept_name;
		if(name.length>1)
			{
		var response = $http.post(urlBase+'/metaTemplate/saveDepartment', $scope.department);
		response.success(function(data, status, headers, config)
			{	
			
				if(data.response=="TRUE")
				{
					bootbox.alert("TEMPLATE CREATED SUCESSFULLY ");
					$('#user_Modal').modal('hide');	
					
				}
				else
				{
					bootbox.alert("TEMPLATE NOT CREATED ");
					$('#user_Modal').modal('hide');	
				}
				getAllDepartmentList();
				
			});
			response.error(function(data, status, headers, config) {
				console.log("Error");
			});
			}
		else
			{
			  alert("TEMPLATE NAME MUST BE of 2 char");
			  $scope.sho=true;
			}
		
	};
	
	$scope.resetdata = function()
	{
		  $scope.department=
		    {
		    		
		    		dept_id :'0',
		    		dept_name : '',
		    		dept_description :'',
		    		dept_repo_id : '',
		    		dept_createdOn : '',
		    		dept_createdBy : '',
		    		dept_updatedOn : '',
		    		dept_updatedBy : '',
		    		status  :''
		    };
		  
		  $scope.sho = false;
		  $scope.sho2 = false;
	}
	
	$scope.test= function(index)
	{
		
		 $scope.addSubDeptparentId=$scope.dep_details[index].dept_id;
		 resetsubdeptdetails();
		
	};
	
	function  resetsubdeptdetails()
	{
		$scope.SubDepartment=
	    {
	    		sub_dept_id         :'',
	    		sub_dept_name       :'',
	    		sub_dept_cr_by      :'',
	    		sub_dept_cr_date    :'',
	    		sub_dept_rec_status :'',
	    		parent_id           :''
	    };
		$scope.sho2 = false;
	};
	
	$scope.createSubDepartment = function()
	{
		if($scope.SubDepartment.sub_dept_name.length>1)
		{	
		   $scope.SubDepartment.parent_id=$scope.addSubDeptparentId;
		   var response = $http.post(urlBase+'/metaTemplate/saveSubDepartment', $scope.SubDepartment);
		   response.success(function(data, status, headers, config)
			{	
			    //alert(data.response);
				if(data.response=="TRUE")
				{
					bootbox.alert("TEMPLATE CREATED SUCESSFULLY ");
					$('#SubDepartment_Modal').modal('hide');	
					
				}
				else
				{
					bootbox.alert("TEMPLATE NOT CREATED ");
					$('#SubDepartment_Modal').modal('hide');	
				}
				getAllDepartmentList();
				
			});
			response.error(function(data, status, headers, config) {
				console.log("Error");
			});
			
		}
		else
		{
			alert("Sub Department name Must Have 2 Char")
			$scope.sho2=true;
		}
				
	};
	
	$scope. getAllSubDept=function(value)
	{
    	
	    $scope.departmentid=value;
	    $scope.documenttypes=[];
    	var response = $http.get(urlBase+'/UploadDcoumet/getSubDept',{params:{'dept_id':value}});
    	response.success(function(data, status, headers, config) {		
    		console.log("== GET Subdept MASTER ==");
    		console.log("subdept data"+data);
    		$scope.subdepartments=data;
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};
	
	$scope.backButton = function()
	{
		$('#SubDepartment_Modal').modal('hide');
		$('#user_Modal').modal('hide');	
		$('#doc_type').modal('hide');	
	};
	
	$scope.createDocType = function()
	{
		console.log($scope.doc_type);
		if($scope.doc_type.dt_name.length>1)
		{	
		   
		   var response = $http.post(urlBase+'/metaTemplate/saveDocumentType', $scope.doc_type);
		   response.success(function(data, status, headers, config)
			{	
			    //alert(data.response);
				if(data.response=="TRUE")
				{
					bootbox.alert("TEMPLATE CREATED SUCESSFULLY ");
					$('#doc_type').modal('hide');	
					window.location ='/mdi/metatemplate/metaTemplateCreation'
					
				}
				else
				{
					bootbox.alert("TEMPLATE NOT CREATED ");
					$('#SubDepartment_Modal').modal('hide');	
				}
				getAllDepartmentList();
				
			});
			response.error(function(data, status, headers, config) {
				console.log("Error");
			});
			
		}
		else
		{
			alert("Sub Department name Must Have 2 Char")
			
		}
			
	};
	
	$scope.getDocumentType = function(sub_dept_id)
	{
		var response = $http.get(urlBase+'/metatemplate/getDocumentType',{params:{'sub_dept_id':sub_dept_id}});
    	response.success(function(data, status, headers, config) {		
    		
    		if(data.response=="TRUE")
    		{
    			 $scope.documenttypes = data.modelList;
    			
    		}
    		else
    		{
    			bootbox.alert(data.data);
    		}
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};
	
	$scope.createFolder = function()
	{
		$scope.folder.dept_id       = $scope.doc.sub_dept_id;
		$scope.folder.rep_id        = 1;
		$scope.folder.folder_name   = $scope.doc.fd_name;
		$scope.folder.display_name  = $scope.doc.fd_name;
		$scope.folder.parent_id     = $scope.doc.dt_id;
		
		var response = $http.post(urlBase+'/folder/createFolder',$scope.folder);
    	response.success(function(data, status, headers, config) {		
    		
    		if(data.response=="TRUE")
    		{
    			bootbox.alert("Folder Created");
    			 $('#folder_create').modal('hide');
    		}
    		else
    		{
    			bootbox.alert("Folder Not Created");
    		}
    		
    	});
    	response.error(function(data, status, headers, config) {
    		console.log("Error");
    	});
	};
	
	$scope.resetdatafolder = function()
	{
		$scope.doc={};
		$scope.folder=
	    {
	    		id : '',
	    		parent_id : '',
	    		rep_id : '',
	    		folder_name :'',
	    		display_name :'',
	    		status : '',
	    		dept_id :''
	    }
	};
	
});