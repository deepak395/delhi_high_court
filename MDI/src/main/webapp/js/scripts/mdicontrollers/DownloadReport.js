var DocumentApp = angular.module("mdiApp", ['ui.bootstrap','ng-bootstrap-datepicker','smart-table','ngSanitize', 'ngCsv']);


DocumentApp.controller("DownloadReportCtrl",function($scope, $http,$filter,$document) {
	
	$scope.buttonDisabled=false;
	$scope.Users=[];
	$scope.cartonListData=[];
	$scope.casefileList=[];
	$scope.model=[];
	$scope.model1={};
	$scope.parseInt = parseInt;
	$scope.LoginList=[];
	$scope.reportListData=[];

	var baseUrl="/mdi/";
	
	getAllUsers();
	$scope.today = function() {
	    $scope.dt = new Date();
	};
	$scope.today();
	
	$scope.clear = function () {
		$scope.dt = null;
  	};	
	
	$scope.toggleMax = function() {
	    //$scope.minDate = $scope.minDate ? null : new Date();
		$scope.maxDate = new Date();
	};
	$scope.toggleMax();
	
	$scope.open = function($event,type) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    if(type=="fromDate")
	    	$scope.fromDate= true;
	    if(type=="toDate")
	    	$scope.toDate= true;
	};
	
	$scope.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1
	    
	};
	$scope.dailyreportListData=[];
	$scope.dailyreportList = 
	{						
			'parameter1':'Dept Name',
            'parameter2':'SubDept Name',
            'parameter3':'Doctype Name',
            'parameter4':'File Name',
			'parameter5':'User Name',
			'parameter6':'User Full Name',
			'parameter7':'Download Time',
			'parameter8':'Ip Address'
	};
	$scope.dailyreportListData.push($scope.dailyreportList);
	
	$scope.formats = ['dd-MMMM-yyyy','dd-mm-yyyy', 'yyyy/MM/dd', 'dd-MM-yyyy', 'shortDate'];
	$scope.format = $scope.formats[3];

	function convertDate(inputFormat) 
	{
		  function pad(s) { return (s < 10) ? '0' + s : s; }
		  var d = new Date(inputFormat);
		  return [ d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-');
	}
	
	function getAllUsers()
    {
		//get Users
		var response = $http.get(baseUrl+'/Reports/getAllUsers');
		response.success(function(data, status, headers, config) {		
			console.log("== GET Departments MASTER ==");
			console.log(data);
			$scope.Users=data;
			
			});
		response.error(function(data, status, headers, config) {
			console.log("Error");
			});
	 };
$scope.getLoginData = function() 
{		
	var btn = $("#submit");
	$scope.buttonDisabled = true;
	$scope.displayedCollection=[];
	$scope.LoginList=[];
	
	if($scope.user.fromDate!=null){
		$scope.user.fromDate=convertDate($scope.user.fromDate);
	}
	else{
		bootbox.alert("<B>Select From Date....!!!");
	}
	if($scope.user.toDate!=null){
		$scope.user.toDate=convertDate($scope.user.toDate);
	}
	else{
		bootbox.alert("<B>Select To Date....!!!");
	}
	if($scope.um_id == null){
		$scope.um_id = null;
	}
		
	

		var date = new Date($scope.fromdate);
		var date1=new Date($scope.todate);
			$http.get(baseUrl+'/Reports/getDownloadData',{params: {'um_id':$scope.um_id,
				'fromdate':$scope.user.fromDate,'todate':$scope.user.toDate}}).success(function(data) {
								$scope.buttonDisabled = false;	
								$scope.LoginList=data.modelList;	
								
								if(data.modelList=="")
									{
									bootbox.alert("<B>Inavalid User !!!");
									}
								for(var i=0;i<data.modelList.length;i++)
								{
									$scope.dailyreportList = {
											'parameter1':data.modelList[i][0],
											'parameter2':data.modelList[i][1],
											'parameter3':data.modelList[i][2],
											'parameter4':data.modelList[i][3],
											'parameter5':data.modelList[i][4],
									        'parameter6':data.modelList[i][5],
									        'parameter7':data.modelList[i][6],
									        'parameter8':data.modelList[i][7]
															};
															
									$scope.dailyreportListData.push($scope.dailyreportList);
									console.log(data.modelList[i][0]);	
								};
																
					}).error(function(data, status, headers, config) {
							console.log("Error in getting search Bundle number");
				});
	
		};
});

//date format
CartonApp.filter('dateFormat1', function($filter)
		{
		 return function(input)
		 {
		  if(input == null){ return ""; } 
		 
		  var _date = $filter('date')(new Date(input), 'dd/ MM /yyyy ');
		 
		  return _date.toUpperCase();

		 };
		});



