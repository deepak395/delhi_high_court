var mdiApp = angular.module('mdiApp', [ 'ngFileUpload', 'smart-table' ]);

mdiApp.controller('UploadDocumentController', [
		'$scope',
		'$http',
		'Upload',
		'$window',
		function($scope, $http, Upload, $window) {
			
			

			$scope.$watch('files', function(files) {
				$scope.formUpload = false;
				if (files != null) {
					if (!angular.isArray(files)) {
						$timeout(function() {
							$scope.files = files = [ files ];
						});
						return;
					}
					for (var i = 0; i < files.length; i++) {
						$scope.errorMsg = null;
						(function(f) {
							$scope.upload(f, true);
						})(files[i]);
					}
				}
			});
			$scope.tx = 'Virendra';
			var urlBase = '/mdi'
			$scope.viewDfdData = {};
			$scope.folderform = {};
			$scope.repositories = [];
			$scope.departments = [];
			$scope.userdepartments = [];
			$scope.subdepartments = [];
			$scope.indexDataList = [];
			$scope.fieldsdata = [];
			$scope.years = [];
			$scope.documentTypeList=[];

			$scope.folder = {};
			$scope.errorlist = null;
			$scope.selectedNode = "";
			$scope.selectedFolder = "";
			$scope.picFile = '';
			$scope.treedata = [];
			$scope.benchCodes = [];
			$scope.caseTypes = [];
			$scope.file_name = "";
			$scope.buttonDisabled = false;
			$scope.errorbuttonDisabled = false;
			$scope.documentfiledetails = [];
			$scope.chckedIndexs = [];
			//$scope.fileurl = [];
			$scope.chckedPassValues = [];
			$scope.documentTypeList = [];
			$scope.foldersList =[];
			var ur =$scope.fileurl;
			//var DEFAULT_URL = "/mdi/uploads/example.pdf";

			getAllData();
			
			//getUserDepartmentData();

			
			function getDocumentData(value) {
				var response = $http.get(urlBase
						+ '/metadata/getIndexFieldsDataMetaData', {
					params : {
						'dept_id' : value
					}
				});
				response.success(function(data, status, headers, config) {
					console.log(data);
					$scope.indexData = data.modelList;
					$scope.doc_complete_data = data.modelData;
					$scope.showtxt = 'Y';
					$scope.fileurl = '/mdi/uploads/' + data.data;

				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			}
			$scope.approveAll = function() {

				$scope.chckedPassValues.splice(0,
						$scope.chckedPassValues.length);

				var i;
				for (i = 0; i < $scope.chckedIndexs.length; i++) {
					var id = $scope.chckedIndexs[i].dfd_id;
					console.log(id);
					$scope.chckedPassValues.push({
						if_id : id,
						if_value : 'aa'
					});
				}

				var url = 'search/metadataForSearch';

				// prepare headers for posting
				var config = {
					headers : {
						'Content-Type' : 'application/json',
						'Accept' : 'text/plain'
					}
				}
				console.log($scope.chckedPassValues + "ddddd");

				var dataArr = $scope.chckedPassValues;
				var url = urlBase + '/UploadDocument/approveALl';
				$http.post(url, dataArr, config).then(function(response) {
					console.log($scope.chckedPassValues);
					console.log("== GET Files MASTER ==");
					$scope.documentfiledetails = response.data;
					window.location.href = urlBase + "/metadata/manage";
				}, function error(response) {
					/*console.log($scope.chckedIndexs);*/
				});
			}
			$scope.checkAll = function() {
				if ($scope.selectedAll) {
					$scope.selectedAll = true;
				} else {
					$scope.selectedAll = false;
				}
				angular.forEach($scope.documentfiledetails, function(itm) {
					itm.checkbox = $scope.selectedAll;
					$scope.chckedIndexs.push(itm);
				});
				if ($scope.selectedAll == false) {
					$scope.chckedIndexs = [];
				}
			};
			$scope.checkedIndex = function(data) {
				if ($scope.chckedIndexs.indexOf(data) === -1) {
					$scope.selectedAll = false;
					$scope.chckedIndexs.push(data);
				} else {
					$scope.selectedAll = false;
					$scope.chckedIndexs.splice($scope.chckedIndexs
							.indexOf(data), 1);
				}
				console.log("checked value"
						+ JSON.stringify($scope.chckedIndexs));
			}
			$scope.ViewFile = function(value) {
				$http.get('/mdi/search/viewFile?dfd_id=' + value).success(
						function(data) {
							console.log(data);
							$scope.fileurl = '/mdi/uploads/' + data.data;
							DEFAULT_URL = $scope.fileurl;
							$scope.viewDfdData = data.modelData;
							var url='/mdi/document/open?dfd_id='+value;
							window.open(url, '_blank');
							//window.open($scope.fileurl, "popup", "width=300,height=200,left=10,top=150");
							//window.location.href = urlBase + "/document/openFile";

						}).error(function(data, status, headers, config) {
					console.log("Error in getting User data");
				});
				
				
			}

			$scope.getUserWiseData = function(value) {
				//alert(value);
				var response = $http.get(urlBase
						+ '/UploadDocument/getUserwisefiles');
				response.success(function(data, status, headers, config) {
					console.log("== GET Files MASTER ==");
					console.log(data);
					$scope.documentfiledetails = data;

				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			};
			
			function getUserDepartmentData() {
				//get Departments
				var response = $http.get(urlBase
						+ '/UploadDocument/getUserWiseDepartments');
				response.success(function(data, status, headers, config) {
					console.log("== GET Departments MASTER ==");
					console.log(data);
					$scope.userdepartments = data;

				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});

				var year = new Date().getFullYear();
				var range = [];
				range.push(year);
				for (var i = 1; i < 100; i++) {
					range.push(year - i);
				}
				$scope.years = range;
			};
			
			
			function getAllData() {
				//get Departments
				var response = $http.get(urlBase
						+ '/UploadDocument/getAllDepartments');
				response.success(function(data, status, headers, config) {
					console.log("== GET Departments MASTER ==");
					console.log(data);
					$scope.departments = data;

				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});

				var year = new Date().getFullYear();
				var range = [];
				range.push(year);
				for (var i = 1; i < 100; i++) {
					range.push(year - i);
				}
				$scope.years = range;
			}
			;
			
			
			$scope.getyears = function(value) {
				console.log("TEST"+value);
				$scope.getfolders(value);
				var year = new Date().getFullYear();
				var range = [];
				range.push(year);
				for (var i = 1; i < 100; i++) {
					range.push(year - i);
				}
				$scope.years = range;
			};
			
			
			
			$scope.getSubDept = function(value) {
				//$scope.document.dfd_dept_mid=value;
				var response = $http.get(urlBase + '/UploadDcoumet/getSubDept',
						{
							params : {
								'dept_id' : value
							}
						});
				response.success(function(data, status, headers, config) {
					console.log("== GET LOOK UP MASTER ==");
					console.log(data);
					$scope.subdepartments = data;
					$scope.documentTypeList=[];

				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			};

			$scope.user={};
			function getUserDownloadStatus() {
					//get Departments
					var response = $http.get(urlBase
							+ '/UploadDocument/getUserDownloadStatus');
					response.success(function(data, status, headers, config) {
						$scope.user= data;
						
					});
					response.error(function(data, status, headers, config) {
						console.log("Error");
					});

				}
			
			$scope.downloadFile=function(value) {
				/*var response = $http.post(urlBase+'/UploadDocument/downloadFile?'
						+'dfd_id=' + value);
				response.success(function(data, status, headers, config) {
				});*/
				 window.open(urlBase+'/UploadDocument/downloadFile/?dfd_id='+value,"_self");
				
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			}
			
			
			$scope.getfiles = function(value, value1, value2,value3,value4) {
				
				console.log($scope.document);
				console.log(value4)
				
				var response = $http.post(urlBase + '/UploadDcoumet/getfiles?'
						+ 'dept_id=' + value2 + '&sub_dept_id=' + value
						+ '&year=' + value1+ '&dfd_dt_mid=' + value3+'&fd_id='+value4);
				
				response.success(function(data, status, headers, config) {
					console.log("== GET Files MASTER ==");
					console.log(data);
					getUserDownloadStatus();
					$scope.documentfiledetails = data;
					
					$scope.total_count = $scope.documentfiledetails.length;
					
					//alert($scope.documentfiledetails.length);
					if($scope.documentfiledetails== null)
						{
						$scope.total_count =0;
						}

				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			};
			
			$scope.selectedAll = $scope.documentfiledetails;
			$scope.save = function()
			{

				debugger;
				console.log($scope.document.year+"   year");
				console.log($scope.document.dfd_fd_mid +"  folder");
				//alert($scope.document.dfd_fd_mid +"  folder");
				
				if($scope.document.year==undefined && $scope.document.dfd_fd_mid == undefined)
				{
					$scope.document.year = 1001
					$scope.document.dfd_fd_mid=0;
				}
				
				if($scope.document.dfd_fd_mid==undefined && $scope.document.year!=undefined)
				{
					$scope.document.dfd_fd_mid=0;
				}
				
				if($scope.document.dfd_fd_mid!=undefined && $scope.document.year==undefined)
				{
					$scope.document.year=1000;
				}
				
				console.log($scope.document);
				
				if ($scope.picFile.length > 0) {
					$scope.buttonDisabled = true;
					var file = $scope.picFile;
					if ($scope.selectedNode != "") {
						$scope.document.folder_id = $scope.selectedNode.id;
					}

					file.upload = Upload.upload({
						url : urlBase + '/UploadDcoumet/test',
						headers : {
							'optional-header' : 'header-value'
						},
						fields : $scope.document,
						file : file,
					});

					file.upload.then(function(response) {
						debugger
						$scope.buttonDisabled = false;
						if (response.data.response == "TRUE") {

							console.log(response);
							$scope.errorlist = response.data.dataList;
							console.log($scope.errorlist);
							alert(response.data.data
									+ " document uploaded sucessfully");
							window.location = '/mdi/document/UploadDocument';

						} else {
							bootbox.alert("No document uploaded");
							$scope.errorlist = response.data.dataMapList;
						}
					}, function(response) {

					}, function(evt) {
						// Math.min is to fix IE
						// which reports 200%
						// sometimes
						// file.progress =
						// Math.min(100,
						// parseInt(100.0 *
						// evt.loaded / evt.total));
					});

					file.upload.xhr(function(xhr) {
						// xhr.upload.addEventListener('abort',
						// function(){console.log('abort
						// complete')}, false);
					});

				} else {
					alert('please chhose at least one file');
				}

			};

			$scope.gotoDataEntry = function(index) {
              //  alert(index);
				var doc_id = index;
				window.open(urlBase + '/metadata/dataEntry?id=' + doc_id);
			}
			/*$scope.ViewFile = function(index)
			{
			
				
				//var doc_id=$scope.documentfiledetails[index].dfd_id;
				window.open(urlBase+'/document/File_Meta?id='+index);
			}*/
			//Veiw file
			$scope.Getfilename = function(dfd_id, dfd_name) {

				if (dfd_id != null) {
					$http.get(urlBase + '/report/Getfilename', {
						params : {
							'dfd_id' : dfd_id
						}
					}).success(function(data) {

						$scope.fileurl = urlBase + "/uploads/" + dfd_name;
						$scope.title = dfd_name;
						$('#documentfile_Modal').modal('show');

					}).error(function(data, status, headers, config) {
						console.log("Error in getting Report ");
					});
				}
			};

			$scope.closeDiv = function() {
				$('#user_Modal').modal('hide');
			}
			

			$scope.getDocumentTypes = function (sub_dept_id)
			{
				var response = $http.get(urlBase + '/UploadDcoument/getDocumentType',
				{
							params : 
							{
								'sub_dept_id' : sub_dept_id
							}
				});
				response.success(function(data, status, headers, config) 
				{
					console.log("== GET Document Type MASTER ==");
					console.log(data);
					$scope.documentTypeList = data.modelList;
					//alert("documentTypeList"+$scope.documentTypeList)
				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			};
			
			/*$scope.getDocumentTypes = function (sub_dept_id)
			{
				var response = $http.get(urlBase + '/UploadDcoument/getDocumentType',
				{
							params : 
							{
								'sub_dept_id' : sub_dept_id
							}
				});
				response.success(function(data, status, headers, config) 
				{
					console.log("== GET Document Type MASTER ==");
					console.log(data);
					$scope.documentTypeList = data.modelList;
				});
				response.error(function(data, status, headers, config) {
					console.log("Error");
				});
			};*/
			
			
			
			$scope.getfolders = function(dt_id)
			{
				var response = $http.get(urlBase + '/UploadDcoument/getFolders',
						{
									params : 
									{
										'dt_id' : dt_id
									}
						});
						response.success(function(data, status, headers, config) 
						{
							console.log("== GET Folder Type MASTER ==");
							console.log(data.modelList.length)
							$scope.foldersList= data.modelList;
						});
						response.error(function(data, status, headers, config) {
							console.log("Error");
						});
			};
			
			/* $scope.getFileByDeptAndSubDeptAndDocTypeUserWiseData = function() {
				 console.log("super");
			 }; */
			
			$scope.yearSelected = function() {
			     var selectedYear = $scope.document;
			  //   alert("selectedYear"+selectedYear)
			     if (!selectedCustomer) {
			          alert("Please select a customer");
			          return;
			     }

			     var secondId = selectedCustomer.split('|')[2];
			     // You can directly put your main logic here but since you want to
			     // call a separate method with passing the id
			     $scope.doSomethingWithCustomer(secondId);
			};
			
			
          //search data by dept sub dept user
			$scope.getFileByDeptAndSubDeptAndDocTypeUserWiseData = function(document) {
        	  var dfd_dept_mid=$scope.document.dfd_dept_mid;
        	  var dfd_sub_dept_mid=$scope.document.dfd_sub_dept_mid;
        	  var dfd_dt_mid=$scope.document.dfd_dt_mid;
        	  var dfd_fd_mid=$scope.document.dfd_fd_mid;
        	  var dfd_year=$scope.document.dfd_year;
				   $http({
					    method : "POST",
					    url : urlBase +"/UploadDocument/getFileByDeptAndSubDeptAndDocTypeUserWiseData",
					     params :document
					    }).then(function (result) {
				            $scope.documentDetailList = result.data; 
				            
				        });
		
			     }

		} ]);