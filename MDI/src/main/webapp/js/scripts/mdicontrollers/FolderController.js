var mdiApp= angular.module('mdiApp', []);

mdiApp.controller('FolderController',function($scope,$http) 
{
	$scope.tx='durgendra';
	var urlBase='/mdi'
	$scope.folderform={};
	$scope.repositories=[];
	$scope.departments=[];
	$scope.departments2=[];
	$scope.folders=[];
	$scope.folders2=[];
	$scope.view='';
	$scope.view2='';
	$scope.view3='';
	$scope.content='';
	
    getAllData();
    
    
    function getAllData()
    {
    	//getRepository
    	var response = $http.get(urlBase+'/folder/getRepository');
		response.success(function(data, status, headers, config) {		
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			$scope.repositories=data;
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
		
		//get Departments
		var response = $http.get(urlBase+'/folder/getAllDepartments');
		response.success(function(data, status, headers, config) {		
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			$scope.departments=data;
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
		
		//get Folders
		var response = $http.get(urlBase+'/folder/getAllFolders');
		response.success(function(data, status, headers, config) {		
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			$scope.folders=data;
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	
	
	
	$scope.createFolder = function()
	{
		debugger
		if($scope.content==0)
		{
			$scope.folderform.parent_id='1';
		}
		var response = $http.post(urlBase+'/folder/createFolder',$scope.folderform);
		response.success(function(data, status, headers, config) 
		{		
			
			console.log(data);
			getAllData();
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
    
	$scope.getFolderByDeptId = function(value)
	{
		debugger
		
		$scope.folders2.splice(0,$scope.folders2.length);
		
		$scope.departments2=$scope.departments;
		
		var k;
		var d_name;
		for(k=0;k<$scope.departments2.length;k++)
		{
			var did=$scope.departments2[k].dept_id;
			if(did==value)
			{
				d_name=$scope.departments2[k].dept_name;
				console.log(d_name);
			}
		}
		
		var i;
		var j=0;
		console.log(d_name);
		for (i = 0; i < $scope.folders.length; i++) 
		{
		    var dpt_id = $scope.folders[i].dept_id;
		    var f_name= $scope.folders[i].folder_name;
		    console.log(f_name);
		    if(dpt_id==value && f_name==d_name )
		    {
		    	$scope.folders2[j]= $scope.folders[i];
		    	j=j+1;
		    }
		}
	};
	
    
});