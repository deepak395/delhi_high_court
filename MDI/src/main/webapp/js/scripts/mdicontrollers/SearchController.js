var mdiApp = angular.module("mdiApp", ['smart-table','ui.bootstrap','treeControl','ngMask']);

mdiApp.controller("searchController", function($scope, $http) 
{
	var baseUrl="/mdi/";
	var urlBase='/mdi'
	
	$scope.departments=[];
	$scope.viewDfdData={};
	$scope.years=[];
	$scope.searchcriterias=[];
	$scope.document={};
	$scope.departmentid='';
	
	$scope.editstatus='';
	$scope.criteriaindex='';
	$scope.documentTypeList =[];
	  
	getRepositories();
	getAllDepartment();
	
	
	 $scope.criterias=[{"id":"%like%","value":"%Like%"},{"id":"%like","value":"%Like"},{"id":"like%","value":"Like%"},{"id":"equalto","value":"Equal To"},{"id":"in","value":"IN"},{"id":"notin","value":"Not In"},{"id":"between","value":"Between"}];
	 
	
	 $scope.refreshcriteria=function(){
	    	$scope.searchcriterias=[];
	    }
	 
	 function getRepositories(){
		$http.get(baseUrl+'search/getrepositories').
        success(function (data) {
        	console.log("data"+data);
        	$scope.repositories = data;
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting repositories data");
        });
	};
	
	function getAllDepartment()
    {
	//get Departments
	var response = $http.get(baseUrl+'/UploadDocument/getAllDepartments');
	response.success(function(data, status, headers, config) {		
		console.log("== GET Departments MASTER ==");
		console.log(data);
		$scope.departments=data;
		
		});
	response.error(function(data, status, headers, config) {
		console.log("Error");
		});
	
	//get year
		var year=new Date().getFullYear();
		var data=[];
		data.push(year);
		for(var i=1;i<100;i++)
		{
			data.push(year-i);
		}
		$scope.years=data;
		console.log("Year"+$scope.years);
	};
	
	$scope. getAllSubDept=function(value)
		{
	    	//alert(value);
		    $scope.departmentid=value;
	    	var response = $http.get(baseUrl+'/UploadDcoumet/getSubDept',{params:{'dept_id':value}});
	    	response.success(function(data, status, headers, config) {		
	    		console.log("== GET Subdept MASTER ==");
	    		console.log("subdept data"+data);
	    		$scope.subdepartments=data;
	    		changeDepartment();
	    		
	    	});
	    	response.error(function(data, status, headers, config) {
	    		console.log("Error");
	    	});
		};
		
		$scope.createquery=function(searchcriteria,operator){
			$scope.searchcriteria=searchcriteria;
			$scope.error="";
			
			var update=false;
			
			$scope.searchcriteria.operator=operator;
			
			if(!update)
			{
				if($scope.searchcriteria.metafield==12){
					$scope.searchcriteria.searchtext=$scope.date1+","+$scope.date2;
				}
				$scope.searchcriterias.push($scope.searchcriteria);
			}
			else
			{
				for (i in $scope.searchcriterias) {
					if ($scope.searchcriterias[i].searchrowid == $scope.searchcriteria.searchrowid) {
	                	$scope.searchcriterias[i]=$scope.searchcriteria;
	                }
				}
			}
			$scope.searchcriteria='';
		};
		$scope.newquery=function(){
			$scope.searchcriterias=[];
		};
		$scope.savequery=function()
		{
			$scope.entity={"criterias":$scope.searchcriterias};
			$scope.searchquery.criterias=$scope.searchcriterias;
			$http.post(baseUrl+'document/savequery',$scope.searchquery).
	        success(function (data) {
	        	$scope.searchquery=data.modelData;
	        	$scope.searchcriterias=data.modelData.criterias;
	        	$scope.addQuery();
	        	bootbox.alert("Query Saved Successfully", function() {});
	        }).
	        error(function(data, status, headers, config) {
	        	console.log("Error in getting tree data");
	        });
		};
		$scope.setQuery=function(searchquery){
			$scope.searchquery=searchquery;
			$scope.searchcriterias=$scope.searchquery.criterias;
			$('#queriesModal').modal('hide');
		};
		
		$scope.deleteQuery=function(searchquery){
			$scope.entity=searchquery;
			bootbox.confirm({className:'delete',message: "Are you sure ?", 
			    buttons:{
			    	confirm: {   
			    		 label: "Delete",
			    		 className: "btn-success",
			    		 callback: function(result) {}
			    	 },
			    	 cancel: {
			    		 label: "Cancel",
					      className: "btn-danger",
					      callback: function(result) {}
					    },
			    },
			    callback: function(result){ /* your callback code */ 		
			    	if(result) {
			    		var response = $http.post(baseUrl+'document/deletequery',$scope.entity);
						response.success(function(data, status, headers, config) {					
							bootbox.alert("Query Successfully Deleted.", function() {});
							var index = -1;
							for( var i = 0; i < $scope.searchqueries.length; i++ ) {
								if( $scope.searchqueries[i].id === searchquery.id ) {					
									index = i;
									break;
								}
							}
							
							if(index!=-1)
							{
								$scope.searchqueries.splice(index,1);
							}
							if($scope.searchquery.id==searchquery.id)
							{
								$scope.searchquery={};
								$scope.searchcriterias=[];
							}
						});
						response.error(function(data, status, headers, config) {
							alert( "Angular Post Error");
						});
					}
			    }
			});
			
			
		};
		
		$scope.searchdocument=function(){
			/*** search content  method ***/
		//alert($scope.document.dt_id+'  year='+$scope.document.dfd_year)
		    if($scope.document.dfd_sub_dept_mid==undefined || $scope.document.dfd_sub_dept_mid=='')
			{
		    	$scope.document.dfd_sub_dept_mid=0;
			}
		    
		    if($scope.document.dfd_year== undefined)
		    {
		    	$scope.document.dfd_year=0000;
		    }
		    $scope.searchcriterias[0] = $scope.searchcriteria;
			debugger;
			$scope.entity={"searchlist":$scope.searchcriterias};
			console.log($scope.document);
			
			if($scope.departmentid =='' || $scope.departmentid == null)
			{
				$scope.departmentid=0;
			}
			if($scope.document.dt_id ==undefined || $scope.document.dt_id=='' )
			{
				$scope.document.dt_id=0;
			}
			$http.post(baseUrl+'document/searchdocument?s_d_id='+$scope.document.dfd_sub_dept_mid+'&year='+$scope.document.dfd_year+'&de_id='+$scope.departmentid+'&dt_id='+$scope.document.dt_id , $scope.entity).
	        success(function (data) {
	        	
	        	
	        	$scope.documentlist=data.modelList;
	        	/*var i;
	        	var j=0;
	        	for(i=0;i<data.modelList.length;i++)
	        	{
	        		var year =data.modelList[i].dfd_year;
	        		if(year==$scope.document.dfd_year)
	        		{
	        			$scope.documentlist[j] = data.modelList[i];
	        			j=j+1;
	        		}
	        	}*/
	        	//$scope.documentlist=data.modelList;
	        	console.log("$scope.documentlist"+$scope.documentlist);
	        	        }).
	        error(function(data, status, headers, config) {
	        	console.log("Error in getting tree data");
	        });
			
		};
		
		$scope.viewfile =function(value)
		{
			        $http.get('/mdi/search/viewFile?dfd_id='+value).success(function (data) 
					 {
								console.log(data);
								$scope.fileurl='/mdi/uploads/'+data.data;
								$scope.viewDfdData=data.modelData;
								
								var url='/mdi/document/open?dfd_id='+value;
								
								
								window.open(url, '_blank');
								            
					 }).
							  error(function(data, status, headers, config) {
								        	console.log("Error in getting User data");
					});
		}
	
		$scope.closeDiv = function()
		{
			$('#user_Modal').modal('hide');	
		}
		
		
		$scope.editCondition = function(data,index)
		{
			$scope.searchcriteria.criteria = $scope.searchcriterias[index].criteria;
			$scope.searchcriteria.searchtext = $scope.searchcriterias[index].searchtext;
			
			$scope.criteriaindex=index;
			
			$scope.editstatus='YES';
			
			
		}
		
		$scope.deleteCondition = function (index)
		{
			$scope.searchcriterias.splice(index, 1);
		}
		
		$scope.charforEdit = function()
		{
			if($scope.editstatus=='YES')
			{
				var index = $scope.criteriaindex;
				$scope.searchcriterias[index].criteria = $scope.searchcriteria.criteria;
				$scope.searchcriterias[index].searchtext = $scope.searchcriteria.searchtext;
			}
		}
		$scope.changeForEdit = function()
		{
			if($scope.editstatus=='YES')
			{
				var index = $scope.criteriaindex;
				$scope.searchcriterias[index].criteria = $scope.searchcriteria.criteria;
				$scope.searchcriterias[index].searchtext = $scope.searchcriteria.searchtext;
			}
		}
		
	    $scope.getSubDept = function(value)
		{
	    	
	    	
	    	$scope.departmentid=value;
	    	
	    	//$scope.document.dfd_dept_mid=value;
	    	var response = $http.get(urlBase+'/UploadDcoumet/getSubDept',{params:{'dept_id':value}});
	    	response.success(function(data, status, headers, config) {		
	    		console.log("== GET LOOK UP MASTER ==");
	    		console.log(data);
	    		$scope.subdepartments=data;
	    		
	    	});
	    	response.error(function(data, status, headers, config) {
	    		console.log("Error");
	    	});
		};
		
		
		$scope.getDocumentTypes = function (sub_dept_id)
		{
			var response = $http.get(urlBase + '/UploadDcoument/getDocumentType',
			{
						params : 
						{
							'sub_dept_id' : sub_dept_id
						}
			});
			response.success(function(data, status, headers, config) 
			{
				console.log("== GET Document Type MASTER ==");
				console.log(data);
				$scope.documentTypeList = data.modelList;
			});
			response.error(function(data, status, headers, config) {
				console.log("Error");
			});
		};
		
		/*$scopesetDeptName = function(value)
		{
			alert(value);
		}*/
		
		function changeDepartment()
		{
			$scope.document.dfd_sub_dept_mid = '';
			$scope.document.dt_id ='';
			$scope.documentTypeList =[];
		};
		
		
	
		
		
		
});
