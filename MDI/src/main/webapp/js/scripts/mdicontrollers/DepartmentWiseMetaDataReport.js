var DocumentApp = angular.module("mdiApp", ['ui.bootstrap','ng-bootstrap-datepicker','smart-table','ngSanitize', 'ngCsv']);


DocumentApp.controller("DepartmentWiseMetaDataReportCtl",function($scope, $http,$filter,$document) {
	
/*	$scope.buttonDisabled=false;
	$scope.departments=[];
	$scope.MetaDataList=[];
	$scope.reportListData=[];
	$scope.documentfileList=[];
	$scope.count1=0;*/

	var baseUrl="/mdi/";
	$scope.buttonDisabled=false;
	$scope.departments=[];
	$scope.subdepartments = [];
	$scope.MetaDataList=[];
	$scope.reportListData=[];
	$scope.documentfileList=[];
	$scope.count1=0;
	getAllDepartment();
	$scope.today = function() {
	    $scope.dt = new Date();
	};
	$scope.today();
	
	$scope.clear = function () {
		$scope.dt = null;
  	};	
	
	$scope.toggleMax = function() {
		$scope.maxDate = new Date();
	};
	$scope.toggleMax();
	
	$scope.open = function($event,type) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    if(type=="fromDate")
	    	$scope.fromDate= true;
	    if(type=="toDate")
	    	$scope.toDate= true;
	};
	
	$scope.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1
	    
	};

	$scope.dailyreportListData=[];
	$scope.dailyreportList = 
	{						
							'parameter1':'Department',
							'parameter2':'Meta Data Count',
							'parameter3':'',
							'parameter4':'',
							'parameter5':''
	};
	$scope.dailyreportListData.push($scope.dailyreportList);
	
	$scope.formats = ['dd-MMMM-yyyy','dd-mm-yyyy', 'yyyy/MM/dd', 'dd-MM-yyyy', 'shortDate'];
	$scope.format = $scope.formats[3];

	function convertDate(inputFormat) 
	{
		  function pad(s) { return (s < 10) ? '0' + s : s; }
		  var d = new Date(inputFormat);
		  return [ d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-');
	}
	
	function getAllDepartment()
    {
	var response = $http.get(baseUrl+'/Reports/getAllDepartments');
	response.success(function(data, status, headers, config) {		
		console.log("== GET Departments MASTER ==");
		console.log(data);
		$scope.departments=data;
		
	});
	response.error(function(data, status, headers, config) {
		console.log("Error");
	});

 };
 
 
 $scope.getSubDept = function(value) {
		//$scope.document.dfd_dept_mid=value;
		var response = $http.get(baseUrl + '/UploadDcoumet/getSubDept',
				{
					params : {
						'dept_id' : value
					}
				});
		response.success(function(data, status, headers, config) {
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			$scope.subdepartments = data;
			$scope.documentTypeList=[];

		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	

$scope.getMetaEntryData = function() 
{	
	$scope.count1=0;
	var btn = $("#submit");
	$scope.buttonDisabled = true;
	$scope.displayedCollection=[];
	$scope.UploadedList=[];
	
	if($scope.model.fromDate!=null){
		$scope.model.fromDate=convertDate($scope.model.fromDate);
	}
	else{
		bootbox.alert("<B>Select From Date....!!!");
	}
	if($scope.model.toDate!=null){
		$scope.model.toDate=convertDate($scope.model.toDate);
	}
	else{
		bootbox.alert("<B>Select To Date....!!!");
	}
	if($scope.dept_id == null){
		$scope.dept_id = null;
	}

		var date = new Date($scope.fromdate);
		var date1=new Date($scope.todate);
			$http.get(baseUrl+'/Reports/getDepartmentWiseMetaData',{params: { 'dept_id':$scope.document.dfd_dept_mid,'sub_dept_id':$scope.document.dfd_sub_dept_mid,'fromdate':$scope.model.fromDate,'todate':$scope.model.toDate}})
			.success(function(data) {
								$scope.buttonDisabled = false;	
								$scope.MetaDataList=data.modelList;	
								console.log($scope.mdCountReporytList);
								if(data.modelList=="")
									{
									  bootbox.alert("<B>Data Not Found");
									}
								
								for(var i=0;i<data.modelList.length;i++)
								{
									$scope.dailyreportList = {
											'parameter1':data.modelList[i][0],
											'parameter2':data.modelList[i][1],
											'parameter3':data.modelList[i][2],
											'parameter4':data.modelList[i][3],
											'parameter5':data.modelList[i][4]
															};
															
									$scope.dailyreportListData.push($scope.dailyreportList);
									console.log(data.modelList[i][0]);	
								};
								
								for(var i=0;i<data.modelList.length;i++)
								{	
									$scope.count1+=parseInt(data.modelList[i][1]);
									
								}	
								
								
								$scope.dailyreportList = {

										'parameter1':'TOTAL',
										'parameter2':$scope.count1,
										'parameter3':'',
										'parameter4':'',
										'parameter5':'',
									};
								$scope.dailyreportListData.push($scope.dailyreportList);
								
																
					}).error(function(data, status, headers, config) {
							console.log("Error in getting search Bundle number");
				});
	
		};
		
});

//date format
DocumentApp.filter('dateFormat1', function($filter)
		{
		 return function(input)
		 {
		  if(input == null){ return ""; } 
		 
		  var _date = $filter('date')(new Date(input), 'dd/ MM /yyyy ');
		 
		  return _date.toUpperCase();

		 };
		});



