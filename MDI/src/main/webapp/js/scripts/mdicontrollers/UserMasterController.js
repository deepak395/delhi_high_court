var mdiApp = angular.module("mdiApp",['smart-table']);

mdiApp.controller("UserMasterCtrl",['$scope','$http', function($scope,$http) {
	var urlBase="/mdi/";
	$scope.masterentity = {};	
	$scope.masterdata=  [];
	$scope.roleData=[];
	$scope.departmentData=[];
	$scope.designationData=[];
	$scope.userrole={};
	$scope.userroles=[];
	$scope.errorlist=null;
	$scope.userpemissions=[];
	$scope.repositories=[];
	$scope.folders=[];
	$scope.displayedCollection=[];
	$scope.roleid=false;
	$scope.userPermissionData={};
	$scope.departments =[];
	$scope.subdepartments=[];
	$scope.subdepartments2=[];
	
	$scope.departments=[];
	
	getMasterdata();
	getRoleDD();
	getBenchdata();
	function getMasterdata() 
	{
		$http.get(urlBase+'user/getallusers').success(function (data) {
            	$scope.masterdata = data; 
            	$scope.displayedCollection = [].concat($scope.masterdata);
            }).
            error(function(data, status, headers, config) {
            	console.log("Error in getting User data");
            });
    };
    function getBenchdata() {			
		var response = $http.get(urlBase+'user/getbenchcode');
		response.success(function(data, status, headers, config) {
			$scope.branchDataList=data;
			//console.log($scope.branchDataList);
		});
		response.error(function(data, status, headers, config) {
			alert("Error");
		});
		
	};
	
	
    function getRoleDD() {
    	debugger
		$http.get(urlBase+'user/getmasters').
            success(function (data) {
            	$scope.roleData=data.roleData;
            	$scope.departmentData=data.departmentData;
            	$scope.designationData=data.designationData;
            	$scope.masterdata.um_pass_validity_date = new Date();
            }).
            error(function(data, status, headers, config) {
            	console.log("Error in getting property details");
            });
		
		}
    
  $scope.checkboxSelection = '1';
    $scope.isCheckboxSelected = function(index) {
        return index === $scope.checkboxSelection;
    };
    
    $scope.updateAllChildrens=function(permission){
    	$scope.entity=angular.copy(permission);
    	
    	if($scope.entity.status==0)
    		$scope.entity.status=1;
    	else
    		$scope.entity.status=0;
    	
    	$http.post(urlBase+'user/assignallfolders',$scope.entity).
        success(function (data) {
        	if(data.response=="TRUE"){
        		$scope.repositories=data.repositories;
            	$scope.folders=data.folders;
        	}else{
        		$scope.errorlist=data.dataMapList;
        	}
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting property details");
        });
    };
    $scope.changeStatus=function(permission){
    	$scope.errorlist=[];
    	$scope.entity=angular.copy(permission);
    	
    	if($scope.entity.status==0)
    		$scope.entity.status=1;
    	else
    		$scope.entity.status=0;
    	
    	$http.post(urlBase+'user/updateuserpermission',$scope.entity).
        success(function (data) {
        	if(data.response=="TRUE"){
        		$scope.repositories=data.repositories;
            	$scope.folders=data.folders;
        	}else{
        		$scope.errorlist=data.dataMapList;
        	}
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting property details");
        });
    };
    $scope.getUserPermissions=function(user){
    	console.log(user);
    	$scope.user=user;
    	$scope.repositories=[];
    	$scope.folders=[];
    	$http.post(urlBase+'user/getuserpermission',$scope.user).
        success(function (data) {
        	if(data.response=="TRUE"){
        		$scope.repositories=data.repositories;
            	$scope.folders=data.folders;
            	console.log("$scope.repositories"+$scope.folders)
        	}else{
        		$scope.errorlist=data.dataMapList;
        	}
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting property details");
        });
	};
	
    $scope.updateAllChildrens=function(permission)
    {
    	console.log(permission);
    	debugger
    	$scope.entity=angular.copy(permission);
    	
    	if($scope.entity.status==0)
    		$scope.entity.status=1;
    	else
    		$scope.entity.status=0;
    	
    	$http.post(urlBase+'user/assignallfolders',$scope.entity).
        success(function (data) {
        	if(data.response=="TRUE"){
        		$scope.repositories=data.repositories;
            	$scope.folders=data.folders;
        	}else{
        		$scope.errorlist=data.dataMapList;
        	}
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting property details");
        });
    };
    
    $scope.changeStatus=function(permission)
    {
    	console.log(permission);
    	debugger
    	$scope.errorlist=[];
    	$scope.entity=angular.copy(permission);
    	
    	if($scope.entity.status==0)
    		$scope.entity.status=1;
    	else
    		$scope.entity.status=0;
    	
    	$http.post(urlBase+'user/updateuserpermission',$scope.entity).
        success(function (data) {
        	if(data.response=="TRUE"){
        		$scope.repositories=data.repositories;
            	$scope.folders=data.folders;
        	}else{
        		$scope.errorlist=data.dataMapList;
        	}
        }).
        error(function(data, status, headers, config) {
        	console.log("Error in getting property details");
        });
    };
	
    $scope.user_create = function(masterentity) {
    	console.log(masterentity);
    	debugger
    	$scope.masterentity=masterentity;
    	
//    	if($scope.masterentity){
//    		angular.forEach($scope.masterdata,function(value,index){	    			
//    			if(value.username==$scope.masterentity.username){
//    				if($scope.masterentity.um_id){
//    					if($scope.masterentity.um_id!=value.um_id){
//    						$scope.error=true;
//    					}
//    				}else{
//    					$scope.error=true;
//    				}
//    			}
//            });
//    	}
    	
    		var response = $http.post(urlBase+'user/create',$scope.masterentity);		 
				response.success(function(data, status, headers, config) {					
					if(data.response=="FALSE"){					
						$scope.errorlist=data.dataMapList;					
						$.each($scope.errorlist, function(k, v) {
		                    $("#"+k).parent().parent().addClass('has-error');
						});						
					}else{		
						$('#user_Modal').modal('hide');		
						$scope.errorlist=[];
						
						$('.form-group').removeClass('has-error');
						
								bootbox.alert("User created Successfully!");
								$scope.masterdata.push(data.modelData);
								getMasterdata();						
					}
					
			});
			response.error(function(data, status, headers, config) {
				alert( "Error");
			});
    	
	};
	
	
	$scope.user_update=function(masterentity)
	{
		console.log(masterentity);
		$scope.masterentity=masterentity;
		
	/*	var myDate=$scope.masterentity.um_pass_validity_date;
		myDate=myDate.split("/");
		var newDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
		$scope.masterentity.um_pass_validity_date=new Date(newDate).getTime();*/
		
		var response=$http.post(urlBase+'user/update',$scope.masterentity);
		response.success(function(data,status,headers,config){
			if(data.response=="FALSE")
				{
				$scope.errorlist=data.dataMapList;					
				$.each($scope.errorlist, function(k, v) {
                    $("#"+k).parent().parent().addClass('has-error');
				 });	
				}
			else
				{
				$('#user_Modal').modal('hide');		
				$scope.errorlist=null;
				$('.form-group').removeClass('has-error');
				bootbox.alert("User updated Successfully!");
				$scope.masterdata.push(data.modelData);
				getMasterdata();					
			    }
			
			
		});
			response.error(function(data, status, headers, config) {
				alert( "Error");
			}); 
	};
	
	$scope.setMasterdata = function(masterentity) {
		$scope.masterentity = angular.copy(masterentity);
		
		if($scope.masterentity.userroles.length>0)
			$scope.masterentity.um_role_id= $scope.masterentity.userroles[0].ur_role_id;
	};
	
	$scope.Date_Reset= function()
	{
		
		var response=$http.post(urlBase+'user/validityDays');
		response.success(function(data,status,headers,config){
			console.log(data)
			var date=new Date($scope.masterentity.um_pass_validity_date);
			var newdate=new Date(date);
			newdate.setDate(newdate.getDate()+data);
			
			var dd=newdate.getDate();
			var mm=newdate.getMonth()+1;
			var y=newdate.getFullYear();
			$scope.masterentity.um_pass_validity_date = dd+'/'+mm+'/'+y;
			
			
		});
			response.error(function(data, status, headers, config) {
				alert( "Error");
			}); 
		
		
	//	$scope.masterentity.um_pass_validity_date=data;
		
	};
	
	
	$scope.setMasterdata = function(masterentity) {
		$scope.masterentity = angular.copy(masterentity);
		var value=350991;
		$scope.showDeptDiv(value);
		if($scope.masterentity.userroles.length>0)
			$scope.masterentity.um_role_id= $scope.masterentity.userroles[0].ur_role_id;
	};
    

	$scope.resetModel=function()
	{
		$scope.masterentity.rec_status=1;
		$scope.masterentity={};		
		$('.form-group').removeClass('has-error');	
		$scope.index=-1;
		$scope.roleid=false;
		
	};
    

	$scope.Refresherrorlist=function()
	{
		$scope.errorlist = null;
	};

	$scope.changePassword = function(data) {
		
		
		console.log(data);
		console.log("********");

				
			$scope.passwordn;
			$scope.passwordcn;
			$scope.entity={'password':$scope.passwordn,'confirmpassword':$scope.passwordcn,'um_id':$scope.useridtochangepass}
		

		var response = $http.post(urlBase+'user/changepassword',
				$scope.entity);
		response.success(function(data, status, headers, config) {
			if (data.response == "TRUE") {
			
				$('#pass_Modal').modal('hide');		
				$scope.errorlist=null;
				$('.form-group').removeClass('has-error');
				bootbox.alert("Password changed Successfully!");
				
				//$scope.errorlist = [];
				//$scope.ansDetails = {};

			} else {

				$scope.errorlist = data.dataMapList;
				alert($scope.errorlist);
				console.log($scope.errorlist);
				/*$.each($scope.errorlist, function(k, v) {
					$("#" + k).parent().parent().addClass('has-error');
				});*/
				//bootbox.alert("Password and Confirm Password not matched");
				$scope.ansDetails = [];
			
			}

		});
		response.error(function(data, status, headers, config) {
			console.log("Error in getting Master");
		});

  
}
	
	
	$scope.errorlistSecond = [];
	$scope.ansDetails = [];
	$scope.password;
	$scope.passwordc;
	$scope.useridtochangepass='';
	

	
	$scope.login = function() {

		window.location.href = "/mdi/logout";
	};

	console.log("********aaa");

	$scope.Refresherrorlist = function(value)
	{
		
		
		$scope.useridtochangepass=$scope.displayedCollection[value].um_id;
		//$scope.useridtochangepass=angular.element('#userid').val();
	};
	
	$scope.showDeptDiv = function (value)
	{
	   console.log(value);
		
		if(value ==350991 || value==350993 || value==10000580)
		{
			$scope.roleid=true;
		}
		else
		{
			$scope.roleid=false;
		}
		 
	};
	
	$scope.deptpermission=[];
	$scope.permission3=[];
	$scope.permissions=[];
	$scope.permissions2=[];
	
	/****************************************************/
	$scope.getDepartments = function(index)
	{
		$scope.permissions2=[];
		$scope.userPermissionData = $scope.displayedCollection[index];
		console.log($scope.userPermissionData);
		var response = $http.get(urlBase+'/user/getUserSubDepartments?dpt_id='+$scope.userPermissionData.um_department_id+'&um_id='+$scope.userPermissionData.um_id);
		response.success(function(data, status, headers, config) {		
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			 $scope.permissions=data.modelList;
			 $scope.permissions2= $scope.permissions;
			 console.log($scope.permissions2);
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
		
	
	
	$scope.setValue = function(value,index)
	{
		
		if(value==true)
		{
			$scope.permissions2[index].value=true;
			
		}
		
		if(value==0)
		{
			$scope.permissions2[index].value=false;
		}
		
		
		console.log($scope.permissions2);
		
	}
	
	$scope.SavePermission = function()
	{
		if($scope.permissions2.length>0)
		{
			var response=$http.post(urlBase+'user/SaveSubDeptPermission', $scope.permissions2);
			response.success(function(data,status,headers,config){
				if(data.response=="TRUE")
				{
					$('#permission_Modal').modal('hide');		
					
					bootbox.alert("Permission Created");
					
				}
				else
				{
					console.log(data.data);
                    $('#permission_Modal').modal('hide');		
					
					bootbox.alert("Some Thing is Wrong");
				}
				
				
			});
			response.error(function(data, status, headers, config) {
					alert( "Error");
			}); 
		}
	}
	
	
	$scope.getAllDepartments = function(index)
	{
		$scope.permissions3=[];
		//$scope.userPermissionData = $scope.displayedCollection[index];
		var response = $http.get(urlBase+'/user/getAllDepartments')
		response.success(function(data, status, headers, config) {		
			console.log("== GET LOOK UP MASTER ==");
			console.log(data);
			 $scope.deptpermission=data.modelList;
			 $scope.permissions3= $scope.permissions;
			 console.log($scope.permissions3);
			
		});
		response.error(function(data, status, headers, config) {
			console.log("Error");
		});
	};
	
	
	
	
	
	
	
}]);