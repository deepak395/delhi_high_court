/**
 * 
 */
package com.mdi.email;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.mdi.service.UserService;
/**
 * @author v0038
 *
 */
public class SendEmail {
	
	@Autowired
	private TaskExecutor taskExecutor;
	
	@Autowired 
	private JavaMailSender mailSender;
	
	@Autowired 
	UserService userService;
	
	String status=""; 
	
		final String sys_from ="deepakc395";
	
		final String sys_password = "anu@deep";
	


	
	public String sendMail(String toemail ,String msg){
				
		taskExecutor.execute(new Runnable() {
			
			@Override
			public void run() {
				
			 try {			        
			        MimeMessage mimeMessage = mailSender.createMimeMessage();
			        
			        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
			        
			        //helper.setFrom(fromemail);
			       /* helper.setTo(toemail);
			        helper.setSubject("Sindhi Matrimonial");
			        helper.setText(msg);
			        			        */
			      /*  String file = attachFile.getOriginalFilename();
			        if(attachFile != null)
			        	helper.addAttachment("yourfile"+"."+FilenameUtils.getExtension(file), attachFile);*/
			        
			        mailSender.send(mimeMessage);
			        status = "success";
			        
				  }catch(Exception e){
					  
					  status = "error";
					  e.printStackTrace();					
				  } 				
			}
		});
		
		return status;		
	}
	
////////////////////////////////////////////////////////////
	
/*		public String sendMailForFirstPassWord(String username, String to, String OTP ) throws UnsupportedEncodingException  {
	
			String result = null;
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			
			props.put("mail.smtp.auth", "true"); 
			props.put("mail.smtp.starttls.enable", "true");
		
			
			// get Session
		
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			    protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication(sys_from, sys_password);
			    }
			});
			
			
	
			// compose message
			try {
				
				MimeMessage message = new MimeMessage(session);
				//change this address
				message.setFrom(new InternetAddress("deepakc395@gmail.com"));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				message.setSubject("OTP For Forget Password");
			
				message.setText("Dear Friend, \n\n"
						+ "Welcome to MDI.com.\n\n"
						+ "Your password is recover successfully \n\n"
						+ "USER ID -:    "+username+" \n"
						+ "OTP -:    "+OTP+" \n\n"
						+ "Your password, hence system will help you to change the password when you will log in for the first time."
						+ "\n\nPlease do not reply on this mail."
						+ "\n Wish you all the best.............. "
						+ "\n\n\n Regards,"
						+ "\n MDI.com  ");
			
				
					
				
				// send message
				Transport.send(message);
				
				result = "message sent successfully";
				System.out.println("message sent successfully");
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}

			return result;

		}*/

		public String sendMailForFirstPassWord(String username, String to, String OTP ) throws UnsupportedEncodingException  {
			
			String result = null;
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			
			props.put("mail.smtp.auth", "true"); 
			props.put("mail.smtp.starttls.enable", "true");
		
			
			// get Session
		
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			    protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication(sys_from, sys_password);
			    }
			});
			
			
	
			// compose message
			try {
				
				MimeMessage message = new MimeMessage(session);
				//change this address
				message.setFrom(new InternetAddress("deepakc395@gmail.com"));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				message.setSubject("OTP For Forget Password");
			
				message.setText("Dear Friend, \n\n"
						+ "Welcome to MDI.com.\n\n"
						+ "Your password is recover successfully \n\n"
						+ "USER ID -:    "+username+" \n"
						+ "OTP -:    "+OTP+" \n\n"
						+ "Your password, hence system will help you to change the password when you will log in for the first time."
						+ "\n\nPlease do not reply on this mail."
						+ "\n Wish you all the best.............. "
						+ "\n\n\n Regards,"
						+ "\n MDI.com  ");
			
				
					
				
				// send message
				Transport.send(message);
				
				result = "message sent successfully";
				System.out.println("message sent successfully");
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}

			return result;

		}

		
		
		
		/*public String sendMailForForgetPassword(String Fullname, String to, String OTP ) throws UnsupportedEncodingException  {
			
			String result = null;
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.logix.in");
			props.put("mail.smtp.port", "587");
			
			props.put("mail.smtp.auth", "true"); 
			props.put("mail.smtp.starttls.enable", "true");
		
			
			// get Session
		
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			    protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication(sys_from, sys_password);
			    }
			});	
			
			
	
			// compose message
			try {
				
		
			
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress("noreply@SindhiMatrimonial.com","Team.SindhiMatrimonial"));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				message.setSubject("OTP For First LogIn ");
			
				message.setText("Dear Friend, \n\n"
						+ "Welcome to MDI.\n\n Congratulation on becoming a member of MDI family. \n\n"
						+ "Your Login Details : \n\n"
						+ "USER ID -:    "+to+" \n"
						+ "OTP -:    "+OTP+" \n\n"
						+ "\n\nPlease do not reply on this mail."
						+ "\n Wish you all the best.............. "
						+ "\n\n\n Regards,"
						+ "\n MDI.com  ");
			
				
				// send message
				Transport.send(message);
				
				result = "message sent successfully";
				System.out.println("message sent successfully");
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}

			return result;

		}*/
}
