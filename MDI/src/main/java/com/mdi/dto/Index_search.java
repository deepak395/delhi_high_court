package com.mdi.dto;

public class Index_search 
{
	private Long if_id;
	private String if_name;
	private String if_type;
	
	public Long getIf_id() {
		return if_id;
	}
	public void setIf_id(Long long1) {
		this.if_id = long1;
	}
	public String getIf_name() {
		return if_name;
	}
	public void setIf_name(String if_name) {
		this.if_name = if_name;
	}
	public String getIf_type() {
		return if_type;
	}
	public void setIf_type(String if_type) {
		this.if_type = if_type;
	}
	
	

}
