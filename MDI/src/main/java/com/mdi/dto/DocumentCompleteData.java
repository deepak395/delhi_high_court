package com.mdi.dto;

import java.util.List;

public class DocumentCompleteData 
{
	private Long doc_id;
	private String doc_name;
	private Long dept_id;
	private String dept_name;
	private Long sub_dept_id;
	private String sub_dept_name;
	private String dfd_md_status;
	
	
	
	public String getDfd_md_status() {
		return dfd_md_status;
	}

	public void setDfd_md_status(String dfd_md_status) {
		this.dfd_md_status = dfd_md_status;
	}

	private List<Index_Meta_Data> indexfields;

	public Long getDoc_id() {
		return doc_id;
	}

	public void setDoc_id(Long doc_id) {
		this.doc_id = doc_id;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public Long getDept_id() {
		return dept_id;
	}

	public void setDept_id(Long dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public Long getSub_dept_id() {
		return sub_dept_id;
	}

	public void setSub_dept_id(Long sub_dept_id) {
		this.sub_dept_id = sub_dept_id;
	}

	public String getSub_dept_name() {
		return sub_dept_name;
	}

	public void setSub_dept_name(String sub_dept_name) {
		this.sub_dept_name = sub_dept_name;
	}

	public List<Index_Meta_Data> getIndexfields() {
		return indexfields;
	}

	public void setIndexfields(List<Index_Meta_Data> indexfields) {
		this.indexfields = indexfields;
	}
	
	

}
