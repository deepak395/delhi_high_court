package com.mdi.dto;

public class DepartmentDetailsDto 
{
	private Long dept_id;
	private String dept_name;
	private Long dept_repo_id;
	private String dept_repo_name;
	private String dept_cr_by;
	private Long dept_cr_by_id;
	public Long getDept_id() {
		return dept_id;
	}
	public void setDept_id(Long dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public Long getDept_repo_id() {
		return dept_repo_id;
	}
	public void setDept_repo_id(Long dept_repo_id) {
		this.dept_repo_id = dept_repo_id;
	}
	public String getDept_repo_name() {
		return dept_repo_name;
	}
	public void setDept_repo_name(String dept_repo_name) {
		this.dept_repo_name = dept_repo_name;
	}
	public String getDept_cr_by() {
		return dept_cr_by;
	}
	public void setDept_cr_by(String dept_cr_by) {
		this.dept_cr_by = dept_cr_by;
	}
	public Long getDept_cr_by_id() {
		return dept_cr_by_id;
	}
	public void setDept_cr_by_id(Long dept_cr_by_id) {
		this.dept_cr_by_id = dept_cr_by_id;
	}
	@Override
	public String toString() {
		return "DepartmentDetailsDto [dept_id=" + dept_id + ", dept_name=" + dept_name + ", dept_repo_id="
				+ dept_repo_id + ", dept_repo_name=" + dept_repo_name + ", dept_cr_by=" + dept_cr_by
				+ ", dept_cr_by_id=" + dept_cr_by_id + "]";
	}
	
	

}
