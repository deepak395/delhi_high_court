package com.mdi.dto;

import java.util.List;

import com.mdi.model.Lookup;
public class Index_Meta_Data 
{
	private Long if_id;
	private String if_name;
	private String if_type;
	private List<Lookup> if_value_drop;
	private String meta_value;
	private String meta_value_if_drop;
	private String meta_value_if_date;
	
	
	
	
	
	public String getMeta_value_if_date() {
		return meta_value_if_date;
	}
	public void setMeta_value_if_date(String meta_value_if_date) {
		this.meta_value_if_date = meta_value_if_date;
	}
	public String getMeta_value_if_drop() {
		return meta_value_if_drop;
	}
	public void setMeta_value_if_drop(String meta_value_if_drop) {
		this.meta_value_if_drop = meta_value_if_drop;
	}
	public Long getIf_id() {
		return if_id;
	}
	public void setIf_id(Long if_id) {
		this.if_id = if_id;
	}
	public String getIf_name() {
		return if_name;
	}
	public void setIf_name(String if_name) {
		this.if_name = if_name;
	}
	public String getIf_type() {
		return if_type;
	}
	public void setIf_type(String if_type) {
		this.if_type = if_type;
	}
	public List<Lookup> getIf_value_drop() {
		return if_value_drop;
	}
	public void setIf_value_drop(List<Lookup> if_value_drop) {
		this.if_value_drop = if_value_drop;
	}
	public String getMeta_value() {
		return meta_value;
	}
	public void setMeta_value(String meta_value) {
		this.meta_value = meta_value;
	}
	
	@Override
	public String toString() {
		return "Index_Meta_Data [if_id=" + if_id + ", if_name=" + if_name + ", if_type=" + if_type + ", if_value_drop="
				+ if_value_drop + ", meta_value=" + meta_value + "]";
	}
	
	
	
	

}
