package com.mdi.dto;

import java.util.List;

import com.mdi.model.SearchCriteria;

public class SearchForm {

	private Long sub_dept_id;
	
	private Long repositoryid;
	
	private String searchtype;
	
	private String searchtext;
	
	private List<SearchCriteria> searchlist;

	private Long year;

	public Long getSub_dept_id() {
		return sub_dept_id;
	}

	public void setSub_dept_id(Long sub_dept_id) {
		this.sub_dept_id = sub_dept_id;
	}

	public Long getRepositoryid() {
		return repositoryid;
	}

	public void setRepositoryid(Long repositoryid) {
		this.repositoryid = repositoryid;
	}

	public String getSearchtype() {
		return searchtype;
	}

	public void setSearchtype(String searchtype) {
		this.searchtype = searchtype;
	}

	public String getSearchtext() {
		return searchtext;
	}

	public void setSearchtext(String searchtext) {
		this.searchtext = searchtext;
	}

	public List<SearchCriteria> getSearchlist() {
		return searchlist;
	}

	public void setSearchlist(List<SearchCriteria> searchlist) {
		this.searchlist = searchlist;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}
	
	
}
