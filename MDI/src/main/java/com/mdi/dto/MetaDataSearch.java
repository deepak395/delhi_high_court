package com.mdi.dto;

public class MetaDataSearch 
{
	private Long if_id;
	private String if_value;
	
	public Long getIf_id() {
		return if_id;
	}
	public void setIf_id(Long if_id) {
		this.if_id = if_id;
	}
	public String getIf_value() {
		return if_value;
	}
	public void setIf_value(String if_value) {
		this.if_value = if_value;
	}
	@Override
	public String toString() {
		return "MetaDataSearch [if_id=" + if_id + ", if_value=" + if_value + "]";
	}
	
	

}
