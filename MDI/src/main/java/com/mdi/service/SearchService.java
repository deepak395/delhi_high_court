package com.mdi.service;

import java.util.List;

import com.mdi.dto.MetaDataSearch;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.IndexField;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;

public interface SearchService {

	public List<Repository>  getRepositories();
	public List<DocumentFileDetails> getAllWithoutIndex();
	public DocumentFileDetails save(DocumentFileDetails doc);
	public SubDepartment getSubDeptById(Long s_dept_id);
	public Department getDeptById(Long parent_id);
	public List<DocumentFileDetails> getcaseFilesBySearchqueryContent(String querystring);
	public IndexField getindexfieldsById(Long if_id);
	public List<DocumentFileDetails> getSearchFiles(List<MetaDataSearch> md,Long dept_id,Long sub_dept_id, Long dt_id,Long md_year,Long fd_id)throws Exception;
	public List<String> getindexfields(Long sub_dept_id);
	
	
}
