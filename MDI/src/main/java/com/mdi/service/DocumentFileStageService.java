package com.mdi.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.DocumentFileStage;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;

@Service
public class DocumentFileStageService 
{
	
	//public List<Department> getDepartments() throws Exception;
	
	/*public Repository saveFolder(Folder folder) throws Exception;
	
	public List<Folder> getAllFolders() throws Exception;
	
	public Department getDepartmentById(Long id) throws Exception;*/


		@PersistenceContext 
		private EntityManager em;
		
		@Transactional
		public DocumentFileStage  save(DocumentFileStage s) {

			DocumentFileStage master = null;
			try {
				master = em.merge(s);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return master;
		}
		
		
		@Transactional
		public Long  getUploadFiles(Long user,Long stage) {
			Long result = 0L;

			Query query = em
					.createQuery("select count(dfs) from DocumentFileStage dfs where dfs_cr_by="+user+ " and dfs_stage_lid="+stage);

			System.out.println("query2=" + query);
			try {
				result = (Long) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(result);
			return result;
		}
		
		@Transactional
		public Long  getUploadFilesForAdmin(Long user,Long stage) {
			Long result = 0L;

			Query query = em
					.createQuery("select count(dfs) from DocumentFileStage dfs where dfs_stage_lid="+stage);

			System.out.println("query2=" + query);
			try {
				result = (Long) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(result);
			return result;
		}
		
		@Transactional
		public Long  getUploadFilesCountForAdmin(Long dept_id,Long stage) {
			Long result = 0L;

			Query query = em
					.createQuery("select count(dfd) from DocumentFileDetails dfd where dfd.dfd_dept_mid="+dept_id);

			System.out.println("query2=" + query);
			try {
				result = (Long) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(result);
			return result;
		}
		
		@Transactional
		public Long  getUploadFilesCountForSysAdmin(Long dept_id,Long stage) {
			Long result = 0L;

			Query query = em
					.createQuery("select count(dfd) from DocumentFileDetails dfd");

			System.out.println("query2=" + query);
			try {
				result = (Long) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(result);
			return result;
		}
		
}
