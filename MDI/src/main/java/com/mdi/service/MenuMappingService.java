package com.mdi.service;

import java.util.List;

import com.mdi.model.Tree;
import com.mdi.model.User;

public interface MenuMappingService {

	List<User> getAlluserById();
	List<Tree> getRoleTree();
}
