/*package com.mdi.service;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.mdi.dto.Index_Meta_Data;
import com.mdi.dto.MetaDataSearch;
import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.DocumentFileStage;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.service.DocumentFileDetailsService;
import com.mdi.service.DocumentFileStageService;
import com.mdi.service.LookupServices;
import com.mdi.service.MetaDataService;
import com.mdi.service.UploadDocumentService;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.management.loading.PrivateClassLoader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Request;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
public class UploadDocumentController {
	
	@Autowired
	private MetaDataService service;
	
	@Autowired
	private UploadDocumentService  Upservice;
	
	@Autowired
	private LookupServices lookupservices;
	
	@Autowired
	private DocumentFileDetailsService docuumetFileDetailsService;
	
	@Autowired
	private DocumentFileStageService docuumetFileStageService;
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private CommonController common;
	
	CommenMethods cm;
	
	public UploadDocumentController()
	{
		cm= new CommenMethods();
	}

	@RequestMapping("document/UploadDocument")
	public String UploadDocument()
	{
		System.out.print("Hello");
		return "views/document/UploadDocument";
	}
	@RequestMapping("/metadata/manage")
	public String Manage()
	{
		System.out.print("Hello");
		return "views/metadata/manage";
	}
	@RequestMapping(value="/UploadDocument/getAllDepartments",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getDepartments()
	{
		List<Department> repo= new ArrayList<Department>();
		
		try 
		{
			repo=service.getDepartments();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	
	@RequestMapping(value="/UploadDocument/getUserwisefiles",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getUserWiseFiles(HttpSession session)
	{
		User user=new User();
		user=(User) session.getAttribute("USER");
			List <DocumentFileDetails> dfd=new ArrayList<DocumentFileDetails>();
				try 
				{
					dfd= docuumetFileDetailsService.getFilesByUserWise(user);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			String result=cm.convert_to_json(dfd);
			return result;
	}
	public String changeDocFileStatus(Long id,HttpSession session)
	{
		ActionResponse<DocumentFileDetails> response = new ActionResponse<>();
		String result="";
		DocumentFileDetails docfile = service.getDocumentFileDetailsById(id);
		    Lookup	role2= lookupservices.getAllByLongname("MDIChecker");  //11
			Lookup	role1= lookupservices.getAllByLongname("MDIMetaData");	//21	
		try 
		{
			User user = (User) session.getAttribute("USER");
			
			System.out.println(id);
			List<Object> obj=new ArrayList<Object>();
			Long userId=0L;
			System.out.println(user.getUserroles().get(0).getUr_role_id());
			if(user.getUserroles().get(0).getUr_role_id().equals(role1.getLk_id())) {
				if(userId==0L)
				{
					userId=docuumetFileDetailsService.getNewUserList("MDIChecker",15L);
				}
			
				if(userId==0L)
				{
					obj=docuumetFileDetailsService.getUserList("MDIChecker",15L);
					userId=common.getLeastTaskUser(obj);
				}
				docfile.setDfd_stage_lid(15l);
				docfile.setDfd_assign_to(userId);
				service.mergedocumentFile(docfile);
				System.out.println(userId);
				DocumentFileStage dc_fs=new DocumentFileStage();
			
				Long index =service.getMaxIndexOfDocumentFileStage(docfile.getDfd_id());
				dc_fs.setDfs_assign_to(userId);	
				dc_fs.setDfs_cr_by(user.getUm_id());
				dc_fs.setDfs_dfd_mid(docfile.getDfd_id());
				dc_fs.setDfs_rec_status(1);
				dc_fs.setDfs_stage_lid(15l);
				dc_fs.setDfs_reject_status('N');
				dc_fs.setDfs_cr_date(new Date());
				dc_fs.setDfs_stage_date(new Date());
				dc_fs.setDfs_index(index+1l);
			
				service.saveDocumentFileStage(dc_fs);
				response.setResponse("TRUE");
			}
			System.out.println(role2.getLk_id());
			if(user.getUserroles().get(0).getUr_role_id().equals(role2.getLk_id())) {
				if(userId==0L)
				{
					userId=docuumetFileDetailsService.getNewUserList("MDIAdmin",16L);
				}
			
				if(userId==0L)
				{
					obj=docuumetFileDetailsService.getUserList("MDIAdmin",16L);
					userId=common.getLeastTaskUser(obj);
				}
				docfile.setDfd_stage_lid(16l);
				docfile.setDfd_assign_to(userId);
				service.mergedocumentFile(docfile);
				System.out.println(userId);
				DocumentFileStage dc_fs=new DocumentFileStage();
			
				Long index =service.getMaxIndexOfDocumentFileStage(docfile.getDfd_id());
				dc_fs.setDfs_assign_to(userId);	
				dc_fs.setDfs_cr_by(user.getUm_id());
				dc_fs.setDfs_dfd_mid(docfile.getDfd_id());
				dc_fs.setDfs_rec_status(1);
				dc_fs.setDfs_stage_lid(16l);
				dc_fs.setDfs_reject_status('N');
				dc_fs.setDfs_cr_date(new Date());
				dc_fs.setDfs_stage_date(new Date());
				dc_fs.setDfs_index(index+1l);
			
				service.saveDocumentFileStage(dc_fs);
				response.setResponse("TRUE");
			}
			
		}

		catch (Exception e) 
		{
			e.printStackTrace();
			response.setResponse("ELSE");
		}
		response.setModelData(docfile);
		result= cm.convert_to_json(response);
		return result;
	}
	
	@RequestMapping(value="/UploadDcoumet/getfiles",method=RequestMethod.POST,headers = "Accept=application/json")
	public @ResponseBody String getFiles(HttpSession session,HttpServletRequest request)
	{
		String name=null;
		//,@RequestParam(value="dept_id")Long dept_id,@RequestParam(value="sub_dept_mid")Long sub_dept,@RequestParam(value="year")Long year,@RequestParam(value="name")String name
        Long dept_id=Long.parseLong(request.getParameter("dept_id"));
        Long sub_dept=Long.parseLong(request.getParameter("sub_dept_id"));
        Long year=Long.parseLong(request.getParameter("year"));
        name=request.getParameter("name");
        
        System.out.println(name+" ddddd");
		List <DocumentFileDetails> dspres=new ArrayList<DocumentFileDetails>();
		if(name.equals("undefined")){
			try 
			{
				dspres= docuumetFileDetailsService.getFilesBySubDeptAndYear(dept_id,sub_dept,year);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		System.out.println( dspres.size()+" m m");
		
		String result=cm.convert_to_json(dspres);
		return result;
	}
	
	@RequestMapping(value="/UploadDocument/approveALl",method=RequestMethod.POST)
	public @ResponseBody String approveAll(@RequestBody List<MetaDataSearch> dfd_id ,HttpSession session)
	{
		String repo=null;
		
		System.out.println("viru"+dfd_id.size());
		
	    try 
		{
	    	for(MetaDataSearch dfd : dfd_id) {
	    		repo=changeDocFileStatus(dfd.getIf_id(),session);
	    	}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	@RequestMapping(value="/UploadDcoumet/getSubDept",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getSubDept(HttpSession session,@RequestParam(value="dept_id")Long dept_id)
	{
		List<SubDepartment> repo= new ArrayList<SubDepartment>();
		
		try 
		{
			repo=service.getSubDept(dept_id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	 @RequestMapping(value = "/UploadDcoumet/test",method = RequestMethod.POST)
	 public @ResponseBody String createtest(MultipartHttpServletRequest request,HttpSession session) throws DocumentException{
	 MultipartFile mpf = null;
	 DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
	    Date startDate = new Date();;
	    
 	//ActionResponse<Repository> response = new ActionResponse();
 	User user=new User();
		user=(User) session.getAttribute("USER");
		
		 DocumentFileDetails  documenttemp=new DocumentFileDetails();
		 DocumentFileStage documentstage=new DocumentFileStage();
		 long dept_id=(Long.parseLong(request.getParameter("dept_id")));
		 
		 long sub_dept_id=(Long.parseLong(request.getParameter("sub_dept_id")));
		 System.out.println(sub_dept_id);
		 long year=(Long.parseLong(request.getParameter("year")));
		 Department  dept= new Department();
		 String jsonData="";
		 Lookup lookup=lookupservices.getLookUpObject("REPOSITORYPATH");
		 SubDepartment sub_dept= new SubDepartment();
			try 
			{
				dept=Upservice.getDepartment(dept_id);
				sub_dept=Upservice.getsubdeptById(sub_dept_id);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
	 	Iterator<String> itr = request.getFileNames();
	 	List<User> userID = new ArrayList<User>();
		
		userID=userservice.getUserByRole("MDIMetaData");
		
		System.out.println(userID);
		System.out.println(userID.size());	
		System.out.println(year);
		int count=0;
	 	while (itr.hasNext()) 
		{
	 		
			mpf = request.getFile(itr.next());
			int newPageCount;
			String filename=mpf.getOriginalFilename();
			String filenamereal=filename.substring(0,filename.length()-4)+"_"+year+".pdf";
			System.out.println("Filename="+filenamereal);
			Long file_stage=0L;
			File path = new File(lookup.getLk_longname() + File.separator + dept.getDept_name() + File.separator +sub_dept.getSub_dept_name()+ File.separator + year);
			String temppath=path+File.separator + filenamereal;
			System.out.println(temppath);
			String ext = FilenameUtils.getExtension(temppath);
			File oldfile=new File(temppath);
			if (!path.exists()) {
			    path.mkdirs();
			}
			if(count==userID.size()) {
				count=0;
			}
			
			if(ext.equalsIgnoreCase("pdf") && !oldfile.isFile())
			{
				try {
					FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(temppath));
					PdfReader readernewFile = new PdfReader(temppath);
					newPageCount = readernewFile.getNumberOfPages();
					 readernewFile.close(); 
					 documenttemp.setDfd_year(year);
					 documenttemp.setDfd_cr_by(user.getUm_id());
					 documenttemp.setDfd_cr_date(new Date());
					 documenttemp.setDfd_dept_mid(dept.getDept_id());
					 documenttemp.setDfd_file_name(filenamereal);
					 documenttemp.setDfd_name(filename.substring(0,filename.length()-4));
					 documenttemp.setDfd_page_count(newPageCount);
					 documenttemp.setDfd_rec_status(1);
					 documenttemp.setDfd_sub_dept_mid(sub_dept.getSub_dept_id());
					 documenttemp.setDfd_rep_mid(1L);
					 documenttemp.setDfd_stage_lid(14L);
					 documenttemp.setDfd_assign_to(userID.get(count).getUm_id());
					 documenttemp.setDfd_edit_mode(0);
					 documenttemp.setDfd_mod_by(null);
					 documenttemp.setDfd_mod_date(null);
					 documenttemp.setDfd_print_flag(1);
					 documenttemp.setDfd_remark(null);
					 documenttemp.setDfd_reject_status('N');
					 documenttemp.setDfd_system_page_count(0);
					 
					 DocumentFileDetails dfd= docuumetFileDetailsService.save(documenttemp);
					 
					 
					 documentstage.setDfs_assign_to(userID.get(count++).getUm_id());
					 documentstage.setDfs_cr_by(user.getUm_id());
					 documentstage.setDfs_cr_date(new Date());
					 documentstage.setDfs_dfd_mid(dfd.getDfd_id());
					 documentstage.setDfs_index(1L);
					 documentstage.setDfs_rec_status(1);
					 documentstage.setDfs_reject_status('N');
					 documentstage.setDfs_remark(null);
					 documentstage.setDfs_stage_date(new Date());
					 documentstage.setDfs_stage_lid(14L);
					 
					 docuumetFileStageService.save(documentstage);
					 
				} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("File Uploded...!");
			}
			else{
				System.out.println("file is already there...!");
			}
			
		}
		 return "Success";
	 }
	 @RequestMapping(value = "/document/create",method = RequestMethod.POST)
	    public @ResponseBody String create(MultipartHttpServletRequest request,HttpSession session ){
			
			String jsonData="";
			String path="";
			String parentfolderName="";
			String childfolderName="";
			 
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
		    Date startDate = new Date();;
		    
	    	//ActionResponse<Repository> response = new ActionResponse();
	    	User user=new User();
			user=(User) session.getAttribute("USER");
			
			Document documenttemp=new Document();
			long folderid=(Long.parseLong(request.getParameter("folder_id")));
			Folder getChildFolderid=documentService.getChildFolderId(folderid);
		
			
			Folder getParentFolderId=documentService.getParentFolderId(getChildFolderid.getParent_id());
			
			
	    	 Repository repository=repositoryService.getRepository(getChildFolderid.getRep_id());
	    	 
	    	 String repBasepath=repository.getBasepath()+File.separator+repository.getName();
	    	 parentfolderName =getParentFolderId.getFolder_name()+File.separator+repository.getName();
	    	 childfolderName=getChildFolderid.getFolder_name()+File.separator+repository.getName();
	    	 
	    	
	     	
			Iterator<String> itr = request.getFileNames();
			MultipartFile mf=null;
			MultipartFile mf2=null;
			
			while(itr.hasNext())
			{
			  mf =	request.getFile(itr.next());
			  
			  try
			  {
				  
				  String file_name=mf.getOriginalFilename();
				  
				  path=repository.getBasepath()+File.separator+getParentFolderId.getFolder_name()+File.separator+getChildFolderid.getFolder_name()+File.separator+mf.getOriginalFilename();
					 
				  String ext = FilenameUtils.getExtension(path);
				  
				  FileCopyUtils.copy(mf.getBytes(), new FileOutputStream(path));
				  
				  PdfReader readernewFile = new PdfReader(path);
				  int newPageCount = readernewFile.getNumberOfPages();
				  readernewFile.close(); 
				  
				    documenttemp.setDept_id(getChildFolderid.getDept_id());
			 		documenttemp.setRep_id(getChildFolderid.getRep_id());
			     	documenttemp.setFolder_id(getChildFolderid.getId());
			     	documenttemp.setFile_name(file_name);
			     	documenttemp.setCreated(new Date());
					documenttemp.setStatus(1);
					documenttemp.setDoc_reject_status("N");
					documenttemp.setFile_extension(ext);
					documenttemp.setDoc_stage_lid((long) 14);
					documenttemp.setDoc_page_count(newPageCount);
					documenttemp=documentService.save(documenttemp);
			  }
			  catch(Exception ex)
			  {
				  ex.printStackTrace();
			  }
			}
			
	    	
			return null;  
		}
	 @RequestMapping(value = "/uploadfile/create",method = RequestMethod.POST)
	    public @ResponseBody String create(MultipartHttpServletRequest request,HttpSession session) throws DocumentException 
		{
	    	
	    	String jsonData="";
	    //	ActionResponse<UploadedFilesLog>response=new ActionResponse();
	    	ActionResponse<UploadedFilesLog>response=uploadfilesvalidator.doValidation();
	    	response.setResponse("TRUE");
	    	String ipaddress = request.getRemoteAddr();
	    	
	    	User user=new User();
			user=(User) session.getAttribute("USER");

					
			     MultipartFile mpf = null;
			     Iterator<String> itr = request.getFileNames();
			    	
			
				 String basePath="";
				 List<Lookup> lookupForRaw = lkservice.getAll("REPOSITORYPATH");
	    	     List<Lookup> lookupForUBackup = lkservice.getAll("UPLOADED_FILES_BACKUP");
				 
				String repBasepath =lookupForRaw.get(0).getLk_longname();	
				
				String backupPath = lookupForUBackup.get(0).getLk_longname(); 
			//	Map<String, Object> error = new HashMap<String, Object>();
				List <Object> errorList=new ArrayList();

					while (itr.hasNext()) 
					{
						try
						{
						mpf = request.getFile(itr.next());
				         		
						UploadedFilesLog ufl = new UploadedFilesLog();
						
						int oldPageCount=0;
						int newPageCount;
						String filename = mpf.getOriginalFilename();  
						int length=filename.length();
						
				        String temppath=repBasepath + File.separator +mpf.getOriginalFilename();
	                    String backuppath=backupPath + File.separator +mpf.getOriginalFilename();
	                    
						String ext = FilenameUtils.getExtension(temppath);
						File oldfile=new File(temppath);
						
						File backupfile =new File(backuppath);
						
						
						  List <CaseFileDetail> cfd = new ArrayList<CaseFileDetail>();
						  CaseFileDetail cfdobj=new CaseFileDetail();
						  DocumentFileDetails dfd = new DocumentFileDetails();
						  Long file_stage=0L;
						  Long file_status=351417L;
				
						  if(ext.equalsIgnoreCase("pdf") && oldfile.isFile())
							{			  
						  
						 if(length == 16)
						    { 	
						       String file_name=filename.substring(0, 12);
						       ufl.setUfl_document_type(249L);
						       ufl.setUfl_file_bar_code(file_name);					      
						       cfd = casefileUpdateService.getBarCodeData(file_name);
						       if(!cfd.isEmpty())
						       {
						    	   cfdobj=cfd.get(0);
						    	   file_stage=cfdobj.getFd_stage_lid();
						    	   file_status=cfdobj.getFd_case_status();
						       }
						    }
						    else if(length>16) 
						    {
						    	String file_name=filename.substring(0,(length-4));
						    	ufl.setUfl_document_type(250L);
						    	ufl.setUfl_file_bar_code(file_name);
						    	dfd = documentfiledetailservice.checkUniqueBarcode(file_name);
						    	if(dfd != null)
						    	{
						    		file_stage=dfd.getDfd_stage_lid();
						    	}
							}
						
						
							//if((file_stage==13 || file_stage== 351075) && file_status.longValue()==351417L)
						 if((file_stage==13 || file_stage== 351075))
							{
						         try{
							      PdfReader reader = new PdfReader(temppath);	
								   oldPageCount=reader.getNumberOfPages();
									 reader.close();
						         }
						         catch(Exception e)
						         {
						        	 e.printStackTrace();
						         }
							        ufl.setUfl_ip_address(ipaddress);
							        ufl.setUfl_previous_page_count(oldPageCount);
							        ufl.setUfl_uploaded_by(user.getUm_id());
							        ufl.setUfl_uploaded_date(new Date());
							        ufl.setUfl_stage_lid(file_stage);
							        
							    FileCopyUtils.copy(oldfile,backupfile);					    
							    FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(temppath));
							    
							    PdfReader readernewFile = new PdfReader(temppath);
								newPageCount = readernewFile.getNumberOfPages();
								 readernewFile.close(); 
								 
								 ufl.setUfl_new_page_count(newPageCount);
								
								 if(length == 16)
								    { 	
								       cfdobj.setFd_system_page_count(newPageCount);
								       casefiledetailservice.save(cfdobj);
								    }
								    else if(length>16) 
								    {
								    	dfd.setDfd_system_page_count(newPageCount);
								    	documentfiledetailservice.save(dfd);
									}
					             uploadfileservice.save(ufl);
					      }
					     else 
						  {
								//error.put(filename, filename+" File Does Not Exist at Verifier Stage");
								errorList.add(filename+" File Does Not Exist at Verifier Stage OR Uploaded File Status Is Wrong");
							  	 
						  }	
					   }
					 else 
					 {
						//error.put(filename, filename+" File Does Not Exist in Raw_Pdf Folder");
						errorList.add(filename+" File Does Not Exist in Raw_Pdf Folder");
					 }	
					}
						catch (IOException e) {
						e.printStackTrace();
					}
				 			
			 }
	    	  
					//response.setDataMapList(error);
					response.setDataList(errorList);
					if(response != null)
					{
						jsonData = commonMethods.convert_to_json(response);
					}
			if(error != null)
			{
				response.setResponse("FALSE");
				response.setDataMapList(error);
				response.setData("TRUE");
				
				//String err=error.toString();
				jsonData = commonMethods.convert_to_json(response);
				//jsonData = "{\"error\":"+err+"}";
				if(response!=null)
				{
				jsonData = commonMethods.convert_to_json(response);
				}
			} 
			else 
			{
			  response.setResponse("TRUE");	
			}
			
			return jsonData;
		
		}
	@RequestMapping(value="/metadata/getindexfields",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getIndexFields(HttpSession session,@RequestParam(value="sub_dept_id")Long sub_dept_id)
	{
		List<IndexField> repo= new ArrayList<IndexField>();
		
		try 
		{
			repo=service.getindexfields(sub_dept_id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	
	@RequestMapping(value="/metadata/getfielddata",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getFieldData(HttpSession session,@RequestParam(value="if_id")Long if_id)
	{
		List<Lookup> repo= new ArrayList<Lookup>();
		
		try 
		{
			repo=service.getFieldDataService(if_id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
}
*/