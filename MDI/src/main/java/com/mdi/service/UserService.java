package com.mdi.service;

import java.util.List;

import com.mdi.model.Department;
import com.mdi.model.LoginLog;
import com.mdi.model.Lookup;
import com.mdi.model.ObjectMaster;
import com.mdi.model.Permission;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;

public interface UserService 
{
	public User validateLogin (User user) throws Exception;
	
	public User forgetPassword (String  username) throws Exception;
    
	public User save(User user);
	
	public List<Lookup> getAll(String setname) ;
	
	public User getUserDetail(Long um_id); 
	
	public List<User> getAllUser();
	
	public void update(User user);
	
	public void saveLoginLog(LoginLog log,User user);
	
	public User getChangepassword(long uid);

	List<ObjectMaster> getUserObjects(Long um_id);
	
	public User getUser(Long id);

	public List<User> getUserByRole(String string);

	public Department getDeptById(Long um_department_id);
	
	public List<LoginLog> getLoginLog(Long userId);
	
	public LoginLog saveLog(LoginLog s);

	public List<SubDepartment> getSubDepartmentIds(Long dpt_id);

	public List<Permission> getUserPermittedSubDepts(Long um_id);

	public void saveUserSubDeptPermissions(List<Permission> perm) throws Exception;
	
	public List<Department> getAllDepartment();

	public User getUserByName(String username) throws Exception;
	
	
}



