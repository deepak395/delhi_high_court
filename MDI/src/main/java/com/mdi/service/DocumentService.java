package com.mdi.service;

import com.mdi.model.Document;
import com.mdi.model.Folder;

public interface DocumentService {

	Folder getChildFolderId(long folderid);

	Folder getParentFolderId(long folderid); 
	
	public Document save(Document d);
}
