package com.mdi.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Department;
import com.mdi.model.Document;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.DocumentFileStage;
import com.mdi.model.Document_type;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.MetaData;
import com.mdi.model.Permission;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;

@Service
public class MetaDataService 
{
		@PersistenceContext 
		private EntityManager em;
		
		public List<Department> getDepartments(){
			List<Department> list = new ArrayList<Department>();
			try 
			{
				Query query = em.createQuery("Select d from Department d where status=1 ");
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		public List<SubDepartment> getSubDept(Long dept_id){
			List<SubDepartment> list = new ArrayList<SubDepartment>();
			try 
			{
				Query query=em.createQuery("SELECT sd from SubDepartment sd Where (PARENT_ID=:id)");
				query.setParameter("id",dept_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		
		public List<IndexField> getindexfields(Long sub_dept_id){
			List<IndexField> list = new ArrayList<IndexField>();
			try 
			{
				Query query=em.createQuery("SELECT ifd from IndexField ifd Where (if_parent=:id and if_rec_status=0) order by if_sequence");
				query.setParameter("id",sub_dept_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		public List<Lookup> getFieldDataService(Long if_id){
			List<Lookup> list = new ArrayList<Lookup>();
			try 
			{
				Query query=em.createQuery("SELECT lk from Lookup lk Where (lk_parent=:id)");
				query.setParameter("id",if_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}


	 public IndexField getIndexFieldById(long id)
	 {
		 IndexField master = null;
		 try 
		 {
			 		master =em.find(IndexField.class,id);
		 } 
		 catch (Exception e)
		 {
			e.printStackTrace();
		}
		 return master;
	 }
	 
	 @Transactional
	 public void mergeIndexField(IndexField index) throws Exception
	 {
		
		 try 
		 {
			 em.merge(index);
		 } 
		 catch (Exception e)
		 {
			e.printStackTrace();
			throw new Exception();
		}
		 
	 }
	 @Transactional
	 public void delteIndexFiled(long id) throws Exception{
		 
		 IndexField master=null;
		 try 
		 {
			     master=em.find(IndexField.class,id);
			     master.setIf_rec_status(1);
			     
			     em.merge(master);
		  } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
			throw new Exception();
		  }
	 }
	 public Integer getSequenceId(long id) throws Exception{
		 System.out.println(id);
		 Integer result = null;
		 try 
		 {
			    String sql="Select max(f.if_sequence) from IndexField f where f.if_parent=:iid";
	    	    result=(int) em.createQuery(sql).setParameter("iid", id).getSingleResult();	
		  } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
			return 0;
		  }
		 
		 return result;
	 }
	 @Transactional
     public void save(IndexField index) throws Exception{
		 
		 try 
		 {
			 	em.merge(index);
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
			throw new Exception();
		  }
		 
	 }
	 
	 public Document getDocumentById(Long id)
	 {
		 Document doc =null;
		 try 
		 {
			doc=em.find(Document.class, id);
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
		 return doc;
	 }
	 
	 public DocumentFileDetails getDocumentFileDetailsById(Long id)
	 {
		 DocumentFileDetails doc =null;
		 System.out.println("getDocumentFileDetailsById  id" + id);
		 try 
		 {
			/* String sql="Select d from document_file_details d where d.dfd_id="+id+" ";
	    	 doc=(DocumentFileDetails) em.createNativeQuery(sql).getSingleResult();*/
			/*doc=em.find(DocumentFileDetails.class, id);*/
	    	 
	    	 	Query query = em.createQuery("Select d from DocumentFileDetails d where d.dfd_id=:id");
				query.setParameter("id", id);
				doc=(DocumentFileDetails) query.getSingleResult();
	    	 
	    	 
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
		 
		
		 return doc;
	 }
	 
	 
	 public boolean checkMetaDataExistence(Long doc_id,Long if_id)
	 {
		 boolean result = false;
		 MetaData meta = null;
		 try 
		 {
			Query query = em.createQuery("Select m from MetaData m where (md_mf_mid=:inf_id and md_fd_mid=:doc_id)");
			query.setParameter("inf_id", if_id);
			query.setParameter("doc_id", doc_id);
			meta=(MetaData) query.getSingleResult();
			if(meta.getMd_id()!=null)
			{
				return true;
			}
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
		 return result;
	 }
	 
	 public Integer getMetaDataSequence(Long doc_id,Long if_id)
	 {
		 Integer seq=0;
		 try 
		 {
			String sql="Select max(f.md_sequence) from MetaData f where ( md_fd_mid=:doc_id)";
			Query query = em.createQuery(sql);
			
			query.setParameter("doc_id", doc_id);
			seq=(Integer) query.getSingleResult();
			System.out.println(seq+"   gggg");
			if(seq==null)
			{
				return 0;
			}
			else
			{
			   return seq;
			}
		 } 
		 catch (Exception e) 
		 {
			 System.out.println("durgendra");
			e.printStackTrace();
		 }
		 return 0;
	 }
	 
	 public Long getMetaDataSequence(Long doc_type_id)
	 {
		 
		 Long result=0L;
		 try 
		 {
			
			String sql="Select if.if_id from IndexField if where if.if_add_multiple=1 and if.if_parent="+doc_type_id;
			result = Long.parseLong(em.createQuery(sql).getSingleResult().toString());
			System.out.println(result+"       result for indexfield id");
			//System.out.println(seq+"   gggg");
			//return result;
		 } 
		 catch (Exception e) 
		 {
			 System.out.println("durgendra");
			e.printStackTrace();
		 }
		 return result;
	 }

	 
	 @Transactional
	 public void saveMetaData(MetaData meta)
	 {
		 try 
		 {
			em.merge(meta);
		 }
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
	 }
	 
	 /*............will be worked  later ...........*/
	public List<Lookup> getIndexFieldsFromLookUp()
	{
		List<Lookup> list = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("");
		} 
		catch (Exception e) 
		{
			
		}
		return null;
	}
	public List<Lookup> getLookupByName(String if_name) 
	{
		List<Lookup> look = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("Select l  from Lookup l where (lk_setname=:name)");
			query.setParameter("name", if_name);
			look= query.getResultList();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return look;
	}
	
	public MetaData  getMetaData (Long doc_id,Long if_id)
	{
		 MetaData meta = null;
		 try 
		 {
			Query query = em.createQuery("Select m from MetaData m where (md_mf_mid=:inf_id and md_fd_mid=:doc_id)");
			query.setParameter("inf_id", if_id);
			query.setParameter("doc_id", doc_id);
			meta=(MetaData) query.getSingleResult();
			
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
		 return meta;
	}
	
	public List<MetaData>  getMetaDataByDfd(Long doc_id)
	{
		 List<MetaData> meta = null;
		 try 
		 {
			Query query = em.createQuery("Select m from MetaData m where (md_fd_mid=:doc_id)");
			query.setParameter("doc_id", doc_id);
			meta=(List<MetaData>) query.getResultList();
			System.out.println(meta);
		 } 
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
		 return meta;
	}
	
	@Transactional
	public void mergeMetaData(MetaData meta) 
	{
		try 
		{
			em.merge(meta);
		    MetaData data=	em.find(MetaData.class, meta.getMd_id());
		    System.out.println(data);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	public String getDropDownValueFromLookup(String meta_value) 
	{
		
		String result="";
		try 
		{
			System.out.println(meta_value+" dddd");
			Long id=Long.parseLong(meta_value);
			Query query = em.createQuery("Select l.lk_longname from Lookup l where (lk_id=:lk_id)");
			query.setParameter("lk_id", id);
			result=(String) query.getSingleResult();
			return result;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Transactional
	public void mergedocumentFile(DocumentFileDetails docfile) 
	{
		try 
		{
			em.merge(docfile);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
	}

	public Long getMaxIndexOfDocumentFileStage(Long dfd_id) 
	{
		Long result = null;
		try 
		{
			Query query = em.createQuery("Select max(d.dfs_index) from DocumentFileStage d where(dfs_dfd_mid=:id)");
			query.setParameter("id", dfd_id);
			result = (Long) query.getSingleResult();
			System.out.println(result);
			if(result.equals(null))
			{
				return 0l;
			}
			return result;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return 0l;
		}
		
	}
	
	@Transactional
	public void saveDocumentFileStage(DocumentFileStage dc_fs) throws Exception 
	{
		try 
		{
			em.merge(dc_fs);
		} 
		catch (Exception e) 
		{
			throw new Exception("Some thing error",e);
		}
		
	}
	public List<Document_type> getDocumentTypeBySubDept(Long sub_dept_id) 
	{
		 List<Document_type> dt = new ArrayList<>();
		 try 
		 {
			Query query = em.createQuery("select dt from Document_type dt where (dt_sub_mid =:sub_dept AND dt_rec_status =1  ) ");
			query.setParameter("sub_dept", sub_dept_id);
			dt = query.getResultList();
		 } 
		 catch (Exception e) 
		 {
			 e.printStackTrace();
			return null;
		 }
		 return dt;
		
	}
	@SuppressWarnings("unchecked")
	public List<Folder> getFoldersByParentId(Long dt_id) 
	{
		List<Folder> folder = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("select fo from Folder fo where ( parent_id =:dt_id AND status =1  ) ");
			query.setParameter("dt_id", dt_id);
			folder= query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return folder;
	}
	public List<Long> getSubDeptIdsPerMission(User user) {
		List<Long> list = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("select p.id from Permission p where p.pm_um_mid="+user.getUm_id()+" and p.pm_rec_status=1");
			list=query.getResultList();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	public List<Department> getVikasDepartments(){
		List<Department> list = new ArrayList<Department>();
		try 
		{
			Query query = em.createQuery("Select d from Department d where dept_id in(24,12) ");
			list= query.getResultList();
			return list;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return list;
	}
	public List<Department> getAtishDepartments(){
		List<Department> list = new ArrayList<Department>();
		try 
		{
			Query query = em.createQuery("Select d from Department d where dept_id in(25,26)");
			list= query.getResultList();
			return list;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<Department> getNamartaDepartments(){
		List<Department> list = new ArrayList<Department>();
		try 
		{
			Query query = em.createQuery("Select d from Department d where dept_id in(16,17)");
			list= query.getResultList();
			return list;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return list;
	}
	public List<Permission> getUserPermission(Long um_id) {
		List<Permission> list = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("select p from Permission p where p.pm_um_mid="+um_id+" and p.pm_rec_status=1");
			list=query.getResultList();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return list;
}
	
	
	
	
	
	
}
