package com.mdi.service;

import java.util.List;

import com.mdi.model.ObjectMaster;
import com.mdi.model.ObjectTree;
import com.mdi.model.RoleObject;

public interface ObjectMasterService {
 
	public ObjectMaster save(ObjectMaster objectMaster);
	
	public List<RoleObject> getDataByRoleID(Long roleID);
	
	public List<ObjectTree> getRoleTree();
}
