package com.mdi.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;

@Service
public class CommanReportsServices {
	@PersistenceContext
	private EntityManager em;

	public List<Object> getLoginData(String um_id, String fromDate, String toDate) {

		List<Object> result = new ArrayList<Object>();
		result = null;
		System.err.println("from " + fromDate + " To " + toDate + "um_id : " + um_id);

		String sql = "select u.username,u.um_fullname,ll.ll_login_time,ll.ll_logout_time,ll.ll_ip_address from User u,LoginLog ll where"
				+ " ll.ll_user_mid=u.um_id ";

		if(fromDate!=null && toDate!=null) {
			sql+= " and to_date(to_char(ll.ll_login_time, 'yyyy-MM-dd'),'yyyy-MM-dd') between to_date('" + fromDate
			+ "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd')";	
		}
		
		if (um_id != null) {
			sql += " and u.um_id=" + um_id + "  order by ll.ll_login_time desc";
			System.out.println("UserWiseLoginReport:" + sql);
			result = em.createQuery(sql).getResultList();
		}
		
		else {
			sql += "  order by ll.ll_login_time desc";
			System.out.println("All :" + sql);
			result = em.createQuery(sql).getResultList();
		}
		return result;
	}

	public List<DocumentFileDetails> getUploadedData(String dept_id, String fromDate, String toDate) {

		List<DocumentFileDetails> result = new ArrayList<DocumentFileDetails>();
		result = null;
		System.err.println("from " + fromDate + " To " + toDate + "dept_id : " + dept_id);

		String sql = "select dfd from DocumentFileDetails dfd  where "
				+ " to_date(to_char(dfd.dfd_cr_date, 'yyyy-MM-dd'),'yyyy-MM-dd') between to_date('" + fromDate
				+ "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd')";

		if (dept_id != null) {
			sql += " and dfd.dfd_dept_mid=" + dept_id;
			System.out.println("UploadedReport:" + sql);
			result = em.createQuery(sql).getResultList();
		}
		/*
		 * else { sql+= " group by d.dept_name";
		 * System.out.println("All :"+sql); result =
		 * em.createQuery(sql).getResultList(); }
		 */
		return result;
	}

	public List<Object> getMetaEntryData(String dept_id, String fromDate, String toDate) {

		List<Object> result = new ArrayList<Object>();
		result = null;
		System.err.println("from " + fromDate + " To " + toDate + "dept_id : " + dept_id);

		String sql = "select dfd.dfd_id,dfd.dfd_name,(select dept_name from Department where"
				+ " dept_id=dfd.dfd_dept_mid),(select sub_dept_name from SubDepartment where"
				+ " sub_dept_id=dfd.dfd_sub_dept_mid),dfs.dfs_cr_date from DocumentFileDetails dfd,DocumentFileStage dfs where dfd.dfd_id=dfs.dfs_dfd_mid and dfs.dfs_stage_lid=15 and "
				+ " to_date(to_char(dfs.dfs_cr_date, 'yyyy-MM-dd'),'yyyy-MM-dd') between to_date('" + fromDate
				+ "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd')";

		if (dept_id != null) {
			sql += " and dfd.dfd_dept_mid=" + dept_id;
			System.out.println("MetaDataReport:" + sql);
			result = em.createQuery(sql).getResultList();
		}
		/*
		 * else { sql+= " group by d.dept_name";
		 * System.out.println("All :"+sql); result =
		 * em.createQuery(sql).getResultList(); }
		 */
		return result;
	}

	public List<Object> getQCDataData(String dept_id, String fromDate, String toDate) {

		List<Object> result = new ArrayList<Object>();
		result = null;
		System.err.println("from " + fromDate + " To " + toDate + "dept_id : " + dept_id);

		String sql = "select dfd.dfd_id,dfd.dfd_name,(select dept_name from Department where"
				+ " dept_id=dfd.dfd_dept_mid),(select sub_dept_name from SubDepartment where"
				+ " sub_dept_id=dfd.dfd_sub_dept_mid),dfs.dfs_cr_date from DocumentFileDetails dfd,DocumentFileStage dfs where dfd.dfd_id=dfs.dfs_dfd_mid and dfs.dfs_stage_lid=16 and "
				+ " to_date(to_char(dfs.dfs_cr_date, 'yyyy-MM-dd'),'yyyy-MM-dd') between to_date('" + fromDate
				+ "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd')";

		if (dept_id != null) {
			sql += " and dfd.dfd_dept_mid=" + dept_id;
			System.out.println("MetaDataReport:" + sql);
			result = em.createQuery(sql).getResultList();
		}
		/*
		 * else { sql+= " group by d.dept_name";
		 * System.out.println("All :"+sql); result =
		 * em.createQuery(sql).getResultList(); }
		 */
		return result;
	}

	public Integer getUploadCount(String dept_id, String fromDate) {

		Integer result;
		result = null;

		String sql = "select count(DISTINCT dfd) from DocumentFileDetails dfd,DocumentFileStage dfs where  dfd.dfd_id=dfs.dfs_dfd_mid and dfd.dfd_stage_lid=14 and dfs.dfs_cr_date <='"
				+ fromDate + "'";

		if (dept_id != null) {
			sql += " and dfd.dfd_dept_mid=" + dept_id;
			System.out.println("MetaDataReport:" + sql);
			result = Integer.parseInt(em.createQuery(sql).getSingleResult().toString());
		}
		/*
		 * else { sql+= " group by d.dept_name";
		 * System.out.println("All :"+sql); result =
		 * em.createQuery(sql).getResultList(); }
		 */
		return result;
	}

	public Integer getMetaCount(String dept_id, String fromDate) {

		Integer result;
		result = null;

		String sql = "select count(DISTINCT dfd) from DocumentFileDetails dfd,DocumentFileStage dfs where dfd.dfd_id=dfs.dfs_dfd_mid and dfd.dfd_stage_lid=15 and dfs.dfs_cr_date <='"
				+ fromDate + "'";

		if (dept_id != null) {
			sql += " and dfd.dfd_dept_mid=" + dept_id;
			System.out.println("MetaDataReport:" + sql);
			result = Integer.parseInt(em.createQuery(sql).getSingleResult().toString());
		}
		/*
		 * else { sql+= " group by d.dept_name";
		 * System.out.println("All :"+sql); result =
		 * em.createQuery(sql).getResultList(); }
		 */
		return result;
	}

	public Integer getQCCount(String dept_id, String fromDate) {

		Integer result;
		result = null;

		String sql = "select count(DISTINCT dfd) from DocumentFileDetails dfd,DocumentFileStage dfs where  dfd.dfd_id=dfs.dfs_dfd_mid and dfd.dfd_stage_lid=16 and dfs.dfs_cr_date <='"
				+ fromDate + "'";

		if (dept_id != null) {
			sql += " and dfd.dfd_dept_mid=" + dept_id;
			System.out.println("MetaDataReport:" + sql);
			result = Integer.parseInt(em.createQuery(sql).getSingleResult().toString());
		}
		/*
		 * else { sql+= " group by d.dept_name";
		 * System.out.println("All :"+sql); result =
		 * em.createQuery(sql).getResultList(); }
		 */
		System.out.println("MetaDataReport:" + sql);
		return result;
	}

	@SuppressWarnings("unused")
	public Integer getPageCount(String dept_id, String fromDate) {
		Integer result;
		result = null;
		try {
			// and dfd.dfd_stage_lid=16
			String sql = "select sum(dfd.dfd_page_count) from DocumentFileDetails dfd,DocumentFileStage dfs where  dfd.dfd_id=dfs.dfs_dfd_mid  and dfs.dfs_cr_date <='"
					+ fromDate + "'";

			if (dept_id != null) {
				sql += " and dfd.dfd_dept_mid=" + dept_id;
				System.out.println("MetaDataReport:" + sql);
				result = Integer.parseInt(em.createQuery(sql).getSingleResult().toString());
			    if(result == null)
			    {
			    	result =0;
			    }
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = 0;
		} 

		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object> getDepartmentWiseMetaData(String sub_dept_id,String dept_id, String fromDate, String toDate) {

		List<Object> result = new ArrayList<Object>();
		result = null;
		System.err.println("from " + fromDate + " To " + toDate);

		/*String sql="select dept_name , sub_dept_name ,dt_name,if_name,count(md_fd_mid) as metadatacount " 
				+" from document_file_details,meta_data,department,sub_department ,index_fields,document_type"
				+" where to_date(to_char(md_cr_date, 'yyyy-MM-dd'),'yyyy-MM-dd')"
				+" between to_date('" + fromDate + "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd') and "
				+" md_fd_mid=dfd_id and sub_dept_id=dfd_sub_dept_mid and dfd_dept_mid="+dept_id+"  and "
				+" md_mf_mid=if_id and dfd_dt_mid=dt_id"
				+" group by  dept_name ,sub_dept_name,if_name,dt_name"
				+" order by dept_name desc,sub_dept_name,dt_name,if_name asc ";*/
		if(sub_dept_id==null || sub_dept_id=="" )
		{
		String sql="select dfd_file_name,count(md_fd_mid) as a from document_file_details ,meta_data "
				+" where to_date(to_char(md_cr_date, 'yyyy-MM-dd'),'yyyy-MM-dd')"
				+" between to_date('" + fromDate + "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd') and "
				+ " dfd_id=md_fd_mid and dfd_dept_mid="+dept_id+"  "
				+ " GROUP BY dfd_file_name order by a desc";
		
		System.out.println("MetaDataReport:" + sql);
		result = em.createNativeQuery(sql).getResultList();
		return result;
		}else
		{
			String sql1="select dfd_file_name,count(md_fd_mid) as a from document_file_details ,meta_data "
					+" where to_date(to_char(md_cr_date, 'yyyy-MM-dd'),'yyyy-MM-dd')"
					+" between to_date('" + fromDate + "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd') and "
					+ " dfd_id=md_fd_mid and dfd_dept_mid="+dept_id+" and dfd_sub_dept_mid="+sub_dept_id+"  "
					+ " GROUP BY dfd_file_name order by a desc";
			System.out.println("MetaDataReport:" + sql1);
			result = em.createNativeQuery(sql1).getResultList();
			return result;
		}
		
		
	}
	
	
	public List<Object> getDownloadData(String um_id, String fromDate, String toDate) {

		List<Object> result = new ArrayList<Object>();
		result = null;
		System.err.println("from " + fromDate + " To " + toDate + "um_id : " + um_id);

		String sql = "select d.dept_name,sd.sub_dept_name,dt.dt_name,dfd.dfd_file_name,u.username,u.um_fullname,dl.dl_download_time,dl.dl_ip_address from User u,DownloadLog dl, DocumentFileDetails dfd ,"
				+ " Department d, SubDepartment sd, Document_type dt where"
				+ " dl.dl_user_mid=u.um_id and dl.dl_dfd_mid=dfd.dfd_id  and d.dept_id=dfd.dfd_dept_mid and sd.sub_dept_id=dfd.dfd_sub_dept_mid and dt.dt_id=dfd.dfd_dt_mid ";

		if(fromDate!=null && toDate!=null) {
			sql+= " and to_date(to_char(dl.dl_download_time, 'yyyy-MM-dd'),'yyyy-MM-dd') between to_date('" + fromDate
			+ "', 'yyyy-MM-dd') and to_date('" + toDate + "', 'yyyy-MM-dd')";	
		}
		
		if (um_id != null) {
			sql += " and u.um_id=" + um_id + "  order by dl.dl_download_time desc";
			System.out.println("UserWiseLoginReport:" + sql);
			result = em.createQuery(sql).getResultList();
		}
		
		else {
			sql += "  order by dl.dl_download_time desc";
			System.out.println("All :" + sql);
			result = em.createQuery(sql).getResultList();
		}
		return result;
	}
	
	
	
	
}
