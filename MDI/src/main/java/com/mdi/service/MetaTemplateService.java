package com.mdi.service;

import java.util.List;

import com.mdi.dto.DepartmentDetailsDto;
import com.mdi.model.Department;
import com.mdi.model.Document_type;
import com.mdi.model.SubDepartment;

public interface MetaTemplateService 
{
	public void SaveDepartMent(Department department) throws Exception;
	
	public List<DepartmentDetailsDto> getAllDepartMentList() throws Exception;
	
	public void saveSubDepartment(SubDepartment subdept) throws Exception;
	
	public Department getAllDepartMentById(Long id);

	public void saveDocumentType(Document_type doc_type) throws Exception;

	public List<Document_type> getDocumentTypes(Long sub_dept_id);
	

}
