package com.mdi.service;

import java.util.List;

import com.mdi.model.Department;
import com.mdi.model.Folder;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;

public interface FolderService 
{
	public List<Repository> getRepository() throws Exception;
	
	public List<Department> getDepartments() throws Exception;
	
	public void saveFolder(Folder folder) throws Exception;
	
	public List<Folder> getAllFolders() throws Exception;
	
	public Department getDepartmentById(Long id) throws Exception;

	public List<Folder> getFoldersByDeptid(Long dept_id);
	
	public SubDepartment getSubDepartmentById(Long id);
	
	public Folder getFolderById(Long value);

	public List<Folder> getFoldersByParentId(Long folderId);

}
