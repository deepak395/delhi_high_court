package com.mdi.service;

import java.util.List;

import com.mdi.model.User;
import com.mdi.model.UserRole;

public interface UserRoleService {

	UserRole getUserRoleById(long id);
	
	UserRole save(UserRole userRole);
}
