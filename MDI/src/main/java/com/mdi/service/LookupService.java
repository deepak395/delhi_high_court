package com.mdi.service;

import java.util.List;

import com.mdi.model.Department;
import com.mdi.model.Lookup;
import com.mdi.model.User;

public interface LookupService 
{
	
	public List<Lookup> getAll(String setname) ;
	public List<Department> getAllDepartment() ;
	public List<Lookup> CheckRegex(String setname);
	public List<Lookup> getRoleList(String rolename);
	
	public Lookup getLookUpObject(String setname);
}
