package com.mdi.service;

import java.util.List;

import com.mdi.model.Folder;
import com.mdi.model.Permission;
import com.mdi.model.Repository;

public interface FolderPermissionService 
{
	public List<Folder> getNotAssignedFolders(Long userId) throws Exception;

	public Permission checkFolderexist(Folder f, Long userId)throws Exception;

	public Permission save(Permission perm)throws Exception;

	public List<Repository> getNotAssignedRepositories(Long userId) throws Exception;

	public Permission checkRepositoryexist(Repository r, Long userId) throws Exception;

	public List<Permission> getPermissionByUser(Long userId);

	public Repository getRepository(Long value);

	public Folder getFolderById(Long value);
	
	public Permission checkPermissionexist(Long rep_id, Long userId, int i);

	public void updateChildFolders(List<Long> folderids, Long userId, Integer status);

}
