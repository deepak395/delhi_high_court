package com.mdi.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Department;
import com.mdi.model.Document_type;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;

@Service
public class UploadDocumentService 
{
	
	//public List<Department> getDepartments() throws Exception;
	
	/*public Repository saveFolder(Folder folder) throws Exception;
	
	public List<Folder> getAllFolders() throws Exception;
	
	public Department getDepartmentById(Long id) throws Exception;*/


		@PersistenceContext 
		private EntityManager em;
		
		public Department getDepartment(Long dept_id){
			
			Department list = new Department();
			try 
			{
				Query query = em.createQuery("Select d from Department d where status=1 and (d.dept_id=:dept_id) ").setParameter("dept_id", dept_id);;
				list= (Department)query.getSingleResult();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		public SubDepartment getsubdeptById(Long sub_dept_id){
			
			SubDepartment list = new SubDepartment();
			try 
			{
				Query query = em.createQuery("SELECT sd from SubDepartment sd Where (sd.sub_dept_id=:sub_dept_id) ").setParameter("sub_dept_id", sub_dept_id);;
				list= (SubDepartment)query.getSingleResult();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		
			//Validation to avoid uploading of documents wihtout Index Field Creation
			public IndexField getIfByDtId(Long if_parent){
			
				IndexField list = new IndexField();
			try 
			{
				Query query = em.createQuery("SELECT ifs from IndexField ifs Where (ifs.if_parent=:if_parent) and ifs.if_add_multiple=1").setParameter("if_parent", if_parent);
				list= (IndexField)query.getSingleResult(); 
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		
		
		
		public List<SubDepartment> getSubDept(Long dept_id){
			List<SubDepartment> list = new ArrayList<SubDepartment>();
			try 
			{
				Query query=em.createQuery("SELECT sd from SubDepartment sd Where (PARENT_ID=:id)");
				query.setParameter("id",dept_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		
		public List<IndexField> getindexfields(Long sub_dept_id){
			List<IndexField> list = new ArrayList<IndexField>();
			try 
			{
				Query query=em.createQuery("SELECT ifd from IndexField ifd Where (if_parent=:id) order by if_sequence");
				query.setParameter("id",sub_dept_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		public List<Lookup> getFieldDataService(Long if_id){
			List<Lookup> list = new ArrayList<Lookup>();
			try 
			{
				Query query=em.createQuery("SELECT lk from Lookup lk Where (lk_parent=:id)");
				query.setParameter("id",if_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		/*public List<Department> getAllDepartMentList() throws Exception
		{
			List<Department> list = new ArrayList<Department>();
			try 
			{
				Query query = em.createQuery("Select d from Department d where status=1 ");
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				throw new Exception("Dao_layer",e);
			}
			
		}
		*/
		/*@Transactional
		public List<MetaField> getAll() {
			List<MetaField> result = new ArrayList<MetaField>() ;
			try{
				result = em.createQuery("SELECT m FROM MetaField m where mf_rec_status =1 ORDER BY m.mf_sequence ").getResultList();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}
		
		@Transactional
		public List<Object> getDropDownList(String query) {
			List<Object> result = new ArrayList<Object>() ;
			try{
				result = em.createQuery(query).getResultList();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}	
		
		@Transactional
		public List<MetaData> getAllByfdid(Long fd_id) {
			List<MetaData> result = new ArrayList<MetaData>() ;
			try{
				result = em.createQuery("SELECT md FROM MetaData md_rec_status =1 AND md where md_fd_mid = "+fd_id).getResultList();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}
		@Transactional
		public MetaData getByPk(Long md_id) {
			MetaData result = new MetaData() ;
			try{
				result = (MetaData) em.createQuery("SELECT md FROM MetaData md where md_id = "+md_id).getSingleResult();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}
		
		@Transactional
		public List<MetaData> getAllData(Long md_fd_mid) {
			List<MetaData> result = new ArrayList<MetaData>() ;
			try{
				Query query = em
						.createQuery("SELECT m FROM MetaData m WHERE md_rec_status =1 AND md_fd_mid=:md_fd_mid");
				query.setParameter("md_fd_mid", md_fd_mid);			
				result = (List<MetaData>) query.getResultList();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}
		
		@Transactional
		public List<MetaData> getByfd_mfid(Long md_fd_mid,Long md_mf_mid) {
			List<MetaData> result = new ArrayList<MetaData>() ;
			try{
				Query query = em
						.createQuery("SELECT m FROM MetaData m WHERE md_rec_status =1 AND md_fd_mid=:md_fd_mid and md_mf_mid=:md_mf_mid");
				query.setParameter("md_fd_mid", md_fd_mid).setParameter("md_mf_mid", md_mf_mid);			
				result = (List<MetaData>) query.getResultList();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}
		
		@Transactional
		public MetaData save(MetaData metaData) {
			MetaData result = new MetaData();		
			try {
				result = em.merge(metaData);
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return result;
		}
		
		
		@Transactional
		public String getById(MetaData mdl) {
			
			Long[] list1= {11L, 25L, 31L};
	    	Long[] list2= {21L, 27L,10L,17L,32L,26L,20L,1L};
	    	Long[] list3= {19L};
	    	Long[] list4= {5L};
	    	String name=mdl.getMd_value();    	
	    	System.out.println(Arrays.asList(list1).contains(mdl.getMd_mf_mid()));
	    	System.out.println(Arrays.binarySearch(list2, mdl.getMd_mf_mid()));
	    	System.out.println(mdl.getMd_mf_mid());
	    	if(Arrays.asList(list1).contains(mdl.getMd_mf_mid())){ 
	    		Long id = Long.parseLong(mdl.getMd_value(), 10);
	    		Judge r2 = em.find(Judge.class, id);
	    		name = r2.getJg_name();
	    	}else if(Arrays.asList(list2).contains(mdl.getMd_mf_mid())){
	    		Long id = Long.parseLong(mdl.getMd_value(), 10);
	    		Lookup r2 = em.find(Lookup.class, id);
	    		name = r2.getLk_longname();
	    	}else if(Arrays.asList(list3).contains(mdl.getMd_mf_mid())){
	    		Long id = Long.parseLong(mdl.getMd_value(), 10);
	    		PoliceStation r2 = em.find(PoliceStation.class, id);
	    		name = r2.getPs_name();
	    	}else if(Arrays.asList(list4).contains(mdl.getMd_mf_mid())){
	    		Long id = Long.parseLong(mdl.getMd_value(), 10);
	    		CategoryCode r2 = em.find(CategoryCode.class, id);
	    		name = r2.getCc_code();
	    	}
			return name;
		}
		
		@Transactional
		public void deleteByPk(Long id) {
			MetaData r2 = em.find(MetaData.class, id);
			em.remove(r2);
		}
		
		@Transactional
		public Integer getByMaxSequence(Long md_fd_mid, Long md_mf_mid) {
			// TODO Auto-generated method stub
			Integer sequence=2;
			try{
				Query query = em
						.createQuery("select max(md_rec_status) from MetaData where md_fd_mid=:md_fd_mid and md_mf_mid=:md_mf_mid");
				query.setParameter("md_fd_mid", md_fd_mid).setParameter("md_mf_mid", md_mf_mid);			
				sequence = (Integer) query.getSingleResult();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return sequence;
		}
*/
		public Document_type getDocumentTypeById(long doc_type_id) 
		{
			Document_type doc_type = null;
			try 
			{
				doc_type = em.find(Document_type.class, doc_type_id);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return doc_type;
		}
		
		public Folder getFolderById(Long fd_id) {
			Folder folder = null;
			try 
			{
				folder = em.find(Folder.class, fd_id);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return folder;
		}


}
