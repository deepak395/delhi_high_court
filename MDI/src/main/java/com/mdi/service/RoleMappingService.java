package com.mdi.service;

import com.mdi.model.RoleObject;

public interface RoleMappingService {

	RoleObject getByRoleAndObject(Long role_id, Long ro_om_mid);
	
	RoleObject save(RoleObject roleObject);
}
