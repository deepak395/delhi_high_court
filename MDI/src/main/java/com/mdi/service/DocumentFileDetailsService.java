package com.mdi.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.Document_type;
import com.mdi.model.DownloadLog;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.model.ViewLog;

@Service
public class DocumentFileDetailsService 
{
	
	//public List<Department> getDepartments() throws Exception;
	
	/*public Repository saveFolder(Folder folder) throws Exception;
	
	public List<Folder> getAllFolders() throws Exception;
	
	public Department getDepartmentById(Long id) throws Exception;*/


		@PersistenceContext 
		private EntityManager em;
		
		@Transactional
		public DocumentFileDetails  save(DocumentFileDetails s) {

			DocumentFileDetails master = null;
			try {
				master = em.merge(s);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return master;
		}
		@Transactional
		public ViewLog  saveViewLog(ViewLog s) {

			ViewLog master = null;
			try {
				master = em.merge(s);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return master;
		}
		
		@Transactional
		public DownloadLog  saveDownloadLog(DownloadLog d) {
			DownloadLog master = null;
			try {
				master = em.merge(d);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return master;
		}
		
		
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional
		public List<DocumentFileDetails>  getFilesBySubDeptAndYear(String dfd_dept_mid,String dfd_sub_dept_mid,String dfd_year,String dt_id,String fd_id) {
			List<DocumentFileDetails> list = new ArrayList<DocumentFileDetails>();
			
			try 
			{
				String sql = "Select dfd from DocumentFileDetails dfd where dfd.dfd_rec_status=1";
			    
				if(dfd_dept_mid!=null){
					sql=sql+ " and dfd.dfd_dept_mid="+dfd_dept_mid;
				}
				if(dfd_sub_dept_mid!=null){
					sql=sql+ " and dfd.dfd_sub_dept_mid="+dfd_sub_dept_mid;
				}
				if(dfd_year!=null){
					sql=sql+ " and dfd.dfd_year="+dfd_year;
				}
				if(dt_id!=null){
					sql=sql+ " and dfd.dfd_dt_mid="+dt_id;
				}
				if(fd_id!=null)
				{
					sql=sql+ " and dfd.dfd_fd_mid="+fd_id;
				}
				
				sql=sql+ " order by dfd.dfd_id";
				list=(List<DocumentFileDetails>)em.createQuery(sql).getResultList();
				System.out.println("Upload Recent"+list.toString());
				return list;
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		
		public List<Department> getDepartments(){
			List<Department> list = new ArrayList<Department>();
			try 
			{
				Query query = em.createQuery("Select d from Department d where status=1 ");
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		public List<SubDepartment> getSubDept(Long dept_id){
			List<SubDepartment> list = new ArrayList<SubDepartment>();
			try 
			{
				Query query=em.createQuery("SELECT sd from SubDepartment sd Where (PARENT_ID=:id)");
				query.setParameter("id",dept_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		
		public List<IndexField> getindexfields(Long sub_dept_id){
			List<IndexField> list = new ArrayList<IndexField>();
			try 
			{
				Query query=em.createQuery("SELECT ifd from IndexField ifd Where (if_parent=:id) order by if_sequence");
				query.setParameter("id",sub_dept_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		public List<Lookup> getFieldDataService(Long if_id){
			List<Lookup> list = new ArrayList<Lookup>();
			try 
			{
				Query query=em.createQuery("SELECT lk from Lookup lk Where (lk_parent=:id)");
				query.setParameter("id",if_id);
				list= query.getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		

		@Transactional
		public List<DocumentFileDetails>  getFilesByUserWise(User user) {
			List<DocumentFileDetails> list = new ArrayList<DocumentFileDetails>();
			try 
			{
				String sql="Select dfd from DocumentFileDetails dfd where dfd.dfd_rec_status=1 and dfd.dfd_assign_to="+user.getUm_id();
				list= (List<DocumentFileDetails>) em.createQuery(sql).setMaxResults(500).getResultList();
				return list;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return list;
		}
		

		/**
		 * @author Rup
		 * new method for searching
		 * 
		 */
		@SuppressWarnings("unchecked")
		public List<DocumentFileDetails> getFileByDeptAndSubDeptAndDocTypeUserWiseData(DocumentFileDetails documentFileDetails) {
			List<DocumentFileDetails> list = new ArrayList<DocumentFileDetails>();
			DocumentFileDetails documentFileDetail=null;
			Department department=null;
			SubDepartment subDepartment=null;
			Document_type document_type=null;
			try {
				//String sql="FROM DocumentFileDetails WHERE dfd_dept_mid="+documentFileDetails.getDfd_dept_mid()+" AND dfd_sub_dept_mid ="+documentFileDetails.getDfd_sub_dept_mid()+" AND dfd_dt_mid="+documentFileDetails.getDfd_dt_mid()+" AND dfd_assign_to="+documentFileDetails.getDfd_assign_to()+" AND dfd_rec_status=1";
				//String sql="Select * from mdi.document_file_details  where dfd_rec_status=1 and dfd_assign_to='3' and dfd_dept_mid='8' and dfd_sub_dept_mid='22' and dfd_dt_mid= 88 and dfd_fd_mid=79";
				String sql="Select f.dfd_id,f.dfd_name,d.dept_name,sd.sub_dept_name,t.dt_name,f.dfd_year from document_file_details f"
							+" join department d on f.dfd_dept_mid=d.dept_id"
							+" join mdi.sub_department sd on f.dfd_sub_dept_mid=sd.sub_dept_id"
							+" join document_type t on f.dfd_dt_mid=t.dt_id"
							+" where f.dfd_rec_status=1 and f.dfd_assign_to="+documentFileDetails.getDfd_assign_to()
							+" and f.dfd_dept_mid="+documentFileDetails.getDfd_dept_mid()
							+" and f.dfd_sub_dept_mid="+documentFileDetails.getDfd_sub_dept_mid()
							+" and f.dfd_dt_mid="+documentFileDetails.getDfd_dt_mid();
				
				if(documentFileDetails.getDfd_fd_mid()!=null){
					sql=sql+" and f.dfd_fd_mid="+documentFileDetails.getDfd_fd_mid();
				}
				if(documentFileDetails.getDfd_year()!=null){
					sql=sql+" and f.dfd_year="+documentFileDetails.getDfd_year();
				}
				List<Object[]> objlist= em.createNativeQuery(sql).getResultList();
				for (Object[] objects : objlist) {
					documentFileDetail=new DocumentFileDetails();
					department=new Department();
					subDepartment=new SubDepartment();
					document_type=new Document_type();
					if(objects[0]!=null){
						documentFileDetail.setDfd_id(Long.parseLong(objects[0].toString()));
					}
					
					if(objects[1]!=null){
						documentFileDetail.setDfd_name(objects[1].toString());
					}
					
					if(objects[2]!=null){
						department.setDept_name(objects[2].toString());
						documentFileDetail.setDepartments(department);
					}
					
					if(objects[3]!=null){
						subDepartment.setSub_dept_name(objects[3].toString());	
						documentFileDetail.setSubdepartments(subDepartment);
					}
				
					if(objects[4]!=null){
						document_type.setDt_name(objects[4].toString());
						documentFileDetail.setDoctype(document_type);
					}
					
					if(objects[5]!=null){
						documentFileDetail.setDfd_year(Long.parseLong(objects[5].toString()));
					}
					list.add(documentFileDetail);
				}
				System.out.println("list"+list);
				return list;
			} 
			catch (EntityNotFoundException e) {
				 e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
				System.out.println("query Exception"+e.getMessage());
			}
			return list;
		}
		
		
		@Transactional
		public Long getNewUserList(String rolename,
				Long stage) {
			// TODO Auto-generated method stub
			Long result = 0L;

			Query query = em
					.createQuery("select u.um_id from User u where "
							+ " u.rec_status=1 "
							+ "	and u.um_id In(select ur.ur_um_mid from UserRole ur where ur.ur_role_id in (select l.lk_id from Lookup l	"
							+ "	where l.lk_longname='"
							+ rolename
							+ "')) and not exists(select dfd_id from DocumentFileDetails where dfd_stage_lid="
							+ stage + " and dfd_assign_to=u.um_id)");

			System.out.println("query2=" + query);
			try {
				result = (Long) query.setMaxResults(1).getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result;
		}
		
		@Transactional
		public List<Object> getUserList(String rolename,
				Long stage) {
			// TODO Auto-generated method stub
			List<Object> result1 = new ArrayList<Object>();
			Query query = em
					.createNativeQuery("select count(*),d.dfd_assign_to from (select count(*),c.dfd_assign_to from document_file_details c where c.dfd_stage_lid="
							+ stage
							+"and c.dfd_assign_to in(select u.um_id from user_master u where u.um_avilable_status='Y' and "
							+" u.um_rec_status=1 and u.um_id In(select ur.ur_um_mid from user_role ur where ur.ur_role_id =(select l.lk_id from lookup l"
							+ " where l.lk_longname='"
							+ rolename
							+ "'))) group by  c.dfd_assign_to) d group by d.dfd_assign_to");

			System.out.println("query2=" + query);
			try {
				result1 = query.getResultList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result1;
		}
		
		@Transactional
		public List  getRecentData(Long user,
				Long stage) {
			// TODO Auto-generated method stub
			List<Object> result1 = new ArrayList<Object>();
			String sql="Select dfd.dfd_name,dfs.dfs_cr_date,(select dept_name from Department where dept_id=dfd.dfd_dept_mid),"
					+ " (select sub_dept_name from SubDepartment where sub_dept_id=dfd.dfd_sub_dept_mid) from DocumentFileDetails dfd,"
					+ "DocumentFileStage dfs where dfd.dfd_id=dfs.dfs_dfd_mid"
					+ " and dfs.dfs_cr_by="+user+" and dfs.dfs_stage_lid="+stage+" ORDER BY dfs.dfs_cr_date DESC";
			try {
				result1 =(List<Object>)em.createQuery(sql).setMaxResults(10).getResultList();
				System.out.println("Upload Recent"+result1.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result1;
		}
		
		@Transactional
		public DocumentFileDetails  getById(String dfd_id) {
			// TODO Auto-generated method stub
			DocumentFileDetails result1 = new DocumentFileDetails();
			
			try {
				String sql="SELECT dfd from DocumentFileDetails dfd Where dfd.dfd_id="+dfd_id;
				//Query query=em.createQuery("SELECT dfd from DocumentFileDetails dfd Where (dfd.dfd_id=:id)");
				//query.setParameter("id",dfd_id);
				result1= (DocumentFileDetails) em.createQuery(sql).getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result1;
		}
		
		@SuppressWarnings("unchecked")
		@Transactional
		public List<DocumentFileDetails>  getDataByDeptName(String dept_name) {
			
			List<DocumentFileDetails> result1 = new ArrayList<DocumentFileDetails>();
			try {
				String sql="SELECT dfd from DocumentFileDetails dfd Where dfd.dfd_dept_mid="
						+ "(select d.dept_id from Department d where d.dept_name='"+dept_name+"')";
				result1= (List<DocumentFileDetails>) em.createQuery(sql).getResultList();
			} catch (Exception e) {
				
				e.printStackTrace();
			}

			return result1;
		}

		public List<Object> getRecentDataAdmin(Long um_id, long l,Long dept_id) 
		{
			List<Object> result1 = new ArrayList<Object>();
			String sql="Select dfd.dfd_name,dfs.dfs_cr_date,(select dept_name from Department where dept_id=dfd.dfd_dept_mid),"
					+ " (select sub_dept_name from SubDepartment where sub_dept_id=dfd.dfd_sub_dept_mid) from DocumentFileDetails dfd,"
					+ "DocumentFileStage dfs where dfd.dfd_id=dfs.dfs_dfd_mid"
					+ " and dfd.dfd_dept_mid="+dept_id+" and dfs.dfs_stage_lid="+l+" ORDER BY dfs.dfs_cr_date DESC";
			try {
				result1 =(List<Object>)em.createQuery(sql).setMaxResults(10).getResultList();
				System.out.println("Upload Recent"+result1.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result1;
		}
		public List<DocumentFileDetails> getMostViewedFiles(Long um_department_id) {
			List<DocumentFileDetails> dfd = new ArrayList<>();
			try 
			{
				String sql = "select dfd from   DocumentFileDetails dfd where dfd.dfd_id in "+
						 "( select  vl.vl_dfd_mid from ViewLog vl group by vl.vl_dfd_mid order by count(vl.vl_dfd_mid) desc) and dfd.dfd_dept_mid="+um_department_id;
				
				Query query = em.createQuery(sql);
				
				dfd =query.setMaxResults(5).getResultList();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return dfd;
		}
		public List<DocumentFileDetails> getMostViewedSysAdmFiles(Long um_department_id) {
			List<DocumentFileDetails> dfd = new ArrayList<>();
			try 
			{
				String sql = "select dfd from   DocumentFileDetails dfd where dfd.dfd_id in "+
						 "( select  vl.vl_dfd_mid from ViewLog vl group by vl.vl_dfd_mid order by count(vl.vl_dfd_mid) desc)";
				
				Query query = em.createQuery(sql);
				
				dfd =query.setMaxResults(5).getResultList();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return dfd;
		}
		public Long getViewdfileCount( Long dfd_id) 
		{
			Long result = null;
			try 
			{
				String sql ="select count(DISTINCT vl.vl_dfd_mid) from ViewLog vl  where vl_dfd_mid in (select dfd_id from DocumentFileDetails dfd  where dfd_dept_mid ="+dfd_id+")";
				Query query = em.createQuery(sql);
				 result = (Long) query.getSingleResult();
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			return result;
		}
		
		public Long getViewdfileCountSysAdmin() {

			Long result = null;
			try 
			{
				Query query = em.createQuery("select count(DISTINCT vl.vl_dfd_mid) from ViewLog vl  where vl_dfd_mid in (select dfd_id from DocumentFileDetails dfd)");
				 result = (Long) query.getSingleResult();
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			return result;
		}
		/*public List<Object> getRecentDataSysAdmin() 
		{
			System.out.println("durgendra");
			List<Object> result1 = new ArrayList<Object>();
			String sql="Select dfd.dfd_name,dfd.dfd_cr_date,(select dept_name from Department where dept_id=dfd.dfd_dept_mid),"
					+ " (select sub_dept_name from SubDepartment where sub_dept_id=dfd.dfd_sub_dept_mid) from DocumentFileDetails dfd"
					
					+ "  ORDER BY dfd.dfd_cr_date DESC";
			try {
				result1 =(List<Object>)em.createQuery(sql).setMaxResults(10).getResultList();
				System.out.println("Upload Recent"+result1.toString());
			} catch (Exception e) {
				
				e.printStackTrace();
			}

			return result1;
		}*/
		public List<Object> getRecentDataSysAdmin() 
		{
			System.out.println("Virendra");
			List<Object> result1 = new ArrayList<Object>();
			String sql="select (select sub_dept_name from SubDepartment where sub_dept_id =dfd_sub_dept_mid ),"
					+ "(select dept_name from Department where dept_id=dfd_dept_mid),count(dfd) from DocumentFileDetails dfd"
					+ " group by dfd_sub_dept_mid,dfd_dept_mid"
                    +"  ORDER BY count(dfd) DESC";
			try {
				result1 =(List<Object>)em.createQuery(sql).setMaxResults(10).getResultList();
				System.out.println("Upload Recent"+result1.toString());
			} catch (Exception e) {
				
				e.printStackTrace();
			}

			return result1;
		}
		public List<Object> getUploadDataAdmin(Long um_id, long l,Long dept_id) 
		{
			System.out.println("Virendra");
			List<Object> result1 = new ArrayList<Object>();
			
			String sql2="select s.sub_dept_name,d.dept_name,count(dfd_id) "
					+ "from SubDepartment s,Department d ,DocumentFileDetails dfd  "
					+ "where parent_id =dept_id and sub_dept_id=dfd_sub_dept_mid "
					+ "and dept_id="+dept_id+"group by dept_name,sub_dept_name ";
			
			String sql="select (select sub_dept_name from SubDepartment where sub_dept_id =dfd_sub_dept_mid ),"
					+ "(select dept_name from Department where dept_id="+dept_id+"),count(dfd) from DocumentFileDetails dfd"
					+ " where dfd.dfd_dept_mid="+dept_id+" "
					+ " group by dfd_sub_dept_mid,dfd_dt_mid"
                    +"  ORDER BY count(dfd) DESC";
			try {
				result1 =(List<Object>)em.createQuery(sql2).setMaxResults(10).getResultList();
				System.out.println("Upload Recent"+result1.toString());
			} catch (Exception e) {
				
				e.printStackTrace();
			}

			return result1;
		}
		
	
}
