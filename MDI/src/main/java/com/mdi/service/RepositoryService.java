package com.mdi.service;

import com.mdi.model.Repository;

public interface RepositoryService {

	public Repository getRepository(Long id);
	//public Repository getRepositoryById(Long id);
}
