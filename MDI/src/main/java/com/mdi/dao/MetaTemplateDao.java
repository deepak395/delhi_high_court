package com.mdi.dao;

import java.util.List;

import com.mdi.model.Department;
import com.mdi.model.SubDepartment;

public interface MetaTemplateDao 
{
	public void SaveDepartMent(Department department) throws Exception;
	
	public List<Department> getAllDepartMentList() throws Exception;

	public void saveSubDepartment(SubDepartment subdept) throws Exception;

}
