package com.mdi.dao;

import java.util.List;

import com.mdi.model.Folder;
import com.mdi.model.Permission;
import com.mdi.model.Repository;

public interface FolderPermissionDao 
{
	public List<Folder> getNotAssignedFolders(Long userId) throws Exception;

	public Permission checkFolderexist(Folder f, Long userId) throws Exception;

	public Permission save(Permission perm) throws Exception;

	public List<Repository> getNotAssignedRepositories(Long userId);

	public Permission checkRepositoryexist(Repository r, Long userId) throws Exception;
}
