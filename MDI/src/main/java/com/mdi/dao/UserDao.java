package com.mdi.dao;

import com.mdi.model.LoginLog;
import com.mdi.model.User;

public interface UserDao 
{
	public User validateLogin(User user) throws Exception;
	public User forgetPassword (String user) throws Exception;
	
	public void saveLoginLog(LoginLog log,User user);
	public User getUserById(Long id) throws Exception;
	public User getUserByName(String username) throws Exception;
}
