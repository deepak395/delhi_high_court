package com.mdi.dao;

import java.util.List;

import com.mdi.model.Department;
import com.mdi.model.Folder;
import com.mdi.model.Repository;

public interface FolderDao 
{
	public List<Repository> getRepository() throws Exception;
	
	public Repository getRepositoryById(Long id) throws Exception;
	
	public void saveFolder(Folder folder) throws Exception;

	public List<Folder> getAllFolders() throws Exception;
	
	public Department getDepartmentById(Long id) throws Exception ;

}
