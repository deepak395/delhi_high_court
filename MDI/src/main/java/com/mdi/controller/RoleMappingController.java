package com.mdi.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.ActionResponse;
import com.mdi.model.RoleObject;
import com.mdi.model.User;
import com.mdi.service.RoleMappingService;


@Controller
public class RoleMappingController {


	@Autowired
	RoleMappingService mappingService;
	
	@RequestMapping(value="roleMapping/create",method = RequestMethod.POST)
	public @ResponseBody String create(@RequestBody List<RoleObject> roleObject, HttpSession session){
		RoleObject robject = new RoleObject();
		ActionResponse<RoleObject> response = new ActionResponse();
		User u = (User) session.getAttribute("USER");
		Long role_id = roleObject.get(0).getRo_role_id();
		
		System.out.println("Called RoleObject/create");
		String jsonData = null;
		
		for (RoleObject rob : roleObject) {
			RoleObject roleobject = mappingService.getByRoleAndObject(role_id,rob.getRo_om_mid());
			if (roleobject.getRo_id() == null) {
				if (rob.getCheckbox() == true) {
					rob.setRo_rec_status(1);
					rob.setRo_cr_by(u.getUm_id());
					rob.setRo_cr_date(new Date());
					mappingService.save(rob);
				}
			} else {
				if (rob.getCheckbox() == true) {
					roleobject.setRo_mod_by(u.getUm_id());
					roleobject.setRo_mod_date(new Date());
					roleobject.setRo_rec_status(1);
					mappingService.save(roleobject);
				} else if (rob.getCheckbox() == false) {
					roleobject.setRo_mod_by(u.getUm_id());
					roleobject.setRo_mod_date(new Date());
					roleobject.setRo_rec_status(2);
					mappingService.save(roleobject);

				}
			}
		}
		return "";
	}
}
