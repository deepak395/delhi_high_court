package com.mdi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.itextpdf.text.pdf.PdfReader;
import com.mdi.dto.Index_Meta_Data;
import com.mdi.model.Document;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.MetaData;
import com.mdi.model.Repository;
import com.mdi.model.User;
import com.mdi.model.ViewLog;
import com.mdi.service.DocumentService;
import com.mdi.service.MetaDataService;
import com.mdi.service.RepositoryService;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;


@Controller
public class DocumentController {

	@Autowired
	private DocumentService  documentService;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private MetaDataService service;
	
	@Autowired
	private UserService userservice;
	
	
	CommenMethods cm;
	public DocumentController() {
		cm = new CommenMethods();
	}
	
	@RequestMapping(value="/document",method = RequestMethod.GET)
	public String documents(){
		
		return "views/document/document";
	}
	

	@RequestMapping(value="/test",method = RequestMethod.GET)
	public String test(){
		
		return "views/document/Test";
	}
	
	@RequestMapping(value = "/document/search")
	public String advancedsearch(Model model) {
	
		return "views/document/search";
	}
	@RequestMapping(value = "/document/File_Meta")
	public String File_Meta(Model model , @RequestParam("id") Long id) {
		model.addAttribute("doc_id", id);
	
		return "views/document/File_Meta";
	}
	
	@RequestMapping(value = "/document/basicsearch")
	public String basicsearch(Model model) {
	
		return "views/document/SearchDocument";
	}
	
	@RequestMapping(value="/document/searchByMetaFields",method = RequestMethod.GET)
	public String testContentSearch()
	{
		return "views/document/searchByMetaField";
		
	}
	
	@RequestMapping("/document/openFile")
	public String openFile()
	{
		return "views/document/openFile";
	}
	
	@RequestMapping("/document/open")
	public String openViewFile(@RequestParam("dfd_id") Long dfd_id,Model model)
	{
		DocumentFileDetails dfd = service.getDocumentFileDetailsById(dfd_id);
		String file_url ="/mdi/uploads/"+dfd.getDfd_file_name();
		
		List<IndexField> repo= new ArrayList<IndexField>();
		List<Index_Meta_Data> in_md = new ArrayList<>();
		
		repo=service.getindexfields(dfd.getDfd_dt_mid());
		Comparator<IndexField> com = new Comparator<IndexField>() 
		{

			@Override
			public int compare(IndexField o1, IndexField o2) {
				
				return o1.getIf_name().compareTo(o2.getIf_name());
			}
			
		};
		Collections.sort(repo,com);
		
		for(IndexField in : repo)
		{
			MetaData meta = service.getMetaData(dfd_id, in.getIf_id());
			Index_Meta_Data inm = new Index_Meta_Data();
			
			inm.setIf_name(in.getIf_name());
			try 
			{
				if(meta.getMd_value()!=null)
				{
					System.out.println(meta.getMd_value());
				}
				inm.setMeta_value(meta.getMd_value());
			}
			catch (Exception e) 
			{
				
			}
			
			in_md.add(inm);
		}
		
		System.out.println(in_md.size());
		model.addAttribute("metadataValue", in_md);
		model.addAttribute("file_url", file_url);
		model.addAttribute("document", dfd);
		
		return "views/document/openFile";
	}
	

	
	
	@RequestMapping(value = "/document/create",method = RequestMethod.POST)
    public @ResponseBody String create(MultipartHttpServletRequest request,HttpSession session ){
		
		String jsonData="";
		String path="";
		String parentfolderName="";
		String childfolderName="";
		 
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
	    Date startDate = new Date();;
	    
    	//ActionResponse<Repository> response = new ActionResponse();
    	User user=new User();
		user=(User) session.getAttribute("USER");
		
		Document documenttemp=new Document();
		long folderid=(Long.parseLong(request.getParameter("folder_id")));
		Folder getChildFolderid=documentService.getChildFolderId(folderid);
	
		
		Folder getParentFolderId=documentService.getParentFolderId(getChildFolderid.getParent_id());
		
		
    	 Repository repository=repositoryService.getRepository(getChildFolderid.getRep_id());
    	 
    	 String repBasepath=repository.getBasepath()+File.separator+repository.getName();
    	 parentfolderName =getParentFolderId.getFolder_name()+File.separator+repository.getName();
    	 childfolderName=getChildFolderid.getFolder_name()+File.separator+repository.getName();
    	 
    	
     	
		Iterator<String> itr = request.getFileNames();
		MultipartFile mf=null;
		MultipartFile mf2=null;
		
		while(itr.hasNext())
		{
		  mf =	request.getFile(itr.next());
		  
		  try
		  {
			  
			  String file_name=mf.getOriginalFilename();
			  
			  path=repository.getBasepath()+File.separator+getParentFolderId.getFolder_name()+File.separator+getChildFolderid.getFolder_name()+File.separator+mf.getOriginalFilename();
				 
			  String ext = FilenameUtils.getExtension(path);
			  
			  FileCopyUtils.copy(mf.getBytes(), new FileOutputStream(path));
			  
			  PdfReader readernewFile = new PdfReader(path);
			  int newPageCount = readernewFile.getNumberOfPages();
			  readernewFile.close(); 
			  
			    documenttemp.setDept_id(getChildFolderid.getDept_id());
		 		documenttemp.setRep_id(getChildFolderid.getRep_id());
		     	documenttemp.setFolder_id(getChildFolderid.getId());
		     	documenttemp.setFile_name(file_name);
		     	documenttemp.setCreated(new Date());
				documenttemp.setStatus(1);
				documenttemp.setDoc_reject_status("N");
				documenttemp.setFile_extension(ext);
				documenttemp.setDoc_stage_lid((long) 14);
				documenttemp.setDoc_page_count(newPageCount);
				documenttemp=documentService.save(documenttemp);
		  }
		  catch(Exception ex)
		  {
			  ex.printStackTrace();
		  }
		}
		
    	
		return null;  
	}
	
	@RequestMapping("search/search")
	public String Serarch(HttpSession session)
	{
		//User user = (User)session.getAttribute("USER");
		//Department dept = userservice.getDeptById(user.getUm_department_id());
		System.out.print("Hello");
		return "views/document/SearchDocument";
	}
}
