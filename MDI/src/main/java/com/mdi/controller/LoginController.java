package com.mdi.controller;
import java.net.Inet4Address;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.email.SendEmail;
import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.LoginLog;
import com.mdi.model.ObjectMaster;
import com.mdi.model.User;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;
import com.mdi.utility.GlobalFunction;


@Controller
public class LoginController extends HttpServlet {

	 private Logger logger = Logger.getLogger(LoginController.class);
	
	@Autowired  
	 private MessageSource messageSource; 
	
	@Autowired
	private UserService userService;
	
	@Autowired
	SendEmail sendmail;
	
	CommenMethods cm;

    private GlobalFunction globalfunction;
	
	
	public LoginController() {
		
		cm = new CommenMethods();
		globalfunction = new GlobalFunction();
	}

	@RequestMapping(value = "/login")
	public String srologin(Model model) 
	{	
		User user = new User();
		model.addAttribute("user", user);
		return "views/user/loginpage";
	}


	@RequestMapping(value = "/userlogin", method = RequestMethod.POST)
	public  String userlogin(@ModelAttribute("user") User user, HttpSession session,HttpServletRequest request,Model model) throws ParseException {
		
		
		if(user.getUsername().equals("") || user.getUsername().equals(null))
		{
			user.setCaptcha("");
            model.addAttribute("message", "User Id is required");
            return "views/user/loginpage";
		}
		
		if (user.getPassword()==null || user.getPassword().equals(""))
	    {
	        user.setCaptcha("");
	        model.addAttribute("message", "Password is required");
	        return "views/user/loginpage";
	    }
		String captcha=(String)session.getAttribute("CAPTCHA");
        if(captcha==null || (captcha!=null && !captcha.equals(user.getCaptcha()))){
            user.setCaptcha("");
            model.addAttribute("message", "Captcha does not match");
            return "views/user/loginpage";
        }
        
        try 
        {
		
        	User u = userService.validateLogin(user);
			LoginLog log=new LoginLog();
			String ip_address= Inet4Address.getLocalHost().getHostAddress();
			Date date= new Date();
			//System.out.println(u.getPassword()+" "+user.getPassword()+" "+u.getUm_login_attemps());
			   
			if(u.getPassword().equals(user.getPassword())  && u.getUm_login_attemps()<5 && u.getUm_parent_id()==1L)
			{
				System.out.println("OTP sent  sucess fully");
				        String result=genearteOTP(user,session,request);
				        if(result!=null) {
				        	model.addAttribute("user", user);
				      model.addAttribute("message","OTP Sent Successfully");
				       return "views/user/otp_validate";
				        }
				        model.addAttribute("user", user);
				        model.addAttribute("message","Error In Sending OTP");
				        return "views/user/otp_validate";
			}
			
			
			
			
				if(u.getPassword().equals(user.getPassword())  && u.getUm_login_attemps()<5)
				{
					System.out.println("Logged in sucess fully");
					u.setUm_login_attemps(0);
					log.setLl_user_mid(u.getUm_id());
					log.setLl_ip_address(ip_address);
					log.setLl_login_time(date);
					userService.saveLoginLog(log,u);
					session.setAttribute("USER", u);
					session.setAttribute("Ram", "Ram");
					
					List<ObjectMaster> ob_list = userService.getUserObjects(u.getUm_id());
					session.setAttribute("ob_list", ob_list);
				
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
					try 
					{
						String strDate= formatter.format(u.getLast_login());
						session.setAttribute("date",strDate);
						SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm:ss");
						String strtime= formatter1.format(u.getLast_login());
						session.setAttribute("Time",strtime);
						System.out.println("MOHAAN"+u.getUm_department_id());
						if(u.getUm_department_id()!=null){
						Department dept = userService.getDeptById(u.getUm_department_id());			
						session.setAttribute("DEPT", dept);
						}
						
					} 
					catch (Exception e) 
					{
					     e.printStackTrace();
				    }
                         
				return "views/user/dashboard";
			 }
			else
			{
				u.setUm_login_attemps(u.getUm_login_attemps()+1);
				log.setLl_user_mid(u.getUm_id());
				log.setLl_ip_address(ip_address);
				log.setLl_login_time(date);
				userService.saveLoginLog(log,u);
				user.setCaptcha("");
				
	            model.addAttribute("message","User ID or Password Incorrect");
				
				return "views/user/loginpage";
			}
		
			
		} 
        catch (Exception e) 
        {
        	e.printStackTrace();
        	user.setCaptcha("");
            model.addAttribute("message","User Doesn't Exist Or Blocked");
            return "views/user/loginpage";

		}

		
	}


	
	
	@RequestMapping(value = "dashboard/landingPage")
	public String index(Model model, HttpSession session) {
		User user = new User();

		return "/views/user/dashboard";
	}
	@RequestMapping(value = "/views/securityQuestion")
	public String index2(Model model, HttpSession session) {
	//	User user = new User();

		return "/views/pdms/securityQuestion";
	}
	
	@RequestMapping(value = "/forgetPassword")
	public String index3(Model model, HttpSession session) {
	//	User user = new User();

		return "views/user/forgetPassword";
	}
	
	
	@RequestMapping(value = "/urlforgetPassword", method = RequestMethod.POST)
	public @ResponseBody String forgetPass(Model model, HttpSession session,HttpServletRequest req) throws Exception {
		
		String result ="";
		User user = new User();
        
		String uname=req.getParameter("userId");
		
	    user= userService.forgetPassword(uname);
	    
	    ActionResponse <User> response = new ActionResponse();
	    
	    String from = "digitization@mdi.ac.in";
        String pass = "mdi@1234";
        String[] to = { user.getUm_email_id()}; // list of recipient email addresses
        String subject = "OTP For Forgot Password";
	    //String password=user.getPassword();
	  //  String password=globalfunction.md5encryption(um.getPassword())
		System.out.println("Uname"+uname);
		try {
				if(user.getUm_email_id()!=null)
				{
					//session.setAttribute("USER", user);
					
				/*	msg = sendmail.sendMailForFirstPassWord(user.getUsername(),uname,user.getPassword());*/
					
				String smstext="Dear User, \n\n"
							
							+ "Your password for DMS software is \n\n"
							+ "USER ID -:"+user.getUsername()+" \n"
							+ "Password -:"+user.getPassword()+" \n\n"
							+" Kindly change the password after first login.\n\n"
							+ "This is system generated mail, please do not reply to this mail"
							+ "\n\n\n Regards,"
							+ "\n MDI  ";
				
					
					String otpresponse=globalfunction.sendEmail(from, pass, to, subject, smstext);
					if(otpresponse.equals("TRUE")) {
					response.setResponse("TRUE");
					user.setIsfirstlogin(true);
					userService.save(user);
					response.setModelData(user);
					}
					
				}
				else{
					response.setResponse("FALSE");
					response.setData("Please Enter correct Username");
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Response"+response);
		result = cm.convert_to_json(response);
		System.out.println("Result"+result);
		
		
		return result;
	}
	
	@RequestMapping(value = "/mdi/logout", method = {RequestMethod.GET,RequestMethod.POST})
	public String logout(HttpSession session) {	 
		/*session.removeAttribute("USER");*/
		
		Date date1=new Date();
		User u = (User) session.getAttribute("USER");
		
		List<LoginLog> loginlog=new ArrayList<LoginLog>();
	
		loginlog = userService.getLoginLog(u.getUm_id());
		
		
		for(LoginLog ll:loginlog )
		{
			ll.setLl_logout_time(date1);			
			userService.saveLog(ll);
		}
		
		
		session.removeAttribute("USER");
		
		
		return "redirect:/";
	}
	
	
	@RequestMapping(value = "user/changePassword")
	public String changePassword(Model model) 
	{	
		return "views/user/passwordPage";
	}
	
	
	@RequestMapping(value = "/genearteOTP", method = RequestMethod.POST)
	@ResponseBody
	public  String genearteOTP(@ModelAttribute("user") User user, HttpSession session,HttpServletRequest req) 
	{
        
	        User u= new User();
	        
			try {
				u= userService.getUserByName(user.getUsername());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
		
		 String from = "digitization@mdi.ac.in";
	        String pass = "mdi@1234";
	        String[] to = { u.getUm_altemail_id()}; // list of recipient email addresses
	        String subject = "MDI Login OTP";
	       // String body = "Hello Ajab .";

	        
	        
	        
		String result = " ";
		try {
			user = userService.validateLogin(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		Date dt= new Date(System.currentTimeMillis());
		user.setUm_otp_intime(dt);
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate= formatter.format(user.getUm_otp_intime());
		SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm:ss");
		String strtime= formatter1.format(user.getUm_otp_intime());
		session.setAttribute("startime",strtime);
		
		System.out.println(session.getAttribute("startime"));
	 Date end_time	=new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5));
	       user.setUm_otp_exptime(end_time);
		   String end_date=formatter.format(user.getUm_otp_exptime());
		   String exp_time=formatter1.format(user.getUm_otp_exptime());
		   session.setAttribute("endtime", exp_time);
		   System.out.println(session.getAttribute("endtime"));
		
		ActionResponse <User> response = new ActionResponse();		
		if(user.getUm_id() != null)
		{
			 Integer otp=globalfunction.generateOTP();
			 user.setUm_otp(otp);
			 //user.setUm_otp_intime(um_otp_intime);
			 user=userService.save(user);
			  response.setResponse("TRUE");
			 //Lookup urlLookup=lookupService.getLookUpObject("SMS_URL");
			 
			// String sms_url=urlLookup.getLk_longname();
			 //String mob_no=user.getUm_mobile();
			 /*String smstext=""+otp+" is the OTP for your login Do not share it with anyone."
			 		+ "This OTP is valid only for 5 minutes";
			 
			 */
				String smstext="Dear User, \n\n"
			             +otp+" is the OTP for DMS Software login.\n"
						 +"Do not share it with anyone.\n"
		 		         +"This OTP is valid only for 5 minutes"
		 		        + "\n\n\n Regards,"
						+ "\n MDI";
			 
			 String otpresponse=globalfunction.sendEmail(from, pass, to, subject, smstext);
			 
			 if(otpresponse.equals("TRUE"))
			 {
				 response.setData("OTP sent successfully");
	
		result = cm.convert_to_json(response);
		//System.out.println(result);
		
		
	}
	
		}
		return result;
	}
	@RequestMapping(value = "/validateOtp", method = RequestMethod.POST)
	public String validateOtp(@ModelAttribute("user") User um,HttpSession session,Model model) throws ParseException
	{
		User user=null;
		
		try {
			user = userService.validateLogin(um);
			String check=null;
			
			Date dt= new Date(System.currentTimeMillis());
			SimpleDateFormat formatter11 = new SimpleDateFormat("dd/MM/yyyy");
			String str= formatter11.format(dt);
			SimpleDateFormat formatter2 = new SimpleDateFormat("hh:mm:ss");
			String strt= formatter2.format(dt);
			String start_time=formatter2.format(user.getUm_otp_intime());
			String end_time=formatter2.format(user.getUm_otp_exptime());
			Integer in_time=strt.compareTo(start_time);
			System.out.println(in_time);
			Integer exp_time=strt.compareTo(end_time);
			System.out.println(exp_time);
				
		
		
		ActionResponse <User> response = new ActionResponse();		
		    if(in_time>0 && exp_time< 0) {
			 if(user.getUm_otp().equals(um.getUm_otp())  )
			 {
				 LoginLog log=new LoginLog();
					String ip_address= Inet4Address.getLocalHost().getHostAddress();
					Date date= new Date();
						System.out.println("Logged in sucess fully");
						user.setUm_login_attemps(0);
						log.setLl_user_mid(user.getUm_id());
						log.setLl_ip_address(ip_address);
						log.setLl_login_time(date);
						userService.saveLoginLog(log,user);
						session.setAttribute("USER", user);
						session.setAttribute("Ram", "Ram");
						
						List<ObjectMaster> ob_list = userService.getUserObjects(user.getUm_id());
						session.setAttribute("ob_list", ob_list);
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
							String strDate= formatter.format(user.getLast_login());
							session.setAttribute("date",strDate);
							SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm:ss");
							String strtime= formatter1.format(user.getLast_login());
							session.setAttribute("Time",strtime);
							System.out.println("MOHAAN"+user.getUm_department_id());
							if(user.getUm_department_id()!=null){
							Department dept = userService.getDeptById(user.getUm_department_id());			
							session.setAttribute("DEPT", dept);
							}
					
			 }else {
				 model.addAttribute("message","Enter Correct OTP");
			       return "views/user/otp_validate";
			 }
			 
		    }
			 else {
				 model.addAttribute("message","OTP Expired");
				   model.addAttribute("user", user);
					return "views/user/loginpage";
			     }
						} 
						catch (Exception e) 
						{
						     e.printStackTrace();
					    }
	                         
			 return "views/user/dashboard";
		
	}
	}

	
	
	
	
	
	
	

