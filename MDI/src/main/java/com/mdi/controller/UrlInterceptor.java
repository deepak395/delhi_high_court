package com.mdi.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.mdi.model.User;

public class UrlInterceptor extends HandlerInterceptorAdapter
{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception 
	{
		HttpSession session = request.getSession();
		String url =request.getRequestURL().toString();
		try 
		{
			String loginurl = url.substring(url.lastIndexOf("/mdi/"));
			User user =(User)session.getAttribute("USER"); 
			if(loginurl.equals("/mdi/login"))
			{
				System.out.println(loginurl+" "+session);
			}
			else if( user==null)
			{
				System.out.println(loginurl+" "+user);
				
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return true;	
	}

}
