package com.mdi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.dto.DepartmentDetailsDto;
import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.Document_type;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.service.MetaTemplateService;
import com.mdi.utility.CommenMethods;

@Controller
public class MetaTemplateController 
{
	@Autowired
	private MetaTemplateService service;
	
	CommenMethods cm;
	
	public MetaTemplateController()
	{
		cm= new CommenMethods();
	}
	
	@RequestMapping("metatemplate/metaTemplateCreation")
	public String metaTemplateCreation()
	{
		return "views/metatemplate/metatemplate";
	}
	
	
	@RequestMapping("metatemplate/createMetaFields")
	public String metaFieldCreation()
	{
		return "views/metatemplate/createMetaField";
		
	}
	
	@RequestMapping("/metatemplate/createMetaTemplate")
	public String createMetaTemplate()
	{
		return "metatemplate/createMetaTemplate";
		
	}
	
	@RequestMapping(value="/metaTemplate/saveDepartment",method=RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String saveMetaTemplate(@RequestBody Department dept,HttpSession session) throws Exception
	{
		ActionResponse<Department> response = new ActionResponse<>();
		
		try 
		{
			User user = (User) session.getAttribute("USER");
			System.out.println(user);
			dept.setDept_cr_by(user.getUm_id());
			Date date = new Date();
			dept.setDept_createdOn(date);
			dept.setStatus(1);
			service.SaveDepartMent(dept);
			response.setResponse("TRUE");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			response.setResponse("FALSE");
			
		}
		
		String result = cm.convert_to_json(response);
		System.out.println(result);
		return result;
	}
	///
	
	@RequestMapping(value="/metaTemplate/getAllDepartmentList",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getAllDepartMentList(HttpSession session) throws Exception
	{
		ActionResponse<DepartmentDetailsDto> response = new ActionResponse<>();
		List<DepartmentDetailsDto> list = new ArrayList<DepartmentDetailsDto>();
		
		try 
		{
			list=service.getAllDepartMentList();
			System.out.println(list.size());
			response.setResponse("TRUE");
			response.setModelList(list);
			System.out.println("sucess");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			response.setResponse("FALSE");
			
		}
		
		String result = cm.convert_to_json(response);
		System.out.println(result);
		return result;
	}
	
	@RequestMapping(value="/metaTemplate/saveSubDepartment",method=RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String saveSubMetaTemplate(@RequestBody SubDepartment dept,HttpSession session) throws Exception
	{
		ActionResponse<Department> response = new ActionResponse<>();
		System.out.println(dept);
		try 
		{
			User user = (User) session.getAttribute("USER");
			System.out.println(user);
			dept.setSub_dept_cr_by(user.getUm_id());
			Date date = new Date();
			dept.setSub_dept_cr_date(date);
			dept.setSub_dept_rec_status(1l);
			service.saveSubDepartment(dept);
			response.setResponse("TRUE");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		
		String result = cm.convert_to_json(response);
		System.out.println(result);
		return result;
	}
	
	
	/*************** Document Type *****************/
	
	@RequestMapping(value="/metaTemplate/saveDocumentType",method=RequestMethod.POST)
	public @ResponseBody String createDocumentType(@RequestBody Document_type doc_type,HttpSession session)
	{
		ActionResponse<Document_type> response = new ActionResponse<>();
		try 
		{
			User user = (User) session.getAttribute("USER");
			Date date = new Date();
			doc_type.setDt_cr_by(user.getUm_id());
			doc_type.setDt_cr_date(date);
			doc_type.setDt_rec_status(1);
			
			service.saveDocumentType(doc_type);
			response.setResponse("TRUE");
			
		} 
		catch (Exception e) 
		{
			response.setResponse("FALSE");
		}
		
		return cm.convert_to_json(response);
		
	}
	
	
	/***************************get Document Type********************************/
	
	@RequestMapping(value="/metatemplate/getDocumentType",method=RequestMethod.GET)
	public @ResponseBody String GetDocumentType(@RequestParam("sub_dept_id")Long sub_dept_id)
	{
		ActionResponse<Document_type> response = new ActionResponse<>();
		try 
		{
			List<Document_type> doc_type = service.getDocumentTypes(sub_dept_id);
			if(!doc_type.isEmpty())
			{
				response.setModelList(doc_type);
				response.setResponse("TRUE");
			}
			else
			{
				response.setResponse("FALSE");
				response.setData("NO Document Type Found");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return cm.convert_to_json(response);
	}

}
