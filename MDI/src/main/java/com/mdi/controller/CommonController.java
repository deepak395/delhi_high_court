package com.mdi.controller;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.Department;
import com.mdi.model.Lookup;
import com.mdi.model.SubDepartment;
import com.mdi.service.LookupServices;
import com.mdi.service.UploadDocumentService;

@Component
public class CommonController 
{
	@Autowired
	private LookupServices lookupService;
	
	@Autowired
	private UploadDocumentService  Upservice;
	
	public String getPathToViewDocument( Long dept_id , Long sub_dept_id , long year, String doc_type)
	{
		String path = "";
		 Department  dept= new Department();
		 SubDepartment sub_dept= new SubDepartment();
		 Lookup lookup=lookupService.getLookUpObject("REPOSITORYPATH");
		 try 
		 {
			    dept=Upservice.getDepartment(dept_id);
				sub_dept=Upservice.getsubdeptById(sub_dept_id);
				path=lookup.getLk_longname() + "/" + dept.getDept_name() + "/" +sub_dept.getSub_dept_name()+"/"+doc_type+ "/"+ year;
		 } 
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		return path;
		
	}
	
	public Long getLeastTaskUser(List<Object> objs) {
		// TODO Auto-generated method stub
		Long leastCount=Long.valueOf(0);
		Long userId=0L;
		for(int i=0;i<objs.size();i++){					
			Object[] row = (Object[]) objs.get(i);	
			Long count=Long.parseLong(row[0].toString());
			
			if(i==0)
				leastCount=count;
			
			if(count<=leastCount)
			{
				leastCount=count;
				userId=Long.parseLong(row[1].toString());
			}
			
		}
		return userId;
	}

	public String getPathToViewDocument(Long dfd_dept_mid, Long dfd_sub_dept_mid, String folder_name, String dt_name) {
		String path = "";
		 Department  dept= new Department();
		 SubDepartment sub_dept= new SubDepartment();
		 Lookup lookup=lookupService.getLookUpObject("REPOSITORYPATH");
		 try 
		 {
			    dept=Upservice.getDepartment(dfd_dept_mid);
				sub_dept=Upservice.getsubdeptById(dfd_sub_dept_mid);
				path=lookup.getLk_longname() + "/" + dept.getDept_name() + "/" +sub_dept.getSub_dept_name()+"/"+dt_name+ "/"+ folder_name;
		 } 
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		return path;
	}

	public String getPathToViewDocument(Long dfd_dept_mid, Long dfd_sub_dept_mid, String dt_name) {
		String path = "";
		 Department  dept= new Department();
		 SubDepartment sub_dept= new SubDepartment();
		 Lookup lookup=lookupService.getLookUpObject("REPOSITORYPATH");
		 try 
		 {
			    dept=Upservice.getDepartment(dfd_dept_mid);
				sub_dept=Upservice.getsubdeptById(dfd_sub_dept_mid);
				path=lookup.getLk_longname() +File.separator + dept.getDept_name() + File.separator +sub_dept.getSub_dept_name()+File.separator+dt_name;
		 } 
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		return path;
	}
	
	
	

}
