package com.mdi.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.Folder;
import com.mdi.model.Repository;
import com.mdi.model.User;
import com.mdi.service.FolderService;
import com.mdi.utility.CommenMethods;

@Controller
public class FolderController 
{
	
	@Autowired
	private FolderService service;
	
	CommenMethods cm;
	
	public FolderController()
	{
		cm= new CommenMethods();
	}

	@RequestMapping("folder/folderCreation")
	public String folderManagement()
	{
		return "views/folder/fodercreation";
	}
	
	@RequestMapping(value="/folder/getRepository",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getRepository()
	{
		List<Repository> repo= new ArrayList<Repository>();
		
		try 
		{
			repo=service.getRepository();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		ActionResponse<Repository> action= new ActionResponse<>();
		String result=cm.convert_to_json(repo);
		return result;
	}
	
	@RequestMapping(value="/folder/getAllDepartments",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getDepartments()
	{
		List<Department> repo= new ArrayList<Department>();
		
		try 
		{
			repo=service.getDepartments();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	
	@RequestMapping(value="/folder/getAllFolders",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getAllFolders()
	{
		String result= null;

		List<Folder> list = new ArrayList<>();
		try 
		{
			list=service.getAllFolders();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		result= cm.convert_to_json(list);
		System.out.println(result);
		return result;
	}
	
	
	@RequestMapping(value="/folder/createFolder",method=RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String createFolder(@RequestBody Folder folder,HttpSession session,HttpServletRequest request)
	{
		ActionResponse<Folder> response = new ActionResponse<>();
		String result= null;
	
		
		System.out.println(folder);
		User user= (User) session.getAttribute("USER");
		folder.setCreated_by(user.getUm_id());
		folder.setCreated(new Date());
		folder.setDisplay_name(folder.getFolder_name());
		
		folder.setStatus(1);
		try 
		{
			System.out.println(folder);
			service.saveFolder(folder);
			/*Department dept = service.getDepartmentById(folder.getDept_id());
			File  file= null;
			if(folder.getLevel()==3)
			{
				file= new File(repo.getBasepath()+File.separator+dept.getDept_name()+File.separator+folder.getFolder_name()+File.separator);
				System.out.println(file.getAbsolutePath());
			}
			else
			{
				file= new File(repo.getBasepath()+File.separator+folder.getFolder_name()+File.separator);
			}
			
			if(!file.exists())
			{
				file.mkdir();
				System.out.println(file.exists());
				System.out.println("folder created");
			}
			System.out.println(folder);*/
			response.setResponse("TRUE");
			response.setModelData(folder);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		
		result= cm.convert_to_json(response);
		return result;
	}
	
	@RequestMapping(value="/folder/getFolderByDeptid")
	public @ResponseBody String getFolderByDeptId(HttpServletRequest request)
	{
		String result="";
		String id=request.getParameter("id");
		List<Folder> fold = new ArrayList<>();
		Folder folder= null;
		Long dept_id=Long.parseLong(id);
		try 
		{
			Department dept=service.getDepartmentById(dept_id);
			fold = service.getFoldersByDeptid(dept_id);
			for(Folder fo : fold)
			{
				if(fo.getFolder_name().equals(dept.getDept_name()))
				{
					folder= fo;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		fold.clear();
		fold.add(folder);
		result = cm.convert_to_json(fold);
		System.out.println("durgendra");
		System.out.println(result);
		return result;
		
	}

}
