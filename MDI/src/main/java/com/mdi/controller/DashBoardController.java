package com.mdi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.ActionResponse;
import com.mdi.model.DailyActivityReport;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.User;
import com.mdi.service.DocumentFileDetailsService;
import com.mdi.service.DocumentFileStageService;
import com.mdi.service.LookupServices;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;

@Controller
public class DashBoardController {
	private CommenMethods commenMethod;

	@Autowired
	DocumentFileStageService documentFileStageService;

	@Autowired
	DocumentFileDetailsService documentfiledetailsService;

	// ccc
	@Autowired
	private LookupServices lkService;

	@Autowired
	UserService userservice;

	public DashBoardController() {
		commenMethod = new CommenMethods();
	}

	@RequestMapping(value = "/dashboard/getrecentdata", method = RequestMethod.GET)
	public @ResponseBody String getRecentData(HttpSession session, HttpServletRequest request) {

		String result = null;
		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		List<DailyActivityReport> darList = new ArrayList<DailyActivityReport>();
		User user = (User) session.getAttribute("USER");
		Long roleId = user.getUserroles().get(0).getUr_role_id();

		String path = request.getContextPath();
		List<Object> dfd = new ArrayList<Object>();
		if (roleId == 350992) // Uploader
		{
			dfd = documentfiledetailsService.getRecentData(user.getUm_id(), 14L);
			for (int i = 0; i < dfd.size(); i++) {
				Object[] row1 = (Object[]) dfd.get(i);
				dar = new DailyActivityReport();
				dar.setParameter1(row1[0].toString());
				dar.setParameter2(row1[1].toString());
				dar.setParameter3(row1[2].toString());
				dar.setParameter4(row1[3].toString());
				darList.add(dar);
				System.out.println(dar.getParameter1());
			}

		}
		if (roleId == 350994) // "MetaData"
		{
			dar.setFlag1(1);
			Long uplodedfiles = documentFileStageService.getUploadFiles(user.getUm_id(), 15L);
			dar.setValue1("" + uplodedfiles);

		}
		response.setModelList(darList);
		System.out.println(darList.size() + "   size");

		if (dar != null) {
			response.setResponse("TRUE");
			result = commenMethod.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return result;
	}

	@RequestMapping(value = "/dashboard/getreport", method = RequestMethod.GET)
	public @ResponseBody String getDashBoardReport(HttpSession session, HttpServletRequest request) {

		String result = null;

		ActionResponse<DailyActivityReport> response = new ActionResponse();

		DailyActivityReport dar = new DailyActivityReport();
		DailyActivityReport recentUpload = new DailyActivityReport();

		List<DailyActivityReport> darList = new ArrayList<DailyActivityReport>();

		User user = (User) session.getAttribute("USER");

		Long roleId = user.getUserroles().get(0).getUr_role_id();

		String path = request.getContextPath();
		if (roleId == 350992) // Uploader
		{
			dar.setFlag1(1);
			Long uplodedfiles = documentFileStageService.getUploadFiles(user.getUm_id(), 14L);
			dar.setValue1("" + uplodedfiles);
			System.out.println("Viru " + dar.getValue1());
		}
		if (roleId == 350994) // "MetaData"
		{
			dar.setFlag1(1);

			Long uplodedfiles = documentFileStageService.getUploadFiles(user.getUm_id(), 15L);
			dar.setValue1("" + uplodedfiles);

		}
		if (roleId == 350991) // "admin"
		{
			dar.setFlag1(1);

			Long uplodedfiles = documentFileStageService.getUploadFilesCountForAdmin(user.getUm_department_id(), 15L);
			dar.setValue1("" + uplodedfiles);

			List<DocumentFileDetails> dfd = documentfiledetailsService.getMostViewedFiles(user.getUm_department_id());

			dar.setDfd1(dfd);
		}
		if (roleId == 350993) // "checker"
		{
			dar.setFlag1(1);

			Long uplodedfiles = documentFileStageService.getUploadFilesForAdmin(user.getUm_id(), 16L);
			dar.setValue1("" + uplodedfiles);

		}
		if (roleId == 1 || roleId==11  || roleId==10000580) // "SysAdmin"
		{
			dar.setFlag1(1);

			Long uplodedfiles = documentFileStageService
					.getUploadFilesCountForSysAdmin(user.getUm_department_id(),
							15L);
			dar.setValue1("" + uplodedfiles);

			List<DocumentFileDetails> dfd = documentfiledetailsService
					.getMostViewedSysAdmFiles(user.getUm_department_id());

			dar.setDfd1(dfd);

		}
		/*
		 * if(roleId == 21) // "Checker" { dar.setFlag1(1); dar.setFlag2(1);
		 * 
		 * dar.setParameter1("Pending Data Entry");
		 * dar.setLink1(path+"/casefile/landingpage");
		 * 
		 * dar.setParameter2("Total Rejected");
		 * dar.setLink2(path+"/casefile/landingpage");
		 * 
		 * Integer totalRejected=caseFileDetailService.getCaseFileCount(
		 * "20,350984,350985", user.getUm_id(),"Y"); Integer
		 * totalVerified=caseFileDetailService.getCaseFileCount(
		 * "20,350984,350985", user.getUm_id(),"N");
		 * 
		 * //List<CaseFileDetail> totalVerified =
		 * caseFileDetailService.getAllByRejectStatus(20L, user.getUm_id(),"N");
		 * //List<CaseFileDetail> totalRejected =
		 * caseFileDetailService.getAllByRejectStatus(20L, user.getUm_id(),"Y");
		 * 
		 * dar.setValue1(""+totalVerified); dar.setValue2(""+totalRejected);
		 * 
		 * } if(roleId == 22) // Verifier { dar.setFlag1(1); dar.setFlag2(1);
		 * 
		 * dar.setParameter1("Verify Pending");
		 * dar.setLink1(path+"/qualityCheckIndex");
		 * 
		 * dar.setParameter2("Ready for Outward"); dar.setLink2(path);
		 * 
		 * 
		 * 
		 * Integer verifyPending =
		 * ibService.getAssignBundlesCount(user.getUm_id()); Integer
		 * getbundles=outwardBundleService.getBundleListCount(user.getUm_id(),
		 * 14L);
		 * 
		 * 
		 * dar.setValue1(""+verifyPending); dar.setValue2(""+getbundles);
		 * 
		 * dar.setFlag1(1); dar.setFlag2(1); dar.setFlag3(1); dar.setFlag4(1);
		 * dar.setFlag5(1);
		 * 
		 * //Added by Sandesh dar.setFlag6(1); dar.setFlag7(1); dar.setFlag8(1);
		 * 
		 * dar.setParameter1("Verify Pending");
		 * dar.setLink1(path+"/qualityCheckIndex");
		 * 
		 * dar.setParameter2("Approved Files");
		 * dar.setLink2(path+"/report/verifierproductivityReport");
		 * 
		 * dar.setParameter3("Rejected For Metadata");
		 * dar.setLink3(path+"/report/verifierproductivityReport");
		 * 
		 * dar.setParameter4("Rejected For Scan");
		 * dar.setLink4(path+"/report/verifierproductivityReport");
		 * 
		 * dar.setParameter5("File OnHold");
		 * dar.setLink5(path+"/report/verifierproductivityReport");
		 * 
		 * // dar.setParameter6("Verify Pending TeamB");
		 * //dar.setLink1(path+"/qualityCheckIndex");
		 * 
		 * dar.setParameter7("Approved Files TeamB");
		 * //dar.setLink2(path+"/report/verifierproductivityReport");
		 * 
		 * dar.setParameter8("Rejected For Metadata TeamB");
		 * //dar.setLink3(path+"/report/verifierproductivityReport");
		 * 
		 * String username= user.getUsername();
		 * 
		 * String finalusername = (username +"_B").toString(); //String
		 * finalusername2 = username +" B";
		 * System.out.println("User name "+username );
		 * System.out.println("User name "+finalusername.toString() );
		 * 
		 * 
		 * Integer verifyPending =
		 * ibService.getAssignBundlesCount(user.getUm_id()); //Integer
		 * getbundles=outwardBundleService.getBundleListCount(user.getUm_id(),
		 * 14L); // old count Integer
		 * approveFiles=reportsService.getApprovedFilesCount(user.getUm_id());
		 * Integer
		 * rejectedForMeta=reportsService.getRejectedForMetaFilesCount(user.
		 * getUm_id()); Integer
		 * rejectedForScan=reportsService.getRejectedForScanFilesCount(user.
		 * getUm_id()); Integer
		 * onHold=reportsService.getHoldFileCount(user.getUm_id());
		 * 
		 * 
		 * 
		 * Long um_id2 = userservice.getUserId(finalusername);
		 * 
		 * if(um_id2 != 0L){ Integer verifyPending_teamb =
		 * ibService.getAssignBundlesCount(um_id2); //Integer
		 * getbundles=outwardBundleService.getBundleListCount(user.getUm_id(),
		 * 14L); // old count Integer approveFiles_teamb
		 * =reportsService.getApprovedFilesCount(um_id2); Integer
		 * rejectedForMeta_teamb
		 * =reportsService.getRejectedForMetaFilesCount(um_id2); Integer
		 * rejectedForScan_teamb
		 * =reportsService.getRejectedForScanFilesCount(um_id2); Integer
		 * onHold_teamb =reportsService.getHoldFileCount(um_id2);
		 * 
		 * 
		 * 
		 * System.out.println(verifyPending_teamb + " "+approveFiles_teamb + " "
		 * +rejectedForMeta_teamb + " " +rejectedForScan_teamb);
		 * 
		 * // Final count
		 * 
		 * System.out.println("TEAM B"
		 * +verifyPending_teamb+"TEAMB"+verifyPending); Integer
		 * final_verifyPending = verifyPending +verifyPending_teamb; Integer
		 * final_approveFiles = approveFiles + approveFiles_teamb; Integer
		 * final_rejectedForMeta = rejectedForMeta + rejectedForMeta_teamb;
		 * Integer final_rejectedForScan = rejectedForScan +
		 * rejectedForScan_teamb; Integer final_onHold = onHold + onHold_teamb;
		 * 
		 * 
		 * System.out.println(final_verifyPending );
		 * 
		 * 
		 * 
		 * dar.setValue1(""+verifyPending); dar.setValue2(""+approveFiles);
		 * dar.setValue3(""+rejectedForMeta); dar.setValue4(""+rejectedForScan);
		 * dar.setValue5(""+onHold);
		 * 
		 * dar.setValue1(""+final_verifyPending);
		 * dar.setValue2(""+final_approveFiles);
		 * dar.setValue3(""+final_rejectedForMeta);
		 * dar.setValue4(""+final_rejectedForScan);
		 * dar.setValue5(""+final_onHold); } else{
		 * dar.setValue1(""+verifyPending); dar.setValue2(""+approveFiles);
		 * dar.setValue3(""+rejectedForMeta); dar.setValue4(""+rejectedForScan);
		 * dar.setValue5(""+onHold); } }
		 */
		darList.add(dar);

		response.setModelList(darList);

		if (dar != null) {
			response.setResponse("TRUE");
			result = commenMethod.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return result;

	}

	@RequestMapping(value = "/dashboard/getRecentMetaEntryFiles", method = RequestMethod.GET)
	public @ResponseBody String getRecentMetaEntryFiles(HttpSession session) {
		String result = null;
		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		List<DailyActivityReport> darList = new ArrayList<DailyActivityReport>();
		User user = (User) session.getAttribute("USER");
		Long roleId = user.getUserroles().get(0).getUr_role_id();

		List<Object> dfd = new ArrayList<Object>();
		if (roleId == 350994) // metadata
		{
			dfd = documentfiledetailsService.getRecentData(user.getUm_id(), 15L);
			for (int i = 0; i < dfd.size(); i++) {
				Object[] row1 = (Object[]) dfd.get(i);
				dar = new DailyActivityReport();
				dar.setParameter1(row1[0].toString());
				dar.setParameter2(row1[1].toString());
				dar.setParameter3(row1[2].toString());
				dar.setParameter4(row1[3].toString());
				darList.add(dar);
				System.out.println(dar.getParameter1());
			}

		}
		if (roleId == 350994) // "MetaData"
		{
			dar.setFlag1(1);
			Long uplodedfiles = documentFileStageService.getUploadFiles(user.getUm_id(), 15L);
			dar.setValue1("" + uplodedfiles);

		}
		response.setModelList(darList);
		System.out.println(darList.size() + "   size");

		if (dar != null) {
			response.setResponse("TRUE");
			result = commenMethod.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return result;
	}

	@RequestMapping(value = "/dashboard/getRecentCheckedFiles", method = RequestMethod.GET)
	public @ResponseBody String getRecentCheckedFiles(HttpSession session) {
		String result = null;
		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		List<DailyActivityReport> darList = new ArrayList<DailyActivityReport>();
		User user = (User) session.getAttribute("USER");
		Long roleId = user.getUserroles().get(0).getUr_role_id();

		List<Object> dfd = new ArrayList<Object>();
		if (roleId == 350993) // checker
		{
			dfd = documentfiledetailsService.getRecentData(user.getUm_id(), 16L);
			for (int i = 0; i < dfd.size(); i++) {
				Object[] row1 = (Object[]) dfd.get(i);
				dar = new DailyActivityReport();
				dar.setParameter1(row1[0].toString());
				dar.setParameter2(row1[1].toString());
				dar.setParameter3(row1[2].toString());
				dar.setParameter4(row1[3].toString());
				darList.add(dar);
				System.out.println(dar.getParameter1());
			}

		}
		if (roleId == 350993) // "checker"
		{
			dar.setFlag1(1);
			Long uplodedfiles = documentFileStageService.getUploadFiles(user.getUm_id(), 16L);
			dar.setValue1("" + uplodedfiles);

		}
		response.setModelList(darList);
		System.out.println(darList.size() + "   size");

		if (dar != null) {
			response.setResponse("TRUE");
			result = commenMethod.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return result;
	}

	@RequestMapping(value = "/dashboard/getUploadedDataAdmin", method = RequestMethod.GET)
	public @ResponseBody String getUploadedDataAdmin(HttpSession session) {
		String result = null;
		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		List<DailyActivityReport> darList = new ArrayList<DailyActivityReport>();
		User user = (User) session.getAttribute("USER");
		Long roleId = user.getUserroles().get(0).getUr_role_id();

		List<Object> dfd = new ArrayList<Object>();
		if (roleId == 350991) // admin
		{
			dfd = documentfiledetailsService.getUploadDataAdmin(user.getUm_id(), 14L, user.getUm_department_id());
			for (int i = 0; i < dfd.size(); i++) {
				Object[] row1 = (Object[]) dfd.get(i);
				dar = new DailyActivityReport();
				dar.setParameter1(row1[0].toString());
				dar.setParameter2(row1[1].toString());
				dar.setParameter3(row1[2].toString());
				//dar.setParameter4(row1[3].toString());
				darList.add(dar);
				System.out.println(dar.getParameter1());
			}

		}
		if (roleId == 350991) // "admin"
		{
			dar.setFlag1(1);
			Long uplodedfiles = documentFileStageService.getUploadFiles(user.getUm_id(), 16L);
			dar.setValue1("" + uplodedfiles);

		}
		response.setModelList(darList);
		System.out.println(darList.size() + "   size");

		if (dar != null) {
			response.setResponse("TRUE");
			result = commenMethod.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return result;
	}

	@RequestMapping(value = "/dashboard/getviewedDocCount")
	public @ResponseBody String getViewedFileCount(HttpSession session) {
		User user = (User) session.getAttribute("USER");
		ActionResponse<Long> response = new ActionResponse<>();
		String result = "";
		Long count = 0l;
		try {
			Long role_id = user.getUserroles().get(0).getUr_role_id();
			if (role_id.equals(1l) || role_id.equals(11l) || role_id.equals(10000580l) )
				count = documentfiledetailsService.getViewdfileCountSysAdmin();
			else {
				count = documentfiledetailsService.getViewdfileCount(user
						.getUm_department_id());
			}
			System.out.println(count + "  mmm");
			response.setResponse("TRUE");
			response.setData(count);
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		result = commenMethod.convert_to_json(response);
		return result;
	}

	@RequestMapping(value = "/dashboard/getUploadedDataSysAdmin", method = RequestMethod.GET)
	public @ResponseBody String getUploadedDataSysAdmin( HttpSession session) {
		String result = null;
		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		List<DailyActivityReport> darList = new ArrayList<DailyActivityReport>();
		User user = (User) session.getAttribute("USER");
		Long roleId = user.getUserroles().get(0).getUr_role_id();

		List<Object> dfd = new ArrayList<Object>();
		if (roleId == 1l || roleId==11L || roleId==10000580l) // admin
		{
			dfd = documentfiledetailsService.getRecentDataSysAdmin( );
			for (int i = 0; i < dfd.size(); i++) {
				Object[] row1 = (Object[]) dfd.get(i);
				dar = new DailyActivityReport();
				dar.setParameter1(row1[0].toString());
				dar.setParameter2(row1[1].toString());
				dar.setParameter3(row1[2].toString());
				//dar.setParameter4(row1[3].toString());
				darList.add(dar);
				System.out.println(dar.getParameter1());
			}

		}
		/*if (roleId == 350991) // "admin"
		{
			dar.setFlag1(1);
			Long uplodedfiles = documentFileStageService.getUploadFiles(
					user.getUm_id(), 16L);
			dar.setValue1("" + uplodedfiles);

		}*/
		response.setModelList(darList);
		System.out.println(darList.size() + "   size");

		if (dar != null) {
			response.setResponse("TRUE");
			result = commenMethod.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return result;
	}
}
