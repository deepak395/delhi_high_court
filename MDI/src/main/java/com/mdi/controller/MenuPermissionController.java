package com.mdi.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.ActionResponse;
import com.mdi.model.Lookup;
import com.mdi.model.Tree;
import com.mdi.model.User;
import com.mdi.service.LookupService;
import com.mdi.service.MenuMappingService;
import com.mdi.utility.CommenMethods;
import com.mdi.utility.GlobalFunction;

@Controller
public class MenuPermissionController {

	@Autowired 
	private MenuMappingService mappingService;
	
	@Autowired
	private LookupService lookupService;
	
	 private GlobalFunction globalfunction;
		
		
		public MenuPermissionController() {
		
			globalfunction = new GlobalFunction();
		}
		
	@RequestMapping(value = "menupermission/menupermission")
	public String menuPermission() {
		return "views/menupermission/menupermission";
	}
	
	@RequestMapping(value = "/menupermission/getRoleList", method = RequestMethod.GET)
	public @ResponseBody String getRoleList(HttpSession session) {

		String json = null;
		ActionResponse<Lookup> response = new ActionResponse();
		response.setResponse("FALSE");
		List<Lookup> list = lookupService.getRoleList("MDI_ROLE");
		System.out.println("role list in contoller" + list.size());
		System.out.println("-----------" + list);
		
		response.setModelList(list);
		if (list != null)
			response.setResponse("TRUE");
		json = globalfunction.convert_to_json(list);
		return json;

	}
	
	@RequestMapping(value = "/menupermission/getRole", method = RequestMethod.GET)
	public @ResponseBody String getRole() {
		String jsonData = null;

		List<Tree> treeList = mappingService.getRoleTree();
		System.out.println(treeList);
		jsonData = globalfunction.convert_to_json(treeList);
		return jsonData;
	}
}
