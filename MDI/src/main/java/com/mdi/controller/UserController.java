package com.mdi.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileStage;
import com.mdi.model.Folder;
import com.mdi.model.FolderPermission;
import com.mdi.model.Lookup;
import com.mdi.model.Permission;
import com.mdi.model.Repository;
import com.mdi.model.RepositoryPermission;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.model.UserRole;
import com.mdi.service.DocumentFileStageService;
import com.mdi.service.FolderPermissionService;
import com.mdi.service.FolderService;
import com.mdi.service.LookupService;
import com.mdi.service.UserRoleService;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;
import com.mdi.utility.GlobalFunction;
import com.mdi.validation.PermissionValidator;
import com.mdi.validation.UserMstrValidator;

@Controller
public class UserController {

	@Autowired
	private UserService userservice;
	
	@Autowired
	private PermissionValidator permissionValidation;

	@Autowired
	private LookupService lookUpService;

	@Autowired
	private UserRoleService userroleService;
	HttpSession session;

	@Autowired
	private UserMstrValidator umvalidation;
	
	@Autowired
	private FolderService folderService;
	
	@Autowired
	private DocumentFileStageService documentfilestage;

	
	@Autowired
	private FolderPermissionService fold_per_ser;
	
	private CommenMethods cm;
	
	private List<Long> childfolders=new ArrayList<Long>();

	private GlobalFunction globalfunction;

	public UserController() {
		globalfunction = new GlobalFunction();
		cm = new CommenMethods();
	}

	@RequestMapping(value = "user/manage")
	public String srologin(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "views/user/manage";
	}
	
	
	@RequestMapping("/user/dashBoard")
	public String gotoDashBoard()
	{
		return "views/user/dashboard";
		
	}

	@RequestMapping(value = "/user/create", method = RequestMethod.POST)
	public @ResponseBody String create(@RequestBody User um, HttpSession session) {
		String jsonData = "";
		ActionResponse<User> response = umvalidation.doValidation(um);
		User temp_um = um;
		User user = (User) session.getAttribute("USER");

		System.out.print(um.getUm_fullname());
		if (response.getResponse() == "TRUE") {
			

			//um.setPassword(globalfunction.md5encryption(um.getPassword()));
			um.setCr_date(new Date());
			um.setPassword(um.getPassword());
			um.setCr_by(user.getUm_id());
			um.setCr_date(new Date());
			um.setLast_login(new Date());
			um.setIsfirstlogin(true);
			
			//long attempt
			um.setUm_login_attemps(0);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DATE, -1);
			um.setUm_pass_validity_date(cal.getTime());
			um.setUm_avilable_status("Y");
			if (um.getRec_status() == 1) {
				um.setUm_account_activation(1);
			}
			  
			if(temp_um.getUm_role_id()==10000579L || temp_um.getUm_role_id()==1L || temp_um.getUm_role_id()==10000580L) {
				um.setUm_parent_id(1L);
			}
			else {
				um.setUm_parent_id(2L);
			}
			um = userservice.save(um);
			if(um.getUm_id()!=null){ 

				  UserRole ur=userroleService.getUserRoleById(um.getUm_id());
				  if(ur.getUr_id()==null){
				  ur.setUr_um_mid(um.getUm_id());
				  ur.setUr_role_id(temp_um.getUm_role_id()); 
				  ur.setUr_cr_date(new Date()); 
				  ur.setUr_rec_status(1); 
				  }
			      else{  
				  ur.setUr_role_id(temp_um.getUm_role_id()); 
				  ur.setUr_mod_date(new Date()); 
				  } 
				  userroleService.save(ur);
			      
				  }
				 
				um=userservice.getUser(um.getUm_id());
				response.setModelData(um);
		}
		jsonData = globalfunction.convert_to_json(response);

		return jsonData;
	}

	@RequestMapping(value = "/user/getbenchcode", method = RequestMethod.GET)
	public @ResponseBody String getbenchcode(Model model) {
		String jsonData = null;

		List<Lookup> benchcodes = lookUpService.getAll("BRANCH");

		if (benchcodes != null) {
			jsonData = globalfunction.convert_to_json(benchcodes);
		}

		return jsonData;
	}

	@RequestMapping(value = "/user/getmasters", method = RequestMethod.GET)
	public @ResponseBody String getrolelist(Model model) {
		String jsonData = null;
		String roleData = "";
		String designationData = "";
		String departmentData = "";

		List<Lookup> getrole = lookUpService.getAll("MDI_ROLE");

		if (getrole != null) {
			roleData = globalfunction.convert_to_json(getrole);
		}

		List<Lookup> getdesignationlist = lookUpService.getAll("DESIGNTION");

		if (getdesignationlist != null) {
			designationData = globalfunction.convert_to_json(getdesignationlist);
		}

		List<Department> getdepartmentlist = lookUpService.getAllDepartment();

		if (getdepartmentlist != null) {
			departmentData = globalfunction.convert_to_json(getdepartmentlist);
		}

		jsonData = "{\"roleData\":" + roleData + ",\"designationData\":" + designationData + ",\"departmentData\":"
				+ departmentData + "}";
		System.out.println(jsonData);
		return jsonData;

	}

	@RequestMapping(value = "/user/updateName", method = RequestMethod.POST)
	public @ResponseBody String updateName(@RequestBody User user, HttpSession session) {
		String jsonData = null;

		ActionResponse<User> response = new ActionResponse();
		User userdetail = userservice.getUserDetail(user.getUm_id());

		System.out.println("userdetail:" + userdetail.getUm_id());
		System.out.println("updated full name :" + user.getUm_fullname());

		userdetail.setUm_fullname(user.getUm_fullname());
		userdetail.setPdf_viewer_speed(user.getPdf_viewer_speed());
		userservice.save(userdetail);
		// usersession.setUm_fullname(user.getUm_fullname());

		session.setAttribute("USER", userdetail);

		response.setResponse("TRUE");
		jsonData = globalfunction.convert_to_json(response);

		return jsonData;

	}

	// get all user according there login

	@RequestMapping(value = "/user/getallusers")
	public @ResponseBody String getUserData(HttpSession session) {
		String jsonData = null;
		User user = new User();
		user = (User) session.getAttribute("USER");

		// get all users

		List<User> getAllUsers = userservice.getAllUser();

		// get all admin user
		/*
		 * List<Long> userids= urService.getSytemAdminUsers();
		 * 
		 * List<User> users=new ArrayList<User>();
		 * 
		 * if(userids.contains(user.getUm_id())){ // if logged in user is system
		 * admin then get all users users=userservice.getAllByRole(); }else{ //
		 * else get users by benchcode
		 * users=userservice.getUsersByBechcode(user.getUm_bench_code(),user.
		 * getUm_id()); }
		 * 
		 * if(users != null){ jsonData = globalfunction.convert_to_json(users);
		 * }
		 */

		if (getAllUsers != null) {
			jsonData = globalfunction.convert_to_json(getAllUsers);
		}
		System.out.println(jsonData);
		return jsonData;
	}
	
	@RequestMapping(value = "/user/getUsersDataById")
	public @ResponseBody String getUserDataById(HttpSession session) {
		String jsonData = null;
		User user = new User();
		user = (User) session.getAttribute("USER");

		// get all users

	

		// get all admin user
		/*
		 * List<Long> userids= urService.getSytemAdminUsers();
		 * 
		 * List<User> users=new ArrayList<User>();
		 * 
		 * if(userids.contains(user.getUm_id())){ // if logged in user is system
		 * admin then get all users users=userservice.getAllByRole(); }else{ //
		 * else get users by benchcode
		 * users=userservice.getUsersByBechcode(user.getUm_bench_code(),user.
		 * getUm_id()); }
		 * 
		 * if(users != null){ jsonData = globalfunction.convert_to_json(users);
		 * }
		 */

		
	     jsonData = globalfunction.convert_to_json(user);
		System.out.println(jsonData);
		return jsonData;
	}
	
	/*@RequestMapping(value="/user/getuserpermission")
	public @ResponseBody String getuserpermission(@RequestBody User user,HttpSession session)
	{
		
		String jsonData="";
		Long userId=user.getUm_id();
		
		try 
		{
			List<Folder>folderpermissions=fold_per_ser.getNotAssignedFolders(userId);
			
			if(!folderpermissions.isEmpty())
			{
				for(Folder f:folderpermissions)
				{
					Permission folderexist=fold_per_ser.checkFolderexist(f,userId);
					if(folderexist==null)
					{
					    Permission perm=new Permission();
					    perm.setType(2);
					    perm.setStatus(0);
					    perm.setUserId(userId);
					    perm.setValue(f.getId());
					    fold_per_ser.save(perm);
					}
				}
			}
			List<Repository> repositorypermissions=fold_per_ser.getNotAssignedRepositories(userId);
			if(!repositorypermissions.isEmpty())
			{
				for(Repository r:repositorypermissions)
				{
					Permission repositoryexist=fold_per_ser.checkRepositoryexist(r,userId);
					if(repositoryexist==null)
					{
					   Permission perm=new Permission();
					   perm.setType(1);
					   perm.setStatus(0);
					   perm.setUserId(userId);
					   perm.setValue(r.getId());
					   fold_per_ser.save(perm);
					}
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}

		jsonData = getUserPermissions(userId);
		System.out.println(jsonData);
		return jsonData;
	}*/
	
	/*private String getUserPermissions(Long userId) 
	{
		List<RepositoryPermission> repperms = new ArrayList<RepositoryPermission>();
		List<FolderPermission> folderperms = new ArrayList<FolderPermission>();
		//Map<List<RepositoryPermission>, List<FolderPermission>> allpermissions = new HashMap<List<RepositoryPermission>, List<FolderPermission>>();

		List<Permission> temppermissions = fold_per_ser
				.getPermissionByUser(userId);

		for (Permission permission : temppermissions) {
			permission.setIsParent(0);
			permission.setAssignAll(false);
			if (permission.getType() == 1) {
				Repository r = fold_per_ser.getRepository(permission
						.getValue());
				RepositoryPermission repperm = new RepositoryPermission();

				repperm.setId(permission.getId());
				repperm.setAssignAll(permission.getAssignAll());
				repperm.setStatus(permission.getStatus());
				repperm.setName(r.getName());
				repperm.setFolderPath(permission.getFolderPath());
				repperm.setUserId(permission.getUserId());
				repperm.setValue(permission.getValue());
				repperm.setType(permission.getType());
				repperm.setIsParent(permission.getIsParent());

				repperms.add(repperm);
			} else if (permission.getType() == 2) {
				Folder f = fold_per_ser.getFolderById(permission.getValue());
				String folderPath = "";
				Long parent_id = f.getParent_id();
				while (parent_id != null) {
					Folder fd = fold_per_ser.getFolderById(parent_id);
					folderPath = fd.getFolder_name() + " -> " + folderPath;
					parent_id = fd.getParent_id();
				}

				folderPath = folderPath + f.getFolder_name();
				permission.setFolderPath(folderPath);

				if (!f.getChildrens().isEmpty()) {
					permission.setAssignAll(false);
					permission.setIsParent(1);
					int size = f.getChildrens().size();
					int i = 0;
					for (Folder fchildren : f.getChildrens()) {
						i++;
						Permission childfolderpermission = findchildren(fchildren, temppermissions, userId);
						if (childfolderpermission.getId() != null) {
							if (childfolderpermission.getStatus() != 1) {
								i--;
							}
						}
					}
					if (i != size)
						permission.setAssignAll(true);

					System.out.println("folderID=" + permission.getName()
							+ "&i=" + i + "&size=" + size);
				}

				FolderPermission folderperm = new FolderPermission();

				folderperm.setId(permission.getId());
				folderperm.setAssignAll(permission.getAssignAll());
				folderperm.setStatus(permission.getStatus());
				folderperm.setName(f.getFolder_name());
				folderperm.setFolderPath(permission.getFolderPath());
				folderperm.setUserId(permission.getUserId());
				folderperm.setValue(permission.getValue());
				folderperm.setIsParent(permission.getIsParent());
				folderperm.setType(permission.getType());
				folderperms.add(folderperm);
			}

		}
		Comparator<FolderPermission> com = new Comparator<FolderPermission>() {

			@Override
			public int compare(FolderPermission o1, FolderPermission o2) {

				return o1.getFolderPath().compareTo(o2.getFolderPath());
			}
		};

		Collections.sort(folderperms, com);
		String jsonData = "{\"response\":\"TRUE\",\"repositories\":"
				+ globalfunction.convert_to_json(repperms) + ",\"folders\":"
				+ globalfunction.convert_to_json(folderperms) + "}";
		return jsonData;
	}
	
	private Permission findchildren(Folder fchildren,
			List<Permission> temppermissions, Long userId) {
		Permission child = new Permission();
		for (Permission permission : temppermissions) {
			if (permission.getValue().longValue() == fchildren.getId()
					.longValue()
					&& userId.longValue() == permission.getUserId().longValue()) {
				child = new Permission();
				child.setAssignAll(permission.getAssignAll());
				child.setId(permission.getId());
				child.setStatus(permission.getStatus());
			}
		}
		return child;
	}
	*/
	@RequestMapping(value = "/user/changepassword", method = RequestMethod.POST)
	public @ResponseBody String changePassword(@RequestBody User u,HttpSession session) 
	{ 
		String jsonData = null;
		System.out.println(u.getConfirmpassword()+" "+u.getPassword()+u.getUm_id());
		User user=(User) session.getAttribute("USER");
		ActionResponse<User> response =new ActionResponse<User>() ;
		response = umvalidation.doValidationForPassword(u);
		
		String password=u.getPassword();
		String congirmPassword=u.getConfirmpassword();
		
		User getdata =userservice.getChangepassword(u.getUm_id());
		
		System.out.println(getdata+"getdata");
		String pass=u.getPassword();
		System.out.println(u.getPassword()+"pass");
		if (password.equals(congirmPassword)) {
			//globalfunction.md5encryption
			getdata.setPassword((u.getPassword()));
			getdata.setIsfirstlogin(false);
			userservice.update(getdata);
		}
		
		jsonData = globalfunction.convert_to_json(response);
		System.out.println("last "+response.getResponse());
	    return jsonData;
	}
	

	/*@RequestMapping(value = "/user/updateuserpermission", method = RequestMethod.POST)	
	public @ResponseBody String updateuserpermission(@RequestBody Permission perm,HttpSession session) throws Exception {
		String jsonData="";
		this.childfolders.clear();
		ActionResponse<Permission> response = new ActionResponse();
		response = permissionValidation.doValidation(perm);
		
		if(response.getResponse()=="TRUE")
		{
		
			perm=fold_per_ser.save(perm);
			
			if(perm.getType()==2){
				
				if(perm.getStatus()==0)
				{
					List<Long> folderids=new ArrayList<Long>();
					
					folderids=getallchildrens(perm.getValue());
					
					if(!folderids.isEmpty())
						fold_per_ser.updateChildFolders(folderids,perm.getUserId(),perm.getStatus());
				}
			}
			
			Long userId=perm.getUserId();
			List<Folder>folderpermissions=fold_per_ser.getNotAssignedFolders(userId);
			
			if(!folderpermissions.isEmpty())
			{
				for(Folder f:folderpermissions)
				{
					Permission folderexist=fold_per_ser.checkFolderexist(f,userId);
					if(folderexist==null){
					Permission per=new Permission();
					perm.setType(2);
					perm.setStatus(0);
					perm.setUserId(userId);
					perm.setValue(f.getId());
					fold_per_ser.save(per);
					}
				}
			}
			
			List<Repository> repositorypermissions=fold_per_ser.getNotAssignedRepositories(userId);
			if(!repositorypermissions.isEmpty())
			{
				for(Repository r:repositorypermissions)
				{
					Permission repositoryexist=fold_per_ser.checkRepositoryexist(r,userId);
					if(repositoryexist==null){
					Permission per=new Permission();
					perm.setType(1);
					perm.setStatus(0);
					perm.setUserId(userId);
					perm.setValue(r.getId());
					fold_per_ser.save(per);
					}
				}
			}
			
			
			
			jsonData=getUserPermissions(userId);
			
		}
		else
		{
			jsonData=globalfunction.convert_to_json(response);
		}
		
		return jsonData;
	}*/
	
	public List<Long> getallchildrens(Long folderId){
		this.childfolders.add(folderId);
		List<Folder> flist=folderService.getFoldersByParentId(folderId);
		
		if(!flist.isEmpty()){
			for(Folder f:flist)
			{
				this.getallchildrens(f.getId());
			}
		}
		return this.childfolders;
	}

	/*@RequestMapping(value = "/user/assignallfolders", method = RequestMethod.POST)	
	public @ResponseBody String assignallfolders(@RequestBody Permission perm,HttpSession session) throws Exception 
	{
		this.childfolders.clear();
		
		String jsonData="";
		
		Long userId=perm.getUserId();
		Long folderId=perm.getValue();
		ActionResponse<Permission> response = new ActionResponse();
		response = permissionValidation.doValidation(perm);
		
		if(response.getResponse()=="TRUE")
		{
			perm=fold_per_ser.save(perm);
			
			if(perm.getType()==2)
			{
				if(perm.getStatus()==1)
				{
					List<Long> folderids=new ArrayList<Long>();
					
					folderids=getallchildrens(perm.getValue());
					
					if(!folderids.isEmpty())
						fold_per_ser.updateChildFolders(folderids,perm.getUserId(),perm.getStatus());
				}
			}
			jsonData=getUserPermissions(userId);
		
		}else{
			jsonData=globalfunction.convert_to_json(response);
		}
		
		return jsonData;
	}*/
	
	@RequestMapping(value="/user/update",method= RequestMethod.POST)
	public @ResponseBody String userupdate(@RequestBody User um,HttpSession session) {
		
		String jsonData="";
		ActionResponse<User> response = umvalidation.doValidation(um);
		
		User user = (User) session.getAttribute("USER");
		um.setUm_fullname(um.getUm_fullname());
		um.setUm_email_id(um.getUm_email_id());
		um.setUm_pass_validity_date(um.getUm_pass_validity_date());
		if(um.getRec_status()==1)
		{
			um.setUm_account_activation(1);
		}
		

		 um=userservice.save(um);
		 response.setModelData(um);
		 
		 jsonData = globalfunction.convert_to_json(response);
		 return jsonData;
		
	}
	
	
	
	@RequestMapping(value="/user/getUserSubDepartments")
	public @ResponseBody String getUserDeparment(  @RequestParam("dpt_id")Long dpt_id,@RequestParam("um_id")Long um_id)
	{
		ActionResponse<Permission> response = new ActionResponse<>();
		try 
		{
			Department dept = userservice.getDeptById(dpt_id);
			List<SubDepartment> sub_dept = userservice.getSubDepartmentIds(dpt_id);
			List<Permission> perm = userservice.getUserPermittedSubDepts(um_id);
			
			List<Permission> perm2 = new ArrayList<>();
			
				for(SubDepartment s : sub_dept)
				{
					Permission per = new Permission();
					per.setPm_sub_dpt_mid(s.getSub_dept_id());
					per.setPm_um_mid(um_id);
					per.setSub_dept_name(s.getSub_dept_name());
					
					//User_Subdept_permissions up = new User_Subdept_permissions();
					//up.setSub_dept_id(s.getSub_dept_id());
					//up.setSub_dept_name(s.getSub_dept_name());
					//up.setUm_id(um_id);
					//boolean status = false;
					if(perm.size()>0)
					for(Permission p : perm)
					{
						System.out.println(s.getSub_dept_id()+"  "+p.getPm_sub_dpt_mid());
						if(s.getSub_dept_id().equals(p.getPm_sub_dpt_mid()) && p.getPm_rec_status()==1)
						{
							//status = true;
							//up.setStatus(true);
							per.setValue(true);
							per.setPm_id(p.getPm_id());
						}
						if(s.getSub_dept_id().equals(p.getPm_sub_dpt_mid()) && p.getPm_rec_status()==0)
						{
							per.setPm_id(p.getPm_id());
						}
					}
					perm2.add(per);
					//list.add(up);
				}
			
			
			System.out.println(perm2);
			response.setModelList(perm2);
			response.setResponse("TRUE");
			response.setData(dept);
		} 
		catch (Exception e)
        {
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		System.out.println(cm);
		return cm.convert_to_json(response);
		
	}
	
	
	@RequestMapping(value="/user/SaveSubDeptPermission",method=RequestMethod.POST)
	public @ResponseBody String saveUserSubDeptPermission(@RequestBody List<Permission> perm)
	{
		ActionResponse<Permission> response = new ActionResponse<>();
		try 
		{
			userservice.saveUserSubDeptPermissions(perm);
			response.setResponse("TRUE");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			response.setResponse("FALSE");
			response.setData(e);
		}
		System.out.println(perm);
		return cm.convert_to_json(response);
		
	}

	
	@RequestMapping(value="/user/getAllDepartments")
	public @ResponseBody String getAllDepartments()
	{
		ActionResponse<Department> response = new ActionResponse<>();
		List<Department> dptList=null;
		
		
			dptList=userservice.getAllDepartment();
	             response.setModelList(dptList);
	        return cm.convert_to_json(response);
	
	
	
	
}
}
