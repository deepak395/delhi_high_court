package com.mdi.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.ActionResponse;
import com.mdi.model.DailyActivityReport;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.Lookup;
import com.mdi.model.User;
import com.mdi.service.CommanReportsServices;
import com.mdi.service.DocumentFileDetailsService;
import com.mdi.service.DocumentFileStageService;
import com.mdi.service.LookupServices;
import com.mdi.service.MetaDataService;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;

@Controller
public class CommanReportController {
	private CommenMethods cm;

	@Autowired
	private MetaDataService service;

	@Autowired
	DocumentFileStageService documentFileStageService;

	@Autowired
	DocumentFileDetailsService documentfiledetailsService;

	@Autowired
	private LookupServices lkService;

	@Autowired
	UserService userservice;

	@Autowired
	CommanReportsServices ReportServices;

	@Autowired
	ServletContext context;

	public CommanReportController() {
		cm = new CommenMethods();
	}

	@RequestMapping(value = "/reports/LoginReport", method = RequestMethod.GET)
	public String LoginReport() {

		return "views/reports/LoginReport";
	}

	@RequestMapping(value = "/reports/UploadedDocumentRpt", method = RequestMethod.GET)
	public String UploadedDocumentRepord() {

		return "views/reports/DocumentUploadedReport";
	}

	@RequestMapping(value = "/reports/MetaDataReport", method = RequestMethod.GET)
	public String MetaDataReport() {

		return "views/reports/MetaDataReport";
	}

	@RequestMapping(value = "/reports/DepartmentWiseMetaDataReport", method = RequestMethod.GET)
	public String DepartmentWiseMetaDataReport() {

		return "views/reports/DepartmentWiseMetaDataReport";
	}

	@RequestMapping(value = "/reports/QCReport", method = RequestMethod.GET)
	public String QCReport() {

		return "views/reports/QCReport";
	}

	@RequestMapping(value = "/reports/StatusReport", method = RequestMethod.GET)
	public String StatusReport() {

		return "views/reports/StatusReport";
	}

	@RequestMapping(value = "/reports/DownloadReport", method = RequestMethod.GET)
	public String DownloadReport() {

		return "views/reports/DownloadReport";
	}

	@RequestMapping(value = "/Reports/getAllUsers", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getAllUsers() {
		List<User> repo = new ArrayList<User>();

		try {
			repo = userservice.getAllUser();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String result = cm.convert_to_json(repo);
		return result;
	}

	@RequestMapping(value = "/Reports/getLoginData", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getLoginData(HttpServletRequest request, HttpSession session)
			throws NullPointerException {
		String jsonData = null;
		String um_id = request.getParameter("um_id");
		String fromDate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		User user = (User) session.getAttribute("USER");

		List Logindata = ReportServices.getLoginData(um_id, fromDate, todate);

		response.setModelList(Logindata);
		if (Logindata != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/Reports/getUploadedData", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getUploadedDate(HttpServletRequest request, HttpSession session)
			throws NullPointerException {
		String jsonData = null;
		String dept_id = request.getParameter("dept_id");
		String fromDate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		ActionResponse<DocumentFileDetails> response = new ActionResponse<DocumentFileDetails>();
		// DailyActivityReport dar=new DailyActivityReport();
		User user = (User) session.getAttribute("USER");

		List<DocumentFileDetails> Uploadeddata = ReportServices.getUploadedData(dept_id, fromDate, todate);

		response.setModelList(Uploadeddata);
		if (Uploadeddata != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/Reports/", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getMetaEntryData(HttpServletRequest request, HttpSession session)
			throws NullPointerException {
		String jsonData = null;
		String dept_id = request.getParameter("dept_id");
		String fromDate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		ActionResponse<DailyActivityReport> response = new ActionResponse<DailyActivityReport>();
		// DailyActivityReport dar=new DailyActivityReport();
		User user = (User) session.getAttribute("USER");

		List Uploadeddata = ReportServices.getMetaEntryData(dept_id, fromDate, todate);
		// System.out.println(Uploadeddata.get(0).toString());
		response.setModelList(Uploadeddata);
		if (Uploadeddata != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/Reports/getQCData", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getQCData(HttpServletRequest request, HttpSession session) throws NullPointerException {
		String jsonData = null;
		String dept_id = request.getParameter("dept_id");
		String fromDate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		ActionResponse<DailyActivityReport> response = new ActionResponse<DailyActivityReport>();
		// DailyActivityReport dar=new DailyActivityReport();
		User user = (User) session.getAttribute("USER");

		List Uploadeddata = ReportServices.getQCDataData(dept_id, fromDate, todate);
		System.out.println(Uploadeddata.get(0).toString());
		response.setModelList(Uploadeddata);
		if (Uploadeddata != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/Reports/getStatusData", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getStatusData(HttpServletRequest request, HttpSession session)
			throws NullPointerException {
		String jsonData = null;
		String dept_id = request.getParameter("dept_id");
		String fromDate = request.getParameter("fromdate");

		ActionResponse<DailyActivityReport> response = new ActionResponse<DailyActivityReport>();
		DailyActivityReport dar = new DailyActivityReport();
		User user = (User) session.getAttribute("USER");
		Integer Upload_cnt = ReportServices.getUploadCount(dept_id, fromDate);
		Integer Meta_cnt = ReportServices.getMetaCount(dept_id, fromDate);
		Integer QC_cnt = ReportServices.getQCCount(dept_id, fromDate);
		Integer pageCount = ReportServices.getPageCount(dept_id, fromDate);
		System.out.println(Upload_cnt + " " + Meta_cnt + " " + QC_cnt + " " + pageCount);
		dar.setValue1("" + Upload_cnt);
		dar.setValue2("" + Meta_cnt);
		dar.setValue3("" + QC_cnt);
		dar.setValue4("" + pageCount);
		response.setModelData(dar);
		if (dar != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/report/Getfilename", method = RequestMethod.GET)
	public @ResponseBody String Getfilename(HttpServletRequest request) {

		String dfd_id = request.getParameter("dfd_id");
		System.out.println("barcode" + dfd_id);
		String jsonData = null;
		ActionResponse<DocumentFileDetails> response = new ActionResponse();
		Lookup lukUp = lkService.getLookUpObject("REPOSITORYPATH");
		DocumentFileDetails dfd = documentfiledetailsService.getById(dfd_id);
		System.out.println(dfd.getDfd_file_name());
		File src = new File(lukUp.getLk_longname() + "/" + dfd.getDepartments().getDept_name() + "/"
				+ dfd.getSubdepartments().getSub_dept_name() + "/" + dfd.getDfd_year() + "/" + dfd.getDfd_file_name());
		String pth = request.getContextPath();
		// System.out.println("PDF PATH :
		// "+lukUp.getLk_longname()+"/"+dfd.getDfd_barcode()+".pdf");
		String uplodPath = context.getRealPath("");
		File destination = new File(uplodPath + "/uploads/" + dfd.getDfd_file_name());
		// System.out.println("VIEW PDF PATH :
		// "+uplodPath+"/uploads/"+dfd.getDfd_barcode()+".pdf");

		try {
			FileUtils.copyFile(src, destination);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonData;
	}

	@RequestMapping(value = " /report/getDepartmentwiseDocument", method = RequestMethod.GET)
	public @ResponseBody String getDeptWiseDocument(HttpServletRequest request) {
		String dept_name = request.getParameter("dept_name");
		System.out.println("barcode" + dept_name);
		String jsonData = null;
		ActionResponse<DocumentFileDetails> response = new ActionResponse();
		Lookup lukUp = lkService.getLookUpObject("REPOSITORYPATH");
		List<DocumentFileDetails> dfd = documentfiledetailsService.getDataByDeptName(dept_name);
		System.out.println(dfd.get(0).getDfd_file_name());
		response.setModelList(dfd);
		if (dfd != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/getPathServlet", method = RequestMethod.GET)
	public @ResponseBody String getpath(HttpServletRequest req) {
		String str = "durga";
		try {
			ServletContext context = req.getServletContext();
			str = context.getRealPath("/");
		} catch (Exception e) {

		}
		return str;
	}

	@RequestMapping(value = "/Reports/getDepartmentWiseMetaData", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getDepartmentWiseMetaData(HttpServletRequest request) throws NullPointerException {
		String jsonData = null;
		String fromDate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");
		String dept_id = request.getParameter("dept_id");
		String sub_dept_id = request.getParameter("sub_dept_id");

		ActionResponse<DailyActivityReport> response = new ActionResponse<DailyActivityReport>();
		List mdCountReporytList = ReportServices.getDepartmentWiseMetaData(sub_dept_id, dept_id, fromDate, todate);
		response.setModelList(mdCountReporytList);
		if (mdCountReporytList != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

	@RequestMapping(value = "/Reports/getAllDepartments", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getDepartments() {
		List<Department> repo = new ArrayList<Department>();

		try {
			repo = service.getDepartments();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String result = cm.convert_to_json(repo);
		return result;
	}

	@RequestMapping(value = "/Reports/getDownloadData", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getDownloadData(HttpServletRequest request, HttpSession session)
			throws NullPointerException {
		String jsonData = null;
		String um_id = request.getParameter("um_id");
		String fromDate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		ActionResponse<DailyActivityReport> response = new ActionResponse();
		DailyActivityReport dar = new DailyActivityReport();
		User user = (User) session.getAttribute("USER");

		List downloaddata= ReportServices.getDownloadData(um_id, fromDate, todate);

		response.setModelList(downloaddata);
		if (downloaddata != null) {
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);
		} else {
			response.setResponse("FALSE");
		}
		return jsonData;
	}

}
