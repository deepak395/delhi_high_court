package com.mdi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.Inet4Address;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.queryParser.ParseException;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.mdi.dto.Index_search;
import com.mdi.dto.MetaDataSearch;
import com.mdi.dto.SearchForm;
import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.Document_type;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.MetaData;
import com.mdi.model.ReportsView;
import com.mdi.model.Repository;
import com.mdi.model.SearchCriteria;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.model.ViewLog;
import com.mdi.service.DocumentFileDetailsService;
import com.mdi.service.LookupService;
import com.mdi.service.MetaDataService;
import com.mdi.service.MetaTemplateService;
import com.mdi.service.SearchService;
import com.mdi.service.UploadDocumentService;
import com.mdi.utility.GlobalFunction;
import com.mdi.utility.IndexItem;
import com.mdi.utility.Indexer;
import com.mdi.utility.Searcher;

@Controller
public class SearchController {

	Logger logger = Logger.getLogger("MyLog");
	FileHandler fh = null;

	static List<String> list = null;
	private GlobalFunction globalFunction;

	public SearchController() {

		globalFunction = new GlobalFunction();
	}

	@Autowired
	private SearchService searchservice;

	@Autowired
	private UploadDocumentService Upservice;

	@Autowired
	private LookupService lookupService;

	@Autowired
	private MetaDataService metaservice;

	@Autowired
	public MetaTemplateService metatemplateService;

	@Autowired
	private CommonController common;

	@Autowired
	private DocumentFileDetailsService dfdservice;

	private String query = "";
	private List<Long> doc_ids = new ArrayList<Long>();

	GlobalFunction global = new GlobalFunction();

	private static final int DEFAULT_RESULT_SIZE = 100;

	@RequestMapping(value = "search/getrepositories", method = RequestMethod.GET)
	public @ResponseBody String getRepositories() {
		String jsonData = "";
		List<Repository> repositories = searchservice.getRepositories();
		jsonData = globalFunction.convert_to_json(repositories);
		return jsonData;
	}

	/********************* Content Search *****************************/
	@RequestMapping(value = "document/searchdocument", method = RequestMethod.POST)
	public @ResponseBody String doContentSearch(@RequestBody SearchForm searchForm, HttpSession session,
			@RequestParam("s_d_id") Long s_dept_id, @RequestParam("year") Long year, @RequestParam("de_id") Long deptid,
			@RequestParam("dt_id") Long dt_id, HttpServletRequest request) {

		String jsonData = "";
		this.query = "";
		ActionResponse<DocumentFileDetails> response = new ActionResponse<DocumentFileDetails>();

		Lookup look = lookupService.getLookUpObject("LUCENE_DIR");

		/************ Depts *******/
		SubDepartment sub_dept = null;
		Department dept = null;
		Document_type doc_type = null;

		/********** Conditions *********/
		String dir_path = look.getLk_longname();
		System.out.println(dir_path + "    directory");
		try {

			if (s_dept_id != 0l && year != 0l && deptid != 0l && dt_id != 0l) // all
																				// fields
			{

				sub_dept = searchservice.getSubDeptById(s_dept_id);

				dept = searchservice.getDeptById(deptid);

				doc_type = Upservice.getDocumentTypeById(dt_id);

				dir_path = dir_path + File.separator + dept.getDept_name() + File.separator
						+ sub_dept.getSub_dept_name() + "/" + doc_type.getDt_name() + File.separator + year;
				System.out.println(dir_path + " durga");

			} else if (s_dept_id == 0l && year == 0l && deptid != 0l && dt_id == 0l) // dept
			{

				dept = searchservice.getDeptById(deptid);
				dir_path = dir_path + File.separator + dept.getDept_name();

			} else if (s_dept_id != 0l && year == 0l && deptid != 0l && dt_id == 0l) // dept
																						// sub_dept
			{
				sub_dept = searchservice.getSubDeptById(s_dept_id);
				dept = searchservice.getDeptById(deptid);
				dir_path = dir_path + File.separator + dept.getDept_name() + File.separator
						+ sub_dept.getSub_dept_name();

			} else if (s_dept_id != 0l && year == 0l && deptid != 0l && dt_id != 0l) // dept
																						// sub_dept
																						// doc_type
			{
				sub_dept = searchservice.getSubDeptById(s_dept_id);
				dept = searchservice.getDeptById(deptid);
				doc_type = Upservice.getDocumentTypeById(dt_id);
				dir_path = dir_path + File.separator + dept.getDept_name() + File.separator
						+ sub_dept.getSub_dept_name() + File.separator + doc_type.getDt_name();
			} else // none
			{

			}
		} catch (Exception e) {

		}

		List<SearchCriteria> searchlist = searchForm.getSearchlist();
		String querystring = "";
		querystring += this.query;
		List<DocumentFileDetails> dfd = new ArrayList<>();

		if (searchlist != null && searchlist.size() > 0) {
			for (SearchCriteria searchcriteria : searchlist) {
				try {
					// System.out.println(searchcriteria.getCriteria()+"
					// "+searchcriteria.getSearchtext());
					searchContentInPDF(searchcriteria, dir_path, request);
					querystring += this.query;
					// System.out.println(querystring);
					dfd = searchservice.getcaseFilesBySearchqueryContent(querystring);

					// System.out.println(dfd.size()+" docfile Size");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		response.setResponse("TRUE");

		response.setModelList(dfd);

		jsonData = globalFunction.convert_to_json(response);
		System.out.println("jsonData=globalfunction.convert_to_json(response); " + jsonData);

		return jsonData;
	}

	private void searchContentInPDF(SearchCriteria searchcriteria, String dir_path, HttpServletRequest request) {

		try {
			createIndex(request);
		} catch (IOException e) {

			e.printStackTrace();
		}

		/* String operator = searchcriteria.getOperator(); */
		String searchtext = searchcriteria.getSearchtext();
		String[] searchkeywords = searchtext.split(",");
		Set<Long> documentids = new HashSet<>();

		list = new ArrayList<>();
		try {
			getDirectories(dir_path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (String str : searchkeywords) {
			List<List<IndexItem>> inlists = new ArrayList<>();

			for (String dir : list) {
				List<IndexItem> results = new ArrayList<IndexItem>();
				try {
					Searcher searcher = new Searcher(dir);
					results = searcher.findByContent(str, DEFAULT_RESULT_SIZE);
					searcher.close();
					System.out.println(results.size() + "  result");
					if (results.size() > 0)
						inlists.add(results);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			if (list.size() < 1) {
				List<IndexItem> results = new ArrayList<IndexItem>();
				try {
					Searcher searcher = new Searcher(dir_path);
					results = searcher.findByContent(str, DEFAULT_RESULT_SIZE);
					searcher.close();
					System.out.println(results.size() + "  result");
					if (results.size() > 0)
						inlists.add(results);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			if (!inlists.isEmpty()) {
				for (List<IndexItem> ix : inlists) {
					for (IndexItem item : ix) {

						documentids.add(item.getId());

					}
				}

			}
		}

		System.out.println("Docids1=" + globalFunction.convert_to_json(documentids) + "  durgendra");
		this.query = "";
		if (!documentids.isEmpty()) {

			if (this.query.equals("")) {
				// operator = "";
			}
			String str = "";

			for (Long id : documentids) {
				str = str + "" + id + ",";
			}
			str = str.substring(0, str.length() - 1);
			System.out.println(this.query + "durga");
			// this.query += " " + operator + " dfd_id in (" + str + ")";
			this.query += "dfd_id in (" + str + ")";

			System.out.println("searchincontentquery = " + this.query);
		}

	}

	// @RequestMapping(value = "document/searchdocument", method =
	// RequestMethod.POST)
	public @ResponseBody String searchdocument(@RequestBody SearchForm searchForm, HttpSession session,
			@RequestParam("s_d_id") Long s_dept_id, @RequestParam("year") Long year, @RequestParam("de_id") Long deptid,
			HttpServletRequest request) throws ParseException {

		String jsonData = "";
		this.query = "";
		this.doc_ids = new ArrayList<Long>();
		ActionResponse<DocumentFileDetails> response = new ActionResponse<DocumentFileDetails>();

		System.out.println(s_dept_id + "   hhh   " + year + "    " + deptid);
		SubDepartment sub_dept = null;
		Department dept = null;
		String field_status = "";
		String path = "";
		String dir_path = "";
		Lookup look = lookupService.getLookUpObject("LUCENE_DIR");
		if (s_dept_id != 0l && year != 0l && deptid != 0l) {
			System.out.println("with all");
			sub_dept = searchservice.getSubDeptById(s_dept_id);

			dept = searchservice.getDeptById(sub_dept.getParent_id());

			Lookup lk = lookupService.getLookUpObject("REPOSITORYPATH");

			dir_path = dept.getDept_name() + "/" + sub_dept.getSub_dept_name() + "/" + year + "/";
			field_status = "ALLCRITERIA";
		} else if (s_dept_id == 0l && year == 0l && deptid != 0l) {
			field_status = "MISSINGFIELD";
			path = "withDept";
			dir_path = look.getLk_longname().substring(0, look.getLk_longname().length() - 1);
			dept = searchservice.getDeptById(deptid);
			dir_path = dir_path + "\\" + dept.getDept_name();

			System.out.println("/****** withDept********/" + dir_path);
		} else if (s_dept_id != 0l && year == 0l && deptid != 0l) {
			field_status = "MISSINGFIELD";
			path = "withSubdept and dept";
			sub_dept = searchservice.getSubDeptById(s_dept_id);

			dept = searchservice.getDeptById(deptid);
			dir_path = look.getLk_longname().substring(0, look.getLk_longname().length() - 1);
			dir_path = dir_path + "\\" + dept.getDept_name() + "\\" + sub_dept.getSub_dept_name();
			System.out.println("/********with sub dept and dept ********/" + dir_path);
		} else {
			field_status = "MISSINGFIELD";
			path = "no fields";
			dir_path = look.getLk_longname().substring(0, look.getLk_longname().length() - 1);
			System.out.println("dir_path" + dir_path);

			/*
			 * if(s_dept_id.equals(null) || s_dept_id.equals(0l) ) { dept =
			 * searchservice.getDeptById(deptid); } else if(!s_dept_id.equals(0l)) {
			 * sub_dept = searchservice.getSubDeptById(s_dept_id); dept =
			 * searchservice.getDeptById(deptid); withsub_dept="withsub_dept";
			 * System.out.println("sub dept durga"); }
			 */
		}

		searchForm.setSub_dept_id(s_dept_id);
		searchForm.setYear(year);

		List<SearchCriteria> searchlist = searchForm.getSearchlist();

		System.out.println(searchlist.size() + "mmmm");

		/*
		 * System.out.println("searchlist" + searchlist.get(0).getSub_dept_id()); Long
		 * dept_id = searchlist.get(0).getSub_dept_id();
		 */
		System.out.println("");
		String querystring = "";

		querystring += this.query;
		System.out.println("Querystring=" + querystring);
		System.out.println("Docids=" + globalFunction.convert_to_json(this.doc_ids));

		List<ReportsView> reportsData = null;
		List<DocumentFileDetails> dfd = new ArrayList<>();

		/*
		 * if(folderid!=134){ folderids= documentService.getFoldersIds(folderid);
		 * 
		 * }
		 */

		if (null != searchlist && searchlist.size() > 0) {

			for (SearchCriteria searchcriteria : searchlist) {
				/*
				 * if(searchcriteria.getType().equals("metafields")) { addquery(searchcriteria);
				 * querystring+=this.query; reportsData
				 * =documentService.getcaseFilesBySearchquery(querystring ,stagelid,folderids);
				 * }
				 */

				// content search
				// if(searchcriteria.getType().equals("searchincontent"))
				// {
				try {
					searchInContent(searchcriteria, dir_path, field_status, request); // content
																						// search
					// code method
					// lucening
					querystring += this.query;
					dfd = searchservice.getcaseFilesBySearchqueryContent(querystring);
					System.out.println(dfd.size() + "                      docfile size");

					// reportsData=searchservice.getcaseFilesBySearchqueryContent(querystring,stagelid);
				} catch (IOException e) {

					e.printStackTrace();
				}
				// }
			}

		}

		response.setResponse("TRUE");

		response.setModelList(dfd);

		jsonData = globalFunction.convert_to_json(response);
		System.out.println("jsonData=globalfunction.convert_to_json(response); " + jsonData);

		return jsonData;
	}

	// search content
	@RequestMapping(value = "/document/searchincontent", method = RequestMethod.GET)
	public @ResponseBody String searchInContent(SearchCriteria searchcriteria, String dir_path, String field_status,
			HttpServletRequest request) throws IOException, ParseException {

		String jsonData = "";
		String operator = searchcriteria.getOperator();
		System.out.println("=================");
		String searchtext = searchcriteria.getSearchtext();
		String[] searchkeywords = searchtext.split(",");
		List<Long> documentids = new ArrayList<Long>();

		List<Long> doc_idss = new ArrayList<>();

		try {
			createIndex(request);
		} catch (IOException e) {

			e.printStackTrace();
		}

		Lookup lk = lookupService.getLookUpObject("LUCENE_DIR");

		if (field_status.equals("MISSINGFIELD")) {
			list = new ArrayList<>();
			/*
			 * String dir_path
			 * =lk.getLk_longname().substring(0,lk.getLk_longname().length()-1);
			 * if(withsub_dept.equals("withsub_dept")) {
			 * dir_path=dir_path+"\\"+dept.getDept_name()+"\\"+sub_dept. getSub_dept_name();
			 * } else { dir_path = dir_path+"\\"+dept.getDept_name(); }
			 */
			// System.out.println(dir_path+"\\"+dept.getDept_name()+"\\"+sub_dept.getSub_dept_name()+"
			// sssssssssss");
			getDirectories(dir_path);

		}

		for (String str : searchkeywords) {
			List<List<IndexItem>> inlists = new ArrayList<>();

			if (field_status.equals("MISSINGFIELD")) {

				for (String dir : list) {
					List<IndexItem> results = new ArrayList<IndexItem>();
					try {
						Searcher searcher = new Searcher(dir);
						results = searcher.findByContent(str, DEFAULT_RESULT_SIZE);
						searcher.close();
						System.out.println(results.size() + "  result");
						if (results.size() > 0)
							inlists.add(results);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}

			System.out.println(inlists.size() + "  inlists");

			List<IndexItem> results = new ArrayList<IndexItem>();
			if (!field_status.equals("MISSINGFIELD"))
				try {

					Searcher searcher = new Searcher(lk.getLk_longname() + File.separator + File.separator + dir_path);
					results = searcher.findByContent(str, DEFAULT_RESULT_SIZE);
					searcher.close();
				} catch (org.apache.lucene.queryParser.ParseException e) {

					e.printStackTrace();
				}

			if (!results.isEmpty()) {
				for (IndexItem item : results) {

					documentids.add(item.getId());

				}

			}
			if (!inlists.isEmpty()) {
				for (List<IndexItem> ix : inlists) {
					for (IndexItem item : ix) {

						doc_idss.add(item.getId());

					}
				}

			}

		}

		if (doc_idss.isEmpty())
			if (!documentids.isEmpty()) {

				if (this.query.equals("")) {
					operator = "";
				}
				String str = "";

				for (Long id : documentids) {
					str = str + "" + id + ",";
				}
				str = str.substring(0, str.length() - 1);
				this.query += " " + operator + " dfd_id in (" + str + ")";

				System.out.println("searchincontentquery=" + this.query);
			}

		if (!doc_idss.isEmpty()) {
			if (this.query.equals("")) {
				operator = "";
			}
			String str = "";

			for (Long id : doc_idss) {
				str = str + "" + id + ",";
			}
			str = str.substring(0, str.length() - 1);
			this.query += " " + operator + " dfd_id in (" + str + ")";

			System.out.println("searchincontentquery=" + this.query + " durgendra");
		}

		return jsonData;
	}

	@RequestMapping(value = "/document/createindex", method = RequestMethod.GET)
	public @ResponseBody String createIndex(HttpServletRequest request) throws IOException {
		String jsonData = "";

		try {
			ServletContext context = request.getServletContext();
			String path = context.getRealPath("/");
			fh = new FileHandler(path + "//logs//" + "MyLogFile.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
		} catch (SecurityException e1) {

			e1.printStackTrace();
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		List<DocumentFileDetails> documentfile = new ArrayList<DocumentFileDetails>();
		documentfile = searchservice.getAllWithoutIndex();

		Lookup lk = lookupService.getLookUpObject("LUCENE_DIR");

		Lookup lucene_temp = lookupService.getLookUpObject("LUCENE_TEMP_DIR");

		for (DocumentFileDetails documentfiles : documentfile) {

			logger.info("Check point 1 : SearchController filename " + documentfiles.getDfd_file_name() + "   "
					+ documentfiles.getDfd_dt_mid());
			// Repository
			// repository=repositoryService.getRepository(documentfiles.getDfd_rep_mid());
			Document_type doc_type = Upservice.getDocumentTypeById(documentfiles.getDfd_dt_mid());
			String path = common.getPathToViewDocument(documentfiles.getDfd_dept_mid(),
					documentfiles.getDfd_sub_dept_mid(), documentfiles.getDfd_year(), doc_type.getDt_name());

			/*************************************************************************/
			// Document_type doc_type =
			// Upservice.getDocumentTypeById(dfd.getDfd_dt_mid());
			System.out.println(doc_type.getDt_name());
			String path2 = "";
			try {
				System.out.println(documentfile);
				if (documentfiles.getDfd_year() != 1001l && documentfiles.getDfd_year() != 1000l
						&& documentfiles.getDfd_fd_mid() == 0l) {
					path2 = common.getPathToViewDocument(documentfiles.getDfd_dept_mid(),
							documentfiles.getDfd_sub_dept_mid(), documentfiles.getDfd_year(), doc_type.getDt_name());
				} else if (documentfiles.getDfd_year() == 1000l && documentfiles.getDfd_fd_mid() != 0l) {
					// in folder
					Folder folder = Upservice.getFolderById(documentfiles.getDfd_fd_mid());
					path2 = common.getPathToViewDocument(documentfiles.getDfd_dept_mid(),
							documentfiles.getDfd_sub_dept_mid(), folder.getFolder_name(), doc_type.getDt_name());
				} else if (documentfiles.getDfd_year() == 1001l && documentfiles.getDfd_fd_mid() == 0l) {
					// in doc type
					path2 = common.getPathToViewDocument(documentfiles.getDfd_dept_mid(),
							documentfiles.getDfd_sub_dept_mid(), doc_type.getDt_name());
				}

			} catch (Exception e) {
				logger.info("Check point 1 : SearchController filename " + e);
				e.printStackTrace();
			}
			/**********************************************/
			// Repository
			// rep=repositoryService.getRepositoryById(documentfiles.getDfd_rep_mid());

			// Not Folder
			// folder=folderService.getFolderById(document.getFolder_id());

			// Department department = metatemplateService
			// .getAllDepartMentById(documentfiles.getDfd_dept_mid());

			// String
			// repBasepath=repository.getBasepath()+File.separator+repository.getName();

			/*
			 * Not String folderPath=""; if(department.getParent_id()! =null){ Long
			 * parent_id=folder.getParent_id(); while(parent_id !=null){ Folder
			 * fd=folderService.getFolderById(parent_id); folderPath =
			 * fd.getFolder_name()+File.separator+folderPath; parent_id = fd.getParent_id();
			 * } } String basePath=repBasepath+File.separator+
			 * folderPath+folder.getFolder_name(); System.out.println("basepath="+basePath);
			 */

			SubDepartment sub_dept = searchservice.getSubDeptById(documentfiles.getDfd_sub_dept_mid());

			Department dept = searchservice.getDeptById(sub_dept.getParent_id());

			Lookup lk1 = lookupService.getLookUpObject("REPOSITORYPATH");

			String path1 = "";

			if (documentfiles.getDfd_year() != 1001l && documentfiles.getDfd_year() != 1000l
					&& documentfiles.getDfd_fd_mid() == 0l) {
				path1 = dept.getDept_name() + "/" + sub_dept.getSub_dept_name() + "/" + doc_type.getDt_name() + "/"
						+ documentfiles.getDfd_year() + "/";
			}

			else if (documentfiles.getDfd_year() == 1000l && documentfiles.getDfd_fd_mid() != 0l) {
				// in folder
				Folder folder = Upservice.getFolderById(documentfiles.getDfd_fd_mid());
				path1 = dept.getDept_name() + "/" + sub_dept.getSub_dept_name() + "/" + doc_type.getDt_name() + "/"
						+ folder.getFolder_name() + "/";
			} else if (documentfiles.getDfd_year() == 1001l && documentfiles.getDfd_fd_mid() == 0l) {
				// in doc type
				path1 = dept.getDept_name() + "/" + sub_dept.getSub_dept_name() + "/" + doc_type.getDt_name() + "/";
			}
			File source = new File(path2 + File.separator + documentfiles.getDfd_file_name());

			String destfilename = lucene_temp.getLk_longname() + path1 + documentfiles.getDfd_file_name();

			File dest = new File(lucene_temp.getLk_longname() + path1 + documentfiles.getDfd_file_name());

			try {
				try {
					FileUtils.copyFile(source, dest);

					File pdfFile = new File(destfilename);
					if (pdfFile.exists()) {
						IndexItem pdfIndexItem = index(pdfFile, documentfiles.getDfd_id());
						Indexer indexer = new Indexer(lk.getLk_longname() + "/" + path1);
						indexer.index(pdfIndexItem);
						indexer.close();

						documentfiles.setDfd_index(1);
						searchservice.save(documentfiles);

						pdfFile.delete();
					}

				} catch (Throwable e) {
					logger.info("Check point 1 : SearchController filename " + e);
					e.printStackTrace();
				}
			} catch (Exception e2) {
				logger.info("Check point 1 : SearchController filename " + e2);
				e2.printStackTrace();
			}

		}
		return "Indexing Done";
	}

	// Extract text from PDF document
	public static IndexItem index(File file, Long doc_id) throws IOException {
		PDDocument doc = PDDocument.load(file);
		String content = new PDFTextStripper().getText(doc);
		doc.close();
		return new IndexItem(doc_id, file.getName(), content);
	}

	/************** Search by meta fields **************/

	@RequestMapping("/search/indexfieldname")
	public @ResponseBody String getindexfieldnames(@RequestParam("dt_id") Long dt_id) {
		String result = "";
		List<String> list = new ArrayList<>();
		List<IndexField> repo = new ArrayList<IndexField>();
		List<Index_search> list2 = new ArrayList<>();

		try {
			repo = metaservice.getindexfields(dt_id);
			for (IndexField in : repo) {
				Index_search ins = new Index_search();
				ins.setIf_id(in.getIf_id());
				ins.setIf_name(in.getIf_name());
				ins.setIf_type(in.getIf_type());
				list2.add(ins);
			}

			list = searchservice.getindexfields(dt_id);
			result = global.convert_to_json(list2);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/search/getindexdropdownlookupdata", method = RequestMethod.GET)
	public @ResponseBody String getindexdropdownlookupdata(@RequestParam("if_id") Long if_id) {
		String result = "";
		List<Lookup> li_look = new ArrayList<>();
		try {
			IndexField in = searchservice.getindexfieldsById(if_id);

			li_look = metaservice.getLookupByName(in.getIf_name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		result = global.convert_to_json(li_look);
		return result;
	}

	@RequestMapping(value = "/search/metadataForSearch", method = RequestMethod.POST)
	public @ResponseBody String metadataForSearch(@RequestBody List<MetaDataSearch> md,
			@RequestParam("sub_dept_id") Long sub_dept_id, @RequestParam("dept_id") Long dept_id,
			@RequestParam("dt_id") Long dt_id, @RequestParam("md_year") Long md_year,
			@RequestParam("fd_id") Long fd_id) {

		System.out.println("folder id " + fd_id);

		ActionResponse<DocumentFileDetails> response = new ActionResponse<>();
		System.out.println(dept_id + " " + sub_dept_id);

		System.out.println(md_year);
		/**********************************/
		int index = 0;
		for (MetaDataSearch m : md) {
			try {
				DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
						Locale.ENGLISH);
				DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
				LocalDate date = LocalDate.parse(m.getIf_value(), inputFormatter);
				String formattedDate = outputFormatter.format(date);
				String[] str = formattedDate.split("-");
				String str2 = "" + (Integer.parseInt(str[2]) + 1);
				if (str2.length() < 2) {
					str2 = "0" + str2;
				}
				String fi = str[0] + "-" + str[1] + "-" + str2;
				System.out.println(formattedDate);
				md.get(index).setIf_value(fi);

			} catch (Exception e) {

			}
			index++;
		}

		/***************************/

		List<DocumentFileDetails> dfd = new ArrayList<>();
		if (sub_dept_id == 0)
			sub_dept_id = null;

		try {
			dfd = searchservice.getSearchFiles(md, dept_id, sub_dept_id, dt_id, md_year, fd_id);
			System.out.println(dfd.size());
			response.setResponse("TRUE");
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		response.setModelList(dfd);
		String result = global.convert_to_json(response);
		return result;

	}

	@RequestMapping(value = "/search/viewFile", method = RequestMethod.GET)
	public @ResponseBody String viewFile(HttpServletRequest request, HttpSession session) {
		Long dfd_id = Long.parseLong(request.getParameter("dfd_id"));
		ActionResponse<DocumentFileDetails> response = new ActionResponse<>();
		String result = "";
		User user = (User) session.getAttribute("USER");
		try {
			DocumentFileDetails dfd = metaservice.getDocumentFileDetailsById(dfd_id);
			ServletContext context = request.getServletContext();
			String uploadpath = context.getRealPath("/");
			try {
				Document_type doc_type = Upservice.getDocumentTypeById(dfd.getDfd_dt_mid());
				System.out.println(doc_type.getDt_name());
				String path = "";
				if (dfd.getDfd_year() != 1001l && dfd.getDfd_year() != 1000l && dfd.getDfd_fd_mid() == 0l) {
					path = common.getPathToViewDocument(dfd.getDfd_dept_mid(), dfd.getDfd_sub_dept_mid(),
							dfd.getDfd_year(), doc_type.getDt_name());
				} else if (dfd.getDfd_year() == 1000l && dfd.getDfd_fd_mid() != 0l) {
					// in folder
					Folder folder = Upservice.getFolderById(dfd.getDfd_fd_mid());
					path = common.getPathToViewDocument(dfd.getDfd_dept_mid(), dfd.getDfd_sub_dept_mid(),
							folder.getFolder_name(), doc_type.getDt_name());
				} else if (dfd.getDfd_year() == 1001l && dfd.getDfd_fd_mid() == 0l) {
					// in doc type
					path = common.getPathToViewDocument(dfd.getDfd_dept_mid(), dfd.getDfd_sub_dept_mid(),
							doc_type.getDt_name());
				} else if (dfd.getDfd_year() != 1001l && dfd.getDfd_year() != 10001l && dfd.getDfd_fd_mid() != 0l) {
					// in doc type
					path = common.getPathToViewDocument(dfd.getDfd_dept_mid(), dfd.getDfd_sub_dept_mid(),
							dfd.getDfd_year(), doc_type.getDt_name());
				}

				if (path != null) {
					System.out.println("Path " + path);
					PdfReader reader = new PdfReader(path + File.separator + dfd.getDfd_file_name(),
							"allahabad".getBytes());
					PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(
							uploadpath + File.separator + "uploads" + File.separator + dfd.getDfd_file_name()));
					stamper.close();
					reader.close();
					response.setData(dfd.getDfd_file_name());
					response.setModelData(dfd);
					String ip_address = Inet4Address.getLocalHost().getHostAddress();
					ViewLog viewlog = new ViewLog();
					viewlog.setVl_dfd_mid(dfd.getDfd_id());
					viewlog.setVl_user_mid(user.getUm_id());
					viewlog.setVl_view_time(new Date());
					viewlog.setVl_ip_address(ip_address);
					dfdservice.saveViewLog(viewlog);
				}
				response.setResponse("TRUE");

			} catch (Exception e) {
				e.printStackTrace();
				response.setResponse("FALSE");
			}
		} catch (Exception e) {
			response.setResponse("FALSE");
			e.printStackTrace();
		}
		System.out.println("response " + response);
		result = global.convert_to_json(response);
		return result;
	}

	public static void getDirectories(String name) {
		File file = new File(name);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});

		if (directories != null) {
			for (String str : directories) {

				String name2 = name + "/" + str;
				list.add(name2);
				getDirectories(name2);
			}
		}

	}

	@RequestMapping(value = "/search/MetaData", method = RequestMethod.GET)
	public @ResponseBody String metadataForSearch(@RequestParam("dfd_id") Long dfd_id) {
		ActionResponse<MetaData> response = new ActionResponse<>();
		List<MetaData> md = new ArrayList<>();
		try {
			md = metaservice.getMetaDataByDfd(dfd_id);
			System.out.println(md.size());
			response.setResponse("TRUE");
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		response.setModelList(md);
		String result = global.convert_to_json(response);
		return result;

	}

}
