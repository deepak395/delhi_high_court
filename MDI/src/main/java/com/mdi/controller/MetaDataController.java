package com.mdi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.mdi.dto.DocumentCompleteData;
import com.mdi.dto.Index_Meta_Data;
import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.Document;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.DocumentFileStage;
import com.mdi.model.Document_type;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.MetaData;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.model.ViewLog;
import com.mdi.service.DocumentFileDetailsService;
import com.mdi.service.DocumentFileStageService;
import com.mdi.service.FolderService;
import com.mdi.service.LookupServices;
import com.mdi.service.MetaDataService;
import com.mdi.service.UploadDocumentService;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;

@Controller
public class MetaDataController {
	
	@Autowired
	private MetaDataService service;
	
	@Autowired
	private UploadDocumentService  Upservice;
	
	@Autowired
	private FolderService folderservice;
	
	@Autowired
	private CommonController common;
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private DocumentFileDetailsService dfdservice;
	
	@Autowired
	private LookupServices lookupservices;
	
	@Autowired
	private DocumentFileStageService docuumetFileStageService;
	
	CommenMethods cm;
	
	public MetaDataController()
	{
		cm= new CommenMethods();
	}

	@RequestMapping("metadata/metadata")
	public String MetaData()
	{
		return "views/metadata/metadata";
	}
	
	@RequestMapping(value="/metadata/getAllDepartments",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getDepartments()
	{
		List<Department> repo= new ArrayList<Department>();
		
		try 
		{
			repo=service.getDepartments();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	@RequestMapping(value="/metadata/getSubDept",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getSubDept(HttpSession session,@RequestParam(value="dept_id")Long dept_id)
	{
		List<SubDepartment> repo= new ArrayList<SubDepartment>();
		
		try 
		{
			User user =(User)session.getAttribute("USER");
			List<Long> sub_d = service.getSubDeptIdsPerMission(user);
			System.out.println(sub_d+" ddd");
			repo=service.getSubDept(dept_id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	
	@RequestMapping(value="/metadata/getindexfields",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getIndexFields(HttpSession session,@RequestParam(value="sub_dept_id")Long sub_dept_id)
	{
		ActionResponse<IndexField> response = new ActionResponse<>();
		List<IndexField> repo= new ArrayList<IndexField>();
		
		try 
		{
			repo=service.getindexfields(sub_dept_id);
			Comparator<IndexField> com = new Comparator<IndexField>() 
			{

				@Override
				public int compare(IndexField o1, IndexField o2) {
					
					return o1.getIf_name().compareTo(o2.getIf_name());
				}
				
			};
			Collections.sort(repo,com);
			boolean show = true;
			for(IndexField in : repo)
			{
				if(in.getIf_add_multiple()==1)
				{
					show = false;
				}
			}
			response.setData(show);
			response.setModelList(repo);
			response.setResponse("TRUE");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		
		String result=cm.convert_to_json(response);
		return result;
	}
	
	@RequestMapping(value="/metadata/getfielddata",method=RequestMethod.GET,headers = "Accept=application/json")
	public @ResponseBody String getFieldData(HttpSession session,@RequestParam(value="if_id")Long if_id)
	{
		List<Lookup> repo= new ArrayList<Lookup>();
		
		try 
		{
			repo=service.getFieldDataService(if_id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		String result=cm.convert_to_json(repo);
		return result;
	}
	
	@RequestMapping(value="/metadata/updatemetadata",method=RequestMethod.POST)
	public @ResponseBody String updateMetaData(@RequestBody IndexField index,HttpSession session){
		
		ActionResponse<IndexField> response =new ActionResponse<>();
		String result="";
		IndexField ind =null;
		try 
		{
			User user=(User) session.getAttribute("USER");
			
			ind=service.getIndexFieldById(index.getIf_id());
			ind.setIf_name(index.getIf_name());
			System.out.print(index.getIf_name());
			ind.setIf_type(index.getIf_type());
			ind.setIf_mod_by(user.getUm_id());
			Date date = new Date();
			ind.setIf_mod_date(date);
			
			service.mergeIndexField(ind);
			response.setResponse("TRUE");
			
		} 
		catch (Exception e) 
		{
			response.setResponse("FALSE");
		}
		result =cm.convert_to_json(response);
		return result;
	}
	@RequestMapping(value="/metadata/deletemetadata",method=RequestMethod.POST)
	public @ResponseBody String deleteMetaData(@RequestBody long id,HttpSession session){
		
		ActionResponse<IndexField> response =new ActionResponse<>();
		String result="";
		
		
		try {
			 service.delteIndexFiled(id); 
			  response.setResponse("TRUE");
		} catch (Exception e) {
		
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		result=cm.convert_to_json(response);
		return result;
	}
	
	/*............will be worked  later ...........*/
	@RequestMapping(value="/metadata/getIndexFieldsFromLookup")
	public String getIndexFieldsFromLookUp()
	{
		String result="";
		List<Lookup> lookup = new ArrayList<>();
		try 
		{
			lookup = service.getIndexFieldsFromLookUp();
		} 
		catch (Exception e) 
		{
			
		}
		return result;
	}
	
	@RequestMapping(value="/metadata/addmetadata",method=RequestMethod.POST)
	public @ResponseBody String addMetaData(@RequestBody IndexField indexField,HttpSession session){
		
		ActionResponse<IndexField> response =new ActionResponse<>();
		String result="";
		
		User user =(User)session.getAttribute("USER");
		
		try 
		{
			int getdata=service.getSequenceId(indexField.getIf_parent());
			
			 indexField.setIf_cr_by(user.getUm_id());
			 Date date =new Date();
			 indexField.setIf_cr_date(date);
			 indexField.setIf_rec_status(0);
			 indexField.setIf_lable(indexField.getIf_name());
			 indexField.setIf_sequence(getdata+1);
			 
			  response.setResponse("TRUE");
			 
			 service.save(indexField); 
		} 
		catch (Exception e) {
		
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		result=cm.convert_to_json(response);
		return result;
	}
	
	
	@RequestMapping("/metadata/dataEntry")
	public ModelAndView dataEntry(HttpServletRequest request,HttpSession session,Model map)
	{
		Long doc_id= Long.parseLong(request.getParameter("id"));
		System.out.println("MetaDataController-- docid"+doc_id);
		/************* Imp Code  ***********/
		
		/*User user = (User) session.getAttribute("USER");
		Document doc = service.getDocumentById(doc_id);
		List<IndexField> repo= new ArrayList<IndexField>();
		try 
		{
			repo=service.getindexfields(doc.getDept_id());
			Comparator<IndexField> com = new Comparator<IndexField>() 
			{

				@Override
				public int compare(IndexField o1, IndexField o2) {
					
					return o1.getIf_name().compareTo(o2.getIf_name());
				}
				
			};
			Collections.sort(repo,com);
			for(IndexField in : repo)
			{
				boolean result =service.checkMetaDataExistence(doc_id, in.getIf_id());
				if(!result)
				{
					MetaData md = new MetaData();
					md.setMd_fd_mid(doc_id);
					md.setMd_cr_date(new Date());
					md.setMd_mf_mid(in.getIf_id());
					md.setMd_rec_status(1);
					Integer seq = service.getMetaDataSequence(doc_id, in.getIf_id());
					md.setMd_sequence(seq+1);
					service.saveMetaData(md);
				}
			}
			
			List<Index_Meta_Data> data = new ArrayList<>();
			for(IndexField in : repo)
			{
				Index_Meta_Data field_data = new Index_Meta_Data();
				
				field_data.setIf_id(in.getIf_id());
				field_data.setIf_name(in.getIf_name());
				field_data.setIf_type(in.getIf_type());
				if(in.getIf_type().equals("dropdown"))
				{
					List<Lookup> li_look = new ArrayList<>();
					System.out.println(in.getIf_type());
					*//***   temp code from ***//*
					if(in.getIf_name().equals("Bench Type"))
					{
						li_look = service.getLookupByName("BENCH_TYPE");
					}
					else
					{
						li_look = service.getLookupByName("CASE_TYPE");
					}
					*//***   temp code up to ***//*
					field_data.setIf_value_drop(li_look);
				}
				
				MetaData meta = service.getMetaData(doc_id, in.getIf_id());
				field_data.setMeta_value(meta.getMd_value());
				data.add(field_data);
			}
			
			for(Index_Meta_Data in : data)
			{
				System.out.println(in);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}*/
		map.addAttribute("doc_id", doc_id);
		return new ModelAndView("views/metadata/dataEntry");
		
	}
	
	
	//@RequestMapping(value="/metadata/getIndexFieldsDataMetaData",method=RequestMethod.GET)
	public @ResponseBody String getDocumentData(@RequestParam(value="dept_id")Long doc_id,HttpSession session)
	{
		System.out.println("Check point 1 - Doc ID " + doc_id );
		ActionResponse<DocumentCompleteData> response = new ActionResponse<>();
		DocumentCompleteData doc_data = new  DocumentCompleteData();
		List<Index_Meta_Data> data = new ArrayList<>();
		
		String result="";
		
		try 
		{
			User user = (User) session.getAttribute("USER");
			
			Document doc = service.getDocumentById(doc_id);
			
			SubDepartment sub_dept = folderservice.getSubDepartmentById(doc.getDept_id());
			
			Department dept = folderservice.getDepartmentById(sub_dept.getParent_id());
			
			doc_data.setDoc_id(doc.getId());
			doc_data.setDoc_name(doc.getDocument_name());
			
			doc_data.setDept_id(dept.getDept_id());
			doc_data.setDept_name(dept.getDept_name());
			
			doc_data.setSub_dept_id(sub_dept.getSub_dept_id());
			doc_data.setSub_dept_name(sub_dept.getSub_dept_name());
			
			List<IndexField> repo= new ArrayList<IndexField>();
			System.out.println("Check point 2 - repo " + repo.get(0) );
				repo=service.getindexfields(doc.getDept_id());
				Comparator<IndexField> com = new Comparator<IndexField>() 
				{

					@Override
					public int compare(IndexField o1, IndexField o2) {
						
						return o1.getIf_name().compareTo(o2.getIf_name());
					}
					
				};
				Collections.sort(repo,com);
				for(IndexField in : repo)
				{
					boolean result2 =service.checkMetaDataExistence(doc_id, in.getIf_id());
					if(!result2)
					{
						MetaData md = new MetaData();
						md.setMd_fd_mid(doc_id);
						md.setMd_cr_date(new Date());
						md.setMd_mf_mid(in.getIf_id());
						md.setMd_rec_status(1);
						md.setMd_cr_by(user.getUm_id());
						Integer seq = service.getMetaDataSequence(doc_id, in.getIf_id());
						md.setMd_sequence(seq+1);
						service.saveMetaData(md);
					}
				}
				
				
				for(IndexField in : repo)
				{
					Index_Meta_Data field_data = new Index_Meta_Data();
					
					field_data.setIf_id(in.getIf_id());
					field_data.setIf_name(in.getIf_name());
					field_data.setIf_type(in.getIf_type());
					
					if(in.getIf_type().equals("dropdown"))
					{
						List<Lookup> li_look = new ArrayList<>();
						System.out.println(in.getIf_type());
						/***   temp code from ***/
						if(in.getIf_name().equals("Bench Type"))
						{
							li_look = service.getLookupByName("BENCH_TYPE");
						}
						else
						{
							li_look = service.getLookupByName("CASE_TYPE");
						}
						/***   temp code up to ***/
						field_data.setIf_value_drop(li_look);
					}
					
					MetaData meta = service.getMetaData(doc_id, in.getIf_id());
					field_data.setMeta_value(meta.getMd_value());
					try 
					{
						if(field_data.getMeta_value()!="" || field_data.getMeta_value()!=null && in.getIf_type().equals("dropdown") && field_data.getMeta_value()!=null)
						{
							String value = service.getDropDownValueFromLookup(field_data.getMeta_value());
							field_data.setMeta_value_if_drop(value);
						}
						
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					
					data.add(field_data);
				}
				
			response.setResponse("TRUE");
		} 
		catch (Exception e) 
		{
		
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		
		Comparator<Index_Meta_Data> com = new Comparator<Index_Meta_Data>() {
			
			@Override
			public int compare(Index_Meta_Data o1, Index_Meta_Data o2) {
				
				return o1.getIf_name().compareTo(o2.getIf_name());
			}
		};
		Collections.sort(data,com);
		doc_data.setIndexfields(data);
		response.setModelData(doc_data);
		result =cm.convert_to_json(response);
		return result;
	}

/**************** Second Method ***********/
	@RequestMapping(value="/metadata/getIndexFieldsDataMetaData",method=RequestMethod.GET)
	public @ResponseBody String getDocumentData1(HttpServletRequest request , @RequestParam(value="dept_id")Long doc_id,HttpSession session)
	{
		ActionResponse<DocumentCompleteData> response = new ActionResponse<>();
		DocumentCompleteData doc_data = new  DocumentCompleteData();
		List<Index_Meta_Data> data = new ArrayList<>();
		DocumentFileDetails docfile = null;
		System.out.println("MetaData - getDocumentData1 -->");
		String result="";
		
		try 
		{
			User user = (User) session.getAttribute("USER");
			Lookup	role1= lookupservices.getAllByLongname("MDIAdmin");
			String ip_address= Inet4Address.getLocalHost().getHostAddress();
			if(user.getUserroles().get(0).getUr_role_id().equals(role1.getLk_id())){
				ViewLog viewlog=new ViewLog();
				viewlog.setVl_dfd_mid(doc_id);
				viewlog.setVl_user_mid(user.getUm_id());
				viewlog.setVl_view_time(new Date());
				viewlog.setVl_ip_address(ip_address);
				dfdservice.saveViewLog(viewlog);
			}
			
			docfile = service.getDocumentFileDetailsById(doc_id);
			
			System.out.println("Doc File = "+docfile );
			//System.out.println(docfile.getDfd_md_status());
			if(docfile.getDfd_md_status()!=null  )
			{
				doc_data.setDfd_md_status("YES");
			}
			SubDepartment sb_dept=folderservice.getSubDepartmentById(docfile.getDfd_sub_dept_mid());
			Department dpt=folderservice.getDepartmentById(docfile.getDfd_dept_mid());
			
			//Document doc = service.getDocumentById(doc_id);
			
			//SubDepartment sub_dept = folderservice.getSubDepartmentById(doc.getDept_id());
			
			//Department dept = folderservice.getDepartmentById(sub_dept.getParent_id());
			
			doc_data.setDoc_id(docfile.getDfd_id());
			doc_data.setDoc_name(docfile.getDfd_file_name());
			
			doc_data.setDept_id(dpt.getDept_id());
			doc_data.setDept_name(dpt.getDept_name());
			
			doc_data.setSub_dept_id(sb_dept.getSub_dept_id());
			doc_data.setSub_dept_name(sb_dept.getSub_dept_name());
			
			List<IndexField> repo= new ArrayList<IndexField>();
			
			
			repo=service.getindexfields(docfile.getDfd_dt_mid());
				/*repo=service.getindexfields(sb_dept.getSub_dept_id());*/
			System.out.println("Doc File = "+repo );	
				Comparator<IndexField> com = new Comparator<IndexField>() 
				{

					@Override
					public int compare(IndexField o1, IndexField o2) {
						
						return o1.getIf_name().compareTo(o2.getIf_name());
					}
					
				};
				Collections.sort(repo,com);
				for(IndexField in : repo)
				{
					boolean result2 =service.checkMetaDataExistence(doc_id, in.getIf_id());
					if(!result2)
					{
						MetaData md = new MetaData();
						md.setMd_fd_mid(doc_id);
						md.setMd_cr_date(new Date());
						md.setMd_mf_mid(in.getIf_id());
						md.setMd_rec_status(1);
						md.setMd_cr_by(user.getUm_id());
						Integer seq = service.getMetaDataSequence(doc_id, in.getIf_id());
						md.setMd_sequence(seq+1);
						service.saveMetaData(md);
					}
				}
				
				
				for(IndexField in : repo)
				{
					Index_Meta_Data field_data = new Index_Meta_Data();
					
					field_data.setIf_id(in.getIf_id());
					field_data.setIf_name(in.getIf_name());
					field_data.setIf_type(in.getIf_type());
					
					if(in.getIf_type().equals("dropdown"))
					{
						List<Lookup> li_look = new ArrayList<>();
						System.out.println(in.getIf_type());
						/***   temp code from ***/
						if(in.getIf_name().equals("Bench Type"))
						{
							li_look = service.getLookupByName(in.getIf_name());
						}
						else
						{
							li_look = service.getLookupByName(in.getIf_name());
						}
						/***   temp code up to ***/
						field_data.setIf_value_drop(li_look);
					}
					
					MetaData meta = service.getMetaData(doc_id, in.getIf_id());
					
					field_data.setMeta_value(meta.getMd_value());
					
					if(in.getIf_type().equals("dropdown") && field_data.getMeta_value()!="" || in.getIf_type().equals("dropdown") && field_data.getMeta_value()!=null )
					{
						System.out.println(in.getIf_type()+"     "+in.getIf_name());
						String value = service.getDropDownValueFromLookup(field_data.getMeta_value());
						field_data.setMeta_value_if_drop(value);
					}
					
					/********* date **********/
					
					if(in.getIf_type().equals("date") && field_data.getMeta_value()!="" || in.getIf_type().equals("date") && field_data.getMeta_value()!=null )
					{
						
						field_data.setMeta_value_if_date(field_data.getMeta_value());
					}
					data.add(field_data);
				}
				
			response.setResponse("TRUE");
		} 
		catch (Exception e) 
		{
		
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		
		Comparator<Index_Meta_Data> com = new Comparator<Index_Meta_Data>() {
			
			@Override
			public int compare(Index_Meta_Data o1, Index_Meta_Data o2) {
				
				return o1.getIf_name().compareTo(o2.getIf_name());
			}
		};
		
		Collections.sort(data,com);
		doc_data.setIndexfields(data);
		ServletContext context= request.getServletContext();
		String uploadpath= context.getRealPath("/");
		try 
		{
			Document_type doc_type = Upservice.getDocumentTypeById(docfile.getDfd_dt_mid());
			String path =  common.getPathToViewDocument(docfile.getDfd_dept_mid(), docfile.getDfd_sub_dept_mid(), docfile.getDfd_year(),doc_type.getDt_name());
			System.out.println(path);
			if(path!=null)
			{
				PdfReader reader = new PdfReader(path+ File.separator+ docfile.getDfd_file_name(), "allahabad".getBytes());
				PdfStamper stamper = new PdfStamper(reader,
						new FileOutputStream(uploadpath + File.separator
								+ "uploads" + File.separator
								+ docfile.getDfd_file_name()));
				stamper.close();
				reader.close();
				response.setData(docfile.getDfd_file_name());
			}
			
		} 
		catch (Exception e) 
		{
			
		}
		
		response.setModelData(doc_data);
		
		result =cm.convert_to_json(response);
		return result;
	}
	
	
	
	
	//@RequestMapping(value="/metadata/saveMetaData",method=RequestMethod.POST)
	public ModelAndView saveMetaData(HttpServletRequest request,Model map) throws Exception
	{
		Long doc_id =Long.parseLong(request.getParameter("doc_id"));
		
		String [] st =request.getParameterValues("datadrop");
		String [] st1 =request.getParameterValues("Ft-petit");
		String [] st2 =request.getParameterValues("datachbox");
		String [] st3 = request.getParameterValues("datadate");
		if(st3.length>0)
		{
			System.out.println(st3[0]);
		}
		
		
		
		Document doc = service.getDocumentById(doc_id);
		
		
		SubDepartment sub_dept = folderservice.getSubDepartmentById(doc.getDept_id());
		
		Department dept = folderservice.getDepartmentById(sub_dept.getParent_id());
		
		List<IndexField> repo= new ArrayList<IndexField>();
		
		repo=service.getindexfields(doc.getDept_id());
		Comparator<IndexField> com = new Comparator<IndexField>() 
		{

			@Override
			public int compare(IndexField o1, IndexField o2) {
				
				return o1.getIf_name().compareTo(o2.getIf_name());
			}
			
		};
		Collections.sort(repo,com);
		int dropcount  = 0;
		int chboxcount = 0;
		int textcount  = 0;
		int radiocount = 0;
		
		try 
		{
			System.out.println(repo.size());
			for(IndexField in : repo)
			{
				
				MetaData meta = service.getMetaData(doc_id, in.getIf_id());
				System.out.println(in.getIf_type());
				if(in.getIf_type().equals("dropdown"))
				{
					System.out.println(st[dropcount]);
					if(st!=null)
					meta.setMd_value(st[dropcount]);
					dropcount++;
				}
				else if(in.getIf_type().equals("text"))
				{
					System.out.println(st1[textcount]);
					if(st1!=null)
					meta.setMd_value(st1[textcount]);
					textcount++;
				}
				else if(in.getIf_type().equals("check box"))
				{
					System.out.println(st1[chboxcount]);
					if(st2!=null)
					meta.setMd_value(st1[chboxcount]);
					chboxcount++;
				}
				else
				{
					
				}
				service.mergeMetaData(meta);
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		map.addAttribute("doc_id", doc_id);
       return new ModelAndView("views/metadata/dataEntry");
	}
	
	
	/*** Second Method ****/
	@RequestMapping(value="/metadata/saveMetaData",method=RequestMethod.POST)
	public ModelAndView saveMetaData2(HttpServletRequest request,Model map) throws Exception
	{
		
		String [] st3=null;
		String [] st =request.getParameterValues("datadrop");
		String [] st1 =request.getParameterValues("Ft-petit");
		String [] st2 =request.getParameterValues("datachbox");
		st3 = request.getParameterValues("datadate");
		/*if(st3.length>0)
		{
			System.out.println(st3[0]);
		}*/
		
		
		Long doc_id = null;
		try 
		{
			String d_id = request.getParameter("document_id");
			
			doc_id = Long.parseLong(d_id);
		} 
		catch (Exception e) 
		{
			
		}
		
		
		DocumentFileDetails docfile = service.getDocumentFileDetailsById(doc_id);
		SubDepartment sb_dept=folderservice.getSubDepartmentById(docfile.getDfd_sub_dept_mid());
		Department dpt=folderservice.getDepartmentById(docfile.getDfd_dept_mid());
		
		
		//Document doc = service.getDocumentById(doc_id);
		
		//SubDepartment sub_dept = folderservice.getSubDepartmentById(doc.getDept_id());
		
		//Department dept = folderservice.getDepartmentById(sub_dept.getParent_id());
		
		List<IndexField> repo= new ArrayList<IndexField>();
		System.out.println(docfile.getDfd_dt_mid());
		repo=service.getindexfields(docfile.getDfd_dt_mid());
		/*repo=service.getindexfields(sb_dept.getSub_dept_id());*/
		Comparator<IndexField> com = new Comparator<IndexField>() 
		{

			@Override
			public int compare(IndexField o1, IndexField o2) {
				
				return o1.getIf_name().compareTo(o2.getIf_name());
			}
			
		};
		Collections.sort(repo,com);
		int dropcount  = 0;
		int chboxcount = 0;
		int textcount  = 0;
		int radiocount = 0;
		int datecount  = 0;
		int count=0;
		try 
		{
			for(IndexField in : repo)
			{
				try 
				{
					MetaData meta = service.getMetaData(doc_id, in.getIf_id());
					
					if(in.getIf_type().equals("dropdown"))
					{
						if(st!=null)
						meta.setMd_value(st[dropcount]);
						dropcount++;
					}
					else if(in.getIf_type().equals("text"))
					{
						if(st1!=null)
						meta.setMd_value(st1[textcount]);
						textcount++;
					}
					else if(in.getIf_type().equals("check box"))
					{
						if(st2!=null)
						meta.setMd_value(st1[chboxcount]);
						chboxcount++;
					}
					else if(in.getIf_type().equals("date"))
					{
						if(st3!=null)
						{
							meta.setMd_value(st3[datecount]);
						}
						datecount++;
					}
					service.mergeMetaData(meta);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			
			docfile.setDfd_md_status("YES");
			service.mergedocumentFile(docfile);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		map.addAttribute("doc_id", doc_id);

       return new ModelAndView("views/metadata/dataEntry");
	}
	
	
@RequestMapping(value="metadata/changeDocfileStage",method=RequestMethod.GET)
	public @ResponseBody String changeDocFileStatus(@RequestParam("doc_id") Long id,HttpSession session)
	{
		ActionResponse<DocumentFileDetails> response = new ActionResponse<>();
		String result="";
		DocumentFileDetails docfile = service.getDocumentFileDetailsById(id);
		    Lookup	role2= lookupservices.getAllByLongname("MDIChecker");  //11
			Lookup	role1= lookupservices.getAllByLongname("MDIMetaData");	//21	
		try 
		{
			User user = (User) session.getAttribute("USER");
			
			System.out.println(id);
			List<Object> obj=new ArrayList<Object>();
			Long userId=0L;
			System.out.println(user.getUserroles().get(0).getUr_role_id());
			if(user.getUserroles().get(0).getUr_role_id().equals(role1.getLk_id())) {
				if(userId==0L)
				{
					userId=dfdservice.getNewUserList("MDIChecker",15L);
				}
			
				if(userId==0L)
				{
					obj=dfdservice.getUserList("MDIChecker",15L);
					userId=common.getLeastTaskUser(obj);
				}
				docfile.setDfd_stage_lid(15l);
				docfile.setDfd_assign_to(userId);
				service.mergedocumentFile(docfile);
				System.out.println(userId);
				DocumentFileStage dc_fs=new DocumentFileStage();
			
				Long index =service.getMaxIndexOfDocumentFileStage(docfile.getDfd_id());
				dc_fs.setDfs_assign_to(userId);	
				dc_fs.setDfs_cr_by(user.getUm_id());
				dc_fs.setDfs_dfd_mid(docfile.getDfd_id());
				dc_fs.setDfs_rec_status(1);
				dc_fs.setDfs_stage_lid(15l);
				dc_fs.setDfs_reject_status('N');
				dc_fs.setDfs_cr_date(new Date());
				dc_fs.setDfs_stage_date(new Date());
				dc_fs.setDfs_index(index+1l);
			
				service.saveDocumentFileStage(dc_fs);
				response.setResponse("TRUE");
			}
			System.out.println(role2.getLk_id());
			if(user.getUserroles().get(0).getUr_role_id().equals(role2.getLk_id())) {
				if(userId==0L)
				{
					userId=dfdservice.getNewUserList("MDIAdmin",16L);
				}
			
				if(userId==0L)
				{
					obj=dfdservice.getUserList("MDIAdmin",16L);
					userId=common.getLeastTaskUser(obj);
				}
				docfile.setDfd_stage_lid(16l);
				docfile.setDfd_assign_to(userId);
				service.mergedocumentFile(docfile);
				System.out.println(userId);
				DocumentFileStage dc_fs=new DocumentFileStage();
			
				Long index =service.getMaxIndexOfDocumentFileStage(docfile.getDfd_id());
				dc_fs.setDfs_assign_to(userId);	
				dc_fs.setDfs_cr_by(user.getUm_id());
				dc_fs.setDfs_dfd_mid(docfile.getDfd_id());
				dc_fs.setDfs_rec_status(1);
				dc_fs.setDfs_stage_lid(16l);
				dc_fs.setDfs_reject_status('N');
				dc_fs.setDfs_cr_date(new Date());
				dc_fs.setDfs_stage_date(new Date());
				dc_fs.setDfs_index(index+1l);
			
				service.saveDocumentFileStage(dc_fs);
				response.setResponse("TRUE");
			}
			
		}

		catch (Exception e) 
		{
			e.printStackTrace();
			response.setResponse("ELSE");
		}
		response.setModelData(docfile);
		result= cm.convert_to_json(response);
		return result;
	}
	
}
