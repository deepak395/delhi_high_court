package com.mdi.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.mdi.dto.MetaDataSearch;
import com.mdi.model.ActionResponse;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.DocumentFileStage;
import com.mdi.model.Document_type;
import com.mdi.model.DownloadLog;
import com.mdi.model.Folder;
import com.mdi.model.IndexField;
import com.mdi.model.Lookup;
import com.mdi.model.MetaData;
import com.mdi.model.Permission;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.service.DocumentFileDetailsService;
import com.mdi.service.DocumentFileStageService;
import com.mdi.service.FolderService;
import com.mdi.service.LookupServices;
import com.mdi.service.MetaDataService;
import com.mdi.service.UploadDocumentService;
import com.mdi.service.UserService;
import com.mdi.utility.CommenMethods;

@Controller
public class UploadDocumentController {

	@Autowired
	private MetaDataService service;

	@Autowired
	private UploadDocumentService Upservice;

	@Autowired
	private LookupServices lookupservices;

	@Autowired
	private DocumentFileDetailsService docuumetFileDetailsService;

	@Autowired
	private DocumentFileStageService docuumetFileStageService;

	@Autowired
	private UserService userservice;

	@Autowired
	private CommonController common;

	@Autowired
	ServletContext context;

	@Autowired
	private FolderService folderService;

	CommenMethods cm;

	public UploadDocumentController() {
		cm = new CommenMethods();
	}

	@RequestMapping("document/UploadDocument")
	public String UploadDocument() {
		System.out.print("Hello");
		return "views/document/UploadDocument";
	}

	@RequestMapping("/metadata/manage")
	public String Manage() {
		System.out.print("Hello");
		return "views/metadata/manage";
	}

	@RequestMapping(value = "/UploadDocument/getAllDepartments", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getDepartments() {
		List<Department> repo = new ArrayList<Department>();

		try {
			repo = service.getDepartments();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String result = cm.convert_to_json(repo);
		return result;
	}

	@RequestMapping(value = "/UploadDocument/getUserWiseDepartments", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getUserWiseDepartments(HttpSession session) {

		User user = (User) session.getAttribute("USER");
		List<Department> vikasrepo = new ArrayList<Department>();

		List<Department> atishrepo = new ArrayList<Department>();

		List<Department> namrepo = new ArrayList<Department>();

		String result = "";
		if (user.getUsername().equals("vikas.kumar")) {

			vikasrepo = service.getVikasDepartments();
			result = cm.convert_to_json(vikasrepo);
		}

		if (user.getUsername().equals("namrata.soni")) {

			namrepo = service.getNamartaDepartments();
			result = cm.convert_to_json(namrepo);
		}

		if (user.getUsername().equals("atish.singh")) {

			atishrepo = service.getAtishDepartments();

			result = cm.convert_to_json(atishrepo);
		}

		return result;
	}

	@RequestMapping(value = "/UploadDocument/getUserwisefiles", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getUserWiseFiles(HttpSession session) {
		User user = new User();
		user = (User) session.getAttribute("USER");
		List<DocumentFileDetails> dfd = new ArrayList<DocumentFileDetails>();
		try {
			dfd = docuumetFileDetailsService.getFilesByUserWise(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String result = cm.convert_to_json(dfd);
		return result;
	}

	// get record with filteration
	@ResponseBody
	@RequestMapping(value = "/UploadDocument/getFileByDeptAndSubDeptAndDocTypeUserWiseData", method = RequestMethod.POST)
	public String getFileByDeptAndSubDeptAndDocTypeUserWiseData(DocumentFileDetails documentFileDetails,
			HttpSession session) {

		System.out.println("controller " + documentFileDetails);
		String result = null;
		User user = (User) session.getAttribute("USER");
		if (user != null) {
			documentFileDetails.setDfd_assign_to(user.getUm_id());
			List<DocumentFileDetails> documentFileDetail = docuumetFileDetailsService
					.getFileByDeptAndSubDeptAndDocTypeUserWiseData(documentFileDetails);
			System.out.println("documentFileDetail::: " + documentFileDetail);
			if (documentFileDetail != null) {
				result = cm.convert_to_json(documentFileDetail);
			}
		}
		return result;
	}

	public String changeDocFileStatus(Long id, HttpSession session) {
		ActionResponse<DocumentFileDetails> response = new ActionResponse<>();
		String result = "";
		DocumentFileDetails docfile = service.getDocumentFileDetailsById(id);
		Lookup role2 = lookupservices.getAllByLongname("MDIChecker"); // 11
		Lookup role1 = lookupservices.getAllByLongname("MDIMetaData"); // 21
		try {
			User user = (User) session.getAttribute("USER");

			System.out.println(id);
			List<Object> obj = new ArrayList<Object>();
			Long userId = 0L;
			System.out.println(user.getUserroles().get(0).getUr_role_id());
			if (user.getUserroles().get(0).getUr_role_id().equals(role1.getLk_id())) {
				if (userId == 0L) {
					userId = docuumetFileDetailsService.getNewUserList("MDIChecker", 15L);
				}

				if (userId == 0L) {
					obj = docuumetFileDetailsService.getUserList("MDIChecker", 15L);
					userId = common.getLeastTaskUser(obj);
				}
				docfile.setDfd_stage_lid(15l);
				docfile.setDfd_assign_to(userId);
				service.mergedocumentFile(docfile);
				System.out.println(userId);
				DocumentFileStage dc_fs = new DocumentFileStage();

				Long index = service.getMaxIndexOfDocumentFileStage(docfile.getDfd_id());
				dc_fs.setDfs_assign_to(userId);
				dc_fs.setDfs_cr_by(user.getUm_id());
				dc_fs.setDfs_dfd_mid(docfile.getDfd_id());
				dc_fs.setDfs_rec_status(1);
				dc_fs.setDfs_stage_lid(15l);
				dc_fs.setDfs_reject_status('N');
				dc_fs.setDfs_cr_date(new Date());
				dc_fs.setDfs_stage_date(new Date());
				dc_fs.setDfs_index(index + 1l);

				service.saveDocumentFileStage(dc_fs);
				response.setResponse("TRUE");
			}
			System.out.println(role2.getLk_id());
			if (user.getUserroles().get(0).getUr_role_id().equals(role2.getLk_id())) {
				if (userId == 0L) {
					userId = docuumetFileDetailsService.getNewUserList("MDIAdmin", 16L);
				}

				if (userId == 0L) {
					obj = docuumetFileDetailsService.getUserList("MDIAdmin", 16L);
					userId = common.getLeastTaskUser(obj);
				}
				docfile.setDfd_stage_lid(16l);
				docfile.setDfd_assign_to(userId);
				service.mergedocumentFile(docfile);
				System.out.println(userId);
				DocumentFileStage dc_fs = new DocumentFileStage();

				Long index = service.getMaxIndexOfDocumentFileStage(docfile.getDfd_id());
				dc_fs.setDfs_assign_to(userId);
				dc_fs.setDfs_cr_by(user.getUm_id());
				dc_fs.setDfs_dfd_mid(docfile.getDfd_id());
				dc_fs.setDfs_rec_status(1);
				dc_fs.setDfs_stage_lid(16l);
				dc_fs.setDfs_reject_status('N');
				dc_fs.setDfs_cr_date(new Date());
				dc_fs.setDfs_stage_date(new Date());
				dc_fs.setDfs_index(index + 1l);

				service.saveDocumentFileStage(dc_fs);
				response.setResponse("TRUE");
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			response.setResponse("ELSE");
		}
		response.setModelData(docfile);
		result = cm.convert_to_json(response);
		return result;
	}

	@RequestMapping(value = "/UploadDcoumet/getfiles", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String getFiles(HttpSession session, HttpServletRequest request) {
		String name = null;
		// ,@RequestParam(value="dept_id")Long
		// dept_id,@RequestParam(value="sub_dept_mid")Long
		// sub_dept,@RequestParam(value="year")Long
		// year,@RequestParam(value="name")String name
		String dept_id = request.getParameter("dept_id");
		String sub_dept = request.getParameter("sub_dept_id");
		String year = request.getParameter("year");
		String dt_id = request.getParameter("dfd_dt_mid");
		String fd_id = request.getParameter("fd_id");
		System.out.println(fd_id);
		int fdid = Integer.parseInt(fd_id);
		// name=request.getParameter("name");
		if (year.equals("undefined")) {
			year = null;
		}
		if (sub_dept.equals("undefined")) {
			sub_dept = null;
		}
		if (dept_id.equals("undefined")) {
			dept_id = null;
		}
		if (dt_id.equals("undefined")) {
			dt_id = null;
		}
		if (fd_id.equals("undefined") || fdid == 0) {
			fd_id = null;
		}
		System.out.println(name + " ddddd" + dept_id);
		List<DocumentFileDetails> dspres = new ArrayList<DocumentFileDetails>();
		try {
			dspres = docuumetFileDetailsService.getFilesBySubDeptAndYear(dept_id, sub_dept, year, dt_id, fd_id);
			System.out.println(dspres.size() + " m m");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(dspres.size() + " m m");
		String result = cm.convert_to_json(dspres);
		return result;
	}

	@RequestMapping(value = "/UploadDocument/approveALl", method = RequestMethod.POST)
	public @ResponseBody String approveAll(@RequestBody List<MetaDataSearch> dfd_id, HttpSession session) {
		String repo = null;

		System.out.println("viru" + dfd_id.size());

		try {
			for (MetaDataSearch dfd : dfd_id) {
				repo = changeDocFileStatus(dfd.getIf_id(), session);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String result = cm.convert_to_json(repo);
		return result;
	}

	@RequestMapping(value = "/UploadDcoumet/getSubDept", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getSubDept(HttpSession session, @RequestParam(value = "dept_id") Long dept_id) {
		List<SubDepartment> repo = new ArrayList<SubDepartment>();
		User user = new User();
		String result = null;
		user = (User) session.getAttribute("USER");
		List<Permission> permList = null;
		List<SubDepartment> finalList = new ArrayList<SubDepartment>();
		try {
			repo = service.getSubDept(dept_id);
			permList = service.getUserPermission(user.getUm_id());

			if (permList.size() != 0) {
				for (Permission pm : permList) {
					for (SubDepartment sb : repo) {

						if (pm.getPm_sub_dpt_mid() == sb.getSub_dept_id()) {

							finalList.add(sb);
						}

					}
				}
				result = cm.convert_to_json(finalList);

			} else {
				result = cm.convert_to_json(repo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/UploadDcoumet/test", method = RequestMethod.POST)
	public @ResponseBody String createtest(MultipartHttpServletRequest request, HttpSession session)
			throws DocumentException {

		int counter = 0;
		MultipartFile mpf = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date startDate = new Date();
		;
		String result = null;
		ActionResponse<DocumentFileDetails> response = new ActionResponse();
		User user = new User();
		user = (User) session.getAttribute("USER");

		DocumentFileDetails documenttemp = new DocumentFileDetails();
		DocumentFileStage documentstage = new DocumentFileStage();
		MetaData metadata = new MetaData();
		long dept_id = (Long.parseLong(request.getParameter("dept_id")));

		long sub_dept_id = (Long.parseLong(request.getParameter("sub_dept_id")));

		long doc_type_id = (Long.parseLong(request.getParameter("dfd_dt_mid")));
		System.out.println(sub_dept_id);
		long year = (Long.parseLong(request.getParameter("year")));
		Long fd_id = (Long.parseLong(request.getParameter("dfd_fd_mid")));
		System.out.println(fd_id);
		Department dept = new Department();
		String jsonData = "";
		Lookup lookup = lookupservices.getLookUpObject("REPOSITORYPATH");
		SubDepartment sub_dept = new SubDepartment();
		IndexField index_field = null;
		Document_type doc_type = null;
		Folder folder = null;
		try {
			dept = Upservice.getDepartment(dept_id);
			sub_dept = Upservice.getsubdeptById(sub_dept_id);
			doc_type = Upservice.getDocumentTypeById(doc_type_id);
			index_field = Upservice.getIfByDtId(doc_type_id);
			if (fd_id != 0) {
				folder = Upservice.getFolderById(fd_id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Iterator<String> itr = request.getFileNames();
		List<User> userID = new ArrayList<User>();

		userID = userservice.getUserByRole("MDIMetaData");

		System.out.println(userID);
		System.out.println(userID.size());
		System.out.println(year);
		int count = 0;
		try {
			if (!index_field.equals(null)) {
				while (itr.hasNext()) {

					mpf = request.getFile(itr.next());
					int newPageCount;
					String filename = mpf.getOriginalFilename();
					String filenamereal = filename.substring(0, filename.length() - 4) + "_" + year + ".pdf";
					// System.out.println("Filename="+filenamereal);
					// Long file_stage=0L;
					File path = null;

					if (!doc_type.equals(null) && fd_id != 0 && year == 1000) {
						path = new File(lookup.getLk_longname() + File.separator + dept.getDept_name() + File.separator
								+ sub_dept.getSub_dept_name() + File.separator + doc_type.getDt_name() + File.separator
								+ folder.getFolder_name());

					}

					else if (!doc_type.equals(null) && year != 1000 && year != 1001 && fd_id == 0) {
						path = new File(lookup.getLk_longname() + File.separator + dept.getDept_name() + File.separator
								+ sub_dept.getSub_dept_name() + File.separator + doc_type.getDt_name() + File.separator
								+ year);
					} else if (!doc_type.equals(null) && year != 1000 && year == 1001 && fd_id == 0) {
						path = new File(lookup.getLk_longname() + File.separator + dept.getDept_name() + File.separator
								+ sub_dept.getSub_dept_name() + File.separator + doc_type.getDt_name());
					} else if (!doc_type.equals(null) && year != 1000 && year != 1001 && fd_id != 0) {
						// fd_id=0l;
						path = new File(lookup.getLk_longname() + File.separator + dept.getDept_name() + File.separator
								+ sub_dept.getSub_dept_name() + File.separator + doc_type.getDt_name() + File.separator
								+ year);
					}

					String temppath = path + File.separator + filenamereal;
					System.out.println(temppath);
					String ext = FilenameUtils.getExtension(temppath);
					File oldfile = new File(temppath);
					if (!path.exists()) {
						path.mkdirs();
					}
					if (count == userID.size()) {
						count = 0;
					}

					if (ext.equalsIgnoreCase("pdf") && !oldfile.isFile()) {
						try {
							FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(temppath));
							PdfReader readernewFile = new PdfReader(temppath);
							newPageCount = readernewFile.getNumberOfPages();
							readernewFile.close();
							documenttemp.setDfd_year(year);
							documenttemp.setDfd_cr_by(user.getUm_id());
							documenttemp.setDfd_cr_date(new Date());
							documenttemp.setDfd_dept_mid(dept.getDept_id());
							documenttemp.setDfd_file_name(filenamereal);
							documenttemp.setDfd_name(filename.substring(0, filename.length() - 4));
							documenttemp.setDfd_page_count(newPageCount);
							documenttemp.setDfd_rec_status(1);
							documenttemp.setDfd_sub_dept_mid(sub_dept.getSub_dept_id());
							documenttemp.setDfd_rep_mid(1L);
							documenttemp.setDfd_stage_lid(14L);
							documenttemp.setDfd_assign_to(userID.get(count).getUm_id());
							documenttemp.setDfd_edit_mode(0);
							documenttemp.setDfd_mod_by(null);
							documenttemp.setDfd_mod_date(null);
							documenttemp.setDfd_print_flag(1);
							documenttemp.setDfd_remark(null);
							documenttemp.setDfd_reject_status('N');
							documenttemp.setDfd_system_page_count(0);
							documenttemp.setDfd_index(0);
							documenttemp.setDfd_dt_mid(doc_type_id);
							documenttemp.setDfd_fd_mid(fd_id);

							DocumentFileDetails dfd = docuumetFileDetailsService.save(documenttemp);

							documentstage.setDfs_assign_to(userID.get(count++).getUm_id());
							documentstage.setDfs_cr_by(user.getUm_id());
							documentstage.setDfs_cr_date(new Date());
							documentstage.setDfs_dfd_mid(dfd.getDfd_id());
							documentstage.setDfs_index(1L);
							documentstage.setDfs_rec_status(1);
							documentstage.setDfs_reject_status('N');
							documentstage.setDfs_remark(null);
							documentstage.setDfs_stage_date(new Date());
							documentstage.setDfs_stage_lid(14L);

							docuumetFileStageService.save(documentstage);

							Long metamf = service.getMetaDataSequence(doc_type_id);

							metadata.setMd_cr_by(user.getUm_id());
							metadata.setMd_cr_date(new Date());
							metadata.setMd_fd_mid(dfd.getDfd_id());
							metadata.setMd_mf_mid(metamf);
							metadata.setMd_rec_status(1);
							metadata.setMd_sequence(1);
							metadata.setMd_value(dfd.getDfd_name());

							service.saveMetaData(metadata);

							response.setResponse("TRUE");
							counter++;
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							// TODO Auto-generated catch block
							response.setResponse("FALSE");
							e.printStackTrace();
						} catch (IOException e) {

							// TODO Auto-generated catch block
							response.setResponse("FALSE");
							e.printStackTrace();
						}

						System.out.println("File Uploded...!");
					} else {
						response.setResponse("FALSE");
						System.out.println("file is already there...!");
					}
				} // While Loop End
			} // Index Field Condition terminated

		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse("FALSE");
		}
		response.setData(counter);
		result = cm.convert_to_json(response);
		return result;
	}

	/*
	 * @RequestMapping(value="/metadata/getindexfields",method=RequestMethod.GET,
	 * headers = "Accept=application/json") public @ResponseBody String
	 * getIndexFields(HttpSession session,@RequestParam(value="sub_dept_id")Long
	 * sub_dept_id) { List<IndexField> repo= new ArrayList<IndexField>();
	 * 
	 * try { repo=service.getindexfields(sub_dept_id); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * String result=cm.convert_to_json(repo); return result; }
	 * 
	 * @RequestMapping(value="/metadata/getfielddata",method=RequestMethod.GET,
	 * headers = "Accept=application/json") public @ResponseBody String
	 * getFieldData(HttpSession session,@RequestParam(value="if_id")Long if_id) {
	 * List<Lookup> repo= new ArrayList<Lookup>();
	 * 
	 * try { repo=service.getFieldDataService(if_id); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * String result=cm.convert_to_json(repo); return result; }
	 */

	@RequestMapping(value = "/UploadDcoument/getDocumentType")
	public @ResponseBody String getDocumentType(@RequestParam("sub_dept_id") Long sub_dept_id) {
		ActionResponse<Document_type> response = new ActionResponse<>();
		List<Document_type> dt = new ArrayList<>();
		try {
			dt = service.getDocumentTypeBySubDept(sub_dept_id);
			System.out.println(dt);
			response.setResponse("TRUE");
			response.setModelList(dt);
		} catch (Exception e) {
			response.setResponse("FALSE");
		}
		return cm.convert_to_json(response);

	}

	/****************** get Folders *******************/

	@RequestMapping(value = "/UploadDcoument/getFolders", method = RequestMethod.GET)
	public @ResponseBody String getFolders(@RequestParam("dt_id") Long dt_id) {
		ActionResponse<Folder> response = new ActionResponse<>();
		List<Folder> folder = new ArrayList<>();
		try {
			folder = service.getFoldersByParentId(dt_id);
			System.out.println(folder.size() + "//////////////////////");
			if (!folder.isEmpty()) {
				response.setModelList(folder);
				response.setResponse("TRUE");
			} else {
				response.setData("No Folder Found");
				response.setResponse("false");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cm.convert_to_json(response);
	}

	/*
	 * @RequestMapping(value="/downloadPendingCaseDocument",
	 * method=RequestMethod.GET) public void
	 * downloadPendingCaseDocument(@RequestParam("documentId") Long documentId,
	 * HttpServletResponse response) {
	 * 
	 * PendingCaseDocument pendingCaseDocument=pendingCaseDocumentService.
	 * getPendingCaseDocumentByDocumentId(documentId); if(pendingCaseDocument!=null)
	 * { Path file = Paths.get(pendingCaseDocument.getDestinationPath(),
	 * pendingCaseDocument.getDocumentName()); if (Files.exists(file)){
	 * pendingCaseDocument.setQcDownloaded(true); boolean
	 * flag=pendingCaseDocumentService.updatePendingCaseDocument(pendingCaseDocument
	 * ); if (flag) { response.setContentType("application/pdf");
	 * response.addHeader("Content-Disposition",
	 * "attachment; filename="+pendingCaseDocument.getDocumentName()); try {
	 * Files.copy(file, response.getOutputStream());
	 * response.getOutputStream().flush(); } catch (Exception ex) {
	 * ex.printStackTrace(); } } } } }
	 */

	@RequestMapping(value = "/UploadDocument/downloadFile", method = RequestMethod.GET, headers = "Accept=application/json")
	public void downloadFile(HttpSession session, HttpServletResponse response, HttpServletRequest request)
			throws FileNotFoundException {
		String id = request.getParameter("dfd_id");
		DocumentFileDetails dfd = docuumetFileDetailsService.getById(id);
		String destinationPath = session.getServletContext().getRealPath("/resources/upload/");
		if (dfd != null) {
			StringBuilder sourcebasepath = new StringBuilder();
			StringBuilder destination = new StringBuilder();
			Lookup lookup = lookupservices.getLookUpObject("REPOSITORYPATH");
			Department dept = new Department();
			SubDepartment subdept = new SubDepartment();
			Folder fd = new Folder();

			if (dfd.getDfd_dept_mid() != null) {
				sourcebasepath.append(lookup.getLk_longname() + dfd.getDepartments().getDept_name());
				// destination.append("MDI"+File.separator+document.getDepartments().getDept_name());

			}
			if (dfd.getDfd_sub_dept_mid() != null) {
				sourcebasepath.append(File.separator + dfd.getSubdepartments().getSub_dept_name());
				// destination.append(File.separator+dfd.getSubdepartments().getSub_dept_name());
			}
			if (dfd.getDfd_dt_mid() != null) {
				sourcebasepath.append(File.separator + dfd.getDoctype().getDt_name());
				// destination.append(File.separator+document.getDoctype().getDt_name());
			}
			if (dfd.getDfd_fd_mid() != null && dfd.getDfd_fd_mid() != 0) {
				fd = folderService.getFolderById(dfd.getDfd_fd_mid());
				sourcebasepath.append(File.separator + fd.getDisplay_name());
				// destination.append(File.separator+fd.getDisplay_name());
			}
			if (dfd.getDfd_year() != null) {
				sourcebasepath.append(File.separator + dfd.getDfd_year());
				// destination.append(File.separator+dfd.getDfd_year());
			}
			sourcebasepath.append(File.separator + dfd.getDfd_file_name());
			// destination.append(File.separator+dfd.getDfd_file_name());
			String src = new String(sourcebasepath);

			File source = new File(src);
			String path = request.getContextPath();

			String uploadPath = context.getRealPath("");
			File dest = new File(uploadPath + File.separator + "uploads" + File.separator + dfd.getDfd_file_name());
			try {
				FileUtils.copyFile(source, dest);
			} catch (IOException e) {
				e.printStackTrace();
			}

			DownloadLog dl = new DownloadLog();
			String ip_address = null;
			try {
				 ip_address= Inet4Address.getLocalHost().getHostAddress();
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			User user = (User) session.getAttribute("USER");
			dl.setDl_user_mid(user.getUm_id());
			dl.setDl_ip_address(ip_address);
			dl.setDl_download_time(new Date());
			dl.setDl_dfd_mid(dfd.getDfd_id());
			docuumetFileDetailsService.saveDownloadLog(dl);
			
			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("");
			System.out.println("appPath = " + appPath);

			// construct the complete absolute path of the file
			String fullPath = appPath + File.separator + "uploads" + File.separator + dfd.getDfd_file_name();
			File downloadFile = new File(fullPath);
			FileInputStream inputStream = new FileInputStream(downloadFile);
			// get MIME type of the file
			String mimeType = context.getMimeType(fullPath);
			if (mimeType == null) {
				// set to binary type if MIME mapping not found
				mimeType = "application/octet-stream";
			}
			System.out.println("MIME type: " + mimeType);

			// set content attributes for the response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// set headers for the response
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);
			try {
				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				inputStream.close();
				outStream.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@RequestMapping(value = "/UploadDocument/getUserDownloadStatus", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getUserDownloadStatus(HttpSession session) {
		User user = null;
		user = (User) session.getAttribute("USER");
		String result = cm.convert_to_json(user);
		return result;
	}

}
