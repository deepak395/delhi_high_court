package com.mdi.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mdi.model.ActionResponse;
import com.mdi.model.SecurityQuestion;
import com.mdi.model.User;
import com.mdi.service.UserService;
import com.mdi.utility.GlobalFunction;
import com.mdi.validation.SecurityQuestionValidator;

@Controller
public class SecurityQuestionController {

	@Autowired
	private SecurityQuestionValidator sqvalidation;
	
	@Autowired
	private UserService userService;
	
	
	HttpSession session;
	GlobalFunction cm;
	
	public SecurityQuestionController() {
		cm = new GlobalFunction();
	}
	
	@RequestMapping(value="securityquestion/changePassword",method = RequestMethod.POST)
	public @ResponseBody String changePassword(@RequestBody SecurityQuestion sq,HttpSession session,HttpServletRequest request)
	{
		User user=(User) session.getAttribute("USER");
		String u_id=request.getParameter("user_id");
		System.out.println("----------"+u_id+"      ddd "+sq.getPassword()+" "+sq.getPasswordc()+" "+sq.getPra_user_mid());
		String jsonData=null;
		ActionResponse<SecurityQuestion> response=new ActionResponse<SecurityQuestion>();
		response=sqvalidation.doValidationForChanePwd(sq);
		
		System.out.print("ram");
		String pass=sq.getPassword();
		String passc=sq.getPasswordc();
		
		if(pass.equals(passc)){
			response.setResponse("TRUE");
			response.setData(pass);
			user.setPassword((pass));
			System.out.println(pass);
			userService.update(user);
		}
		jsonData =cm.convert_to_json(response);
		System.out.println("last "+response.getResponse());
	    return jsonData;
	}
}
