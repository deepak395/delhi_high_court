package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.dao.UserDao;
import com.mdi.model.Department;
import com.mdi.model.LoginLog;
import com.mdi.model.Lookup;
import com.mdi.model.ObjectMaster;
import com.mdi.model.Permission;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.service.UserService;

@Service
public class UserServiceImpl implements UserService
{
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private UserDao userDao;


	@Transactional
	public LoginLog saveLog(LoginLog s) {

		LoginLog master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Override
	public User validateLogin(User user) throws Exception 
	{
		try 
		{
			User u =userDao.validateLogin(user);
			return u;
		} 
		catch (Exception e) 
		{
			throw new Exception("service_layer",e);
		}

	}

	@Override
	public User forgetPassword(String username) throws Exception {

		try{
			
			User u=userDao.forgetPassword(username);
			return u;
		}
		catch (Exception e) 
		{
			throw new Exception("service_layer",e);
		}
	}

	
	
	@Override
	public User getUserByName(String username) throws Exception {

		try{
			
			User u=userDao.getUserByName(username);
			return u;
		}
		catch (Exception e) 
		{
			throw new Exception("service_layer",e);
		}
	}
	
	
	@Transactional
	public User save(User user) {

		User master = null;
		try {
			master = em.merge(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public List<Lookup> getAll(String setname) {
		List<Lookup> result = new ArrayList<Lookup>() ;
		try
		{		
			String sql = "SELECT l FROM Lookup l WHERE l.lk_setname= :val1 AND l.lk_rec_status=1 AND l.lk_parent=0 ORDER BY l.lk_longname";
			result = em.createQuery(sql).setParameter("val1", setname).getResultList();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public User getUserDetail(Long um_id) 
	{
		User result = new User();
		
		String query = "select um from User um where um.um_id="+um_id;
		
		result = (User) em.createQuery(query).getSingleResult(); 
		
		System.out.println("Result is :" +result);
		
		return result;
	}

	@Transactional
	public List<User> getAllUser() {
		
		List<User> result =new ArrayList<User>();
		
		String query="Select um from User um where um.rec_status=1";
		
		result = em.createQuery(query).getResultList();
		
		System.out.println("Result is :" +result);
		
		return result;
	}

	@Transactional
	public void update(User user) {
		
		User u=em.merge(user);
	}

	@Transactional
	@Override
	public void saveLoginLog(LoginLog log,User user) 
	{
		
		try 
		{
			userDao.saveLoginLog(log,user);
		} 
		catch (Exception e) 
		{
			
		}
	}
	
	@Transactional
	public User getChangepassword(long uid)
	{
		User master=null;
		try {
			 master =em.find(User.class, uid);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return master;
	}

	@Override
	public List<ObjectMaster> getUserObjects(Long um_id) {

		String sql = "select o from ObjectMaster o "
				+ " where o.om_id in (select ro_om_mid from  RoleObject where ro_role_id in (select ur_role_id from UserRole where ur_um_mid = "+um_id+") and ro_rec_status = 1)";
		List<ObjectMaster> l1 = (List<ObjectMaster>) em.createQuery(sql)
				.getResultList();

		for (int i = 0; i < l1.size(); i++) {
			//System.out.println(l1.get(i));
			ObjectMaster om = l1.get(i);
			//System.out.println(om.getOm_object_name());
		}
		return l1;
	}
	

	@Transactional
	public User getUser(Long id) {

		User master =em.find(User.class,id);
		return master;
	}
	
	@Transactional
	public List<User> getUserByRole(String role) {
		List<User> result = new ArrayList<User>();
		Query query = em
				.createQuery("select u from User u where u.um_id in (select ur_um_mid from UserRole where ur_role_id in (select lk_id from Lookup where lk_longname = :role)) order by u.um_fullname");
		query.setParameter("role", role);

		try {
			result = (List<User>) query.getResultList();
		} catch (Exception e) {
			throw new EntityNotFoundException("Entity Does Not Exit");
		} finally {
			return result;
		}

	}

	@Override
	public Department getDeptById(Long um_department_id) {
		Department dept = null;
		try 
		{
			dept= em.find(Department.class,  um_department_id); 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dept;
	}
	
	@Transactional
	public List<LoginLog> getLoginLog(Long userId) {
		List<LoginLog> result = new ArrayList<LoginLog>();
		
		String sql = "select l from LoginLog l where ll_user_mid = "+userId+" and ll_logout_time is null";
		
		Query query = em.createQuery(sql);
		
		result = (List<LoginLog>) query.getResultList();
		
		
		return result;
	}

	@Override
	public List<SubDepartment> getSubDepartmentIds(Long dpt_id) {
		List<SubDepartment> sub_dept = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("Select sub from SubDepartment sub where parent_id=:p_id");
			query.setParameter("p_id", dpt_id);
			sub_dept= query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sub_dept;
	}

	@Override
	public List<Permission> getUserPermittedSubDepts(Long um_id)
	{
		List<Permission> um_perm = new ArrayList<>();
		try 
		{
			Query query = em.createQuery("Select p  from Permission p where pm_um_mid=:id");
			query.setParameter("id", um_id);
			um_perm= query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return um_perm;
	}

	@Override
	@Transactional
	public void saveUserSubDeptPermissions(List<Permission> perm) throws Exception 
	{
		try 
		{
			for(Permission per : perm)
			{
				if(!per.isValue() )
				{
					per.setPm_rec_status(0);
				}
				else if(per.isValue())
				{
					per.setPm_rec_status(1);
				}
				em.merge(per);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception();
		}
		
	}

	@Override
	public List<Department> getAllDepartment() {
		
			List<Department> sub_dept = new ArrayList<>();
			try 
			{
				Query query = em.createQuery("Select d from Department d where d.status=1");
				sub_dept= query.getResultList();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sub_dept;
		}
	
}
