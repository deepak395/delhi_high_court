package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.ObjectTree;
import com.mdi.model.ObjectMaster;
import com.mdi.model.RoleObject;
import com.mdi.service.ObjectMasterService;

@Service
public class ObjectMasterServiceImpl implements ObjectMasterService{

	@PersistenceContext
	public EntityManager em;
	
	
	@Transactional
	public ObjectMaster save(ObjectMaster s) {

		ObjectMaster master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	@Transactional
	public List<RoleObject> getDataByRoleID(Long roleID) {
		List<RoleObject> result =  new ArrayList<RoleObject>();
		Query query = em
				.createQuery("select ro from RoleObject ro WHERE ro_role_id =:roleID ");		
		try {
			result = query.setParameter("roleID", roleID).getResultList();
			System.out.println("role id List"+result.size());
		} catch (Exception e) {
			throw new EntityNotFoundException("Entity does not exist.");
		} finally {
			return result;
		}
		
	}

	@SuppressWarnings({ "unchecked", "finally" })
	@Transactional
	public List<ObjectTree> getRoleTree() {
		List<ObjectTree> result =  new ArrayList<ObjectTree>();
		Query query = em
				.createQuery("select ob from ObjectTree ob WHERE om_parent_id =0 ");		
		try {
			result = query.getResultList();
			System.out.println("role List"+result.size());
		} catch (Exception e) {
			throw new EntityNotFoundException("Entity does not exist.");
		} finally {
			return result;
		}
		
	}
}
