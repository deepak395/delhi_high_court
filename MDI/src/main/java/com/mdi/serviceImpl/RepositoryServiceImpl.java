package com.mdi.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Repository;
import com.mdi.service.RepositoryService;

@Service
public class RepositoryServiceImpl implements RepositoryService{

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public Repository getRepository(Long id) {
	
		Repository r= em.find(Repository.class, id);
		return r;
	}
	
	/*@Transactional
	public Repository getRepositoryById(Long id) {
		
		Repository r= em.find(Repository.class, id);
		return r;
	}*/
}
