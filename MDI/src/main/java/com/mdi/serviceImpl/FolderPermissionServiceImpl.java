package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.dao.FolderPermissionDao;
import com.mdi.model.Folder;
import com.mdi.model.Permission;
import com.mdi.model.Repository;
import com.mdi.service.FolderPermissionService;

@Service
public class FolderPermissionServiceImpl implements FolderPermissionService
{
	
	@Autowired
	private FolderPermissionDao permissiondao;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Folder> getNotAssignedFolders(Long userId) throws Exception
	{
		List<Folder> folders= new ArrayList<Folder>();
		try 
		{
			folders = permissiondao.getNotAssignedFolders(userId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return folders;
	}

	@Override
	public Permission checkFolderexist(Folder f, Long userId) throws Exception 
	{
		Permission folderexist = null;
		try 
		{
			folderexist =permissiondao.checkFolderexist(f,userId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return folderexist;
	}

	@Override
	@Transactional
	public  Permission save(Permission perm) throws Exception 
	{
		Permission master = null;
		try 
		{
			master=permissiondao.save(perm);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return master;
	}

	@Override
	public List<Repository> getNotAssignedRepositories(Long userId) throws Exception 
	{
		List<Repository> repositories= new ArrayList<Repository>();
		try 
		{
			repositories = permissiondao.getNotAssignedRepositories(userId);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return repositories;
	}

	@Override
	public Permission checkRepositoryexist(Repository r, Long userId) throws Exception 
	{
		Permission perm= null;
		try 
		{
			perm = permissiondao.checkRepositoryexist( r, userId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return perm;
	}

	@Override
	public List<Permission> getPermissionByUser(Long userId) {
		List<Permission> permissions= new ArrayList<Permission>();
		try
		{
			Query query  =  em.createQuery("SELECT p from Permission p where p.userId=:userId order by p.id)");
			query.setParameter("userId", userId);
			permissions= query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return permissions;
	}

	@Override
	public Repository getRepository(Long value) 
	{
		try
	    {   
	
		   Repository r= em.find(Repository.class, value); 
		   return r;
		   
	    }
	
	    catch(Exception ex)
	    {
	    	ex.printStackTrace();
	    	return null;
	    }
	}

	@Override
	public Folder getFolderById(Long value)
	{

		Folder r= new Folder();
		try 
		{
			Query query  =  em.createQuery("SELECT r from Folder r WHERE r.id =:id");
			query.setParameter("id", value);
			r= (Folder) query.getSingleResult();
			
		} 
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		return r;
	}

	@Override
	public Permission checkPermissionexist(Long rep_id, Long userId, int i) {
		System.out.println(rep_id+" "+userId+" "+i);
		Permission permission=new Permission();
		try 
		{
			Query query  =  em.createQuery("SELECT p from Permission p where p.userId=:userId AND p.value=:value AND type=:type and status=1");
			query.setParameter("userId", userId);
			query.setParameter("value", rep_id);
			query.setParameter("type", i);
			permission= (Permission) query.getSingleResult();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		return permission;
	}

	@Override
	@Transactional
	public void updateChildFolders(List<Long> folderids, Long userId, Integer status) {
		Query query  =  em.createQuery("UPDATE Permission p set p.status=:status where p.value In(:values) AND p.userId=:userId AND type=2");
		query.setParameter("userId", userId);
		query.setParameter("status", status);
		query.setParameter("values", folderids);
		query.executeUpdate();
		
	}

}
