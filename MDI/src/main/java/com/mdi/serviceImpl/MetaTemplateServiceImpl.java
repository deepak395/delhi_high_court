package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.poi.hssf.record.DConRefRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.dao.FolderDao;
import com.mdi.dao.MetaTemplateDao;
import com.mdi.dao.UserDao;
import com.mdi.dto.DepartmentDetailsDto;
import com.mdi.model.Department;
import com.mdi.model.Document_type;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;
import com.mdi.model.User;
import com.mdi.service.MetaTemplateService;


@Service
public class MetaTemplateServiceImpl implements MetaTemplateService
{
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	private MetaTemplateDao dao;
	
	@Autowired
	private UserDao userdao;
	
	@Autowired
	private FolderDao folderdao;

	@Override
	public void SaveDepartMent(Department department) throws Exception
	{
		
		try 
		{
			dao.SaveDepartMent(department);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("service_layer",e);
		}
	}

	@Override
	public List<DepartmentDetailsDto> getAllDepartMentList() throws Exception {
		List<Department> list = new ArrayList<Department>();
		try 
		{
			list = dao.getAllDepartMentList();
			List<DepartmentDetailsDto> dept_details=departmentDetailsDtolist(list);
			return dept_details;
		} 
		catch (Exception e) 
		{
			throw new Exception("Service_layer",e);
		}
		
	}
	 
	public List<DepartmentDetailsDto> departmentDetailsDtolist(List<Department> dept)
	{
		List<DepartmentDetailsDto> dept_details = new ArrayList<>();
		for(Department department :dept)
		{
			DepartmentDetailsDto dto = new DepartmentDetailsDto();
			dto.setDept_id(department.getDept_id());
			dto.setDept_name(department.getDept_name());
			try 
			{
			   User user=	userdao.getUserById(department.getDept_cr_by());
			   Repository repo= folderdao.getRepositoryById(department.getDept_repo_id());
			  // dto.setDept_cr_by(user.getUsername());
			 //  dto.setDept_cr_by_id(user.getUm_id());
			   dto.setDept_repo_id(repo.getId());
			   dto.setDept_repo_name(repo.getName());
			   dept_details.add(dto);
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return dept_details;
	}
	
	@Override
	@Transactional
	public void saveSubDepartment(SubDepartment subdept) throws Exception {
		try 
		{
			dao.saveSubDepartment(subdept);
		} 
		catch (Exception e) 
		{
			throw new Exception("metatemplate_service_layer",e);
		}
		
	}
	
    public Repository getRepositoryById(Long id) {
		
		Repository r= em.find(Repository.class, id);
		return r;
	}

	@Override
	public Department getAllDepartMentById(Long id) {
		
		Department d=em.find(Department.class,id);
		return d;
	}

	@Override
	@Transactional
	public void saveDocumentType(Document_type doc_type) throws Exception 
	{
		try 
		{
			em.merge(doc_type);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("Meta Template Service",e);
		}
		
	}

	@Override
	public List<Document_type> getDocumentTypes(Long sub_dept_id) 
	{
		List<Document_type> doc_type = new ArrayList<>();
		try 
		{
			Query query=  em.createQuery("select dt from Document_type dt where  dt_sub_mid=:parent and dt_rec_status =1");
			query.setParameter("parent", sub_dept_id);
			doc_type = query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return doc_type;
	}

}
