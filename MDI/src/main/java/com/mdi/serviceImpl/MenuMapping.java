package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Tree;
import com.mdi.model.User;
import com.mdi.service.MenuMappingService;

@Service
public class MenuMapping implements MenuMappingService{

	@PersistenceContext
	private EntityManager em;
	
	
	@Transactional
	public List<User> getAlluserById(){
		
		User master=null;
		 
		try {
				String sql="Select u from User u";
				master =(User) em.createQuery(sql).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (List<User>) master;
		
	}


	@SuppressWarnings({ "unchecked", "finally" })
	@Transactional
	public List<Tree> getRoleTree() {
		List<Tree> result =  new ArrayList<Tree>();
		Query query = em
				.createQuery("select ob  from Tree ob WHERE ob.parentid =0  AND ob.om_rec_status=1");		
		try {
			result = query.getResultList();
			System.out.println("role List"+result.size());
		} catch (Exception e) {
			throw new EntityNotFoundException("Entity does not exist.");
		} finally {
			return result;
		}
		
	}
}
