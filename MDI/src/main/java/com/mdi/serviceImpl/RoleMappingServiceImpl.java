package com.mdi.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.RoleObject;
import com.mdi.service.RoleMappingService;

@Service
public class RoleMappingServiceImpl implements RoleMappingService{

	@PersistenceContext
	public EntityManager em;
	
	@Override
	public RoleObject getByRoleAndObject(Long role_id, Long ro_om_mid) {
		RoleObject roleObject=new RoleObject();
		
		try {
			Query query = em.createQuery("SELECT r FROM RoleObject r  WHERE  r.ro_role_id =:roleID  AND  r.ro_om_mid=:ro_om_mid and r.ro_rec_status = 1");
			roleObject=(RoleObject) query.setParameter("roleID", role_id).setParameter("ro_om_mid", ro_om_mid).getSingleResult();
		} catch (Exception e) {
				e.printStackTrace();
		}
		return roleObject;
	}

	@Transactional	
	public RoleObject save(RoleObject roleObject) {
		
		RoleObject master=null;
		try {
			  master=em.merge(roleObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}

}
