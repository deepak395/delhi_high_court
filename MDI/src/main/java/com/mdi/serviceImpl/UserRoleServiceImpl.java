package com.mdi.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.UserRole;
import com.mdi.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@PersistenceContext
	private EntityManager em;

	@Override
	public UserRole getUserRoleById(long userId) {
		UserRole result=new UserRole();
		
		try {

			Query query =em.createQuery("Select ur From UserRole ur where ur.ur_um_mid=:userId");
			query.setParameter("userId", userId);
			result=(UserRole)query.getSingleResult();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new EntityNotFoundException("Entity does not exits");
		}
		finally{
			return result;
		}
		
	}

	@Transactional
	public UserRole save(UserRole userRole) {
		UserRole master=null;
		
		try {
			
			master=em.merge(userRole);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}

	
}
