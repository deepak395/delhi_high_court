package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Department;
import com.mdi.model.Lookup;
import com.mdi.service.LookupService;

@Service
public class LookupServiceImpl implements LookupService{

	@PersistenceContext
	private EntityManager em;
	
	
	@Transactional
	public List<Lookup> getAll(String setname) {
		List<Lookup> result = new ArrayList<Lookup>() ;
		try
		{		
			String sql = "SELECT l FROM Lookup l WHERE l.lk_setname= :val1 AND l.lk_rec_status=1 AND l.lk_parent=0 ORDER BY l.lk_longname";
			result = em.createQuery(sql).setParameter("val1", setname).getResultList();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	@Transactional
	public List<Department> getAllDepartment() {
		List<Department> result = new ArrayList<Department>() ;
		try
		{		
			String sql = "SELECT d FROM Department d  ORDER BY d.dept_name";
			result = em.createQuery(sql).getResultList();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	@Transactional
	public List<Lookup> CheckRegex(String setname) {
		List<Lookup> result = new ArrayList<Lookup>() ;
		try
		{		
			String sql = "SELECT l FROM Lookup l WHERE l.lk_setname=:setname AND l.lk_rec_status=1 ORDER BY l.lk_longname";
			result = em.createQuery(sql).setParameter("setname", setname).getResultList();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	@Override
	public List<Lookup> getRoleList(String rolename) {
		List<Lookup> result = new ArrayList<Lookup>() ;
		try
		{
			String sql = "SELECT l FROM Lookup l WHERE l.lk_setname= :val1 AND l.lk_rec_status=1 ORDER BY l.lk_id";
		    result = em.createQuery(sql).setParameter("val1", rolename).getResultList();
		System.out.println("get list size ==  "+result.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public Lookup getLookUpObject(String setname) {
		Lookup result = new Lookup() ;
		try{			
			String sql = "SELECT l FROM Lookup l WHERE l.lk_setname= :setname  AND l.lk_rec_status=1";
			result = (Lookup) em.createQuery(sql).setParameter("setname", setname).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}return result;
	}
}
