package com.mdi.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.model.Document;
import com.mdi.model.Folder;
import com.mdi.service.DocumentService;

@Service
public class DocumentServiceImpl implements DocumentService{


	@PersistenceContext
	private EntityManager em;
	

	public Folder getChildFolderId(long folderid) {
			
		Folder result =null;
		try
		{
			String sql="SELECT f from Folder f Where (id=:fid and status=1)";
			result =  (Folder) em.createQuery(sql).setParameter("fid",folderid).getSingleResult();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	
	}
	public Folder getParentFolderId(long folderid) {
		
		Folder result =null;
		try
		{
			String sql="SELECT f from Folder f Where (id=:fid and status=1)";
			result =  (Folder) em.createQuery(sql).setParameter("fid",folderid).getSingleResult();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	
	}
	@Transactional
	public Document save(Document d) {
		// TODO Auto-generated method stub

		Document master = null;
    	try {	
    		master= em.merge(d);	    	
	    }catch (Exception e) {		
	    	e.printStackTrace();
		}
    	return master;
	}
	
}
