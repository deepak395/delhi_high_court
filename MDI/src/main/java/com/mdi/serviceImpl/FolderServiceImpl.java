package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.dao.FolderDao;
import com.mdi.dao.MetaTemplateDao;
import com.mdi.model.Department;
import com.mdi.model.Folder;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;
import com.mdi.service.FolderService;


@Service
public class FolderServiceImpl implements FolderService
{
	@Autowired
	private FolderDao dao;
	
	@Autowired
	private MetaTemplateDao metadao;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Repository> getRepository() throws Exception 
	{
		List<Repository> list = new ArrayList<Repository>();
		try 
		{
		   list=	dao.getRepository();
		} 
		catch (Exception e) 
		{
			
			e.printStackTrace();
			throw new Exception("Service_layer",e);
		}
		return list;
	}

	@Override
	public List<Department> getDepartments() throws Exception
	{
		List<Department> list = new ArrayList<>();
		try
		{
			list=metadao.getAllDepartMentList();
		} 
		catch (Exception e) 
		{
			throw new Exception("folderService_laye",e);
		}
		return list;
	}

	@Override
	@Transactional
	public void saveFolder(Folder folder) throws Exception
	{
		try 
		{
		   dao.saveFolder(folder);
		  
		} 
		catch (Exception e) 
		{
			throw new Exception("FolderService_layer",e);
		}
		
	}

	@Override
	public List<Folder> getAllFolders() throws Exception
	{
		List<Folder> list = new ArrayList<>();
		try 
		{
			list=dao.getAllFolders();
			return list;
		} 
		catch (Exception e) 
		{
			throw new Exception("Folder ServiceLayer",e);
		}
		
	}

	@Override
	public Department getDepartmentById(Long id) throws Exception 
	{
		Department dept= null;
		try 
		{
			dept=dao.getDepartmentById(id);
			return dept;
		} 
		catch (Exception e) 
		{
			throw new Exception("Folder_Service",e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Folder> getFoldersByDeptid(Long dept_id) 
	{
		List<Folder> list = new ArrayList<>();
		try 
		{
			Query query =em.createQuery("SELECT f from Folder f Where (dept_id=:dpt_id and status=1)");
			query.setParameter("dpt_id", dept_id);
			list=query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public SubDepartment getSubDepartmentById(Long id) {
		SubDepartment sub_dept = null; 
		try
		{
			sub_dept = em.find(SubDepartment.class, id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sub_dept;
	}

	@Override
	@Transactional
	public Folder getFolderById(Long value) 
	{
		Folder r= new Folder();
		try {
			Query query  =  em.createQuery("SELECT r from Folder r WHERE r.id =:id");
			query.setParameter("id", value);
			r= (Folder) query.getSingleResult();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return r;
	}

	@Override
	public List<Folder> getFoldersByParentId(Long folderId) {
		List<Folder> folders= new ArrayList<Folder>();
		try 
		{
			Query query  =  em.createQuery("SELECT r from Folder r WHERE r.parent_id =:folderId");
			query.setParameter("folderId", folderId);
			folders= query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return folders;
	}
}
