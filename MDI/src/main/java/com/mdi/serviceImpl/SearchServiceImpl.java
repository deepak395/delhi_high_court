package com.mdi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.dto.MetaDataSearch;
import com.mdi.model.Department;
import com.mdi.model.DocumentFileDetails;
import com.mdi.model.IndexField;
import com.mdi.model.Repository;
import com.mdi.model.SubDepartment;
import com.mdi.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Repository> getRepositories() {
		List<Repository> master = em.createQuery("Select r from Repository r").getResultList();
		return master;
	}

	public List<DocumentFileDetails> getAllWithoutIndex() {
		List<DocumentFileDetails> result = em.createQuery("SELECT d FROM DocumentFileDetails d where d.dfd_index=0")
				.getResultList();
		return result;
	}

	@Transactional
	public DocumentFileDetails save(DocumentFileDetails d) {
		// TODO Auto-generated method stub

		DocumentFileDetails master = null;
		try {
			master = em.merge(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}

	@Override
	public SubDepartment getSubDeptById(Long s_dept_id) {
		SubDepartment s_dept = null;
		try {
			s_dept = em.find(SubDepartment.class, s_dept_id);
			return s_dept;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Department getDeptById(Long parent_id) {
		Department dept = null;
		try {
			dept = em.find(Department.class, parent_id);
			return dept;
		} catch (Exception e) {

		}
		return null;
	}

	@Transactional
	public List<DocumentFileDetails> getcaseFilesBySearchqueryContent(String querystring) {
		// List<ReportsView> documentFileDetails = new ArrayList<ReportsView>();
		List<DocumentFileDetails> dfd = new ArrayList<>();

		// Query query = em.createNativeQuery("SELECT * FROM
		// document_file_details WHERE dfd_id ="+querystring+" limit
		// 1000",DocumentFileDetails.class);
		Query query = em.createNativeQuery("SELECT * FROM mdi.document_file_details  WHERE dfd_id =4 limit 1000",
				DocumentFileDetails.class);

		try {
			Query q = em.createQuery("select dfd from DocumentFileDetails  dfd where dfd." + querystring);
			dfd = q.getResultList();
			// documentFileDetails = query.getResultList();
		} catch (Exception e) {
			throw new EntityNotFoundException("Entity Does Not Exit");
		} finally {
			return dfd;
		}

	}

	@Override
	public IndexField getindexfieldsById(Long if_id) {
		IndexField in = null;
		try {
			in = em.find(IndexField.class, if_id);
			return in;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<DocumentFileDetails> getSearchFiles(List<MetaDataSearch> mds, Long dept_id, Long sub_dept_id,
			Long dt_id,Long md_year,Long fd_id) throws Exception {
		List<DocumentFileDetails> dfd = new ArrayList<DocumentFileDetails>();
		System.out.println("MEA" + dept_id + " " + sub_dept_id);
		try 
		{
			int count = 0;
			String sql = "select dfd from DocumentFileDetails dfd where dfd.dfd_dept_mid=" + dept_id;
			if (sub_dept_id != null) 
			{
				sql = sql + " and dfd.dfd_sub_dept_mid=" + sub_dept_id;
			}
			if(dt_id!=0l)
			{
				
				sql = sql + " and dfd.dfd_dt_mid=" + dt_id;
			}
			if(md_year!=0l)
			{
				sql = sql + " and dfd.dfd_year=" + md_year;
			}
			
			if(fd_id!=0l)
			{
				sql = sql + " and dfd.dfd_fd_mid=" + fd_id;
			}
			if (mds.get(0).getIf_id() != null) 
			{
				sql = sql + " and  dfd.dfd_id in (select md.md_fd_mid from MetaData md where ";
				for (MetaDataSearch m : mds) 
				{
					System.out.println(m.getIf_value());
					if (count == 0) 
					{
						sql = sql + " (md.md_mf_mid=" + m.getIf_id() + " and lower(md.md_value) LIKE  lower('%" + m.getIf_value()
								+ "%')) ";
					} 
					else if (mds.size() - 1 == count) 
					{
						sql = sql + " or md.md_fd_mid in (select md.md_fd_mid from MetaData md where(md.md_mf_mid = "
								+ m.getIf_id() + " and lower(md.md_value) LIKE lower('%" + m.getIf_value() + "%'))";
					} 
					else 
					{
						sql = sql + "or md.md_fd_mid in (select md.md_fd_mid from MetaData md where(md.md_mf_mid ="
								+ m.getIf_id() + " and lower(md.md_value) LIKE lower('%" + m.getIf_value() + "%'))";
					}
					
					
					count++;
				}
			}
			
			while (count > 0) 
			{
				sql = sql + ")";
				count--;
			}
			System.out.println(sql);
			dfd = (List<DocumentFileDetails>) em.createQuery(sql).getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("SearchService", e);
		}
		return dfd;
	}

	@Override
	public List<String> getindexfields(Long sub_dept_id) {
		List<String> list = new ArrayList<>();
		try {
			Query query = em.createQuery(
					"SELECT ifd.if_name from IndexField ifd Where (if_parent=:id and if_rec_status=0) order by if_sequence");
			query.setParameter("id", sub_dept_id);
			list = query.getResultList();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
