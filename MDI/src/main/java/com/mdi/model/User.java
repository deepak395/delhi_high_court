
package com.mdi.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "user_master")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_masterseq")
	@SequenceGenerator(name = "user_masterseq", sequenceName = "user_masterseq", allocationSize = 1)
	@Column(name = "um_id")
	private Long um_id;

	@Column(name = "um_username")
	private String username;

	@Column(name = "um_password")
	private String password;

	@Column(name = "um_last_login")
	private Date last_login;

	@Column(name = "um_cr_by")
	private Long cr_by;

	@Column(name = "um_cr_date")
	private Date cr_date;

	@Column(name = "um_mod_by")
	private Long mod_by;

	@Column(name = "um_mod_date")
	private Date mod_date;

	@Column(name = "um_rec_status")
	private Integer rec_status;

	@Column(name = "um_vendor_id")
	private Long um_vendor_id;

	@Column(name = "um_department_id")
	private Long um_department_id;

	@Column(name = "um_designation")
	private Long um_designation;

	@Column(name = "um_fullname")
	private String um_fullname;

	@Column(name = "um_pass_validity_date")
	private Date um_pass_validity_date;

	@Column(name = "roaster_fr_date")
	private Date roaster_fr_date;

	@Column(name = "roaster_to_date")
	private Date roaster_to_date;

	@Column(name = "um_shift_lid")
	private Long um_shift_id;

	@Column(name = "um_bench_code")
	private Long um_bench_code;

	@Column(name = "um_avilable_status")
	private String um_avilable_status;

	@Column(name = "um_parent_id")
	private Long um_parent_id;

	@Column(name = "um_scanserialno_id")
	private Long um_scanserialno_id;

	@Column(name = "um_email_id")
	private String um_email_id;

	
	@Column(name = "um_altemail_id")
	private String um_altemail_id;
	
	@Column(name="um_download_status")
	private Integer um_download_status;
	
	
	
	
	@Transient
	private Boolean checkbox;

	@Column(name = "pdf_viewer_speed")
	private Integer pdf_viewer_speed;

	@Column(name = "um_account_activation")
	private Integer um_account_activation;

	@Column(name = "priority")
	private Integer priority;
	
	@Transient
	private String captcha;

	@Transient
	private String confirmpassword;
	
	@Column(name = "um_login_attemps")
	private Integer um_login_attemps;

	@Column(name = "isfirstlogin")
	private Boolean isfirstlogin;
	
	@Column(name = "um_otp")
	private Integer um_otp;

	@Column(name = "um_otp_intime")
	private Date um_otp_intime;
	
	@Column(name = "um_otp_exptime")
	private Date um_otp_exptime;
	
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, orphanRemoval=true)
	@JoinColumn(name = "ur_um_mid",referencedColumnName="um_id",insertable = false, updatable = false)	
	private List<UserRole> userroles;
	
	@ManyToOne
    @JoinColumn(name="um_bench_code",referencedColumnName="lk_id",insertable = false, updatable = false)
    private Lookup lk_benchcode;
	
	
	@Transient 
	private Long um_role_id;

	
	public Integer getUm_download_status() {
		return um_download_status;
	}

	public void setUm_download_status(Integer um_download_status) {
		this.um_download_status = um_download_status;
	}

	public Integer getUm_otp() {
		return um_otp;
	}

	public void setUm_otp(Integer um_otp) {
		this.um_otp = um_otp;
	}

	public Integer getUm_login_attemps() {
		return um_login_attemps;
	}

	public void setUm_login_attemps(Integer um_login_attemps) {
		this.um_login_attemps = um_login_attemps;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public Long getUm_shift_id() {
		return um_shift_id;
	}

	public void setUm_shift_id(Long um_shift_id) {
		this.um_shift_id = um_shift_id;
	}

	public Long getUm_bench_code() {
		return um_bench_code;
	}

	public void setUm_bench_code(Long um_bench_code) {
		this.um_bench_code = um_bench_code;
	}

	public Long getUm_designation() {
		return um_designation;
	}

	public void setUm_designation(Long um_designation) {
		this.um_designation = um_designation;
	}

	/*
	 * @Transient private Long um_role_id;
	 * 
	 * @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,
	 * orphanRemoval=true)
	 * 
	 * @JoinColumn(name = "ur_um_mid",referencedColumnName="um_id",insertable =
	 * false, updatable = false) private List<UserRole> userroles;
	 * 
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="um_bench_code",referencedColumnName="lk_id",insertable =
	 * false, updatable = false) private Lookup lk_benchcode;
	 * 
	 * 
	 * @OneToOne(optional=true)
	 * 
	 * @JoinColumn(name="um_vendor_id",referencedColumnName="lk_id",insertable =
	 * false, updatable = false) private Lookup lk_team;
	 * 
	 * 
	 * @OneToOne(optional=true)
	 * 
	 * @JoinColumn(name="um_shift_lid",referencedColumnName="lk_id",insertable =
	 * false, updatable = false) private Lookup lk_shift;
	 * 
	 * 
	 * public Lookup getLk_benchcode() { return lk_benchcode; }
	 * 
	 * public void setLk_benchcode(Lookup lk_benchcode) { this.lk_benchcode =
	 * lk_benchcode; }
	 */
	
	
	
	
	
	public Long getUm_id() {
		return um_id;
	}

	public String getUm_altemail_id() {
		return um_altemail_id;
	}

	public void setUm_altemail_id(String um_altemail_id) {
		this.um_altemail_id = um_altemail_id;
	}

	public void setUm_id(Long um_id) {
		this.um_id = um_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getLast_login() {
		return last_login;
	}

	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}

	public Long getCr_by() {
		return cr_by;
	}

	public void setCr_by(Long cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public Long getMod_by() {
		return mod_by;
	}

	public void setMod_by(Long mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public Integer getRec_status() {
		return rec_status;
	}

	public void setRec_status(Integer rec_status) {
		this.rec_status = rec_status;
	}

	public Long getUm_vendor_id() {
		return um_vendor_id;
	}

	public void setUm_vendor_id(Long um_vendor_id) {
		this.um_vendor_id = um_vendor_id;
	}

	public String getUm_fullname() {
		return um_fullname;
	}

	public void setUm_fullname(String um_fullname) {
		this.um_fullname = um_fullname;
	}

	public Date getUm_pass_validity_date() {
		return um_pass_validity_date;
	}

	public void setUm_pass_validity_date(Date um_pass_validity_date) {
		this.um_pass_validity_date = um_pass_validity_date;
	}

	/*
	 * public List<UserRole> getUserroles() { return userroles; }
	 * 
	 * public void setUserroles(List<UserRole> userroles) { this.userroles =
	 * userroles; }
	 * 
	 * public Long getUm_role_id() { return um_role_id; }
	 * 
	 * public void setUm_role_id(Long um_role_id) { this.um_role_id = um_role_id; }
	 */
	public String getUm_avilable_status() {
		return um_avilable_status;
	}

	public void setUm_avilable_status(String um_avilable_status) {
		this.um_avilable_status = um_avilable_status;
	}

	public Date getRoaster_fr_date() {
		return roaster_fr_date;
	}

	public void setRoaster_fr_date(Date roaster_fr_date) {
		this.roaster_fr_date = roaster_fr_date;
	}

	public Date getRoaster_to_date() {
		return roaster_to_date;
	}

	public void setRoaster_to_date(Date roaster_to_date) {
		this.roaster_to_date = roaster_to_date;
	}

	public Boolean getCheckbox() {
		return checkbox;
	}

	public void setCheckbox(Boolean checkbox) {
		this.checkbox = checkbox;
	}

	/*
	 * public Lookup getLk_team() { return lk_team; }
	 * 
	 * public void setLk_team(Lookup lk_team) { this.lk_team = lk_team; }
	 * 
	 * public Lookup getLk_shift() { return lk_shift; }
	 * 
	 * public void setLk_shift(Lookup lk_shift) { this.lk_shift = lk_shift; }
	 */

	
	public Long getUm_parent_id() {
		return um_parent_id;
	}

	public Date getUm_otp_intime() {
		return um_otp_intime;
	}

	public void setUm_otp_intime(Date um_otp_intime) {
		this.um_otp_intime = um_otp_intime;
	}

	public Date getUm_otp_exptime() {
		return um_otp_exptime;
	}

	public void setUm_otp_exptime(Date um_otp_exptime) {
		this.um_otp_exptime = um_otp_exptime;
	}

	public void setUm_parent_id(Long um_parent_id) {
		this.um_parent_id = um_parent_id;
	}

	public Long getUm_scanserialno_id() {
		return um_scanserialno_id;
	}

	public void setUm_scanserialno_id(Long um_scanserialno_id) {
		this.um_scanserialno_id = um_scanserialno_id;
	}

	public Long getUm_department_id() {
		return um_department_id;
	}

	public void setUm_department_id(Long um_department_id) {
		this.um_department_id = um_department_id;
	}

	public Integer getPdf_viewer_speed() {
		return pdf_viewer_speed;
	}

	public void setPdf_viewer_speed(Integer pdf_viewer_speed) {
		this.pdf_viewer_speed = pdf_viewer_speed;
	}

	public Integer getUm_account_activation() {
		return um_account_activation;
	}

	public void setUm_account_activation(Integer um_account_activation) {
		this.um_account_activation = um_account_activation;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getUm_email_id() {
		return um_email_id;
	}

	public void setUm_email_id(String um_email_id) {
		this.um_email_id = um_email_id;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public Long getUm_role_id() {
		return um_role_id;
	}

	public void setUm_role_id(Long um_role_id) {
		this.um_role_id = um_role_id;
	}

	public List<UserRole> getUserroles() {
		return userroles;
	}

	public void setUserroles(List<UserRole> userroles) {
		this.userroles = userroles;
	}

	public Lookup getLk_benchcode() {
		return lk_benchcode;
	}

	public void setLk_benchcode(Lookup lk_benchcode) {
		this.lk_benchcode = lk_benchcode;
	}

	public Boolean getIsfirstlogin() {
		return isfirstlogin;
	}

	@Override
	public String toString() {
		return "User [um_id=" + um_id + ", username=" + username + ", password=" + password + ", last_login="
				+ last_login + ", cr_by=" + cr_by + ", cr_date=" + cr_date + ", mod_by=" + mod_by + ", mod_date="
				+ mod_date + ", rec_status=" + rec_status + ", um_vendor_id=" + um_vendor_id + ", um_department_id="
				+ um_department_id + ", um_designation=" + um_designation + ", um_fullname=" + um_fullname
				+ ", um_pass_validity_date=" + um_pass_validity_date + ", roaster_fr_date=" + roaster_fr_date
				+ ", roaster_to_date=" + roaster_to_date + ", um_shift_id=" + um_shift_id + ", um_bench_code="
				+ um_bench_code + ", um_avilable_status=" + um_avilable_status + ", um_parent_id=" + um_parent_id
				+ ", um_scanserialno_id=" + um_scanserialno_id + ", um_email_id=" + um_email_id + ", checkbox="
				+ checkbox + ", pdf_viewer_speed=" + pdf_viewer_speed + ", um_account_activation="
				+ um_account_activation + ", priority=" + priority + ", captcha=" + captcha + ", confirmpassword="
				+ confirmpassword + ", um_login_attemps=" + um_login_attemps + ", isfirstlogin=" + isfirstlogin
				+ ", userroles=" + userroles + ", um_role_id=" + um_role_id + "]";
	}

	public void setIsfirstlogin(Boolean isfirstlogin) {
		this.isfirstlogin = isfirstlogin;
	}

	
}
