package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "document_file_stage")
public class DocumentFileStage {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_file_stage_seq")
	@SequenceGenerator(name = "document_file_stage_seq", sequenceName = "document_file_stage_seq", allocationSize = 1)

	@Column(name = "dfs_id")
	private Long dfs_id;

	@Column(name = "dfs_dfd_mid")
	private Long dfs_dfd_mid;

	@Column(name = "dfs_stage_lid")
	private Long dfs_stage_lid;

	@Column(name = "dfs_stage_date")
	Date dfs_stage_date;

	@Column(name = "dfs_cr_by")
	private Long dfs_cr_by;

	@Column(name = "dfs_cr_date")
	Date dfs_cr_date;

	@Column(name = "dfs_rec_status")
	private Integer dfs_rec_status;

	@Column(name = "dfs_remark")
	private String dfs_remark;

	@Column(name = "dfs_assign_to")
	private Long dfs_assign_to;

	@Column(name = "dfs_reject_status")
	private char dfs_reject_status;

	@Column(name = "dfs_index")
	private Long dfs_index;

	public Long getDfs_id() {
		return dfs_id;
	}

	public void setDfs_id(Long dfs_id) {
		this.dfs_id = dfs_id;
	}

	public Long getDfs_dfd_mid() {
		return dfs_dfd_mid;
	}

	public void setDfs_dfd_mid(Long dfs_dfd_mid) {
		this.dfs_dfd_mid = dfs_dfd_mid;
	}

	public Long getDfs_stage_lid() {
		return dfs_stage_lid;
	}

	public void setDfs_stage_lid(Long dfs_stage_lid) {
		this.dfs_stage_lid = dfs_stage_lid;
	}

	public Date getDfs_stage_date() {
		return dfs_stage_date;
	}

	public void setDfs_stage_date(Date dfs_stage_date) {
		this.dfs_stage_date = dfs_stage_date;
	}

	public Long getDfs_cr_by() {
		return dfs_cr_by;
	}

	public void setDfs_cr_by(Long dfs_cr_by) {
		this.dfs_cr_by = dfs_cr_by;
	}

	public Date getDfs_cr_date() {
		return dfs_cr_date;
	}

	public void setDfs_cr_date(Date dfs_cr_date) {
		this.dfs_cr_date = dfs_cr_date;
	}

	public Integer getDfs_rec_status() {
		return dfs_rec_status;
	}

	public void setDfs_rec_status(Integer dfs_rec_status) {
		this.dfs_rec_status = dfs_rec_status;
	}

	public String getDfs_remark() {
		return dfs_remark;
	}

	public void setDfs_remark(String dfs_remark) {
		this.dfs_remark = dfs_remark;
	}

	public Long getDfs_assign_to() {
		return dfs_assign_to;
	}

	public void setDfs_assign_to(Long dfs_assign_to) {
		this.dfs_assign_to = dfs_assign_to;
	}

	public char getDfs_reject_status() {
		return dfs_reject_status;
	}

	public void setDfs_reject_status(char dfs_reject_status) {
		this.dfs_reject_status = dfs_reject_status;
	}

	public Long getDfs_index() {
		return dfs_index;
	}

	public void setDfs_index(Long dfs_index) {
		this.dfs_index = dfs_index;
	}

}
