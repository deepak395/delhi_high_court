package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="documents")
public class Document {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "documentSeq")
	@SequenceGenerator(name="documentSeq",sequenceName="documentSeq",allocationSize=1)
	
	@Column(name="id")
	private Long id;
	
	@Column(name="document_name")
	private String document_name;
	
	@Column(name="file_name")
	private String file_name;
	
	@Column(name="rep_id")
	private Long rep_id;
	
	@Column(name="folder_id")
	private Long folder_id;
	
	@Column(name="tags")
	private String tags;

	@Column(name="status")
	private Integer status;
	
	@Column(name="doc_page_count")
	private Integer doc_page_count;
	
	@Column(name="DOC_STAGE_LID")
	private Long doc_stage_lid;
	
	@Column(name="DOC_ASSIGN_TO")
	private Long doc_assign_to;
	
	@Column(name="file_extension")
	private String file_extension;
	
	@Column(name="created")
	private Date created;
	
	@Column(name="updated")
	private Date updated;
	
	@Column(name="created_by")
	private Long created_by;
	
	@Column(name="updated_by")
	private Long updated_by;

	@Column(name="doc_reject_status")
	private String doc_reject_status;
	
	@Column(name="dept_id")
	private Long dept_id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocument_name() {
		return document_name;
	}

	public void setDocument_name(String document_name) {
		this.document_name = document_name;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Long getRep_id() {
		return rep_id;
	}

	public void setRep_id(Long rep_id) {
		this.rep_id = rep_id;
	}

	public Long getFolder_id() {
		return folder_id;
	}

	public void setFolder_id(Long folder_id) {
		this.folder_id = folder_id;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFile_extension() {
		return file_extension;
	}

	public void setFile_extension(String file_extension) {
		this.file_extension = file_extension;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Long created_by) {
		this.created_by = created_by;
	}

	public Long getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(Long updated_by) {
		this.updated_by = updated_by;
	}

	public Integer getDoc_page_count() {
		return doc_page_count;
	}

	public void setDoc_page_count(Integer doc_page_count) {
		this.doc_page_count = doc_page_count;
	}

	public Long getDoc_stage_lid() {
		return doc_stage_lid;
	}

	public void setDoc_stage_lid(Long doc_stage_lid) {
		this.doc_stage_lid = doc_stage_lid;
	}

	public String getDoc_reject_status() {
		return doc_reject_status;
	}

	public void setDoc_reject_status(String doc_reject_status) {
		this.doc_reject_status = doc_reject_status;
	}

	public Long getDoc_assign_to() {
		return doc_assign_to;
	}

	public void setDoc_assign_to(Long doc_assign_to) {
		this.doc_assign_to = doc_assign_to;
	}

	public Long getDept_id() {
		return dept_id;
	}

	public void setDept_id(Long dept_id) {
		this.dept_id = dept_id;
	}
	
	
}

