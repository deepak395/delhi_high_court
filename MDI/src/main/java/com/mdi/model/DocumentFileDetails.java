package com.mdi.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "document_file_details")
public class DocumentFileDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_file_details_seq")
	@SequenceGenerator(name = "document_file_details_seq", sequenceName = "document_file_details_seq", allocationSize = 1)

	@Column(name = "dfd_id")
	private Long dfd_id;

	@Column(name = "dfd_dept_mid")
	private Long dfd_dept_mid;

	@Column(name = "dfd_sub_dept_mid")
	private Long dfd_sub_dept_mid;

	@Column(name = "dfd_name")
	private String dfd_name;

	@Column(name = "dfd_year")
	private Long dfd_year;

	@Column(name = "dfd_file_name")
	private String dfd_file_name;

	@Column(name = "dfd_cr_by")
	private Long dfd_cr_by;

	@Column(name = "dfd_cr_date")
	private Date dfd_cr_date;

	@Column(name = "dfd_mod_by")
	private Long dfd_mod_by;

	@Column(name = "dfd_mod_date")
	private Date dfd_mod_date;

	@Column(name = "dfd_rec_status")
	private Integer dfd_rec_status;

	@Column(name = "dfd_stage_lid")
	private Long dfd_stage_lid;

	@Column(name = "dfd_edit_mode")
	private Integer dfd_edit_mode;

	@Column(name = "dfd_page_count")
	private Integer dfd_page_count;

	@Column(name = "dfd_reject_status")
	private char dfd_reject_status;

	@Column(name = "dfd_rep_mid")
	private Long dfd_rep_mid;

	@Column(name = "dfd_assign_to")
	private Long dfd_assign_to;

	@Column(name = "dfd_remark")
	private String dfd_remark;

	@Column(name = "dfd_print_flag")
	private Integer dfd_print_flag;

	@Column(name = "dfd_system_page_count")
	private Integer dfd_system_page_count;
	
	@Column(name="dfd_md_status")
	private String dfd_md_status;
	
	@Column(name="dfd_index")
	private Integer dfd_index;
	
	@Column(name="dfd_dt_mid")
	private Long dfd_dt_mid;
	
	@Column(name="dfd_fd_mid")
	private Long dfd_fd_mid;
	
	
	@ManyToOne(optional = false)
    @JoinColumn(name="dfd_dept_mid",referencedColumnName="dept_id",insertable = false, updatable = false)
    private Department departments;	
	
	@ManyToOne
    @JoinColumn(name="dfd_sub_dept_mid",referencedColumnName="sub_dept_id",insertable = false, updatable = false)
    private SubDepartment subdepartments;	
	
	@ManyToOne
    @JoinColumn(name="dfd_dt_mid",referencedColumnName="dt_id",insertable = false, updatable = false)
    private Document_type doctype;
	
	
	@OneToMany
    @JoinColumn(name="md_fd_mid",referencedColumnName="dfd_id",insertable = false, updatable = false)
    private List<MetaData> metadata;
	

	public Long getDfd_fd_mid() {
		return dfd_fd_mid;
	}

	public void setDfd_fd_mid(Long dfd_fd_mid) {
		this.dfd_fd_mid = dfd_fd_mid;
	}

	public Long getDfd_dt_mid() {
		return dfd_dt_mid;
	}

	public void setDfd_dt_mid(Long dfd_dt_mid) {
		this.dfd_dt_mid = dfd_dt_mid;
	}

	public SubDepartment getSubdepartments() {
		return subdepartments;
	}

	public void setSubdepartments(SubDepartment subdepartments) {
		this.subdepartments = subdepartments;
	}

	public Department getDepartments() {
		return departments;
	}

	public void setDepartments(Department departments) {
		this.departments = departments;
	}

	public String getDfd_md_status() {
		return dfd_md_status;
	}

	public void setDfd_md_status(String dfd_md_status) {
		this.dfd_md_status = dfd_md_status;
	}

	public Long getDfd_id() {
		return dfd_id;
	}

	public void setDfd_id(Long dfd_id) {
		this.dfd_id = dfd_id;
	}

	public Long getDfd_dept_mid() {
		return dfd_dept_mid;
	}

	public void setDfd_dept_mid(Long dfd_dept_mid) {
		this.dfd_dept_mid = dfd_dept_mid;
	}

	public Long getDfd_sub_dept_mid() {
		return dfd_sub_dept_mid;
	}

	public void setDfd_sub_dept_mid(Long dfd_sub_dept_mid) {
		this.dfd_sub_dept_mid = dfd_sub_dept_mid;
	}

	public String getDfd_name() {
		return dfd_name;
	}

	public void setDfd_name(String dfd_name) {
		this.dfd_name = dfd_name;
	}

	public Long getDfd_year() {
		return dfd_year;
	}

	public void setDfd_year(Long dfd_year) {
		this.dfd_year = dfd_year;
	}

	public String getDfd_file_name() {
		return dfd_file_name;
	}

	public void setDfd_file_name(String dfd_file_name) {
		this.dfd_file_name = dfd_file_name;
	}

	public Long getDfd_cr_by() {
		return dfd_cr_by;
	}

	public void setDfd_cr_by(Long dfd_cr_by) {
		this.dfd_cr_by = dfd_cr_by;
	}

	public Date getDfd_cr_date() {
		return dfd_cr_date;
	}

	public void setDfd_cr_date(Date dfd_cr_date) {
		this.dfd_cr_date = dfd_cr_date;
	}

	public Long getDfd_mod_by() {
		return dfd_mod_by;
	}

	public void setDfd_mod_by(Long dfd_mod_by) {
		this.dfd_mod_by = dfd_mod_by;
	}

	public Date getDfd_mod_date() {
		return dfd_mod_date;
	}

	public void setDfd_mod_date(Date dfd_mod_date) {
		this.dfd_mod_date = dfd_mod_date;
	}

	public Integer getDfd_rec_status() {
		return dfd_rec_status;
	}

	public void setDfd_rec_status(Integer dfd_rec_status) {
		this.dfd_rec_status = dfd_rec_status;
	}

	public Long getDfd_stage_lid() {
		return dfd_stage_lid;
	}

	public void setDfd_stage_lid(Long dfd_stage_lid) {
		this.dfd_stage_lid = dfd_stage_lid;
	}

	public Integer getDfd_edit_mode() {
		return dfd_edit_mode;
	}

	public void setDfd_edit_mode(Integer dfd_edit_mode) {
		this.dfd_edit_mode = dfd_edit_mode;
	}

	public Integer getDfd_page_count() {
		return dfd_page_count;
	}

	public void setDfd_page_count(Integer dfd_page_count) {
		this.dfd_page_count = dfd_page_count;
	}

	public char getDfd_reject_status() {
		return dfd_reject_status;
	}

	public void setDfd_reject_status(char dfd_reject_status) {
		this.dfd_reject_status = dfd_reject_status;
	}

	public Long getDfd_rep_mid() {
		return dfd_rep_mid;
	}

	public void setDfd_rep_mid(Long dfd_rep_mid) {
		this.dfd_rep_mid = dfd_rep_mid;
	}

	public Long getDfd_assign_to() {
		return dfd_assign_to;
	}

	public void setDfd_assign_to(Long dfd_assign_to) {
		this.dfd_assign_to = dfd_assign_to;
	}

	public String getDfd_remark() {
		return dfd_remark;
	}

	public void setDfd_remark(String dfd_remark) {
		this.dfd_remark = dfd_remark;
	}

	public Integer getDfd_print_flag() {
		return dfd_print_flag;
	}

	public void setDfd_print_flag(Integer dfd_print_flag) {
		this.dfd_print_flag = dfd_print_flag;
	}

	public Integer getDfd_system_page_count() {
		return dfd_system_page_count;
	}

	public void setDfd_system_page_count(Integer dfd_system_page_count) {
		this.dfd_system_page_count = dfd_system_page_count;
	}

	public Integer getDfd_index() {
		return dfd_index;
	}

	public void setDfd_index(Integer dfd_index) {
		this.dfd_index = dfd_index;
	}

	public List<MetaData> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<MetaData> metadata) {
		this.metadata = metadata;
	}

	public Document_type getDoctype() {
		return doctype;
	}

	public void setDoctype(Document_type doctype) {
		this.doctype = doctype;
	}

	

	

	
}
