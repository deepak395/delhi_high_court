package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="download_log")

public class DownloadLog {
	
	@Id
	@GeneratedValue (strategy = GenerationType.SEQUENCE, generator="download_logseq")
	@SequenceGenerator(name="download_logseq", sequenceName="download_logseq", allocationSize=1)
	@Column(name = "dl_id")
	private Long dl_id;
	
	@Column(name="dl_user_mid")
	private Long dl_user_mid;
	
	@Column(name="dl_dfd_mid")
	private Long dl_dfd_mid;
	
	@Column(name="dl_download_time")
	private Date dl_download_time;
	
	@Column(name="dl_ip_address")
	private String dl_ip_address;

	public Long getDl_id() {
		return dl_id;
	}

	public void setDl_id(Long dl_id) {
		this.dl_id = dl_id;
	}

	public Long getDl_user_mid() {
		return dl_user_mid;
	}

	public void setDl_user_mid(Long dl_user_mid) {
		this.dl_user_mid = dl_user_mid;
	}

	public Long getDl_dfd_mid() {
		return dl_dfd_mid;
	}

	public void setDl_dfd_mid(Long dl_dfd_mid) {
		this.dl_dfd_mid = dl_dfd_mid;
	}

	public Date getDl_download_time() {
		return dl_download_time;
	}

	public void setDl_download_time(Date dl_download_time) {
		this.dl_download_time = dl_download_time;
	}

	public String getDl_ip_address() {
		return dl_ip_address;
	}

	public void setDl_ip_address(String dl_ip_address) {
		this.dl_ip_address = dl_ip_address;
	}

	

	
}

