package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="sub_department")
public class SubDepartment 
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sub_dept_seq")
	@SequenceGenerator(name="sub_dept_seq", sequenceName="sub_dept_seq", allocationSize=1)
	@Column(name = "sub_dept_id")
	private Long sub_dept_id ;
	
	@Column(name = "sub_dept_name")
	private String  sub_dept_name ;
	
	@Column(name = "sub_dept_cr_by")
	private Long sub_dept_cr_by ;
	
	@Column(name = "sub_dept_cr_date")
	private Date  sub_dept_cr_date ;
	
	@Column(name = "sub_dept_rec_status")
	private Long  sub_dept_rec_status;
	
	@Column(name = "parent_id")
	private Long  parent_id ;
	
	public Long getSub_dept_id() {
		return sub_dept_id;
	}
	public void setSub_dept_id(Long sub_dept_id) {
		this.sub_dept_id = sub_dept_id;
	}
	public String getSub_dept_name() {
		return sub_dept_name;
	}
	public void setSub_dept_name(String sub_dept_name) {
		this.sub_dept_name = sub_dept_name;
	}
	public Long getSub_dept_cr_by() {
		return sub_dept_cr_by;
	}
	public void setSub_dept_cr_by(Long sub_dept_cr_by) {
		this.sub_dept_cr_by = sub_dept_cr_by;
	}
	public Date getSub_dept_cr_date() {
		return sub_dept_cr_date;
	}
	public void setSub_dept_cr_date(Date sub_dept_cr_date) {
		this.sub_dept_cr_date = sub_dept_cr_date;
	}
	public Long getSub_dept_rec_status() {
		return sub_dept_rec_status;
	}
	public void setSub_dept_rec_status(Long sub_dept_rec_status) {
		this.sub_dept_rec_status = sub_dept_rec_status;
	}
	public Long getParent_id() {
		return parent_id;
	}
	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}
	
	@Override
	public String toString() {
		return "SubDepartment [sub_dept_id=" + sub_dept_id + ", sub_dept_name=" + sub_dept_name + ", sub_dept_cr_by="
				+ sub_dept_cr_by + ", sub_dept_cr_date=" + sub_dept_cr_date + ", sub_dept_rec_status="
				+ sub_dept_rec_status + ", parent_id=" + parent_id + "]";
	}
	
	

}
