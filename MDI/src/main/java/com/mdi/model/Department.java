package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "department")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dept_seq")
	@SequenceGenerator(name = "dept_seq", sequenceName = "dept_seq", allocationSize = 1)
	@Column(name = "dept_id")
	private Long dept_id;

	@Column(name = "dept_name")
	private String dept_name;

	@Column(name = "dept_cr_by")
	private Long dept_cr_by;
	
	@Column(name = "dept_cr_date")
	private Date dept_createdOn;
	
	@Column(name = "dept_rec_status")
	private int status;

	@Column(name = "dept_description")
	private String dept_description;

	@Column(name = "dept_repo_id")
	private Long dept_repo_id;

	@Column(name = "dept_updatedOn")
	private Date dept_updatedOn;

	@Column(name = "dept_updatedBy")
	private Long dept_updatedBy;

	public Long getDept_id() {
		return dept_id;
	}

	public void setDept_id(Long dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public Long getDept_cr_by() {
		return dept_cr_by;
	}

	public void setDept_cr_by(Long dept_cr_by) {
		this.dept_cr_by = dept_cr_by;
	}

	public Date getDept_createdOn() {
		return dept_createdOn;
	}

	public void setDept_createdOn(Date dept_createdOn) {
		this.dept_createdOn = dept_createdOn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDept_description() {
		return dept_description;
	}

	public void setDept_description(String dept_description) {
		this.dept_description = dept_description;
	}

	public Long getDept_repo_id() {
		return dept_repo_id;
	}

	public void setDept_repo_id(Long dept_repo_id) {
		this.dept_repo_id = dept_repo_id;
	}

	public Date getDept_updatedOn() {
		return dept_updatedOn;
	}

	public void setDept_updatedOn(Date dept_updatedOn) {
		this.dept_updatedOn = dept_updatedOn;
	}

	public Long getDept_updatedBy() {
		return dept_updatedBy;
	}

	public void setDept_updatedBy(Long dept_updatedBy) {
		this.dept_updatedBy = dept_updatedBy;
	}
	

	
	
}