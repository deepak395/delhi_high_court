package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="uploaded_files_log")
public class UploadedFilesLog 
{
@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE ,generator = "uploaded_files_logseq")
@SequenceGenerator(name = "uploaded_files_logseq" ,sequenceName = "uploaded_files_logseq" ,allocationSize = 1)
@Column(name="ufl_id")	
private int ufl_id; 

@Column(name="ufl_file_bar_code")
private String ufl_file_bar_code="";

@Column(name="ufl_document_type")
private Long ufl_document_type;


@Column(name="ufl_uploaded_by")
private Long ufl_uploaded_by;

@Column(name="ufl_uploaded_date")
private Date ufl_uploaded_date;	


@Column(name="ufl_previous_page_count")
private int ufl_previous_page_count;

@Column(name="ufl_new_page_count")
private int ufl_new_page_count;


@Column(name="ufl_ip_address")
private String ufl_ip_address;

@Column(name="ufl_stage_lid")
private Long ufl_stage_lid;


public Long getUfl_stage_lid() {
	return ufl_stage_lid;
}


public void setUfl_stage_lid(Long ufl_stage_lid) {
	this.ufl_stage_lid = ufl_stage_lid;
}


public int getUfl_id() {
	return ufl_id;
}


public void setUfl_id(int ufl_id) {
	this.ufl_id = ufl_id;
}


public String getUfl_file_bar_code() {
	return ufl_file_bar_code;
}


public void setUfl_file_bar_code(String ufl_file_bar_code) {
	this.ufl_file_bar_code = ufl_file_bar_code;
}


public Long getUfl_document_type() {
	return ufl_document_type;
}


public void setUfl_document_type(Long ufl_document_type) {
	this.ufl_document_type = ufl_document_type;
}


public Long getUfl_uploaded_by() {
	return ufl_uploaded_by;
}


public void setUfl_uploaded_by(Long ufl_uploaded_by) {
	this.ufl_uploaded_by = ufl_uploaded_by;
}


public Date getUfl_uploaded_date() {
	return ufl_uploaded_date;
}


public void setUfl_uploaded_date(Date ufl_uploaded_date) {
	this.ufl_uploaded_date = ufl_uploaded_date;
}


public int getUfl_previous_page_count() {
	return ufl_previous_page_count;
}


public void setUfl_previous_page_count(int ufl_previous_page_count) {
	this.ufl_previous_page_count = ufl_previous_page_count;
}


public int getUfl_new_page_count() {
	return ufl_new_page_count;
}


public void setUfl_new_page_count(int ufl_new_page_count) {
	this.ufl_new_page_count = ufl_new_page_count;
}


public String getUfl_ip_address() {
	return ufl_ip_address;
}


public void setUfl_ip_address(String ufl_ip_address) {
	this.ufl_ip_address = ufl_ip_address;
 }
}
