package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="view_log")

public class ViewLog {
	
	@Id
	@GeneratedValue (strategy = GenerationType.SEQUENCE, generator="view_logseq")
	@SequenceGenerator(name="view_logseq", sequenceName="view_logseq", allocationSize=1)
	@Column(name = "vl_id")
	private Long vl_id;
	
	@Column(name="vl_user_mid")
	private Long vl_user_mid;
	
	@Column(name="vl_dfd_mid")
	private Long vl_dfd_mid;
	
	@Column(name="vl_view_time")
	private Date vl_view_time;
	
	@Column(name="vl_ip_address")
	private String vl_ip_address;

	public Long getVl_id() {
		return vl_id;
	}

	public void setVl_id(Long vl_id) {
		this.vl_id = vl_id;
	}

	public Long getVl_user_mid() {
		return vl_user_mid;
	}

	public void setVl_user_mid(Long vl_user_mid) {
		this.vl_user_mid = vl_user_mid;
	}

	public Long getVl_dfd_mid() {
		return vl_dfd_mid;
	}

	public void setVl_dfd_mid(Long vl_dfd_mid) {
		this.vl_dfd_mid = vl_dfd_mid;
	}

	public Date getVl_view_time() {
		return vl_view_time;
	}

	public void setVl_view_time(Date vl_view_time) {
		this.vl_view_time = vl_view_time;
	}

	public String getVl_ip_address() {
		return vl_ip_address;
	}

	public void setVl_ip_address(String vl_ip_address) {
		this.vl_ip_address = vl_ip_address;
	}

	
}

