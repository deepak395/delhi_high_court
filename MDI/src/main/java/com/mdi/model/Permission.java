package com.mdi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PERMISSION")
public class Permission {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="permission_seq")
	@SequenceGenerator(name="permission_seq", sequenceName="permission_seq", allocationSize=1)
	@Column(name = "pm_id")
	private Long pm_id;

	
	@Column(name = "pm_um_mid")
	private Long pm_um_mid;
	
	@Column(name = "pm_sub_dpt_mid")
	private Long pm_sub_dpt_mid;
	
	@Column(name = "pm_rec_status")
	private Integer pm_rec_status;
	
	@Transient
	private boolean value;
	
	@Transient
	private String sub_dept_name;
	
	
	
	

	public String getSub_dept_name() {
		return sub_dept_name;
	}

	public void setSub_dept_name(String sub_dept_name) {
		this.sub_dept_name = sub_dept_name;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public Long getPm_id() {
		return pm_id;
	}

	public void setPm_id(Long pm_id) {
		this.pm_id = pm_id;
	}

	public Long getPm_um_mid() {
		return pm_um_mid;
	}

	public void setPm_um_mid(Long pm_um_mid) {
		this.pm_um_mid = pm_um_mid;
	}

	public Long getPm_sub_dpt_mid() {
		return pm_sub_dpt_mid;
	}

	public void setPm_sub_dpt_mid(Long pm_sub_dpt_mid) {
		this.pm_sub_dpt_mid = pm_sub_dpt_mid;
	}

	public Integer getPm_rec_status() {
		return pm_rec_status;
	}

	public void setPm_rec_status(Integer pm_rec_status) {
		this.pm_rec_status = pm_rec_status;
	}

	@Override
	public String toString() {
		return "Permission [pm_id=" + pm_id + ", pm_um_mid=" + pm_um_mid + ", pm_sub_dpt_mid=" + pm_sub_dpt_mid
				+ ", pm_rec_status=" + pm_rec_status + ", value=" + value + ", sub_dept_name=" + sub_dept_name + "]";
	}

	
	
	
	
	
	
}
