package com.mdi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "document_type")
public class Document_type {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "doc_type_seq")
	@SequenceGenerator(name = "doc_type_seq", sequenceName = "doc_type_seq", allocationSize = 1)

	@Column(name = "dt_id")
	private Long dt_id;

	@Column(name = "dt_name")
	private String dt_name;

	@Column(name = "dt_cr_by")
	private Long dt_cr_by;

	@Column(name = "dt_cr_date")
	private Date dt_cr_date;

	@Column(name = "dt_rec_status")
	private int dt_rec_status;

	@Column(name = "dt_sub_mid")
	private Long dt_sub_mid;

	public Long getDt_id() {
		return dt_id;
	}

	public void setDt_id(Long dt_id) {
		this.dt_id = dt_id;
	}

	public String getDt_name() {
		return dt_name;
	}

	public void setDt_name(String dt_name) {
		this.dt_name = dt_name;
	}

	public Long getDt_cr_by() {
		return dt_cr_by;
	}

	public void setDt_cr_by(Long dt_cr_by) {
		this.dt_cr_by = dt_cr_by;
	}

	public Date getDt_cr_date() {
		return dt_cr_date;
	}

	public void setDt_cr_date(Date dt_cr_date) {
		this.dt_cr_date = dt_cr_date;
	}

	public int getDt_rec_status() {
		return dt_rec_status;
	}

	public void setDt_rec_status(int dt_rec_status) {
		this.dt_rec_status = dt_rec_status;
	}

	public Long getDt_sub_mid() {
		return dt_sub_mid;
	}

	public void setDt_sub_mid(Long dt_sub_mid) {
		this.dt_sub_mid = dt_sub_mid;
	}

	@Override
	public String toString() {
		return "Document_type [dt_id=" + dt_id + ", dt_name=" + dt_name + ", dt_cr_by=" + dt_cr_by + ", dt_cr_date="
				+ dt_cr_date + ", dt_rec_status=" + dt_rec_status + ", dt_sub_mid=" + dt_sub_mid + "]";
	}

	
}