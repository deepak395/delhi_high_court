package com.mdi.utility;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.pdfbox.pdmodel.PDDocument;
import org.springframework.web.multipart.MultipartFile;

public class CommenMethods {

	public String convert_to_json(Object object){
		ObjectMapper mapper = new ObjectMapper();
		String jsonData = null;
		try {			 
			//System.out.println(mapper.writeValueAsString(object));
			jsonData = mapper.writeValueAsString(object);	
		} catch (JsonGenerationException e) {	 
			e.printStackTrace();	 
		} catch (JsonMappingException e) {	 
			e.printStackTrace();	 
		} catch (IOException e) {	 
			e.printStackTrace();	 
		}	 
		return jsonData;		
	}

	public Long getLeastTaskUser(List<Object> objs) {
		// TODO Auto-generated method stub
		Long leastCount=Long.valueOf(0);
		Long userId=0L;
		for(int i=0;i<objs.size();i++){					
			Object[] row = (Object[]) objs.get(i);	
			Long count=Long.parseLong(row[0].toString());
			
			if(i==0)
				leastCount=count;
			
			if(count<=leastCount)
			{
				leastCount=count;
				userId=Long.parseLong(row[1].toString());
			}
			
		}
		return userId;
	}
	
	
	public  String  PasswordDecryption(String passWord){

		int intlen  = 0;
		String strPwd  = "";
		String strNewPwd = "";
		int i = 0;

		intlen = passWord.length();
		strPwd = passWord;
		i = 1;

		for(int count = 0; intlen > count;count++ ){
				strNewPwd = strNewPwd + (char)(strPwd.charAt(count) - i);
				i = i + 1;
		}
		return strNewPwd;
	}// end of PasswordDecryption class
	
	public  String  PasswordEncryption(String passWord){

		int intlen  = 0;
		String strPwd  = "";
		String strNewPwd = "";
		int i = 0;

		intlen = passWord.length();
		strPwd = passWord;
		i = 1;

		for(int count = 0; intlen > count;count++ ){
				strNewPwd = strNewPwd + (char)(strPwd.charAt(count) + i);
				i = i + 1;
		}
//System.out.println("strNewPwd::"+strNewPwd);
		return strNewPwd;
	}
	
	public String md5encryption(String password)
	{
		String encryptpwd="";
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());

		    byte byteData[] = md.digest();
		    
		    StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
	       // System.out.println("Digest(in hex format):: " + sb.toString());
	        
	      //convert the byte to hex format method 2
	        StringBuffer hexString = new StringBuffer();
	    	for (int i=0;i<byteData.length;i++) {
	    		String hex=Integer.toHexString(0xff & byteData[i]);
	   	     	if(hex.length()==1) hexString.append('0');
	   	     	hexString.append(hex);
	    	}
	    	
	    	encryptpwd = hexString.toString();
	    	//System.out.println("Digest(in hex format):: " + hexString.toString());
		    
		    
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		
		return encryptpwd;
	}
	
	public String getBaseURL(String longurl){
		String jsonData = null;
		int index=longurl.indexOf("/pdms");
		if(index!= -1)
		jsonData=longurl.substring(0, index);
		return jsonData;		
	}
	
	public String generateBarcodeForPendingFile(Long sequence)
	{
		String seq=String.valueOf(sequence);
		
		int len=seq.length();
		
		while(len<10)
		{
			seq="0"+seq;
			len++;
		}
		
		seq="PF"+seq;
		
		return seq;
	}
	
	public static int pageCount(MultipartFile mf) throws IllegalStateException, IOException
	{
		File convFile = new File( mf.getOriginalFilename());
	  //  mf.transferTo(convFile);
	  
	    PDDocument doc = PDDocument.load(convFile);
	 
	  int count = doc.getNumberOfPages();
	  
	  return count;
	}
	
}
