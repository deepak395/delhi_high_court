package com.mdi.utility;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class TestProgram 
{
	static List<String >list = new ArrayList<>();
	/*public static void main(String[] args) throws IOException 
	{
		File folder = new File("C:\\Lucene\\");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
		  File file = listOfFiles[i];
		  if (file.isFile() && file.getName().endsWith(".fdx")) {
		    String content = FileUtils.readFileToString(file);
		    System.out.println(content);
		    
		  } 
		}
		File file = new File("C:\\Lucene\\");
		String[] directories = file.list(new FilenameFilter() {
		  @Override
		  public boolean accept(File current, String name) {
		    return new File(current, name).isDirectory();
		  }
		});
		System.out.println(Arrays.toString(directories));
	}*/
	
	static void RecursivePrint(File[] arr,int index,int level) 
	{ 
		// terminate condition 
		if(index == arr.length) 
			return; 
		
		// tabs for internal levels 
		for (int i = 0; i < level; i++) 
			System.out.print("\t"); 
		
		// for files 
		if(arr[index].isFile()) 
			System.out.println(arr[index].getName()); 
		
		// for sub-directories 
		else if(arr[index].isDirectory()) 
		{ 
			System.out.println("[" + arr[index].getName() + "]"); 
			
			// recursion for sub-directories 
			RecursivePrint(arr[index].listFiles(), 0, level + 1); 
		} 
			
		// recursion for main directory 
		RecursivePrint(arr,++index, level); 
	} 
	
	// Driver Method 
	public static void main(String[] args) 
	{ 
		// Provide full path for directory(change accordingly) 
		String maindirpath = "E:\\MDI"; 
				
		// File object 
		File maindir = new File(maindirpath); 
		
		if(maindir.exists() && maindir.isDirectory()) 
		{ 
			// array for files and sub-directories 
			// of directory pointed by maindir 
			File arr[] = maindir.listFiles(); 
			
			System.out.println("**********************************************"); 
			System.out.println("Files from main directory : " + maindir); 
			System.out.println("**********************************************"); 
			
			// Calling recursive method 
			RecursivePrint(arr,0,0); 
	} 
		System.out.println("/*****************************");
		
		String name ="E:\\MDI";
		getDirectories(name);
		for(String str :list)
		{
			System.out.println(str);
		}
		/*String[] directories = getDirectories(name);
		System.out.println(Arrays.toString(directories));
		
		for(String str : directories)
		{
			String name2 = name+"\\"+str;
			String []directories2 = getDirectories(name2);
			if(directories2.length>0)
			{
				for(String s : directories2)
				{
					
				}
			}
			System.out.println(Arrays.toString(directories));
		}*/
	} 
	
	public static void getDirectories(String name)
	{
		File file = new File(name);
		String[] directories = file.list(new FilenameFilter() {
		  @Override
		  public boolean accept(File current, String name) {
		    return new File(current, name).isDirectory();
		  }
		});
		
		if(directories!=null )
		{
			for(String str : directories)
			{
				
				String name2= name+"\\"+str;
				list.add(name2);
				getDirectories(name2);
			}
		}
		
	}

}
