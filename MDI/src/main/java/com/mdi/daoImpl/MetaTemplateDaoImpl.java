package com.mdi.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.dao.MetaTemplateDao;
import com.mdi.model.Department;
import com.mdi.model.SubDepartment;

@Repository
public class MetaTemplateDaoImpl  implements MetaTemplateDao
{
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public void SaveDepartMent(Department department) throws Exception 
	{
		try 
		{
			em.merge(department);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("Dao_layer",e);
		}
		
	}

	@Override
	public List<Department> getAllDepartMentList() throws Exception
	{
		List<Department> list = new ArrayList<Department>();
		try 
		{
			Query query = em.createQuery("Select d from Department d where status=1 ");
			list= query.getResultList();
			return list;
		} 
		catch (Exception e) 
		{
			throw new Exception("Dao_layer",e);
		}
		
	}
	
	@Override
	public void saveSubDepartment(SubDepartment subdept) throws Exception 
	{
		
		try 
		{
			em.merge(subdept);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("metatemplate_dao_layer",e);
		}
	}

}
