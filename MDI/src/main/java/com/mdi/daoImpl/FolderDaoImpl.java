package com.mdi.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.mdi.dao.FolderDao;
import com.mdi.model.Department;
import com.mdi.model.Folder;


@Repository
public class FolderDaoImpl implements FolderDao
{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<com.mdi.model.Repository> getRepository() throws Exception 
	{
		List<com.mdi.model.Repository> list= new ArrayList<com.mdi.model.Repository>();
		try 
		{
			Query query=em.createQuery("Select r from Repository r");
			list=query.getResultList();
			return list;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("Dao_Layer",e);
		}
		
	}

	@Override
	public com.mdi.model.Repository getRepositoryById(Long id) throws Exception 
	{
		try 
		{
			Query query=em.createQuery("SELECT u from Repository u Where (id=:r_id)");
			query.setParameter("r_id", id);
			com.mdi.model.Repository repo=(com.mdi.model.Repository)query.getSingleResult();
			return repo;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void saveFolder(Folder folder) throws Exception
	{
		
		try 
		{
			em.merge(folder);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("FolderDao_layer",e);
		}
	}

	@Override
	public List<Folder> getAllFolders() throws Exception
	{
		List<Folder>  folders= new ArrayList<>();
		try 
		{
			Query query=em.createQuery("SELECT f from Folder f Where (status=1)");
			folders = query.getResultList(); 
			return folders;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("Folder_Dao_layer",e);
		}
	
	}

	@Override
	public Department getDepartmentById(Long id) throws Exception 
	{
		Department dept= null;
		try 
		{
			Query query=em.createQuery("SELECT d from Department d Where (dept_id=:id)");
			query.setParameter("id",id);
			dept= (Department)query.getSingleResult();
			return dept;
		} 
		catch (Exception e) 
		{
			throw new Exception("Folder_dao_Layer",e);
		}
	}

}
