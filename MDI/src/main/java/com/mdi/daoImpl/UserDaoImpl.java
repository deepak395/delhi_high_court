package com.mdi.daoImpl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.mdi.dao.UserDao;
import com.mdi.model.LoginLog;
import com.mdi.model.User;


@Repository
public class UserDaoImpl implements UserDao 
{
	@PersistenceContext
	private EntityManager em;
	

	@Override
	public User validateLogin(User user) throws Exception 
	{
		User u= null;
		try 
		{
			Query query =em.createQuery("SELECT u from User u Where (username=:uname and password=:pass and rec_status=1)");
			query.setParameter("uname", user.getUsername());
			query.setParameter("pass", user.getPassword());
			u=(User) query.getSingleResult();
			return u;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("dao_layer", e);
		}
	}


	@Override
	public User forgetPassword(String username) throws Exception {
		User user = new User();
		System.out.println(username);
		try {
			Query query = em//createNativeQuery("SELECT * FROM mdi.user_master u WHERE um_email_id ='ramkrishna.pandey_v@stockholdingdms.com'");
					.createQuery("SELECT u FROM User u WHERE (um_email_id =:username)");
			query.setParameter("username", username);
			System.out.println("query="+query);
			user = (User) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			//throw new EntityNotFoundException("Entity does not exist.");
		} finally {
			return user;
		}
	}
	
	
	@Override
	public User getUserByName(String username) throws Exception {
		User user = new User();
		System.out.println(username);
		try {
			Query query = em//createNativeQuery("SELECT * FROM mdi.user_master u WHERE um_email_id ='ramkrishna.pandey_v@stockholdingdms.com'");
					.createQuery("SELECT u FROM User u WHERE (username =:username)");
			query.setParameter("username", username);
			System.out.println("query="+query);
			user = (User) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			//throw new EntityNotFoundException("Entity does not exist.");
		} finally {
			return user;
		}
	}
	
	
	@Override
	public void saveLoginLog(LoginLog log,User user) 
	{
		try 
		{
			if(user.getUm_login_attemps()<1)
			{
				em.merge(log);
				Date date= new Date();
				user.setLast_login(date);
				
				em.merge(user);
			}
			else
			{
				em.merge(log);
				Date date= new Date();
				user.setLast_login(date);
				if(user.getUm_login_attemps()<5)
				{
				  
				   em.merge(user);
				}
				else
				{
					user.setRec_status(0);
					em.merge(user);
				}
				
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	public User getUserById(Long id) throws Exception 
	{
		try 
		{
			Query query =em.createQuery("SELECT u from User u Where (um_id=:u_id  and rec_status=1)");
			query.setParameter("u_id", id);
			
			User user=(User)query.getSingleResult();
			return user;
		} 
		catch (Exception e)
        {
			e.printStackTrace();
		}
		
		return null;
	}

}
