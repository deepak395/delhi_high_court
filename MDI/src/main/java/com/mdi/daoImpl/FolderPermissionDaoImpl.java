package com.mdi.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.mdi.dao.FolderPermissionDao;
import com.mdi.model.Folder;
import com.mdi.model.Permission;


@Repository
public class FolderPermissionDaoImpl implements FolderPermissionDao
{
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Folder> getNotAssignedFolders(Long userId) throws Exception {
		List<Folder> folders= new ArrayList<Folder>();
		try 
		{
			Query query  =  em.createQuery("SELECT f from Folder f where f.id Not IN (SELECT p.value from Permission p where p.userId=:userId AND p.type=2) order by f.id");
			query.setParameter("userId", userId);
			folders= query.getResultList();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return folders;
		
	}

	@Override
	public Permission checkFolderexist(Folder f, Long userId) throws Exception 
	{
		Permission permission=null;
		try 
		{
			Query query  =  em.createQuery("SELECT p from Permission p where p.userId=:userId AND p.value=:value AND type=2");
			query.setParameter("userId", userId);
			query.setParameter("value", f.getId());
			permission= (Permission) query.getSingleResult();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return permission;
	}

	@Override
	public Permission save(Permission perm) throws Exception 
	{
		

    	Permission master = null;
    	try 
    	{	
    		master= em.merge(perm);	    	
	    }
    	catch (Exception e) 
    	{		
	    	e.printStackTrace();
		}
    	return master;
	}

	@Override
	public List<com.mdi.model.Repository> getNotAssignedRepositories(Long userId) 
	{
		List<com.mdi.model.Repository> repositories= new ArrayList<com.mdi.model.Repository>();
		try {
			Query query  =  em.createQuery("SELECT r from Repository r where r.id Not IN (SELECT p.value from Permission p where p.userId=:userId AND p.type=1) order by r.id");
			query.setParameter("userId", userId);
			repositories= query.getResultList();
		} 
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		return repositories;
	}

	@Override
	public Permission checkRepositoryexist(com.mdi.model.Repository r, Long userId) throws Exception {
		Permission permission=null;
		try 
		{
			Query query  =  em.createQuery("SELECT p from Permission p where p.userId=:userId AND p.value=:value AND type=1");
			query.setParameter("userId", userId);
			query.setParameter("value", r.getId());
			permission= (Permission) query.getSingleResult();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return permission;
	}

}
