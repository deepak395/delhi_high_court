import java.util.List;

public class GetNumber {

	public static List<Integer> getList(int digits, int sum) {

		String s1 = "";

		String s2 = "1";

		for (int i = 0; i < digits; i++) {

			s1 = "9" + s1;

			if (i > 0)
				s2 = s2 + "0";

		}

		//System.out.println("Number" + s1);
		//System.out.println("Number" + s2);

		Integer max = Integer.parseInt(s1);
		Integer min = Integer.parseInt(s2);

		for (int i = min; i <= max; i++) {

			int num = 0;
			int min1 = i;

			while (min1 > 0) {

				int temp = min1 % 10;
				num = temp + num;

				min1 = min1/10;

			}

			if (num == sum) {
				System.out.println("Numbers " + i);

			}

		}
		return null;

	}

	public static void main(String[] arg) {

		getList(25, 26);

	}

}
