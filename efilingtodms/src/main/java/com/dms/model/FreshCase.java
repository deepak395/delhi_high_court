package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="fresh_case")
public class FreshCase{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fresh_case_sequence")
	@SequenceGenerator(name="fresh_case_sequence", sequenceName="fresh_case_sequence",allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="diary_no", nullable=false, unique=true)
	private String diaryNo;
	
	@Column(name="year")
	private String year;
	
	@Column(name="document_type")
	private String documentType;
	
	@Column(name="case_stage")
	private String caseStage;
	
	@Column(name="document_name")
	private String documentName;
	
	@Column(name="scanning_page")
	private Integer scanningPage;
	
	@Column(name="qc_page")
	private Integer qcPage;
	
	@Column(name="bookmarking_page")
	private Integer bookmarkingPage;
	
	@Column(name="scanning_folder_id")
	private Long scanningFolderId;
	
	@Column(name="qc_folder_id")
	private Long qcFolderId;
	
	@Column(name="bookmarking_folder_id")
	private Long bookmarkingFolderId;
	
	@Column(name="case_type")
	private String caseType;
	
	/*@Column(name="auto_no", nullable=false, unique=true)
	private Long autoNo;*/
	@Column(name="auto_no", nullable=false)
	private Long autoNo;
	
	@Column(name="scanning_on")
	private Date scanningOn;
	
	@Column(name="scanning_by")
	private String scanningBy;
	
	@Column(name="qc_on")
	private Date qcOn;
	
	@Column(name="qc_by")
	private String qcBy;
	
	@Column(name="bookmarking_on")
	private Date bookmarkingOn;
	
	@Column(name="bookmarking_by")
	private String bookmarkingBy;
	
	@Column(name="qc_downloaded")
	private Boolean qcDownloaded;
	
	@Column(name="bookmarking_downloaded")
	private Boolean bookmarkingDownloaded;
	
	@Column(name="status")
	private Boolean status;
	
	@Column(name="scanner_fullname")
	private String scannerFullname;
	
	@Column(name="qc_fullname")
	private String qcFullname;
	
	@Column(name="bookmark_fullname")
	private String bookMarkFullname;
	
	
	
	
	
	public String getScannerFullname() {
		return scannerFullname;
	}

	public void setScannerFullname(String scannerFullname) {
		this.scannerFullname = scannerFullname;
	}

	public String getQcFullname() {
		return qcFullname;
	}

	public void setQcFullname(String qcFullname) {
		this.qcFullname = qcFullname;
	}

	public String getBookMarkFullname() {
		return bookMarkFullname;
	}

	public void setBookMarkFullname(String bookMarkFullname) {
		this.bookMarkFullname = bookMarkFullname;
	}

	
	
	@Transient
	private String caseStageReport;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDiaryNo() {
		return diaryNo;
	}

	public void setDiaryNo(String diaryNo) {
		this.diaryNo = diaryNo;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getCaseStage() {
		return caseStage;
	}

	public void setCaseStage(String caseStage) {
		this.caseStage = caseStage;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Integer getScanningPage() {
		return scanningPage;
	}

	public void setScanningPage(Integer scanningPage) {
		this.scanningPage = scanningPage;
	}

	public Integer getQcPage() {
		return qcPage;
	}

	public void setQcPage(Integer qcPage) {
		this.qcPage = qcPage;
	}

	public Integer getBookmarkingPage() {
		return bookmarkingPage;
	}

	public void setBookmarkingPage(Integer bookmarkingPage) {
		this.bookmarkingPage = bookmarkingPage;
	}

	public Long getScanningFolderId() {
		return scanningFolderId;
	}

	public void setScanningFolderId(Long scanningFolderId) {
		this.scanningFolderId = scanningFolderId;
	}

	public Long getQcFolderId() {
		return qcFolderId;
	}

	public void setQcFolderId(Long qcFolderId) {
		this.qcFolderId = qcFolderId;
	}

	public Long getBookmarkingFolderId() {
		return bookmarkingFolderId;
	}

	public void setBookmarkingFolderId(Long bookmarkingFolderId) {
		this.bookmarkingFolderId = bookmarkingFolderId;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public Long getAutoNo() {
		return autoNo;
	}

	public void setAutoNo(Long autoNo) {
		this.autoNo = autoNo;
	}

	public Date getScanningOn() {
		return scanningOn;
	}

	public void setScanningOn(Date scanningOn) {
		this.scanningOn = scanningOn;
	}

	public String getScanningBy() {
		return scanningBy;
	}

	public void setScanningBy(String scanningBy) {
		this.scanningBy = scanningBy;
	}

	public Date getQcOn() {
		return qcOn;
	}

	public void setQcOn(Date qcOn) {
		this.qcOn = qcOn;
	}

	public String getQcBy() {
		return qcBy;
	}

	public void setQcBy(String qcBy) {
		this.qcBy = qcBy;
	}

	public Date getBookmarkingOn() {
		return bookmarkingOn;
	}

	public void setBookmarkingOn(Date bookmarkingOn) {
		this.bookmarkingOn = bookmarkingOn;
	}

	public String getBookmarkingBy() {
		return bookmarkingBy;
	}

	public void setBookmarkingBy(String bookmarkingBy) {
		this.bookmarkingBy = bookmarkingBy;
	}

	public Boolean getQcDownloaded() {
		return qcDownloaded;
	}

	public void setQcDownloaded(Boolean qcDownloaded) {
		this.qcDownloaded = qcDownloaded;
	}

	public Boolean getBookmarkingDownloaded() {
		return bookmarkingDownloaded;
	}

	public void setBookmarkingDownloaded(Boolean bookmarkingDownloaded) {
		this.bookmarkingDownloaded = bookmarkingDownloaded;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Column(name ="accepted_by_scanner")
	private Boolean acceptedByScanner;
	
	@Column(name ="old_case_type")
	private String oldCaseType;
	

	public Boolean getAcceptedByScanner() {
		return acceptedByScanner;
	}

	public void setAcceptedByScanner(Boolean acceptedByScanner) {
		this.acceptedByScanner = acceptedByScanner;
	}

	public String getOldCaseType() {
		return oldCaseType;
	}

	public void setOldCaseType(String oldCaseType) {
		this.oldCaseType = oldCaseType;
	}

	

	public String getCaseStageReport() {
		return caseStageReport;
	}

	public void setCaseStageReport(String caseStageReport) {
		this.caseStageReport = caseStageReport;
	}

	@Override
	public String toString() {
		return "FreshCase [id=" + id + ", diaryNo=" + diaryNo + ", year=" + year + ", documentType=" + documentType
				+ ", caseStage=" + caseStage + ", documentName=" + documentName + ", scanningPage=" + scanningPage
				+ ", qcPage=" + qcPage + ", bookmarkingPage=" + bookmarkingPage + ", scanningFolderId="
				+ scanningFolderId + ", qcFolderId=" + qcFolderId + ", bookmarkingFolderId=" + bookmarkingFolderId
				+ ", caseType=" + caseType + ", autoNo=" + autoNo + ", scanningOn=" + scanningOn + ", scanningBy="
				+ scanningBy + ", qcOn=" + qcOn + ", qcBy=" + qcBy + ", bookmarkingOn=" + bookmarkingOn
				+ ", bookmarkingBy=" + bookmarkingBy + ", qcDownloaded=" + qcDownloaded + ", bookmarkingDownloaded="
				+ bookmarkingDownloaded + ", status=" + status + ", scannerFullname=" + scannerFullname
				+ ", qcFullname=" + qcFullname + ", bookMarkFullname=" + bookMarkFullname + ", caseStageReport="
				+ caseStageReport + ", acceptedByScanner=" + acceptedByScanner + ", oldCaseType=" + oldCaseType + "]";
	}

	
	
	

	

	
}
