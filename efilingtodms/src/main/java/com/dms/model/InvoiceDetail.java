package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="invoice_detail")
public class InvoiceDetail{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="invoice_detail_generator")
	@SequenceGenerator(name="invoice_detail_generator", sequenceName="invoice_detail_sequence",allocationSize=1)
	@Column(name="auto_no")
	private Long autoNo;
	
	@Column(name="bill_no")
	private String billNo;
	
	@Column(name="case_type")
	private String caseType;
	
	@Column(name="client_name")
	private String clientName;
	
	@Column(name="case_no")
	private String caseNo;
	
	@Column(name="no_of_page")
	private Integer noOfPage;
	
	@Column(name="amount")
	private Double amount;
	
	@Column(name="is_canceled")
	private Boolean isCanceled;
	
	@Column(name="canceled_on")
	private Date canceledOn;
	
	@Column(name="canceled_by")
	private String canceledBy;
	
	@Column(name="canceled_remark")
	private String canceledRemark;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="status")
	private Boolean status;
	
	@Column(name="temp_bill_no", columnDefinition="bigint default nextval('bill_no_reset_sequence')", insertable = false)
	private Long tempBillNo;

	public Long getAutoNo() {
		return autoNo;
	}

	public void setAutoNo(Long autoNo) {
		this.autoNo = autoNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public Integer getNoOfPage() {
		return noOfPage;
	}

	public void setNoOfPage(Integer noOfPage) {
		this.noOfPage = noOfPage;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getIsCanceled() {
		return isCanceled;
	}

	public void setIsCanceled(Boolean isCanceled) {
		this.isCanceled = isCanceled;
	}

	public Date getCanceledOn() {
		return canceledOn;
	}

	public void setCanceledOn(Date canceledOn) {
		this.canceledOn = canceledOn;
	}

	public String getCanceledBy() {
		return canceledBy;
	}

	public void setCanceledBy(String canceledBy) {
		this.canceledBy = canceledBy;
	}

	public String getCanceledRemark() {
		return canceledRemark;
	}

	public void setCanceledRemark(String canceledRemark) {
		this.canceledRemark = canceledRemark;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getTempBillNo() {
		return tempBillNo;
	}

	public void setTempBillNo(Long tempBillNo) {
		this.tempBillNo = tempBillNo;
	}

	@Override
	public String toString() {
		return "InvoiceDetail [autoNo=" + autoNo + ", billNo=" + billNo + ", caseType=" + caseType + ", clientName="
				+ clientName + ", caseNo=" + caseNo + ", noOfPage=" + noOfPage + ", amount=" + amount + ", isCanceled="
				+ isCanceled + ", canceledOn=" + canceledOn + ", canceledBy=" + canceledBy + ", canceledRemark="
				+ canceledRemark + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", status=" + status + ", tempBillNo=" + tempBillNo + "]";
	}

}
