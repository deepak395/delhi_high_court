package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="fresh_case_document")
public class FreshCaseDocument {
	
	

		@Id
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fresh_case_doc_sequence")
		@SequenceGenerator(name="fresh_case_doc_sequence", sequenceName="fresh_case_doc_sequence",allocationSize=1)
		@Column(name="id")
		private Long id;
		
		
		@Column(name="fresh_case_id")
		private Long freshcaseId;
		
		@Column(name="diary_no")
		private String diaryNo;
		
		@Column(name="year")
		private String year;
		
		@Column(name="document_type")
		private String documentType;
		
		@Column(name="case_stage")
		private String caseStage;
		
		@Column(name="document_name")
		private String documentName;
		
		@Column(name="scanning_page")
		private Integer scanningPage;
		
		@Column(name="qc_page")
		private Integer qcPage;
		
		@Column(name="bookmarking_page")
		private Integer bookmarkingPage;
		
		
		@Column(name="case_type")
		private String caseType;
		
		
		
		@Column(name="scanning_on")
		private Date scanningOn;
		
		@Column(name="scanning_fullname")
		private String scanningfullname;
		
		@Column(name="scanning_by")
		private String scanningBy;
		
		@Column(name="qc_on")
		private Date qcOn;
		
		@Column(name="qc_fullname")
		private String qcfullname;
		
		
		@Column(name="qc_by")
		private String qcBy;
		
		@Column(name="bookmarking_on")
		private Date bookmarkingOn;
		
		@Column(name="bookmarking_fullname")
		private String bookmarkingfullname;
		
		
		@Column(name="bookmarking_by")
		private String bookmarkingBy;
		
		@Column(name="qc_downloaded")
		private Boolean qcDownloaded;
		
		@Column(name="bookmarking_downloaded")
		private Boolean bookmarkingDownloaded;
		
		@Column(name="status")
		private Boolean status;
		
		
		
		
		
	
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getDiaryNo() {
			return diaryNo;
		}

		public void setDiaryNo(String diaryNo) {
			this.diaryNo = diaryNo;
		}

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}

		public String getDocumentType() {
			return documentType;
		}

		public void setDocumentType(String documentType) {
			this.documentType = documentType;
		}

		public String getCaseStage() {
			return caseStage;
		}

		public void setCaseStage(String caseStage) {
			this.caseStage = caseStage;
		}

		public String getDocumentName() {
			return documentName;
		}

		public void setDocumentName(String documentName) {
			this.documentName = documentName;
		}

		public Integer getScanningPage() {
			return scanningPage;
		}

		public void setScanningPage(Integer scanningPage) {
			this.scanningPage = scanningPage;
		}

		public Integer getQcPage() {
			return qcPage;
		}

		public void setQcPage(Integer qcPage) {
			this.qcPage = qcPage;
		}

		public Integer getBookmarkingPage() {
			return bookmarkingPage;
		}

		public void setBookmarkingPage(Integer bookmarkingPage) {
			this.bookmarkingPage = bookmarkingPage;
		}

		
		public String getCaseType() {
			return caseType;
		}

		public void setCaseType(String caseType) {
			this.caseType = caseType;
		}

		

		public Date getScanningOn() {
			return scanningOn;
		}

		public void setScanningOn(Date scanningOn) {
			this.scanningOn = scanningOn;
		}

		public String getScanningBy() {
			return scanningBy;
		}

		public void setScanningBy(String scanningBy) {
			this.scanningBy = scanningBy;
		}

		public Date getQcOn() {
			return qcOn;
		}

		public void setQcOn(Date qcOn) {
			this.qcOn = qcOn;
		}

		public String getQcBy() {
			return qcBy;
		}

		public void setQcBy(String qcBy) {
			this.qcBy = qcBy;
		}

		public Date getBookmarkingOn() {
			return bookmarkingOn;
		}

		public void setBookmarkingOn(Date bookmarkingOn) {
			this.bookmarkingOn = bookmarkingOn;
		}

		public String getBookmarkingBy() {
			return bookmarkingBy;
		}

		public void setBookmarkingBy(String bookmarkingBy) {
			this.bookmarkingBy = bookmarkingBy;
		}


		public Boolean getStatus() {
			return status;
		}

		public void setStatus(Boolean status) {
			this.status = status;
		}

		public Long getFreshcaseId() {
			return freshcaseId;
		}

		public void setFreshcaseId(Long freshcaseId) {
			this.freshcaseId = freshcaseId;
		}

		public String getScanningfullname() {
			return scanningfullname;
		}

		public void setScanningfullname(String scanningfullname) {
			this.scanningfullname = scanningfullname;
		}

		public String getQcfullname() {
			return qcfullname;
		}

		public void setQcfullname(String qcfullname) {
			this.qcfullname = qcfullname;
		}

		public String getBookmarkingfullname() {
			return bookmarkingfullname;
		}

		public void setBookmarkingfullname(String bookmarkingfullname) {
			this.bookmarkingfullname = bookmarkingfullname;
		}

		public Boolean getQcDownloaded() {
			return qcDownloaded;
		}

		public void setQcDownloaded(Boolean qcDownloaded) {
			this.qcDownloaded = qcDownloaded;
		}

		public Boolean getBookmarkingDownloaded() {
			return bookmarkingDownloaded;
		}

		public void setBookmarkingDownloaded(Boolean bookmarkingDownloaded) {
			this.bookmarkingDownloaded = bookmarkingDownloaded;
		}

		@Override
		public String toString() {
			return "FreshCaseDocument [id=" + id + ", freshcaseId=" + freshcaseId + ", diaryNo=" + diaryNo + ", year="
					+ year + ", documentType=" + documentType + ", caseStage=" + caseStage + ", documentName="
					+ documentName + ", scanningPage=" + scanningPage + ", qcPage=" + qcPage + ", bookmarkingPage="
					+ bookmarkingPage + ", caseType=" + caseType + ", scanningOn=" + scanningOn + ", scanningfullname="
					+ scanningfullname + ", scanningBy=" + scanningBy + ", qcOn=" + qcOn + ", qcfullname=" + qcfullname
					+ ", qcBy=" + qcBy + ", bookmarkingOn=" + bookmarkingOn + ", bookmarkingfullname="
					+ bookmarkingfullname + ", bookmarkingBy=" + bookmarkingBy + ", qcDownloaded=" + qcDownloaded
					+ ", bookmarkingDownloaded=" + bookmarkingDownloaded + ", status=" + status + "]";
		}

		

	


		
	}

	
	


