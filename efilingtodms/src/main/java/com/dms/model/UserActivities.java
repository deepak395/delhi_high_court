package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="user_activities")
public class UserActivities {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="useractivities_generator")
	@SequenceGenerator(name="useractivities_generator", sequenceName="useractivities_sequence",allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="ip_address")
	private String ipAdress;
	
	@Column(name="browser_details")
	private String browserDetails;
	
	@Column(name="activity")
	private String activity;
	
	@Column(name="created_on")
	private Date createdOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}

	public String getBrowserDetails() {
		return browserDetails;
	}

	public void setBrowserDetails(String browserDetails) {
		this.browserDetails = browserDetails;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "UserActivities [id=" + id + ", userName=" + userName + ", ipAdress=" + ipAdress + ", browserDetails="
				+ browserDetails + ", activity=" + activity + ", createdOn=" + createdOn + "]";
	}
	
	
}
