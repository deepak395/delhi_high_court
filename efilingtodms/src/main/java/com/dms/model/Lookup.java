package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * 
 * @author Rupnarayan Shahu, Sameer Shelar
 *
 */
@Entity
@Table(name="lookup_master")
public class Lookup {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lookup_generator")
	@SequenceGenerator(name="lookup_generator", sequenceName="lookup_sequence",allocationSize=1)
	@Column(name="lookup_id")
	private Long lookupId;
	
	@Column(name="lookup_name")
	private String lookupName;
	
	@Column(name="lookup_value")
	private String lookupValue;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="status")
	private Boolean status;

	public Long getLookupId() {
		return lookupId;
	}

	public void setLookupId(Long lookupId) {
		this.lookupId = lookupId;
	}

	public String getLookupName() {
		return lookupName;
	}

	public void setLookupName(String lookupName) {
		this.lookupName = lookupName;
	}

	public String getLookupValue() {
		return lookupValue;
	}

	public void setLookupValue(String lookupValue) {
		this.lookupValue = lookupValue;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Lookup [lookupId=" + lookupId + ", lookupName=" + lookupName + ", lookupValue=" + lookupValue
				+ ", createdOn=" + createdOn + ", createdBy=" + createdBy + ", status=" + status + "]";
	}

	
}
