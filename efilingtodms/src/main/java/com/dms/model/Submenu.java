package com.dms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="submenu_master")
public class Submenu implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="submenu_generator")
	@SequenceGenerator(name="submenu_generator", sequenceName="submenu_sequence",allocationSize=1)
	@Column(name="submenu_id")
	private Long submenuId;
	
	@Column(name="submenu_name")
	private String submenuName;
	
	@Column(name="submenu_url")
	private String submenuUrl;
	
	@Column(name="role_name")
	private String roleName;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="menu_id")
	private Long menuId;
	
	@Column(name="status")
	private Boolean status;

	public Long getSubmenuId() {
		return submenuId;
	}

	public void setSubmenuId(Long submenuId) {
		this.submenuId = submenuId;
	}

	public String getSubmenuName() {
		return submenuName;
	}

	public void setSubmenuName(String submenuName) {
		this.submenuName = submenuName;
	}

	public String getSubmenuUrl() {
		return submenuUrl;
	}

	public void setSubmenuUrl(String submenuUrl) {
		this.submenuUrl = submenuUrl;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Submenu [submenuId=" + submenuId + ", submenuName=" + submenuName + ", submenuUrl=" + submenuUrl
				+ ", roleName=" + roleName + ", createdOn=" + createdOn + ", createdBy=" + createdBy + ", menuId="
				+ menuId + ", status=" + status + "]";
	}
}
