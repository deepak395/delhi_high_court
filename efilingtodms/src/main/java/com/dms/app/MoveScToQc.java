package com.dms.app;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dms.model.Lookup;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;
import com.dms.service.MoveScanToQc;
import com.itextpdf.text.pdf.PdfReader;

public class MoveScToQc {
	
	
	public static void main(String[] args) throws IOException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		MoveScanToQc m = (MoveScanToQc) ctx.getBean("moveScanToQc");
		List<PendingCase> scanList = m.getPendingListForScan("READY TO MOVE QC FILES");
		
			moveCaseFiles(m,scanList);
			
		
		

}
	
	
	
	
	
	public static void moveCaseFiles(MoveScanToQc mc,List<PendingCase> scanList){

		Lookup lookupScanning = mc.getLookUpObjectDMS("PENDING_SCAN");
		Lookup pending_lookup = mc.getLookUpObjectDMS("PENDING_QC");

		 PendingCase pd=null;
		 
		SimpleDateFormat  formatter = new SimpleDateFormat("dd-M-yyyy");  
		   
		String scan_date=null;
		String qc_date=null;
		 String sourcebasepath=null;
	   for(PendingCase pc:scanList){
		   scan_date= new String();
		   qc_date= new String();
		   if(pc.getScannedOn()!=null)
		   scan_date=formatter.format(pc.getScannedOn());
		   if(pc.getQcOn()!=null)
		   qc_date= formatter.format(pc.getQcOn());
	    	
	    	if(pc.getCaseType().equalsIgnoreCase("UPDATE")){
		   sourcebasepath=new String(lookupScanning.getLookupValue()+File.separator+scan_date+File.separator+"UPDATE"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear());
	    	}
	    	else if (pc.getCaseType().equalsIgnoreCase("DAK")){
	    		 sourcebasepath=new String(lookupScanning.getLookupValue()+File.separator+scan_date+File.separator+"DAK"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear());
	    	}
	    	else
	    	{
	    	 sourcebasepath=new String(lookupScanning.getLookupValue()+File.separator+scan_date+File.separator+"FRESH"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear());
	    	}	
		   String destination_path= null;
		 pd= new PendingCase();
		 File folder = new File(sourcebasepath);
		 File[] flist= folder.listFiles();
		 if(flist!=null){
		    for(File file: flist){
		    	destination_path=new String();
		    	if(pc.getCaseType().equalsIgnoreCase("UPDATE")){
		    	destination_path=pending_lookup.getLookupValue()+File.separator+qc_date+File.separator+"UPDATE"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear()+File.separator+file.getName();
		    	}
		    	else  if(pc.getCaseType().equalsIgnoreCase("DAK")){
		    		destination_path=pending_lookup.getLookupValue()+File.separator+qc_date+File.separator+"DAK"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear()+File.separator+file.getName();    		
		    		
		    	}
		    	else{
		    		destination_path=pending_lookup.getLookupValue()+File.separator+qc_date+File.separator+"FRESH"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear()+File.separator+file.getName();
		    	
			     }
		
		    	File source= new File(sourcebasepath+File.separator+file.getName());
		    	File dest= new File(destination_path);
		    	
		    	  int no_of_pages=0;
		    	  boolean copy_flag=false;
		    	try {
					PdfReader reader = new PdfReader(source.getAbsolutePath());
					no_of_pages = reader.getNumberOfPages();
					FileUtils.copyFile(source, dest); 
					copy_flag = true;
			
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	
		    	
		    	  if(copy_flag){
		    		  pc.setCaseStage("QC DONE");
		    		  PendingCaseDocument pcd=null; 
		    	    pcd=mc.getAllPendingDocument(pc.getPendingCaseId(),file.getName()); 
		    		  if(pcd==null){
		    			  pcd= new PendingCaseDocument();
		    			  pcd.setCaseStage("QC DONE");
				    	   pcd.setQcPage(no_of_pages);
				    	   pcd.setQcBy(pc.getQcBy());
				    	   pcd.setQcOn(pc.getQcOn());
				    	   mc.save(pc);
				    	   mc.save(pcd);
		    		  }
		    		  else{
		    			  pcd.setCaseStage("QC DONE");
				    	   pcd.setQcPage(no_of_pages);
				    	   pcd.setQcBy(pc.getQcBy());
				    	   pcd.setQcOn(pc.getQcOn());
				    	   mc.save(pc);
				    	   mc.save(pcd);
		    			  
		    		  }
		    	   
				
			}
			 
			 
		 }
		    System.out.println("QC Done Moved Successfully "+ pc.getCaseCategory()+ " "+pc.getCaseNo()+ " "+pc.getCaseYear()   );
		 }
		 
		   
		
	}

	}
}


