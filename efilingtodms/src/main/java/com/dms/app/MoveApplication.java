/*package com.dms.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.dms.model.DMSApplicationIndexFieldMapping;
import com.dms.model.DMSCaseFileDetail;
import com.dms.model.DMSIndexField;
import com.dms.model.DMSLookup;
import com.dms.model.DMSSubDocuments;
import com.dms.service.ApplicationService;
import com.dms.service.CaseFileDetailService;
import com.dms.service.LookupService;
import com.dms.service.MetaDataService;
import com.dms.service.SubDocumentService;
import com.efiling.model.Application;
import com.efiling.model.ApplicationStage;
import com.efiling.model.SubApplication;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;

@Component
public class MoveApplication {

	public static void main(String[] args) throws IOException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		ApplicationService applicationService = (ApplicationService) ctx.getBean("applicationService");
		CaseFileDetailService caseFileDetailService = (CaseFileDetailService) ctx.getBean("caseFileDetailService");
		LookupService lookupService = (LookupService) ctx.getBean("lookupService");
		SubDocumentService subDocumentService = (SubDocumentService) ctx.getBean("subDocumentService");
		MetaDataService metaDataService = (MetaDataService) ctx.getBean("metaDataService");

		List<Application> applicationfileList = applicationService.getApplicationsForMove();

		HashMap<Long, Long> indexFieldMapping = new HashMap<Long, Long>();

		List<DMSApplicationIndexFieldMapping> appIndexFieldList = applicationService.getApplicationIndexFieldMapping();

		Iterator<DMSApplicationIndexFieldMapping> itr = appIndexFieldList.iterator();

		while (itr.hasNext()) {
			DMSApplicationIndexFieldMapping daim = itr.next();
			indexFieldMapping.put(daim.getAim_at_mid(), daim.getAim_if_mid());
		}

		moveApplicationFiles(applicationfileList, indexFieldMapping, applicationService, caseFileDetailService,
				lookupService, subDocumentService, metaDataService);

	}

	public static void moveApplicationFiles(List<Application> appList, HashMap<Long, Long> aim,
			ApplicationService applicationService, CaseFileDetailService caseFileDetailService,
			LookupService lookupService, SubDocumentService subDocumentService, MetaDataService metaDataService) {

		for (Application app : appList) {
			System.out.println("app_draft_no " + app.getAp_draft_no() + " ap_fd_mid " + app.getAp_fd_mid());
			Long at_mid = app.getAp_at_mid();
			if (aim.containsKey(at_mid)) {

				try {
					DMSCaseFileDetail dmscfd = caseFileDetailService.getCaseFileByCaseTypeNoYear(
							app.getCaseFileDetail().getFd_case_type(), app.getCaseFileDetail().getFd_case_no(),
							app.getCaseFileDetail().getFd_case_year());

					if (dmscfd.getFd_document_name() == null) {
						dmscfd.setFd_case_type(app.getCaseFileDetail().getFd_case_type());
						dmscfd.setFd_case_no(app.getCaseFileDetail().getFd_case_no());
						dmscfd.setFd_case_year(app.getCaseFileDetail().getFd_case_year());
						dmscfd.setFd_document_name(app.getCaseFileDetail().getCaseType().getCt_label()
								+ app.getCaseFileDetail().getFd_case_no() + app.getCaseFileDetail().getFd_case_year());
						dmscfd.setFd_file_source("A");
						dmscfd.setFd_rec_status(1);
						dmscfd.setFd_stage_lid(1000047L);
						dmscfd.setFd_cr_by(1L);
						dmscfd.setFd_cr_date(new Date());

						dmscfd = caseFileDetailService.save(dmscfd);

					} // end if loop

					boolean copy_flag = false, sub_flag = false;
					int no_of_pages = 0;

					DMSIndexField indexField = subDocumentService.getIndexField(aim.get(at_mid));

					Long docCount = applicationService.getCount(dmscfd.getFd_id());
					docCount++;
					String doc_name = dmscfd.getFd_document_name() + "_" + indexField.getIf_type_code() + "_"
							+ docCount;

					DMSLookup lookupDMS = lookupService.getLookUpObjectDMS("REPOSITORYPATH");
					DMSLookup lookupEfiling = lookupService.getLookUpObjectDMS("EFILING_PATH");

					String sourcebasepath = lookupEfiling.getLk_longname();
					String destbasepath = lookupDMS.getLk_longname();

					File source = new File(sourcebasepath + File.separator + "application" + File.separator
							+ app.getAp_draft_no() + ".pdf");

					File dest = new File(destbasepath + File.separator + dmscfd.getCaseType().getCt_label()
							+ File.separator + indexField.getIf_name() + File.separator + doc_name + ".pdf");

					try {
						FileUtils.copyFile(source, dest);
						PdfReader reader = new PdfReader(dest.getAbsolutePath());
						no_of_pages = reader.getNumberOfPages();
						copy_flag = true;
					} catch (IOException e) {
						e.printStackTrace();
					}

					if (copy_flag && no_of_pages > 0) {

						DMSSubDocuments dmssb = new DMSSubDocuments();

						dmssb.setSd_fd_mid(dmscfd.getFd_id());
						dmssb.setSd_if_mid(indexField.getIf_id());
						dmssb.setSd_document_name(doc_name);
						dmssb.setSd_document_id(app.getAp_at_mid().intValue());
						dmssb.setSd_document_no(app.getAp_no());
						dmssb.setSd_document_year(app.getAp_year());
						dmssb.setSd_olr_no(app.getAp_olr_no()); // olr no
						dmssb.setSd_olr_year(app.getAp_olr_year()); // olr year
						dmssb.setSd_major_sequence(indexField.getIf_sequence());
						dmssb.setSd_minor_sequence(docCount.intValue());
						dmssb.setSd_no_of_pages(no_of_pages);
						dmssb.setSd_cr_date(new Date());
						dmssb.setSd_cr_by(1);
						dmssb.setSd_rec_status(1);
						dmssb.setSd_version(1);
						if (app.getAp_filed_by() == 1) {
							dmssb.setSd_party("P");
						} else if (app.getAp_filed_by() == 2) {
							dmssb.setSd_party("R");
						} else if (app.getAp_filed_by() == 3) {
							dmssb.setSd_party("O");
						}

						dmssb.setSd_description(app.getAp_applicant_name());
						dmssb.setSd_counsel(app.getCounsel().getUm_fullname());

						ApplicationStage aps = applicationService.getFileSubmittedStage(app.getAp_id());

						dmssb.setSd_submitted_date(aps.getAs_cr_date());

						dmssb = subDocumentService.save(dmssb);

						// Move Sub Applications to Subdocument in DMS
						List<SubApplication> subApplications = applicationService.getSubApplications(app.getAp_id());
						int noofpages = 0;

						if (!subApplications.isEmpty()) {
							for (SubApplication subApplication : subApplications) {

								Long sub_at_mid = subApplication.getSb_ap_at_mid();

								if (aim.containsKey(sub_at_mid)) {

									DMSIndexField indexField1 = subDocumentService.getIndexField(aim.get(sub_at_mid));

									Long doccumentCount = applicationService.getCount(dmscfd.getFd_id());
									doccumentCount++;
									String documentName = dmscfd.getFd_document_name() + "_"
											+ indexField.getIf_type_code() + "_" + docCount;

									File source1 = new File(sourcebasepath + File.separator + "application"
											+ File.separator + app.getAp_draft_no() + ".pdf");

									File dest1 = new File(destbasepath + File.separator
											+ dmscfd.getCaseType().getCt_label() + File.separator
											+ indexField.getIf_name() + File.separator + doc_name + ".pdf");

									if (subApplication.getSb_ap_from_page() != 0L
											&& subApplication.getSb_ap_to_page() != 0L) {

										String sPath = source1.getAbsolutePath();
										String dPath = dest1.getAbsolutePath();

										PdfReader reader = new PdfReader(sPath);

										Document document = new Document();
										FileOutputStream outputStream = new FileOutputStream(dPath);

										PdfSmartCopy copy1 = new PdfSmartCopy(document, outputStream);

										copy1.setFullCompression();
										document.open();
										// Integer i = (int) (long) theLong;

										for (int i = (int) (long) subApplication
												.getSb_ap_from_page(); i <= (int) (long) subApplication
														.getSb_ap_to_page(); i++) {
											copy1.addPage(copy1.getImportedPage(reader, i));
											noofpages++;
										}
										copy1.freeReader(reader);
										reader.close();

										document.close();
										copy1.close();

									}

									DMSSubDocuments dmsSubDocument = new DMSSubDocuments();
									dmsSubDocument.setSd_fd_mid(dmscfd.getFd_id());
									dmsSubDocument.setSd_if_mid(indexField.getIf_id());
									dmsSubDocument.setSd_document_name(documentName);
									dmsSubDocument.setSd_document_id(subApplication.getSb_ap_at_mid().intValue());
									dmsSubDocument.setSd_document_no(subApplication.getSb_ap_no());
									dmsSubDocument.setSd_document_year(subApplication.getSb_ap_year());
									// dmssb.setSd_olr_no(app.getAp_olr_no()); // olr no
									// dmssb.setSd_olr_year(app.getAp_olr_year()); // olr year

									dmsSubDocument.setSd_major_sequence(indexField.getIf_sequence());
									dmsSubDocument.setSd_minor_sequence(docCount.intValue());
									dmsSubDocument.setSd_no_of_pages(no_of_pages);
									dmsSubDocument.setSd_cr_date(new Date());
									dmsSubDocument.setSd_submitted_date(new Date());
									dmsSubDocument.setSd_cr_by(1);
									dmsSubDocument.setSd_rec_status(1);
									dmsSubDocument.setSd_version(1);

									if (app.getAp_filed_by() == 1) {
										dmsSubDocument.setSd_party("P");
									} else if (app.getAp_filed_by() == 2) {
										dmsSubDocument.setSd_party("R");
									} else if (app.getAp_filed_by() == 3) {
										dmsSubDocument.setSd_party("O");
									}

									dmsSubDocument.setSd_description(app.getAp_applicant_name());
									dmsSubDocument.setSd_counsel(app.getCounsel().getUm_fullname());

									ApplicationStage aps1 = applicationService.getFileSubmittedStage(app.getAp_id());

									dmsSubDocument.setSd_submitted_date(aps1.getAs_cr_date());

									dmsSubDocument = subDocumentService.save(dmsSubDocument);

								}
							}
						}

						sub_flag = true;

						if (copy_flag && sub_flag) {
							System.out.println(app.getAp_draft_no() + " File Moved");

							ApplicationStage as = new ApplicationStage();
							as.setAs_ap_mid(app.getAp_id());
							as.setAs_cr_date(new Date());
							as.setAs_stage_lid(1000049L);
							as.setAs_cr_by(1L);

							applicationService.saveStage(as);
							// update register case file in efiling
							app.setAp_stage_lid(1000049L);
							applicationService.save(app);
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} // end if(aim.containsKey(at_mid))
		} // end for each loop

	}

}
*/