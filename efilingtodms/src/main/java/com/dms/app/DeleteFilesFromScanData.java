package com.dms.app;

	import java.io.File;
	import java.io.IOException;
	import java.nio.file.Files;
	import java.nio.file.Path;
	import java.nio.file.Paths;
	import java.nio.file.StandardCopyOption;
	import java.text.SimpleDateFormat;
	import java.util.List;

	import org.apache.commons.io.FileUtils;
	import org.springframework.context.ApplicationContext;
	import org.springframework.context.support.ClassPathXmlApplicationContext;

	import com.dms.model.FreshCase;
	import com.dms.model.FreshCaseDocument;
	import com.dms.model.Lookup;
	import com.dms.model.PendingCase;
	import com.dms.service.MoveScanToQc;
	import com.itextpdf.text.pdf.PdfReader;
public class DeleteFilesFromScanData {
	

	

		public static void main(String[] args) throws IOException {

			ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

			MoveScanToQc m = (MoveScanToQc) ctx.getBean("moveScanToQc");
			List<FreshCase> scanList = m.getFreshListForScan("READY TO MOVE QC FILES");

			moveCaseFiles(m, scanList);

		}

		public static void moveCaseFiles(MoveScanToQc mc, List<FreshCase> scanList) {

			Lookup lookupScanning = mc.getLookUpObjectDMS("COUNTER_QC_PENDING");
			Lookup pending_lookup = mc.getLookUpObjectDMS("COUNTER_QC_DATA");

			PendingCase pd = null;

			SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
			String sourcebasepath = null;
			String diary = null;
			for (FreshCase fc : scanList) {
				String Date = null;
				String scan_date = null;
				diary = new String();
				scan_date = new String();
				scan_date = formatter.format(fc.getScanningOn());
				diary = fc.getDiaryNo() + "-" + fc.getYear();
				if (fc.getCaseType() != null && fc.getDocumentType() != null) {
					sourcebasepath = new String(lookupScanning.getLookupValue() + File.separator + scan_date
							+ File.separator + fc.getCaseType() + File.separator + fc.getDocumentType());
				} else {
					sourcebasepath = new String(lookupScanning.getLookupValue() + File.separator + scan_date
							+ File.separator + fc.getCaseType());
				}
				String destination_path = null;
				File folder = new File(sourcebasepath);

				File[] flist = folder.listFiles();
				if (flist != null) {
					for (File file : flist) {
						if (file.getName().contains(diary)) {
							Date = new String();
							Date = formatter.format(fc.getQcOn());
							destination_path = new String();

							if (fc.getCaseType() != null && fc.getDocumentType() != null) {
								destination_path = pending_lookup.getLookupValue() + File.separator + Date + File.separator
										+ fc.getCaseType() + File.separator + fc.getDocumentType() + File.separator
										+ file.getName();
							}

							else {
								destination_path = pending_lookup.getLookupValue() + File.separator + Date + File.separator
										+ fc.getCaseType() + File.separator + file.getName();

							}

							File source = new File(sourcebasepath + File.separator + file.getName());
							File dest = new File(destination_path);
							 Path src=Paths.get(sourcebasepath+File.separator+file.getName());
		                       Path des=Paths.get(destination_path);

							int no_of_pages = 0;
							boolean copy_flag = false;
							try {
								PdfReader reader = new PdfReader(source.getAbsolutePath());
								no_of_pages = reader.getNumberOfPages();
								//Files.move(source.toPath(),dest.toPath().resolve(source.getName()),StandardCopyOption.REPLACE_EXISTING);
								
									Files.delete(src);
								

							} catch (IOException e) {
								e.printStackTrace();
							}

							System.out
									.println("Counter QC Done Moved Successfully " + fc.getDiaryNo() + " " + fc.getYear());

						}
					}
				} else {

				}

			}

		}
	}


