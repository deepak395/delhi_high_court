package com.dms.app;

import static  java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.Lookup;
import com.dms.model.PendingCase;
import com.dms.service.MoveScanToQc;
import com.itextpdf.text.pdf.PdfReader;

public class QcPendingToQcData {

	public static void main(String[] args) throws IOException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		MoveScanToQc m = (MoveScanToQc) ctx.getBean("moveScanToQc");
		List<FreshCase> scanList = m.getFreshListForScan("READY TO MOVE QC FILES");

		moveCaseFiles(m, scanList);

	}

	public static void moveCaseFiles(MoveScanToQc mc, List<FreshCase> scanList) {

		Lookup lookupScanning = mc.getLookUpObjectDMS("COUNTER_QC_PENDING");
		Lookup pending_lookup = mc.getLookUpObjectDMS("COUNTER_QC_DATA");

		PendingCase pd = null;
		Date date = new Date();
		SimpleDateFormat s =new SimpleDateFormat("yyyy");
		String currentYear =s.format(date);
		//currentYear ="2031";
		SimpleDateFormat s1 =new SimpleDateFormat("MMM");
		String currentMonth =s1.format(date);
		
		
		
		SimpleDateFormat s2 =new SimpleDateFormat("dd-MM-YYYY");
		String currentDate =s2.format(date);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
		String sourcebasepath = null;
		String diary = null;
		for (FreshCase fc : scanList) {
			String Date = null;
			String scan_date = null;
			diary = new String();
			scan_date = new String();
			scan_date = formatter.format(fc.getScanningOn());
			diary = fc.getDiaryNo() + "-" + fc.getYear();
			if (fc.getCaseType() != null && fc.getDocumentType() != null) {
				sourcebasepath = new String(lookupScanning.getLookupValue() + File.separator + scan_date
						+ File.separator + fc.getCaseType() + File.separator + fc.getDocumentType());
			} else {
				sourcebasepath = new String(lookupScanning.getLookupValue() + File.separator + scan_date
						+ File.separator + fc.getCaseType());
			}
			String destination_path = null;
			File folder = new File(sourcebasepath);

			File[] flist = folder.listFiles();
			if (flist != null) {
				for (File file : flist) {
					if (file.getName().contains(diary)) {
						Date = new String();
						Date = formatter.format(fc.getQcOn());
						destination_path = new String();

						if (fc.getCaseType() != null && fc.getDocumentType() != null) {
							destination_path = pending_lookup.getLookupValue() + File.separator +fc.getCaseType() + File.separator +currentYear+File.separator+currentMonth+file.separator+Date+File.separator+fc.getDocumentType() + File.separator
									+ file.getName();
						}

						else {
							destination_path = pending_lookup.getLookupValue() + File.separator +fc.getCaseType() + File.separator +currentYear+File.separator+currentMonth+file.separator+Date+ File.separator
									+ file.getName();

						}

						File source = new File(sourcebasepath + File.separator + file.getName());
						File dest = new File(destination_path);
						 Path src=Paths.get(sourcebasepath+File.separator+file.getName());
	                       Path des=Paths.get(destination_path);

						int no_of_pages = 0;
						boolean copy_flag = false;
						try {
							/*PdfReader reader = new PdfReader(source.getAbsolutePath());
							no_of_pages = reader.getNumberOfPages();*/
							Files.move(src, des, REPLACE_EXISTING);
							copy_flag = true;
							PdfReader reader = new PdfReader(dest.getAbsolutePath());
							no_of_pages = reader.getNumberOfPages();
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (copy_flag) {
							FreshCaseDocument fcd=null;
							fcd =mc.getAllFreshDocument(fc.getId(), file.getName());
								if(fcd==null){
							fcd= new FreshCaseDocument();
							fcd.setYear(fc.getYear());
							fcd.setCaseStage("QC DONE");
							fcd.setCaseType(fc.getCaseType());
							fcd.setFreshcaseId(fc.getId());
							fcd.setQcPage(no_of_pages);
							fcd.setQcBy(fc.getQcBy());
							fcd.setDiaryNo(fc.getDiaryNo());
							fcd.setQcfullname(fc.getQcFullname());
							fcd.setDocumentName(file.getName());
							fcd.setDocumentType(fc.getDocumentType());
							fcd.setStatus(true);
							mc.save(fcd);
							fc.setCaseStage("QC DONE");
							mc.save(fc);

						}
								
								else{
									fcd.setYear(fc.getYear());
									fcd.setCaseStage("QC DONE");
									fcd.setCaseType(fc.getCaseType());
									fcd.setFreshcaseId(fc.getId());
									fcd.setQcPage(no_of_pages);
									fcd.setQcBy(fc.getQcBy());
									fcd.setDiaryNo(fc.getDiaryNo());
									fcd.setQcfullname(fc.getQcFullname());
									fcd.setDocumentName(file.getName());
									fcd.setDocumentType(fc.getDocumentType());
									fcd.setStatus(true);
									mc.save(fcd);
									fc.setCaseStage("QC DONE");
									mc.save(fc);

								}
									
									
								}

						System.out
								.println("Counter QC Done Moved Successfully " + fc.getDiaryNo() + " " + fc.getYear());

					}
				}
			} else {

			}

		}

	}
}
