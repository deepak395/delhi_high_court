package com.dms.app;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.dms.model.Lookup;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;
import com.dms.service.MoveScanToQc;
import com.itextpdf.text.pdf.PdfReader;

@Component
public class MoveCaseFiles {

	
	//private static Logger logger=Logger.getLogger(MoveCaseFiles.class);
	
	public static void main(String[] args) throws IOException {
		
		

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		MoveScanToQc m = (MoveScanToQc) ctx.getBean("moveScanToQc");
		//CaseFileDetailService caseFileDetailService = (CaseFileDetailService) ctx.getBean("caseFileDetailService");
	//	LookupService lookupService = (LookupService) ctx.getBean("lookupService");
		//SubDocumentService subDocumentService = (SubDocumentService) ctx.getBean("subDocumentService");
		//MetaDataService metaDataService = (MetaDataService) ctx.getBean("metaDataService");
	//	List<PendingCaseDocument> casefileList = m.getPendingListForScan();
		List<PendingCase> scanList = m.getPendingListForScan("READY TO MOVE SCANNED FILES");
		
			moveCaseFiles(m,scanList);

}
	public static void moveCaseFiles(MoveScanToQc mc,List<PendingCase> scanList){

		 // logger.info("");
		Lookup lookupScanning = mc.getLookUpObjectDMS("SCAN");
		Lookup pending_lookup = mc.getLookUpObjectDMS("PENDING_SCAN");
		//Lookup replica = mc.getLookUpObjectDMS("SCAN_REPLICA");
		
		 //StringBuilder destination_path=null;
		 PendingCase pd=null;
		 
		SimpleDateFormat  formatter = new SimpleDateFormat("dd-M-yyyy");  
		   
		   
		 String sourcebasepath=null;
	   for(PendingCase pc:scanList){
		   String Date=null;
		   sourcebasepath=new String(lookupScanning.getLookupValue()+File.separator+pc.getScannedBy()+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear());
		 String destination_path= null;
		 pd= new PendingCase();
		 File folder = new File(sourcebasepath);
		 File[] flist= folder.listFiles();
		 PendingCaseDocument pcd= null;
		   if(flist!=null){
		    for(File file: flist){
		    	Date= new String();
		    	
		    	Date=formatter.format(pc.getScannedOn());
		    	destination_path=new String();
		    	if(pc.getCaseType().equalsIgnoreCase("UPDATE")){
		    	destination_path=pending_lookup.getLookupValue()+File.separator+Date+File.separator+"UPDATE"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear()+File.separator+file.getName();
		    	}
		    	else  if(pc.getCaseType().equalsIgnoreCase("DAK")){
		    		destination_path=pending_lookup.getLookupValue()+File.separator+Date+File.separator+"DAK"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear()+File.separator+file.getName();    		
		    		
		    	}
		    	else{
		    		destination_path=pending_lookup.getLookupValue()+File.separator+Date+File.separator+"FRESH"+File.separator+pc.getCaseCategory()+"_"+pc.getCaseNo()+"_"+pc.getCaseYear()+File.separator+file.getName();
		    	
			     }
		
		    	File source= new File(sourcebasepath+File.separator+file.getName());
		    	File dest= new File(destination_path);
		    	
		    	  int no_of_pages=0;
		    	  boolean copy_flag=false;
		    	try {
					PdfReader reader = new PdfReader(source.getAbsolutePath());
					no_of_pages = reader.getNumberOfPages();
					FileUtils.copyFile(source, dest);
					 pc.setCaseStage("SCANNING DONE");
					 mc.save(pc);
					 
					copy_flag = true;
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	  if(copy_flag){
		    		pcd= new PendingCaseDocument();
		    	   pcd.setCaseCategory(pc.getCaseCategory());
		    	   pcd.setCaseNo(pc.getCaseNo());
		    	   pcd.setCaseYear(pc.getCaseYear());
		    	   pcd.setCaseStage("SCANNING DONE");
		    	   pcd.setCaseType(pc.getCaseType());
		    	   pcd.setPendingCaseId(pc.getPendingCaseId());
		    	   pcd.setScanningPage(no_of_pages);
		    	   pcd.setScanningBy(pc.getScannedBy());
		    	   pcd.setScanningOn(pc.getScannedOn());
		    	   pcd.setDocumentName(file.getName());
		    	   pcd.setStatus(true);
		    	   mc.save(pcd);
				
			}
			 
			 
			 
			 
		 }
		    System.out.println("Scanning Done Moved Successfully "+ pc.getCaseCategory()+ " "+pc.getCaseNo()+ " "+pc.getCaseYear()   );
			
		   }
		   else {
			   
			   //logger.error("");
		   }
		 
	 	}

	}
}
	
	

	
	