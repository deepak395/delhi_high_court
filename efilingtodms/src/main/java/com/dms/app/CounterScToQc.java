package com.dms.app;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.Lookup;
import com.dms.model.PendingCase;
import com.dms.service.MoveScanToQc;
import com.itextpdf.text.pdf.PdfReader;

@Component
public class CounterScToQc {

	// private static Logger logger=Logger.getLogger(MoveCaseFiles.class);

	public static void main(String[] args) throws IOException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		MoveScanToQc m = (MoveScanToQc) ctx.getBean("moveScanToQc");
		// CaseFileDetailService caseFileDetailService = (CaseFileDetailService)
		// ctx.getBean("caseFileDetailService");
		// LookupService lookupService = (LookupService)
		// ctx.getBean("lookupService");
		// SubDocumentService subDocumentService = (SubDocumentService)
		// ctx.getBean("subDocumentService");
		// MetaDataService metaDataService = (MetaDataService)
		// ctx.getBean("metaDataService");
		// List<PendingCaseDocument> casefileList = m.getPendingListForScan();
		List<FreshCase> scanList = m.getFreshListForScan("READY TO MOVE SCANNED FILES");

		moveCaseFiles(m, scanList);

	}

	public static void moveCaseFiles(MoveScanToQc mc, List<FreshCase> scanList) {

		// logger.info("");
		Lookup lookupScanning = mc.getLookUpObjectDMS("COUNTER_SCAN");
		Lookup pending_lookup = mc.getLookUpObjectDMS("COUNTER_QC_PENDING");
		// Lookup replica = mc.getLookUpObjectDMS("SCAN_REPLICA");

		// StringBuilder destination_path=null;
		PendingCase pd = null;

		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
		String sourcebasepath = null;
		String diary = null;
		for (FreshCase fc : scanList) {
			String Date = null;
			diary = new String();
			diary = fc.getDiaryNo() + "-" + fc.getYear();
			if (fc.getCaseType() != null && fc.getDocumentType() != null) {
				sourcebasepath = new String(lookupScanning.getLookupValue() + File.separator + fc.getScanningBy()
						+ File.separator + fc.getCaseType() + File.separator + fc.getDocumentType());
			} else {
				sourcebasepath = new String(lookupScanning.getLookupValue() + File.separator + fc.getScanningBy()
						+ File.separator + fc.getCaseType());
			}
			String destination_path = null;
			File folder = new File(sourcebasepath);

			File[] flist = folder.listFiles();
			FreshCaseDocument fcd = null;
			if (flist != null) {
				for (File file : flist) {
					if (file.getName().contains(diary)) {
						Date = new String();
						Date = formatter.format(fc.getScanningOn());
						destination_path = new String();

						if (fc.getCaseType() != null && fc.getDocumentType() != null) {
							destination_path = pending_lookup.getLookupValue() + File.separator + Date + File.separator
									+ fc.getCaseType() + File.separator + fc.getDocumentType() + File.separator
									+ file.getName();
						}

						else {
							destination_path = pending_lookup.getLookupValue() + File.separator + Date + File.separator
									+ fc.getCaseType() + File.separator + file.getName();

						}

						File source = new File(sourcebasepath + File.separator + file.getName());
						File dest = new File(destination_path);

						int no_of_pages = 0;
						boolean copy_flag = false;
						try {
							PdfReader reader = new PdfReader(source.getAbsolutePath());
							no_of_pages = reader.getNumberOfPages();

							FileUtils.copyFile(source, dest);
							copy_flag = true;
							fc.setCaseStage("SCANNING DONE");
							mc.save(fc);

						} catch (IOException e) {
							e.printStackTrace();
						}
						if (copy_flag) {
							fcd = new FreshCaseDocument();
							fcd.setYear(fc.getYear());
							fcd.setCaseStage("SCANNING DONE");
							fcd.setCaseType(fc.getCaseType());
							fcd.setFreshcaseId(fc.getId());
							fcd.setScanningPage(no_of_pages);
							fcd.setScanningBy(fc.getScanningBy());
							fcd.setScanningfullname(fc.getScannerFullname());
							fcd.setDiaryNo(fc.getDiaryNo());
							fcd.setScanningOn(fc.getScanningOn());
							fcd.setDocumentName(file.getName());
							fcd.setDocumentType(fc.getDocumentType());
							fcd.setStatus(true);
							mc.save(fcd);

						}

						System.out.println(
								"Counter Scanning Done Moved Successfully " + fc.getDiaryNo() + " " + fc.getYear());

					}
				}
			} else {

				// logger.error("");
			}

		}

	}
}
