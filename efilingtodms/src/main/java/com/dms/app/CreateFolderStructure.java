package com.dms.app;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateFolderStructure {

	public static void main(String[] args) {

		//CreatefolderStructureAtQcData("D:\\DHC_REPOSITORY\\COUNTER\\QC_DATA");
		CreatefolderStructureAtBookMarkData("D:\\DHC_REPOSITORY\\COUNTER\\BOOKMARKED_DATA");

	}


	public static void CreatefolderStructureAtQcData(String path) {
		String sourcePath = path;
		String writ = "WRIT";
		Date date = new Date();

		SimpleDateFormat s = new SimpleDateFormat("yyyy");
		String currentYear = s.format(date);
		// currentYear ="2031";
		SimpleDateFormat s1 = new SimpleDateFormat("MMM");
		String currentMonth = s1.format(date);

		SimpleDateFormat s2 = new SimpleDateFormat("dd-MM-YYYY");
		String currentDate = s2.format(date);

		List<String> caseTypes = new ArrayList<String>();
		caseTypes.add("WRIT");
		caseTypes.add("APPEAL");
		caseTypes.add("CAVEAT");
		caseTypes.add("CRIMINAL");
		caseTypes.add("SUIT");

		for (String caseType : caseTypes) {
			File theDir = new File(sourcePath + File.separator + caseType);
			if (theDir.exists()) {

				File caseDir = new File(theDir + File.separator + currentYear);

				if (caseDir.exists()) {

					File monDir = new File(caseDir + File.separator + currentMonth);

					if (monDir.exists()) {
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						}
					} else if (!monDir.exists()) {
						monDir.mkdir();
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						}

					}
				} else if (!caseDir.exists()) {

					caseDir.mkdir();

					File monDir = new File(caseDir + File.separator + currentMonth);

					if (monDir.exists()) {

					} else if (!monDir.exists()) {
						monDir.mkdir();
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							} else if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						}
					}

				}

			} else if (!theDir.exists()) {

				theDir.mkdir();
				File caseDir = new File(theDir + File.separator + currentYear);

				if (caseDir.exists()) {

					File monDir = new File(caseDir + File.separator + currentMonth);

					if (monDir.exists()) {
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						}

					} else if (!monDir.exists()) {
						monDir.mkdir();
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						}
					}
				} else if (!caseDir.exists()) {

					caseDir.mkdir();
					File monDir = new File(caseDir + File.separator + currentMonth);

					if (monDir.exists()) {
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						}
					} else if (!monDir.exists()) {
						monDir.mkdir();
						File dateDir = new File(monDir + File.separator + currentDate);

						if (dateDir.exists()) {
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}

						} else if (!dateDir.exists()) {
							dateDir.mkdir();
							if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "DOCUMENT");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}

							}
							if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
								File docDir = new File(dateDir + File.separator + "IA");

								if (docDir.exists()) {

								} else if (!docDir.exists()) {
									docDir.mkdir();

								}
							}
						}
					}

				}

			}

		}
	}



public static void CreatefolderStructureAtBookMarkData(String path){
	

	String sourcePath = path;
	String writ = "WRIT";
	Date date = new Date();

	SimpleDateFormat s = new SimpleDateFormat("yyyy");
	String currentYear = s.format(date);
	// currentYear ="2031";
	SimpleDateFormat s1 = new SimpleDateFormat("MMM");
	String currentMonth = s1.format(date);

	SimpleDateFormat s2 = new SimpleDateFormat("dd-MM-YYYY");
	String currentDate = s2.format(date);

	List<String> caseTypes = new ArrayList<String>();
	caseTypes.add("WRIT");
	caseTypes.add("APPEAL");
	caseTypes.add("CAVEAT");
	caseTypes.add("CRIMINAL");
	caseTypes.add("SUIT");

	for (String caseType : caseTypes) {
		File theDir = new File(sourcePath + File.separator + caseType);
		if (theDir.exists()) {

			File caseDir = new File(theDir + File.separator + currentYear);

			if (caseDir.exists()) {

				File monDir = new File(caseDir + File.separator + currentMonth);

				if (monDir.exists()) {
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {
						/*if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							//File docDir = new File(dateDir + File.separator + "DOCUMENT");

						}
						if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							//File docDir = new File(dateDir + File.separator + "IA");

						}*/

					} else if (!dateDir.exists()) {
						dateDir.mkdir();
						/*if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							//File docDir = new File(dateDir + File.separator + "DOCUMENT");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}

						}*/
						/*if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "IA");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}
						}*/
					}
				} else if (!monDir.exists()) {
					monDir.mkdir();
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {
						/*if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "DOCUMENT");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}

						}*/
						/*if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "IA");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}
						}*/

					} else if (!dateDir.exists()) {
						dateDir.mkdir();
						if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "DOCUMENT");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}

						}
						/*if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "IA");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}
						}*/
					}

				}
			} else if (!caseDir.exists()) {

				caseDir.mkdir();

				File monDir = new File(caseDir + File.separator + currentMonth);

				if (monDir.exists()) {

				} else if (!monDir.exists()) {
					monDir.mkdir();
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {
						/*if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "DOCUMENT");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}

						}*/
						/*if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "IA");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}
						}*/

					} else if (!dateDir.exists()) {
						dateDir.mkdir();
						/*if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "DOCUMENT");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}

						}*/ /*else if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "IA");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}
						}*/

					}
				}

			}

		} else if (!theDir.exists()) {

			theDir.mkdir();
			File caseDir = new File(theDir + File.separator + currentYear);

			if (caseDir.exists()) {

				File monDir = new File(caseDir + File.separator + currentMonth);

				if (monDir.exists()) {
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {
						/*if (caseType.equals("WRIT") || caseType.equals("APPEAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "DOCUMENT");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}*/

						}
						/*if (caseType.equals("CRIMINAL") || caseType.equals("SUIT")) {
							File docDir = new File(dateDir + File.separator + "IA");

							if (docDir.exists()) {

							} else if (!docDir.exists()) {
								docDir.mkdir();

							}
						}*/

					 else if (!dateDir.exists()) {
						dateDir.mkdir();
						
						
					}

				} else if (!monDir.exists()) {
					monDir.mkdir();
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {
						

					} else if (!dateDir.exists()) {
						dateDir.mkdir();
						

						}
						
					}
				}
			 else if (!caseDir.exists()) {

				caseDir.mkdir();
				File monDir = new File(caseDir + File.separator + currentMonth);

				if (monDir.exists()) {
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {
						
						
					} else if (!dateDir.exists()) {
						dateDir.mkdir();
						
						
					}
				} else if (!monDir.exists()) {
					monDir.mkdir();
					File dateDir = new File(monDir + File.separator + currentDate);

					if (dateDir.exists()) {

					} else if (!dateDir.exists()) {
						dateDir.mkdir();
						
						
					}
				}

			}

		}

	}

	
	
	
	
	
}
}





