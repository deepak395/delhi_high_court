
package com.dms.app;
import static  java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.Lookup;
import com.dms.model.PendingCase;
import com.dms.service.MoveScanToQc;
import com.itextpdf.text.pdf.PdfReader;

public class QcDoneToBookMarkDone {

	public static void main(String[] args) throws IOException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		MoveScanToQc m = (MoveScanToQc) ctx.getBean("moveScanToQc");
	
		List<FreshCase> scanList = m.getFreshListForScan("READY TO MOVE BOOKMARK FILES");

		moveCaseFiles(m, scanList);

	}

	public static void moveCaseFiles(MoveScanToQc mc, List<FreshCase> scanList) {

		Lookup lookupScanning = mc.getLookUpObjectDMS("COUNTER_QC_DATA");
		Lookup pending_lookup = mc.getLookUpObjectDMS("COUNTER_BOOKMARK_DATA");
		PendingCase pd = null;
		Date date = new Date();
		SimpleDateFormat s =new SimpleDateFormat("yyyy");
		String currentYear =s.format(date);
		//currentYear ="2031";
		SimpleDateFormat s1 =new SimpleDateFormat("MMM");
		String currentMonth =s1.format(date);
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
		String sourcebasepath = null;
		String diary = null;
		for (FreshCase fc : scanList) {
			String Date = null;
			String qc_date = null;
			diary = new String();
			qc_date = new String();
			qc_date = formatter.format(fc.getQcOn());
			diary = fc.getDiaryNo() + "-" + fc.getYear();
			if (fc.getCaseType() != null && fc.getDocumentType() != null) {
				sourcebasepath = lookupScanning.getLookupValue() + File.separator +fc.getCaseType() + File.separator +currentYear+File.separator+currentMonth+File.separator+qc_date+File.separator+fc.getDocumentType();
			}
		     else {
		    	 sourcebasepath = lookupScanning.getLookupValue() + File.separator +fc.getCaseType() + File.separator +currentYear+File.separator+currentMonth+File.separator+qc_date;
			}
			String destination_path = null;
			File folder = new File(sourcebasepath);
			File[] flist = folder.listFiles();
			if (flist != null) {
				for (File file : flist) {
					if (file.getName().contains(diary)) {
						Date = new String();
						Date = formatter.format(fc.getBookmarkingOn());
						destination_path = new String();

						if (fc.getCaseType() != null && fc.getDocumentType() != null) {
							destination_path = pending_lookup.getLookupValue() + File.separator +fc.getCaseType() + File.separator +currentYear+File.separator+currentMonth+file.separator+Date+fc.getDocumentType() + File.separator
									+ file.getName();
						}

						else {
							destination_path = pending_lookup.getLookupValue() + File.separator +fc.getCaseType() + File.separator +currentYear+File.separator+currentMonth+file.separator+Date+ File.separator
									+ file.getName();
						}
						File source = new File(sourcebasepath + File.separator + file.getName());
						File dest = new File(destination_path);
						 Path src=Paths.get(sourcebasepath+File.separator+file.getName());
	                       Path des=Paths.get(destination_path);

						int no_of_pages = 0;
						boolean copy_flag = false;
						try {
							Files.move(src,des,REPLACE_EXISTING);
							copy_flag = true;
							PdfReader reader = new PdfReader(dest.getAbsolutePath());
							no_of_pages = reader.getNumberOfPages();
							fc.setCaseStage("BOOKMARK DONE");
							mc.save(fc);

						} catch (IOException e) {
							e.printStackTrace();
						}
						if (copy_flag) {
							FreshCaseDocument fcd=null;
							fcd =mc.getAllFreshDocument(fc.getId(), file.getName());
							if(fcd==null){
							fcd= new FreshCaseDocument();
							fcd.setYear(fc.getYear());
							fcd.setCaseStage("BOOKMARK DONE");
							fcd.setCaseType(fc.getCaseType());
							fcd.setFreshcaseId(fc.getId());
							fcd.setBookmarkingPage(no_of_pages);
							fcd.setBookmarkingBy(fc.getBookmarkingBy());
							fcd.setDiaryNo(fc.getDiaryNo());
						   fcd.setBookmarkingfullname(fc.getBookMarkFullname());
							fcd.setDocumentName(file.getName());
							fcd.setDocumentType(fc.getDocumentType());
							fcd.setStatus(true);
							mc.save(fcd);
							fc.setCaseStage("BOOKMARK DONE");
							mc.save(fc);

						}
								
								else{
									fcd.setYear(fc.getYear());
									fcd.setCaseStage("BOOKMARK DONE");
									fcd.setCaseType(fc.getCaseType());
									fcd.setFreshcaseId(fc.getId());
									fcd.setBookmarkingPage(no_of_pages);
									fcd.setBookmarkingBy(fc.getBookmarkingBy());
									fcd.setDiaryNo(fc.getDiaryNo());
								   fcd.setBookmarkingfullname(fc.getBookMarkFullname());
									fcd.setDocumentName(file.getName());
									fcd.setDocumentType(fc.getDocumentType());
									fcd.setStatus(true);
									mc.save(fcd);
									fc.setCaseStage("BOOKMARK DONE");
									mc.save(fc);

								}
						}

						System.out.println(
								"Counter BookMark Done Moved Successfully " + fc.getDiaryNo() + " " + fc.getYear());

					}
				}
			} else {

				// logger.error("");
			}

		}

	}

}
