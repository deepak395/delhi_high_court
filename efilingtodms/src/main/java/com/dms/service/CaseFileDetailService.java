/*package com.dms.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.efiling.model.CaseFileDetail;
import com.efiling.model.Caveat;
import com.efiling.model.CaveatOld;
import com.efiling.model.StampReporterData;


@Service
public class CaseFileDetailService {

	@PersistenceContext(unitName="persistenceUnitEFILING")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager em;
	
	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em2;
	
	
	@Transactional("transactionManager2")
	public DMSCaseFileDetail save(DMSCaseFileDetail s) {

		DMSCaseFileDetail master = null;
		try {
			master = em2.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public DMSCaseFileDetail getCaseFileByCaseTypeNoYear(Long caseType,String caseNo,Integer caseYear) 
	{
		System.out.println("caseType "+caseType+" caseNo "+caseNo+" caseYear "+caseYear);
		DMSCaseFileDetail result=new DMSCaseFileDetail();
		try {
			result = (DMSCaseFileDetail) em2.createQuery("SELECT cfd FROM DMSCaseFileDetail cfd WHERE cfd.fd_case_type="+caseType+" and cfd.fd_case_no = '"+caseNo+"' and cfd.fd_case_year="+caseYear+" and cfd.fd_rec_status = 1").getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	

	@Transactional
	public StampReporterData getStampReporterData(Long id) {

		StampReporterData result=null;
	    Query query=null;
		query = em.createQuery("SELECT srd from StampReporterData srd where srd.srd_rcd_mid=:id").setParameter("id", id);
		try {
			result=(StampReporterData) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Transactional
	public Long getEcourtFeesSum(Long id) {

		Long result=null;
	    Query query=null;
		query = em.createQuery("SELECT sum(cf.cf_amount) from CourtFee cf where cf.cf_rcd_mid=:id").setParameter("id", id);
		try {
			result=(Long) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public CaveatOld getCaveatOld(int no,int year) {

		CaveatOld result=null;
	    Query query=null;
		query = em.createQuery("SELECT c from CaveatOld c where c.cav_no=:no and c.cav_year=:year").setParameter("no", no).setParameter("year", year);
		try {
			result=(CaveatOld) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Transactional
	public Caveat getCaveat(int no,int year) {

		Caveat result=null;
	    Query query=null;
		query = em.createQuery("SELECT c from Caveat c where c.cav_no=:no and c.cav_year=:year").setParameter("no", no).setParameter("year", year);
		try {
			result=(Caveat) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Transactional
	public Long getCount(Long fd_id) {
		Long result=0L;
		try{
			result =(Long) em2.createQuery("SELECT count(sd) FROM DMSSubDocuments sd where sd.sd_fd_mid="+fd_id).getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional("transactionManager2")
	public DMSImpugnedOrder saveIO(DMSImpugnedOrder s) 
	{
		DMSImpugnedOrder master = null;
		try 
		{
			master = em2.merge(s);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional("transactionManager2")
	public void saveActDetails(DMSActDetails act_details) 
	{
		try 
		{
			em2.merge(act_details);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	@Transactional("transactionManager2")
	public void savePetDetails(DMSPetitionerDetails ptDetails) {
		{
			try 
			{
				em2.merge(ptDetails);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	@Transactional("transactionManager2")
	public void saveResDetails(DMSRespondentDetails rtDetails) {
		{
			try 
			{
				em2.merge(rtDetails);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		
	}
	@Transactional("transactionManager2")
	public void savePCounsel(DMSPetitionerCounsel pCounsel) {
		{
			try 
			{
				em2.merge(pCounsel);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		
	}
	
	
	
	
	
}
	
*/