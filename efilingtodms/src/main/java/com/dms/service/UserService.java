/*package com.dms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.efiling.model.User;

@Service
public class UserService{
	
	@PersistenceContext(unitName="persistenceUnitEFILING")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager em;

	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em2;
	
	@Transactional
	public List<User> getAll() {
		List<User> result = em.createQuery("SELECT r FROM User r").getResultList();
		return result;
	}
	@Transactional
	public List<DMSUser> getDMSUserList() {
		List<DMSUser> result = em2.createQuery("SELECT r FROM DMSUser r").getResultList();
		return result;
	}
	
	@Transactional
	public User save(User u) {
		User user = em.merge(u);
		return user;
	}
	@Transactional
	public DMSUser save(DMSUser u) {
		DMSUser user = em2.merge(u);
		return user;
	}
	@Transactional("transactionManager2")
	public DMSUser saveDMS(DMSUser u) {
		DMSUser user = em2.merge(u);
		return user;
	}

}
*/