/*package com.dms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class MetaDataService {	

	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em;
	
	@Transactional("transactionManager2")
	public DMSMetaData save(DMSMetaData metaData) {
		DMSMetaData result = new DMSMetaData();		
		try {
			result = em.merge(metaData);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return result;
	}
	
	@Transactional
	public List<DMSMetaField> getAll() {
		List<DMSMetaField> result = new ArrayList<DMSMetaField>() ;
		try{
			result = em.createQuery("SELECT m FROM MetaField m where mf_rec_status =1 ORDER BY m.mf_sequence ").getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<Object> getDropDownList(String query) {
		List<Object> result = new ArrayList<Object>() ;
		try{
			result = em.createQuery(query).getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}	
	
	@Transactional
	public List<DMSMetaData> getAllByfdid(Long fd_id) {
		List<DMSMetaData> result = new ArrayList<DMSMetaData>() ;
		try{
			result = em.createQuery("SELECT md FROM DMSMetaData md_rec_status =1 AND md where md_fd_mid = "+fd_id).getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	@Transactional
	public DMSMetaData getByPk(Long md_id) {
		DMSMetaData result = new DMSMetaData() ;
		try{
			result = (DMSMetaData) em.createQuery("SELECT md FROM DMSMetaData md where md_id = "+md_id).getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<DMSMetaData> getAllData(Long md_fd_mid) {
		List<DMSMetaData> result = new ArrayList<DMSMetaData>() ;
		try{
			Query query = em
					.createQuery("SELECT m FROM DMSMetaData m WHERE md_rec_status =1 AND md_fd_mid=:md_fd_mid");
			query.setParameter("md_fd_mid", md_fd_mid);			
			result = (List<DMSMetaData>) query.getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<DMSMetaData> getByfd_mfid(Long md_fd_mid,Long md_mf_mid) {
		List<DMSMetaData> result = new ArrayList<DMSMetaData>() ;
		try{
			Query query = em
					.createQuery("SELECT m FROM DMSMetaData m WHERE md_rec_status =1 AND md_fd_mid=:md_fd_mid and md_mf_mid=:md_mf_mid");
			query.setParameter("md_fd_mid", md_fd_mid).setParameter("md_mf_mid", md_mf_mid);			
			result = (List<DMSMetaData>) query.getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	

	
	
	@Transactional
	public void deleteByPk(Long id) {
		DMSMetaData r2 = em.find(DMSMetaData.class, id);
		em.remove(r2);
	}
	
	@Transactional
	public Integer getByMaxSequence(Long md_fd_mid, Long md_mf_mid) {
		// TODO Auto-generated method stub
		Integer sequence=2;
		try{
			Query query = em
					.createQuery("select max(md_rec_status) from DMSMetaData where md_fd_mid=:md_fd_mid and md_mf_mid=:md_mf_mid");
			query.setParameter("md_fd_mid", md_fd_mid).setParameter("md_mf_mid", md_mf_mid);			
			sequence = (Integer) query.getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return sequence;
	}



}
*/