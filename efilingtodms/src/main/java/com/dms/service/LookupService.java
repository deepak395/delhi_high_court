/*package com.dms.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.efiling.model.Lookup;

@Service
public class LookupService
{
	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em2;
	
	@PersistenceContext(unitName="persistenceUnitEFILING")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager em;
	
	@Transactional
	public DMSLookup getLookUpObjectDMS(String setname) {
		DMSLookup result = new DMSLookup() ;
		try{			
			String sql = "SELECT l FROM DMSLookup l WHERE l.lk_setname= :setname  AND l.lk_rec_status=1";
			result = (DMSLookup) em2.createQuery(sql).setParameter("setname", setname).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}return result;
	}
	
	@Transactional
	public Lookup getLookUpObjectEfiling(String setname) {
		Lookup result = new Lookup() ;
		try{			
			String sql = "SELECT l FROM Lookup l WHERE l.lk_setname= :setname  AND l.lk_rec_status=1";
			result = (Lookup) em.createQuery(sql).setParameter("setname", setname).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}return result;
	}

}
*/