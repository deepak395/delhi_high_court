/*package com.dms.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.DMSApplicationIndexFieldMapping;
import com.efiling.model.Application;
import com.efiling.model.ApplicationStage;
import com.efiling.model.SubApplication;

@Service
public class ApplicationService {
	@PersistenceContext(unitName = "persistenceUnitEFILING")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager em;

	@PersistenceContext(unitName = "persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em2;

	@Transactional("transactionManager")
	public Application save(Application s) {

		Application master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}

	@Transactional("transactionManager")
	public ApplicationStage saveStage(ApplicationStage s) {

		ApplicationStage master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}

	@Transactional
	public List<Application> getApplicationsForMove() {
		List<Application> result = new ArrayList<Application>();
		try {
			result = em.createQuery("SELECT ap FROM Application ap where ap.ap_stage_lid=1000043").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Transactional
	public ApplicationStage getFileSubmittedStage(Long ap_id) {
		ApplicationStage result = new ApplicationStage();
		try {
			result = (ApplicationStage) em.createQuery("SELECT aps FROM ApplicationStage aps where aps.as_ap_mid="
					+ ap_id + " and aps.as_stage_lid=1000042").getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Transactional
	public List<DMSApplicationIndexFieldMapping> getApplicationIndexFieldMapping() {
		List<DMSApplicationIndexFieldMapping> result = new ArrayList<DMSApplicationIndexFieldMapping>();
		try {
			result = em.createQuery("SELECT aim FROM DMSApplicationIndexFieldMapping aim").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Transactional
	public Long getFilesCount(Long fd_id, Long if_id) {
		Long result = 0L;
		try {
			result = (Long) em2.createQuery("SELECT count(sd) FROM DMSSubDocuments sd where sd.sd_fd_mid=" + fd_id
					+ " and sd.sd_if_mid=" + if_id).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Transactional
	public Long getCount(Long fd_id) {
		Long result = 0L;
		try {
			result = (Long) em2.createQuery("SELECT count(sd) FROM DMSSubDocuments sd where sd.sd_fd_mid=" + fd_id)
					.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<SubApplication> getSubApplications(Long ap_id) {

		List<SubApplication> result = new ArrayList<SubApplication>();
		try {
			result = em.createQuery(
					"SELECT sbap FROM SubApplication sbap where sbap.sb_ap_mid=:ap_id and sbap.sb_ap_rec_status=1")
					.setParameter("ap_id", ap_id).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	
	 * @Transactional public DMSApplicationTypes getByPk(Long id) {
	 * DMSApplicationTypes result = (DMSApplicationTypes) em.
	 * createQuery("select at from DMSApplicationTypes at where at.at_id = :at_id").
	 * setParameter("at_id", id).getSingleResult(); return result; }
	 * 
	 * @Transactional public List<DMSApplicationTypes> getAllFiles() {
	 * List<DMSApplicationTypes> result = em.
	 * createQuery("select at from DMSApplicationTypes at where at_rec_status = 1").
	 * getResultList(); return result; }
	 

}
*/