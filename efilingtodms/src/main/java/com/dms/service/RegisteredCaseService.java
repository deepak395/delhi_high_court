package com.dms.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegisteredCaseService {	

	/*@PersistenceContext(unitName="persistenceUnitEFILING")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager em;
	
	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em2;
	
	@Transactional("transactionManager")
	public RegisteredCaseDetails save(RegisteredCaseDetails s) {

		RegisteredCaseDetails master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional("transactionManager")
	public RegisteredCaseStages saveStage(RegisteredCaseStages s) {

		RegisteredCaseStages master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public List<RegisteredCaseDetails> getCaseFilesForMove() {
		List<RegisteredCaseDetails> result = new ArrayList<RegisteredCaseDetails>() ;
		try{
			result = em.createQuery("SELECT rcd FROM RegisteredCaseDetails rcd where rcd.rcd_stage_lid=1000043").getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<PetitionerDetails> getPetitioners(Long rcd_id) {
		List<PetitionerDetails> result = new ArrayList<PetitionerDetails>() ;
		try{
			result = em.createQuery("SELECT pd FROM PetitionerDetails pd where pd.pt_rcd_mid="+rcd_id+" and pd.pt_rec_status=1 order by pd.pt_sequence ").getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<RespondentDetails> getRespondents(Long rcd_id) {
		List<RespondentDetails> result = new ArrayList<RespondentDetails>() ;
		try{
			result = em.createQuery("SELECT rd FROM RespondentDetails rd where rd.rt_rcd_mid="+rcd_id+" and rd.rt_rec_status=1 order by rd.rt_sequence").getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<ImpugnedOrder> getImpugnedOrders(Long rcd_id) {
		List<ImpugnedOrder> result = new ArrayList<ImpugnedOrder>() ;
		try{
			result = em.createQuery("SELECT io FROM ImpugnedOrder io where io.io_rcd_mid="+rcd_id+" and io.io_rec_status=1 order by io_decision_date").getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public List<ActDetails> getActDetails(Long rcd_id) {
		List<ActDetails> result = new ArrayList<ActDetails>() ;
		try{
			result = em.createQuery("SELECT ad FROM ActDetails ad where ad.act_rcd_mid="+rcd_id+" and ad.act_rec_status=1").getResultList();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public RegisteredCaseStages getFileSubmittedStage(Long rcd_id) {
		RegisteredCaseStages result = new RegisteredCaseStages() ;
		try{
			result = (RegisteredCaseStages) em.createQuery("SELECT rcs FROM RegisteredCaseStages rcs where rcs.rcs_rcd_mid="+rcd_id+" and rcs.rcs_stage_lid=1000042").getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	@Transactional
	public DMSLCCaseTypesMapping getDMSLowerCourtCaseType(Long old_id,String source) {
		DMSLCCaseTypesMapping result = new DMSLCCaseTypesMapping() ;
		try{
			result = (DMSLCCaseTypesMapping) em2.createQuery("SELECT lc FROM DMSLCCaseTypesMapping lc where lc.lcm_old_id="+old_id+" and lc.lcm_source='"+source+"'").getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	*/
		
}
