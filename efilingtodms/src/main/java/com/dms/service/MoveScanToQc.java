package com.dms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.FreshCase;
import com.dms.model.FreshCaseDocument;
import com.dms.model.Lookup;
import com.dms.model.PendingCase;
import com.dms.model.PendingCaseDocument;

@Service
public class MoveScanToQc {
	
	
	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em;
	
	
	
	@Transactional("transactionManager2")
	public PendingCase save(PendingCase s) {

		PendingCase master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional("transactionManager2")
	public List<PendingCaseDocument> getCaseFileByCaseTypeNoYear(String caseType,String caseNo,String caseYear) 
	{
		System.out.println("caseType "+caseType+" caseNo "+caseNo+" caseYear "+caseYear);
		 List<PendingCaseDocument> result=null;
		try {
			result = (List<PendingCaseDocument>)em.createQuery("SELECT pcd FROM PendingCaseDocument pcd").getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	@Transactional("transactionManager2")
	public Lookup getLookUpObjectDMS(String lookupName) {
		Lookup result = new Lookup() ;
		try{			
			String sql = "SELECT l FROM Lookup l WHERE l.lookupName= :lookupName  AND l.status=TRUE";
			result = (Lookup) em.createQuery(sql).setParameter("lookupName", lookupName).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}return result;
	}
	
	
	@Transactional("transactionManager2")
	public List<PendingCase> getPendingListForScan(String stage) 
	{
		 List<PendingCase> result=null;
		 try {
				String sql = "SELECT pc  FROM PendingCase pc WHERE pc.caseStage=:stage";
				result = (List<PendingCase>) em.createQuery(sql).setParameter("stage", stage).getResultList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return result;
	}
	
	
	
	
	
	
	@Transactional("transactionManager2")
	public PendingCaseDocument save(PendingCaseDocument pcd) {
		PendingCaseDocument master = null;
		try {
			master = em.merge(pcd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	
	
	@Transactional("transactionManager2")
	public PendingCaseDocument getAllPendingDocument(Long pending_case_id,String doc_name) 
	{
	
		 PendingCaseDocument result=null;
		try {
			String sql = "SELECT pcd  FROM PendingCaseDocument pcd WHERE pcd.pendingCaseId=:pending_caseId  AND pcd.documentName=:DocName";
			result = (PendingCaseDocument) em.createQuery(sql).setParameter("pending_caseId", pending_case_id).setParameter("DocName", doc_name).getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
	@Transactional("transactionManager2")
	public List<FreshCase> getFreshListForScan(String stage) 
	{
		 List<FreshCase> result=null;
		 try {
				String sql = "SELECT fc  FROM FreshCase fc WHERE fc.caseStage=:stage";
				result = (List<FreshCase>) em.createQuery(sql).setParameter("stage", stage).getResultList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return result;
	}
	
	
	@Transactional("transactionManager2")
	public FreshCase save(FreshCase fc) {
		FreshCase master = null;
		try {
			master = em.merge(fc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	
	
	
	
	@Transactional("transactionManager2")
	public FreshCaseDocument save(FreshCaseDocument fcd) {
		FreshCaseDocument master = null;
		try {
			master = em.merge(fcd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	
	@Transactional("transactionManager2")
	public FreshCaseDocument getAllFreshDocument(Long fresh_case_id,String doc_name) 
	{
	
		FreshCaseDocument result=null;
		try {
			String sql = "SELECT fcd  FROM FreshCaseDocument fcd WHERE fcd.freshcaseId=:fresh_case_id  AND fcd.documentName=:DocName";
			result = (FreshCaseDocument) em.createQuery(sql).setParameter("fresh_case_id", fresh_case_id).setParameter("DocName", doc_name).getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	

}
