/*package com.dms.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class SubDocumentService
{
	@PersistenceContext(unitName="persistenceUnitDMS")
	@Qualifier(value = "entityManagerFactory2")
	private EntityManager em;
	
	@Transactional("transactionManager2")
	public DMSSubDocuments save(DMSSubDocuments s) {

		DMSSubDocuments master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public DMSIndexField getIndexField(Long id) {
		DMSIndexField result = new DMSIndexField() ;
		try{			
			String sql = "SELECT dif FROM DMSIndexField dif WHERE dif.if_id= :id ";
			result = (DMSIndexField) em.createQuery(sql).setParameter("id", id).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}return result;
	}
	
	@Transactional
	public DMSSubDocuments getPetitionDocument(Long fd_id) {
		DMSSubDocuments result = new DMSSubDocuments() ;
		try{
			result = (DMSSubDocuments) em.createQuery("SELECT sd FROM DMSSubDocuments sd where sd.sd_fd_mid="+fd_id+" and sd.sd_if_mid=1  and sd.sd_rec_status=1 ").getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}

}
*/